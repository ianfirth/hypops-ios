//
//  ExpandableTableViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#define SECTION_HEIGHT 44

@interface ExpandableTableViewController : UITableViewController {
    @private
    NSMutableArray *visibility;
}

- (BOOL) tableView:(UITableView *)tableView isHeaderExpandedForSection:(NSInteger)section;
- (void) tableView:(UITableView *)tableView setHeaderExpandedForSection:(NSInteger)section expandedState:(BOOL)expanded;

- (NSString*) convertToDisplayString: (id) inputString ;

- (NSString*) convertStringToBool: (NSString*) inputString ;

@end

#pragma mark -

@interface ExpandableTableViewController (AbstractMethods)
// Define Abstract methods which should be implemented by subclasses:
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section;
- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section;
- (CGFloat)tableView:(UITableView *)tableView heightForUnExpandedHeaderInSection:(NSInteger)section;
@end
