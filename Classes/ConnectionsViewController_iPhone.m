//
//  ConnectionsViewController_iPhone.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ConnectionsViewController_iPhone.h"


@implementation ConnectionsViewController_iPhone

#pragma mark -
#pragma mark view actions

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // setup the save and cancel buttons   
    UIBarButtonItem *cancelButtonBarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(close)];
    UIBarButtonItem *saveButtonBarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveEdit)];
    
    self.navigationItem.leftBarButtonItem = cancelButtonBarItem;
    self.navigationItem.rightBarButtonItem = saveButtonBarItem;
    
}

/*
 This action is called from the cancel button bar item
 */
- (void)close {
    // quit the view
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
}

@end
