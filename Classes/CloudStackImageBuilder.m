//
//  CloudStackImageBuilder.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackImageBuilder.h"

@implementation CloudStackImageBuilder

// build an image that represents the current VM with overlayed power state icon
+ (UIImage*) buildVMImageForInstance:(CloudStackVMCore *)vm{
    UIImage *vmImage = [ImageBuilder vMImage];
    
    if ([[vm state] isEqualToString:@"Running"]){
            vmImage = [ImageBuilder vMImageStarted];        
    }
    if ([[vm state] isEqualToString:@"Stopped"]){
        vmImage = [ImageBuilder vMImageStopped];        
    }
    if ([[vm state] isEqualToString:@"Alert"]){
        vmImage = [ImageBuilder vMImageAlert];        
    }
    if ([[vm state] isEqualToString:@"Destroyed"]) {
        return [ImageBuilder vMImageDestroyed];
    }
    
    return vmImage;
}

// build an image that represents the current Host with overlayed state icon
+ (UIImage*) buildHostImageForInstance:(CloudStackHostCore *)host{
    UIImage *hostImage = [ImageBuilder hostImage];
    
    if ([[host state] isEqualToString:@"Up"]){
        hostImage = [ImageBuilder hostImageUp];        
    }
    if ([[host state] isEqualToString:@"Down"]){
        hostImage = [ImageBuilder hostImageDown];        
    }
    if ([[host state] isEqualToString:@"Alert"]){
        hostImage = [ImageBuilder hostImageAlert];        
    }
    
    return hostImage;
}

@end
