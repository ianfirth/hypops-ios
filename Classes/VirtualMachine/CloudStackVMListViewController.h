//
//  EC2ImageListViewController.h
//  hypOps
//
//  Created by Ian Firth on 06/07/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "CloudStackNativeHypervisorConnection.h"
#import "PullRefreshTableViewController.h"

@interface CloudStackVMListViewController : PullRefreshTableViewController <UISearchBarDelegate, HypervisorConnectionDelegate> {
        
    // the search predicate used to further filter the objects
    NSPredicate *searchPredicate;
    NSMutableArray *sectionKeys;
    HypervisorType hypervisorType; 
    
    NSPredicate * _rootPredicate;
}

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection predicate:(NSPredicate *) predicate;

@property (copy) NSString *hypConnID;
@property (strong) NSPredicate *rootPredicate;
@end
