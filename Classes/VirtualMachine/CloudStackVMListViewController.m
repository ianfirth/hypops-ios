//
//  EC2ImageListViewController.m
//  hypOps
//
//  Created by Ian Firth on 06/07/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "CloudStackVMListViewController.h"
#import "ImageBuilder.h"
#import "CloudStackVMCore.h"
#import "TitledDetailViewController.h"
#import "RootViewController_ipad.h"
#import "CommonUIStaticHelper.h"
#import "CloudStackImageBuilder.h"

@interface CloudStackVMListViewController (private)

-(NSPredicate *) getFinalPredicateForSection:(NSString *) sectionKey;
   - (void) configureCell:(UITableViewCell *)cell forObject:(CloudStackVMCore *)imageObject;
   - (void) reloadTableData;
   - (void) buildSectionInformation;
   - (NSArray *) sortedImagesForSection:(int) section; 
@end

@implementation CloudStackVMListViewController
@synthesize hypConnID;

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection predicate:(NSPredicate *) predicate{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        [self setHypConnID:[hypervisorConnection connectionID]];
        [self setRootPredicate:predicate];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
    }

    return self;
}

- (HypervisorConnectionFactory*) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (void)dealloc
{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    sectionKeys = nil;
}

- (void) buildSectionInformation{
  // run predicaed for each letter of aplphabet and if returns a result then add predicate and letter
  // to dictionary
  // the number of entiries in the dictionary is the number of sections, and the predicate will
  // provide the content.
    if (sectionKeys != nil){
        sectionKeys = nil;
    }
    sectionKeys = [[NSMutableArray alloc] initWithCapacity:10];

    char letter = 'A';
    while (letter <= 'Z'){
        NSString *letterStr = [NSString stringWithFormat:@"%c",letter];
        NSPredicate *pred = [self getFinalPredicateForSection:letterStr];
        NSArray *matches = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_VM_CORE withCondition:pred]; 
        if ([matches count] >0){
            // add to dictionary
            [sectionKeys addObject:letterStr];
        }        
        letter ++;
    }

    letter = '0';
    while (letter <= '9'){
        NSString *letterStr = [NSString stringWithFormat:@"%c",letter];
        NSPredicate *pred = [self getFinalPredicateForSection:letterStr];
        NSArray *matches = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_VM_CORE withCondition:pred]; 
        if ([matches count] >0){
            // add to dictionary
            [sectionKeys addObject:letterStr];
        }        
        letter ++;
    }

    // add a section for all the rest
    NSString *letterStr = @"";
    NSPredicate *pred = [self getFinalPredicateForSection:letterStr];
    NSArray *matches = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_VM_CORE withCondition:pred]; 
    if ([matches count] >0){
        // add to dictionary
        [sectionKeys addObject:letterStr];
    }            

}

-(void) setRootPredicate:(NSPredicate *)rootPredicate{
    _rootPredicate = rootPredicate;
    [self buildSectionInformation];
    [self reloadTableData];
}

-(NSPredicate *)rootPredicate{
    return _rootPredicate;
}

-(void) reloadTableData{
    // actually needs to break the things up into alpahbetical sections counts etc.
    // the same as when the thing is constucted.  Perhaps use a common method for this
    [self buildSectionInformation];
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark View Search Bar

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    // only show the status bar’s cancel button while in edit mode
    searchBar.showsCancelButton = YES;
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    // flush the previous search content
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
    
    if([searchText isEqualToString:@""] || searchText == nil){
    [self reloadTableData];
        return;
    }
    
    searchPredicate = [CloudStackVMCore nameBeginsOrContainsAWordBeginningWith:searchText];
    [self reloadTableData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
    [self reloadTableData];
    
    [searchBar resignFirstResponder];
    searchBar.text = @"";
}

// called when Search (in our case “Done”) button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self buildSectionInformation];
    UISearchBar *theSearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0,320,40)];
    theSearchBar.delegate = self;
    [self.view addSubview:theSearchBar];
    [[self tableView] setTableHeaderView:theSearchBar];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self buildSectionInformation];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

#pragma mark - Table view data source

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return sectionKeys;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [sectionKeys count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString* sectionStr = [sectionKeys objectAtIndex:section];
    if (sectionStr == @""){
        return @"** No Name **";
    }
    return [sectionKeys objectAtIndex:section];
}

- (void) configureCell:(UITableViewCell *)cell forObject:(CloudStackVMCore *)vmObject{
    // set the lable text for the cell
    [[cell textLabel] setText:[vmObject displayname]];
    // set the detail text for the cell
    [[cell detailTextLabel] setText:[vmObject instancename]];
    // set the image for the cell

    [[cell imageView] setImage:[CloudStackImageBuilder buildVMImageForInstance:vmObject]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionKey = [sectionKeys objectAtIndex:section];
    
    NSArray *vms = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_VM_CORE withCondition:[self getFinalPredicateForSection:sectionKey]];
                               
    return [vms count];
}

/**
 * returns the list of images for the view in the correctly sorted order
 */
- (NSArray *) sortedImagesForSection:(int) section {
    NSString *sectionKey = [sectionKeys objectAtIndex:section];
  
    NSArray *unsortedObjects = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_VM_CORE withCondition:[self getFinalPredicateForSection:sectionKey]];
    NSArray *sortedObjects = [unsortedObjects sortedArrayUsingSelector:@selector(compareByDisplayName:)];
  return sortedObjects;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    
    [self configureCell:cell forObject:[[self sortedImagesForSection:[indexPath section]] objectAtIndex:[indexPath row]]];
    return cell;
}

#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {    
    if (![[self hypervisorConnection] isUpdatePendingForType:HYPOBJ_CS_VM_CORE]){
        [[self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_CS_VM_CORE];
    }
    // the call to stopLoading is on the data update received.
}


#pragma mark -
#pragma mark HypervisorConnection protocol implementation
- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | HYPOBJ_CS_VM_CORE) >0){
        //[self setXenObjectList:[xenHypervisorConnection hypObjectsForType:hypObjectType withCondition:predicate]];
        [self reloadTableData];
        [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
    }
}

#pragma mark - private methods
// combines the search predicate and the root predicate
-(NSPredicate *) getFinalPredicateForSection:(NSString *) sectionKey{
    
    NSPredicate *finalPredicate = [self rootPredicate];
    NSPredicate *sectionPredicate = nil;
    
    if (sectionKey){
        if (sectionKey == @""){
            sectionPredicate = [CloudStackVMCore nameNullOrEmpty];
        }else{
            sectionPredicate = [CloudStackVMCore nameBeginWith:sectionKey];
        }
    }    

    if (sectionPredicate != nil){
        finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:sectionPredicate, finalPredicate, nil]];
    }
    
    if (searchPredicate != nil){
        finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:searchPredicate, finalPredicate, nil]];
    }
    return finalPredicate;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UITabBarController *tabBarController = [[UITabBarController alloc] init];
//    NSMutableArray *localControllersArray = [[NSMutableArray alloc] initWithCapacity:2];
//
//    EC2ImageWrapper *imageForDisplay = [[self sortedImagesForSection:[indexPath section]] objectAtIndex:[indexPath row]];
//
//    
//    // add the general tab
//    EC2ImageGeneralTableViewController *tbc = [[EC2ImageGeneralTableViewController alloc] initWithhypervisorConnection: [self hypervisorConnection]
//                                                                                                          withImage:imageForDisplay];
//    
//    
//    NSPredicate *objectPred = [EC2ImageWrapper imageWithReference:[imageForDisplay imageId]];
//    TitledDetailViewController *imageGeneral = [[TitledDetailViewController alloc] initWithHypervisorConnection:[self hypervisorConnection] 
//                                 rootObjectRef:[imageForDisplay imageId]
//                           rootObjectPredicate:objectPred
//                                 hypObjectType:HYPOPJ_EC2IMAGES_MACHINE
//                              namePropertyName:@"name"
//                       descriptionPropertyName:nil
//                                   displayMode:DISPLAYMODE_NAME
//                                       tabName:@"General" 
//                               tabBarImageName:@"tab_General" 
//                                         image:[ImageBuilder vMTemplateImage]
//                       extentionViewController:tbc];    
//    
//    [localControllersArray addObject:imageGeneral];
//    
//    
//    tabBarController.viewControllers = localControllersArray;
//    
//    [tabBarController setTitle:@"Image"];
//    
//    [CommonUIStaticHelper displayNextView:tabBarController usingNavigationController:[self navigationController]];
}

@end
