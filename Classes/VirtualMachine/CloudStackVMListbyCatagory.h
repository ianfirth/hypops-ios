//
//  CloudStackVMListbyCatagory.h
//  hypOps
//
//  Created by Ian Firth on 11/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackNativeHypervisorConnection.h"
#import "PullRefreshTableViewController.h"

#define CS_VM_ALL 0
#define CS_VM_BYOS 1
#define CS_VM_BYSTATE 2
#define CS_VM_BYHYPERVISOR 3
#define CS_VM_BYHOST 4
#define CS_VM_BYDOMAIN 5
#define CS_VM_BYSERVICEOFFERING 6
#define CS_VM_OWNEDBYME 7

@interface CloudStackVMListbyCatagory : PullRefreshTableViewController <HypervisorConnectionDelegate> {
    HypervisorType hypervisorType; // allows for the controller to be used by EC2 or CloudStackAWS
}

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withResult:(BOOL)sucsess;

@property (copy) NSString *hypConnID;
@property (nonatomic, strong) NSDictionary *csVmCatagories;

@end