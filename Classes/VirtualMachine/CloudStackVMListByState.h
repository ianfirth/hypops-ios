//
//  CloudStackVMListByState.h
//  hypOps
//
//  Created by Ian Firth on 11/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackObjectsListByProperty.h"

@interface CloudStackVMListByState : CloudStackObjectsListByProperty

@end
