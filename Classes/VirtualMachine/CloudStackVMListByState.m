//
//  CloudStackVMListByState.m
//  hypOps
//
//  Created by Ian Firth on 11/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackVMListByState.h"
#import "ImageBuilder.h"

@implementation CloudStackVMListByState
-(UIImage*) cellImageforPropertyValue:(NSString*)propertyValue{
    // build the image based on state
    if ([propertyValue isEqualToString:@"Running"]) {
            return [ImageBuilder vMImageStarted];
    }
    if ([propertyValue isEqualToString:@"Stopped"]) {
        return [ImageBuilder vMImageStopped];
    }
    return [ImageBuilder vMImage];
}


@end
