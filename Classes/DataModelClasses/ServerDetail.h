//
//  ServerDetail.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import <UIKit/UIKit.h>

@interface ServerDetail: NSManagedObject {
}
@property  NSString *address;
@property  NSString *name;
@property  NSString *password;
@property  NSString *username;
@property  NSString *type;
@property  NSDate *lastConnectionTime;
@property  int connectionCount;
@property  NSSet *has_MetaData;
@end
