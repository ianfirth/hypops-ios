//
//  ConnectionObjectUITableViewCell.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ConnectionObjectUITableViewCell.h"

#define COLOR_ALTERNATE [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0]

@implementation ConnectionObjectUITableViewCell

@synthesize detailTextLabel2,textLabel2,imageView2,alternateView;

-(void) setAlternate:(BOOL) useAlternate{
    if (useAlternate){
        [[self alternateView] setBackgroundColor:COLOR_ALTERNATE];
    }
    else{
        [[self alternateView] setBackgroundColor:[UIColor whiteColor]];
    }
}

-(void) setLoading:(BOOL) isLoading{
    if (isLoading){
        [[self imageView2] setAlpha:0.5];
        [[self countHeading]setHidden:YES];
        [[self detailTextLabel2]setHidden:YES];
        [[self loadingView] setHidden:NO];
        [[self loadingView] startAnimating];
    }
    else{
        [[self imageView2] setAlpha:1.0];
        [[self countHeading]setHidden:NO];
        [[self detailTextLabel2]setHidden:NO];
        [[self loadingView] setHidden:YES];
        [[self loadingView] stopAnimating];
    }
}

-(void) setManualRefresh:(BOOL) needsManualRefresh{
    [[self refreshButton] setHidden:!needsManualRefresh];
}
@end
