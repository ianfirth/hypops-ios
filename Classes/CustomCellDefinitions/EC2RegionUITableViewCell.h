//
//  EC2RegionUITableViewCell
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#import <UIKit/UIKit.h>
#import "HypervisorConnectionFactory.h"
#import "EC2RegionWrapper.h"

@interface EC2RegionUITableViewCell : UITableViewCell

@property (nonatomic,retain) IBOutlet  UIImageView *regionImage;
@property (nonatomic,retain) IBOutlet  UILabel *regionNameLabel;
@property (nonatomic,retain) IBOutlet  UILabel *instanceCountLabel;
@property (nonatomic,retain) IBOutlet  UILabel *runningInstanceCountLabel;
@property (nonatomic,retain) IBOutlet  UIView *alternateView;
@property IBOutlet UIActivityIndicatorView* instanceSpinner;
@property IBOutlet UIActivityIndicatorView* runningInstanceSpinner;

-(void) setInstanceCountsUnknown;
-(void) setInstanceCountsLoading;
-(void) setRegion:(EC2RegionWrapper*)region;
-(void) setInstanceCount:(NSInteger)instanceCout andRunning:(NSInteger)runningCount;
-(void) setAlternate:(BOOL) useAlternate;

@end
