//
//  MultiLineDetailUITableViewCell.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "MultiLineDetailUITableViewCell.h"

#define COLOR_ALTERNATE [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0]

@implementation MultiLineDetailUITableViewCell

@synthesize detailTextLabel2,textLabel2,imageView2,alternateView;

-(void) setAlternate:(BOOL) useAlternate{
    if (useAlternate){
        [[self alternateView] setBackgroundColor:COLOR_ALTERNATE];
    }
    else{
        [[self alternateView] setBackgroundColor:[UIColor whiteColor]];
    }
}

@end
