//
//  EC2RegionUITableViewCell.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2RegionUITableViewCell.h"
#import "ImageBuilder.h"
#import "EC2RegionWrapper.h"

#define COLOR_ALTERNATE [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0]

@implementation EC2RegionUITableViewCell

@synthesize regionImage, regionNameLabel,runningInstanceCountLabel, instanceCountLabel;

-(void) setRegion:(EC2RegionWrapper*)region{
    [[self regionNameLabel] setText:[region regionName]];
    // get the region image here too
    if ([[region regionName] hasPrefix:@"us-"])
    {
        [[self regionImage] setImage:[ImageBuilder flagUSAImage]];
    }
    else if ([[region regionName] hasPrefix:@"eu-"])
    {
        [[self regionImage] setImage:[ImageBuilder flagEUImage]];
    }
    else if ([[region regionName] hasPrefix:@"sa-"])
    {
        [[self regionImage] setImage:[ImageBuilder flagBrazilImage]];
    }
    else if ([[region regionName] hasPrefix:@"ap-southeast-1"])
    {
        [[self regionImage] setImage:[ImageBuilder flagSingaporeImage]];
    }
    else if ([[region regionName] hasPrefix:@"ap-southeast-2"])
    {
        [[self regionImage] setImage:[ImageBuilder flagAustraliaImage]];
    }
    else if ([[region regionName] hasPrefix:@"ap-southeast"])  // catch any others for this region
    {
        [[self regionImage] setImage:[ImageBuilder flagAustraliaImage]];
    }
    else if ([[region regionName] hasPrefix:@"ap-northeast"])
    {
        [[self regionImage] setImage:[ImageBuilder flagJapanImage]];
    }
    else
    {
        [[self regionImage] setImage:nil];
    }
}


-(void)setInstanceCount:(NSInteger)instanceCount andRunning:(NSInteger)runningCount{
    [[self instanceCountLabel] setText:[NSString stringWithFormat:@"%ld",(long)instanceCount]];
    [[self instanceSpinner] stopAnimating];
    [[self instanceCountLabel] setHidden:NO];

    [[self runningInstanceCountLabel] setText:[NSString stringWithFormat:@"%ld",(long)runningCount]];
    [[self runningInstanceSpinner] stopAnimating];
    [[self runningInstanceCountLabel] setHidden:NO];

}

-(void) setInstanceCountsUnknown{
    // can these be an alert spinner panel saying loading or something better
    [[self instanceCountLabel] setText:@"?"];
    [[self instanceSpinner] stopAnimating];
    [[self instanceCountLabel] setHidden:NO];

    [[self runningInstanceCountLabel] setText:@"?"];
    [[self runningInstanceSpinner] stopAnimating];
    [[self runningInstanceCountLabel] setHidden:NO];
}

-(void) setInstanceCountsLoading{
    // can these be an alert spinner panel saying loading or something better
    [[self instanceCountLabel] setText:@"?"];
    [[self instanceSpinner] setHidesWhenStopped:YES];
    [[self instanceSpinner] setHidden:NO];
    [[self instanceCountLabel] setHidden:YES];
    [[self instanceSpinner] startAnimating];

    [[self runningInstanceCountLabel] setText:@"?"];
    [[self runningInstanceSpinner] setHidesWhenStopped:YES];
    [[self runningInstanceSpinner] setHidden:NO];
    [[self runningInstanceCountLabel] setHidden:YES];
    [[self runningInstanceSpinner] startAnimating];

}

-(void) setAlternate:(BOOL) useAlternate{
    if (useAlternate){
        [[self alternateView] setBackgroundColor:COLOR_ALTERNATE];
    }
    else{
        [[self alternateView] setBackgroundColor:[UIColor whiteColor]];
    }
}
@end
