//
//  RRDPlotViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "DataSource.h"
#import "ProgressAlert.h"
#import "CorePlot-CocoaTouch.h"
#import "RRDDisplayButtonDelegate.h"

@class CPTGraph;
@class CPTTheme;

@interface RRDPlotViewController : UIViewController < CPTPlotSpaceDelegate,
                                          CPTPlotDataSource,
                                          CPTScatterPlotDelegate>
{  
    ProgressAlert *progressAlert;
    
    NSString* sourceReference;
    
    CPTGraphHostingView  *defaultLayerHostingView;
    UISegmentedControl* rrdTimeScale;
    
    CPTGraph*           graph;

    CPTLayerAnnotation   *symbolTextAnnotation;
    
    // this is the data definition for the graph
    NSMutableArray* plotDataSourceNames;
    
    // plot colorList
    NSArray* plotcolorList;
    
    // marker colorlist
    NSArray* MarkerColorList;
    
    // consolidation level
    NSInteger  consolidationStepCount;
    
    NSMutableArray* dataSourceLinePlots;
    
    // the current plotrange for the selected consolidation range
    NSDate* firstkey;
    NSDate* lastkey;
    double plotlength;
        
    // formatter for the pop over text on point selection
    NSDateFormatter *dateFormatterOverlay;
        
    // xaxis dateFormatter
    NSDateFormatter *dateFormatter;
}

@property (nonatomic, strong) CPTGraphHostingView *defaultLayerHostingView;
@property (readonly) id<RRDDisplayButtonDelegate> rrdDisplayButtonDelegate;
@property (strong) ProgressAlert *progressAlert;

- (id)initWithRRDDsiplayButtonDelegate:(id <RRDDisplayButtonDelegate>)delegate withReference:(NSString*)sourcereference;
@end
