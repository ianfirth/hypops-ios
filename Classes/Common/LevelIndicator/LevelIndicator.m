//
//  LevelIndicator.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "LevelIndicator.h"

@implementation LevelIndicator
@synthesize minumumValue, maximumValue;
@synthesize warningLevel, criticalLevel;
@synthesize okColor,warningColor,criticalColor;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setOkColor:[UIColor greenColor]];
        [self setWarningColor:[UIColor yellowColor]];
        [self setCriticalColor:[UIColor redColor]];
        [self setWarningLevel:60];
        [self setCriticalLevel:80];
        [self setMaximumValue:100];
        [self setMinumumValue:0];
    }
    return self;
}

-(void) setCurrentValue:(double)currentValue{
    _currentValue = currentValue;
    [self setNeedsDisplay];
}

-(double)currentValue{
    return _currentValue;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{

    UIColor* drawColor = [self okColor];
    // select the color
    if ([self currentValue] > [self warningLevel]){
        drawColor = [self warningColor];
    }
    if ([self currentValue] > [self criticalLevel]){
        drawColor = [self criticalColor];
    }

    UIColor* backColor = [UIColor colorWithRed:.7 green:0.7 blue:0.7 alpha:1.0];

    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    
    // draw background
    [self drawGradientRectinContext:currentContext withColor:backColor inRect:rect];

    // draw color filled area
    float w = ([self currentValue]- [self minumumValue])/([self maximumValue]-[self minumumValue]);
    
    CGRect finalRect = rect;
    finalRect.origin.x +=2;
    finalRect.size.width -=4;
    finalRect.size.width = finalRect.size.width * w;
    finalRect.size.height -=4;
    finalRect.origin.y += 2;
    
    [self drawGradientRectinContext:currentContext withColor:drawColor inRect:finalRect];    
    [super drawRect:rect];
}

-(void) drawGradientRectinContext:(CGContextRef)currentContext withColor:(UIColor*)color inRect:(CGRect)rect
{
    CGFloat start_red = 1;
    CGFloat start_green = 1;
    CGFloat start_blue = 1;
    CGFloat start_alpha = 1;
    
    CGFloat end_red = 1;
    CGFloat end_green = 1;
    CGFloat end_blue = 1;
    CGFloat end_alpha = 1;
    
    CGColorRef startColorref = [color CGColor];NSUInteger numComponents = CGColorGetNumberOfComponents(startColorref);
    
    if (numComponents == 4) {
        const CGFloat *components = CGColorGetComponents(startColorref);
        start_red     = components[0] * 0.6;
        start_green = components[1] * 0.6;
        start_blue   = components[2] * 0.6;
        start_alpha = components[3] * 0.6;
    }
    
    // change this to be a slightly different color (darker version)
    CGColorRef endColorref = [color CGColor];
    numComponents = CGColorGetNumberOfComponents(endColorref);
    
    if (numComponents == 4) {
        const CGFloat *components = CGColorGetComponents(endColorref);
        end_red     = components[0];
        end_green = components[1];
        end_blue   = components[2];
        end_alpha = components[3];
    }
    
    CGGradientRef glossGradient;
    CGColorSpaceRef rgbColorspace;
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    
    CGFloat components[8] = { start_red, start_green, start_blue, start_alpha,  // Start color
        end_red, end_green, end_blue, end_alpha }; // End color
    
    rgbColorspace = CGColorSpaceCreateDeviceRGB();
    glossGradient = CGGradientCreateWithColorComponents(rgbColorspace, components, locations, num_locations);
    
    // set the clipping path
    CGContextClipToRect(currentContext,rect);    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));    
    CGContextDrawLinearGradient(currentContext, glossGradient, startPoint, endPoint, 0);
    
    CGGradientRelease(glossGradient);
    CGColorSpaceRelease(rgbColorspace); 
}
@end
