//
//  TitledDetailViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "HypervisorConnectionFactory.h"
#import "UIWaitForDataLoad.h"
#import "RefreshablePage.h"
#import "Identifyable.h"

/**
 Enum to represent the different header modes for the controller 
 */
typedef enum{
    DISPLAYMODE_NOHEADER,
    DISPLAYMODE_NAME,
    DISPLAYMODE_NAMEDESCRIPTION, 
} GeneralDisplayMode;

@interface TitledDetailViewController : UIViewController <HypervisorConnectionDelegate, UIWaitForDataLoadDelegate> {
    UIImage* _image;
    GeneralDisplayMode mode;
    NSString *namePropertyName;
    NSString *descriptionPropertyName;NSUInteger  rootHypType;
    HypervisorType hypervisorType;
    NSPredicate* _rootObjectPredicate;
    NSString* _rootObjectRef;
    SEL _titlePressesSelector;
    __weak id _selctorObject;
}

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypConnection
                     rootObjectRef:(NSString *)rootObjectIdentifier
               rootObjectPredicate:(NSPredicate *) theRootObjectPredicate
                     hypObjectType:(NSInteger)theHypObjectType
                  namePropertyName:(NSString *)theNameProperty
           descriptionPropertyName:(NSString *)theDescriptionPropertyName
                       displayMode:(GeneralDisplayMode)theMode
                           tabName:(NSString *)tabName
                   tabBarImageName:(NSString *)imageName 
                             image:(UIImage *)mainImage 
           extentionViewController:(UIViewController<Identifyable,RefreshablePage> *)theExtentionViewController
              titlePressedSelector:(SEL)titleSelector
selectorOnObject:(id) selctorObject;

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypConnection
                     rootObjectRef:(NSString *)rootObjectIdentifier
               rootObjectPredicate:(NSPredicate *) theRootObjectPredicate
                     hypObjectType:(NSInteger)theHypObjectType
                  namePropertyName:(NSString *)theNameProperty
           descriptionPropertyName:(NSString *)theDescriptionPropertyName
                       displayMode:(GeneralDisplayMode)theMode
                           tabName:(NSString *)tabName
                   tabBarImageName:(NSString *)imageName
                             image:(UIImage *)mainImage
           extentionViewController:(UIViewController<Identifyable,RefreshablePage> *)theExtentionViewController;

-(void)setRootObjectPredicate:(NSPredicate *)rootObjectPredicate andReference:(NSString *)rootObjectRef;

+(void) setRightBarButtonsForNavigationController:(UINavigationController *)navigationController andButtons:(NSArray *)barButtonItems;

- (void) updatePage;

-(void) setTakeActionOnAppear:(NSNumber*)takeAction;

@property (readonly) NSString *rootObjectRef;
@property (copy) NSString *hypConnID;
@property (strong) UILabel *objectDescriptionLabel;
@property (strong) UILabel *objectNameLabel;
@property (strong) UIView *shadowView;
@property (strong) UIImageView* objectImageView;
@property (strong) UIViewController<RefreshablePage,Identifyable> *extentionViewController;
@property (readonly) NSPredicate* rootObjectPredicate;
@property UIImage* image;

@end

@interface TitledDetailViewController (AbstractMethods)
 - (void)hypervisorObjectsUpdatedOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;
@end
