//
//  AlertWithDataStore.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "AlertWithDataStore.h"


@implementation AlertWithDataStore

/*
 * Initialize view with maximum of two buttons
 */
- (id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate
   cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... {
    self = [super initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelButtonTitle
              otherButtonTitles:otherButtonTitles, nil];
    if (self) {
        NSString *reqSysVer = @"5.0";
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        isPreiOS5 = !([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending);
        if (isPreiOS5){
            // Create and add UITextField to UIAlertView
            UITextField *myTextField = [[UITextField alloc] initWithFrame:CGRectZero];
            myTextField.tag = -101;
            myTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            myTextField.alpha = 0.75;
            myTextField.borderStyle = UITextBorderStyleRoundedRect;
            myTextField.delegate = delegate;
            textField = myTextField;
            // insert UITextField before first button
            BOOL inserted = NO;
            for( UIView *view in self.subviews ){
                if(!inserted && ![view isKindOfClass:[UILabel class]])
                    [self insertSubview:myTextField aboveSubview:view];
            }
            
            // ensure that layout for views is done once
            layoutDone = NO;
        }
        else{
            [self setAlertViewStyle:UIAlertViewStylePlainTextInput];
        }
    }
    return self;
}

-(NSString*) textString{
    if (isPreiOS5){
        for (UIView* subview in [self subviews]){
            if ([subview tag] == -101){
                UITextField *field = (UITextField *)subview;
                return [field text];
            }
        }
    }
    else{
        return [[self textFieldAtIndex:0] text];
    }
    return nil;    
}

-(UITextField*) textField{
    if (isPreiOS5){
        for (UIView* subview in [self subviews]){
            if ([subview tag] == -101){
                UITextField *field = (UITextField *)subview;
                return field;
            }
        }
    }
    else{
        return [self textFieldAtIndex:0];
    }
    return nil;    
}
/*
 * Show alert view and make keyboard visible
 */
- (void) show {
    [super show];
    if (isPreiOS5){
         [[self textField] becomeFirstResponder];
    }
}

/*
 * Determine maximum y-coordinate of UILabel objects. This method assumes that only
 * following objects are contained in subview list:
 * - UILabel
 * - UITextField
 * - UIThreePartButton (Private Class)
 */
- (CGFloat) maxLabelYCoordinate {
    // Determine maximum y-coordinate of labels
    CGFloat maxY = 0;
    for( UIView *view in self.subviews ){
        if([view isKindOfClass:[UILabel class]]) {
            CGRect viewFrame = [view frame];
            CGFloat lowerY = viewFrame.origin.y + viewFrame.size.height;
            if(lowerY > maxY)
                maxY = lowerY;
        }
    }
    return maxY;
}

/*
 * Override layoutSubviews to correctly handle the UITextField
 */
- (void)layoutSubviews {
    [super layoutSubviews];
    if (isPreiOS5){
        CGRect frame = [self frame];   
        CGRect bounds = [self bounds];
        CGFloat alertWidth = bounds.size.width;   //was frame.size.width;
        
        // Perform layout of subviews just once
        if(!layoutDone) {
            CGFloat labelMaxY = [self maxLabelYCoordinate];
            
            // Insert UITextField below labels and move other fields down accordingly
            for(UIView *view in self.subviews){
                if([view isMemberOfClass:[UITextField class]]){
                    CGRect viewFrame = CGRectMake(
                                                  kUITextFieldXPadding, 
                                                  labelMaxY + kUITextFieldYPadding, 
                                                  alertWidth - 2.0*kUITextFieldXPadding, 
                                                  kUITextFieldHeight);
                    [view setFrame:viewFrame];
                } else if(![view isMemberOfClass:[UILabel class]] && ![view isMemberOfClass:[UIImageView class]]) {
                    CGRect viewFrame = [view frame];
                    viewFrame.origin.y += kUITextFieldHeight;
                    [view setFrame:viewFrame];
                }
            }
            
            // size UIAlertView frame by height of UITextField
            frame.size.height += kUITextFieldHeight + 2.0;
            [self setFrame:frame];
            layoutDone = YES;
        } else {
            // reduce the x placement and width of the UITextField based on UIAlertView width
            for(UIView *view in self.subviews){
                if([view isMemberOfClass:[UITextField class]]){
                    CGRect viewFrame = [view frame];
                    viewFrame.origin.x = kUITextFieldXPadding;
                    viewFrame.size.width = alertWidth - (2.0*kUITextFieldXPadding);
                    [view setFrame:viewFrame];
                }
            }
        }
    }
}


- (void) addMetaDataWithKey:(NSString *)key andValue:(NSString *)value{
    if (!metaData){
        metaData = [[NSMutableDictionary alloc] initWithCapacity:1];
    }
    [metaData setValue:value forKey:key];
}

- (NSString *) metaDataForKey:(NSString *)key{
    return [metaData valueForKey:key];
}

- (void) dealloc{
    textField = nil;
}

@end
