//
//  RootViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "HypervisorConnectionFactory.h"
#import "HypervisorConnectionDefinition.h"

@interface RootViewController : UITableViewController <HypervisorConnectionDelegate, 
                                                        UITextFieldDelegate,
                                                        NSFetchedResultsControllerDelegate, 
                                                        UIActionSheetDelegate,
                                                        NSURLConnectionDelegate>

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) UIAlertView *alertView;
@property (nonatomic, strong) HypervisorConnectionFactory *connectedTo;
@property (nonatomic, strong) HypervisorConnectionFactory *connectingTo;
@property (nonatomic, strong) UIActionSheet *theActionSheet;

- (void)connectToAddress:(NSString *)address 
            withUsername:(NSString *)username 
             andPassword:(NSString *)password 
              forHypType:(HypervisorType)hypType;

- (void) connectToConnectionDefinition:(HypervisorConnectionDefinition* )hypConDef;

-(void) makeConnectionWithAddress:(NSString*)address Username:(NSString*)username password:(NSString*)password hypType:(NSUInteger)hypType;

- (void)makeConnectionWithIndexPath:(NSIndexPath *)indexPath;

- (void)displayHypervisorContentViewForHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnectionFactory;

- (void)displayConnectingDialogForConnection:(HypervisorConnectionFactory *)connection;

- (void)displayConnectingErrorDialogWithMessage:(NSString *)message;

- (void)clearDialogWithAnimation:(BOOL)animated;

- (void)insertNewObject;

+(NSDictionary*) processStartupURL;

/** 
 * called when a connection is deleted
 */
-(void) connectionDeleted;

-(void) refreshData;
@end


#pragma mark -

@interface RootViewController (AbstractMethods)
// Define Abstract methods which should be implemented by subclasses:
- (void)insertNewObjectForHypervisorType:(HypervisorType) type;

// ios 7 use this one
-(void) displayHypervisorTypeSelectionActionSheet:(UIActionSheet *) actionSheet;
// ios8 and later use this one
-(void) displayHypervisorTypeSelectionAlertController:(UIAlertController *)alertController;

@end
