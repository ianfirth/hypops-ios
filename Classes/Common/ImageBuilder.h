//
//  ImageBuilder.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVM.h"

/**
 Class to provide a static set of functions to help create icons from images by overlaying them
 appropriatly.  These images are not inteded to be hypervisor specific, but instead a collection
 of all possible icons for all VM types.  This is not split into specific hypervisor types since 
 it is beleived that this is a small set of common images for all platforms.
 */
@interface ImageBuilder : NSObject {}

/**
 builds an Image with the new sash overlayed on it.
 */
+ (UIImage *)AddNewSashtoImage:(UIImage*)baseImage;

/**
 builds a delete Image.
 This consists of an icon background and the padlock image overlay.
 */
+ (UIImage *)deleteImage;

/**
 builds a snapshot Image.
 This consists of an icon background and the padlock image overlay.
 */
+ (UIImage *)snapshotImage;

/**
 builds a share Image.
 This consists of an icon background and the share image overlay.
 */
+ (UIImage *)shareImage;

/**
 builds a snapshot revert Image.
 This consists of an icon background and the padlock image overlay.
 */
+ (UIImage *)snapshotRevertImage;
/**
 builds a security Image.
 This consists of an icon background and the padlock image overlay.
 */
+ (UIImage *)securityImage;

/**
 builds a graph Image.
 This consists of an icon background and the graph image overlay.
 */
+ (UIImage *)graphImage;

/**
 builds a vm Image.
 @param color the background color for the image
 This consists of an icon background and the VM image overlay.
 */
+ (UIImage *)vMImage;

/**
 builds a vm destroyed Image.
 This consists of an icon background and the VM image overlay with the destroyed overlay on top of that.
 */
+ (UIImage *)vMImageDestroyed;


/**
 builds a vm started Image.
 This consists of an icon background and the VM image overlay with the started overlay on top of that.
 */
+ (UIImage *)vMImageStarted;

/**
 builds a vm started Image.
 This consists of an icon background and the VM image overlay with the started overlay on top of that.
 */

+ (UIImage *)vMImageAlert;

/**
 builds a vm stopped Image.
 This consists of an icon background and the VM image overlay with the stopped overlay on top of that.
 */
+ (UIImage *)vMImageStopped;

/**
 builds a vm terminated Image.
 This consists of an icon background and the VM image overlay with the terminated overlay on top of that.
 */
+ (UIImage *)vMImageTerminated;

/**
 builds a vm paused Image.
 This consists of an icon background and the VM image overlay with the paused overlay on top of that.
 */
+ (UIImage *)vMImagePaused;

/**
 builds a vm suspended Image.
 This consists of an icon background and the VM image overlay with the suspended overlay on top of that.
 */
+ (UIImage *)vMImageSuspended;

/**
 builds a vm tagged Image.
 This consists of an icon background and the VM alert image overlay.
 */
+ (UIImage *)vMTaggedImage;

/**
 builds a vm template Image.
 This consists of an icon background and the VM template image overlay.
 */
+ (UIImage *)vMTemplateImage;

/**
 builds a host Image.
 This consists of an icon background and the host image overlay.
 */
+ (UIImage *)hostImage;

/**
 builds a network Image.
 This consists of an icon background and the network image overlay.
 */
+ (UIImage *)networkImage;

/**
 builds a zone Image.
 This consists of an icon background and the zone image overlay.
 */
+ (UIImage *)zoneImage;

/**
 builds a storage Image.
 This consists of an icon background and the storage image overlay.
 */
+ (UIImage *)storageImage;

/**
 builds a flag Image of USA.
 This consists of an icon background and the USA Flag image overlay.
 */
+ (UIImage *)flagUSAImage;

/**
 builds a flag Image of Australia.
 This consists of an icon background and the Australia Flag image overlay.
 */
+ (UIImage *)flagAustraliaImage;

/**
 builds a flag Image of EU.
 This consists of an icon background and the EU Flag image overlay.
 */
+ (UIImage *)flagEUImage;

/**
 builds a flag Image of Brazil.
 This consists of an icon background and the Brazil Flag image overlay.
 */
+ (UIImage *)flagBrazilImage;

/**
 builds a flag Image of Japan.
 This consists of an icon background and the Japan Flag image overlay.
 */
+ (UIImage *)flagJapanImage;

/**
 builds a flag Image of Singapore.
 This consists of an icon background and the Singapore Flag image overlay.
 */
+ (UIImage *)flagSingaporeImage;

/**
 builds a Host up Image.
 This consists of an icon background and the Host image overlay with the up overlay on top of that.
 */
+ (UIImage *)hostImageUp;

/**
 builds a Host sown Image.
 This consists of an icon background and the Host image overlay with the down overlay on top of that.
 */
+ (UIImage *)hostImageDown;

/**
 builds a Host sown Image.
 This consists of an icon background and the Host image overlay with the alert overlay on top of that.
 */
+ (UIImage *)hostImageAlert;

@end
