//
//  GradientView.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ShadowView.h"
#import <QuartzCore/QuartzCore.h>


@implementation ShadowView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setStartColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8]];
        [self setEndColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0]];
        [self setGradientViewFillMode:GradientViewFillModeFull];
        [self setUserInteractionEnabled:NO];
    }
    return self;
}

@end
