//
//  vncViewController.m
//  VNC Client
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "vncViewController.h"
#import "VncController.h"

@implementation vncViewController

@synthesize vncController;
@synthesize useLowQuality;
@synthesize vncView = _vncView;

#pragma mark ViewContoller
-(id) init{
    self = [super init];
    if (self){
    }
    return self;
}

// set up the view
- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _vncView = [[VncView alloc] initWithFrame:[[self view] bounds] andRemoteScreenSize:CGSizeMake(0,0)];
    [[self view] addSubview:[self vncView]];
    vncController = [[VncController alloc] initWithRendeder:[self vncView] andUsingLowQuality:[self useLowQuality]];
    [vncController setNeedsFullScreenUpdate];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin ];
    [[self vncView] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin  ];
    [[self view] setAutoresizesSubviews:YES];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    // clear off any subviews.
    _vncView = nil;
    for (UIView* view in [[self view] subviews]) {
        [view removeFromSuperview];
    }

    [vncController close];
    vncController = nil;
}

// this can be used by callers to inject their own streams that have been created as
// part of an alternate connection intiailization
-(void)assignDelegateToStreams:(NSInputStream*) theInputStream and:(NSOutputStream*)theOutputStream withRFBVersion:(NSString*)rfbVersion{
    // this is called as assuming that we are at the begining of the stream
    // set the currentPacketType to the handshake to start the process
    [vncController assignDelegateToStreams:theInputStream and:theOutputStream withRFBVersion:rfbVersion];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
