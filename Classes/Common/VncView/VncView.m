//
//  vVcView.m
//  VNC Client
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "vncView.h"

#define localScreenBytesPerPixel 4
@interface ScreenBudderWrapper : NSObject

@property uint8_t* screenData;
@property long long screenDataSize;
@property long long screenRowByteCount;
@property CGSize remoteFrameSize;
@end

@implementation ScreenBudderWrapper

@synthesize screenData,screenDataSize,remoteFrameSize,screenRowByteCount;

@end

@interface VncView (){
    float xScale;
    float yScale;NSUInteger remoteScreenWidth;NSUInteger remoteScreenHeight;

    uint8_t* _screenData;
    long long  _screenDataSize;
    long long _screenRowByteCount;NSUInteger bitsPerComponent;NSUInteger localBitsPerPixel;

}

-(void)WriteText:(NSString*)text;
-(CGRect) getScaledRectFor:(CGRect)original;
-(void) updateScalingFactors;
@end

@implementation VncView

@synthesize vncConnectionState, failedString, remoteFrameSize;

// create the view with the remove screen size
- (id)initWithFrame:(CGRect)frame andRemoteScreenSize:(CGSize)size{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        remoteFrameSize = size;
        [self setVncConnectionState:VNC_CONNECTION_NOCONNECTIONAVAILABLE];

        bitsPerComponent = 8;
        localBitsPerPixel = 4 * bitsPerComponent;  // assume a byte each (rgba) and 8 bits each
        _screenData = nil;
        _screenDataSize = 0;
    }
    return self;

}


-(void) setScreenBufferFrowWrapper:(ScreenBudderWrapper*)wrapper{
    if (![[NSThread currentThread] isMainThread]) {
        [self performSelector:_cmd onThread:[NSThread mainThread] withObject:wrapper waitUntilDone:YES];
        NSLog(@"ERROR ------- This is not on the main thread !!!");
        return;
    }
    _screenData = [wrapper screenData];
    _screenDataSize = [wrapper screenDataSize];
    remoteFrameSize = [wrapper remoteFrameSize];
    _screenRowByteCount = [wrapper screenRowByteCount];
    [self updateScalingFactors];
}

-(void) setScreenBuffer:(uint8_t*)screenBuffer withDataSize:(long long)screenDataSize bytesPerRow:(NSInteger)
    screenBytesPerRow remoteScreenSize:(CGSize) size{
    ScreenBudderWrapper* wrapper = [[ScreenBudderWrapper alloc] init];
    [wrapper setScreenData:screenBuffer];
    [wrapper setScreenDataSize:screenDataSize];
    [wrapper setRemoteFrameSize:size];
    [wrapper setScreenRowByteCount:screenBytesPerRow];

    if (![[NSThread currentThread] isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            //Your code goes in here
                [self setScreenBufferFrowWrapper:wrapper];
        });
        //[self performSelector:_cmd onThread:[NSThread mainThread] withObject:wrapper waitUntilDone:YES];
        return;
    }

    [self setScreenBufferFrowWrapper:wrapper];
}

- (float) xScale{
    return xScale;
}

-(float) yScale{
    return yScale;
}

-(void)WriteText:(NSString*)text{
    if (![[NSThread currentThread] isMainThread]) {
        NSLog(@"ERROR ------- this is not on the main thread !!!");
        return;
    }
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGColorRef blackfillColor = [[UIColor blackColor] CGColor];
    CGContextSetFillColor(context, CGColorGetComponents(blackfillColor));
    CGContextFillRect(context, [self bounds]);
    
    CGContextSetTextDrawingMode(context, kCGTextStroke);
    CGContextSetRGBFillColor (context, 0, 1, 0, .5); // 6
    CGContextSetRGBStrokeColor (context, 0, 0, 1, 1);
    CGAffineTransform transform = CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0);
    CGContextSetTextMatrix(context, transform);
    
    CGSize	textSize = { 2000.0f, 20000.0f };		// width and height of text area
    UIFont* font = [UIFont fontWithName:@"Helvetica" size:20];
    
    CGRect textRendersize = [text boundingRectWithSize:textSize
                                               options:NSStringDrawingTruncatesLastVisibleLine
                                            attributes:@{NSFontAttributeName:font}
                                               context:nil];
    
    float startLocation = (([self bounds].size.width) - textRendersize.size.width)/2;

    CGContextSetTextDrawingMode(context, kCGTextFill); // This is the default
    [[UIColor blackColor] setFill];
    
    [text drawAtPoint:CGPointMake(startLocation, 200)
                    withAttributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:[UIColor grayColor]}];
    
}

/**
 *  This expects thht the rect is the location in the remote screen that needs to be drawn to the local screen
 */
-(void) drawScreenFromBufferWithRect:(CGRect) rect{
    if (![[NSThread currentThread] isMainThread]) {
        NSLog(@"ERROR ------- his is not on the main thead !!!");
        return;
    }
    
    // don't draw anything witha  zero size its ppintless
    if (rect.size.width == 0 || rect.size.height == 0){
        return;
    }

    int8_t* rectBuffer;
    if (_screenData){
        @try{
            // make sure that there are no overflows
            rect = CGRectIntersection(rect, CGRectMake(0,0,remoteScreenWidth, remoteScreenHeight) );
            
            long long rectRowLength = rect.size.width * 4;
            long long rectBufferSize = rectRowLength * rect.size.height;
            rectBuffer = calloc(rectBufferSize, sizeof(int8_t));
            
            long long xOffset = rect.origin.x * 4;
            long long yOffset = (rect.origin.y ) *  _screenRowByteCount;
            long long startLocation =  xOffset + yOffset;

            long long totalBytes = rectRowLength * rect.size.height;
            
            for (long long row = 0; row < rect.size.height; row++) {
                //NSLog(@"copying from %lld for %lld bytes", startLocation,copyLength);
                long long currrentRowByte = (row * rectRowLength);
                if ( (currrentRowByte + rectRowLength) <= totalBytes){
                    memcpy( rectBuffer + currrentRowByte, _screenData + startLocation + (row * _screenRowByteCount) , rectRowLength );
                }
                else{
                    NSLog(@"error does not fit");
                }
            }
                        
            CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
            
            CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, rectBuffer, rectBufferSize, NULL);
            
            CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
            CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
            
            CGImageRef imageRef = CGImageCreate(rect.size.width, rect.size.height, bitsPerComponent, localBitsPerPixel, rectRowLength, rgbColorSpace, bitmapInfo, dataProvider, NULL, NO, renderingIntent);
            
            UIImage* image= [[UIImage alloc] initWithCGImage:imageRef];
            
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextSetShouldAntialias(context, NO);
            
            [image drawInRect:[self getScaledRectFor:rect]];
            
            CGColorSpaceRelease(rgbColorSpace);
            CGDataProviderRelease(dataProvider);
            CGImageRelease(imageRef);
        }
        @finally {
            free(rectBuffer);
            rectBuffer = nil;
        }
    }
}

/**
 *  Causes the local view to be invalidated in the correct region based on the remote update area
 */
-(void) setNeedsDisplayInRemoteRect:(NSValue*)rectValue{
    if (![[NSThread currentThread] isMainThread]) {
        [self performSelector:_cmd onThread:[NSThread mainThread] withObject:rectValue waitUntilDone:NO];
        return;
    }
    
    CGRect rect = [rectValue CGRectValue];
    [self setNeedsDisplayInRect:[self getScaledRectFor:rect]];
}

/**
 *  This takes the memory screen representation and updates the required part of the screen
 */
- (void)drawRect:(CGRect)rect
{
    if ([self vncConnectionState] == VNC_CONNECTION_NOCONNECTIONAVAILABLE){
        [self WriteText:@"No console available"];
    }
    else{
       CGRect newRect = [self getRemoteScreenRectFromLocalScreenRect:rect];
       [self drawScreenFromBufferWithRect:newRect];
    }
}

#pragma mark public methods

-(CGSize) remoteScreenSize{
    return remoteFrameSize;
}

-(void) updateScalingFactors{NSUInteger screenWidth = [self bounds].size.width;NSUInteger screenHeight = [self bounds].size.height;
    
    remoteScreenWidth = remoteFrameSize.width;
    remoteScreenHeight = remoteFrameSize.height;
    
    xScale = (float)screenWidth / remoteScreenWidth;
    yScale = (float)screenHeight / remoteScreenHeight;
    
    [self setNeedsDisplay];
}

// ensure that setNeedsDisplay is called on the correct thread
-(void) updateDisplay{
    if (![[NSThread currentThread] isMainThread]) {
        [self performSelector:_cmd onThread:[NSThread mainThread] withObject:nil waitUntilDone:NO];
        return;
    }
    [self setNeedsDisplay];
}

-(void) setNeedsDisplay{
    if (![[NSThread currentThread] isMainThread]) {
        [self performSelector:_cmd onThread:[NSThread mainThread] withObject:nil waitUntilDone:NO];
        return;
    }
    [super setNeedsDisplay];
}

/**
 * Convert a remote screen rect to the coordinates in the local screen area
 */
-(CGRect) getScaledRectFor:(CGRect)original{
    return CGRectMake(original.origin.x * xScale , original.origin.y *yScale, original.size.width * xScale, original.size.height * yScale);
}

/**
 * Convert a local screen rect for redraw to the coordinates in the remote screen buffer
 */
-(CGRect) getRemoteScreenRectFromLocalScreenRect:(CGRect)original{
    if (xScale == 0 || yScale == 0){
        return CGRectMake(0,0,0,0);
    }
    
    // this needs to be integer values always
    CGRect result  = CGRectMake( (NSInteger)(original.origin.x / xScale)-1 , (NSInteger) (original.origin.y /yScale)-1, (NSInteger)(original.size.width / xScale)+1, (NSInteger)(original.size.height / yScale) +1);
    
    CGRectIntegral(result);
    return CGRectIntersection(result, CGRectMake(0,0,remoteScreenWidth, remoteScreenHeight) );
}

@end
