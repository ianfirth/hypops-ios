//
//  VncView.h
//  VNC Client
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import <UIKit/UIKit.h>
#import "VncController.h"

@interface VncView : UIView<VNCRenderer>{
    // size of the remote screen to be rendered
    CGSize remoteFrameSize;
    CGSize alreadyCreatedFrameSize;
    
    // number of bytes in each screen rowNSUInteger  bytesPerRow;
}

- (id)initWithFrame:(CGRect)frame andRemoteScreenSize:(CGSize)size;

-(void) setScreenBuffer:(uint8_t*)screenBuffer withDataSize:(long long)screenDataSize bytesPerRow:(NSInteger)screenBytesPerRow remoteScreenSize:(CGSize) size;

// this can be called from another thead it will move the the main thread as required
-(void) setNeedsDisplayInRemoteRect:(NSValue*)rectValue;
-(void) updateScalingFactors;

@property VncConnectionState vncConnectionState;
@property NSString* failedString;
@property (readonly) CGSize remoteFrameSize;

@end
