//
//  UIWaitForDataLoad.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//  This class provides the ability to load a hypervisor object and all the objects that it requires in its tree
//  into the cache.  This is integrated into the UI layer via this object to provide appropriate on screen loading
//  notifications.
//  The class usses the hypervisorConnection interface that it is constructed with to perform all the data loading 
//  and cache checking.

#import "HypervisorConnectionFactory.h"

// declare forward reference to the class used in the protocol
@class UIWaitForDataLoad;

@protocol UIWaitForDataLoadDelegate
- (void)dataLoaderFinished:(UIWaitForDataLoad *)dataLoader withResult:(BOOL)sucsess;
@end

@interface UIWaitForDataLoad : NSObject <HypervisorConnectionDelegate> {
    UIAlertView *alertView;
    HypervisorConnectionFactory *hypConnection;

    // they type of object that was requrestedNSUInteger  requestType;
    
    // the reference of the root object that has been requested.
    NSString *requestedObjectReference;
    
    // The delegate to send the events to.
    id <UIWaitForDataLoadDelegate> uIWaitForDataLoadDelegate;

    // error
    NSError *__weak connectionError;
    
    BOOL cancelled;
}
// lifecycle
- (id) initWithHypervisorConnection:(HypervisorConnectionFactory *)connection;
- (void)setUIWaitForDataLoadDelegate:(id<UIWaitForDataLoadDelegate>)delegate;

// general
- (void)waitForObjectTreeForObject:(id<HypObjReferencesProtocol>)object;
- (void)waitForObjectsOfType:(NSUInteger)hypObjectType;
- (void)waitForObjectTreeForObjectReference:(NSString *)reference objectType:(NSUInteger)hypObjectType;

// for hypConnectionDelegate
- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess;

@property (strong) UIAlertView *alertView;
@property NSUInteger  requestType;
@property (strong) id<HypObjReferencesProtocol> requestedObject;
@property (weak, readonly) NSError *connectionError;
@property (strong) NSString *requestedObjectReference;

@end
