//
//  HypOpsAppDelegate.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "HypOpsAppDelegate.h"
#import "ProgressAlert.h"
#import "EC2RegionsViewController.h"
#import "EC2ConnectionViewController.h"
#import "CloudStackZoneViewController.h"

@interface HypOpsAppDelegate ()
- (void) displayConnectingDialog;
- (void) displayFailedConnectingDialog;
@end

@implementation HypOpsAppDelegate

@synthesize alertView=alertView_;
@synthesize rootViewController;

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    if (!url) {  return NO; }
    
    NSString *URLString = [url absoluteString];
    [[NSUserDefaults standardUserDefaults] setObject:URLString forKey:@"url"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

// method to deal with any uncaught application exceptions that will cause the app to crash
void uncaughtExceptionHandler(NSException *exception) {
    NSLog(@"CRASH: %@", exception);
    NSLog(@"Stack Trace: %@", [exception callStackSymbols]);
    // add any other internal error reporting
}

-(BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);    // Normal launch stuff
    return YES;
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    
    if ([rootViewController connectedTo]){
        ConnectionState state = [[rootViewController connectedTo] GetConnectionState];
        if (state == CONNECTION_CONNECTED){
            NSLog(@"Should suspend the connection here");
            [[rootViewController connectedTo] suspendConnection];
            // wait here for the connection to suspend
            while ([[rootViewController connectedTo] GetConnectionState] == CONNECTION_SUSPENDING){
                [NSThread sleepForTimeInterval:5.0];
            };
        }
    }
    [self saveContext];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    NSDictionary* params = [RootViewController processStartupURL];
    
    // if not connected to anything
    if (![rootViewController connectedTo] || [[rootViewController connectedTo] GetConnectionState] == CONNECTION_UNCONNECTED){
        // rewind the view to the view that the hypervisor connection provides
        UINavigationController *navCon = [rootViewController navigationController];
        
        NSArray* conntrolers = [navCon viewControllers];
        UIViewController* viewController = nil;
        viewController = [conntrolers objectAtIndex:0];
        [navCon popToViewController:viewController animated:YES];
        // cause the logic to fire that processes the URL if the root was already being displayed
        if ([conntrolers count] == 1){
            [viewController viewDidAppear:NO];
        }
    }

    if ([rootViewController connectedTo]){
        // if there is a URL with a launch in it then need to close the connection
        // make sure at the connection list screen then proceed as required
        ConnectionState state = [[rootViewController connectedTo] GetConnectionState];
        if (state == CONNECTION_SUSPENDED){
            NSLog(@"Should resume suspend connection here");
            if ([params count] > 0){
                [[rootViewController connectedTo] closeConnection];
                [rootViewController setConnectedTo:nil];
            }
            else{
                // display an alert here and wait for connection
                [self displayConnectingDialog];
                // add a delegate here to the connection
                [[rootViewController connectedTo] addHypervisorConnectionDelegate:self];
                [[rootViewController connectedTo] resumeConnection];
            }
        }
    }
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    [rootViewController refreshData];
}


-(void)displayConnectingDialog{
    ProgressAlert *progressAlert = [[ProgressAlert alloc] initWithTitle:@"Syncronizing" delegate:self];
    [self setAlertView:progressAlert];
    [[self alertView] show];
}

/**
 applicationWillTerminate: saves changes in the application's managed object context before the application terminates.
 */
- (void)applicationWillTerminate:(UIApplication *)application {
    [self saveContext];
}


- (void)saveContext {
    
    NSError *error = nil;
	NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}    


#pragma mark -
#pragma mark Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext {
    
    if (managedObjectContext_ != nil) {
        return managedObjectContext_;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext_ = [[NSManagedObjectContext alloc] init];
        [managedObjectContext_ setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext_;
}


/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel {
    
    if (managedObjectModel_ != nil) {
        return managedObjectModel_;
    }
    NSString *modelPath = [[NSBundle mainBundle] pathForResource:@"HypOps" ofType:@"momd"];
    NSURL *modelURL = [NSURL fileURLWithPath:modelPath];
    managedObjectModel_ = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];    
    return managedObjectModel_;
}


/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (persistentStoreCoordinator_ != nil) {
        return persistentStoreCoordinator_;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"hypOps.sqlite"];
    
    NSError *error = nil;
    
    // temporary line to remove any old stores
    // this causes the old store to be removed and start from scratch
    // allowing the data store to be modified frequently during development
    // Remove this once the store is stable...
    // [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
    
    
    // set up the options to do auto migrate between datamodels
    // v1 has an issue where the relationship from serverDetail to metadata is a to 1
    // v2 has removed metadata as it seems a bit pointless
    // v2 also has connectionCount and lastConnectionTime fields
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        
    persistentStoreCoordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    if (![persistentStoreCoordinator_ addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES],NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    
        abort();
    }    
    
    return persistentStoreCoordinator_;
}


#pragma mark -
#pragma mark Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
    // get any caching in the hypervisor connections to free up mamory where possible
}



-(void)displayFailedConnectingDialog{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Connection failed" message:@"The hypervisor can no longer be contacted.  Please reconnect again when connection is available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [self setAlertView:alertView];
    [[self alertView] show];
}


#pragma mark -
#pragma mark HypervisorConnection delegate
- (void)hypervisorConnectedToConnection:(HypervisorConnectionFactory *)hypervisorConnection withResult:(BOOL)result{
    if (!result){
        [[rootViewController connectedTo] removeHypervisorConnectionDelegate:self];
        UINavigationController *navCon = [rootViewController navigationController];
        [navCon popToRootViewControllerAnimated:YES];
        [[self alertView] dismissWithClickedButtonIndex:0 animated:YES];
        [self setAlertView:nil];
        [self displayFailedConnectingDialog];
        return;
    }
    
    // if the connecting dialog was cancelled ignore the connection result.
    ConnectionState state = [hypervisorConnection GetConnectionState];
    if (state == CONNECTON_RESUMING){
        [[rootViewController connectedTo] removeHypervisorConnectionDelegate:self];
        [hypervisorConnection reloadCoreData];
        // rewind the view to the view that the hypervisor connection provides
        UINavigationController *navCon = [rootViewController navigationController];

        NSArray* conntrolers = [navCon viewControllers];
        UIViewController* viewController = nil;
        // controller at 0 is root
        // controller at 1 is the connection top node
        if ([conntrolers count] > 1){
           viewController = [conntrolers objectAtIndex:1];
        }
        else{
           viewController = [conntrolers objectAtIndex:0];
        }
        
        [navCon popToViewController:viewController animated:YES];
        [[self alertView] dismissWithClickedButtonIndex:0 animated:YES];
        [self setAlertView:nil];
    }
}

@end
