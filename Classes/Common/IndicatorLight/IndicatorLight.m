//
//  IndicatorLight.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "IndicatorLight.h"

@implementation IndicatorLight

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setColor:[UIColor greenColor]];
    }
    return self;
}

-(void)setColor:(UIColor *)color{
    _color = color;
    [self setNeedsDisplay];
}

-(UIColor *)color{
    return _color;
}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    float lineWidth = 2;
    
    CGRect rect2 = rect;
    rect2.origin.x += lineWidth;
    rect2.origin.y += lineWidth;
    rect2.size.width -=(2*lineWidth);
    rect2.size.height -=(2*lineWidth);

    CGRect rect3 = rect2;
    rect3.origin.x += lineWidth;
    rect3.origin.y += lineWidth;
    rect3.size.width -=(2*lineWidth);
    rect3.size.height -=(2*lineWidth);

    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, lineWidth);
    CGContextSetFillColorWithColor(context,_color.CGColor);
    CGContextAddEllipseInRect(context, rect2);
    CGContextFillPath(context);

    CGContextSetStrokeColorWithColor(context,[UIColor grayColor].CGColor);
    CGContextAddEllipseInRect(context, rect3);
    CGContextStrokePath(context);

    
}
@end
