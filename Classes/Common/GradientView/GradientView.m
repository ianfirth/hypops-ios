//
//  GradientView.m
//  UICommonControls
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "GradientView.h"


@implementation GradientView

@synthesize startColor, endColor, gradientViewFillMode, useRadial;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setStartColor : [UIColor colorWithRed:0 green:0 blue:0 alpha:1]];
        [self setEndColor : [UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
        [self setGradientViewFillMode:GradientViewFillModeFull];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGFloat start_red = 1;
    CGFloat start_green = 1;
    CGFloat start_blue = 1;
    CGFloat start_alpha = 1;
    
    CGFloat end_red = 1;
    CGFloat end_green = 1;
    CGFloat end_blue = 1;
    CGFloat end_alpha = 1;
    
    CGColorRef startColorref = [[self startColor] CGColor];
    long numComponents = CGColorGetNumberOfComponents(startColorref);
    
    if (numComponents == 4) {
        const CGFloat *components = CGColorGetComponents(startColorref);
        start_red     = components[0];
        start_green = components[1];
        start_blue   = components[2];
        start_alpha = components[3];
    }
    
    CGColorRef endColorref = [[self endColor] CGColor];
    numComponents = CGColorGetNumberOfComponents(endColorref);
    
    if (numComponents == 4) {
        const CGFloat *components = CGColorGetComponents(endColorref);
        end_red     = components[0];
        end_green = components[1];
        end_blue   = components[2];
        end_alpha = components[3];
    }
    
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    
    CGGradientRef glossGradient;
    CGColorSpaceRef rgbColorspace;
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    
    CGFloat components[8] = { start_red, start_green, start_blue, start_alpha,  // Start color
        end_red, end_green, end_blue, end_alpha }; // End color
    
    rgbColorspace = CGColorSpaceCreateDeviceRGB();
    glossGradient = CGGradientCreateWithColorComponents(rgbColorspace, components, locations, num_locations);
    
    if (!useRadial){
    
    CGRect currentBounds = rect;
    CGPoint topCenter = CGPointMake(CGRectGetMidX(currentBounds), CGRectGetMinY(currentBounds));
    CGPoint BottomCenter = CGPointMake(CGRectGetMidX(currentBounds), CGRectGetHeight(currentBounds));
    CGPoint midCenter = CGPointMake(CGRectGetMidX(currentBounds), CGRectGetMidY(currentBounds));

    if (gradientViewFillMode == GradientViewFillModeFull){
        CGContextDrawLinearGradient(currentContext, glossGradient, topCenter, BottomCenter, 0);
    }
    else
    {
        // tophalf
        CGContextDrawLinearGradient(currentContext, glossGradient, topCenter, midCenter, 0);
        CGContextDrawLinearGradient(currentContext, glossGradient, midCenter, BottomCenter, 0);
        // bottomhalf
        CGRect bottomRect = CGRectMake(rect.origin.x, midCenter.y, rect.size.width, BottomCenter.y - midCenter.y);
        CGContextSetFillColorWithColor(currentContext, endColorref);
        CGContextFillRect(currentContext, bottomRect);
    }
    }
    else
    {
        // Normalise the 0-1 ranged inputs to the width of the image
        float radius = 0.8;
        float centerx = rect.origin.x + (rect.size.width /2);
        float centery = rect.origin.y + (rect.size.height /2);
        
        CGPoint myCentrePoint = CGPointMake(centerx, centery);
        float myRadius = MIN(rect.size.width, rect.size.height) * radius;
        
        // Draw it!
        CGContextDrawRadialGradient (UIGraphicsGetCurrentContext(), glossGradient, myCentrePoint,
                                     0, myCentrePoint, myRadius,
                                     kCGGradientDrawsAfterEndLocation);
        
   }
    
    CGGradientRelease(glossGradient);
    CGColorSpaceRelease(rgbColorspace); 
    [super drawRect:rect];
}



@end
