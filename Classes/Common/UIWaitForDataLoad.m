//
//  UIWaitForDataLoad.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "UIWaitForDataLoad.h"
#import "ProgressAlert.h"

@interface UIWaitForDataLoad ()
- (void)displayErrorDialog;
- (void)displayLoadingDialog;
- (void)waitForObjectTreeForObjectReference;
- (void)waitForObjectTreeForObjectReference2;
@end

@implementation UIWaitForDataLoad

@synthesize alertView, requestType, requestedObject, requestedObjectReference, connectionError;


- (id) initWithHypervisorConnection:(HypervisorConnectionFactory *)connection{
    self = [super init];
    if (self){
        hypConnection = connection;
        cancelled = NO;
    }
    return self;
}

- (void) dealloc{
    hypConnection = nil;
    [self setUIWaitForDataLoadDelegate:nil];
}

#pragma mark -
#pragma mark XenUIWaitForDataLoadDelegate

/*
 * enable consumer to set itself as the delegage for the instance
 */
- (void)setUIWaitForDataLoadDelegate:(id<UIWaitForDataLoadDelegate>)delegate{
    uIWaitForDataLoadDelegate = delegate;
}

 - (void)displayLoadingDialog{
    // set the cancel to a selector so that can set error and return on this too.
     if (alertView)
     {
         alertView = nil;
     }
     alertView = [[ProgressAlert alloc] initWithTitle:@"Loading" delegate:self];
    [alertView show];
}

- (void)displayErrorDialog{
    if (alertView)
    {
        alertView = nil;
    }
    alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[connectionError localizedDescription] delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
    [alertView show];
}


- (void)clearDialogWithAnimation:(BOOL)animated{
    if (!cancelled){
    if ([self alertView] != nil)
    {
        [[self alertView] dismissWithClickedButtonIndex:0 animated:animated];
        [self setAlertView:nil];
    }

    if (connectionError){
        [self displayErrorDialog];
        // move this onto the close action.
        if ([(NSObject *)uIWaitForDataLoadDelegate respondsToSelector: @selector(dataLoaderFinished:withResult:)]){
            [uIWaitForDataLoadDelegate dataLoaderFinished:self withResult:NO];
        }
    }
    else
    {
        // always indicates sucsess at present need to fix this up.
        if ([(NSObject *)uIWaitForDataLoadDelegate respondsToSelector: @selector(dataLoaderFinished:withResult:)]){
            [uIWaitForDataLoadDelegate dataLoaderFinished:self withResult:YES];
        }
    }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    // always indicates sucsess at present need to fix this up.
    cancelled = YES;
    if (!connectionError){
        connectionError = [NSError errorWithDomain:@"Loading Canceled by User" code:0 userInfo:nil];
    }
    
    if ([(NSObject *)uIWaitForDataLoadDelegate respondsToSelector: @selector(dataLoaderFinished:withResult:)]){
        [uIWaitForDataLoadDelegate dataLoaderFinished:self withResult:NO];
    }
}

- (void)waitForObjectTreeForObjectReference:(NSString *)reference objectType:(NSUInteger)hypObjectType{
    NSLog(@"Dataloader created to wait for data load");

    [self setRequestedObjectReference:reference];
    [self setRequestType:hypObjectType];
    [self waitForObjectTreeForObjectReference];
}

// makes the request then sits in loop waiting for it
- (void)waitForObjectTreeForObjectReference{
    [hypConnection PopulateHypObjectreference:[self requestedObjectReference] objectType:[self requestType] checkOnly:NO];
        
    [self waitForObjectTreeForObjectReference2];
}

- (void)waitForObjectTreeForObjectReference2{
    // keep requesting until got it all.
    // This requests (i,e, checkOnly NO) and looks (i.e. checkOnly YES) so that async calls still wait for completion
    // The yes from the populate only really means that all the requests have been made.
    if ([hypConnection PopulateHypObjectreference:[self requestedObjectReference] objectType:[self requestType] checkOnly:YES]){
        [self clearDialogWithAnimation:YES];
        return;
    }
    else if ([self alertView] == nil)
    {
        [self displayLoadingDialog];
    }
    
    
    [self performSelector:@selector(waitForObjectTreeForObjectReference2) withObject:nil afterDelay:1]; // try again in a second
}

- (void)waitForObjectTreeForObject:(id<HypObjReferencesProtocol>)object{
    // need some way of detecting errors here I suspect.
    // same sort of idea as in the wait for objects of type.
    // need to have a play arround with this idea
    [self setRequestedObject:object];
    
    if ([hypConnection CheckPopulatedHypObject:object]){
        [self clearDialogWithAnimation:YES];
    }
    else{
        if ([self alertView] == nil)
        {
            [self displayLoadingDialog];
        }
        
        [self performSelector:@selector(waitForObjectTreeForObject:) withObject:object afterDelay:1]; // try again in a second
    }
}

- (void)waitForObjectsOfType:(NSUInteger)hypObjectType{
    [self setRequestType:hypObjectType];
    if ([[hypConnection hypObjectsForType:hypObjectType] count] >0 ){
        [self clearDialogWithAnimation:YES];
    }
    else{
        if ([self alertView] == nil)
        {
            [self displayLoadingDialog];
        }
        [hypConnection addHypervisorConnectionDelegate:self];

        [hypConnection RequestHypObjectsForType:hypObjectType];
    }
}

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // this assumes that it worked.
    // if it did not then need to decide what the best way to resolve this is
    // probably should set an error indicator so that the caller can resolve the issue
    // might also display an error alert too.
    // lets look into this next
    if (sucsess){
        if (([self requestType] & objectType) > 0){
            [self clearDialogWithAnimation:YES];
        }
    }
    else
    {
        connectionError = [hypervisorConnection lastError];
    }
}

@end
