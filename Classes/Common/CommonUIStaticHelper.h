//
//  CommonUIStaticHelper.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "DetailViewProtocol.h"

@interface CommonUIStaticHelper : NSObject

/**
 *  This method descides id the viewController should be displayed in the iPad details list
 *  if one is available otherwise it will be displayed in the current list view that is used for
 *  navigation
 *  @param controller The view controller to be displayed
 *  @param the navigation controller that should be used to determine the areas available
 */
+ (void) displayNextView:(UIViewController *)controller usingNavigationController:(UINavigationController *)navCon andRootViewController:(UIViewController*)rootController;


/**
 *  displays the controller in the master (list) area and if there is a detail view associated
 * and there is a detail view area available it is displayed too
 *  @param controller The view controller to be displayed
 *  @param the navigation controller that should be used to determine the areas available
 */

+ (void) displayMasterViewAndDetail:(UIViewController<DetailViewProtocol> *)controller usingNavigationController:(UINavigationController *)navCon;

@end
