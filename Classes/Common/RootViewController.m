//
//  RootViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "RootViewController.h"
#import "HypervisorConnectionFactory.h"
#import "ImageBuilder.h"
#import "AlertWithDataStore.h"
#import "ProgressAlert.h"
#import "EC2RegionsViewController.h"
#import "XenPoolViewController.h"
#import "EC2ConnectionViewController.h"
#import "CloudStackZoneViewController.h"
#import "ServerDetail.h"
#import "ConnectionItemUITableViewCell.h"

@interface HypTypeAlertAction : UIAlertAction
    @property HypervisorType hypType;
@end

@implementation HypTypeAlertAction
    @synthesize hypType;
@end

@implementation RootViewController 

@synthesize fetchedResultsController=fetchedResultsController_, managedObjectContext=managedObjectContext_;
@synthesize alertView=alertView_;
@synthesize connectedTo;
@synthesize connectingTo;
@synthesize theActionSheet;

+(NSDictionary*) processStartupURL{
    NSMutableDictionary* result = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    NSString* urlString = [[NSUserDefaults standardUserDefaults] objectForKey:@"url"];
    if (!urlString){
        return result;
    }
    
    NSURL* url = [NSURL URLWithString:urlString];
    NSString* host = [url host];
    if (host){
        [result setObject:host forKey:@"&actionType"];
        NSArray *urlComponents = [[url query] componentsSeparatedByString:@"&"];
        
        for (NSString *keyValuePair in urlComponents)
        {
            NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
            NSString *key = [pairComponents objectAtIndex:0];
            NSString *value = [pairComponents objectAtIndex:1];
            
           
            // url decode the value
            NSString* decodedValue = [value stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
            [result setObject:decodedValue forKey:key];
        }
    }
    return result;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(ConnectionItemUITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
  
    // get the new badge and last connection time added here
    ServerDetail* serverDetail = [[self fetchedResultsController] objectAtIndexPath:indexPath];NSUInteger  connectionCount = [serverDetail connectionCount];
    
    [cell setConnectionName:[serverDetail name]];
    NSString *addressString = [serverDetail address];

    // if there is no address set then do not display
    // EC2 connections do not have an address field. In future might be better to change this
    // so that the entire cell is provided by the connection object then it cab be rendered
    // appropriatly for each connection type.
    if (addressString){
        [cell setConnectionAddress:addressString];
    }
    else{
        [cell setConnectionAddress:@" - "];
    }
    
    // to make sure that this is backward compatible it used to put a string in here when
    // there was only a xenServer type, now it puts an integer in it.
    if ([[serverDetail type] isEqualToString:@"XenServer"]){
        cell.tag = HYPERVISOR_XEN;
    }
    else{
        cell.tag = [[serverDetail type] intValue];
    }

    NSArray *conDefs = [HypervisorConnectionFactory connectionDefinitions];
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"hypervisorType == %@",[NSNumber numberWithLong:cell.tag]]];
    NSArray *result = [conDefs filteredArrayUsingPredicate:pred];
    
    if ([result count] >0){
        ConnectionDefinition *conDef  = (ConnectionDefinition *)[result objectAtIndex:0]; 
        
        // if connection has < 2 counts then is new
        if (connectionCount < 2 ){
            [cell setConnectionImage:conDef isNew:YES];
        }
        else{
            [cell setConnectionImage:conDef isNew:NO];
        }
    }
    
    // do alternate striping on the rows
    [cell setAlternate:(indexPath.row % 2)];
}

-(void) raiseAlertWithTitle:(NSString*)title andMessage:(NSString*)message{
     UIAlertView *myAlertView= [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    
    [myAlertView setTag:-500];
    [myAlertView show];
}

-(NSUInteger)getHypTypeFromString:(NSString*)typeStr{NSUInteger  type = HYPERVISOR_UNKNOWN;
    if (typeStr && [typeStr caseInsensitiveCompare:@"XenServer"] == NSOrderedSame){
        type = HYPERVISOR_XEN;
    }
    else if (typeStr && [typeStr caseInsensitiveCompare:@"Amazon"] == NSOrderedSame){
        type = HYPERVISOR_EC2;
    }
    else if (typeStr && [typeStr caseInsensitiveCompare:@"AmazonEC2"] == NSOrderedSame){
        type = HYPERVISOR_EC2;
    }
    else if (typeStr && [typeStr caseInsensitiveCompare:@"CloudStack"] == NSOrderedSame){
        type = HYPERVISOR_CLOUDSTACK_NATIVE;
    }
    return type;
}

-(void) refreshData{
    NSDictionary* params = [RootViewController processStartupURL];
    // remove the entry now its been used once
    NSLog(@"The url params are %@",params);
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"url"];
    if (![[NSUserDefaults standardUserDefaults] synchronize]){
        NSLog(@"Failed to remove url input object after use");
    }
    
    HypervisorConnectionDefinition *hypConDef = [[HypervisorConnectionDefinition alloc] initWithParams:params];
    
    if ([hypConDef hasAction:@"makeConnection"]){
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:0];
        NSInteger resultCount = [sectionInfo numberOfObjects];
        for (int i = 0; i < resultCount ; i++){
            NSManagedObject* connectionObject = [[sectionInfo objects] objectAtIndex:i] ;
            if ([[hypConDef name] isEqualToString:[connectionObject valueForKey:@"name"]]){
                [self makeConnectionWithIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
                // remove the entry now its been used once
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"url"];
                break;
            }
            
            if ([hypConDef name]){
                [self raiseAlertWithTitle:@"No Connection Found" andMessage:[NSString stringWithFormat:@"The connection '%@' does not exist",[hypConDef name]]];
            }
            else{
                [self raiseAlertWithTitle:@"No Connection Provided" andMessage:[NSString stringWithFormat:@"The connection name must be provided"]];
            }
            
        }
    }
    else if ([hypConDef hasAction:@"newConnection"]){
        // if connection with this name already exists then raise alert
        if (![hypConDef isRecognisedType]){
            // raise an alert here unknown hypervisor type
            if ([hypConDef hypervisorTypeString]){
                [self raiseAlertWithTitle:@"New Connection Failed" andMessage:[NSString stringWithFormat:@"The connection type '%@' was not recognised",[hypConDef hypervisorTypeString]]];
            }else{
                [self raiseAlertWithTitle:@"New Connection Failed" andMessage:@"The connection type was not provided."];
                
            }
        }
        else{
            NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
            
            NSEntityDescription *serverDetailEntity = [NSEntityDescription entityForName:@"ServerDetail"
                                                                  inManagedObjectContext:context];
            
            ServerDetail* serverDetail = [[ServerDetail alloc] initWithEntity:serverDetailEntity
                                               insertIntoManagedObjectContext:context];
            [serverDetail setConnectionCount:0];
            // this will put it to the top of the list.
            [serverDetail setLastConnectionTime:[NSDate date]];
            [serverDetail setType:[NSString stringWithFormat:@"%lu",(unsigned long)[hypConDef hypervisorType]]];
            [serverDetail setName:[hypConDef name]];
            [serverDetail setAddress:[hypConDef address]];
            [serverDetail setUsername:[hypConDef userName]];
            [serverDetail setPassword:[hypConDef password]];
            
            NSError* err;
            [context save:&err];
            [context processPendingChanges];
            // also launch the connection if required.
            if ([hypConDef hasSecondaryAction:@"launch"]){
                [self makeConnectionWithAddress:[hypConDef address] Username:[hypConDef userName] password:[hypConDef password] hypType:[hypConDef hypervisorType]];
            }
        }
    }
    
    if ([hypConDef hasAction:@"launchConnection"]){
        // launch the connection without saving it.
        if (hypConDef && ![hypConDef isRecognisedType]){
            [self raiseAlertWithTitle:@"Launch Connection Failed" andMessage:@"The connection type was not recognised"];
        }
        else if (hypConDef && ![hypConDef isSuitableForLaunch]){
            [self raiseAlertWithTitle:@"Launch Connection Failed" andMessage:@"The connection address or username was not provided"];
        }
        else{
            [self makeConnectionWithAddress:[hypConDef address] Username:[hypConDef userName] password:[hypConDef password] hypType:[hypConDef hypervisorType]];
        }
    }
    [[self tableView] reloadData];    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setConnectedTo:nil];
    [self refreshData];
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self setConnectedTo:nil];
}

- (void) viewDidLoad{    
    self.tableView.allowsSelectionDuringEditing = YES;
    [[self tableView] registerNib:[UINib nibWithNibName:@"ConnectionItemUITableViewCell" bundle:nil] forCellReuseIdentifier:@"ConnectionItemUITableViewCell"];

    [super viewDidLoad];
}

- (void) dealloc{
    if ([self connectingTo]){
    }
}


-(void) insertNewObject{
    // if the UIAlertAction class is available (initially used in ios8 then use it, otherwise fallback to the UIActionSheet)
    if(NSClassFromString(@"UIAlertAction")) {
        [self insertNewObject_ios8];
    }
    else{
        [self insertNewObject_ios7];
    }
}

-(void) insertNewObject_ios8{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Choose the virtualization platform"
                                                                      message:@""
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    
      NSArray *connectionTypes = [HypervisorConnectionFactory connectionDefinitions];
      for (ConnectionDefinition *con in connectionTypes){
          HypTypeAlertAction* action = [HypTypeAlertAction actionWithTitle:[con connectionString] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
              [self insertNewObjectForHypervisorType: [(HypTypeAlertAction*)action hypType]];
          }];
          
          action.hypType = [con hypervisorType];
          [alertController addAction:action];
      }

    // add the cancel button
    UIAlertAction* action = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        NSLog(@"canceling Do nothing");
      }];
    
    [alertController addAction:action];

      // get the appropritate subclass (ipad or iPhone) to render the action sheet
      [self displayHypervisorTypeSelectionAlertController:alertController];
}
      

-(void) insertNewObject_ios7{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose the virtualization platform" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    [self setTheActionSheet:actionSheet ];
    
    NSArray *connectionTypes = [HypervisorConnectionFactory connectionDefinitions];
    for (ConnectionDefinition *con in connectionTypes){
        [theActionSheet addButtonWithTitle:[con connectionString]];
    }
    
    // get the appropritate subclass (ipad or iPhone) to render the action sheet
    [self displayHypervisorTypeSelectionActionSheet:theActionSheet];
}


#pragma mark - UIActionSheetdelegate
// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSArray *connectionTypes = [HypervisorConnectionFactory connectionDefinitions];
    // button index is an index into this array
    // its one of the connection types
    if (buttonIndex < connectionTypes.count){
        ConnectionDefinition* hypDef = connectionTypes[buttonIndex];
        [self insertNewObjectForHypervisorType: [hypDef hypervisorType]];
        [self setTheActionSheet:nil];
    }
}

#pragma mark -
#pragma mark Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (fetchedResultsController_ != nil) {
        return fetchedResultsController_;
    }
    
    /*
     Set up the fetched results controller.
     */
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ServerDetail" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    // sort by last connection date followed by name
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"lastConnectionTime" ascending:NO];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor1, sortDescriptor2, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Root"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    
    NSError *error = nil;
    if (![fetchedResultsController_ performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return fetchedResultsController_;
}    


#pragma mark -
#pragma mark Fetched results controller delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            break;
        case NSFetchedResultsChangeUpdate:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSInteger resultCount = [sectionInfo numberOfObjects];
    UINavigationItem *navItem = [self navigationItem];
    // if no items in the list do not enable the edit button
    if (resultCount > 0){
        [[navItem leftBarButtonItem] setEnabled:YES];
    }
    else
    {
        // if in edit mode do not diable the button as it is a done not an edit
        if (![tableView isEditing]){
            [[navItem leftBarButtonItem] setEnabled:NO];
        }
    }
    return resultCount;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ConnectionItemUITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ConnectionItemUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    [self configureCell:(ConnectionItemUITableViewCell*)cell atIndexPath:indexPath];
    return cell;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the managed object for the given index path
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        // Save the context.
        NSError *error = nil;
        if (![context save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        // allow anything to respons as required
        [self connectionDeleted];
    }  
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // The table view should not be re-orderable.
    return NO;
}

// this method gets the user to add the password if required.
-(void) makeConnectionWithAddress:(NSString*)address Username:(NSString*)username password:(NSString*)password hypType:(NSUInteger)hypType{
    if (!password || [password isEqualToString:@""]){
        
        AlertWithDataStore *myAlertView = [[AlertWithDataStore alloc] initWithTitle:@"Password not stored"
                                                                            message:@"Enter password for connection"
                                                                           delegate:self
                                                                  cancelButtonTitle:@"Cancel"
                                                                  otherButtonTitles:@"Connect", nil];
        
        myAlertView.textField.placeholder = @"password";
        myAlertView.textField.autocorrectionType = UITextAutocorrectionTypeNo;
        myAlertView.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        myAlertView.textField.secureTextEntry = YES;
        myAlertView.textField.returnKeyType = UIReturnKeyGo;
        myAlertView.textField.delegate = self;
        
        [myAlertView  addMetaDataWithKey:@"address" andValue:address];
        [myAlertView  addMetaDataWithKey:@"username" andValue:username];
        if (hypType == HYPERVISOR_XEN){
            [myAlertView  addMetaDataWithKey:@"hyptype" andValue:@"XEN"];
        }
        else if (hypType == HYPERVISOR_EC2){
            [myAlertView  addMetaDataWithKey:@"hyptype" andValue:@"EC2"];
        }
        else{
            [myAlertView  addMetaDataWithKey:@"hyptype" andValue:@""];
        }
        
        [myAlertView setTag:-100];
        [self setAlertView:myAlertView];
        [myAlertView show];
    }else{
        [self connectToAddress:address withUsername:username andPassword:password forHypType:(HypervisorType)hypType];
    }

}

-(void) makeConnectionWithIndexPath:(NSIndexPath *)indexPath{

    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    
    // not editing so lets navigate to the relevent page
    // construct an appropritate connection obect
    ServerDetail *serverDetail = [self.fetchedResultsController objectAtIndexPath:indexPath];

    [serverDetail setConnectionCount:[serverDetail connectionCount]+1];
    [serverDetail setLastConnectionTime:[NSDate date]];
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"cannot update metadata for some reason");
    }

    // if password is empty then display an enter a password pop up dialog here so that it can be entered on demand
    // TODO change this to a call back to the conection Definition like the other things as
    // this will not always be called password in UI.
    
    HypervisorType hypType;
    if ([[serverDetail type] isEqualToString:@"XenServer"]){
        hypType = HYPERVISOR_XEN;
    }
    else{
        hypType = [[serverDetail type] intValue];
    }

    [self makeConnectionWithAddress:[serverDetail address] Username:[serverDetail username] password:[serverDetail password] hypType:hypType];
}

NSMutableData* receivedData;

- (void) connectToConnectionDefinition:(HypervisorConnectionDefinition *) hypConDef{
    return [self connectToAddress:[hypConDef address] withUsername:[hypConDef userName] andPassword:[hypConDef password] forHypType:(HypervisorType)[hypConDef hypervisorType]];
}
             
- (void) connectToAddress:(NSString *)address withUsername:(NSString *)username andPassword:(NSString *)password forHypType:(HypervisorType)hypType{
    HypervisorConnectionFactory *connectionFactory = [[HypervisorConnectionFactory alloc] initWithHypervisorType:hypType usingAddress:address withUsername:username andPassword:password delegate:self];
    // Display a conecting popup until the connection connects
    if ([connectionFactory GetConnectionState] == CONNECTION_CONNECTED){
        [self displayHypervisorContentViewForHypervisorConnection:connectionFactory];
    }
    else {
        [self displayConnectingDialogForConnection:connectionFactory];
    }
}

// ====================
// Callbacks
// ====================

#pragma mark -
#pragma mark NSURLConnection delegate methods
- (NSURLRequest *)connection:(NSURLConnection *)connection
 			 willSendRequest:(NSURLRequest *)request
 			redirectResponse:(NSURLResponse *)redirectResponse {
 	NSLog(@"Connection received data, retain count");
    return request;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
 	NSLog(@"Received response: %@", response);
 	
    //[receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
 	NSLog(@"Received %lu bytes of data", (unsigned long)[data length]);
 	
   [receivedData appendData:data];
   NSLog(@"Received data is now %lu bytes", (unsigned long)[receivedData length]);
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
 	NSLog(@"Error receiving response: %@", error);
   // [[NSAlert alertWithError:error] runModal];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // Once this method is invoked, "responseData" contains the complete result
 	NSLog(@"Succeeded! Received %lu bytes of data", (unsigned long)[receivedData length]);
 	
 	NSString *dataStr=[[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
 	NSLog(@"Succeeded! Received %@ bytes of data", dataStr);
}

#pragma mark -
#pragma mark Text Field protocol
- (BOOL)textFieldShouldReturn:(UITextField *) theTextField {
    [theTextField resignFirstResponder];
    // do not know why thie dismiss does not call the method below it, so call it directly for now at least.
    [[self alertView] dismissWithClickedButtonIndex:0 animated:YES];
    [self alertView:[self alertView] clickedButtonAtIndex:1];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {   
    [textField resignFirstResponder];

}

-(void)displayConnectingDialogForConnection:(HypervisorConnectionFactory *)hypConnection{
    ProgressAlert *alert = [[ProgressAlert alloc] initWithTitle:@"Connecting" delegate:self];
    [self setAlertView:alert];
    [self setConnectedTo:nil];
    [self setConnectingTo:hypConnection];
    [[self alertView] show];
}

-(void)displayConnectingErrorDialogWithMessage:(NSString *)message{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Connection Failed" message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [self setAlertView:alertView];
    [[self alertView] show];
}

-(void)clearDialogWithAnimation:(BOOL)animated{
    if ([self alertView] != nil)
    {
        [[self alertView] dismissWithClickedButtonIndex:0 animated:animated];
        [self setAlertView:nil];
    }
}

// process any UIAlert view responses
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    // determine if this is a password box (contains a subView with a tag of -100);
    bool passwordDialog = ([alertView tag] == -100);
    
    if (passwordDialog){
        if (buttonIndex > 0){   // only interested in the connect button
            AlertWithDataStore *awds = (AlertWithDataStore*)alertView;
            NSString *password = [awds textString];

            NSString *address = [awds metaDataForKey:@"address"];
            NSString *username = [awds metaDataForKey:@"username"];

            NSString *strHypType = [awds metaDataForKey:@"hyptype"];NSUInteger  hypType;
            if ([strHypType isEqualToString:@"XEN"]){
                hypType = HYPERVISOR_XEN;
            }
            else if ([strHypType isEqualToString:@"EC2"]){
                hypType = HYPERVISOR_EC2;
            }
            else if ([strHypType isEqualToString:@"CLOUDSTACK"]){
                hypType = HYPERVISOR_EC2;
            }
            else {
                // throw an exception only support XEN or EC2 at present
                [NSException raise:NSInternalInconsistencyException format:@"Hypervisor type was not a supported value",nil];
            }
            
            [self connectToAddress:address withUsername:username andPassword:password forHypType:(HypervisorType)hypType];
        }
    }
}



- (void)displayHypervisorContentViewForHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnectionFactory{
    
    UIViewController* viewController;
    if ([hypervisorConnectionFactory hypervisorConnectionType] == HYPERVISOR_XEN){
        viewController = [[XenPoolViewController alloc] initWithHypervisorConnection:hypervisorConnectionFactory];
    }
    else if ([hypervisorConnectionFactory hypervisorConnectionType] == HYPERVISOR_EC2){
        viewController  = [[EC2RegionsViewController alloc] initWithHypervisorConnection:hypervisorConnectionFactory];
    }
    else if ([hypervisorConnectionFactory hypervisorConnectionType] == HYPERVISOR_CLOUDSTACK_AWS){
        viewController  = [[EC2ConnectionViewController alloc] initWithHypervisorConnection:hypervisorConnectionFactory];
    }
    else if ([hypervisorConnectionFactory hypervisorConnectionType] == HYPERVISOR_CLOUDSTACK_NATIVE){
        viewController  = [[CloudStackZoneViewController alloc] initWithHypervisorConnection:hypervisorConnectionFactory];
    }
    else{
        NSLog(@"Unrecognised hypervisor Connection type");
    }

    // clear the connecting dialog
    [self clearDialogWithAnimation:YES];
    [self setConnectedTo:hypervisorConnectionFactory];

    if (viewController){
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

#pragma mark -
#pragma mark HypervisorConnection delegate
- (void)hypervisorConnectedToConnection:(HypervisorConnectionFactory *)hypervisorConnection withResult:(BOOL)result{
    ConnectionState state = [hypervisorConnection GetConnectionState];
    if (state != CONNECTON_RESUMING){
        if (result){
            [self setConnectedTo:hypervisorConnection];
            [self displayHypervisorContentViewForHypervisorConnection:hypervisorConnection];
        }
        else {
            // change it to a failed alert with a warning message
            [self clearDialogWithAnimation:NO];
            [self displayConnectingErrorDialogWithMessage:[[hypervisorConnection lastError] localizedDescription]];
            [self setConnectedTo:nil];
        }
        if ([self connectingTo]){
            [self setConnectingTo:nil];
        }
    }
}

/** this is here purely to allow it to be overriden in a subclass to 
 * undertake any operations required when this happens
 */
-(void) connectionDeleted{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:0];
    NSInteger resultCount = [sectionInfo numberOfObjects];
    
    if (resultCount == 0){
        [self setEditing:NO];
        [[self tableView] reloadData];
    }
}

@end
