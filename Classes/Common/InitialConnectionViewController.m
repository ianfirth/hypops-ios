//
//  InitialConnectionViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "InitialConnectionViewController.h"
#import "RootViewController_ipad.h"

@implementation InitialConnectionViewController
@synthesize hypConnID, hypervisorType;

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection{
    self = [super init];
    if (self){
        [self setHypConnID: [hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
    }
    return self;
}

-(void) dealloc{
    NSLog(@"InitialConnectionViewController being deallocated");
    [self setHypConnID:nil];
}


#pragma mark -
#pragma mark HypervisorConnection protocol implementation
- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    [[self tableView] reloadData];
}


#pragma mark -
#pragma mark View lifecycle

// if the navigation controller returns to the root view then close the current connection
-(void) navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if ([[navigationController viewControllers] count] == 1){
        // back to root view so close this connection as must be finished with
        HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:[self hypervisorType] connectonID:[self hypConnID]];
        [hypervisorConnection closeConnection];
        [self setHypConnID:nil];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}


-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    inRotation = NO; 
    [[self tableView] reloadData];
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    inRotation = YES;
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void) viewWillAppear:(BOOL)animated{
    UIDevice *device = [UIDevice currentDevice];
    if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        UINavigationController *navCon = [self navigationController];
        UIViewController *rootController = [[navCon viewControllers] objectAtIndex:0];
        
        // use detail view for ipad
        if ([rootController respondsToSelector:@selector(detailViewNavigationController)]){
            UINavigationController *dvController = [rootController performSelector:@selector(detailViewNavigationController)];
            if (dvController){
                if (!inRotation){
                [RootViewController_ipad setDetailViewRootController:[self detailContentView] onNavigationController:dvController];
                }
            }
        }
    }
    
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self navigationController] setDelegate:self];
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
//#error this is used so make sure the connection is closed, but unless this is called in the will disappear the issue exists when the last connection was a XenPool connection
    [[self navigationController] setDelegate:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

@end
