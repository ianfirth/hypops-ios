//
//  HypervisorConnection.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Represents the data needed for a Hypervisor

#import "HypervisorConnectionDefinition.h"
#import "HypervisorConnectionFactory.h"

@interface NSDictionary (caseINsensitive)
-(id) objectForCaseInsensitiveKey:(id)aKey;
@end

@implementation NSDictionary (caseINsensitive)

-(id) objectForCaseInsensitiveKey:(id)aKey {
    for (NSString *key in self.allKeys) {
        if ([key compare:aKey options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            return [self objectForKey:key];
        }
    }
    return  nil;
}
@end

@interface HypervisorConnectionDefinition ()
   -(NSInteger)getHypTypeFromString:(NSString*)typeStr;
@end

@implementation HypervisorConnectionDefinition
@synthesize name = _name;
@synthesize address = _address;
@synthesize userName = _userName;
@synthesize password = _password;
@synthesize hypervisorType = _hypervisorType;
@synthesize hypervisorTypeString = _hypervisorTypeString;
@synthesize secondaryAction = _secondaryAction;

-(id) initWithParams:(NSDictionary*)theParams{
    self = [super init];
    if (self){
        NSString* typeStr = [theParams objectForCaseInsensitiveKey:@"type"];
        _hypervisorTypeString = typeStr;
        _hypervisorType = [self getHypTypeFromString:typeStr];
        _name = [theParams objectForCaseInsensitiveKey:@"name"];
        // EC2 connections must not have an address or they will go wrong.
        if (_hypervisorType == HYPERVISOR_EC2){
            _address = nil;
        }else{
            _address = [theParams objectForCaseInsensitiveKey:@"address"];
        }
        _userName = [theParams objectForCaseInsensitiveKey:@"username"];
        _password = [theParams objectForCaseInsensitiveKey:@"password"];
        _actionType = [theParams objectForCaseInsensitiveKey:@"&actionType"];
        _secondaryAction = [theParams objectForCaseInsensitiveKey:@"secondaryAction"];
    }
    return self;
}

-(bool) isRecognisedType{
    return ([self hypervisorType] != HYPERVISOR_UNKNOWN);
}

-(bool) isSuitableForLaunch{
    return ([self isRecognisedType] && _address && _userName);
}

-(bool) hasAction:(NSString*)actionStr{
    return (_actionType && [_actionType caseInsensitiveCompare:actionStr] == NSOrderedSame);
}

-(bool) hasSecondaryAction:(NSString*)actionStr{
    return (_secondaryAction && [_secondaryAction caseInsensitiveCompare:actionStr] == NSOrderedSame);
}

#pragma mark Helper methods
-(NSInteger) getHypTypeFromString:(NSString*)typeStr{NSUInteger type = HYPERVISOR_UNKNOWN;
    if (typeStr && [typeStr caseInsensitiveCompare:@"XenServer"] == NSOrderedSame){
        type = HYPERVISOR_XEN;
    }
    else if (typeStr && [typeStr caseInsensitiveCompare:@"AmazonEC2"] == NSOrderedSame){
        type = HYPERVISOR_EC2;
    }
    else if (typeStr && [typeStr caseInsensitiveCompare:@"Amazon"] == NSOrderedSame){    // bug in previous version used this
        type = HYPERVISOR_EC2;
    }
    else if (typeStr && [typeStr caseInsensitiveCompare:@"CloudStack"] == NSOrderedSame){
        type = HYPERVISOR_CLOUDSTACK_NATIVE;
    }
    return type;
}

@end
