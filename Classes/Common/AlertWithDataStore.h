//
//  AlertWithDataStore.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#define kUITextFieldHeight 30.0
#define kUITextFieldXPadding 10.0
#define kUITextFieldYPadding 5.0
#define kUIAlertOffset 60.0

//#define kUITextFieldHeight 30.0
//#define kUITextFieldXPadding 12.0
//#define kUITextFieldYPadding 10.0
//#define kUIAlertOffset 100.0

@interface AlertWithDataStore : UIAlertView {
    NSMutableDictionary *metaData;
    UITextField *textField;
    BOOL layoutDone;
    BOOL isPreiOS5;
}

- (id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate 
  cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...;
- (void) addMetaDataWithKey:(NSString *)key andValue:(NSString *)value;
- (NSString *) metaDataForKey:(NSString *)key;
-(NSString*) textString;
-(UITextField*) textField;

@end
