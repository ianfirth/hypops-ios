//
//  HypervisorConnection.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


@interface HypervisorConnectionDefinition : NSObject{
    NSString* _name;
    NSString* _address;
    NSString* _userName;
    NSString* _password;NSUInteger  _hypervisorType;
    NSString* _actionType;
    NSString* _hypervisorTypeString;
    NSString* _secondaryAction;
}

/**
 * Initialize the definition from a dictionary of parameters
 *   The Dictionary can have any of the following (case insensitve) strings as keys
 *     name
 *     address
 *     username
 *     password
 *     type
 *
 *  If there are any missing the associated properties will have default values (i.e. nil or 0)
 */
-(id) initWithParams:(NSDictionary*)theParams;

/*
 * returns true if the connection type is valid
 */
-(bool) isRecognisedType;

/*
 * returns true if the connection has a username and an address and is a recognised type
 */
-(bool) isSuitableForLaunch;

/*
 * returns true if the connection has the specified actiontype
 */
-(bool) hasAction:(NSString*)actionStr;

/*
 * returns true if the connection has the specified secondaryActiontype
 */
-(bool) hasSecondaryAction:(NSString*)actionStr;

/**
 * The name of the hypervisor connection
 */
@property (readonly) NSString* name;

/**
 * The address of the hypervisor connection
 */
@property (readonly) NSString* address;

/**
 * The userName of the hypervisor connection
 *   This strictly might not be the userName, it might be some API key, but it should 
 *   represent the thing that identifies the user connecting
 */
@property (readonly) NSString* userName;

/**
 * The password of the hypervisor connection
 *   This strictly might not be the password, it might be some API key, but it should
 *   represent the thing that holds something to validate that the user connecting is
 *   the user that they is asserting that they are.
 */
@property (readonly) NSString* password;

/**
 * The type of the hypervisor connection
 */
@property (readonly)NSUInteger  hypervisorType;

@property (readonly) NSString* hypervisorTypeString;

/**
 * defines a secondary action that can be made on the connection (like the main action might be create and the secon one might be launch too).
 */
@property (readonly) NSString* secondaryAction;
@end
