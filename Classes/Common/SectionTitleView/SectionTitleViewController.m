//
//  SectionTitleViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "SectionTitleViewController.h"

@interface SectionTitleViewController ()

@end

@implementation SectionTitleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString*) theTitle{
    return titleLabel.text;
}

- (void) setTheTitle:(NSString *)theTitle{
    [titleLabel setText:theTitle];
    [[self view] setNeedsDisplay];
}

- (NSString*) theDescription{
    return descriptionLabel.text;
}

- (void) setTheDescription:(NSString *)theDescription{
    [descriptionLabel setText:theDescription];
    [[self view] setNeedsDisplay];
}

- (UIImage*) theImage{
    return imageView.image;
}

- (void)setTheImage:(UIImage *)theImage{
    [imageView setImage:theImage];
    [[self view] setNeedsDisplay];
}

@end
