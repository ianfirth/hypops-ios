//
//  CommonUIStaticHelper.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CommonUIStaticHelper.h"
#import "RootViewController_ipad.h"

@implementation CommonUIStaticHelper

// displays the specified view in the detail area if on the ipad or the main area if on the iPhone
+ (void) displayNextView:(UIViewController *)controller usingNavigationController:(UINavigationController *)navCon andRootViewController:(UIViewController*)rootController{
    
    if (controller && navCon){
        // get the controller at the root of the provided navigation controller
        
        // use detail view for ipad
        if ([rootController respondsToSelector:@selector(detailViewNavigationController)]){
            UINavigationController *dvController = [rootController performSelector:@selector(detailViewNavigationController)];
            [RootViewController_ipad setDetailViewRootController:controller onNavigationController:dvController];
        }
        else{
            [navCon pushViewController:controller animated:YES];
        }
    }
    else{
        NSLog(@"Cant push null controller onto navigationCOntroller or vice-versa");
    }
}

// displays the controller in the master (list) area and if there is a detail view associated
// and there is a detail view area available it is displayed too
+ (void) displayMasterViewAndDetail:(UIViewController<DetailViewProtocol> *)controller usingNavigationController:(UINavigationController *)navCon{

    // get the controller at the root of the provided navigation controller
    UIViewController *rootController = [[navCon viewControllers] objectAtIndex:0];
    
    // use detail view for ipad
    if ([rootController respondsToSelector:@selector(detailViewNavigationController)]){
        UINavigationController *dvController = [rootController performSelector:@selector(detailViewNavigationController)];
        
        UIViewController *detailController = [controller detailContentView];
        if (detailController){
            [RootViewController_ipad setDetailViewRootController:detailController onNavigationController:dvController];
        }
    }

    // display the main master view
    [navCon pushViewController:controller animated:YES]; 
}
@end
