//
//  ImageBuilder.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ImageBuilder.h"

typedef enum {
    LOCATION_CENTER,         
    LOCATION_BOTTOMRIGHT,    
} overlayLocation;

@interface ImageBuilder ()
+ (UIImage *)buildImageFromImageNamed:(NSString *)image withOverlayImageName:(NSString *)overlayImageName;
+ (UIImage *)buildImageFromImage:(UIImage *)image withOverlayImageName:(NSString *)overlayImageName overlayLocation:(overlayLocation)location;
+ (UIImage *) makeImageFromName:(NSString *)name;
+ (void)drawNewSashInRect:(CGRect)rect withContext:(CGContextRef)myContext;
@end

@implementation ImageBuilder

/* This is a collection of UI static methods that provide images 
 */

+ (UIImage *)AddNewSashtoImage:(UIImage*)baseImage{
    return [ImageBuilder buildImageWithNewNewSashFromImage:baseImage];
}

+ (UIImage *)deleteImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"delete"];
}

+ (UIImage *)shareImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"share"];
}

+ (UIImage *)snapshotImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"snapshot"];
}

+ (UIImage *)snapshotRevertImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"snapshot_revert"];
}

+ (UIImage *)securityImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"security"];
}

+ (UIImage *)graphImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"graph"];
}

+ (UIImage *)vMImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_vm"];
}

+ (UIImage *)vMImageAlert{
    UIImage *result = [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_vm"];
    return [ImageBuilder buildImageFromImage:result withOverlayImageName:@"icon_alert" overlayLocation:LOCATION_BOTTOMRIGHT];
}

+ (UIImage *)vMImageDestroyed{
    UIImage *result = [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_vm"];
    return [ImageBuilder buildImageFromImage:result withOverlayImageName:@"icon_destroyed" overlayLocation:LOCATION_CENTER];
}


+ (UIImage *)vMImageStarted{
    UIImage *result = [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_vm"];
    return [ImageBuilder buildImageFromImage:result withOverlayImageName:@"vm_Running" overlayLocation:LOCATION_BOTTOMRIGHT];
}

+ (UIImage *)vMImageStopped{
    UIImage *result = [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_vm"];
    return [ImageBuilder buildImageFromImage:result withOverlayImageName:@"vm_Stopped" overlayLocation:LOCATION_BOTTOMRIGHT];
}

+ (UIImage *)vMImageTerminated{
    UIImage *result = [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_vm"];
    return [ImageBuilder buildImageFromImage:result withOverlayImageName:@"vm_terminated" overlayLocation:LOCATION_BOTTOMRIGHT];
}

+ (UIImage *)vMImagePaused{
    UIImage *result = [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_vm"];
    return [ImageBuilder buildImageFromImage:result withOverlayImageName:@"vm_Paused" overlayLocation:LOCATION_BOTTOMRIGHT];
}

+ (UIImage *)vMImageSuspended{
    UIImage *result = [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_vm"];
    return [ImageBuilder buildImageFromImage:result withOverlayImageName:@"vm_suspended" overlayLocation:LOCATION_BOTTOMRIGHT];
}

+ (UIImage *)vMTaggedImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_vm_tagged"];
}

+ (UIImage *)vMTemplateImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_template"];
}

+ (UIImage *)hostImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_host"];
}

+ (UIImage *)networkImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_network"];
}

+ (UIImage *)storageImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_storage"];
}

+ (UIImage *)zoneImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_zone"];
}

+ (UIImage *)flagUSAImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"flag_usa"];
}

+ (UIImage *)flagAustraliaImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"flag_australia"];
}

+ (UIImage *)flagEUImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"flag_eu"];
}

+ (UIImage *)flagBrazilImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"flag_brazil"];
}

+ (UIImage *)flagJapanImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"flag_japan"];
}

+ (UIImage *)flagSingaporeImage{
    return [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"flag_singapore"];
}

+ (UIImage *)hostImageUp{
    UIImage *result = [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_host"];
    return [ImageBuilder buildImageFromImage:result withOverlayImageName:@"icon_arrowUp" overlayLocation:LOCATION_BOTTOMRIGHT];
}

+ (UIImage *)hostImageDown{
    UIImage *result = [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_Host"];
    return [ImageBuilder buildImageFromImage:result withOverlayImageName:@"icon_arrowDown" overlayLocation:LOCATION_BOTTOMRIGHT];
}

+ (UIImage *)hostImageAlert{
    UIImage *result = [ImageBuilder buildImageFromImageNamed:@"icon_base" withOverlayImageName:@"icon_Host"];
    return [ImageBuilder buildImageFromImage:result withOverlayImageName:@"icon_alert" overlayLocation:LOCATION_BOTTOMRIGHT];
}


#pragma mark -
#pragma mark Private
+ (UIImage *) makeImageFromName:(NSString *)name{
    
    // for some reason the @2x images are not being loaded automatically so 
    // I am manually adding the logic for now and falling back to the built in logic if this fails.
    // I have no idea why the @2x images are not being loaded by default at present.
    UIImage *fallbackOverlay = [UIImage imageNamed:name];

    if ([UIScreen mainScreen].scale > 1.0) 
    { 
        name = [NSString stringWithFormat:@"%@@2x",name];
    } 
    
    // if the image did not load try adding the .png on the end, this can be required for earlier OS versions
    UIImage *overlay = [UIImage imageNamed:name];
    if (!overlay){
        name = [NSString stringWithFormat:@"%@.png",name];
        overlay = [UIImage imageNamed:name];
     }
    
    if (!overlay){
        return fallbackOverlay;
    }
    
    return overlay;
}

+ (UIImage *)buildImageFromImage:(UIImage *)image withOverlayImageName:(NSString *)overlayImageName{
    return [ImageBuilder buildImageFromImage:image withOverlayImageName:overlayImageName overlayLocation:LOCATION_CENTER];
}

+ (UIImage *)buildImageFromImageNamed:(NSString *)image withOverlayImageName:(NSString *)overlayImageName{
    UIImage *icon = [ImageBuilder makeImageFromName:image];
    return [ImageBuilder buildImageFromImage:icon withOverlayImageName:overlayImageName overlayLocation:LOCATION_CENTER];
}


+ (UIImage *)buildImageFromImageNamed:(NSString *)image withOverlayImageName:(NSString *)overlayImageName overlayLocation:(overlayLocation)location{
    UIImage *icon = [ImageBuilder makeImageFromName:image];
    return [ImageBuilder buildImageFromImage:icon withOverlayImageName:overlayImageName overlayLocation:location];
}

+ (UIImage *)buildImageFromImage:(UIImage *)icon withOverlayImageName:(NSString *)overlayImageName overlayLocation:(overlayLocation)location{

    UIImage *overlay = [ImageBuilder makeImageFromName:overlayImageName];
    
    CGSize iconSize = [icon size];
    CGSize overlaySize = [overlay size];
    CGRect iconBoundingBox = CGRectMake (0.0, 0.0 ,iconSize.width, iconSize.height);
    CGRect overlayBoundingBox;
    
    switch (location) {
        case LOCATION_CENTER:{
            float xdiff = (iconSize.width - overlaySize.width) /2.0; 
            float ydiff = (iconSize.height - overlaySize.height) /2.0; 
            overlayBoundingBox = CGRectMake (xdiff,ydiff, overlaySize.width, overlaySize.height);
            break;
        }
        case LOCATION_BOTTOMRIGHT:{
            float xdiff = (iconSize.width - overlaySize.width);  
            overlayBoundingBox = CGRectMake (xdiff, 0,overlaySize.width, overlaySize.height);
            break;
        }
    }
        
    CGContextRef myBitmapContext = NULL;
    
    CGContextRef    context = NULL;
    CGColorSpaceRef colorSpace;NSUInteger          bitmapBytesPerRow;
    
    bitmapBytesPerRow   = (iconSize.width * 4);
    colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // in iOS 4.0 and later the bitmap data can be managed inside the Context so no need to allocate memory manually
    myBitmapContext = CGBitmapContextCreate (NULL,
                                             iconSize.width,
                                             iconSize.height,
                                             8,       // bits per component
                                             bitmapBytesPerRow,
                                             colorSpace,
                                             kCGBitmapAlphaInfoMask & kCGImageAlphaPremultipliedLast);
   
    
    
    enum {
        kCGBitmapAlphaInfoMask = 0x1F,
        kCGBitmapFloatComponents = (1 << 8),
        
        kCGBitmapByteOrderMask = 0x7000,
        kCGBitmapByteOrderDefault = (0 << 12),
        kCGBitmapByteOrder16Little = (1 << 12),
        kCGBitmapByteOrder32Little = (2 << 12),
        kCGBitmapByteOrder16Big = (3 << 12),
        kCGBitmapByteOrder32Big = (4 << 12)
    };
    typedef uint32_t CGBitmapInfo;

    
    CGColorSpaceRelease( colorSpace );
   
    UIImage *result;
    
    if (myBitmapContext){
    
        CGContextSetRGBFillColor (myBitmapContext, 0 ,0, 0, 0);  
        CGContextSetBlendMode(myBitmapContext,   kCGBlendModeClear);
        CGContextFillRect (myBitmapContext, iconBoundingBox);
        CGContextSetBlendMode(myBitmapContext,   kCGBlendModeNormal);
        CGContextDrawImage(myBitmapContext, iconBoundingBox, icon.CGImage);
        CGContextDrawImage(myBitmapContext, overlayBoundingBox, overlay.CGImage);
        CGImageRef myImageRef = CGBitmapContextCreateImage (myBitmapContext);
        result = [UIImage imageWithCGImage: myImageRef];
        CGImageRelease(myImageRef);
        
    }

    CGContextRelease(myBitmapContext);
    CGContextRelease(context);

    return result;
}


// Take an image and add an 'new' sash to it.
+ (UIImage *)buildImageWithNewNewSashFromImage:(UIImage *)icon{
    
    CGSize iconSize = [icon size];
    CGRect iconBoundingBox = CGRectMake (0.0, 0.0 ,iconSize.width, iconSize.height);

    CGContextRef myBitmapContext = NULL;
    
    CGContextRef    context = NULL;
    CGColorSpaceRef colorSpace;NSUInteger          bitmapBytesPerRow;
    
    bitmapBytesPerRow   = (iconSize.width * 4);
    colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // in iOS 4.0 and later the bitmap data can be managed inside the Context so no need to allocate memory manually
    myBitmapContext = CGBitmapContextCreate (NULL,
                                             iconSize.width,
                                             iconSize.height,
                                             8,       // bits per component
                                             bitmapBytesPerRow,
                                             colorSpace,
                                             kCGBitmapAlphaInfoMask & kCGImageAlphaPremultipliedLast);
    
    CGColorSpaceRelease( colorSpace );
    
    UIImage *result;
    
    if (myBitmapContext){
        
        CGContextSetRGBFillColor (myBitmapContext, 0 ,0, 0, 0);
        CGContextSetBlendMode(myBitmapContext,   kCGBlendModeClear);
        CGContextFillRect (myBitmapContext, iconBoundingBox);
        CGContextSetBlendMode(myBitmapContext,   kCGBlendModeNormal);
        CGContextDrawImage(myBitmapContext, iconBoundingBox, icon.CGImage);
    
        // perform extra draw here
        // need to make sure that it is done as a percent of the overall image as scaling gets applied afterwards
        // to get it to fit in the image on the screen.
        // would be nice if it was added after scaling so that it was drawn in a 1:1
        [ImageBuilder drawNewSashInRect:CGRectMake(0,0,iconSize.width, iconSize.height) withContext:myBitmapContext];
        
        CGImageRef myImageRef = CGBitmapContextCreateImage (myBitmapContext);
        result = [UIImage imageWithCGImage: myImageRef];
        CGImageRelease(myImageRef);
        
    }
    
    CGContextRelease(myBitmapContext);
    CGContextRelease(context);
    
    return result;
}

+ (void)drawNewSashInRect:(CGRect)rect withContext:(CGContextRef)myContext
{
    // if there is no rect then do nothing as its pointless.
    if (rect.size.height == 0 || rect.size.width == 0){
        return;
    }

    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = nil;
    
    @try {

    float sashHeight = 10;
    float sashWidth =rect.size.width;
    float fontsize = 9;
    UIFont *font= [UIFont systemFontOfSize:fontsize];
    NSString* string = @"New";
        
    NSDictionary *attributes = @{NSFontAttributeName: font};
    CGSize stringSize = [string sizeWithAttributes:attributes];
    
    CGContextSaveGState(myContext);

    float x = rect.size.height/4;
    float y = rect.size.height/1.5;

    float x2 = rect.size.height;
    float y2 = sashHeight/2;

    CGAffineTransform transform = CGAffineTransformMakeTranslation(x,y);   // Center it in top right
    // this should rotate arround the center of the sash 45 degrees
    transform = CGAffineTransformTranslate(transform,x2/2, y2);
    transform = CGAffineTransformRotate(transform, -45.0 * M_PI/180.0);
    transform = CGAffineTransformTranslate(transform,-x2/2,-y2);
        
    CGContextConcatCTM(myContext, transform);
    

    
    // Create the gradient
    CGColorRef startColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.6 alpha:1.0].CGColor;
    CGColorRef endColor = [UIColor colorWithRed:0.1 green:0.1 blue:1.0 alpha:1.0].CGColor;
    NSArray *colors = [NSArray arrayWithObjects:(__bridge id)startColor, (__bridge id)endColor, nil];
    CGFloat locations[] = { 0.0, 1.0 };
    gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    
    CGContextDrawLinearGradient(myContext, gradient, CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y), CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y + sashHeight), 0);
    
    // Create text
    
    CGContextSetRGBFillColor(myContext, 1.0, 1.0, 1.0, 1.0);
    CGContextSetLineWidth(myContext, 2.0);

    CGContextSetTextDrawingMode(myContext, kCGTextFill); // This is the default
    [[UIColor blackColor] setFill]; // This is the default
    [string drawAtPoint:CGPointMake((sashWidth - stringSize.width)/2 + 1, 3)
                   withAttributes:@{NSFontAttributeName:font
                                    }];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    }
    @finally {
        // Clean up
        if (gradient){
            CGGradientRelease(gradient);
        }
        CGColorSpaceRelease(colorSpace);
        
        CGContextRestoreGState(myContext);
    }

}
@end
