//
//  ProgressAlert.m
//  UICommonControls
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ProgressAlert.h"

#define minimumWidth 200
#define controlHeight 125

@implementation ProgressAlert
@synthesize parameterMap;

- (id)initWithTitle:(NSString *)title delegate:(id)delegate parameters:(NSDictionary*)parameters{
    self = [super initWithTitle:title message:nil delegate:delegate cancelButtonTitle:nil
              otherButtonTitles:nil, nil];
    if (self) {
        parameterMap = parameters;
        titleLabel = [[UILabel alloc ] initWithFrame:CGRectZero];
        [titleLabel setText:title];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setAdjustsFontSizeToFitWidth:NO];
        
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        /// remove any existing views
        for(UIView *view in self.subviews){
            [view removeFromSuperview];
        }
        
        [self setBackgroundColor:[UIColor clearColor]];
        
        layoutDone = NO;
        
    }
    return self;    
}

- (id)initWithTitle:(NSString *)title delegate:(id)delegate{
    return [self initWithTitle:title delegate:delegate parameters:nil];
}


-(void) drawRect:(CGRect)rect{
    // define the rpund rect parameters
    float strokeWidth = 1;
    UIColor *strokeColor = [UIColor grayColor];
    UIColor *rectColor = [UIColor darkGrayColor];
    CGFloat radius = 15;
    
    // draw it
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, strokeWidth);
    CGContextSetStrokeColorWithColor(context, strokeColor.CGColor);
    CGContextSetFillColorWithColor(context, rectColor.CGColor);
    CGContextSetAlpha(context, 0.8);
    
    CGRect rrect = self.bounds;
    

    CGFloat width = CGRectGetWidth(rrect);
    CGFloat height = CGRectGetHeight(rrect);
    
    // Make sure corner radius isn't larger than half the shorter side
    if (radius > width/2.0)
        radius = width/2.0;
    if (radius > height/2.0)
        radius = height/2.0;    
    
    CGFloat minx = CGRectGetMinX(rrect);
    CGFloat midx = CGRectGetMidX(rrect);
    CGFloat maxx = CGRectGetMaxX(rrect);
    CGFloat miny = CGRectGetMinY(rrect);
    CGFloat midy = CGRectGetMidY(rrect);
    CGFloat maxy = CGRectGetMaxY(rrect);
    CGContextMoveToPoint(context, minx, midy);
    CGContextAddArcToPoint(context, minx, miny, midx, miny, radius);
    CGContextAddArcToPoint(context, maxx, miny, maxx, midy, radius);
    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
    CGContextAddArcToPoint(context, minx, maxy, minx, midy, radius);
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathFillStroke); 
}

/*
 * Override layoutSubviews to correctly handle the UITextField
 */
- (void)layoutSubviews {
    [super layoutSubviews];
    
    // Perform layout of subviews just once
//    if(!layoutDone) {
    /// remove any existing views
        for(UIView *view in self.subviews){
            [view removeFromSuperview];
        }

        [self setBackgroundColor:[UIColor clearColor]];
         // set up the title lable
        UIFont* labelFont = [UIFont fontWithName:@"Helvetica" size:24];
        NSDictionary *attributes = @{NSFontAttributeName: labelFont};
        CGSize labelSize = [[titleLabel text] sizeWithAttributes:attributes];
    
        if (labelSize.width < minimumWidth){
            float additionalWidth = minimumWidth - labelSize.width;
            if (additionalWidth < 20){
                additionalWidth = 20;
            }
            labelSize.width += additionalWidth;
        }
        
        [titleLabel setFrame:CGRectMake(0,controlHeight - labelSize.height - 10, labelSize.width, labelSize.height)];
        [titleLabel setFont:labelFont];
    
        [titleLabel setTextAlignment:NSTextAlignmentCenter];

        // setup the spinner
        float spinerWidth = 50;
        float spinnerx = (labelSize.width - spinerWidth) / 2;
        float spinnery = (titleLabel.frame.origin.y - spinerWidth )/2;
        [spinner setFrame:CGRectMake(spinnerx, spinnery , spinerWidth, spinerWidth)];
       
        [self addSubview:spinner];
        [self addSubview:titleLabel];

        [self setBounds:CGRectMake(0,0, labelSize.width, controlHeight)];
        layoutDone = YES;
//    }
}

-(void) show{
    [spinner startAnimating];
    [super show];
}

-(void) dismissWithClickedButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated{
    [spinner stopAnimating];
    [super dismissWithClickedButtonIndex:buttonIndex animated:animated];
}

@end
