//
//  RelatedListOverlayView.m
//  UICommonControls
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#define degreesToRadians(x) (M_PI * x / 180.0)

#import "RelatedListOverlayView.h"
@interface RelatedListOverlayView (private)
-(void)drawArrowinContext:(CGContextRef)context from:(CGPoint)start to:(CGPoint)end;
-(void)drawArrowinContext:(CGContextRef)context from:(CGPoint)start to:(CGPoint)end color:(CGColorRef)colorRef;
@end

@implementation RelatedListOverlayView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        // Initialization code
     }
    return self;
}


-(void)setRelationshipDataSource:(NSObject<TableViewRelationshipDataSource>*)dataSource{
    relationshipDataSource = dataSource;
}

-(void)drawArrowinContext:(CGContextRef)context from:(CGPoint)start to:(CGPoint)end color:(CGColorRef)colorRef{
    
    float width = 20;
    float height = end.y-start.y;
        
    float reduceFactor = width/height;
    float yshorter = 25 - (20 * reduceFactor);
    
    // move the endPoint so that once the arrow added onto the end, then it touches the blob for
    // the begining of the next one.
    
    // shorten the end by less the shorter the height as the arrow will be at a steper angle
    end = CGPointMake(end.x+(20 * reduceFactor), end.y-yshorter);
    height = end.y-start.y;

    CGContextSetLineWidth(context, 1.0);
    CGContextSetStrokeColorWithColor(context,colorRef);
    CGContextSetFillColorWithColor(context,colorRef);
    CGContextMoveToPoint(context, start.x, start.y);
    // draw startpoint circle
    CGRect rectangle = CGRectMake(start.x-6,start.y-6,12,12);
    CGContextFillEllipseInRect(context, rectangle);
    CGContextFillPath(context);

    // draw curve to end point
    CGContextSetLineWidth(context, 4.0);
    CGContextSetStrokeColorWithColor(context,colorRef);
    CGContextMoveToPoint(context, start.x, start.y);
    CGContextAddQuadCurveToPoint(context, start.x+width, start.y+(height/2), end.x, end.y); 
    CGContextStrokePath(context);
    
    // draw the arrow arround zero, then rotate and move
    CGPoint a = CGPointMake(0, 20);
    CGPoint b = CGPointMake(-5,-0);
    CGPoint c = CGPointMake(5,-0);
    
    UIColor *color = [UIColor colorWithCGColor:colorRef];;
    [color set];
    // add arrow head here
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:a];
    [path addLineToPoint:b];
    [path addLineToPoint:c];
    [path closePath]; // Implicitly does a line between p3 and p1
    
    float headAngle = (width/height) * 60;
    CGAffineTransform transform1 = CGAffineTransformMakeRotation(degreesToRadians(headAngle));
    CGAffineTransform transform2 = CGAffineTransformMakeTranslation(end.x, end.y);
    CGAffineTransform transform = CGAffineTransformConcat(transform1, transform2);
    
    [path applyTransform:transform];
    [path fill]; // If you want it filled, or...
    //[path stroke]; // ...if you want to draw the outline.
}

-(void)drawArrowinContext:(CGContextRef)context from:(CGPoint)start to:(CGPoint)end{

    // draw the shadow version
    CGPoint shadowStart = CGPointMake(start.x-5, start.y-2);
    CGPoint shadowEnd = CGPointMake(end.x-5, end.y-2);   
    [self drawArrowinContext:context from:shadowStart to:shadowEnd color:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2].CGColor];
    CGContextStrokePath(context);

    // draw the real one
    [self drawArrowinContext:context from:start to:end color:[UIColor blueColor].CGColor];    
  
}

-(BOOL) isRect:(CGRect)innerRect PartiallyContainedWithinRect:(CGRect)outerRect{
    return (innerRect.origin.x >= outerRect.origin.x ||
        innerRect.origin.y >= outerRect.origin.y ||
        innerRect.origin.x + innerRect.size.width <= outerRect.origin.x + outerRect.size.width ||
        innerRect.origin.y + innerRect.size.height <= outerRect.origin.y + outerRect.size.height);
        
}

- (void)drawRect:(CGRect)rect {        
    // dont draw anything if there is no data provided
    CGContextRef context = UIGraphicsGetCurrentContext();    

    if (!relationshipDataSource){
         return;
    } else {NSUInteger  allPointCount = [relationshipDataSource getAllPointCount];
        CGPoint start = [relationshipDataSource getAllPointStartPoint];NSUInteger  posy = start.y;
        for (int i= 0; i < allPointCount; i++){            
            // draw startpoint circle
            CGRect rectangle = CGRectMake(start.x-10,posy -10,20,20);
            // dont draw if outside the rect being asked for
            if ([self isRect:rectangle PartiallyContainedWithinRect:rect]){
                CGContextSetLineWidth(context, 1.0);
                CGContextSetRGBStrokeColor(context, 0, 0, 255, 0.5);
                // Draw a circle (border only)
                CGContextStrokeEllipseInRect(context, rectangle);
            }
            
            if (i != allPointCount){
                float pointInterval = [relationshipDataSource getAllPointIntervalForPoint:i];
                posy = posy + pointInterval;
            }
        }NSUInteger  pastPointsCount = [relationshipDataSource getPastPointCount];
        
        if (pastPointsCount > 1){   // must alwasys be a start and an end otherwise no point
            CGPoint lastPoint = [relationshipDataSource pointAt:0];
            for (int i=1; i <pastPointsCount ; i++){
                CGPoint currentPoint = [relationshipDataSource pointAt:i];
                CGRect arrowRect = CGRectMake(lastPoint.x, lastPoint.y, currentPoint.x-lastPoint.x, currentPoint.y-lastPoint.y);
                if ([self isRect:arrowRect PartiallyContainedWithinRect:rect]){
                    [self drawArrowinContext:context from:lastPoint to:currentPoint];
                }
                lastPoint = currentPoint;
            }
        }
    }
}

@end
