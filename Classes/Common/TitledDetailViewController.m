//
//  TitledDetailViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "TitledDetailViewController.h"
#import "ShadowView.h"
#import "GradientView.h"

@interface  TitledDetailViewController ()
  - (NSString *) nameForRootObject;
  - (NSString *) descriptionForRootObject;
  - (void) refreshView;
@end 

@implementation TitledDetailViewController

@synthesize hypConnID, objectNameLabel, objectDescriptionLabel, objectImageView, shadowView, extentionViewController;

static CGRect nameLabelLocation;
static CGRect descriptionLabelLocation;

-(BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return [extentionViewController shouldAutorotateToInterfaceOrientation:toInterfaceOrientation];
}

// this one is for iOS6 (the shouldAutoRotateToInterfaceOrientation is for pre iOS 6 only).
- (BOOL)shouldAutorotate{
    return [extentionViewController shouldAutorotate];
}

-(NSUInteger)supportedInterfaceOrientations
{
    return [extentionViewController supportedInterfaceOrientations];
}

- (void)viewDidLayoutSubviews{
    //TODO remove this [extentionViewController viewDidLayoutSubviews];
    [super viewDidLayoutSubviews];
}

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypConnection
                     rootObjectRef:(NSString *)rootObjectIdentifier
               rootObjectPredicate:(NSPredicate *) theRootObjectPredicate
                     hypObjectType:(NSInteger)theHypObjectType
                  namePropertyName:(NSString *)theNameProperty
           descriptionPropertyName:(NSString *)theDescriptionPropertyName
                       displayMode:(GeneralDisplayMode)theMode
                           tabName:(NSString *)tabName
                   tabBarImageName:(NSString *)imageName
                             image:(UIImage *)mainImage
           extentionViewController:(UIViewController<Identifyable,RefreshablePage> *)theExtentionViewController
{
    return [self initWithHypervisorConnection:hypConnection rootObjectRef:rootObjectIdentifier rootObjectPredicate:theRootObjectPredicate hypObjectType:theHypObjectType namePropertyName:theNameProperty descriptionPropertyName:theDescriptionPropertyName displayMode:theMode tabName:tabName tabBarImageName:imageName image:mainImage extentionViewController:theExtentionViewController titlePressedSelector:nil selectorOnObject:nil];
}

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypConnection
                     rootObjectRef:(NSString *)rootObjectIdentifier
               rootObjectPredicate:(NSPredicate *) theRootObjectPredicate
                     hypObjectType:(NSInteger)theHypObjectType
                  namePropertyName:(NSString *)theNameProperty
           descriptionPropertyName:(NSString *)theDescriptionPropertyName
                       displayMode:(GeneralDisplayMode)theMode
                           tabName:(NSString *)tabName
                   tabBarImageName:(NSString *)imageName 
                             image:(UIImage *)mainImage 
           extentionViewController:(UIViewController<Identifyable,RefreshablePage> *)theExtentionViewController
              titlePressedSelector:(SEL)titleSelector
selectorOnObject:(id) selctorObject{
    self = [super init];
    if (self){
        // on iphone obey the mode, but on iPad always display name and description
        // as there is plenty of display room
        UIDevice *device = [UIDevice currentDevice];
        if([device userInterfaceIdiom] == UIUserInterfaceIdiomPhone )
        {
            mode = theMode;
        }
        else
        {
            if (theDescriptionPropertyName){
                mode = DISPLAYMODE_NAMEDESCRIPTION;
            }
            mode = theMode;
        }
        
        // store the extentionView
        [self setExtentionViewController:theExtentionViewController];
        [self addChildViewController:theExtentionViewController];
        _image = mainImage;
        //this is the label on the tab button itself
        self.title = tabName;
        //Set the image for the tab bar
        self.tabBarItem.image = [UIImage imageNamed:imageName];
        _rootObjectRef = rootObjectIdentifier;
        rootHypType = theHypObjectType;
        [self setHypConnID:[hypConnection connectionID]];
        hypervisorType = [hypConnection hypervisorConnectionType];
        _rootObjectPredicate = theRootObjectPredicate;
        namePropertyName = [theNameProperty copy];
        descriptionPropertyName = [theDescriptionPropertyName copy];
        _titlePressesSelector = titleSelector;
        _selctorObject = selctorObject;
    }
    return self;
}

-(void)setImage:(UIImage *)image{
    _image = image;
    [self updatePage];
}

-(UIImage *)image{
    return _image;
}

-(NSString *)rootObjectRef{
    return _rootObjectRef;
}

-(NSPredicate *)rootObjectPredicate{
    return _rootObjectPredicate;
}

-(void)setRootObjectPredicate:(NSPredicate *)rootObjectPredicate andReference:(NSString *)rootObjectRef{
    _rootObjectPredicate = rootObjectPredicate;
    _rootObjectRef = rootObjectRef;
    [self ensureObjectTreeLoaded];
    [self updatePage];
}

-(HypervisorConnectionFactory *) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

+(void) setRightBarButtonsForNavigationController:(UINavigationController *)navigationController andButtons:(NSArray *)barButtonItems{
    // create a toolbar where we can place some buttons
    UIToolbar* toolbar = [[UIToolbar alloc]
                          initWithFrame:CGRectMake(0, 0, 100, 45)];  //possibly need to control this width properly
    [toolbar setBarStyle: UIBarStyleDefault];NSUInteger  toolBarWidth = 0;
    // create an array for the buttons
    NSMutableArray* buttons = [[NSMutableArray alloc] initWithCapacity:3];
    for (int i= 0 ; i <[barButtonItems count]; i++){
        UIBarButtonItem *bbi = [barButtonItems objectAtIndex:i];
        toolBarWidth += [bbi width];
        [buttons addObject:bbi];
        if (i< ([barButtonItems count] -1)){
            // create a spacer between the buttons
            UIBarButtonItem *spacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil
                                       action:nil];
            [buttons addObject:spacer];
            toolBarWidth += [spacer width];
        }
    }
    
    // put the buttons in the toolbar and release them
    [toolbar setItems:buttons animated:NO];
    
    // place the toolbar into the navigation bar
    NSArray *viewControllers = [navigationController viewControllers];
    UINavigationItem *navItem = [[viewControllers objectAtIndex:[viewControllers count]-1] navigationItem];
    
    //    CGRect toolBarFrame = [toolbar frame];
    //    toolBarFrame.size.width = toolBarWidth;
    //    [toolbar setFrame:toolBarFrame];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]
                                      initWithCustomView:toolbar ];
    navItem.rightBarButtonItem = barButtonItem;
}

+(void) setRightBarButtonForNavigationController:(UINavigationController *)navigationController andButton:(UIBarButtonItem *)barButton{
    // place the toolbar into the navigation bar
    NSArray *viewControllers = [navigationController viewControllers];
    UINavigationItem *navItem = [[viewControllers objectAtIndex:[viewControllers count]-1] navigationItem];
    [navItem setRightBarButtonItem:barButton];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void) viewWillDisappear:(BOOL)animated{
    HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
    
    [hypervisorConnection removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
    
    [hypervisorConnection addHypervisorConnectionDelegate:self];
    
    if (![hypervisorConnection isAutoUpdateEnabled]){
        UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc]
                                          initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                          target:self
                                          action:@selector(refreshView)];
        [refreshButton setStyle:UIBarButtonItemStyleBordered];
        
        [TitledDetailViewController setRightBarButtonForNavigationController:[self navigationController] andButton:refreshButton];
        
    } else{
        [TitledDetailViewController setRightBarButtonForNavigationController:[self navigationController] andButton:nil];
    }
    
    [self ensureObjectTreeLoaded];
    [[self view] setNeedsLayout];
    [[self view] setNeedsDisplay];
    [[self view] layoutSubviews];
    [super viewDidAppear:animated];
    [self updatePage];
    
}

-(BOOL)shouldAutomaticallyForwardRotationMethods{
    return YES;
}

- (BOOL)shouldAutomaticallyForwardAppearanceMethods{
    return YES;
}

-(NSString *) nameForRootObject{
    // get the object from the hypervisor cache
    // use the namepropertyname to retreive the data and return the value
    HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
    NSArray *objects = [hypervisorConnection hypObjectsForType:rootHypType withCondition:[self rootObjectPredicate]];
    if ([objects count]>0){
        id object = [objects objectAtIndex:0];
        return [object valueForKey:namePropertyName];
    }
    
    return nil;
}

-(NSString *) descriptionForRootObject{
    // get the object from the hypervisor cache
    // use the discriptionpropertyname to retreive the data and return the value
    HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
    NSArray *objects = [hypervisorConnection hypObjectsForType:rootHypType withCondition:[self rootObjectPredicate]];
    if ([objects count]>0 && descriptionPropertyName != nil){
        id object = [objects objectAtIndex:0];
        return [object valueForKey:descriptionPropertyName];
    }
    
    return nil;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    // use frame width here
    float width = [[self view] frame].size.width;
    float height = [[self view] frame].size.height;
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    // todo remove again
    //[extentionViewController viewDidLoad];
    nameLabelLocation = CGRectMake(58.0, 19.0, width-68, 21.0);  
    descriptionLabelLocation = CGRectMake(10.0, 50.0, width-20, 60.0);  
    
    [self setShadowView:[[ShadowView alloc] init]];
    [[self shadowView] setFrame:CGRectMake(0,0,width,4)];
    [[self shadowView] setOpaque:NO];
    [[self shadowView] setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    // these elements are fixed in place
    [self setObjectNameLabel:[[UILabel alloc] initWithFrame:nameLabelLocation]];
    [[self objectNameLabel] setBackgroundColor: [UIColor clearColor]];
    [[self objectNameLabel] setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [self setObjectDescriptionLabel:[[UILabel alloc] initWithFrame:descriptionLabelLocation]];
    [[self objectDescriptionLabel] setBackgroundColor: [UIColor clearColor]];
    
    [[self objectDescriptionLabel] setNumberOfLines:3];
    [[self objectDescriptionLabel] setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
    [[self objectDescriptionLabel] setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    
    [self setObjectImageView:[[UIImageView alloc] initWithFrame:CGRectMake(10.0,9.0,40.0,40.0)]];
    
    GradientView *back = [[GradientView alloc] init];
    [back setEndColor:[UIColor colorWithRed:0.25 green:0.37 blue:0.57 alpha:1]];
    [back setStartColor:[UIColor colorWithRed:0.44 green:0.60 blue:0.78 alpha:1]];
    [back setGradientViewFillMode:GradientViewFillModeHalf];
    
    [back setFrame:CGRectMake(0,0,width,height)];
    [back setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [[self view] addSubview:back];
    
    [[self view] addSubview:objectNameLabel];
    [[self view] addSubview:objectDescriptionLabel];
    [[self view] addSubview:objectImageView];
    [[self view] addSubview:[extentionViewController view]];
    [[self view] addSubview:[self shadowView]];

    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    // pass the touches on to other contols
    [recognizer setCancelsTouchesInView:NO];
    CGPoint point = [recognizer locationOfTouch:0 inView:[self view]];
    float y = [[extentionViewController view] frame].origin.y;
    // only use the touch to change title if it is over the title label area
    if (point.y < y && _titlePressesSelector){
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        if ([_selctorObject respondsToSelector:_titlePressesSelector]){
            NSLog(@"------------------ title pressed -----------------------");
            [_selctorObject performSelector:_titlePressesSelector withObject:_rootObjectRef];
        }
#pragma clang diagnostic pop
    }
}

-(void) setTakeActionOnAppear:(NSNumber*)takeAction{
    if ([extentionViewController respondsToSelector:@selector(setTakeActionOnAppear:)]){
        [extentionViewController performSelector:@selector(setTakeActionOnAppear:) withObject:takeAction];
    }
}

-(void) updatePage{
    #warning need to implement title uopdate if needed
    
    [objectNameLabel setText:[self nameForRootObject]];
    
    NSString *object_description = nil;
    // only set the display description if it has been asked for
    if (mode == DISPLAYMODE_NAMEDESCRIPTION){
        object_description = [self descriptionForRootObject];
    }
    
    CGRect fullFrame = [[self view] frame];NSUInteger  moveUpBy = 0;
    if (mode != DISPLAYMODE_NOHEADER){
        if (object_description == nil || [object_description isEqual:@""]){
            moveUpBy = [objectDescriptionLabel frame].size.height - 5;   // leave 5 pixels padding if label not visible
            [[self objectDescriptionLabel] setHidden:YES];
        } else {
            [[self objectDescriptionLabel] setHidden:NO];
            [[self objectDescriptionLabel] setText:object_description];
            fullFrame.size.height -= ([objectDescriptionLabel frame].size.height - 5);  // description label height
        }
    }

    // this view is placed under the description regardless of description size.
    CGRect descrFrame = [objectDescriptionLabel frame];

    if (mode == DISPLAYMODE_NOHEADER){
        moveUpBy = descrFrame.origin.y + descrFrame.size.height;
        fullFrame.size.height += moveUpBy - 55;  // tabbar
    }

    float viewTop = descrFrame.origin.y + descrFrame.size.height - moveUpBy;
    fullFrame.origin.y = viewTop;
    fullFrame.size.height -= 55;  // tabbar
    
    [[extentionViewController view] setFrame:fullFrame];
    
    // this method comes from the controller conforming to the RefreshPage protocol
    [extentionViewController refreshPage];

    [[self shadowView] setFrame:CGRectMake(0,viewTop,[[self shadowView] frame].size.width,14)];
    
    // set the vm image
    if ([self image] != nil){
        [[self objectImageView] setImage:[self image]];
    }
    else{
        [[self objectImageView] setImage:[UIImage imageNamed:@"Blank"]];
    }
    
    [[self extentionViewController] setId:[self rootObjectRef]];

    [objectNameLabel setNeedsDisplay];
    [[self view] setNeedsDisplay];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [self setObjectImageView:nil];
    [super viewDidUnload];
}

- (void)dealloc {
    HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
    [hypervisorConnection removeHypervisorConnectionDelegate:self];
    namePropertyName = nil;
    descriptionPropertyName = nil;
    
}



-(void)ensureObjectTreeLoaded{
    HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
    
    // link in the UIDataload to make sure that the object has all its children
    NSArray *objects = [hypervisorConnection hypObjectsForType:rootHypType withCondition:[self rootObjectPredicate]];
    if ([objects count] >0){
        [hypervisorConnection setWalkTreeMode:YES];
        UIWaitForDataLoad *dataLoader = [[UIWaitForDataLoad alloc] initWithHypervisorConnection:hypervisorConnection];
        [dataLoader setUIWaitForDataLoadDelegate:self];
        [dataLoader waitForObjectTreeForObjectReference:[self rootObjectRef] objectType:rootHypType];
    }
}

- (void) refreshView{
    HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
    
    NSArray *objects = [hypervisorConnection hypObjectsForType:rootHypType withCondition:[self rootObjectPredicate]];
    if ([objects count] >0){
        id<HypObjReferencesProtocol> referencedObj = [objects objectAtIndex:0];     
        [hypervisorConnection clearHypObjectTreeWithRoot:referencedObj];
        [hypervisorConnection setWalkTreeMode:YES];
        UIWaitForDataLoad *dataLoader = [[UIWaitForDataLoad alloc] initWithHypervisorConnection:hypervisorConnection];
        [dataLoader setUIWaitForDataLoadDelegate:self];
        
        NSLog(@"Created dataloader to wait for data to be loaded for the next page");
        [dataLoader waitForObjectTreeForObjectReference:[self rootObjectRef] objectType:rootHypType];
    }
}

#pragma mark -
#pragma mark UIWaitForDownload delegate

- (void)dataLoaderFinished:(UIWaitForDataLoad *)dataLoader withResult:(BOOL)sucsess{
    NSLog(@"dataloader reported that it has loaded the required data.");
    
    HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
    
    NSError *err = [dataLoader connectionError];
    
    if (err){
        // if there was an error connecting do not do anyting.
        return;
    }
    
    [self updatePage];
    
    // also invalidate the extention page so it redraws now all the data is available
    if ([[self extentionViewController] isKindOfClass:[UITableViewController class]]){
        UITableViewController *controller = (UITableViewController *)[self extentionViewController];
        UITabBarController *tabBarController = [self tabBarController];
        // reload all tables in all views in the table view controller.
        if (tabBarController){
            NSArray *allTabbedControllers = [tabBarController viewControllers];
            for (UIViewController *vc in allTabbedControllers){
                if ([vc isKindOfClass:[TitledDetailViewController class]]){
                    UIViewController *extentionVC = [(TitledDetailViewController *)vc extentionViewController];
                    if ([extentionVC isKindOfClass:[UITableViewController class]]){
                        UITableView *tv = [(UITableViewController *)extentionVC tableView];
                        [tv reloadData];
                    }
                }
            }
        }
        UITableView *tv = [controller tableView];
        [tv reloadData];
    }
    [hypervisorConnection setWalkTreeMode:NO];
}


#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    if ([[self rootObjectRef] isEqualToString:objectRef]){
        [objectNameLabel setText:[self nameForRootObject]];
        // needs to re layout this stuff if description goes from nothing to something
        [objectDescriptionLabel setText:[self descriptionForRootObject]];
        
        if ([hypervisorConnection isAutoUpdateEnabled] && [[self extentionViewController] isKindOfClass:[UITableViewController class]]){
            UITableView *tv = (UITableView *)[[self extentionViewController] view];
            [tv reloadData];
        }
        
        // this is needed to make the title on the page appear if added
        // This method is defined by the RefreshabalePage protocol
#warning This will go when remove this class when all uses of it are finally removed
//        UIImage* image = [[self extentionViewController] currentTitleImage];
        UIImage* image = nil;
        if (image){
            [self setImage:image];
        }
    }
    [self updatePage];
}

@end
