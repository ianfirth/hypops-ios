//
//  RRDPlotViewController.h
//  hypOps
//
//  Created by Ian Firth on 20/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "DataSource.h"
#import <Foundation/Foundation.h>
#import "ProgressAlert.h"

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "RRDDisplayButtonDelegate.h"

@class CPTGraph;
@class CPTTheme;

@interface RRDPlotViewController : UIViewController < CPTPlotSpaceDelegate,
                                          CPTPlotDataSource,
                                          CPTScatterPlotDelegate>
{  
    ProgressAlert *progressAlert;
    
    NSString* sourceReference;
    
    CPTGraphHostingView  *defaultLayerHostingView;
    UISegmentedControl* rrdTimeScale;
    
    CPTGraph*           graph;

    CPTLayerAnnotation   *symbolTextAnnotation;
    
    // this is the data definition for the graph
    NSMutableArray* plotDataSourceNames;
    
    // plot colorList
    NSArray* plotcolorList;
    
    // marker colorlist
    NSArray* MarkerColorList;
    
    // consolidation level
    int consolidationStepCount;
    
    NSMutableArray* dataSourceLinePlots;
    
    // the current plotrange for the selected consolidation range
    NSDate* firstkey;
    NSDate* lastkey;
    double plotlength;
        
    // formatter for the pop over text on point selection
    NSDateFormatter *dateFormatterOverlay;
        
    // xaxis dateFormatter
    NSDateFormatter *dateFormatter;
}

@property (nonatomic, retain) CPTGraphHostingView *defaultLayerHostingView;
@property (readonly) id<RRDDisplayButtonDelegate> rrdDisplayButtonDelegate;
@property (retain) ProgressAlert *progressAlert;

- (id)initWithRRDDsiplayButtonDelegate:(id <RRDDisplayButtonDelegate>)delegate withReference:(NSString*)sourcereference;
@end
