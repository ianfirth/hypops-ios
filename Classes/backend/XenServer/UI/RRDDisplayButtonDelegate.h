//
//  RRDDisplayButtonDelegate.h
//  hypOps
//
//  Created by Ian Firth on 03/10/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundRobinDatabase.h"
#import "DataPoint.h"

@class RRDDisplayButton;

@protocol RRDDisplayButtonDelegate <NSObject>

@required
/**
 
 */
- (RoundRobinDatabase *)roundRobinDatabase;

@required
- (BOOL) reloadRoundRobinDatabase;

-(NSDate*) getLastRefreshTime;

@required
-(NSString*) graphTitleWithReference:(NSString*)sourcereference;;

/**
 The Navigation controller to place the graph view on (iPhone only)
 */
@required
-(UINavigationController*) navigationController;

@required
-(NSArray*)dataSourcesToGraphFromRRD:(RoundRobinDatabase*)rrd withReference:(NSString*)sourcereference;

//-(double)yMinForDataSources:(NSArray*)dataSources;
//-(double)yMaxForDataSources:(NSArray*)dataSources;
-(double)yMaxForDataSources:(NSArray*)dataSources withConsolidationStep:(int)step;
-(double)yMinForDataSources:(NSArray*)dataSources withConsolidationStep:(int)step;
-(double) getPlotPointFromDataPoint:(DataPoint*)dataPoint;

-(NSString*)yTitle;
-(NSNumberFormatter*) yFormatter;
@end
