//
//  ExpandableTableViewController.m
//  hypOps
//
//  Created by Ian Firth on 20/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "ExpandableTableViewController.h"

@interface ExpandableTableViewController ()
  - (void)checkAction:(id)sender;
@end

@implementation ExpandableTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [visibility release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self tableView] setAutoresizesSubviews:YES];
    [[self tableView] setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    if (visibility == nil) {
        visibility = [[NSMutableArray alloc] initWithCapacity:[self numberOfSectionsInTableView:[self tableView]]];  
        for (int i=0 ; i < [self numberOfSectionsInTableView:[self tableView]] ; i++){
            if ([self respondsToSelector:@selector(tableView:isExpandableSection:)]){
                if (![self tableView:[self tableView] isExpandableSection:i]){
                    [visibility addObject:[NSNumber numberWithInt:1]];
                }
            }        
            [visibility addObject:[NSNumber numberWithInt:0]];
        }
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // if the section is not expanded return 0 otherwise let the sub class return the value;
    if ([self tableView:tableView isHeaderExpandedForSection:section]){
        //NSLog(@"Number of rows in section %d is %d", section,[self tableView:tableView numberOfRowsInExpandableSection:section]);
        return [self tableView:tableView numberOfRowsInExpandableSection:section];
    }
    else
    {
        //NSLog(@"Number of rows in section %d is 0", section);
        return 0;
    }
}
 
- (void) tableView:(UITableView *)tableView setHeaderExpandedForSection:(NSInteger)section expandedState:(BOOL)expanded{
    if (expanded){
        [visibility replaceObjectAtIndex:section withObject:[NSNumber numberWithInt:1]];
    }
    else 
    {
        [visibility replaceObjectAtIndex:section withObject:[NSNumber numberWithInt:0]];
    }
}

- (BOOL) tableView:(UITableView *)tableView isHeaderExpandedForSection:(NSInteger)section{
    NSNumber *num = (NSNumber *)[visibility objectAtIndex:section];
    return [num isEqual:[NSNumber numberWithInt:1]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    // assume that all sections are expandable if this is not implemented
    if (![self respondsToSelector:@selector(tableView:isExpandableSection:)]){
        return SECTION_HEIGHT;
    }

    if (![self tableView:tableView isExpandableSection:section]){
        if ([self respondsToSelector:@selector(tableView:heightForUnExpandedHeaderInSection:)])
        {
           return [self tableView:tableView heightForUnExpandedHeaderInSection:section]; 
        }
        else
        {
            if ([self tableView:tableView titleForHeaderInSection:section] == nil ||
                [[self tableView:tableView titleForHeaderInSection:section] isEqualToString:@""]){
                return 0;
            }
            else
            {
               return SECTION_HEIGHT;
            }
        }
    }
    else{
        return SECTION_HEIGHT;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (![self respondsToSelector:@selector(tableView:isExpandableSection:)]){
        if (![self tableView:tableView isExpandableSection:section]){
            return [tableView tableHeaderView];
        }
    }
    
    // create the parent view that will hold header Label and button and icon image
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,tableView.bounds.size.width,SECTION_HEIGHT)];
    [customView setBackgroundColor:[UIColor whiteColor]];
    
    // create the label objects
    UILabel *headerLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:16];
    headerLabel.frame = CGRectMake(15,11,250,21);
    headerLabel.text =  [self tableView:tableView titleForHeaderInSection:section];
    headerLabel.textColor = [UIColor darkGrayColor];
    
    // if section is not expandable don't show Icon
    BOOL expandable = YES;
    if ([self respondsToSelector:@selector(tableView:isExpandableSection:)]){
        expandable = [self tableView:tableView isExpandableSection:section];
    }

    if (expandable){
        // create the button objects
        float frameWidth = [[self view] frame].size.width;
        UIButton *buttonDrop = [[UIButton alloc] initWithFrame:CGRectMake(frameWidth - 50, 0, 40, 40)];
        [buttonDrop setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];
        [buttonDrop setTag:section];
        
        if ([self tableView:tableView isHeaderExpandedForSection:section])
        {
            [buttonDrop setBackgroundImage:[UIImage imageNamed:@"up"] forState:UIControlStateNormal];
        }
        else
        {
            [buttonDrop setBackgroundImage:[UIImage imageNamed:@"down"] forState:UIControlStateNormal];
        }
        
        [buttonDrop addTarget:self action:@selector(checkAction:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:buttonDrop];
        [buttonDrop release];
    }

    [customView addSubview:headerLabel];
    return [customView autorelease]; 
}

- (void)checkAction:(id)sender
{
    UIButton *button = (UIButton*)sender;
    NSInteger section = [button tag];
    UIButton *buttonDrop = (UIButton *)sender;
    int numberOfExpandedRows = [self tableView:[self tableView] numberOfRowsInExpandableSection:section];
    
    if ([self tableView:[self tableView] isHeaderExpandedForSection:section])
    {
        [self tableView:[self tableView] setHeaderExpandedForSection:section expandedState:NO];
        [buttonDrop setBackgroundImage:[UIImage imageNamed:@"down"] forState:UIControlStateNormal];
        [[self tableView] beginUpdates];
        NSMutableArray *paths = [[NSMutableArray alloc] init];
        for (int i = 0 ; i < numberOfExpandedRows ; i++){
            NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:section];
            [paths addObject:path];
        }
        [[self tableView] deleteRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationFade];
        [[self tableView] endUpdates];
        [paths release];
    }
    else
    {
        [self tableView:[self tableView] setHeaderExpandedForSection:section expandedState:YES];
        [buttonDrop setBackgroundImage:[UIImage imageNamed:@"up"] forState:UIControlStateNormal];
        [[self tableView] beginUpdates];
        NSMutableArray *paths = [[NSMutableArray alloc] init];
        for (int i = 0 ; i < numberOfExpandedRows ; i++){
            NSIndexPath *path = [NSIndexPath indexPathForRow:i inSection:section];
            [paths addObject:path];
        }
        [[self tableView] insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationFade];
        [[self tableView] endUpdates];
        [paths release];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
