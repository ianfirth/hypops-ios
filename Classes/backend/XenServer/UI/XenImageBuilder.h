//
//  ImageBuilder.h
//  hypOps
//
//  Created by Ian Firth on 08/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XenVM.h"

@interface XenImageBuilder : NSObject {}

+ (void)buildVMImage:(UIImageView *)imageView ForVM:(XenVM *)xenVM;
+ (void)buildVMImage:(UIImageView *)imageView ForVMPowerState:(XenPowerState)xenVMPowerState;
@end
