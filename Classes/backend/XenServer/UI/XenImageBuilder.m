//
//  ImageBuilder.m
//  hypOps
//
//  Created by Ian Firth on 08/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//


#import "XenImageBuilder.h"
#import "ImageBuilder.h"

/* This is a collection of UI static methods that provide images for the Xen
 * Hypervisor
 */
@implementation XenImageBuilder

// bulild an image that represents the current VM with overlayed power state icon
+ (void) buildVMImage:(UIImageView *)imageView ForVM:(XenVM *)xenVM{
    [XenImageBuilder buildVMImage:imageView ForVMPowerState:[xenVM power_state]];
}

// bulild an image that represents the current VM with overlayed power state icon
+ (void)buildVMImage:(UIImageView *)imageView ForVMPowerState:(XenPowerState)powerState{
    UIImage *vmImage = nil;
    switch (powerState) {
        case XENPOWERSTATE_HALTED:
            vmImage = [ImageBuilder vMImageStopped];
            break;
        case XENPOWERSTATE_PAUSED:
            vmImage = [ImageBuilder vMImagePaused];
            break;
        case XENPOWERSTATE_RUNNING:
            vmImage = [ImageBuilder vMImageStarted];
            break;
        case XENPOWERSTATE_SUSPENDED:
            vmImage = [ImageBuilder vMImageSuspended];
            break;
    }
    
    [imageView setImage:vmImage];
}
@end
