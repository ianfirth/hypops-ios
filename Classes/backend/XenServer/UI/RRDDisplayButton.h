//
//  RRDDisplayButton.h
//  hypOps
//
//  Created by Ian Firth on 03/10/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RRDDisplayButtonDelegate.h"

@interface RRDDisplayButton : UIViewController<UIPopoverControllerDelegate>{
    UIButton* graphButton;
    UIButton* refreshButton;
    UILabel *mainTitleLabel;
    UILabel *updateTimeLabel;
    UIButton *backgroundButton;
    UIPopoverController* graphPopover;
    UIAlertView *alertView;
}

@property (retain) id<RRDDisplayButtonDelegate> delegate;
@property (copy) NSString* reference;
@property (retain) UIAlertView *alertView;

@end
