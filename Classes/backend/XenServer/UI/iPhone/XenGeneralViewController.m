//
//  XenVMGeneralViewController.m
//  hypOps
//
//  Created by Ian Firth on 04/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenGeneralViewController.h"
#import "XenImageBuilder.h"
#import "XenHypervisorConnection.h"

@implementation XenGeneralViewController

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)xenHypConnection
           xenDescritiveBase:(XenDescriptiveBase *)xenDescriptiveBase
                 displayMode:(GeneralDisplayMode)theMode
           tabName:(NSString *)tabName
   tabBarImageName:(NSString *)imageName 
             image:(UIImage *)mainImage 
extentionViewController:(UIViewController *)theExtentionViewController{

    NSPredicate *objectPred = [XenBase XenBaseFor:[xenDescriptiveBase opaque_ref]];
    self = [super initWithHypervisorConnection:xenHypConnection 
                                 rootObjectRef:[xenDescriptiveBase opaque_ref]
                           rootObjectPredicate:objectPred
                                 hypObjectType:[xenDescriptiveBase objectType]
                              namePropertyName:@"name_label"
                       descriptionPropertyName:@"name_description"
                                   displayMode:theMode
                                       tabName:tabName 
                               tabBarImageName:imageName 
                                         image:mainImage 
                       extentionViewController:theExtentionViewController];
    return self;
}


@end
