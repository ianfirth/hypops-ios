//
//  VMListViewController.h
//  hypOps
//
//  Created by Ian Firth on 03/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XenHypervisorConnection.h"
#import "UIWaitForDataLoad.h"
#import "XenRefreshableListController.h"

@interface XenVMListViewController : XenRefreshableListController 

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withCondition:(NSPredicate *) condition;


@end
