//
//  VMListByStatusViewController.m
//  hypOps
//
//  Created by Ian Firth on 03/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenVMListByStatusViewController.h"
#import "XenVM.h"
#import "XenVMListViewController.h"
#import "XenImageBuilder.h"
#import "ImageBuilder.h"

@interface XenVMListByStatusViewController ()
- (NSNumber *) xenVMCatagoryAtIndex:(int) index;
-(void) reloadTableData;
@end

@implementation XenVMListByStatusViewController
@synthesize xenVMCatagories, hypConnID;

#pragma mark -
#pragma mark View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil hypervisorConnection:(XenHypervisorConnection *)hypervisorConnection{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        [self setHypConnID:[hypervisorConnection connectionID]];
        // always enough room in portrait so only enable scroll if pull to refresh available
        [[self tableView] setScrollEnabled:![hypervisorConnection isAutoUpdateEnabled]];
    }
    return self;
}

- (void)viewDidLoad {
    [self setXenVMCatagories: [NSDictionary dictionaryWithObjectsAndKeys:
                               [NSNull null], [NSNumber numberWithInt:XEN_VM_ALL],
                               [XenVM VM_Tagged],[NSNumber numberWithInt:XEN_VM_TAGGED],
                               [XenVM VM_PowerStateFor:XENPOWERSTATE_HALTED],[NSNumber numberWithInt:XEN_VM_HALTED],
                               [XenVM VM_PowerStateFor:XENPOWERSTATE_PAUSED],[NSNumber numberWithInt:XEN_VM_PAUSED],
                               [XenVM VM_PowerStateFor:XENPOWERSTATE_RUNNING],[NSNumber numberWithInt:XEN_VM_RUNNING],
                               [XenVM VM_PowerStateFor:XENPOWERSTATE_SUSPENDED],[NSNumber numberWithInt:XEN_VM_SUSPENDED],
                               nil]];
    [super viewDidLoad];
    self.title = @"VMs";
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void) viewDidUnload{
    [self setXenVMCatagories:nil];
    [super viewDidUnload];
}

-(void) reloadTableData{
    [[self tableView] reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [[self tableView] reloadData];
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    [xenHypervisorConnection addHypervisorConnectionDelegate:self];
}

-(void)viewWillDisappear:(BOOL)animated{
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    [xenHypervisorConnection removeHypervisorConnectionDelegate:self];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    NSNumber *xenVMCatagory = [self xenVMCatagoryAtIndex:indexPath.row];   
    
    NSString *displayText;
    NSString *detailTextHeader; 
    int objectCount = 0;
    BOOL validCatagory = YES;
    
    switch ([xenVMCatagory intValue]) {
        case XEN_VM_HALTED:
            displayText = @"Stopped";
            [XenImageBuilder buildVMImage:[cell imageView] ForVMPowerState:XENPOWERSTATE_HALTED];
            detailTextHeader = @"VM Count:%d";
            break;
        case XEN_VM_PAUSED:
            displayText = @"Paused";
            [XenImageBuilder buildVMImage:[cell imageView] ForVMPowerState:XENPOWERSTATE_PAUSED];
            detailTextHeader = @"VM Count:%d";
            break;
        case XEN_VM_RUNNING:
            displayText = @"Running";
            [XenImageBuilder buildVMImage:[cell imageView] ForVMPowerState:XENPOWERSTATE_RUNNING];
            detailTextHeader = @"VM Count:%d";
            break;
        case XEN_VM_SUSPENDED:
            displayText = @"Suspended";
            [XenImageBuilder buildVMImage:[cell imageView] ForVMPowerState:XENPOWERSTATE_SUSPENDED];
            detailTextHeader = @"VM Count:%d";
            break;
        case XEN_VM_ALL:
            displayText = @"All VMs";
            [[cell imageView] setImage:[ImageBuilder vMImage]];
            detailTextHeader = @"VM Count:%d";
            break;
        case XEN_VM_TAGGED:
            displayText = @"Tagged VMs";
            [[cell imageView] setImage:[ImageBuilder vMTaggedImage]];
            detailTextHeader = @"VM Count:%d";
            break;
        default:
            validCatagory = NO;
            break;
    }
    
    if (validCatagory){
        XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

        NSString *detailText;
        NSObject *predicate = [[self xenVMCatagories] objectForKey:xenVMCatagory];
        if ([predicate isKindOfClass:[NSNull class]]){
            objectCount = [[xenHypervisorConnection hypObjectsForType:HYPOBJ_VM] count];
        } else {
            objectCount = [[xenHypervisorConnection hypObjectsForType:HYPOBJ_VM withCondition:(NSPredicate *)predicate] count];
        }
        detailText = [NSString stringWithFormat:detailTextHeader,objectCount];
        
        // set the selection style and the accessory type
        if (objectCount >0){
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        } else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
      
        // set the label text
        [[cell textLabel] setText:displayText];
        
        // set the detail text
        if (detailText != nil) {
            [[cell detailTextLabel] setText:detailText];
        }
    }        
    else {
        NSLog(@"Invalid cell catagory provided %@",xenVMCatagory);
    }
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self xenVMCatagories] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    NSNumber *xenVMCatagory = [self xenVMCatagoryAtIndex:indexPath.row]; 
    NSObject *predicate = [[self xenVMCatagories] objectForKey:xenVMCatagory];
    if ([predicate isKindOfClass:[NSNull class]]){
        predicate = nil;
    }
    XenVMListViewController *vMListViewController = [[XenVMListViewController alloc] initWithHypervisorConnection:xenHypervisorConnection withCondition:(NSPredicate *)predicate];

    switch ([xenVMCatagory intValue]) {
        case XEN_VM_HALTED:
            [vMListViewController setTitle:@"Stopped VMs"];
            break;
        case XEN_VM_PAUSED:
            [vMListViewController setTitle:@"Paused VMs"];
            break;
        case XEN_VM_RUNNING:
            [vMListViewController setTitle:@"Running VMs"];
            break;
        case XEN_VM_SUSPENDED:
            [vMListViewController setTitle:@"Suspended VMs"];
            break;
        case XEN_VM_ALL:
           [vMListViewController setTitle:@"All VMs"];
            break;
        case XEN_VM_TAGGED:
            [vMListViewController setTitle:@"VMs by Tag"];
            break;
        default:
            break;
    }
    
    [self.navigationController pushViewController:vMListViewController animated:YES];
    [vMListViewController release];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)dealloc {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    [xenHypervisorConnection removeHypervisorConnectionDelegate:self];
    [super dealloc];
}

#pragma mark -
#pragma mark Private methods
/*
 * return the catagory from the index specified.  This ensures that the list is provided in numerical order
 */
- (NSNumber *) xenVMCatagoryAtIndex:(int) index{
    NSSortDescriptor *numberDescriptor = [[NSSortDescriptor alloc] initWithKey:@"intValue" ascending:YES];
    NSArray *keys = [[[self xenVMCatagories] allKeys] sortedArrayUsingDescriptors:[NSArray arrayWithObject:numberDescriptor]];
    NSNumber *xenVMCatagory = [keys objectAtIndex:index];
    [numberDescriptor release];
    return xenVMCatagory;
}



#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    
    if (![xenHypervisorConnection isUpdatePendingForType:HYPOBJ_VM]){
        [xenHypervisorConnection RequestHypObjectsForType:HYPOBJ_VM];
    }
    // the call to stopLoading is on the data update received.
}


// override, and only add the header if not in auto refresh mode
- (void)addPullToRefreshHeader {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    if (![xenHypervisorConnection isAutoUpdateEnabled]){
        [super addPullToRefreshHeader];
    }
}

// override, and only add the header if not in auto refresh mode
- (void)startLoading {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    if (![xenHypervisorConnection isAutoUpdateEnabled]){
        [super startLoading];
    }
}

#pragma mark -
#pragma mark XenHypervisorConnection protocol implementation

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | HYPOBJ_VM) >0){
        XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
        
        [self reloadTableData];
        if (![xenHypervisorConnection isAutoUpdateEnabled]){
            [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
        }
    }
}


@end

