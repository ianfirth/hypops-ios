//
//  XenVmCpuDelegateImpl.m
//  hypOps
//
//  Created by Ian Firth on 05/10/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenVmCpuDelegateImpl.h"
#import "DataSource.h"
#import "MemoryNumberFormatter.h"

@implementation XenVmCpuDelegateImpl

@synthesize hypConnID, xenVmRef;

- (id)initWithhypervisorConnectionID:(NSString *)hypervisorConnectionID withVm:(XenVM *)thisXenVm navigationController:(UINavigationController*)navcon{
    self = [super init];
    if (self){
        [self setXenVmRef: [thisXenVm opaque_ref]];
        [self setHypConnID: hypervisorConnectionID];
        navigationController = [navcon retain];
    }
    return self;
}

-(void) dealloc{
    [navigationController release];
    [super dealloc];
}

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenVM *) xenVm{
    NSArray *xenVms = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVmRef]]];
    if ([xenVms count] >0){
        return [xenVms objectAtIndex:0];
    }
    return nil;
}

#pragma mark -
#pragma mark RRDDisplayButtonDelegate

- (RoundRobinDatabase *)roundRobinDatabase{
    XenVM *xenVm = [self xenVm];
    return [xenVm vmPerformanceData];
}

- (BOOL) reloadRoundRobinDatabase{
    XenVM *xenVm = [self xenVm];
    return [xenVm refreshPerformanceData];
}

-(NSDate*) getLastRefreshTime{
    XenVM *xenVm = [self xenVm];
    return [xenVm lastPreformanceDataUpdateTime];
}

-(NSString*) graphTitleWithReference:(NSString*)sourcereference{
    XenVM *xenVm = [self xenVm];
    return [NSString stringWithFormat:@"CPU usage for %@", [xenVm name_label]];
}

-(UINavigationController*) navigationController{
    return navigationController;
}

-(NSArray*)dataSourcesToGraphFromRRD:(RoundRobinDatabase*)rrd withReference:(NSString*)sourceReference{
    NSMutableArray* sourceArray = [[NSMutableArray alloc] initWithCapacity:1];
    for (DataSource* dataSource in [rrd dataSources]) {
        if ([[dataSource name] hasPrefix:@"cpu"]){
            [dataSource setAlternateName:[dataSource name]];
            [sourceArray addObject:dataSource];
        }
    }
    return [sourceArray autorelease];
}

-(double)yMinForDataSources:(NSArray*)dataSources withConsolidationStep:(int)step{
    return 0;
}

-(double)yMaxForDataSources:(NSArray*)dataSources withConsolidationStep:(int)step{
    return 1.0;
}

-(NSString*)yTitle{
    return @"";
}

-(double) getPlotPointFromDataPoint:(DataPoint*)dataPoint{
    // beacuse using a minmanl RRD all points are the same
    return [dataPoint average];
//    double displayVal = [dataPoint average];
//    if (displayVal == 0){
//        displayVal = [dataPoint max];
//    }
//    return displayVal;    
}

-(NSNumberFormatter*) yFormatter{
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterPercentStyle];
    // Set to the current locale
    [formatter setLocale:[NSLocale currentLocale]];
    return [formatter autorelease];
}
@end
