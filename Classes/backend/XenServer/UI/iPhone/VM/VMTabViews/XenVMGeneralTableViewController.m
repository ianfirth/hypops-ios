//
//  XenVMGeneralViewController.m
//  hypOps
//
//  Created by Ian Firth on 04/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenVMGeneralTableViewController.h"
#import "XenImageBuilder.h"
#import "XenVM_guestMetrics.h"
#import "ScrollableDetailTextCell.h"
#import "NetworkAddress.h"
#import "RRDDisplayButton.h"
#import "XenVmCpuDelegateImpl.h"
#import "XenVmMemoryDelegateImpl.h"
#import "XenHost.h"

#define RRDButtonCell @"RRD"
#define GeneralCell @"General"

@interface XenVMGeneralTableViewController ()
    - (UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier;
    - (UITableViewCell *) configureDetailsCellForTableView:(UITableView *)tableView atIndex:(int) index;
    - (UITableViewCell *) configureBootOrderCellForTableView:(UITableView *)tableView atIndex:(int) index;
    - (UITableViewCell *) configureCPUCellForTableView:(UITableView *)tableView atIndex:(int) index;
    - (UITableViewCell *) configureMemoryCellForTableView:(UITableView *)tableView atIndex:(int) index;
@end

@implementation XenVMGeneralTableViewController

@synthesize hypConnID, xenVMRef;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenVM *) xenVM{
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVMRef]]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }
    return nil;
}


- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM navigationController:(UINavigationController*)navcon{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setXenVMRef: [thisXenVM opaque_ref]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        navigationController = [navcon retain];
    }
    return self;
    
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [navigationController release];
    [rrdCPUButton release];
    [rrdMemoryButton release];
    navigationController = nil;
    [self setXenVMRef: nil];
    [self setHypConnID:nil];
    [super dealloc];
}

// provide access to the cell definitions
-(UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier] autorelease];
    }

    // general cells can have different components in them remove them if there are
    if (identifier == GeneralCell){
        [[[cell contentView] viewWithTag:100] removeFromSuperview];
    }
    return cell;    
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    [tableView release];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[[UIView alloc] init] autorelease]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
    // Releases the view if it doesn't have a superview.
    // this page does any memory cleen up possible for host objects
    // this will get called if any of the host tabs is visible
    // clean up any RRD data for any Hosts snce they cannot be in use at present
    // clean up any RRD data for any other vms that are not the one currently in use
    // -- Future enhancement if required --> unless the graph is not in us anyway inwhich case can clean up RRD data for this vm too
    
    NSArray* vms = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM];
    XenVM* currentVm = [self xenVM];
    for (XenVM* vm in vms) {
        if (![[vm opaque_ref] isEqualToString:[currentVm opaque_ref]]){
            [vm clearVmPerformanceData];
        }
    }
    
    NSArray* hosts = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST];
    for (XenHost* host in hosts) {
        [host clearHostPerformanceData];
    }
    
    [super didReceiveMemoryWarning];

}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    [xenHypervisorConnection addHypervisorConnectionDelegate:self];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to vm objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    // could cut this down to just the relevent ones if required
    if (objectType == HYPOBJ_VM_GUEST_METRICS){
        [[self tableView ]reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return XENVM_SECTION_COUNT;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // first section is always expanded
    return (section != 0);
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case XENVM_SECTION_GENERAL:
            return nil; // No heading required for first section
            break;
        case XENVM_SECTION_MEMORY:
            return @"Memory";
            break;
        case XENVM_SECTION_CPU:
            return @"CPU";
            break;
        case XENVM_SECTION_BOOTORDER:
            return @"Boot Order";
            break;
        default:
            break;
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    switch (section) {
        case XENVM_SECTION_GENERAL:
            return XENVMDETAILSTABLECOUNT;
            break;
        case XENVM_SECTION_MEMORY:
            return XENVM_MEMORY_TABLE_COUNT;
            break;
        case XENVM_SECTION_CPU:
            return XENVM_CPU_TABLE_COUNT;
            break;
        case XENVM_SECTION_BOOTORDER:
            return [[[self xenVM] bootOrder] count];
            break;
        default:
            break;
    }
    return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Configure the cell...
    switch ([indexPath section]) {
        case XENVM_SECTION_GENERAL:
            return [self configureDetailsCellForTableView:tableView atIndex:[indexPath row]];
            break;
        case XENVM_SECTION_MEMORY:
            return[self configureMemoryCellForTableView:tableView atIndex:[indexPath row]];
            break;
        case XENVM_SECTION_CPU:
            return [self configureCPUCellForTableView:tableView atIndex:[indexPath row]];
            break;
        case XENVM_SECTION_BOOTORDER:
            return [self configureBootOrderCellForTableView:tableView atIndex:[indexPath row]];
            break;
        default:
            break;
    }
    return nil;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (UITableViewCell *)configureMemoryCellForTableView:(UITableView *)tableView atIndex:(int) index {

    XenVM *xenVM = [self xenVM];

    UITableViewCell* cell;
    
    switch (index) {
        case XENVM_MEMORY_TABLE_MAXAVAILABLE:{  // static MAX
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Available:"];
            NSNumber *memTotalBytes = [xenVM memory_static_max];
            long long memTotal = [memTotalBytes longLongValue]; 
            float finalmemTotalGb = ((float)memTotal)/1024/1024/1024;
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Gb",finalmemTotalGb]];
            break;
        }
        case XENVM_MEMORY_TABLE_MINREQUIRED:{   // Static MIN
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Min Required"];
            NSNumber *memTotalBytes = [xenVM memory_static_min];
            long long memTotal = [memTotalBytes longLongValue]; 
            float finalmemTotalGb = ((float)memTotal)/1024/1024/1024;
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Gb",finalmemTotalGb]];
            break;
        }
        case XENVM_MEMORY_TABLE_DYANAMICMAX:{   
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Dynamic Max"];
            NSNumber *memTotalBytes = [xenVM memory_dynamic_max];
            long long memTotal = [memTotalBytes longLongValue]; 
            float finalmemTotalGb = ((float)memTotal)/1024/1024/1024;
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Gb",finalmemTotalGb]];
            break;
        }
        case XENVM_MEMORY_TABLE_DYNAMIXMIN:{   
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Dynamix Min"];
            NSNumber *memTotalBytes = [xenVM memory_dynamic_min];
            long long memTotal = [memTotalBytes longLongValue]; 
            float finalmemTotalGb = ((float)memTotal)/1024/1024/1024;
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Gb",finalmemTotalGb]];
            break;
        }
        case XENVM_MEMORY_TABLE_PERFORMANCEDATA:{
            cell = [self getCellForTableView:tableView andIdentifier:RRDButtonCell];
            [[cell detailTextLabel] setText:@""];
            [[cell textLabel] setText:@""];
            if (!rrdMemoryButton){
                rrdMemoryButton = [[RRDDisplayButton alloc] init];
                XenVmMemoryDelegateImpl *delegate = [[XenVmMemoryDelegateImpl alloc] initWithhypervisorConnectionID:[self hypConnID] withVm:[self xenVM] navigationController:navigationController];
                [rrdMemoryButton setDelegate:delegate];
                [delegate release];
                [[rrdMemoryButton view ]setTag:100];
            }
            [[rrdMemoryButton view ]setFrame:[[cell contentView] bounds]];
            [[cell contentView] setAutoresizesSubviews:YES];
            [[cell contentView] addSubview:[rrdMemoryButton view]];
            [rrdMemoryButton viewWillAppear:YES];
            break;
        }
        default:
            break;
    }    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];

    return cell;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (UITableViewCell *)configureCPUCellForTableView:(UITableView *)tableView atIndex:(int) index {
    XenVM *xenVM = [self xenVM];
    
    UITableViewCell* cell = nil;
    switch (index) {
        case XENVM_CPU_TABLE_CPUCOUNT:
        {
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"CPUs"];
            [[cell detailTextLabel] setText: [NSString stringWithFormat:@"%d", [xenVM VCPUs_at_startup]]];
            break;
        }
        case XENVM_CPU_TABLE_PERFORMANCEDATA:{
            cell = [self getCellForTableView:tableView andIdentifier:RRDButtonCell];
            [[cell detailTextLabel] setText:@""];
            [[cell textLabel] setText:@""];
            if (!rrdCPUButton){
                rrdCPUButton = [[RRDDisplayButton alloc] init];
                XenVmCpuDelegateImpl *delegate = [[XenVmCpuDelegateImpl alloc] initWithhypervisorConnectionID:[self hypConnID] withVm:[self xenVM] navigationController:navigationController];
                [rrdCPUButton setDelegate:delegate];
                [delegate release];
                [[rrdCPUButton view ]setTag:100];
            }
            [[rrdCPUButton view ]setFrame:[[cell contentView] bounds]];
            [[cell contentView] setAutoresizesSubviews:YES];
            [[cell contentView] addSubview:[rrdCPUButton view]];
            [rrdCPUButton viewWillAppear:YES];
            break;
        }
        default:
            break;
    }   
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];

    return cell;
}

- (UITableViewCell *) configureBootOrderCellForTableView:(UITableView *)tableView atIndex:(int) index{
    UITableViewCell* cell = [self getCellForTableView:tableView andIdentifier:RRDButtonCell];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    [[cell textLabel] setText:[[[self xenVM] bootOrder] objectAtIndex:index]];
    [[cell detailTextLabel] setText: @""];
    return cell;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (UITableViewCell *)configureDetailsCellForTableView:(UITableView *)tableView atIndex:(int) index {
   
    UITableViewCell* cell = nil;

    XenVM *xenVM = [self xenVM];
    switch (index) {
        case XENVMDETAILSTABLEOS:
        {
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"OS"];
            NSArray *opRefs = [xenVM referencesForType:HYPOBJ_VM_GUEST_METRICS];
            if (opRefs == nil || [opRefs count] ==0)
            {
                [[cell detailTextLabel] setText: @"Unknown"];
            }
            else
            {
                NSString *opRef = [opRefs objectAtIndex:0];
                XenVM_guestMetrics *gm = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM_GUEST_METRICS withCondition:[XenBase XenBaseFor:opRef]] objectAtIndex:0];
                [[cell detailTextLabel] setText: [gm OSVersionName]];
            }
            break;
        }
        case XENVMDETAILSTABLEVIRTUALIZATIONSTATE:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Virtulization State"];
            NSArray *opRefs = [xenVM referencesForType:HYPOBJ_VM_GUEST_METRICS];
            if (opRefs == nil || [opRefs count] ==0)
            {
                [[cell detailTextLabel] setText: @"Tools not installed"];
            }
            else
            {
                NSString *opRef = [opRefs objectAtIndex:0];
                XenVM_guestMetrics *gm = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM_GUEST_METRICS withCondition:[XenBase XenBaseFor:opRef]] objectAtIndex:0];
                [[cell detailTextLabel] setText: [NSString stringWithFormat:@"Optimized ( %@ installed )", [gm PVDriversVersionString]]];
            }
            break;
        }
        case XENVMDETAILSTABLEBIOSSTRINGCOPY:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Bios Copy"];
            UISwitch *onOffSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(200.0,7.0, 25.0, 20.0)];
            BOOL switchState = [xenVM are_bios_strings_set];
            [onOffSwitch setEnabled:NO]; 
            [onOffSwitch setTag:100];
            [onOffSwitch setOn:switchState]; 
            [[cell contentView]addSubview:onOffSwitch];
            [onOffSwitch release]; 
            [[cell detailTextLabel] setText: @""];
            break;
            }
        case XENVMDETAILSTABLEAUTOBOOT:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Auto Boot"];
             UISwitch *onOffSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(200.0,7.0, 25.0, 20.0)];
            BOOL switchState = [xenVM auto_power_on];
            [onOffSwitch setEnabled:NO]; 
            [onOffSwitch setTag:100];
            [onOffSwitch setOn:switchState]; 
            [[cell contentView] addSubview:onOffSwitch];
            [onOffSwitch release]; 
            [[cell detailTextLabel] setText: @""];

             break;
        }
        case XENVMDETAILSTABLEIPADDRESS:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"IP"];
            // might want to make this multi line at some point
            // but for now just append all strings together
            // would need to increase the Cell hight otherwise
            NSArray *opRefs = [xenVM referencesForType:HYPOBJ_VM_GUEST_METRICS];
            if (opRefs == nil || [opRefs count] ==0)
            {
                [[cell detailTextLabel] setText: @"Tools not installed"];
            } 
            else{
                NSString *opRef = [opRefs objectAtIndex:0];
                XenVM_guestMetrics *gm = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM_GUEST_METRICS withCondition:[XenBase XenBaseFor:opRef]] objectAtIndex:0];
                NSArray *allAddress = [gm networkAddress];
                NSString* finalString = nil;
                for (NetworkAddress* address in allAddress) {
                    if (!finalString){
                        finalString = [address address];
                    }
                    else{
                        finalString = [NSString stringWithFormat:@"%@ , %@",finalString,[address address]];
                    }
                }
                
                [[cell detailTextLabel] setText: finalString];
            }
            
            break;
        }
        default:
            break;
    }   
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];

    return cell;
}

- (void) buildImageViewCallback:(UIImageView *)objectImageView{
    XenVM *xenVM = [self xenVM];
    [XenImageBuilder buildVMImage:(UIImageView *)objectImageView ForVM:xenVM];
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}



@end
