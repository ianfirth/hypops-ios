//
//  XenVMSnaphostViewController.m
//  hypOps
//
//  Created by Ian Firth on 07/11/2011.
//  Copyright (c) 2011 Ian Firth. All rights reserved.
//

#import "XenVMSnaphostViewController.h"
#import "ImageBuilder.h"
#import "HypOpsAppDelegate.h"
#import "AlertWithDataStore.h"

@interface tempView:UIView{
    BOOL drawImage;
}

@property BOOL drawImage;

@end

@implementation tempView
@synthesize drawImage;

- (void)drawRect:(CGRect)rect {
    UIImage *snapshotImage2 = [UIImage imageNamed:@"noSnapshots"];
    CGRect rect2 = rect;

    UIDevice *device = [UIDevice currentDevice];
    // ipad portrait
    uint direction = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(direction)){
        if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
            // ipad portrait
            rect2.size.width = 400;
            rect2.size.height = 400;
            rect2.origin.x = (rect.size.width - rect2.size.width)/2;
            rect2.origin.y = (rect.size.height - rect2.size.height)/2;
        }
        else{
            // iphone portrait
            rect2.size.width = 200;
            rect2.size.height = 200;            
            rect2.origin.x = (rect.size.width - rect2.size.width)/2;
            rect2.origin.y = (rect.size.height - rect2.size.height)/2;
        }
    }

    if (UIInterfaceOrientationIsLandscape(direction)){
        if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
            // ipad landscape
            rect2.size.width = 400;
            rect2.size.height = 400;
            rect2.origin.x = (rect.size.width - rect2.size.width)/2;
            rect2.origin.y = (rect.size.height - rect2.size.height)/2;
        }
        else{
            // iphone landscape
            rect2.size.width = 120;
            rect2.size.height = 120;            
            rect2.origin.x = (rect.size.width - rect2.size.width)/2;
            rect2.origin.y = 20;
        }
    }

    if (drawImage){
        [snapshotImage2 drawInRect:rect2];
    }
    [super drawRect:rect];
}
@end

@interface XenVMSnaphostViewController (private)
- (XenHypervisorConnection *) hypervisorConnection;
- (XenVM *) xenVM;
- (XenVM *) snapshotForReference:(NSString*)reference;
- (void)configureCell:(UITableViewCell *)cell atIndex:(int) index;
-(NSArray*) allXenSnapshots;
-(NSArray*) snapshotsInPathToRoot:(XenVM*)snapshot;
-(void) takeSnapshotwithName:(NSString*)name;
-(void) revertToSnapshot:(NSString*)snapshotRef;
-(void) deleteSnapshot:(NSString*)snapshotRef;
-(void) buildCurrentSnapshotPath;
-(void) reloadData;
-(void) setOverlayPointerXLocation;
@end

@implementation XenVMSnaphostViewController

@synthesize xenVMRef, hypConnID;
@synthesize slideToCancel;

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM{
    self = [super init];
    if (self){
        [self setXenVMRef: [thisXenVM opaque_ref]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        snapshotSorter = [[NSSortDescriptor alloc] initWithKey:@"snapshot_time" ascending:NO];
    }
    return self;
    
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [c release];
    [snapshotList release];
    snapshotList = nil;
    [snapshotSorter release];
    snapshotSorter = nil;
    c = nil;
    
    [newSnapshotButton release];
    newSnapshotButton = nil;
    [revertToSnapshotButton release];
    revertToSnapshotButton = nil;
    [deleteSnapshotButton release];
    deleteSnapshotButton = nil;
    [currentParentPath release];
    currentParentPath = nil;
    [selectedXenVMReference release];
    selectedXenVMReference = nil;
    [overlayView release];
    overlayView = nil;
    [progressAlert release];
    progressAlert = nil;
    [super dealloc];
}

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenVM *) xenVM{
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVMRef]]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }
    return nil;
}

-(NSArray*) allXenSnapshots{
    // this needs to be a cached list, that is cleaned if the object type is updated.
    if (!snapshotList){
         // build the cache
        NSMutableArray* tempSnapshotList = [[NSMutableArray alloc] init];
        NSArray* allSnapshotRefs = [[self xenVM] snapshots];  
         for (NSString* snapshotRef in allSnapshotRefs){
             XenVM *snapshot = [self snapshotForReference:snapshotRef];
             if (snapshot){
                [tempSnapshotList addObject:snapshot];
             }
         }
        
        
         NSMutableArray* tempSortedArray = [[NSMutableArray alloc] initWithArray:[tempSnapshotList sortedArrayUsingDescriptors:[NSArray arrayWithObject:snapshotSorter]]];

        [tempSortedArray insertObject:[self xenVM] atIndex:0];
        snapshotList = tempSortedArray;
                        
        [snapshotList retain];
        [tempSortedArray release];
        [tempSnapshotList release];
    }
    return snapshotList;
}

- (XenVM *) snapshotForReference:(NSString*)reference{
    // try to see if it is a snapshot first
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_SNAPSHOT withCondition:[XenBase XenBaseFor:reference]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }

    // if not a snapshot check to see if it is a VM as the NOW point is a VM
    xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:reference]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }

    return nil;
}

// retrun a list that is in the correct order that lists the snapshots in the path to the root
-(NSArray*) snapshotsInPathToRoot:(XenVM*)snapshot{
    // take the snapshot and get the parent and keep getting parent until there is not one
    NSMutableArray* parentPath = [[NSMutableArray alloc] init];
    NSString* parentRef = [snapshot opaque_ref];
    while (parentRef){
        BOOL located = NO;
        for (XenVM* snapshot in snapshotList){
            if ([[snapshot opaque_ref] isEqualToString:parentRef]){
                located = YES;
            }
        }
        // only add snapshots to the path list if they are in the all snapshots for VM list
        // this stops the VMTemplate that actually was used for the intial vm point being displayed.
        if (located){
           [parentPath addObject:parentRef];
        }
        
        XenVM* parentSnapshot = [self snapshotForReference:parentRef];
        parentRef = [parentSnapshot parent];
        
     }
    return [parentPath autorelease];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

// this is called by the titledViewController when the manual refresh button
// is pressed and gives the view a chance to redraw properly at the end.
-(void) refreshPage{
    [self reloadData]; 
}

#pragma mark - Snapshot operations
-(void) takeSnapshotwithName:(NSString*)name{
    // take snapshot always takes a snapshot from the latest
    [[self hypervisorConnection] RequestNewSnapshot:name forVMReference:xenVMRef];
}

-(void) revertToSnapshot:(NSString*)snapshotRef{
    [[self hypervisorConnection] RequestRevertToSnapshotWithReference:snapshotRef];
}

-(void) deleteSnapshot:(NSString*)snapshotRef{
    [[self hypervisorConnection] RequestDestroyObjectType:HYPOBJ_SNAPSHOT WithReference:snapshotRef];
}

#pragma mark - UIAlertView delegate

- (void)didPresentAlertView:(UIAlertView *)alertView{

    // if it is not a Progress alert then there is no processing to do here.
    if ([alertView isKindOfClass:[ProgressAlert class]]){

        ProgressAlert* alert = (ProgressAlert*)alertView;
    
        NSDictionary* params = [alert parameterMap];
        
        if ([[params objectForKey:@"operation"] isEqualToString:@"create"]){
            [self takeSnapshotwithName:[params objectForKey:@"name"]];
        }
        else if ([[params objectForKey:@"operation"] isEqualToString:@"revert"]){
            [self revertToSnapshot:[params objectForKey:@"reference"]];
        }
        else if ([[params objectForKey:@"operation"] isEqualToString:@"delete"]){
            [self deleteSnapshot:[params objectForKey:@"reference"]];
        }
    }
}
#pragma mark - View lifecycle


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{

}

-(void)takeSnapshotButton:(id) sender{
    
 
     AlertWithDataStore *myAlertView = [[AlertWithDataStore alloc] initWithTitle:@"Create Snapshot" 
                                                                            message:@"Enter the new snapshot name" 
                                                                           delegate:self 
                                                                  cancelButtonTitle:@"Cancel"
                                                                  otherButtonTitles:@"Create", nil];        
    myAlertView.textField.placeholder = @"snapshot";
    myAlertView.textField.autocorrectionType = UITextAutocorrectionTypeNo;
    myAlertView.textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    myAlertView.textField.secureTextEntry = NO;
    
    [myAlertView setTag:-100];
    [myAlertView show];
    [myAlertView release];
}


// process any UIAlert view responses
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    // determine if this is a password box (contains a subView with a tag of -100);
    bool snapshotNameDialog = ([alertView tag] == -100);
    
    if (snapshotNameDialog){
        if (buttonIndex > 0){   // only interested in the create button
            AlertWithDataStore *awds = (AlertWithDataStore*)alertView;
            NSString *snapshotName = [awds textString];
            NSDictionary* params = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    @"create",@"operation",
                                    snapshotName,@"name",nil];
            progressAlert = [[ProgressAlert alloc] initWithTitle:@"Creating" delegate:self parameters:params];
            [progressAlert show];
        }
    }
}

-(void)revertToSnapshotButton:(id) sender{
    NSDictionary* params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            @"revert", @"operation",
                            selectedXenVMReference,@"reference",nil];
    progressAlert = [[ProgressAlert alloc] initWithTitle:@"Reverting" delegate:self parameters:params];
    [progressAlert show];
}

-(void)deleteSnapshotButton:(id) sender{
    NSDictionary* params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            @"delete",@"operation",
                            selectedXenVMReference,@"reference",nil];
    progressAlert = [[ProgressAlert alloc] initWithTitle:@"Deleting" delegate:self parameters:params];
    [progressAlert show];
}

-(void) reloadData{
    // remember the selected one here
    NSString *selectedRef =  selectedXenVMReference;
    
    [selectedXenVMReference release];
    selectedXenVMReference = nil;
    [currentParentPath release];
    currentParentPath = nil;
    [snapshotList release];
    snapshotList = nil;
    [[c tableView ]reloadData];
    [overlayView setNeedsDisplay];  
    BOOL noRows =  ([[self allXenSnapshots] count] <= 1);
    tempView* background = (tempView *)[self view];
    [background setDrawImage:noRows]; 
    [background setNeedsDisplay];
    [[c view] setHidden:noRows];
    [overlayView setHidden:noRows];
    
    if (selectedRef){
        // then if still exists select it and scroll to it
        for (int i = 0; i< [[self allXenSnapshots] count] ; i++){
            XenVM* snapshot = [[self allXenSnapshots] objectAtIndex:i];
            if ([[snapshot opaque_ref] isEqualToString:selectedRef]){
                // found it
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                [[c tableView] selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                [[[c tableView] delegate] tableView:[c tableView] didSelectRowAtIndexPath:indexPath];
                [[c tableView] setNeedsDisplay];
                break;
            }
        }
    }
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {    
    [super viewDidLoad];

    [self setSlideToCancel: [[[SlideToCancelViewController alloc] init] autorelease]];
    
    UIView *view = [[tempView alloc] init];
    [self setView:view];
    [view setBackgroundColor:[UIColor whiteColor]];
    
    // create a snapshot image button here
    // move this button onto the now tableCell perhaps?
    newSnapshotButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *snapshotImage = [ImageBuilder snapshotImage];
    [newSnapshotButton setImage:snapshotImage forState:UIControlStateNormal];
    [newSnapshotButton addTarget:self 
                          action:@selector(takeSnapshotButton:)
                forControlEvents:UIControlEventTouchDown];

    newSnapshotButton.frame = CGRectMake( 10 , 10.0, 40, 40);
    // can always do this.  Always takes a snaphsot from now
    [newSnapshotButton setEnabled:YES];

    [view addSubview:newSnapshotButton];

    revertToSnapshotButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *revertImage = [ImageBuilder snapshotRevertImage];
    [revertToSnapshotButton setImage:revertImage forState:UIControlStateNormal];
    [revertToSnapshotButton addTarget:self 
                               action:@selector(revertToSnapshotButton:)
                     forControlEvents:UIControlEventTouchDown];
    revertToSnapshotButton.frame = CGRectMake( 60 , 10.0, 40, 40);
    [view addSubview:revertToSnapshotButton];
    [revertToSnapshotButton setEnabled:NO];

    deleteSnapshotButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *deleteImage = [ImageBuilder deleteImage];
    [deleteSnapshotButton setImage:deleteImage forState:UIControlStateNormal];
    [deleteSnapshotButton addTarget:self 
                             action:@selector(deleteSnapshotButton:)
                   forControlEvents:UIControlEventTouchDown];

    deleteSnapshotButton.frame = CGRectMake( 110 , 10.0, 40, 40);
    [view addSubview:deleteSnapshotButton];
    [deleteSnapshotButton setEnabled:NO];
    
    c = [[UITableViewController alloc] initWithStyle:UITableViewStyleGrouped ];
    [[c tableView] setAutoresizesSubviews:YES];
    [[c tableView] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [[c tableView] setBackgroundColor:[UIColor whiteColor]];
    [[c tableView] setBackgroundView:nil];
    [[c tableView] setBackgroundView:[[[UIView alloc] init] autorelease]];
    [[c tableView ]setDataSource:self];
    [[c tableView ]setDelegate:self];
    [[self view] addSubview:[c view]];
    
    overlayView = [[RelatedListOverlayView alloc] init];
    [[self view] addSubview:overlayView];
    [overlayView setUserInteractionEnabled:NO];
    [overlayView setBackgroundColor:[UIColor clearColor]];
    [overlayView setRelationshipDataSource:self];
    
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [view release];
        
    // get this working and in the right places etc..
    // then 
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self setOverlayPointerXLocation];
    NSIndexPath* path = [[c tableView] indexPathForSelectedRow];
    if (path){
        // get currently selected row
        UITableViewCell* cell =[[c tableView] cellForRowAtIndexPath:[[c tableView] indexPathForSelectedRow]]; 
        yLocation = [cell frame].origin.y;
        [overlayView setNeedsDisplay];
    }
    
    BOOL noRows =  ([[self allXenSnapshots] count] <= 1);
    tempView* background = (tempView *)[self view];
    [background setDrawImage:noRows]; 
    [background setNeedsDisplay];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];  

    if ([HypOpsAppDelegate needDisplayUnlock]){
        if (!disableView){
            [disableView release], disableView = nil;
        }
        disableView = [[GradientView alloc] init];
        [disableView setEndColor : [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]];
        [disableView setStartColor : [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]];
        [disableView setUseRadial : YES];
        [disableView setOpaque:NO];
        [disableView setGradientViewFillMode:GradientViewFillModeFull];
        [disableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [[[self slideToCancel] view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        [self slideToCancel].delegate = self;
        [self slideToCancel].label.text = @"slide to unlock";
        // Add slider to the view
        [self.view addSubview:[self slideToCancel].view];
        [[[self slideToCancel] view] setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin| UIViewAutoresizingFlexibleWidth ];
        [[self slideToCancel] viewWillAppear:animated];
    }
}

-(void) setOverlayPointerXLocation{
    UIDevice *device = [UIDevice currentDevice];
    // ipad portrait
    uint direction = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(direction)){
        if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
            xLocation = [[self view] frame].size.width - 60;  //ipad
        }else{
            xLocation = [[self view] frame].size.width - 25;  //iPhone
        }
    }
    if (UIInterfaceOrientationIsLandscape(direction)){
        if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
            xLocation = [[self view] frame].size.width - 57;  //iPad
        }
        else{
            xLocation = [[self view] frame].size.width - 25;  //iPhone
        }
    }
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    [self setOverlayPointerXLocation];

    NSUInteger indexArr[] = {0,0};
    
    NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:indexArr length:2]; 
    UITableViewCell *cell = [[c tableView] cellForRowAtIndexPath:indexPath];
    //    UITableViewCell *cell = [[c tableView] cellForRowAtIndexPath:[[[c tableView] indexPathsForVisibleRows] objectAtIndex:0]];
    yStartLocation = [cell frame].origin.y + 25;

    if ([HypOpsAppDelegate needDisplayUnlock]){
        [[[self slideToCancel] view] setHidden:NO];
        int frameHeight = [[self view] frame].size.height - 30;  // minus tabview height
        int frameWidth = [[self view] frame].size.width;
        float initialSliderLocation = frameHeight + 200;
        [[self view] bringSubviewToFront:[[self slideToCancel] view]];
        CGRect frameRect2 = [[self view] frame];
        frameRect2.size.height = initialSliderLocation;
        frameRect2.origin.y = 0;
        [disableView setFrame:frameRect2];
        [self slideToCancel].view.backgroundColor = [UIColor blackColor];
        [self slideToCancel].view.frame = CGRectMake(0,initialSliderLocation ,frameWidth,200); 
        
        float sliderLocation = frameHeight + [[self view] frame].origin.y -60;
        // Slowly move up the slider from the bottom of the screen
        CGRect frameRect = [[self view] frame];
        float originaly = frameRect.origin.y;
        frameRect.origin.y = 0;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        frameRect.size.height = sliderLocation - originaly;
        [disableView setFrame:frameRect];
        [[self view] bringSubviewToFront:[[self slideToCancel] view]];
        [self slideToCancel].view.frame = CGRectMake(0,sliderLocation - originaly ,frameWidth,200);
        [[self slideToCancel] setEnabled:YES];
        [UIView commitAnimations];
        [[self view] addSubview:disableView];
        [[self view] bringSubviewToFront:disableView];
    }
    else{
        [[[self slideToCancel] view] setHidden:YES];
    }
    
    BOOL noRows =  ([[self allXenSnapshots] count] <= 1);
    tempView* background = (tempView *)[self view];
    [background setDrawImage:noRows]; 
    [background setNeedsDisplay];
    
    CGRect f = [[self view] frame];
    [[c view] setFrame: CGRectMake(0, 50.0, f.size.width, f.size.height -50)];
    [overlayView setFrame:CGRectMake(0, 50.0, f.size.width, f.size.height -50)];
     
    [[c view] setAutoresizingMask: UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
     [overlayView setAutoresizingMask: UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];

     // if there is no data in the table then name the table hidden and this will allow the contol
    // backgrond to show though I think
    [[c view] setHidden:noRows];
    [overlayView setHidden:noRows];
    
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
    [overlayView setNeedsDisplay];
}

- (void) viewWillDisappear:(BOOL)animated{
    [disableView removeFromSuperview];
    [[self slideToCancel] setEnabled:NO];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (disableView){
        [disableView release];
        disableView = nil;
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    // does not have a title
    return @"";
}

// one row for each snapshot
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // this needs to be all snapshots, including snapshots in the children etc..
    // and includes the extra NOW cell
    if ([[self allXenSnapshots] count] == 1){
        return 0;
    }
    return [[self allXenSnapshots] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // make the top row always say Now, so make this a special cell that can be rendered in its own way
    NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
    [self configureCell:cell atIndex:[indexPath row]];
    return cell;
}
        
- (void)configureCell:(UITableViewCell *)cell atIndex:(int) index {    
    // this bit is the same regardless if it is the 
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];

    // remove cells from subview with tag of -200 here
    [[[cell contentView] viewWithTag:-200] removeFromSuperview];
    
    // use this to put data onto the cell    
    XenVM *snapshot = [[self allXenSnapshots] objectAtIndex:index]; // first cell is now
    if ([snapshot vmType] ==   XENVMTYPE_VM){
        [[cell textLabel] setText:@"Now"];        
        [[cell detailTextLabel] setText:@"Current"];
    }else{
        [[cell textLabel] setText:[snapshot name_label]];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss"];
        NSString* displayDate = [dateFormatter stringFromDate:[snapshot snapshot_time]];
        [[cell detailTextLabel] setText:displayDate];
    }
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    
    yLocation = [cell frame].origin.y;
    
    if (selectedXenVMReference){
        [selectedXenVMReference release];
        selectedXenVMReference = nil;
    }
    selectedXenVMReference = [[snapshotList objectAtIndex:[indexPath row]] opaque_ref];
    [selectedXenVMReference retain];
    
    XenVM* currentSnapshotSelected = nil;
    currentSnapshotSelected = [snapshotList objectAtIndex:[indexPath row]];
   
    if ([currentSnapshotSelected vmType] ==   XENVMTYPE_VM){
        // this is the NOW
        [deleteSnapshotButton setEnabled:NO];
        [revertToSnapshotButton setEnabled:NO];
    }
    else{
        // I think that these are all always available but need to check that this is the case
        [deleteSnapshotButton setEnabled:YES];
        [revertToSnapshotButton setEnabled:YES];
    }

 
    // build an array of the snaphsots in the linear path back to the root
    [self buildCurrentSnapshotPath];
    [overlayView setNeedsDisplay];
}

-(void) buildCurrentSnapshotPath{
    // build an array of the snaphsots in the linear path back to the root
    if(currentParentPath){
        [currentParentPath release];
        currentParentPath = nil;
    }
    
   XenVM*  currentSnapshotSelected = [self snapshotForReference:selectedXenVMReference];
    
   currentParentPath = [self snapshotsInPathToRoot:currentSnapshotSelected];
   [currentParentPath retain];
}

#pragma mark -
#pragma mark TableViewRelationShipDataSource

-(int) getAllPointCount{
    return [snapshotList count];
}

-(float) getAllPointIntervalForPoint:(int)pointNum{
    if (pointNum == 0){
        return 44;
    }
    return 45;
}

// return the number of points that there are in the past to draw
-(int) getPastPointCount{
    return [currentParentPath count];
}

-(CGPoint) getAllPointStartPoint{
    if ([[c tableView] isHidden]){
        return CGPointMake(0,0);
    }

    return CGPointMake(xLocation, yStartLocation);
}

// get the point at the required position
- (CGPoint) pointAt:(int)pointNum{    
    // assume that all cells are the same height for this to work
    // convert a snapshot to a positon in the all snapshot list


    // find the first match this is the start then find out how many further down the list the actual one is
    int startIndex = 0;
    NSString *ref= [currentParentPath objectAtIndex:0];
    for (int i=0; i < [snapshotList count] ; i++){ 
        XenVM* snapshot =  [snapshotList objectAtIndex:i];
        if ([[snapshot opaque_ref] isEqualToString: ref]){
            startIndex = i;  
        }
    }    
    
    ref= [currentParentPath objectAtIndex:pointNum];
    
    // find the first match this is the start then find out how many further down the list the actual one is
    for (int i=0; i < [snapshotList count] ; i++){ 
        XenVM* snapshot =  [snapshotList objectAtIndex:i];
        if ([[snapshot opaque_ref] isEqualToString: ref]){
            int difference = i - startIndex;
            
            // first table cell is 1 point higher than all the other cells.
            int tableCellHeight = 44;
            if (i == 0){
                tableCellHeight = 45;
            }
            float yposition = yLocation + (difference * tableCellHeight) + 25;
            CGPoint point = CGPointMake(xLocation,  yposition);  
            return point;
        }
    }
    return CGPointMake(0,0);  // if get here something has gone wrong
}

#pragma mark -
#pragma mark HypervisorCOnnectionDelegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    if ( objectType == HYPOBJ_VM){
        if ([objectRef isEqualToString:[[self xenVM] opaque_ref]]){
            [self reloadData];
        }
    }
    if ( objectType == HYPOBJ_SNAPSHOT){
        // deal with delete here, I am surprised that the VM is not updated to reflect the change in the
        // snapshots but it does not appear to be.
        [self reloadData];
    }
}

- (void)hypervisorResponseReceived:(HypervisorConnectionFactory *)hypervisorConnection requestCmd:(NSString*)cmd error:(NSError*)error{
    if ([cmd isEqualToString:@"VM.snapshot"] ||
        [cmd isEqualToString:@"VM.revert"] ||
        [cmd isEqualToString:@"VM.destroy"]){
        // make sure that the cache has been updated before processing the screen
        [self performSelector:@selector(responsePostProcess) withObject:nil afterDelay:0.1];
    }
}

-(void) responsePostProcess{
    if (progressAlert){
        // if in manual mode do a refresh of the VM here to make sure that the snapshot list is correct
        if (![[self hypervisorConnection] isAutoUpdateEnabled]){
            [[self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_SNAPSHOT];
            [[self hypervisorConnection] RequestHypObject:[self xenVMRef] ForType:HYPOBJ_VM];
        }
        [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
        [progressAlert release];
        progressAlert = nil;
    }    
}

#pragma mark - SlideToCancel protocol
- (void) cancelled{
    [HypOpsAppDelegate unlockDisplay];
    CGRect frameRect = [[self view] frame];
    [UIView beginAnimations:nil context:nil];
   	[UIView setAnimationDuration:0.8];
    int frameWidth = [[self view] frame].size.width;
    [self slideToCancel].view.frame = CGRectMake(0,frameRect.size.height +200 ,frameWidth,200);
  	[UIView commitAnimations];
    [disableView removeFromSuperview];
    [[self slideToCancel] setEnabled:NO];
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint point = [scrollView contentOffset]; 
    [overlayView setContentOffset:point animated:false];
    [overlayView setNeedsDisplay];
}

@end
