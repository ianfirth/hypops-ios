//
//  XenVMPowerViewController.m
//  hypOps
//
//  Created by Ian Firth on 26/04/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenVMPowerViewController.h"
#import "HypOpsAppDelegate.h"

@interface PowerOperationData :NSObject{
    int powerOp;
    NSString *vmRef;
    BOOL powerOpRequested;
}
@property int powerOp;
@property (copy) NSString *vmRef;
@property BOOL powerOpRequested;
@end

@implementation PowerOperationData
@synthesize powerOp;
@synthesize vmRef;
@synthesize powerOpRequested;
@end

@interface XenVMPowerViewController()
- (void) configureCell:(UITableViewCell *)cell atIndex:(int) index;
- (void)timerFireMethod:(NSTimer*)theTimer;

@end

@implementation XenVMPowerViewController

@synthesize hypConnID, xenVMRef, powerOpINProgress;
@synthesize slideToCancel;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenVM *) xenVM{
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVMRef]]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }
    return nil;
}


- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setXenVMRef: [thisXenVM opaque_ref]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        [self setPowerOpINProgress:-1];
        [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    }
    return self;
    
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSlideToCancel: [[[SlideToCancelViewController alloc] init] autorelease]];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    [tableView release];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[[UIView alloc] init] autorelease]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];

    // Without this label the heading will show though the disbale tinit for some reason
    UILabel *l = [[UILabel alloc] init];
    [l setText:@""];
    [tableView setTableHeaderView:l];
    [l release];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [self setSlideToCancel:nil];
    [super viewDidUnload];
}


- (void)dealloc {
    [self setXenVMRef: nil];
    [self setHypConnID:nil];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [super dealloc];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    return NO;
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Power";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[self xenVM] availablePowerOperations] count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    
    [self configureCell:cell atIndex:[indexPath row]];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndex:(int) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    XenVM *xenVM = [self xenVM];
    int powerState = [[[[xenVM availablePowerOperations] allObjects] objectAtIndex:index] intValue];

    NSString *buttonText = nil;
    NSString *imageName = nil;
    switch (powerState){
        case XENPOWEROPERATION_START:
            buttonText = @"Start";
            imageName = @"start_vm";
            break;
        case XENPOWEROPERATION_STOP_CLEAN:
            buttonText = @"Shutdown";
            imageName = @"stop_vm";
            break;
        case XENPOWEROPERATION_STOP_FORCE:
            buttonText = @"** Force Shutdown **";
            imageName = @"stop_vm";
            break;
        case XENPOWEROPERATION_RESUME:
            buttonText = @"Resume";
            imageName = @"strat_vm";
            break;
        case XENPOWEROPERATION_SUSSPEND:
            buttonText = @"Suspend";
            imageName = @"suspend_vm";
            break;
        case XENPOWEROPERATION_RESTART_CLEAN:
            buttonText = @"Restart";
            imageName = @"start_vm";
            break;
        case XENPOWEROPERATION_RESTART_FORCE:
            buttonText = @"** Force Restart **";
            imageName = @"start_vm";
            break;
    }
    
    [[cell textLabel] setText:buttonText];
    [[cell imageView] setImage:[UIImage imageNamed:imageName]];

    // remove any existing button or spinner from the view
    [[[cell contentView] viewWithTag:100] removeFromSuperview];
    
    /// set the spinner on the cells if the op is in progress
    if ([self powerOpINProgress] == powerState){
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        CGRect rect = CGRectMake([[cell contentView] frame].size.width-30, 10.0, 20.0, 20.0);
        [spinner setFrame:rect];
        [spinner setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin];
        [spinner setTag:100];
        [[cell contentView] addSubview:spinner];
        [spinner startAnimating];
        [spinner release];
        [cell setBackgroundColor:[UIColor blueColor]];
    }
    else
    {
        [cell setBackgroundColor:[UIColor whiteColor]];
    }

}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self powerOpINProgress] > -1){
        NSLog(@"PowerOperation in progress cant do another one");
        return;
    }
    NSSet *powerOpSet = [[self xenVM] availablePowerOperations];
    int powerState = [[[powerOpSet allObjects] objectAtIndex:[indexPath row]] intValue];
    // could do this in the first timer option then could display progress in actual cell possibly
    [self setPowerOpINProgress:powerState];
    // get the spinner going
    [[self tableView] reloadData];
    PowerOperationData *pod = [[PowerOperationData alloc] init];
    [pod setPowerOp:powerState];
    [pod setVmRef:[self xenVMRef]];
    [pod setPowerOpRequested:NO];
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFireMethod:) userInfo:pod repeats:YES];
    [pod release];
}



- (void)timerFireMethod:(NSTimer*)theTimer{
    PowerOperationData *pod = (PowerOperationData *)[theTimer userInfo];
    // if not requested the op yet do it now
    if (![pod powerOpRequested]){
        [[self hypervisorConnection] RequestPowerOperation:[pod powerOp] forVMReference:[pod vmRef]];
        [pod setPowerOpRequested:YES];
    }
    else{
       NSArray *vms = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[pod vmRef]]]; 
        if ([vms count]>0){
            XenVM *vm = [vms objectAtIndex:0];
            NSSet *availablePowerOps = [vm availablePowerOperations];
            // if there are no power ops available then assume that the current op is still in progress
            if ([availablePowerOps count] >0){
                // if the powerOp requested is in the list still then not finished
                BOOL found = NO;
                for (NSNumber *op in availablePowerOps) {
                    if ([op intValue] == [pod powerOp]){
                        found = YES;
                        break;
                    }
                }
                if (!found){
                    // must have finished here
                    [self setPowerOpINProgress:-1];
                    [theTimer invalidate];
                    [[self tableView] reloadData];
               }
            }
            if (![[self hypervisorConnection] isAutoUpdateEnabled]){
                [[self hypervisorConnection] RequestHypObject:[pod vmRef] ForType:HYPOBJ_VM];
            }
        }
        else  // something went wrong, can no longer find vm in cache, so give up
        {
            [self setPowerOpINProgress:-1];
            [theTimer invalidate];
        }
    }
}


- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];  
    // make sure that the date on the page is upto date
    [[self tableView] reloadData];
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    [xenHypervisorConnection addHypervisorConnectionDelegate:self];
    [xenHypervisorConnection setWalkTreeMode:NO];

    if ([HypOpsAppDelegate needDisplayUnlock]){
        if (!disableView){
            [disableView release], disableView = nil;
        }
        disableView = [[GradientView alloc] init];
        [disableView setEndColor : [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]];
        [disableView setStartColor : [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]];
        [disableView setUseRadial : YES];
        [disableView setOpaque:NO];
        [disableView setGradientViewFillMode:GradientViewFillModeFull];
        [disableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [[[self slideToCancel] view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        [self slideToCancel].delegate = self;
        [self slideToCancel].label.text = @"slide to unlock";
        // Add slider to the view
        [self.view addSubview:[self slideToCancel].view];
        [[[self slideToCancel] view] setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin| UIViewAutoresizingFlexibleWidth ];
        [[self slideToCancel] viewWillAppear:animated];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];    
    if ([HypOpsAppDelegate needDisplayUnlock]){
        [[[self slideToCancel] view] setHidden:NO];
        int frameHeight = [[self view] frame].size.height - 30;  // minus tabview height
        int frameWidth = [[self view] frame].size.width;
        float initialSliderLocation = frameHeight + 200;
        [[self view] bringSubviewToFront:[[self slideToCancel] view]];
        CGRect frameRect2 = [[self view] frame];
        frameRect2.size.height = initialSliderLocation;
        frameRect2.origin.y = 0;
        [disableView setFrame:frameRect2];
        [self slideToCancel].view.backgroundColor = [UIColor blackColor];
        [self slideToCancel].view.frame = CGRectMake(0,initialSliderLocation ,frameWidth,200); 

        float sliderLocation = frameHeight + [[self view] frame].origin.y -60;
        // Slowly move up the slider from the bottom of the screen
        CGRect frameRect = [[self view] frame];
        float originaly = frameRect.origin.y;
        frameRect.origin.y = 0;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        frameRect.size.height = sliderLocation - originaly;
        [disableView setFrame:frameRect];
        [[self view] bringSubviewToFront:[[self slideToCancel] view]];
        [self slideToCancel].view.frame = CGRectMake(0,sliderLocation - originaly ,frameWidth,200);
        [[self slideToCancel] setEnabled:YES];
        [UIView commitAnimations];
        [[self view] addSubview:disableView];
        [[self view] bringSubviewToFront:disableView];
        [[self tableView] setScrollEnabled:NO];  
    }
    else{
        [[[self slideToCancel] view] setHidden:YES];
    }
}

- (void) viewWillDisappear:(BOOL)animated{
    [disableView removeFromSuperview];
    [[self slideToCancel] setEnabled:NO];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (disableView){
        [disableView release];
        disableView = nil;
    }
}

#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)xenHypervisor:(XenHypervisorConnection *)hypervisorConnection updatesObjectsOfType:(int)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // Nothing required here, everything is on the view root object, this will be handeled
    // by the XenGeneralViewController.  Only need to listed to obejcts that are
    // not the root object in these 'extention' pages.
}

#pragma mark - SlideToCancel protocol
- (void) cancelled{
    [HypOpsAppDelegate unlockDisplay];

    CGRect frameRect = [[self view] frame];
    [UIView beginAnimations:nil context:nil];
   	[UIView setAnimationDuration:0.8];
    int frameWidth = [[self view] frame].size.width;
    [self slideToCancel].view.frame = CGRectMake(0,frameRect.size.height +200 ,frameWidth,200);
  	[UIView commitAnimations];
    [disableView removeFromSuperview];
    [[self slideToCancel] setEnabled:NO];
    [[self tableView] setScrollEnabled:YES];
}

@end
