//
//  XenVMPowerViewController.h
//  hypOps
//
//  Created by Ian Firth on 26/04/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XenVM.h"
#import "SlideToCancelViewController.h"
#import "GradientView.h"

@interface XenVMPowerViewController : UITableViewController<HypervisorConnectionDelegate, SlideToCancelDelegate> {
    NSString *xenVMRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;
    GradientView *disableView;
    int powerOpINProgress;
}
- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenVMRef;
@property int powerOpINProgress;
@property (retain) SlideToCancelViewController *slideToCancel;

@end


