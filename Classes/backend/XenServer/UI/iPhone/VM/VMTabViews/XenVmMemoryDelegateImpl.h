//
//  XenVmMemoryDelegateImp.h
//  hypOps
//
//  Created by Ian Firth on 05/10/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RRDDisplayButtonDelegate.h"
#import "XenVM.h"

@interface XenVmMemoryDelegateImpl : NSObject<RRDDisplayButtonDelegate>{
    NSString *xenVmRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;

    UINavigationController* navigationController;
}

- (id)initWithhypervisorConnectionID:(NSString *)hypervisorConnectionID withVm:(XenVM *)thisXenHost navigationController:(UINavigationController*)navcon;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenVmRef;

@end
