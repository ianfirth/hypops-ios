//
//  XenVMDiskViewController.m
//  hypOps
//
//  Created by Ian Firth on 05/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenVMDiskViewController.h"
#import "XenVBD.h"
#import "XenVDI.h"
#import "XenStorage.h"
#import "ScrollableDetailTextCell.h"
#import "RRDDisplayButton.h"
#import "DataSource.h"
#import "BitNumberFormatter.h"

#define RRDButtonCell @"RRD"
#define GeneralCell @"General"

@interface XenVMDiskViewController ()
- (UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier;
- (UITableViewCell*) configureCellForTableView:(UITableView *)tableView forSection:(int)section atIndex:(int) index;
-(XenVDI *) vdiForSection:(NSInteger)section;
-(XenVBD *) vbdForSection:(NSInteger)section;
-(XenStorage *) storageForSection:(NSInteger)section;
-(XenVBD *) vbdForIndex:(NSInteger)index;
- (void) resetTableDataSource;
@end

@implementation XenVMDiskViewController

@synthesize hypConnID, xenVMRef, CDs, disks;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenVM *) xenVM{
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVMRef]]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }
    return nil;
}


- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM navigationController:(UINavigationController *)navcon{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        navigationController = [navcon retain];
        rrdDiskButtons = [[NSMutableDictionary alloc] init];
        [self setXenVMRef:  [thisXenVM opaque_ref]];
        [self setHypConnID: [hypervisorConnection connectionID]];
    }
    return self;
    
}

- (void)dealloc {
    [rrdDiskButtons release];
    [navigationController release];
    [self setCDs:nil];
    [self setDisks:nil];
    [self setXenVMRef:nil];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [self setHypConnID:nil];
    [super dealloc];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    [tableView release];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[[UIView alloc] init] autorelease]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    [xenHypervisorConnection addHypervisorConnectionDelegate:self];
    [self resetTableDataSource];
}

-(void) viewDidAppear:(BOOL)animated{
    // this gets the text in the labels scrolling again when the view appear, if this is not
    // here, then the text will not start scrolling again until selected. or moved off the screen
    // and back again.
    [[self tableView] reloadData];
    [super viewDidAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

// provide access to the cell definitions
-(UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier] autorelease];
    }
    
    // general cells can have different components in them remove them if there are
    if (identifier == GeneralCell){
        [[[cell contentView] viewWithTag:100] removeFromSuperview];
    }

    return cell;    
}

#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to vm disk objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    // could cut this down to just the relevent ones if required
    if (objectType == HYPOBJ_VBD || objectType == HYPOBJ_VDI || objectType == HYPOBJ_STORAGE){
        [[self tableView ]reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source
- (void) resetTableDataSource{
    [self setCDs:[NSMutableArray arrayWithCapacity:1]];
    [self setDisks:[NSMutableArray arrayWithCapacity:1]];
    NSInteger totalVBDs = [[[self xenVM] referencesForType:HYPOBJ_VBD] count];
    for (int i =0; i < totalVBDs ; i++){
        XenVBD *vbd = [self vbdForIndex:i];
        if ([[vbd type] isEqualToString:@"CD"]){
            [[self CDs] addObject:[vbd opaque_ref]];
        } else {
            [[self disks] addObject:[vbd opaque_ref]];
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of NICs that the host has.
    if (![self CDs] || ![self disks]){
        [self resetTableDataSource];
    }
    return [[self CDs] count] + [[self disks] count];
}

// returns nil if the vdi is not in the cache, this is expected to rectify itself by a subsequent data load
-(XenStorage *) storageForSection:(NSInteger)section{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    XenStorage *storage = nil;
    XenVDI *vdi = [self vdiForSection:section];
    // now go from vdi to the Storage
    NSArray *storageList = [vdi referencesForType:HYPOBJ_STORAGE];
    if ([storageList count] >0){
        NSString *storageRef = [storageList objectAtIndex:0];
        NSArray *vstorageList2 = [xenHypervisorConnection hypObjectsForType:HYPOBJ_STORAGE withCondition:[XenBase XenBaseFor:storageRef]];
        if ([vstorageList2 count] >0){
            storage = [vstorageList2 objectAtIndex:0];
        }
    }
    return storage;
}

// returns nil if the vdi is not in the cache, this is expected to rectify itself by a subsequent data load
-(XenVDI *) vdiForSection:(NSInteger)section{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    XenVDI *vdi = nil;
    XenVBD *vbd = [self vbdForSection:section];
    // now go from vbd to the VDI
    NSArray *vdiList = [vbd referencesForType:HYPOBJ_VDI];
    if ([vdiList count] >0){
        NSString *vdiRef = [vdiList objectAtIndex:0];
        NSArray *vdis = [xenHypervisorConnection hypObjectsForType:HYPOBJ_VDI withCondition:[XenBase XenBaseFor:vdiRef]];
        if ([vdis count] >0){
            vdi = [vdis objectAtIndex:0];
        }
    }
    return vdi;
}

// returns nil if the vbd is not in the cache, this is expected to rectify itself by a subsequent data load
-(XenVBD *) vbdForIndex:(NSInteger)index{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    XenVBD *vbd = nil;
    NSString *opRef = [[[self xenVM] referencesForType:HYPOBJ_VBD] objectAtIndex:index];
    NSArray *vbds = [xenHypervisorConnection hypObjectsForType:HYPOBJ_VBD withCondition:[XenBase XenBaseFor:opRef]];
    if ([vbds count] >0){
        vbd = [vbds objectAtIndex:0];
    }
    return vbd;
}

// returns nil if the vbd is not in the cache, this is expected to rectify itself by a subsequent data load
-(XenVBD *) vbdForSection:(NSInteger)section{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    XenVBD *vbd = nil;
    NSString *opRef;
    if (section < [[self CDs] count])
    {
        opRef = [[self CDs] objectAtIndex:section];
    }else
    {
        opRef = [[self disks] objectAtIndex:section-[[self CDs] count]];
    }
// was this    NSString *opRef = [[[self xenVM] referencesForType:HYPOBJ_VBD] objectAtIndex:section];
    NSArray *vbds = [xenHypervisorConnection hypObjectsForType:HYPOBJ_VBD withCondition:[XenBase XenBaseFor:opRef]];
    if ([vbds count] >0){
        vbd = [vbds objectAtIndex:0];
    }
    return vbd;
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    XenVBD *vbd= [self vbdForSection:section];
    if (section <  [[self CDs] count]){
        return [NSString stringWithFormat:@"DVD Drive"];
    }
    else {
        return [NSString stringWithFormat:@"Disk:%@", [vbd position]];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
   // return disk count if a disk and CD count if a CD
   if (section <  [[self CDs] count]){
        return XENVM_CD_TABLE_COUNT;
    }
    else{
        return XENVM_DISK_TABLE_COUNT;
    }
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // CD/DVD drives are always expanded
    return (section >= [[self CDs] count]);
}

//- (CGFloat)tableView:(UITableView *)tableView heightForUnExpandedHeaderInSection:(NSInteger)section{
//    
//}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self configureCellForTableView:tableView forSection:[indexPath section] atIndex:[indexPath row]];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (UITableViewCell*) configureCellForTableView:(UITableView *)tableView forSection:(int)section atIndex:(int) index{
    
    UITableViewCell *cell = nil;
    
    if (section <  [[self CDs] count]){
        switch (index) {
            case XENVM_CD_TABLE_LOADEDISO:{
                cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
                XenVDI *vdi = [self vdiForSection:section];
                [[cell textLabel] setText:@"Disk"];
                NSString *content = [vdi location];
                if (content == nil || [content isEqualToString:@""])
                {
                    content = @"<empty>";
                }
                [[cell detailTextLabel] setText:content];
                break;
            }
            default:
                break;
        }  
    }
    else{
        switch (index) {
            case XENVM_DISK_TABLE_DESCRIPTION:{
                cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
                XenVDI *vdi = [self vdiForSection:section];
                [[cell textLabel] setText:@"Description"];
                [[cell detailTextLabel] setText:[vdi name_description]];
                break;
            }
            case XENVM_DISK_TABLE_NAME:{
                cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
                XenVDI *vdi = [self vdiForSection:section];
                [[cell textLabel] setText:@"Name"];
                [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%@",[vdi name_label]]];
                break;
            }
            case XENVM_DISK_TABLE_SIZE:{
                cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
                XenVDI *vdi = [self vdiForSection:section];
                [[cell textLabel] setText:@"Size"];
                NSNumber *sizeBytes = [vdi virtual_size];
                float sizeGb = ((float)[sizeBytes longLongValue])/1024/1024/1024;
                if (sizeGb < 0.9){
                    float sizeMb = ((float)[sizeBytes longLongValue])/1024/1024;
                    [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Mb",sizeMb]];
                }
                else{
                    [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Gb",sizeGb]];
                }
                break;
            }
            case XENVM_DISK_TABLE_DEVICE:{
                cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
                XenVBD *vbd = [self vbdForSection:section];
                [[cell textLabel] setText:@"Device"];
                [[cell detailTextLabel] setText: [vbd device]];
                break;
            }
            case XENVM_DISK_TABLE_READONLY:{
                cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
                XenVDI *vdi = [self vdiForSection:section];
                [[cell textLabel] setText:@"Read Only"];
                [[cell detailTextLabel] setText:[vdi read_only]?@"Yes":@"No"];
                break;
            }
            case XENVM_DISK_TABLE_SR:{
                cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
                XenStorage *storage = [self storageForSection:section];
                [[cell textLabel] setText:@"Storage"];
                [[cell detailTextLabel] setText:[storage name_label]];
                break;
            }
            case XENVM_DISK_TABLE_PERFORMANCEDATA:{
                cell = [self getCellForTableView:tableView andIdentifier:RRDButtonCell];
                [[cell detailTextLabel] setText:@""];
                [[cell textLabel] setText:@""];
                XenVBD *vbd = [self vbdForSection:section];
                RRDDisplayButton* rrdButton = [rrdDiskButtons objectForKey:[vbd uuid]];
                if (!rrdButton){
                    rrdButton = [[RRDDisplayButton alloc] init];
                    [rrdDiskButtons setObject:rrdButton forKey:[vbd uuid]];
                    [rrdButton setDelegate:self];
                    [[rrdButton view ]setTag:100];
                    [rrdButton setReference:[[self vbdForSection:section ] device]];
                    [rrdButton release];
                }
                [[rrdButton view ]setFrame:[[cell contentView] bounds]];
                [[cell contentView] setAutoresizesSubviews:YES];
                [[cell contentView] addSubview:[rrdButton view]];
                [rrdButton viewWillAppear:YES];
                break;
            }
            default:
                break;
        }  
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];

    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


#pragma mark -
#pragma mark RRDDisplayButtonDelegate

- (RoundRobinDatabase *)roundRobinDatabase{
    XenVM *xenVM = [self xenVM];
    return [xenVM vmPerformanceData];
}

- (BOOL) reloadRoundRobinDatabase{
    XenVM *xenVM = [self xenVM];
    return [xenVM refreshPerformanceData];
}

-(NSDate*) getLastRefreshTime{
    XenVM *xenVM = [self xenVM];
    return [xenVM lastPreformanceDataUpdateTime];
}

-(NSString*)  graphTitleWithReference:(NSString*)sourcereference{
    return [NSString stringWithFormat:@"Disk usage for %@", sourcereference];
}

-(UINavigationController*) navigationController{
    return navigationController;
}

-(NSArray*)dataSourcesToGraphFromRRD:(RoundRobinDatabase*)rrd withReference:(NSString*)sourcereference{
    NSMutableArray* sourceArray = [[NSMutableArray alloc] initWithCapacity:1];
    // need to convert source referece to the actual reference here
    NSString *storageRef = [NSString stringWithFormat:@"vbd_%@_",sourcereference];
    for (DataSource* dataSource in [rrd dataSources]) {
        if ([[dataSource name] hasPrefix:storageRef]){
            if ([[dataSource name] hasSuffix:@"write"]){
                [dataSource setAlternateName:@"Write"];
                [sourceArray addObject:dataSource];
            }
            if ([[dataSource name] hasSuffix:@"read"]){
                [dataSource setAlternateName:@"Read"];
                [sourceArray addObject:dataSource];
            }
        }
    }
    return [sourceArray autorelease];
}

-(double)yMinForDataSources:(NSArray*)dataSources withConsolidationStep:(int)step{
    return 0;
}

-(double) getPlotPointFromDataPoint:(DataPoint*)dataPoint{
    // beacuse using a minmanl RRD all points are the same
    return [dataPoint average];
    //    double displayVal = [dataPoint average];
    //    if (displayVal == 0){
    //        displayVal = [dataPoint max];
    //    }
    //    // NSLog(@"%f",displayVal);
    //    return displayVal;    
}

-(double)roundup:(double)inputNumber{
    // round up the the value to the nearest value on a 10's boundary
    int resultInt = inputNumber;
    int u = 0;
    while (resultInt > 1){
        resultInt /= 10;
        u ++;
    };
    
    float divisor = pow(10,u-1);
    float resultPower = inputNumber / divisor;
    // round this and multiply by u power 10;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:0];
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    
    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithFloat:resultPower]];
    [formatter release];
    return [numberString intValue] * divisor;
}

-(double)yMaxForDataSources:(NSArray*)dataSources withConsolidationStep:(int)step{
    double result = 0;
    for (DataSource* source in dataSources) {
        NSLog(@"DataSourceName = %@",[source name]);
        NSLog(@"Consolidation Step = %d", step);
        for (DataPoint* point in [source dataPointsForConsolodatedStepCount:step]) {
            double val = [self getPlotPointFromDataPoint:point];
            if (val>result){
                result = val;
            }
        }
    }
    return [self roundup:result];
}

-(NSString*)yTitle{
    return @"bps";
}

-(NSNumberFormatter*) yFormatter{
    NSNumberFormatter* formatter = [[BitNumberFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    return [formatter autorelease];
}

@end
