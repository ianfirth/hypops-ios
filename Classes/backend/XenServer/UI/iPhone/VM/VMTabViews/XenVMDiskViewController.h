//
//  XenVMDiskViewController.h
//  hypOps
//
//  Created by Ian Firth on 05/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XenVM.h"
#import "XenHypervisorConnection.h"
#import "ExpandableTableViewController.h"
#import "RRDDisplayButtonDelegate.h"

#define XENVM_DISK_TABLE_NAME 0
#define XENVM_DISK_TABLE_DESCRIPTION 1
#define XENVM_DISK_TABLE_SIZE 2
#define XENVM_DISK_TABLE_SR 3
#define XENVM_DISK_TABLE_READONLY 4
#define XENVM_DISK_TABLE_DEVICE 5
#define XENVM_DISK_TABLE_PERFORMANCEDATA 6
#define XENVM_DISK_TABLE_COUNT 7

#define XENVM_CD_TABLE_LOADEDISO 0
#define XENVM_CD_TABLE_COUNT 1

@interface XenVMDiskViewController :ExpandableTableViewController<HypervisorConnectionDelegate, RRDDisplayButtonDelegate>{
    NSString *xenVMRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;
    // list of references for disks
    NSMutableArray *disks;
    //list of references for CD/DVD drives
    NSMutableArray *CDs;

    UINavigationController* navigationController;
    
    // stores a list of rrdButtons for each nic
    // so that only one button is created for each object.
    NSMutableDictionary *rrdDiskButtons;

}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM navigationController:(UINavigationController *)navcon;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenVMRef;
@property (retain) NSMutableArray *disks;
@property (retain) NSMutableArray *CDs;
@end
