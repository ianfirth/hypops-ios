//
//  XenVMGeneralViewController.h
//  hypOps
//
//  Created by Ian Firth on 04/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XenVM.h"
#import "XenHypervisorConnection.h"
#import "ExpandableTableViewController.h"
#import "RRDDisplayButton.h"

#define XENVM_SECTION_GENERAL 0
#define XENVM_SECTION_MEMORY 1
#define XENVM_SECTION_CPU 2
#define XENVM_SECTION_BOOTORDER 3
#define XENVM_SECTION_COUNT 4

// general table
#define XENVMDETAILSTABLEVIRTUALIZATIONSTATE 0
#define XENVMDETAILSTABLEOS 1
#define XENVMDETAILSTABLEBIOSSTRINGCOPY 2
#define XENVMDETAILSTABLEAUTOBOOT 3
#define XENVMDETAILSTABLEIPADDRESS 4
#define XENVMDETAILSTABLECOUNT 5

// Memory table
#define XENVM_MEMORY_TABLE_MAXAVAILABLE 0   // static MAX
#define XENVM_MEMORY_TABLE_MINREQUIRED 1    // Static MIN
#define XENVM_MEMORY_TABLE_DYANAMICMAX 2   
#define XENVM_MEMORY_TABLE_DYNAMIXMIN 3    
#define XENVM_MEMORY_TABLE_PERFORMANCEDATA 4
#define XENVM_MEMORY_TABLE_COUNT 5

// CPU Table
#define XENVM_CPU_TABLE_CPUCOUNT 0
#define XENVM_CPU_TABLE_PERFORMANCEDATA 1
#define XENVM_CPU_TABLE_COUNT 2


//number of CPUS
// is using intellicache ( are there any intelli cache stats?  if so possbly in the disk section?
//cpu usage performance data
// memory usage performance data

@interface XenVMGeneralTableViewController :ExpandableTableViewController<HypervisorConnectionDelegate>{ // ExpandableTableViewController<XenHypervisorConnectionDelegate>{
    NSString *xenVMRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;
    
    UINavigationController* navigationController;
    
    RRDDisplayButton* rrdCPUButton;
    RRDDisplayButton* rrdMemoryButton;
}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM navigationController:(UINavigationController*)navcon;
- (void) buildImageViewCallback:(UIImageView *)objectImageView;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenVMRef;

@end
