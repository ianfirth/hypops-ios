//
//  XenVMSnaphostViewController.h
//  hypOps
//
//  Created by Ian Firth on 07/11/2011.
//  Copyright (c) 2011 Ian Firth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XenVM.h"
#import "TableViewRelationshipDataSource.h"
#import "SlideToCancelViewController.h"
#import "GradientView.h"
#import "RelatedListOverlayView.h"
#import "ProgressAlert.h"

@interface XenVMSnaphostViewController : UIViewController<UITableViewDataSource,UITableViewDelegate, TableViewRelationshipDataSource, HypervisorConnectionDelegate,SlideToCancelDelegate, UIScrollViewDelegate,
        UIAlertViewDelegate>{
    NSString *xenVMRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;
    
    UITableViewController* c;
    RelatedListOverlayView *overlayView;
    
    UIButton* newSnapshotButton;
    UIButton* revertToSnapshotButton;
    UIButton* deleteSnapshotButton;
    
    NSArray *snapshotList;
    NSSortDescriptor* snapshotSorter;

    NSArray *currentParentPath;
    
    NSString* selectedXenVMReference;
    
    int pointCount;
    float xLocation;
    float yLocation;
    float yStartLocation;
    
    GradientView *disableView;    
    
    ProgressAlert* progressAlert;
}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM;
- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenVMRef;
@property (retain) SlideToCancelViewController *slideToCancel;

@end
