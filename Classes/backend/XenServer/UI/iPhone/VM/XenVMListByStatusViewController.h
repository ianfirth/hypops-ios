//
//  VMListByStatusViewController.h
//  hypOps
//
//  Created by Ian Firth on 03/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XenHypervisorConnection.h"
#import "PullRefreshTableViewController.h"

#define XEN_VM_ALL 0
#define XEN_VM_TAGGED 1
#define XEN_VM_RUNNING 2
#define XEN_VM_PAUSED 3
#define XEN_VM_HALTED 4
#define XEN_VM_SUSPENDED 5

@interface XenVMListByStatusViewController : PullRefreshTableViewController <HypervisorConnectionDelegate> {
    NSDictionary *xenVMCatagories;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil hypervisorConnection:(XenHypervisorConnection *)hypervisorConnection;

@property (nonatomic, retain) NSDictionary *xenVMCatagories;
@property (copy) NSString *hypConnID;

@end
