//
//  VMListViewController.m
//  hypOps
//
//  Created by Ian Firth on 03/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenVMListViewController.h"
#import "XenGeneralViewController.h"
#import "XenVMGeneralTableViewController.h"
#import "XenVMDiskViewController.h"
#import "XenVMPowerViewController.h"
#import "XenVMSnaphostViewController.h"
#import "XenImageBuilder.h"
#import "ImageBuilder.h"
#import "XenVM.h"

@implementation XenVMListViewController

#pragma mark -
#pragma mark View lifecycle

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withCondition:(NSPredicate *) condition{
    self = [super initWithHypervisorConnection:hypervisorConnection withCondition:condition forHypObjectType:HYPOBJ_VM  withSearch:YES];
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

// render the cell for the table view
- (void) configureCell:(UITableViewCell *)cell forXenObject:(XenBase *)xenObject{
    // set the lable text for the cell
    [[cell textLabel] setText:[(XenDescriptiveBase *)xenObject name_label]];
    // set the detail text for the cell
    [[cell detailTextLabel] setText:[(XenDescriptiveBase *)xenObject name_description]];
    // set the image for the cell
    [XenImageBuilder buildVMImage:[cell imageView] ForVM:(XenVM *)xenObject];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
}


// called when an object is selected in the list
- (void) didSelectObject:(XenBase *)selectedObject{
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    NSMutableArray *localControllersArray = [[NSMutableArray alloc] initWithCapacity:2];

    // add the general tab
    XenVMGeneralTableViewController *tbc = [[XenVMGeneralTableViewController alloc] initWithhypervisorConnection: xenHypervisorConnection
                                                                                                          withVM:(XenVM *)selectedObject
                                            navigationController:[self navigationController]];
    
   
    XenGeneralViewController *vmGeneral = [[XenGeneralViewController alloc] initWithHypervisorConnection:xenHypervisorConnection
                                                                       xenDescritiveBase:(XenDescriptiveBase *)selectedObject  
                                                                             displayMode:DISPLAYMODE_NAMEDESCRIPTION
                                                                                 tabName:@"General"
                                                                         tabBarImageName:@"tab_General" 
                                                                                   image:[ImageBuilder vMImage] 
                                                                 extentionViewController:tbc];
    [localControllersArray addObject:vmGeneral];
    [tbc release];
    [vmGeneral release];
    
    // add the disks tab
    XenVMDiskViewController *disks = [[XenVMDiskViewController alloc] initWithhypervisorConnection: xenHypervisorConnection
                                                                                             withVM:(XenVM *)selectedObject
                                      navigationController:[self navigationController]];

    
    XenGeneralViewController *vmDisks = [[XenGeneralViewController alloc] initWithHypervisorConnection:xenHypervisorConnection
                                                                                     xenDescritiveBase:(XenDescriptiveBase *)selectedObject  
                                                                                           displayMode:DISPLAYMODE_NAME
                                                                                               tabName:@"Disks"
                                                                                       tabBarImageName:@"tab_Storage" 
                                                                                                 image:[ImageBuilder vMImage] 
                                                                               extentionViewController:disks];

    [localControllersArray addObject:vmDisks];
    [disks release];
    [vmDisks release];

    // add the power Control tab
    XenVMPowerViewController *power = [[XenVMPowerViewController alloc] initWithhypervisorConnection: xenHypervisorConnection
                                                                                            withVM:(XenVM *)selectedObject];
    
    
    XenGeneralViewController *vmpower = [[XenGeneralViewController alloc] initWithHypervisorConnection:xenHypervisorConnection
                                                                                     xenDescritiveBase:(XenDescriptiveBase *)selectedObject  
                                                                                           displayMode:DISPLAYMODE_NAME
                                                                                               tabName:@"Control"
                                                                                       tabBarImageName:@"tab_Power" 
                                                                                                 image:[ImageBuilder vMImage] 
                                                                               extentionViewController:power];
    
    [power release];
    [localControllersArray addObject:vmpower];
    [vmpower release];

    // add the snapshot tab
    XenVMSnaphostViewController *snapshots = [[XenVMSnaphostViewController alloc] initWithhypervisorConnection: xenHypervisorConnection
                                                                                              withVM:(XenVM *)selectedObject];
    
    XenGeneralViewController *vmSnapshots = [[XenGeneralViewController alloc] initWithHypervisorConnection:xenHypervisorConnection
                                                                                     xenDescritiveBase:(XenDescriptiveBase *)selectedObject  
                                                                                           displayMode:DISPLAYMODE_NAME
                                                                                               tabName:@"Snapshots"
                                                                                       tabBarImageName:@"tab_Snapshot" 
                                                                                                 image:[ImageBuilder vMImage] 
                                                                               extentionViewController:snapshots];
    
    [snapshots release];
    [localControllersArray addObject:vmSnapshots];
    [vmSnapshots release];

    
    tabBarController.viewControllers = localControllersArray;
    [localControllersArray release];
    
    [tabBarController setTitle:@"Virtual Machine"];

    [self displayNextView:tabBarController];
    
    [tabBarController release];
}


@end