//
//  XenPoolController.h
//  hypOps
//
//  Created by Ian Firth on 20/02/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIWaitForDataLoad.h"
#import "InitialConnectionViewController.h"

@interface XenPoolViewController : InitialConnectionViewController <UIWaitForDataLoadDelegate>{

    // The objects to be displayed in the table view
    NSArray *xenCatagories;
}

- (void)refreshButtonClick:(id)sender;
- (UIViewController *) detailContentView;

@property (nonatomic, retain) NSArray *xenCatagories;

@end
