//
//  BitNumberFormatter.m
//  hypOps
//
//  Created by Ian Firth on 10/10/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "BitNumberFormatter.h"

@implementation BitNumberFormatter

-(NSString*)stringForObjectValue:(id)obj{
    NSNumber* byteNum = (NSNumber*)obj;
    double bitNum = [byteNum doubleValue] *8; 
    int divisor = 1;
    NSString* postFix = @"M";
    divisor = 1024*1024;
    
    if (bitNum >= 1024*1024*1024){
        postFix = @"G";
        divisor = 1024*1024*1024;
    }
    
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    [formatter setRoundingMode:NSNumberFormatterRoundUp];
    //[formatter setNumberStyle:NSNumberFormatterS];
    [formatter setMaximumSignificantDigits:1];
    [formatter setMinimumIntegerDigits:1];
    [formatter setMaximumFractionDigits:1];
    [formatter setMinimumFractionDigits:1];
    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithDouble:bitNum/divisor]];
    [formatter release];
    return [NSString stringWithFormat:@"%@%@",numberString,postFix];
}
@end
