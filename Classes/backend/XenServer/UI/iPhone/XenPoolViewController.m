//
//  XenPoolViewController.m
//  hypOps
//
//  Created by Ian Firth on 20/02/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenPoolViewController.h"
#import "XenVMListByStatusViewController.h"
#import "XenHostListViewController.h"
#import "XenTemplateListViewController.h"
#import "XenStorageListViewController.h"
#import "XenNetworkListViewController.h"
#import "ImageBuilder.h"
#import "UIWaitForDataLoad.h"
#import "XenPoolViewController_ipad.h"


// define the private methods
@interface XenPoolViewController ()
@end

@implementation XenPoolViewController

@synthesize xenCatagories;

#pragma mark -
#pragma mark View lifecycle


- (void) viewWillAppear:(BOOL)animated{
    // request data for any object sets that are marked as out of date
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    [xenHypervisorConnection addHypervisorConnectionDelegate:self];

    [super viewWillAppear:animated];
    [[self tableView] reloadData];

}

- (void) viewWillDisappear:(BOOL)animated{
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    [xenHypervisorConnection removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

/*
 * When load set the title and the name of the pool
 * This should only be done to make sure that the connection is populated with all
 * the top level objects the first time that the page is displayed after connection
 * that is why this is in the on load, otherwise it would happen every time the 
 * page appears.
 */
- (void)viewDidLoad {
    // request data for any object sets that are marked as out of date
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    [xenHypervisorConnection setWalkTreeMode:NO];
  
    [self setXenCatagories:[NSArray arrayWithObjects:
                            [NSNumber numberWithInt:HYPOBJ_HOST],
                            [NSNumber numberWithInt:HYPOBJ_VM],
                            [NSNumber numberWithInt:HYPOBJ_TEMPLATE],
                            [NSNumber numberWithInt:HYPOBJ_STORAGE],
                            [NSNumber numberWithInt:HYPOBJ_NETWORK],
                            nil]];    
   
    [xenHypervisorConnection reloadCoreData];
    
    [super viewDidLoad];
    self.title = @"Xen Pool";
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    float frameWidth = [[self view] frame].size.width;

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    NSNumber *xenCatagory = [[self xenCatagories] objectAtIndex:indexPath.row];
    NSString *displayText;
    NSString *detailTextHeader; 
    UIImage *image = nil;
    BOOL isLoading = NO;
    BOOL isUpdateAvailable = YES;
    BOOL validCatagory = YES;
    int hypObjType = [xenCatagory intValue];
    int objectCount = 0;
    
    switch (hypObjType) {
        case HYPOBJ_VM:
            displayText = @"VMs";
            image = [ImageBuilder vMImage];
            detailTextHeader = @"VM Count:%d";
            break;
        case HYPOBJ_HOST:
            displayText = @"Hosts";
            image = [ImageBuilder hostImage];
            detailTextHeader = @"Host Count:%d";
            break;
        case HYPOBJ_TEMPLATE:
            displayText = @"Templates";
            image = [ImageBuilder vMTemplateImage];
            detailTextHeader = @"Template Count:%d";
            break;
        case HYPOBJ_STORAGE:
            displayText = @"Storage";
            image = [ImageBuilder storageImage];
            detailTextHeader = @"Storage Count:%d";
            break;
        case HYPOBJ_NETWORK:
            displayText = @"Network";
            image = [ImageBuilder networkImage];
            detailTextHeader = @"Network Count:%d";
            break;
        default:
            validCatagory = NO;
            break;
    }
    
    if (validCatagory){
        // get the content view
        UIView *content = [cell contentView];
        // set the tag to be the type of object being displayed in the cell
        content.tag = hypObjType;
        
        // remove any existing button or spinner from the view
        [[content viewWithTag:100] removeFromSuperview];
        
        NSString *detailText;
        isUpdateAvailable = [xenHypervisorConnection isUpdateAvailableForType:hypObjType];
        isLoading =  [xenHypervisorConnection isUpdatePendingForType:hypObjType];

        if (!isLoading){
            objectCount = [[xenHypervisorConnection hypObjectsForType:hypObjType] count];
            detailText = [NSString stringWithFormat:detailTextHeader,objectCount];
            // add the navigation indicator to the cell
            if (objectCount > 0){
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
            [cell.imageView setAlpha:1.0];
        }
        else {
            UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            spinner.tag = 100;
            CGRect rect = CGRectMake(frameWidth - 30, 10.0, 20.0, 20.0);
            [spinner setFrame:rect];
            [content addSubview:spinner];
            [spinner startAnimating];
            [spinner release];
            // remove the navigation indicator to the cell
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell.imageView setAlpha:0.5];
            detailText = @"Loading....";
        }
        
        // set the default selection style
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];

        // put the refresh button in place
        if (!isLoading && ![xenHypervisorConnection isAutoUpdateEnabled] && isUpdateAvailable){
            // construct the refresh button
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button addTarget:self action:@selector(refreshButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            CGRect rect = CGRectMake(frameWidth - 70, 5, 40.0, 40.0);
            [button setFrame:rect];
            [button setBackgroundImage:[UIImage imageNamed:@"Refresh"] forState:UIControlStateNormal];
            button.tag = 100;
            [content addSubview:button];
        }
        // set the image for the cell
        [[cell imageView] setImage:image];  
        [[cell textLabel] setText:displayText];
        
        // set the detail text
        if (detailText != nil) {
            [[cell detailTextLabel] setText:detailText];
        }
    }        
    else {
        NSLog(@"Invalid cell catagory provided %@",xenCatagory);
    }
}

-(void)refreshButtonClick:(id)sender{
    // get the button tag
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    int hypObjectType = [[sender superview] tag];
    [xenHypervisorConnection RequestHypObjectsForType:hypObjectType];
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark HypervisorConnectionDelegate protocol

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withResult:(BOOL)sucsess{
    [[self tableView] reloadData];    
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.xenCatagories count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    NSNumber *xenCatagory = [self.xenCatagories objectAtIndex:indexPath.row];
    int hypObjType = [xenCatagory intValue];
    if (![xenHypervisorConnection isUpdatePendingForType:hypObjType]){
        // check that the host objects are loaded into cache
        UIWaitForDataLoad *dataLoader = [[UIWaitForDataLoad alloc] initWithHypervisorConnection:xenHypervisorConnection];
        [dataLoader setUIWaitForDataLoadDelegate:self];
        NSLog(@"Created dataloader to wait for data to be loaded for the next page");
        [dataLoader waitForObjectsOfType:hypObjType];
    }
}

#pragma mark -
#pragma mark UIWaitForDownload delegate

- (void)dataLoaderFinished:(UIWaitForDataLoad *)dataLoader withResult:(BOOL)sucsess{
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    NSLog(@"dataloader reported that it has loaded the required data.");
    
    NSError *err = [dataLoader connectionError];
    int hypObjType = [dataLoader requestType];
    [dataLoader release];

    if (err){
        // if there was an error connecting do not do anyting.
        return;
    }

    switch (hypObjType) {
        case HYPOBJ_VM:{
            XenVMListByStatusViewController *vMListByStatusViewController = [[[XenVMListByStatusViewController alloc] initWithNibName:@"XenVMListByStatus" bundle:nil hypervisorConnection:xenHypervisorConnection] autorelease];
            [self.navigationController pushViewController:vMListByStatusViewController animated:YES];
            break;
        }
        case HYPOBJ_HOST:{
            XenHostListViewController *hostListViewController = [[[XenHostListViewController alloc] initWithHypervisorConnection:xenHypervisorConnection  withCondition:nil]autorelease];
            [self.navigationController pushViewController:hostListViewController animated:YES];
            break;
        }
        case HYPOBJ_TEMPLATE:{
            XenTemplateListViewController *templateListViewController = [[[XenTemplateListViewController alloc] initWithHypervisorConnection:xenHypervisorConnection  withCondition:nil] autorelease];
            [self.navigationController pushViewController:templateListViewController animated:YES];
            break;
        }
        case HYPOBJ_STORAGE:{
            XenStorageListViewController *storageListViewController = [[[XenStorageListViewController alloc] initWithHypervisorConnection:xenHypervisorConnection  withCondition:nil] autorelease];
            [self.navigationController pushViewController:storageListViewController animated:YES];
            break;
        }
        case HYPOBJ_NETWORK:{
            XenNetworkListViewController *networkListViewController = [[[XenNetworkListViewController alloc] initWithHypervisorConnection:xenHypervisorConnection  withCondition:nil] autorelease];

            [self.navigationController pushViewController:networkListViewController animated:YES];
            break;
        }
        default:
            break;
    }
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // dont do this abnymore.
    // since I cant distinguish between memory warning levels this will happen even in a 
    // low priority one.
//    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
//    [xenHypervisorConnection closeConnection];
//    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)dealloc {
    [super dealloc];
}

- (UIViewController *) detailContentView{
    UIViewController *poolView = [[XenPoolViewController_ipad alloc] initWithNibName:@"XenPool_ipad" bundle:[NSBundle mainBundle]];
    return [poolView autorelease];
}

@end

