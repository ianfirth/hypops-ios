//
//  XenHostListViewController.m
//  hypOps
//
//  Created by Ian Firth on 10/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenHostListViewController.h"
#import "XenGeneralViewController.h"
#import "XenHostGeneralTableViewController.h"
#import "XenHostNicsTableViewController.h"
#import "XenHostStorageTableViewController.h"
#import "ImageBuilder.h"
#import "XenHost.h"

@implementation XenHostListViewController

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withCondition:(NSPredicate *) condition{
    self = [super initWithHypervisorConnection:hypervisorConnection withCondition:condition forHypObjectType:HYPOBJ_HOST  withSearch:YES];
    self.title = @"Hosts";
    return self;
}

- (void) dealloc{    
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

// render the cell for the table view
- (void) configureCell:(UITableViewCell *)cell forXenObject:(XenBase *)xenObject{
    // set the lable text for the cell
    [[cell textLabel] setText:[(XenDescriptiveBase *)xenObject name_label]];
    // set the detail text for the cell
    [[cell detailTextLabel] setText:[(XenDescriptiveBase *)xenObject name_description]];
    // set the image for the cell
    [[cell imageView] setImage:[ImageBuilder hostImage]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
}

// called when an object is selected in the list
- (void) didSelectObject:(XenBase *)selectedObject{
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    XenHost* selectedHost = (XenHost *)selectedObject;

    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    NSMutableArray *localControllersArray = [[NSMutableArray alloc] initWithCapacity:2];
    
    // general details page
    XenHostGeneralTableViewController *generaltvc = [[XenHostGeneralTableViewController alloc] initWithhypervisorConnection:xenHypervisorConnection
                                                                                                              withHost:selectedHost
                                                                                                       navigationController:[self navigationController]];
    
    XenGeneralViewController *hostGeneral = [[XenGeneralViewController alloc] initWithHypervisorConnection:xenHypervisorConnection
                                                                         xenDescritiveBase:(XenDescriptiveBase *)selectedObject 
                                                                               displayMode:DISPLAYMODE_NAMEDESCRIPTION
                                                                                   tabName:@"General"
                                                                           tabBarImageName:@"tab_General" 
                                                                                     image:[ImageBuilder hostImage] 
                                                                   extentionViewController:generaltvc];
    
    [localControllersArray addObject:hostGeneral];
    [hostGeneral release];
    [generaltvc release];     
    
    // Nics page
    XenHostNicsTableViewController *nicstvc = [[XenHostNicsTableViewController alloc] initWithhypervisorConnection: xenHypervisorConnection
                                                                                                          withHost:selectedHost
                                                                                              navigationController:[self navigationController]];
    
    XenGeneralViewController *hostNics = [[XenGeneralViewController alloc] initWithHypervisorConnection:xenHypervisorConnection
                                                                      xenDescritiveBase:(XenDescriptiveBase *)selectedObject
                                                                            displayMode:DISPLAYMODE_NAME
                                                                                tabName:@"NICs"
                                                                        tabBarImageName:@"tab_NIC" 
                                                                                  image:[ImageBuilder hostImage]  
                                                                extentionViewController:nicstvc];
    
    [localControllersArray addObject:hostNics];
    [hostNics release];
    [nicstvc release];
    
    // Storage page
    XenHostStorageTableViewController *storagetvc = [[XenHostStorageTableViewController alloc] initWithhypervisorConnection: xenHypervisorConnection
                                                                                                                   withHost:selectedHost
                                                                                                       navigationController:[self navigationController]];
    
    XenGeneralViewController *hostStorage = [[XenGeneralViewController alloc] initWithHypervisorConnection:xenHypervisorConnection
                                                                         xenDescritiveBase:(XenDescriptiveBase *)selectedObject
                                                                               displayMode:DISPLAYMODE_NAME
                                                                                   tabName:@"Storage"
                                                                           tabBarImageName:@"tab_Storage" 
                                                                                     image:[ImageBuilder hostImage]  
                                                                   extentionViewController:storagetvc];
    
    [localControllersArray addObject:hostStorage];
    [hostStorage release];
    [storagetvc release];
    
    
    tabBarController.viewControllers = localControllersArray;
    [localControllersArray release];
    
    [tabBarController setTitle:@"Host"];
    [self displayNextView:tabBarController];
    [tabBarController release];
}

@end
