//
//  XenHostMemoryDelegateImpl.h
//  hypOps
//
//  Created by Ian Firth on 05/10/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RRDDisplayButtonDelegate.h"
#import "XenHost.h"

@interface XenHostMemoryDelegateImpl : NSObject<RRDDisplayButtonDelegate>{
NSString *xenHostRef;
// the connection ID for the current hypervisor connection
NSString *hypConnID;

UINavigationController* navigationController;
}

- (id)initWithhypervisorConnectionID:(NSString *)hypervisorConnectionID withHost:(XenHost *)thisXenHost navigationController:(UINavigationController*)navcon;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenHostRef;
@end
