//
//  XenVMGeneralViewController.m
//  hypOps
//
//  Created by Ian Firth on 04/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenHostGeneralTableViewController.h"
#import "ScrollableDetailTextCell.h"
#import "RoundRobinDatabase.h"
#import "DataSource.h"
#import "RRDDisplayButton.h"
#import "DataPoint.h"
#import "XenHostMemoryDelegateImpl.h"
#import "XenHostMemoryXapiDelegateImpl.h"
#import "XenVM.h"

#define RRDButtonCell @"RRD"
#define GeneralCell @"General"

@interface XenHostGeneralTableViewController ()
-(UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier;
- (UITableViewCell*) configureDetailsCellForTableView:(UITableView*)tableView atIndex:(int) index;
- (UITableViewCell*) configureTagsCellForTableView:(UITableView*)tableView atIndex:(int) index;
- (UITableViewCell*) configureLicenseCellForTableView:(UITableView*)tableView atIndex:(int) index;
- (UITableViewCell*) configureManagementCellForTableView:(UITableView*)tableView atIndex:(int) index;
- (UITableViewCell*) configureMemoryCellForTableView:(UITableView*)tableView atIndex:(int) index;
- (UITableViewCell*) configureCpuCellForTableView:(UITableView*)tableView atIndex:(int) index;
@end

@implementation XenHostGeneralTableViewController

@synthesize hypConnID, xenHostRef;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenHost *) xenHost{
    NSArray *xenHosts = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST withCondition:[XenBase XenBaseFor:[self xenHostRef]]];
    if ([xenHosts count] >0){
        return [xenHosts objectAtIndex:0];
    }
    return nil;
}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withHost:(XenHost *)thisXenHost navigationController:(UINavigationController*)navcon{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setXenHostRef: [thisXenHost opaque_ref]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        navigationController = [navcon retain];
        rrdXapiButtons = [[NSMutableDictionary alloc] init];
        rrdMemoryButtons = [[NSMutableDictionary alloc] init];
        rrdCpuButtons = [[NSMutableDictionary alloc] init];
    }
    return self;
}


- (void)dealloc {
    [rrdXapiButtons release];
    [rrdMemoryButtons release];
    [rrdCpuButtons release];

    [navigationController release];
    [self setXenHostRef: nil];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [self setHypConnID:nil];
    [super dealloc];
}

- (void) viewWillAppear:(BOOL)animated{
     XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    [xenHypervisorConnection addHypervisorConnectionDelegate:self];
    [xenHypervisorConnection setWalkTreeMode:NO];
   
    [[self tableView] reloadData];
    [super viewWillAppear:animated];
}

-(void) viewDidAppear:(BOOL)animated{
    // this gets the text in the labels scrolling again when the view appear, if this is not
    // here, then the text will not start scrolling again until selected. or moved off the screen
    // and back again.
    [[self tableView] reloadData];
    [super viewDidAppear:animated];
}

-(void) viewDidDisappear:(BOOL)animated{
    // make sure that buttons are freed
    // this makes sure that the delegates are released
    // this enables this to be dealloc'd when the parent is
    for (RRDDisplayButton* but in [rrdCpuButtons allValues]) {
        [but setDelegate:nil];
    }
    [rrdCpuButtons removeAllObjects];
    
    for (RRDDisplayButton* but in [rrdMemoryButtons allValues]) {
        [but setDelegate:nil];
    }
    [rrdMemoryButtons removeAllObjects];
    
    for (RRDDisplayButton* but in [rrdXapiButtons allValues]) {
        [but setDelegate:nil];
    }
    [rrdXapiButtons removeAllObjects];
    [super viewDidDisappear:animated];
}

- (void) viewWillDisappear:(BOOL)animated{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    [xenHypervisorConnection removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    [tableView release];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[[UIView alloc] init] autorelease]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // this page does any memory cleen up possible for host objects
    // this will get called if any of the host tabs is visible
    // clean up any RRD data for any VMs snce they cannot be in use at present
    // clean up any RRD data for any other hosts that are not the one currently in use
    // -- Future enhancement if required --> unless the graph is not in us anyway inwhich case can clean up RRD data for this host too

    NSArray* hosts = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST];
    XenHost* currentHost = [self xenHost];
    for (XenHost* host in hosts) {
        if (![[host opaque_ref] isEqualToString:[currentHost opaque_ref]]){
            [host clearHostPerformanceData];
        }
    }

    NSArray* vms = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM];
    for (XenVM* vm in vms) {
            [vm clearVmPerformanceData];
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // Nothing required here, everything is on the view root object, this will be handeled
    // by the XenGeneralViewController.  Only need to listed to obejcts that are
    // not the root object in these 'extention' pages.
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return XENHOST_SECTION_COUNT;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    return (section != 0);
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case XENHOST_SECTION_GENERAL:
            return nil; // No heading required for first section currently
            break;
        case XENHOST_SECTION_TAGS:
            return @"Tags";
            break;
        case XENHOST_SECTION_MANAGEMENT:
            return @"Management";
            break;
        case XENHOST_SECTION_LICENSE:
            return @"License";
            break;
        case XENHOST_SECTION_MEMORY:
            return @"Memory";
            break;
        case XENHOST_SECTION_CPU:
            return @"CPUs";
            break;
        default:
            return @"Error";
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    switch (section) {
        case XENHOST_SECTION_GENERAL:
            return XENHOST_DETAILS_COUNT;
            break;
        case XENHOST_SECTION_TAGS:
            // other section counts
            // Should we always add a spare row on the end to allow a tag to be added to the list?
            // It could have a '+' icon and text that says 'Add a new tag'
            // unless you can add buttons by the headings easily then I will add a plus there
            // this could pop up a list of all tags availalble and allow entry of a new one
            // Note this would be all tags, not just host tags.  Possibly need to keep an array
            // with all tags in aswell as the ones for each VM etc..
            return [[[self xenHost] tags] count];
            break;
        case XENHOST_SECTION_MANAGEMENT:
            return XENHOST_MANAGEMENT_COUNT;
            break;
        case XENHOST_SECTION_LICENSE:
            return XENHOST_LICENSE_COUNT;
            break;
        case XENHOST_SECTION_MEMORY:
            return XENHOST_MEMORY_COUNT;
            break;
        case XENHOST_SECTION_CPU:
            return XENHOST_CPU_COUNT;
            break;
        default:
            return 0;
            break;
    }
}

// provide access to the cell definitions
-(UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier] autorelease];
    }
    
    // general cells can have different components in them remove them if there are
    if (identifier == GeneralCell){
        [[[cell contentView] viewWithTag:100] removeFromSuperview];
    }

    return cell;    
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
            
    switch ([indexPath section]) {
        case XENHOST_SECTION_GENERAL:
            return [self configureDetailsCellForTableView:tableView atIndex:[indexPath row]];
            break;
        case XENHOST_SECTION_TAGS:
            return [self configureTagsCellForTableView:tableView atIndex:[indexPath row]];
            break;
        case XENHOST_SECTION_MANAGEMENT:
            return [self configureManagementCellForTableView:tableView atIndex:[indexPath row]];
            break;
        case XENHOST_SECTION_LICENSE:
            return [self configureLicenseCellForTableView:tableView atIndex:[indexPath row]];
            break;
        case XENHOST_SECTION_MEMORY:
            return [self configureMemoryCellForTableView:tableView atIndex:[indexPath row]];
            break;
        case XENHOST_SECTION_CPU:
            return [self configureCpuCellForTableView:tableView atIndex:[indexPath row]];
            break;
        default:
            break;
    }

    return nil;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (UITableViewCell*)configureDetailsCellForTableView:(UITableView*)tableView atIndex:(int) index {
    UITableViewCell *cell = nil;
     
    XenHost *xenHost = [self xenHost];
    NSString *dateTime;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy (HH:mm)"];
    switch (index) {
        case XENHOST_DETAILS_TABLE_SOFTWARE_DETAILS:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Version"];
            NSString *versionText = [xenHost productVersionText];
            NSString *versionNumber = [NSString stringWithFormat:@"%@ %@",[xenHost productVersionNumber], [xenHost buildNumber]];
             if (versionText){
                NSString *dispText = [NSString stringWithFormat:@"%@ (%@)",versionText, versionNumber];
                [[cell detailTextLabel] setText:dispText];
            }
            else{
                [[cell detailTextLabel] setText:versionNumber];
            }
            break;
        }
        case XENHOST_DETAILS_TABLE_START_TIME:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Started"];
            dateTime = [dateFormatter stringFromDate:[xenHost serverUpTime]];
            [[cell detailTextLabel] setText:dateTime];
            break;
        }
        case XENHOST_DETAILS_TABLE_TOOLS_START_TIME:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Tools Started"];
            dateTime = [dateFormatter stringFromDate:[xenHost toolStackUpTime]];
            [[cell detailTextLabel] setText: dateTime];
            break;
        }
        case XENHOST_DETAILS_TABLE_ISCSI:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"iScsi iqn"];
            [[cell detailTextLabel] setText: [xenHost iscsi_iqn]];
            break;
        }
        case XENHOST_DETAILS_TABLE_CACHEENABLED:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Intellicache Enabled"];
            if ([xenHost hasCacheStoreage]){
                [[cell detailTextLabel] setText: @"Yes"];
            }else{
                [[cell detailTextLabel] setText: @"No"];
            }
            break;
        }
        default:{
            break;
        }
    }    

    [dateFormatter release];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    return cell;
}

- (UITableViewCell*) configureTagsCellForTableView:(UITableView*)tableView atIndex:(int) index{   
    UITableViewCell *cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];

    // remove the button if it is already there
    // because the same cell is used over and over
    [[[cell contentView] viewWithTag:100] removeFromSuperview];

    XenHost *xenHost = [self xenHost];
    NSString *tag = [[xenHost tags] objectAtIndex:index];
    [[cell textLabel] setText:tag];
    [[cell detailTextLabel] setText:@""];
    return cell;
}

- (UITableViewCell*) configureLicenseCellForTableView:(UITableView*)tableView atIndex:(int) index{
        
    UITableViewCell *cell = nil;

    XenHost *xenHost = [self xenHost];
    switch (index) {
        case XENHOST_LICENSE_TABLE_EDITION:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Edition"];
            [[cell detailTextLabel] setText:[xenHost licenseEdition]];
            break;
        }
        case XENHOST_LICENSE_TABLE_EXPIRY:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Expiry"];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd MMM yyyy (HH:mm)"];
            [[cell detailTextLabel] setText:[dateFormatter stringFromDate:[xenHost licenseExipryDate]]];
            [dateFormatter release];
            break;
        }
        case XENHOST_LICENSE_TABLE_ADDRESS:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Address"];
            [[cell detailTextLabel] setText:[xenHost licenseAddress]];
            break;
        }
        case XENHOST_LICENSE_TABLE_PORT:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Port"];
            [[cell detailTextLabel] setText:[xenHost licensePort]];
            break;
        }
        default:{
            break;
        }
    }   
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];

    return cell;
}

- (UITableViewCell*) configureManagementCellForTableView:(UITableView*)tableView atIndex:(int) index{
    
    UITableViewCell *cell = nil;
            
    XenHost *xenHost = [self xenHost];
    switch (index) {
        case XENHOST_MANAGEMENT_TABLE_HOSTNAME:
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Host"];
            [[cell detailTextLabel] setText:[xenHost hostName]];
            break;
        case XENHOST_MANAGEMENT_TABLE_ADDRESS:
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"IP"];
            [[cell detailTextLabel] setText:[xenHost address]];
            break;
        case XENHOST_MANAGEMENT_TABLE_XAPIPERFORMANCE:{
            cell = [self getCellForTableView:tableView andIdentifier:RRDButtonCell];
            [[cell detailTextLabel] setText:@""];
            [[cell textLabel] setText:@""];
            RRDDisplayButton* rrdButton = [rrdXapiButtons objectForKey:[xenHost uuid]];
            if (!rrdButton){
                rrdButton = [[RRDDisplayButton alloc] init];
                [rrdXapiButtons setObject:rrdButton forKey:[xenHost uuid]];
                XenHostMemoryXapiDelegateImpl *delegate = [[XenHostMemoryXapiDelegateImpl alloc] initWithhypervisorConnectionID:[self hypConnID] withHost:[self xenHost] navigationController:navigationController];
                [rrdButton setDelegate:delegate];
                [delegate release];
                [[rrdButton view ]setTag:100];
                [rrdButton release];
            }
            [[rrdButton view ]setFrame:[[cell contentView] bounds]];
            [[cell contentView] setAutoresizesSubviews:YES];
            [[cell contentView] addSubview:[rrdButton view]];
            [rrdButton viewWillAppear:YES];
            break;
        }
        default:
            break;
    }  
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    return cell;
}


- (UITableViewCell*) configureMemoryCellForTableView:(UITableView*)tableView atIndex:(int) index{

    UITableViewCell *cell = nil;
    
    XenHost *xenHost = [self xenHost];
    switch (index) {
        case XENHOST_MEMORY_TABLE_AVAILABLE:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Total"];
            NSNumber *memTotalBytes = [xenHost totalMemory];
            long long memTotal = [memTotalBytes longLongValue]; 
            float finalmemTotalGb = ((float)memTotal)/1024/1024/1024;
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Gb",finalmemTotalGb]];
            break;
        }
        case XENHOST_MEMORY_TABLE_USED:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Used"];
            NSNumber *memTotalBytes = [xenHost totalMemory];
            NSNumber *memFreeBytes = [xenHost freeMemory];
            long long memFree = [memTotalBytes longLongValue] - [memFreeBytes longLongValue]; 
            float finalmemTotalGb = ((float)memFree)/1024/1024/1024;
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Gb",finalmemTotalGb]];
            break;
        }
        case XENHOST_MEMORY_TABLE_PERFORMANCE:{
            cell = [self getCellForTableView:tableView andIdentifier:RRDButtonCell];
            [[cell detailTextLabel] setText:@""];
            [[cell textLabel] setText:@""];

            RRDDisplayButton* rrdButton = [rrdMemoryButtons objectForKey:[xenHost uuid]];
            if (!rrdButton){
                rrdButton = [[RRDDisplayButton alloc] init];
                [rrdMemoryButtons setObject:rrdButton forKey:[xenHost uuid]];
                XenHostMemoryDelegateImpl *delegate = [[XenHostMemoryDelegateImpl alloc] initWithhypervisorConnectionID:[self hypConnID] withHost:[self xenHost] navigationController:navigationController];
                [rrdButton setDelegate:delegate];
                [delegate release];
                [[rrdButton view ]setTag:100];
                [rrdButton release];
            }
            [[rrdButton view ]setFrame:[[cell contentView] bounds]];
            [[cell contentView] setAutoresizesSubviews:YES];
            [[cell contentView] addSubview:[rrdButton view]];
            [rrdButton viewWillAppear:YES];
            break;
        }
        default:
            break;
    }  
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    return cell;
}

- (UITableViewCell*) configureCpuCellForTableView:(UITableView*)tableView atIndex:(int) index{
    
    UITableViewCell *cell = nil;

    XenHost *xenHost = [self xenHost];
    switch (index) {
        case XENHOST_CPU_TABLE_COUNT:
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Count"];
            [[cell detailTextLabel] setText:[xenHost cpuCount]];
            break;
        case XENHOST_CPU_TABLE_VENDOR:
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Vendor"];
            [[cell detailTextLabel] setText:[xenHost cpuVendor]];
            break;
        case XENHOST_CPU_TABLE_MODEL:
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Model"];
            [[cell detailTextLabel] setText:[xenHost cpuModel]];
            break;
        case XENHOST_CPU_TABLE_PERFORMANCEDATA:{
             cell = [self getCellForTableView:tableView andIdentifier:RRDButtonCell];

            [[cell detailTextLabel] setText:@""];
            [[cell textLabel] setText:@""];
            RRDDisplayButton* rrdButton = [rrdCpuButtons objectForKey:[xenHost uuid]];
            if (!rrdButton){
                rrdButton = [[RRDDisplayButton alloc] init];
                [rrdCpuButtons setObject:rrdButton forKey:[xenHost uuid]];
                RRDDisplayButton *rrdButton = [[RRDDisplayButton alloc] init];
                [[rrdButton view ]setTag:100];
                [rrdButton release];
            }
            [rrdButton setDelegate:self];
            [[rrdButton view ]setFrame:[[cell contentView] bounds]];
            [[cell contentView] setAutoresizesSubviews:YES];
            [[cell contentView] addSubview:[rrdButton view]];
            [rrdButton viewWillAppear:YES];
            break;
        }
        default:
            break;
    }  
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark -
#pragma mark RRDDisplayButtonDelegate

- (RoundRobinDatabase *)roundRobinDatabase{
   XenHost *xenHost = [self xenHost];
   return [xenHost hostPerformanceData];
}

- (BOOL) reloadRoundRobinDatabase{
    XenHost *xenHost = [self xenHost];
    return [xenHost refreshPerformanceData];
}

-(NSDate*) getLastRefreshTime{
    XenHost *xenHost = [self xenHost];
    return [xenHost lastPreformanceDataUpdateTime];
}

-(NSString*) graphTitleWithReference:(NSString*)sourcereference{
    XenHost *xenHost = [self xenHost];
    return [NSString stringWithFormat:@"CPU usage for %@", [xenHost name_label]];
}

-(UINavigationController*) navigationController{
    return navigationController;
}

-(NSArray*)dataSourcesToGraphFromRRD:(RoundRobinDatabase*)rrd withReference:(NSString*)sourceReference{
    NSMutableArray* sourceArray = [[NSMutableArray alloc] initWithCapacity:1];
    for (DataSource* dataSource in [rrd dataSources]) {
        if ([[dataSource name] hasPrefix:@"cpu"]){
            [sourceArray addObject:dataSource];
        }
    }
    return [sourceArray autorelease];
}


-(double)yMinForDataSources:(NSArray*)dataSources withConsolidationStep:(int)step{
    return 0;
}

-(double)yMaxForDataSources:(NSArray*)dataSources withConsolidationStep:(int)step{
    return 1.0;
}

-(double) getPlotPointFromDataPoint:(DataPoint*)dataPoint{
    // beacuse using a minmanl RRD all points are the same
    return [dataPoint average];
//    double displayVal = [dataPoint average];
//    if (displayVal == 0){
//        displayVal = [dataPoint max];
//    }
//    return displayVal;    
}

-(NSString*)yTitle{
    return @"Usage %";
}

-(NSNumberFormatter*) yFormatter{
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterPercentStyle];
    // Set to the current locale
    [formatter setLocale:[NSLocale currentLocale]];
    return [formatter autorelease];
}
@end
