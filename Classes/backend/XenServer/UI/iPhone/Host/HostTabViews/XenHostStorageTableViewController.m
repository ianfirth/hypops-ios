//
//  XenVMGeneralViewController.m
//  hypOps
//
//  Created by Ian Firth on 04/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenHostStorageTableViewController.h"
#import "XenStorage.h"
#import "XenPBD.h"
#import "ScrollableDetailTextCell.h"
#import "RRDDisplayButton.h"
#import "DataSource.h"

#define RRDButtonCell @"RRD"
#define GeneralCell @"General"

@interface XenHostStorageTableViewController ()
- (UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier;
- (UITableViewCell*) configureCellForTableView:(UITableView *)tableView forStorage:(XenStorage *)storage atIndex:(int) index;
-(XenStorage *) storageForSection:(NSInteger)section;
- (XenStorage *) xenStorageWithOpaqueRef:(NSString*)opaque_ref;
@end

@implementation XenHostStorageTableViewController

@synthesize hypConnID, xenHostRef;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenHost *) xenHost{
    NSArray *xenHosts = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST withCondition:[XenBase XenBaseFor:[self xenHostRef]]];
    if ([xenHosts count] >0){
        return [xenHosts objectAtIndex:0];
    }
    return nil;
}

- (XenStorage *) xenStorageWithOpaqueRef:(NSString*)opaque_ref{
    NSArray *xenSRs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_STORAGE withCondition:[XenBase XenBaseFor:opaque_ref]];
    if ([xenSRs count] >0){
        return [xenSRs objectAtIndex:0];
    }
    return nil;
}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withHost:(XenHost *)thisXenHost navigationController:(UINavigationController *)navcon{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setXenHostRef:  [thisXenHost opaque_ref]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        navigationController = [navcon retain];
        rrdStorageButtons = [[NSMutableDictionary alloc ] init];
    }
    return self;
    
}

- (void)dealloc {
    [rrdStorageButtons release];
    [navigationController release];
    navigationController = nil;
    [self setXenHostRef:nil];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [self setHypConnID:nil];
    [super dealloc];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    [tableView release];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[[UIView alloc] init] autorelease]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}

- (void) viewWillAppear:(BOOL)animated{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    [xenHypervisorConnection addHypervisorConnectionDelegate:self];
    [[self tableView] reloadData];
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

-(void) viewDidDisappear:(BOOL)animated{
    // make sure that buttons are freed
    // this makes sure that the delegates are released
    // this enables this to be dealloc'd when the parent is
    for (RRDDisplayButton* but in [rrdStorageButtons allValues]) {
        [but setDelegate:nil];
    }
    [rrdStorageButtons removeAllObjects];
    [super viewDidDisappear:animated];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void) viewDidAppear:(BOOL)animated{
    // this gets the text in the labels scrolling again when the view appear, if this is not
    // here, then the text will not start scrolling again until selected. or moved off the screen
    // and back again.
    [[self tableView] reloadData];
    [super viewDidAppear:animated];
}

#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{

    // this code should only be checking for changes to storage  objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    if (objectType == HYPOBJ_STORAGE || objectType == HYPOBJ_PBD){
        [[self tableView ]reloadData];
    }
}

// provide access to the cell definitions
-(UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier] autorelease];
    }
    
    // general cells can have different components in them remove them if there are
    if (identifier == GeneralCell){
        [[[cell contentView] viewWithTag:100] removeFromSuperview];
    }

    return cell;    
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of NICs that the host has.
    return [[[self xenHost] referencesForType:HYPOBJ_PBD] count];
}

// returns nil if the storage is not in the cache, this is expected to rectify itself by a subsequent data load
-(XenStorage *) storageForSection:(NSInteger)section{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    XenStorage *storage = nil;
    NSString *opRef = [[[self xenHost] referencesForType:HYPOBJ_PBD] objectAtIndex:section];
    NSArray *pbds = [xenHypervisorConnection hypObjectsForType:HYPOBJ_PBD withCondition:[XenBase XenBaseFor:opRef]];
    if ([pbds count] >0){
        XenPBD *pbd = [pbds objectAtIndex:0];
        // now go from pbd to the SR
        NSArray *storageList = [pbd referencesForType:HYPOBJ_STORAGE];
        if ([storageList count] >0){
            NSString *storageRef = [storageList objectAtIndex:0];
            NSArray *storageList = [xenHypervisorConnection hypObjectsForType:HYPOBJ_STORAGE withCondition:[XenBase XenBaseFor:storageRef]];
            if ([storageList count]>0){
                storage = [[xenHypervisorConnection hypObjectsForType:HYPOBJ_STORAGE withCondition:[XenBase XenBaseFor:storageRef]] objectAtIndex:0];
            }
        }
    }
    return storage;
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    XenStorage *storage = [self storageForSection:section];
    if (storage){
    return [storage name_label];
    }
    return @"No Storage object was available";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    XenStorage *storage = [self storageForSection:section];
    int count = XENHOST_STORAGE_TABLE_COUNT;
    if ([storage local_cache_enabled]){
        count = count +1;
    }
    return count;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // all sections are expandable
    return YES;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        
    XenStorage *storage = [self storageForSection:[indexPath section]];

    return [self configureCellForTableView:tableView forStorage:storage atIndex:[indexPath row]];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (UITableViewCell*) configureCellForTableView:(UITableView *)tableView forStorage:(XenStorage *)storage atIndex:(int) index{
    
    UITableViewCell* cell = nil;
    
    switch (index) {
        case XENHOST_STORAGE_TABLE_TYPE:
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Type"];
            [[cell detailTextLabel] setText:[storage type]];
            break;
        case XENHOST_STORAGE_TABLE_CONTENTTYPE:
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Content Type"];
            [[cell detailTextLabel] setText:[storage contentType]];
            break;
        case XENHOST_STORAGE_TABLE_SHARED:
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Shared"];
            if ([storage shared]){
                [[cell detailTextLabel] setText:@"Yes"];
            }
            else
            {
                [[cell detailTextLabel] setText:@"No"];
            }
            break;
        case XENHOST_STORAGE_TABLE_USAGE:
        {
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            NSNumber *memTotalBytes = [storage physicalUtilization];
            long long memTotal = [memTotalBytes longLongValue]; 
            float finalmemTotalGb = ((float)memTotal)/1024/1024/1024;
            [[cell textLabel] setText:@"Usage"];
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Gb",finalmemTotalGb]];
            break;
        }
        case XENHOST_STORAGE_TABLE_SIZE:
        {
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            NSNumber *memTotalBytes = [storage physicalSize];
            long long memTotal = [memTotalBytes longLongValue]; 
            float finalmemTotalGb = ((float)memTotal)/1024/1024/1024;
            [[cell textLabel] setText:@"Size"];
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Gb",finalmemTotalGb]];
            break;
        }
        case XENHOST_STORAGE_TABLE_VIRTUALALLOCATION:
        {
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            NSNumber *memTotalBytes = [storage virtualAllocation];
            long long memTotal = [memTotalBytes longLongValue]; 
            float finalmemTotalGb = ((float)memTotal)/1024/1024/1024;
            [[cell textLabel] setText:@"Allocation"];
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Gb",finalmemTotalGb]];
            break;
        }
        case XENHOST_STORAGE_TABLE_CACHEDRIVE:
        {
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Used by intellicache"];
            if ([storage local_cache_enabled]){
                [[cell detailTextLabel] setText:@"Yes"];
            }
            else
            {
                [[cell detailTextLabel] setText:@"No"];
            }
            break;
        }
        case XENHOST_STORAGE_TABLE_PERFORMANCEDATA:{
            cell = [self getCellForTableView:tableView andIdentifier:RRDButtonCell];
            [[cell detailTextLabel] setText:@""];
            [[cell textLabel] setText:@""];
            RRDDisplayButton* rrdButton = [rrdStorageButtons objectForKey:[storage uuid]];
            if (!rrdButton){
                rrdButton = [[RRDDisplayButton alloc] init];
                [rrdStorageButtons setObject:rrdButton forKey:[storage uuid]];
                [rrdButton setDelegate:self];
                [[rrdButton view ]setTag:100];
                [rrdButton setReference:[storage opaque_ref]];
                [rrdButton release];
            }
            [[rrdButton view ]setFrame:[[cell contentView] bounds]];
            [[cell contentView] setAutoresizesSubviews:YES];
            [[cell contentView] addSubview:[rrdButton view]];
            [rrdButton viewWillAppear:YES];
            break;
        }
        default:
            break;
    }  
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];

    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark -
#pragma mark RRDDisplayButtonDelegate
    
- (RoundRobinDatabase *)roundRobinDatabase{
    XenHost *xenHost = [self xenHost];
    return [xenHost hostPerformanceData];
}

- (BOOL) reloadRoundRobinDatabase{
    XenHost *xenHost = [self xenHost];
    return [xenHost refreshPerformanceData];
}

-(NSDate*) getLastRefreshTime{
    XenHost *xenHost = [self xenHost];
    return [xenHost lastPreformanceDataUpdateTime];
}

-(NSString*)  graphTitleWithReference:(NSString*)sourcereference{
        XenStorage *xenStorage = [self xenStorageWithOpaqueRef:sourcereference];
        return [NSString stringWithFormat:@"Intellicache usage for %@", [xenStorage name_label]];
}

-(UINavigationController*) navigationController{
    return navigationController;
}

-(NSArray*)dataSourcesToGraphFromRRD:(RoundRobinDatabase*)rrd withReference:(NSString*)sourcereference{
    NSMutableArray* sourceArray = [[NSMutableArray alloc] initWithCapacity:1];
    // need to convert source referece to the actual reference here
    XenStorage *xenStorage = [self xenStorageWithOpaqueRef:sourcereference];
    NSString *storageRef = [NSString stringWithFormat:@"sr_%@_",[xenStorage uuid]];
    for (DataSource* dataSource in [rrd dataSources]) {
        if ([[dataSource name] hasPrefix:storageRef]){
            if ([[dataSource name] hasSuffix:@"_cache_size"]){
                [dataSource setAlternateName:@"Size"];
                [sourceArray addObject:dataSource];
            }
            if ([[dataSource name] hasSuffix:@"_cache_hits"]){
                [dataSource setAlternateName:@"Hits"];
                [sourceArray addObject:dataSource];
            }
            if ([[dataSource name] hasSuffix:@"_cache_misses"]){
                [dataSource setAlternateName:@"Misses"];
                [sourceArray addObject:dataSource];
            }
        }
    }
    return [sourceArray autorelease];
}

-(double)yMinForDataSources:(NSArray*)dataSources withConsolidationStep:(int)step{
    return 0;
}
    
-(double) getPlotPointFromDataPoint:(DataPoint*)dataPoint{
    // beacuse using a minmanl RRD all points are the same
    return [dataPoint average];
//        double displayVal = [dataPoint average];
//        if (displayVal == 0){
//            displayVal = [dataPoint max];
//        }
//        // NSLog(@"%f",displayVal);
//        return displayVal;    
}
        
-(double)yMaxForDataSources:(NSArray*)dataSources withConsolidationStep:(int)step{
    return 1.0;
}

-(NSString*)yTitle{
    return @"";
}

-(NSNumberFormatter*) yFormatter{
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterPercentStyle];
    // Set to the current locale
    [formatter setLocale:[NSLocale currentLocale]];
    return [formatter autorelease];
}

@end
