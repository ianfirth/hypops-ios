//
//  XenVMGeneralViewController.m
//  hypOps
//
//  Created by Ian Firth on 04/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenHostNicsTableViewController.h"
#import "XenNIC.h"
#import "XenHypervisorConnection.h"
#import "ScrollableDetailTextCell.h"
#import "RRDDisplayButton.h"
#import "DataSource.h"
#import "DataPoint.h"
#import "BitNumberFormatter.h"

#define RRDButtonCell @"RRD"
#define GeneralCell @"General"

@interface XenHostNicsTableViewController ()
-(UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier;
- (UITableViewCell*) configureCellForTableView:(UITableView*)tableView forNIC:(XenNIC *)nic atIndex:(int) index;
- (XenHypervisorConnection *) hypervisorConnection;
- (XenHost *) xenHost;
@end

@implementation XenHostNicsTableViewController

@synthesize hypConnID, xenHostRef;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenHost *) xenHost{
    NSArray *xenHosts = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST withCondition:[XenBase XenBaseFor:[self xenHostRef]]];
    if ([xenHosts count] >0){
        return [xenHosts objectAtIndex:0];
    }
    return nil;
}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withHost:(XenHost *)thisXenHost navigationController:(UINavigationController *)navcon{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setXenHostRef: [thisXenHost opaque_ref]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        navigationController = [navcon retain];
        rrdNicButtons = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)dealloc {
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    [rrdNicButtons release];
    [self setXenHostRef: nil];
    [xenHypervisorConnection removeHypervisorConnectionDelegate:self];
    [self setHypConnID:nil];
    [navigationController release];
    [super dealloc];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    [tableView release];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[[UIView alloc] init] autorelease]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}

- (void) viewWillAppear:(BOOL)animated{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    [xenHypervisorConnection addHypervisorConnectionDelegate:self];
    [xenHypervisorConnection setWalkTreeMode:NO];
    [[self tableView] reloadData];
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    [xenHypervisorConnection removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

-(void) viewDidDisappear:(BOOL)animated{
    // make sure that buttons are freed
    // this makes sure that the delegates are released
    // this enables this to be dealloc'd when the parent is
    for (RRDDisplayButton* but in [rrdNicButtons allValues]) {
        [but setDelegate:nil];
    }
    [rrdNicButtons removeAllObjects];
    [super viewDidDisappear:animated];
}

-(void) viewDidAppear:(BOOL)animated{
    // this gets the text in the labels scrolling again when the view appear, if this is not
    // here, then the text will not start scrolling again until selected. or moved off the screen
    // and back again.
    [[self tableView] reloadData];
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

// provide access to the cell definitions
-(UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier] autorelease];
    }
    
    // general cells can have different components in them remove them if there are
    if (identifier == GeneralCell){
        [[[cell contentView] viewWithTag:100] removeFromSuperview];
    }

    return cell;    
}

#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to nic objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    if (objectType == HYPOBJ_NIC){
        [[self tableView ]reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of NICs that the host has.
    return [[[self xenHost] referencesForType:HYPOBJ_NIC] count];
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    NSString *opRef = [[[self xenHost] referencesForType:HYPOBJ_NIC] objectAtIndex:section];
    NSArray *nics = [xenHypervisorConnection hypObjectsForType:HYPOBJ_NIC withCondition:[XenBase XenBaseFor:opRef]];
    if ([nics count] == 0){
        return nil;
    }
    XenNIC *nic = [[xenHypervisorConnection hypObjectsForType:HYPOBJ_NIC withCondition:[XenBase XenBaseFor:opRef]] objectAtIndex:0];
    return [nic device];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    return XENHOST_NICS_TABLE_COUNT;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // all sections are expandable
    return YES;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
        
    NSString *opRef = [[[self xenHost] referencesForType:HYPOBJ_NIC] objectAtIndex:[indexPath section]];
    XenNIC *nic = [[xenHypervisorConnection hypObjectsForType:HYPOBJ_NIC withCondition:[XenBase XenBaseFor:opRef]] objectAtIndex:0];

    return [self configureCellForTableView:tableView forNIC:nic atIndex:[indexPath row]];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (UITableViewCell*) configureCellForTableView:(UITableView*)tableView forNIC:(XenNIC *)nic atIndex:(int) index{
    
    UITableViewCell *cell = nil;
    
    switch (index) {
        case XENHOST_NICS_TABLE_MAC:
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"MAC"];
            [[cell detailTextLabel] setText:[nic mac]];
            break;
        case XENHOST_NICS_TABLE_IP:
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Address"];
            [[cell detailTextLabel] setText:[nic ip]];
            break;
        case XENHOST_NICS_TABLE_PERFORMANCEDATA:{
            cell = [self getCellForTableView:tableView andIdentifier:RRDButtonCell];
            [[cell detailTextLabel] setText:@""];
            [[cell textLabel] setText:@""];
            RRDDisplayButton* rrdButton = [rrdNicButtons objectForKey:[nic uuid]];
            if (!rrdButton){
                rrdButton = [[RRDDisplayButton alloc] init];
                [rrdNicButtons setObject:rrdButton forKey:[nic uuid]];
                [rrdButton setDelegate:self];
                [[rrdButton view ]setTag:100];
                [rrdButton setReference:[nic device]];
                [rrdButton release];
            }
            [[rrdButton view ]setFrame:[[cell contentView] bounds]];
            [[cell contentView] setAutoresizesSubviews:YES];
            [[cell contentView] addSubview:[rrdButton view]];
            [rrdButton viewWillAppear:YES];
            break;
        }
        default:
            break;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];

    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark -
#pragma mark RRDDisplayButtonDelegate
    
    - (RoundRobinDatabase *)roundRobinDatabase{
        XenHost *xenHost = [self xenHost];
        return [xenHost hostPerformanceData];
    }
    
    - (BOOL) reloadRoundRobinDatabase{
        XenHost *xenHost = [self xenHost];
        return [xenHost refreshPerformanceData];
    }
    
    -(NSDate*) getLastRefreshTime{
        XenHost *xenHost = [self xenHost];
        return [xenHost lastPreformanceDataUpdateTime];
    }
    
    -(NSString*)  graphTitleWithReference:(NSString*)sourcereference{
        XenHost *xenHost = [self xenHost];
        return [NSString stringWithFormat:@"Network usage for %@", [xenHost name_label]];
    }
    
    -(UINavigationController*) navigationController{
        return navigationController;
    }
    
-(NSArray*)dataSourcesToGraphFromRRD:(RoundRobinDatabase*)rrd withReference:(NSString*)sourcereference{
        NSMutableArray* sourceArray = [[NSMutableArray alloc] initWithCapacity:1];
        NSString *nicDevice = [NSString stringWithFormat:@"pif_%@_",sourcereference];
        for (DataSource* dataSource in [rrd dataSources]) {
            if ([[dataSource name] hasPrefix:nicDevice]){
                if ([[dataSource name] hasSuffix:@"tx"]){
                    [dataSource setAlternateName:@"Transmit"];
                }
                else{
                    [dataSource setAlternateName:@"Receive"];
                        
                }
                [sourceArray addObject:dataSource];
            }
        }
        return [sourceArray autorelease];
    }

-(double)yMinForDataSources:(NSArray*)dataSources withConsolidationStep:(int)step{
    return 0;
}

-(double) getPlotPointFromDataPoint:(DataPoint*)dataPoint{
    // beacuse using a minmanl RRD all points are the same
    return [dataPoint average];
//    double displayVal = [dataPoint average];
//    if (displayVal == 0){
//        displayVal = [dataPoint max];
//    }
//    // NSLog(@"%f",displayVal);
//    return displayVal;    
}

-(double)roundup:(double)inputNumber{
    // round up the the value to the nearest value on a 10's boundary
    int resultInt = inputNumber;
    int u = 0;
    while (resultInt > 1){
        resultInt /= 10;
        u ++;
    };
    
    float divisor = pow(10,u-1);
    float resultPower = inputNumber / divisor;
    // round this and multiply by u power 10;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:0];
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    
    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithFloat:resultPower]];
    [formatter release];
    return [numberString intValue] * divisor;
}

-(double)yMaxForDataSources:(NSArray*)dataSources withConsolidationStep:(int)step{
    double result = 0;
    for (DataSource* source in dataSources) {
        NSLog(@"DataSourceName = %@",[source name]);
            NSLog(@"Consolidation Step = %d", step);
            for (DataPoint* point in [source dataPointsForConsolodatedStepCount:step]) {
                double val = [self getPlotPointFromDataPoint:point];
                if (val>result){
                    result = val;
                }
        }
    }
    return [self roundup:result];
}

-(NSString*)yTitle{
    return @"bps";
}

-(NSNumberFormatter*) yFormatter{
    NSNumberFormatter* formatter = [[BitNumberFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    return [formatter autorelease];
}
@end