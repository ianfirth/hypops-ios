//
//  XenHostMemoryDelegateImpl.m
//  hypOps
//
//  Created by Ian Firth on 05/10/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenHostMemoryDelegateImpl.h"
#import "DataSource.h"
#import "MemoryNumberFormatter.h"

@implementation XenHostMemoryDelegateImpl
@synthesize hypConnID, xenHostRef;

- (id)initWithhypervisorConnectionID:(NSString *)hypervisorConnectionID withHost:(XenHost *)thisXenHost navigationController:(UINavigationController*)navcon{
    self = [super init];
    if (self){
        [self setXenHostRef: [thisXenHost opaque_ref]];
        [self setHypConnID: hypervisorConnectionID];
        navigationController = [navcon retain];
    }
    return self;
}

-(void) dealloc{
    [navigationController release];
    [super dealloc];
}

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenHost *) xenHost{
    NSArray *xenHosts = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST withCondition:[XenBase XenBaseFor:[self xenHostRef]]];
    if ([xenHosts count] >0){
        return [xenHosts objectAtIndex:0];
    }
    return nil;
}

#pragma mark -
#pragma mark RRDDisplayButtonDelegate

- (RoundRobinDatabase *)roundRobinDatabase{
    XenHost *xenHost = [self xenHost];
    return [xenHost hostPerformanceData];
}

- (BOOL) reloadRoundRobinDatabase{
    XenHost *xenHost = [self xenHost];
    return [xenHost refreshPerformanceData];
}

-(NSDate*) getLastRefreshTime{
    XenHost *xenHost = [self xenHost];
    return [xenHost lastPreformanceDataUpdateTime];
}

-(NSString*) graphTitleWithReference:(NSString*)sourcereference{
    XenHost *xenHost = [self xenHost];
    return [NSString stringWithFormat:@"Memory usage for %@", [xenHost name_label]];
}

-(UINavigationController*) navigationController{
    return navigationController;
}

-(NSArray*)dataSourcesToGraphFromRRD:(RoundRobinDatabase*)rrd withReference:(NSString*)sourceReference{
    NSMutableArray* sourceArray = [[NSMutableArray alloc] initWithCapacity:1];
    for (DataSource* dataSource in [rrd dataSources]) {
        if ([[dataSource name] hasPrefix:@"memory_total_kib"]){
            [dataSource setAlternateName:@"Total"];
            [sourceArray addObject:dataSource];
        }
        if ([[dataSource name] hasPrefix:@"memory_free_kib"]){
            [dataSource setAlternateName:@"Free"];
            [sourceArray addObject:dataSource];
        }
        
    }
    return [sourceArray autorelease];
}

-(double)yMinForDataSources:(NSArray*)dataSources withConsolidationStep:(int)step{
    return 0;
}


-(double) getPlotPointFromDataPoint:(DataPoint*)dataPoint{
    // beacuse using a minmanl RRD all points are the same
    return [dataPoint average];
//    double displayVal = [dataPoint average];
//    if (displayVal == 0){
//        displayVal = [dataPoint max];
//    }
//    return displayVal;    
}

-(NSString*)yTitle{
    return @"";
}


-(double)roundup:(double)inputNumber{
    // round up the the value to the nearest value on a 10's boundary
    int resultInt = inputNumber;
    int u = 0;
    while (resultInt > 1){
        resultInt /= 10;
        u ++;
    };
    
    float divisor = pow(10,u-1);
    float resultPower = inputNumber / divisor;
    // round this and multiply by u power 10;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:0];
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    
    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithFloat:resultPower]];
    [formatter release];
    return [numberString intValue] * divisor;
}

-(double)yMaxForDataSources:(NSArray*)dataSources withConsolidationStep:(int)step{
    double result = 0;
    for (DataSource* source in dataSources) {
        NSLog(@"DataSourceName = %@",[source name]);
        NSLog(@"Consolidation Step = %d", step);
        for (DataPoint* point in [source dataPointsForConsolodatedStepCount:step]) {
            double val = [self getPlotPointFromDataPoint:point];
            if (val>result){
                result = val;
            }
        }
    }
    return [self roundup:result];
}


-(NSNumberFormatter*) yFormatter{
    NSNumberFormatter* formatter = [[MemoryNumberFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    return [formatter autorelease];
}
@end
