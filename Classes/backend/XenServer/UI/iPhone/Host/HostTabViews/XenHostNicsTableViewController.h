//
//  XenVMGeneralViewController.h
//  hypOps
//
//  Created by Ian Firth on 04/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XenHost.h"
#import "XenHypervisorConnection.h"
#import "ExpandableTableViewController.h"
#import "RRDDisplayButtonDelegate.h"

#define XENHOST_NICS_TABLE_MAC 0
#define XENHOST_NICS_TABLE_IP 1
#define XENHOST_NICS_TABLE_PERFORMANCEDATA 2
#define XENHOST_NICS_TABLE_COUNT 3

@interface XenHostNicsTableViewController :ExpandableTableViewController<HypervisorConnectionDelegate,RRDDisplayButtonDelegate>{
    NSString *xenHostRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;

    UINavigationController* navigationController;
    
    // stores a list of rrdButtons for each nic
    // so that only one button is created for each object.
    NSMutableDictionary *rrdNicButtons;
}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withHost:(XenHost *)thisXenHost navigationController:(UINavigationController*)navcon;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenHostRef;

@end
