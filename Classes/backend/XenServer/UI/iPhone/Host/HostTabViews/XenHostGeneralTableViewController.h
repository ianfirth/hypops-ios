//
//  XenVMGeneralViewController.h
//  hypOps
//
//  Created by Ian Firth on 04/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XenHost.h"
#import "XenHypervisorConnection.h"
#import "ExpandableTableViewController.h"
#import "RRDDisplayButtonDelegate.h"

#define XENHOST_SECTION_GENERAL 0
#define XENHOST_SECTION_MANAGEMENT 1
#define XENHOST_SECTION_LICENSE 2
#define XENHOST_SECTION_MEMORY 3
#define XENHOST_SECTION_CPU 4
#define XENHOST_SECTION_TAGS 5
#define XENHOST_SECTION_COUNT 6

#define XENHOST_DETAILS_TABLE_SOFTWARE_DETAILS 0
#define XENHOST_DETAILS_TABLE_START_TIME 1
#define XENHOST_DETAILS_TABLE_TOOLS_START_TIME 2
#define XENHOST_DETAILS_TABLE_ISCSI 3
#define XENHOST_DETAILS_TABLE_CACHEENABLED 4
#define XENHOST_DETAILS_COUNT 5

#define XENHOST_MANAGEMENT_TABLE_HOSTNAME 0
#define XENHOST_MANAGEMENT_TABLE_ADDRESS 1 
#define XENHOST_MANAGEMENT_TABLE_XAPIPERFORMANCE 2
#define XENHOST_MANAGEMENT_COUNT 3

#define XENHOST_LICENSE_TABLE_EDITION 0 
#define XENHOST_LICENSE_TABLE_EXPIRY 1 
#define XENHOST_LICENSE_TABLE_ADDRESS 2 
#define XENHOST_LICENSE_TABLE_PORT 3 
#define XENHOST_LICENSE_COUNT 4 

#define XENHOST_CPU_TABLE_COUNT 0 
#define XENHOST_CPU_TABLE_VENDOR 1 
#define XENHOST_CPU_TABLE_MODEL 2 
#define XENHOST_CPU_TABLE_PERFORMANCEDATA 3
#define XENHOST_CPU_COUNT 4

#define XENHOST_MEMORY_TABLE_AVAILABLE 0 
#define XENHOST_MEMORY_TABLE_USED 1 
#define XENHOST_MEMORY_TABLE_PERFORMANCE 2
#define XENHOST_MEMORY_COUNT 3

@interface XenHostGeneralTableViewController :ExpandableTableViewController <HypervisorConnectionDelegate, RRDDisplayButtonDelegate> {
    NSString *xenHostRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;

    UINavigationController* navigationController;
    
    NSMutableDictionary* rrdXapiButtons;
    NSMutableDictionary* rrdMemoryButtons;
    NSMutableDictionary* rrdCpuButtons;
    
}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withHost:(XenHost *)thisXenHost navigationController:(UINavigationController*)navcon;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenHostRef;

@end
