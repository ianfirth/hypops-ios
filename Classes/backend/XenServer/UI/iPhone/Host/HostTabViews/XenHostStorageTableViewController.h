//
//  XenVMGeneralViewController.h
//  hypOps
//
//  Created by Ian Firth on 04/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XenHost.h"
#import "XenHypervisorConnection.h"
#import "ExpandableTableViewController.h"
#import "RRDDisplayButtonDelegate.h"

#define XENHOST_STORAGE_TABLE_TYPE 0
#define XENHOST_STORAGE_TABLE_CONTENTTYPE 1
#define XENHOST_STORAGE_TABLE_SHARED 2
#define XENHOST_STORAGE_TABLE_CACHEDRIVE 3
#define XENHOST_STORAGE_TABLE_USAGE 4
#define XENHOST_STORAGE_TABLE_SIZE 5
#define XENHOST_STORAGE_TABLE_VIRTUALALLOCATION 6
#define XENHOST_STORAGE_TABLE_COUNT 7
// this is not included in the count above as it is optional
// and is only added in if required.
#define XENHOST_STORAGE_TABLE_PERFORMANCEDATA 7

@interface XenHostStorageTableViewController :ExpandableTableViewController<HypervisorConnectionDelegate, RRDDisplayButtonDelegate>{
    NSString *xenHostRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;

    UINavigationController* navigationController;
    
    NSMutableDictionary* rrdStorageButtons;
}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withHost:(XenHost *)thisXenHost navigationController:(UINavigationController *)navcon;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenHostRef;

@end
