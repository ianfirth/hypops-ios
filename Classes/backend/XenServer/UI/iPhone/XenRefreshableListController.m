//
//  XenRefreshableListController.m
//  hypOps
//
//  Created by Ian Firth on 05/04/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenRefreshableListController.h"
#import "XenGeneralViewController.h"
#import "UIWaitForDataLoad.h"
#import "XenDescriptiveBase.h"
#import "RootViewController_ipad.h"
#import "CommonUIStaticHelper.h"

@interface XenRefreshableListController ()
- (NSArray *)filteredObjectList;
- (void) reloadTableData;
@end

@implementation XenRefreshableListController

@synthesize hypConnID, xenObjectList;

#pragma mark -
#pragma mark View lifecycle

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withCondition:(NSPredicate *) condition forHypObjectType:(int)hypObjectType withSearch:(BOOL)searchable{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        objectType = hypObjectType;
        [self setHypConnID:[hypervisorConnection connectionID]];
        if (searchable){
            UISearchBar *theSearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0,320,40)];
            theSearchBar.delegate = self;
            [self.view addSubview:theSearchBar];
            [[self tableView] setTableHeaderView:theSearchBar];
            [theSearchBar release];
        }
       
        NSPredicate *oldPredicate = predicate;
        predicate = condition;
        [condition retain];
        [oldPredicate release];
        [self setXenObjectList:[hypervisorConnection hypObjectsForType:hypObjectType withCondition:predicate]];
        searchPredicate = nil;
        [[self tableView] setDelegate:self];
        [[self tableView] setDataSource:self];
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated{
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    [xenHypervisorConnection addHypervisorConnectionDelegate:self];
    // refresh the data in the lists
    [self setXenObjectList:[xenHypervisorConnection hypObjectsForType:objectType withCondition:predicate]];
    [[self tableView] reloadData];
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated{
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    [xenHypervisorConnection removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
   XenBase *xenObject = [[self filteredObjectList] objectAtIndex:indexPath.row];
   // call the abstract method to render the cell
   [self configureCell:cell forXenObject:xenObject];
}

#pragma mark -
#pragma mark View Search Bar

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    // only show the status bar’s cancel button while in edit mode
    searchBar.showsCancelButton = YES;
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    // flush the previous search content
    if (searchPredicate != nil){
        [searchPredicate release];
        searchPredicate = nil;
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchPredicate != nil){
        [searchPredicate release];
        searchPredicate = nil;
    }
    
    if([searchText isEqualToString:@""] || searchText == nil){
        [self reloadTableData];
        return;
    }
    
    searchPredicate = [XenDescriptiveBase nameBeginsOrContainsAWordBeginningWith:searchText];
    [searchPredicate retain];
    [self reloadTableData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    if (searchPredicate != nil){
        [searchPredicate release];
        searchPredicate = nil;
    }
    [self reloadTableData];
    
    [searchBar resignFirstResponder];
    searchBar.text = @"";
}

// called when Search (in our case “Done”) button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

-(void) reloadTableData{
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    if (![xenHypervisorConnection isUpdatePendingForType:objectType]){
        [xenHypervisorConnection RequestHypObjectsForType:objectType];
    }
    // the call to stopLoading is on the data update received.
}

#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self filteredObjectList] count];
}


- (NSArray *)filteredObjectList{
    // apply the earch predicate if one is set
    if (searchPredicate == nil)
    {
        return [self xenObjectList];
    }
    else
    {
        return [[self xenObjectList] filteredArrayUsingPredicate:searchPredicate];
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
   [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


// override, and only add the header if not in auto refresh mode
- (void)addPullToRefreshHeader {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    if (![xenHypervisorConnection isAutoUpdateEnabled]){
        [super addPullToRefreshHeader];
    }
}

// override, and only add the header if not in auto refresh mode
- (void)startLoading {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    if (![xenHypervisorConnection isAutoUpdateEnabled]){
        [super startLoading];
    }
}

#pragma mark -
#pragma mark XenHypervisorConnection protocol implementation

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)hypObjectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if (hypObjectType | objectType){
        XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

        [self setXenObjectList:[xenHypervisorConnection hypObjectsForType:objectType withCondition:predicate]];
        [self reloadTableData];
        if (![xenHypervisorConnection isAutoUpdateEnabled]){
            [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
        }
    }
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    XenBase *xenBase = [[self filteredObjectList] objectAtIndex:indexPath.row];
    
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    // ensure that the host object data is fully populated here
    [xenHypervisorConnection setWalkTreeMode:YES];
    [xenHypervisorConnection PopulateHypObject:xenBase];
    
    UIWaitForDataLoad *dataLoader = [[UIWaitForDataLoad alloc] initWithHypervisorConnection:xenHypervisorConnection];
    [dataLoader setUIWaitForDataLoadDelegate:self];
    NSLog(@"Created dataloader to wait for data to be loaded for the next page");
    [dataLoader waitForObjectTreeForObject:xenBase];
}

#pragma mark -
#pragma mark UIWaitForDownload delegate

- (void)dataLoaderFinished:(UIWaitForDataLoad *)dataLoader withResult:(BOOL)sucsess{
    NSLog(@"dataloader reported that it has loaded the required data.");
    
    NSError *err = [dataLoader connectionError];
    XenBase *selectedObject = [dataLoader requestedObject];
    
    [dataLoader release];
    
    if (err){
        // if there was an error connecting do not do anyting.
        return;
    }
    
    [self didSelectObject:selectedObject];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}


- (void)dealloc {
    [self setXenObjectList:nil];
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    [xenHypervisorConnection removeHypervisorConnectionDelegate:self];
    [self setHypConnID:nil];
    [predicate release];
    predicate = nil;
    [super dealloc];
}


/**
 * Displays the next view
 * For iPad this is in the detail view area, on the iPhone it is in the rootView list
 * This enables the same view to be used on both devices
 */
-(void) displayNextView:(UIViewController *)controller{
    
    [CommonUIStaticHelper displayNextView:controller usingNavigationController:[self navigationController]];
}
@end


