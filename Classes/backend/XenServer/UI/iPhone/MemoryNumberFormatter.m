//
//  MemoryNumberFormatter.m
//  hypOps
//
//  Created by Ian Firth on 05/10/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "MemoryNumberFormatter.h"

@implementation MemoryNumberFormatter
// this will affect the divisor (memory is in kb for Host and not for VM
-(id) initWithBasereference:(int)ref{
    self = [super init];
    if (self){
        baseRef = ref;
    }
    return self;
    
}

// default to kb i.e. a value of 1 is 1k
-(id)init{
    return [self initWithBasereference:1024];
}

-(NSString*)stringForObjectValue:(id)obj{
    NSNumber* kbnum = (NSNumber*)obj;
    // multiply the number by the base power
    double kb =  (baseRef * [kbnum doubleValue])/1024;
    NSString* postFix = @"K";
    int divisor = 1024;
    
    if (kb >= 1024){
        postFix = @"M";
        divisor = 1024;
    }
    if (kb >= 1024*1024){
        postFix = @"G";
        divisor = 1024*1024;
    }
    
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    [formatter setRoundingMode:NSNumberFormatterRoundUp];
    [formatter setMaximumSignificantDigits:1];
    [formatter setMinimumIntegerDigits:1];
    [formatter setMaximumFractionDigits:1];
    [formatter setMinimumFractionDigits:1];
    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithDouble:kb/divisor]];
    [formatter release];
    return [NSString stringWithFormat:@"%@%@",numberString,postFix];
}
@end
