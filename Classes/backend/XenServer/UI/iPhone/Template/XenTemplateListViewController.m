//
//  XenHostListViewController.m
//  hypOps
//
//  Created by Ian Firth on 10/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenTemplateListViewController.h"
#import "ImageBuilder.h"

@implementation XenTemplateListViewController

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withCondition:(NSPredicate *) condition{
    self = [super initWithHypervisorConnection:hypervisorConnection withCondition:condition forHypObjectType:HYPOBJ_TEMPLATE withSearch:YES];
        self.title = @"Templates";
    return self;
}

// render the cell for the table view
- (void) configureCell:(UITableViewCell *)cell forXenObject:(XenBase *)xenObject{
    // set the lable text for the cell
    [[cell textLabel] setText:[(XenDescriptiveBase *)xenObject name_label]];
    // set the detail text for the cell
    [[cell detailTextLabel] setText:[(XenDescriptiveBase *)xenObject name_description]];
    // set the image for the cell
    [[cell imageView] setImage:[ImageBuilder vMTemplateImage]];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}

// called when an object is selected in the list
- (void) didSelectObject:(XenBase *)selectedObject{
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

@end
