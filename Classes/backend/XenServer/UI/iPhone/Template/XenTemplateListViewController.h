//
//  XenHostListViewController.h
//  hypOps
//
//  Created by Ian Firth on 10/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XenRefreshableListController.h"

@interface XenTemplateListViewController : XenRefreshableListController

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withCondition:(NSPredicate *) condition;

@end
