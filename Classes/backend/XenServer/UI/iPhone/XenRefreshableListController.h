//
//  MyClass.h
//  hypOps
//
//  Created by Ian Firth on 05/04/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XenHypervisorConnection.h"
#import "UIWaitForDataLoad.h"
#import "PullRefreshTableViewController.h"

@interface XenRefreshableListController : PullRefreshTableViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIWaitForDataLoadDelegate, HypervisorConnectionDelegate> {
    
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;
    
    // the type of hyp object that the list deals with
    int objectType;
    
    // the list of objects that are to be displayed in the table view
    NSArray *xenObjectList;
    
    // the predicate used to filter the list of objects
    NSPredicate *predicate;
    
    // the search predicate used to further filter the objects
    NSPredicate *searchPredicate;
}

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withCondition:(NSPredicate *) condition forHypObjectType:(int)hypObjectType withSearch:(BOOL)searchable;

@property (copy) NSString *hypConnID;
@property (retain, nonatomic) NSArray *xenObjectList;

- (void) displayNextView:(UIViewController *)controller;
@end

#pragma mark -

@interface XenRefreshableListController (AbstractMethods)
// Define Abstract methods which should be implemented by subclasses:

// render the cell for the table view
- (void) configureCell:(UITableViewCell *)cell forXenObject:(XenBase *)xenObject;

// called when an object is selected in the list
- (void) didSelectObject:(XenBase *)selectedObject;


@end