//
//  MemoryNumberFormatter.h
//  hypOps
//
//  Created by Ian Firth on 05/10/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MemoryNumberFormatter : NSNumberFormatter{
    int baseRef;
}

-(id) initWithBasereference:(int)ref;
                                    
@end
