//
//  RRDPlotViewController.h
//  hypOps
//
//  Created by Ian Firth on 20/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "RRDPlotViewController.h"
#import "DataPoint.h"

@interface RRDPlotViewController (private)
  - (int)resolveConsolidationCount:(int)requestedCount forDataSource:(DataSource*)dataSource;
  - (void) rescaleGraph;
  - (void)buildXaxis;
  - (void)buildYaxis;
  - (void)buildXaxisFromStart:(double)start toEnd:(double)end withNumSpaces:(int)numberSpaces;
  - (void)arrangePlotSpace;
  - (void)setPaddingDefaultsForGraphWithBounds:(CGRect)bounds;
  - (void)setTitleDefaultsForGraphWithBounds:(CGRect)bounds;
  - (void)killGraph;
  - (void)ShowLoading:(BOOL)show;
  - (void) pickRange:(id)sender;
  - (void)applyTheme:(CPTTheme *)theme withDefault:(CPTTheme *)defaultTheme;
  - (void)addDataSource:(DataSource*) dataSource;
  - (void)setConsolidationlevel:(int)stepCount;
  - (void)renderInLayer:(CPTGraphHostingView *)layerHostingView withTheme:(CPTTheme *)theme;
  - (void)reloadData;
@end

@implementation RRDPlotViewController
@synthesize defaultLayerHostingView;
@synthesize rrdDisplayButtonDelegate;
@synthesize progressAlert;

- (id)initWithRRDDsiplayButtonDelegate:(id <RRDDisplayButtonDelegate>)delegate withReference:(NSString*)theSourcereference{
	if ((self = [super init])) {
        rrdDisplayButtonDelegate = [delegate retain];
        sourceReference = [theSourcereference copy];
        // dont think this is needed outside the constructor so can remove this class variable
        [self setTitle:[delegate graphTitleWithReference:theSourcereference]];

        // list of plot colors to use
        plotcolorList = [[NSArray alloc] initWithObjects:
                         [CPTColor greenColor],
                         [CPTColor redColor],
                         [CPTColor blueColor],
                         [CPTColor grayColor],
                         [CPTColor magentaColor],
                         [CPTColor cyanColor],
                         [CPTColor whiteColor],
                         [CPTColor orangeColor],
                         [CPTColor lightGrayColor],
                         [CPTColor purpleColor],
                         [CPTColor brownColor],
                         [CPTColor darkGrayColor],
                         nil];
        
        // list of marker colors to use
        MarkerColorList = [[NSArray alloc] initWithObjects:
                           [CPTColor greenColor],
                           [CPTColor redColor],
                           [CPTColor blueColor],
                           [CPTColor grayColor],
                           [CPTColor magentaColor],
                           [CPTColor cyanColor],
                           [CPTColor whiteColor],
                           [CPTColor orangeColor],
                           [CPTColor lightGrayColor],
                           [CPTColor purpleColor],
                           [CPTColor brownColor],
                           [CPTColor darkGrayColor],
                           nil];
        
        // formatter for the date on the xAxis
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm:ss\ndd MMM yyyy"];
        
        dateFormatterOverlay = [[NSDateFormatter alloc] init];
        [dateFormatterOverlay setDateFormat:@"dd MMM yyyy HH:mm"];
        
        // add the plots to the graph.
        for (DataSource* source in [rrdDisplayButtonDelegate dataSourcesToGraphFromRRD:[rrdDisplayButtonDelegate roundRobinDatabase] withReference:sourceReference]){
            [self addDataSource:source];
        }
    }
    return self;
}

- (void)dealloc
{
    [self setProgressAlert:nil];
    [rrdDisplayButtonDelegate release];
    rrdDisplayButtonDelegate = nil;
    [sourceReference release];
    [dateFormatter release];
    [dateFormatterOverlay release];
    [symbolTextAnnotation release];
    [plotDataSourceNames release];
    if (firstkey){
        [firstkey release];
        firstkey = nil;
    }
    if (lastkey){
        [lastkey release];
        lastkey = nil;
    }
    [self killGraph];

    [super dealloc];
}

#pragma mark -
#pragma mark viewController methods
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(DataSource*) dataSourceForPlotWithName:(NSString*)name{
    RoundRobinDatabase* rrd = [[self rrdDisplayButtonDelegate] roundRobinDatabase];
    DataSource* selectedDataSource = nil;
    
    for (DataSource* dataSource in [rrd dataSources]) {
        if ([[dataSource name] isEqualToString:name]){
            selectedDataSource = dataSource;
            break;
        }
    }
    return selectedDataSource;
}

-(DataSource*) dataSourceForPlotAtIndex:(int)index{
    RoundRobinDatabase* rrd = [[self rrdDisplayButtonDelegate] roundRobinDatabase];
    DataSource* selectedDataSource = nil;
    
    for (DataSource* dataSource in [rrd dataSources]) {
        if ([[dataSource name] isEqualToString:[plotDataSourceNames objectAtIndex:index]]){
            selectedDataSource = dataSource;
            break;
        }
    }
    return selectedDataSource;
}

-(DataSource*) findFirstDataSourceForPlot{
    DataSource* selectedDataSource = [self dataSourceForPlotAtIndex:0];

    if (!selectedDataSource){
        selectedDataSource = [plotDataSourceNames objectAtIndex:0];
    }
    return selectedDataSource;
}

-(void) viewDidLoad{
    [super viewDidLoad];

    [[self view] setBackgroundColor:[UIColor blackColor]];
    
    // sort view out here
    // make sure that the graph view is added here and the plot range selection stuff
    defaultLayerHostingView = [[CPTGraphHostingView alloc] init];

    rrdTimeScale = [[UISegmentedControl alloc] init];
    [rrdTimeScale removeAllSegments];
    int stepSeconds = [[[self rrdDisplayButtonDelegate] roundRobinDatabase] step];

    DataSource* selectedDataSource = [self findFirstDataSourceForPlot];
    
    for (NSNumber *num in [selectedDataSource availableSteps]){
        NSArray* dataPoints = [selectedDataSource dataPointsForConsolodatedStepCount:[num intValue]];
        int numSteps = [dataPoints count];
        int stepTime = numSteps * [num intValue];
        int totalSeconds = stepTime * stepSeconds;
        
        // set to years
        NSString* foramtString = @"%d year";
        int divisor = 60*60*24*365;
        if (totalSeconds > ((60 * 60 * 24 * 30)-1) && totalSeconds < ((60 * 60 * 24 * 30 * 12)-1)){
            // set Format to months
            divisor = 365*24*60*60/12;
            foramtString = @"%d month";
        }else if (totalSeconds > ((60 * 60) * 24)-1 && totalSeconds < ((60 * 60 * 24 * 30 * 12)-1)){
            // set Format to days
            divisor = 60*60*24;
            foramtString = @"%d day";
        }else if (totalSeconds > ((60 * 60)-1) && totalSeconds < ((60 * 60 * 24 * 30 * 12)-1)){
            // set Format to hours
            foramtString = @"%d hour";
            divisor = 60*60;
        }else if (totalSeconds > 59 && totalSeconds < ((60 * 60 * 24 * 30 * 12)-1)){
            // set Format to minutes
            foramtString = @"%d minute";
            divisor = 60;
        }else if (totalSeconds < ((60 * 60 * 24 * 30 * 12)-1)){
            //set formate to seconds
            foramtString = @"%d second";
            divisor = 1;
        }
        
        if (totalSeconds/divisor >=2){
            foramtString = [NSString stringWithFormat:@"%@s",foramtString];
        }
        
        NSString* finalString = [NSString stringWithFormat:foramtString,totalSeconds/divisor];
        [rrdTimeScale insertSegmentWithTitle:finalString atIndex:[rrdTimeScale numberOfSegments] animated:NO];
    }
    [rrdTimeScale setSelectedSegmentIndex:0];
    [rrdTimeScale setSegmentedControlStyle:UISegmentedControlStyleBar];

    [rrdTimeScale addTarget:self action:@selector(pickRange:) forControlEvents:UIControlEventValueChanged];
    // position these and get then to scale appropriatl// ancor them etc..
    [[self view] addSubview:rrdTimeScale];
    [[self view] addSubview:defaultLayerHostingView];
    [[self view] setFrame:CGRectMake(0,0,0,100)];
    [[self view] setAutoresizesSubviews:YES];
    int h = 30;
    [defaultLayerHostingView setFrame:CGRectMake(0,0,0,100-h)];
    [rrdTimeScale setFrame:CGRectMake(0,100-h,0,h)];
    [rrdTimeScale setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
    
    [defaultLayerHostingView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight ];
    [rrdTimeScale release];
}

- (void) pickRange:(id)sender{
    ProgressAlert* p = [[ProgressAlert alloc] initWithTitle:@"Processing" delegate:self];
    [self setProgressAlert:p];
    [p release];
    [[self progressAlert ]setTag:100];
    [[self progressAlert] show];
}

#pragma mark -
#pragma mark ProgressAlert delegate

// occurs when the processing alert is displayed
- (void)didPresentAlertView:(UIAlertView *)alertView{
    DataSource* selectedDataSource = [self findFirstDataSourceForPlot];
    int selectedSegment = [rrdTimeScale selectedSegmentIndex];
    NSNumber* consolidationCount = [[selectedDataSource availableSteps] objectAtIndex:selectedSegment];
    // use these values here to set the y plot range
    [self setConsolidationlevel:[consolidationCount intValue]];
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
    [self setProgressAlert:nil];
}


// maybe able to put a spinner in here?
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // probably only want to do this once?
    CPTTheme *theme = [CPTTheme themeNamed:kCPTDarkGradientTheme];
    [self renderInLayer:[self defaultLayerHostingView] withTheme:theme];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

#pragma mark -
#pragma mark Graph helper methods
- (void)killGraph
{
    if (graph) {
        if (symbolTextAnnotation) {
            [graph.plotAreaFrame.plotArea removeAnnotation:symbolTextAnnotation];
            [symbolTextAnnotation release];
            symbolTextAnnotation = nil;
        }
    }

    // Remove the CPTLayerHostingView
    if (defaultLayerHostingView) {
        [defaultLayerHostingView removeFromSuperview];
        
        defaultLayerHostingView.hostedGraph = nil;
        [defaultLayerHostingView release];
        defaultLayerHostingView = nil;
    }
}

- (void)setTitleDefaultsForGraphWithBounds:(CGRect)bounds
{
    UIDevice *device = [UIDevice currentDevice];
    if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        graph.title = [self title];
        CPTMutableTextStyle *textStyle = [CPTMutableTextStyle textStyle];
        textStyle.color = [CPTColor grayColor];
        textStyle.fontName = @"Helvetica-Bold";
        textStyle.fontSize = 16;
        graph.titleTextStyle = textStyle;
        graph.titleDisplacement = CGPointMake(0.0f, round(bounds.size.height / 40.0f)); // Ensure that title displacement falls on an integral pixel
        graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop; 
    }
}

- (void)setPaddingDefaultsForGraphWithBounds:(CGRect)bounds
{
    UIDevice *device = [UIDevice currentDevice];
    if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        float boundsPadding = round(bounds.size.width / 50.0f); // Ensure that padding falls on an integral pixel
        graph.paddingLeft = boundsPadding;
        
        if (graph.titleDisplacement.y > 0.0) {
            graph.paddingTop = graph.titleDisplacement.y * 2;
        }
        else {
            graph.paddingTop = boundsPadding;
        }
        
        graph.paddingRight = boundsPadding;
        graph.paddingBottom = boundsPadding; 
    }else
    {
        float boundsPadding = round(bounds.size.width / 100.0f); // Ensure that padding falls on an integral pixel
        graph.paddingLeft = boundsPadding;
        graph.paddingTop = boundsPadding;
        graph.paddingRight = boundsPadding;
        graph.paddingBottom = boundsPadding; 
    }
}


- (void)applyTheme:(CPTTheme *)theme withDefault:(CPTTheme *)defaultTheme
{
    if (theme == nil) {
        [graph applyTheme:defaultTheme];
    }
    else if (![theme isKindOfClass:[NSNull class]])	{
        [graph applyTheme:theme];
    }
}

- (void)reloadData
{
    [graph reloadData];
}

-(int)resolveConsolidationCount:(int)requestedCount forDataSource:(DataSource*)dataSource{
    int result = -1;
    NSArray* availableCounts = [dataSource availableSteps];
    if (![availableCounts containsObject:[NSNumber numberWithInt:requestedCount]]){
        for (NSNumber* num in availableCounts) {
            int intNum = [num intValue];
            if (intNum > requestedCount){
                result = intNum;
                break;
            }
        }
        // if no match make it the largest one
        if (result == -1){
            result = [[availableCounts lastObject] intValue];
        }
    }
    else{
        result = requestedCount;
    }
    return result;
}

-(void)addDataSource:(DataSource*) dataSource{
    // if adding the first datasource check that the specified consolidation count
    // exists if not set it to the closest one
    if (!plotDataSourceNames){
        plotDataSourceNames = [[NSMutableArray alloc] init];
    }
    
    if ([plotDataSourceNames count] ==0){
        consolidationStepCount = [self resolveConsolidationCount:consolidationStepCount forDataSource:dataSource];
        NSDictionary* dataPoints = [dataSource allDataPointsWithStepConsolidation:consolidationStepCount];
        NSArray* sortedDatesArray = [dataPoints allKeys];
        firstkey = [[sortedDatesArray objectAtIndex:0] retain];
        lastkey = [[sortedDatesArray lastObject] retain];
        plotlength =  [firstkey timeIntervalSinceDate:lastkey]; // time since log point
    }
    [plotDataSourceNames addObject:[dataSource name]];     
}

-(void)setConsolidationlevel:(int)stepCount{
    // check the available levels and set the next highest one if there
    // is not an exact match
    // also assume that all datasets have the same available consolidation levels
    if ([plotDataSourceNames count] >0){
        DataSource* testDataSource = [self findFirstDataSourceForPlot];
        consolidationStepCount = [self resolveConsolidationCount:stepCount forDataSource:testDataSource];
        NSDictionary* dataPoints = [testDataSource allDataPointsWithStepConsolidation:consolidationStepCount];
        NSArray* sortedDatesArray = [dataPoints allKeys];
        // recalculate the plotrange for the x axis
        [firstkey release];
        firstkey = nil;
        [lastkey release];
        lastkey = nil;
        firstkey = [[sortedDatesArray objectAtIndex:0] retain];
        lastkey = [[sortedDatesArray lastObject] retain];
        plotlength =  [firstkey timeIntervalSinceDate:lastkey]; // time since log point

        if (symbolTextAnnotation) {
            [graph.plotAreaFrame.plotArea removeAnnotation:symbolTextAnnotation];
            [symbolTextAnnotation release];
            symbolTextAnnotation = nil;
        }

        [self rescaleGraph];
        // reload data
        [self reloadData];
    }
    else{
        // just set it as it will be fixed up when the datasources are added
        consolidationStepCount = stepCount;  
    }
}

-(void) rescaleGraph{
    [self buildXaxis];
    [self buildYaxis];
    [self arrangePlotSpace];
}

-(void)buildYaxis{
    RoundRobinDatabase* rrd = [[self rrdDisplayButtonDelegate] roundRobinDatabase];
    NSArray* sources = [[self rrdDisplayButtonDelegate] dataSourcesToGraphFromRRD:rrd withReference:sourceReference];
    double yMax = [[self rrdDisplayButtonDelegate] yMaxForDataSources:sources withConsolidationStep:consolidationStepCount];
    double yMin = [[self rrdDisplayButtonDelegate] yMinForDataSources:sources withConsolidationStep:consolidationStepCount];
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    CPTXYAxis *y = axisSet.yAxis;

    // if all points are zero I still need to plot an axis
    if(yMax == 0){
        yMax = 1;
        y.minorTicksPerInterval = 2;
        y.preferredNumberOfMajorTicks = 2;
    }
    else{
        y.minorTicksPerInterval = 2;
        y.preferredNumberOfMajorTicks = 9;
    }
    y.visibleRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(yMin) 
                                                 length:CPTDecimalFromDouble(yMax)];
   
    CPTConstraints *yConstraint = [CPTConstraints constraintWithRelativeOffset:1];
    y.axisConstraints = yConstraint;

    [y setNeedsRelabel];
}

-(void)buildXaxis{
    UIDevice *device = [UIDevice currentDevice];
    if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        [self buildXaxisFromStart:0 toEnd:-plotlength withNumSpaces:4];
    }
    else{
        [self buildXaxisFromStart:0 toEnd:-plotlength withNumSpaces:2];
    }
}
// set up the custom labels and length of the xAxis on the graph
-(void)buildXaxisFromStart:(double)start toEnd:(double)end withNumSpaces:(int)numberSpaces{
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    CPTXYAxis *x = axisSet.xAxis;

    //make sure that start is largest (i.e. 0 and end is -x) otherwise swap them
    // need to make sure that this is always the same polarity
    if (start < end){
        double start2 = end;
        end= start;
        start = start2;
    }

    // 0 is the right of the graph, never use any of the axis this side of 0 (this is space
    // for the axis labels to be drawn
    if (start >0){
        start = 0;
    }

    double visibleLength = end-start;
    if (visibleLength <0){
        visibleLength = visibleLength *-1;
    }
    // always have this many points on the xAxis equally spaced
    double interval = visibleLength/numberSpaces;
    
    // add the custom tick locations and values
    NSMutableArray *customLabels = [NSMutableArray arrayWithCapacity:2];
    NSMutableArray *customTickLocations = [NSMutableArray arrayWithCapacity:2];
    for (double tickLocation = 0 ; tickLocation <= visibleLength ; tickLocation+=interval){
        double actualTickLocation = start - tickLocation;  // 0 is the latest
        // format the label for the point on the xAxis using the date formatter
        NSString *labelText = [dateFormatter stringFromDate:[NSDate dateWithTimeInterval:actualTickLocation sinceDate:firstkey]];
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:labelText textStyle:x.labelTextStyle];
        newLabel.tickLocation = [[NSNumber numberWithDouble:actualTickLocation] decimalValue];
        newLabel.offset = x.labelOffset + x.majorTickLength;
        [customLabels addObject:newLabel];
        // specify where on the axis the tick location is
        [customTickLocations addObject:[NSNumber numberWithDouble:actualTickLocation]];
        [newLabel release];
    }
    
    x.axisLabels =  [NSSet setWithArray:customLabels];
    x.majorTickLocations = 	[NSSet setWithArray:customTickLocations];

    x.visibleRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(start) 
                                                  length:CPTDecimalFromDouble(end-start)];
}

- (CPTPlotRange*) getYPlotRange{
    RoundRobinDatabase* rrd = [[self rrdDisplayButtonDelegate] roundRobinDatabase];
    NSArray* sources = [[self rrdDisplayButtonDelegate] dataSourcesToGraphFromRRD:rrd withReference:sourceReference];
    double yMax = [[self rrdDisplayButtonDelegate] yMaxForDataSources:sources withConsolidationStep:consolidationStepCount];
    double yMin = [[self rrdDisplayButtonDelegate] yMinForDataSources:sources withConsolidationStep:consolidationStepCount];
    // make sure that the axis has some length
    if(yMax == 0){
        yMax = 1;
    }

    float ylength = yMax-yMin;

    return [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(yMin) 
                                        length:CPTDecimalFromFloat(ylength)];
}

-(void)arrangePlotSpace{
    
    CPTXYPlotSpace* plotSpace = (CPTXYPlotSpace*)[graph defaultPlotSpace];
    
    // add a little space to the left hand end.
    CPTPlotRange *xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-plotlength) 
                                                        length:CPTDecimalFromFloat(plotlength)];    
    // Restrict x range to a global range
    plotSpace.globalXRange = xRange;     
    plotSpace.xRange = xRange;
    
    CPTPlotRange *yRange = [self getYPlotRange]; 

    // Restrict y range to a global range
    plotSpace.globalYRange = yRange; 
    plotSpace.yRange = yRange;
}

- (void)renderInLayer:(CPTGraphHostingView *)layerHostingView withTheme:(CPTTheme *)theme
{

    CGRect bounds = layerHostingView.bounds;
    
    // if the graph is already created kill it first..
    // ight not need this if there is no way to call this externally
    // by the time the refctor is finished.
    graph = [[[CPTXYGraph alloc] initWithFrame:bounds] autorelease];
    if (layerHostingView) {
		layerHostingView.hostedGraph = graph;
        [self setDefaultLayerHostingView:layerHostingView];
    }
    
//    spinner = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] retain];
//    [spinner setFrame:CGRectMake(0, 0, 100, 100)];
//    CGRect spinnerBounds = [spinner bounds];
//    int spx = (bounds.size.width - spinnerBounds.size.width)/2;
//    int spy = (bounds.size.height - spinnerBounds.size.height)/2;
//    [spinner setFrame:CGRectMake(spx, spy, spinnerBounds.size.width, spinnerBounds.size.height)];
//    [layerHostingView addSubview:spinner];
    
    [self applyTheme:theme withDefault:[CPTTheme themeNamed:kCPTDarkGradientTheme]];

    [self setTitleDefaultsForGraphWithBounds:bounds];
    [self setPaddingDefaultsForGraphWithBounds:bounds];

    // Setup scatter plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.allowsUserInteraction = YES;
    plotSpace.delegate = self;

    // Grid line styles
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorGridLineStyle.lineWidth = 0.75;
    majorGridLineStyle.lineColor = [[CPTColor colorWithGenericGray:0.2] colorWithAlphaComponent:0.75];

    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
    minorGridLineStyle.lineWidth = 0.25;
    minorGridLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:0.1];    

    CPTMutableLineStyle *redLineStyle = [CPTMutableLineStyle lineStyle];
    redLineStyle.lineWidth = 10.0;
    redLineStyle.lineColor = [[CPTColor redColor] colorWithAlphaComponent:0.5];

    // Axes
    // Label x axis with a fixed interval policy
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    CPTXYAxis *x = axisSet.xAxis;
    x.majorIntervalLength = CPTDecimalFromString(@"20");
    x.orthogonalCoordinateDecimal = CPTDecimalFromString(@"0.0");
    x.minorTicksPerInterval = 10;
    x.majorGridLineStyle = majorGridLineStyle;
    x.minorGridLineStyle = minorGridLineStyle;

    x.title = @"";
    x.titleOffset = 30.0;
    x.titleLocation = CPTDecimalFromString(@"1.25");
    
    //define the label text style
    CPTMutableTextStyle *labelTextStyle = [[CPTMutableTextStyle alloc] init];
    [labelTextStyle setTextAlignment:CPTTextAlignmentCenter];
    [labelTextStyle setFontName:@"Helvetica"];
    [labelTextStyle setFontSize:10.0f];
    [labelTextStyle setColor:[CPTColor whiteColor]];
    x.labelTextStyle = labelTextStyle;
    [labelTextStyle release];
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    [self buildXaxis];  
    
    // Label y with an automatic label policy. 
    CPTXYAxis *y = axisSet.yAxis;
    y.labelingPolicy = CPTAxisLabelingPolicyAutomatic;
    y.minorTicksPerInterval = 2;
    y.preferredNumberOfMajorTicks = 9;
    y.majorGridLineStyle = majorGridLineStyle;
    y.minorGridLineStyle = minorGridLineStyle;
    y.labelFormatter = [[self rrdDisplayButtonDelegate] yFormatter];
    y.labelAlignment = CPTAlignmentLeft;

    // move label onto right of axis (this might not be the best way to do this)
    y.labelOffset = -45.0;
    y.titleOffset = -60;

    y.title  = [[self rrdDisplayButtonDelegate] yTitle];
    
    [self buildYaxis];  
    
    // Set axes
    //graph.axisSet.axes = [NSArray arrayWithObjects:x, y, y2, nil];
    graph.axisSet.axes = [NSArray arrayWithObjects:x, y, nil];

    // put the axis on the right hand side (1.0) and have 70 pixels padding to the right of it.
    graph.plotAreaFrame.paddingLeft = 50.0;
    graph.plotAreaFrame.paddingRight = 70.0;
    
    graph.plotAreaFrame.paddingTop = 30.0;
    graph.plotAreaFrame.paddingBottom = 50.0;
    
    // create the dataPlots for each dataSource that is added to the graph
    // ...... factor out 
    int colorCounter = 0;
    dataSourceLinePlots = [[NSMutableArray alloc] init];
    
    for (int counter = [plotDataSourceNames count]-1 ; counter >= 0 ; counter--){
        DataSource* dataSource = [self dataSourceForPlotAtIndex:counter];
        CPTColor* lineColor = [plotcolorList objectAtIndex:colorCounter++];
        if (colorCounter >= [plotcolorList count]){
            colorCounter = 0;
        }
        
        CPTScatterPlot *dataSourceLinePlot = [[[CPTScatterPlot alloc] init] autorelease];
        dataSourceLinePlot.identifier = [dataSource alternateName];
        
        CPTMutableLineStyle *lineStyle = [[dataSourceLinePlot.dataLineStyle mutableCopy] autorelease];
        lineStyle.lineWidth = 2.0;
        lineStyle.lineColor = lineColor;
        dataSourceLinePlot.dataLineStyle = lineStyle;
        
        [dataSourceLinePlot setCachePrecision:CPTPlotCachePrecisionDouble];
        dataSourceLinePlot.dataSource = self;
        [graph addPlot:dataSourceLinePlot];
        [dataSourceLinePlots addObject:dataSourceLinePlot];
        [dataSourceLinePlot setName:[dataSource name]];
    }

    [self arrangePlotSpace];

// this is the symbol code, it slows down the drawing significantly so
// it is commented out presently.
//    colorCounter = 0;
//    // setup the same plot symbol and hit area for all lines as present
//    for (CPTScatterPlot* linePlot in dataSourceLinePlots) {
//        CPTColor* markerColor = [MarkerColorList objectAtIndex:colorCounter++];
//        if (colorCounter >= [MarkerColorList count]){
//            colorCounter = 0;
//        }
//
//        // Add plot symbols
//        CPTMutableLineStyle *symbolLineStyle = [CPTMutableLineStyle lineStyle];
//        symbolLineStyle.lineColor = markerColor;
//        CPTPlotSymbol *plotSymbol = [CPTPlotSymbol ellipsePlotSymbol];       
//        plotSymbol.fill = [CPTFill fillWithColor:markerColor];
//        plotSymbol.lineStyle = symbolLineStyle;
//        plotSymbol.size = CGSizeMake(2.0, 2.0);
//
//        linePlot.plotSymbol = plotSymbol;
//        // Set plot delegate, to know when symbols have been touched
//        // We will display an annotation when a symbol is touched
//        linePlot.delegate = self; 
//        linePlot.plotSymbolMarginForHitDetection = 5.0f;
//    }

	// Add legend if more than one graph to plot
    if ([plotDataSourceNames count] >1){
        graph.legend = [CPTLegend legendWithGraph:graph];
        graph.legend.textStyle = x.titleTextStyle;
        graph.legend.fill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:.3 green:0.3 blue:0.3 alpha:0.5]];
        graph.legend.borderLineStyle = x.axisLineStyle;
        graph.legend.cornerRadius = 5.0;
        graph.legend.numberOfRows = 2;
        graph.legend.swatchSize = CGSizeMake(15.0, 15.0);
        graph.legendAnchor = CPTRectAnchorTopLeft;
        UIDevice *device = [UIDevice currentDevice];
        if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
            graph.legendDisplacement = CGPointMake(30.0, -30.0);  // y - = up.
        }else{
            graph.legendDisplacement = CGPointMake(20.0, -20.0); 
        }
    }
}

#pragma mark -
#pragma mark Plot Data Source Methods

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    // use the plot name to find the dataSource
    DataSource* dataSource = [self dataSourceForPlotWithName:[plot name]];
    return [[dataSource allDataPointsWithStepConsolidation:consolidationStepCount] count];
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    DataSource* dataSource = [self dataSourceForPlotWithName:[plot name]];
    NSNumber* returnVal = nil;
    
    NSDictionary* dataPoints = [dataSource allDataPointsWithStepConsolidation:consolidationStepCount];
    NSArray* sortedDatesArray = [dataPoints allKeys];
    
    NSDate* key = [sortedDatesArray objectAtIndex:index];
    if (fieldEnum == CPTScatterPlotFieldY){
        DataPoint* dp = [dataPoints objectForKey:key];
        // hmm need to use avg or max on the ones that do not have the others set
        double displayVal = [[self rrdDisplayButtonDelegate] getPlotPointFromDataPoint:dp];
        //NSLog(@"Date = %@ plotVal = %f",key,displayVal);
        returnVal = [NSNumber numberWithDouble:displayVal];
    }
    if (fieldEnum == CPTScatterPlotFieldX){
        returnVal =  [NSNumber numberWithDouble:-[key timeIntervalSinceDate:[sortedDatesArray lastObject]]];
    }    
    return returnVal;
}

#pragma mark -
#pragma mark Plot Space Delegate Methods

-(CPTPlotRange *)plotSpace:(CPTPlotSpace *)space willChangePlotRangeTo:(CPTPlotRange *)newRange forCoordinate:(CPTCoordinate)coordinate
{    
    // Impose a limit on how far user can scroll in x
    if (coordinate == CPTCoordinateX) {
        CPTPlotRange *maxRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-plotlength) 
                                                              length:CPTDecimalFromFloat(plotlength)];

        CPTPlotRange *changedRange = [[newRange copy] autorelease];
        [changedRange shiftEndToFitInRange:maxRange];
        [changedRange shiftLocationToFitInRange:maxRange];
        newRange = changedRange;
        double start = [newRange locationDouble];
        double end = [newRange lengthDouble] + start;
        UIDevice *device = [UIDevice currentDevice];
        if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
            [self buildXaxisFromStart:start toEnd:end withNumSpaces:4];
        }
        else{
            [self buildXaxisFromStart:start toEnd:end withNumSpaces:2];
        }
    }
    if (coordinate == CPTCoordinateY) {
        // dont let y range change regardless of what the user does.
        newRange = [self getYPlotRange];
        [self buildYaxis];
    }
    return newRange;
}

#pragma mark -
#pragma mark CPTScatterPlot delegate method

-(void)scatterPlot:(CPTScatterPlot *)plot plotSymbolWasSelectedAtRecordIndex:(NSUInteger)index
{
// works fine, just commented out to see what speed increase I can get
//    if (symbolTextAnnotation) {
//        [graph.plotAreaFrame.plotArea removeAnnotation:symbolTextAnnotation];
//        [symbolTextAnnotation release];
//        symbolTextAnnotation = nil;
//    }
//
//    // Setup a style for the annotation
//    CPTMutableTextStyle *hitAnnotationTextStyle = [CPTMutableTextStyle textStyle];
//    hitAnnotationTextStyle.color = [CPTColor whiteColor];
//    hitAnnotationTextStyle.fontSize = 16.0f;
//    hitAnnotationTextStyle.fontName = @"Helvetica-Bold";
//
//    // Determine point of symbol in plot coordinates
//
//    DataSource* dataSource = [plotDataSources objectForKey:[plot name]];
//    NSDictionary* dataPoints = [dataSource allDataPointsWithStepConsolidation:consolidationStepCount];
//    NSArray* sortedDatesArray = [dataPoints allKeys];
//    
//    NSDate *xdate = [sortedDatesArray objectAtIndex:index];
//    NSNumber *y = [NSNumber numberWithDouble:[self getPlotPointFromDataPoint:[dataPoints objectForKey:xdate]]];
//    NSNumber *x = [NSNumber numberWithDouble:[xdate timeIntervalSinceDate:firstkey]];
//    NSArray *anchorPoint = [NSArray arrayWithObjects:x, y, nil];
//
//    // Add annotation
//    // First make a string for the y value
//    NSString *yString = [yFormatter stringFromNumber:y];
//    NSString *dateText = [dateFormatterOverlay stringFromDate:xdate];
//
//    NSString* fullString = [NSString stringWithFormat:@"%@ = %@",dateText,yString];
//    
//    // Now add the annotation to the plot area
//    CPTTextLayer *textLayer = [[[CPTTextLayer alloc] initWithText:fullString style:hitAnnotationTextStyle] autorelease];
//    symbolTextAnnotation = [[CPTPlotSpaceAnnotation alloc] initWithPlotSpace:graph.defaultPlotSpace anchorPlotPoint:anchorPoint];
//    symbolTextAnnotation.contentLayer = textLayer;
//    symbolTextAnnotation.displacement = CGPointMake(0.0f, 20.0f);
//    [graph.plotAreaFrame.plotArea addAnnotation:symbolTextAnnotation];    
}

@end
