//
//  ExpandableTableViewController.h
//  hypOps
//
//  Created by Ian Firth on 20/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SECTION_HEIGHT 44

@interface ExpandableTableViewController : UITableViewController {
    @private
    NSMutableArray *visibility;
}

- (BOOL) tableView:(UITableView *)tableView isHeaderExpandedForSection:(NSInteger)section;
- (void) tableView:(UITableView *)tableView setHeaderExpandedForSection:(NSInteger)section expandedState:(BOOL)expanded;

@end

#pragma mark -

@interface ExpandableTableViewController (AbstractMethods)
// Define Abstract methods which should be implemented by subclasses:
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section;
- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section;
- (CGFloat)tableView:(UITableView *)tableView heightForUnExpandedHeaderInSection:(NSInteger)section;
@end
