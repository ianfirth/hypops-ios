//
//  RRDDisplayButton.m
//  hypOps
//
//  Created by Ian Firth on 03/10/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "RRDDisplayButton.h"
#import "ImageBuilder.h"
#import "ProgressAlert.h"
#import "DataSource.h"
#import "RRDPlotViewController.h"
#import "CommonUIStaticHelper.h"

@interface RRDDisplayButton (Private)
    - (void)didPresentAlertView:(UIAlertView *)alertView;
    -(void)graphButtonPressed:(id)sender;
    -(void)resetButtonPressed:(id)sender;
    -(BOOL)displayGraph;
    -(BOOL)reloadGraph;
    -(void) setUpdateDate:(NSDate*)date; 
@end
@implementation RRDDisplayButton

@synthesize delegate;
@synthesize reference;
@synthesize alertView;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

-(void) didReceiveMemoryWarning{
    if (graphPopover){
        [graphPopover dismissPopoverAnimated:NO];
    }
}

-(void) dealloc{
    if (graphButton){
        [graphButton release];
    }
    if (refreshButton){
        [refreshButton release];
    }
    if (updateTimeLabel){
        [updateTimeLabel release];
    }
    if (mainTitleLabel){
        [mainTitleLabel release];
    }
    if(backgroundButton){
        [backgroundButton release];
    }
    [self setReference:nil];
    [self setDelegate:nil];
    [super dealloc];
}

-(void) viewDidLoad{
    [super viewDidLoad];
    graphButton = [[UIButton alloc] init];
    [graphButton setImage:[ImageBuilder graphImage] forState:UIControlStateNormal];
    refreshButton = [[UIButton alloc] init];
    [refreshButton setImage:[UIImage imageNamed:@"Refresh"] forState:UIControlStateNormal];
    mainTitleLabel= [[UILabel alloc] init];
    [mainTitleLabel setText:@"View Performance Data"];
    [mainTitleLabel setFont:[UIFont fontWithName:@"Helvetica Bold" size:14.0]];
    
    [mainTitleLabel setTextColor:[UIColor blackColor]];
    updateTimeLabel = [[UILabel alloc] init];
    [updateTimeLabel setText:@""];
    [updateTimeLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
    
    backgroundButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backgroundButton retain];
    [backgroundButton addTarget:self 
             action:@selector(graphButtonPressed:)
            forControlEvents:UIControlEventTouchDown];

    [[self view] addSubview:backgroundButton];
    [backgroundButton addSubview:graphButton];
    [backgroundButton addSubview:mainTitleLabel];
    [backgroundButton addSubview:updateTimeLabel];
    [backgroundButton addSubview:refreshButton];
    [backgroundButton setBackgroundColor:[UIColor clearColor]];
    [graphButton addTarget:self 
                    action:@selector(graphButtonPressed:)
          forControlEvents:UIControlEventTouchDown];

    [refreshButton addTarget:self 
                    action:@selector(refreshButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // set the button locations here  
    CGRect bounds = [[self view] bounds];
    [backgroundButton setFrame:bounds];
    CGRect graphButtonRect = CGRectMake(bounds.origin.x+2,
                                        bounds.origin.y+2, 
                                        40,40);
    
    [graphButton setFrame:graphButtonRect];
    [refreshButton setFrame:CGRectMake(bounds.origin.x + (bounds.size.width)-42,2,40,40)];
    CGRect mainTitleLabelRect = CGRectMake(graphButton.bounds.origin.x + graphButton.bounds.size.width + 5,
                                           bounds.origin.y+2, 
                                           bounds.size.width-refreshButton.bounds.size.width - graphButton.bounds.size.width -10,25);
    CGRect updateTimeLabelRect = CGRectMake(graphButton.bounds.origin.x + graphButton.bounds.size.width + 5,
                                            bounds.origin.y+25, 
                                            bounds.size.width-refreshButton.bounds.size.width - graphButton.bounds.size.width -10,15);

    [mainTitleLabel setFrame:mainTitleLabelRect];
    [updateTimeLabel setFrame:updateTimeLabelRect];
    [backgroundButton setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [backgroundButton setAutoresizesSubviews:YES];

    [refreshButton setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];
    [mainTitleLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin];
    [mainTitleLabel setTextAlignment:UITextAlignmentCenter];
    [updateTimeLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin];
    [updateTimeLabel setTextAlignment:UITextAlignmentCenter];
    [self setUpdateDate:[[self delegate] getLastRefreshTime]];
}

-(void) setUpdateDate:(NSDate*)date{
    if (date){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        // toDO improve this so it says today if today, yesterday otherwise date?
        [dateFormatter setDateFormat:@"dd MMM yyyy (HH:mm)"];
        NSString* dateAsString = [dateFormatter stringFromDate:date];
        [updateTimeLabel setText: dateAsString] ;
        [dateFormatter release];
    }
    else
    {
        [updateTimeLabel setText: @""] ;
    }
}

// method to display the graph.
// Shows a progress loading dialog then
// the event for the loading dialog being complete actually adds the graph to the display.
-(void)graphButtonPressed:(id)sender{
    ProgressAlert *progressAlert = [[ProgressAlert alloc] initWithTitle:@"Loading" delegate:self];
    [self setAlertView:progressAlert];
    [progressAlert release];
    [[self alertView] setTag:100];
    [[self alertView] show];
}

-(void)refreshButtonPressed:(id)sender{
    ProgressAlert *progressAlert = [[ProgressAlert alloc] initWithTitle:@"Loading" delegate:self];
    [self setAlertView:progressAlert];
    [progressAlert release];
    [[self alertView] setTag:101];
    [[self alertView] show];
}

#pragma mark -
#pragma mark ProgressAlert delegate

- (void)didPresentAlertView:(UIAlertView *)theAlertView{
    // if a loading alert then do this otherwise so nothing.
    BOOL result = YES;
    switch ([theAlertView tag]) {
        case 100:
            result = [self displayGraph];
            break;
        case 101:
            result = [self reloadGraph];
            break;
        default:
            break;
    }
    // dismiss the loading Dialog 
    [theAlertView dismissWithClickedButtonIndex:0 animated:YES];
    [self setAlertView: nil];

    if (!result){        
        // display the eror dialog
        UIAlertView *alertViewWarning = [[UIAlertView alloc] initWithTitle:@"Data Load Failed" 
                                                                   message:@"The Performance data failed to load.  Check that the hyperviosr host is still contactable and try again." 
                                                                  delegate:nil 
                                                         cancelButtonTitle:nil 
                                                         otherButtonTitles:@"OK", nil];
        [self setAlertView:alertViewWarning];
        [alertViewWarning release];
        [[self alertView] show];
    }
}

-(BOOL)reloadGraph{
    BOOL result = [[self delegate] reloadRoundRobinDatabase];
    [self setUpdateDate:[[self delegate] getLastRefreshTime]];
    return result;
}

-(BOOL)displayGraph{
    RoundRobinDatabase* rrd = [[self delegate] roundRobinDatabase];
    if (rrd){
                        
        RRDPlotViewController* plotView = [[RRDPlotViewController alloc] initWithRRDDsiplayButtonDelegate:[self delegate] withReference:[self reference]];
        
        UIDevice *device = [UIDevice currentDevice];
        if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
            // set a size that fits in the window
            [plotView setContentSizeForViewInPopover: CGSizeMake(1000, 400)];
            graphPopover = [[UIPopoverController alloc] initWithContentViewController:plotView];
            [graphPopover setDelegate:self];
            // permitted arrow direction 0 makes the popover appear without any arrows :)
            [graphPopover presentPopoverFromRect:CGRectMake(0,0,10,10) inView:[self view] permittedArrowDirections:0 animated:YES];
        }
        else{
            [CommonUIStaticHelper displayNextView:plotView usingNavigationController:[[self delegate]navigationController]];
        }
        [plotView release];
    }
    [self setUpdateDate:[[self delegate] getLastRefreshTime]];
    return (rrd != nil);
}

#pragma mark - 
#pragma mark UIPopoverControllerDelegate
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    [popoverController release];
    popoverController = nil;
}
@end
