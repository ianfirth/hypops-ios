//
//  NetworkAddress.h
//  hypOps
//
//  Created by Ian Firth on 29/09/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkAddress : NSObject{
    int deviceID;
    NSString* address;
}
- (id)initWithDeviceID:(int)theDeviceID andAddress:(NSString*) theAddress;

@property (readonly) NSString* address;
@property (readonly) int deviceID;

@end
