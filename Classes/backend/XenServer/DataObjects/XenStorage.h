//
//  XenStorage.h
//  hypOps
//
//  Created by Ian Firth on 03/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XenDescriptiveBase.h"

@interface XenStorage : XenDescriptiveBase {
    /**
     The Array of tags that are associated with the Storage.
     */
    NSArray *tags;

    /**
     */
    NSNumber *physicalSize;

    /**
     */
    NSNumber *physicalUtilization;

    /**
     */
    NSNumber *virtualAllocation;
    
    /**
     */
    NSString *contentType;

    /**
     */
    NSString *type;

    /**
     */
    BOOL shared;
    
    BOOL local_cache_enabled;

}

@property (retain) NSArray *tags;
@property (copy) NSString *contentType;
@property (copy) NSString *type;
@property BOOL shared;
@property (retain) NSNumber *physicalSize;
@property (retain) NSNumber *physicalUtilization;
@property (retain) NSNumber *virtualAllocation;
@property BOOL local_cache_enabled;
@end
