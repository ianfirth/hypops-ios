//
//  XenHostCPU.h
//  hypOps
//
//  Created by Ian Firth on 19/04/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XenBase.h"

@interface XenHostCPU : XenBase {
    /**
     The cpu vendor.
     */
    NSString *vendor; 
    
    /**
     The cpu model.
     */
    NSString *model;  
    
}

@property (copy) NSString *vendor; 
@property (copy) NSString *modelName; 

@end
