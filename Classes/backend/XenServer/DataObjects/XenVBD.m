//
//  MyClass.m
//  hypOps
//
//  Created by Ian Firth on 23/04/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenVBD.h"


@implementation XenVBD

@synthesize type, position, device;

- (id)initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)vbdProperties{
    self = [super initWithConnection:xenConnection objectType:HYPOBJ_VBD Reference:reference Dictionary:vbdProperties];
    if (self){
        NSNumberFormatter * myNumFormatter = [[NSNumberFormatter alloc] init];
        [myNumFormatter setLocale:[NSLocale currentLocale]]; 
        [myNumFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        // next line is very important!
        [myNumFormatter setNumberStyle:NSNumberFormatterDecimalStyle]; 
        
        // type
        [self setType:[vbdProperties objectForKey:@"type"]];

        // device
        [self setDevice:[vbdProperties objectForKey:@"device"]];

        // position
        NSString *tempStr = [vbdProperties objectForKey:@"userdevice"];  
        NSNumber *tempNum = [myNumFormatter numberFromString:tempStr];
        [self setPosition:tempNum];
        
        [myNumFormatter release]; 

        // VDI
        [self addReference:[vbdProperties valueForKey:@"VDI"] forObjectType:HYPOBJ_VDI];
    }
    return self;
}

- (void) dealloc{
    [self setDevice:nil];
    [self setType:nil];
    [self setPosition:nil];
    [super dealloc];
}

@end
