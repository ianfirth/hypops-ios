//
//  XenPBD.h
//  hypOps
//
//  Created by Ian Firth on 23/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XenBase.h"


@interface XenPBD : XenBase {
    /**
     Indicates if the PBD is currently attached to the host.
     */
    BOOL currentlyAttached; 
}

@property (readonly) BOOL currentlyAttached;

@end
