//
//  XenHost_metrics.h
//  hypOps
//
//  Created by Ian Firth on 23/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XenBase.h"


@interface XenHost_metrics : XenBase {
    /**
     The total memory that the host has (in bytes).
     */
    NSNumber *totalMemory; 

    /**
     The free memory that the host has (in bytes).
     */
    NSNumber *freeMemory; 

}

@property (copy) NSNumber *totalMemory;
@property (copy) NSNumber *freeMemory;

@end
