//
//  XenBase.h
//  hypOps
//
//  Created by Ian Firth on 03/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

// definition of the types of objects that can be stored in the cache
// these are flags so that they can be used in combination
#define HYPOBJ_VM 1
#define HYPOBJ_STORAGE 2
#define HYPOBJ_HOST 4
#define HYPOBJ_TEMPLATE 8
#define HYPOBJ_NIC 16
#define HYPOBJ_HOST_METRICS 32
#define HYPOBJ_NETWORK 64
#define HYPOBJ_PBD 128
#define HYPOBJ_SNAPSHOT 256
#define HYPOBJ_VM_GUEST_METRICS 512
#define HYPOBJ_HOST_CPU 1024
#define HYPOBJ_VBD 2048
#define HYPOBJ_VDI 4096

#import <Foundation/Foundation.h>
#import "XenHypervisorConnection.h"
#import "HypObjReferencesProtocol.h"

/**
 Class to represent a base XenServer object.
 A great deal of the objects in XenServer API contain common properties.  This class abstracts the common ones
 and forma a base for other objects to extend.
 
 Objects that are representing Xen Hypervisor objects should not hold onto references to other objects, they
 should store the opaque_reference for the objects and access them back via the connection which has a cache of
 all these objects.  This allows for memory to be freed if required and the object to be refetched on demand.
 */
@interface XenBase : NSObject<HypObjReferencesProtocol> {
    /**
     The uuid of the object.
     */
    NSString *uuid;

    /**
     The opaqueRef of the object.
     */
    NSString *opaque_ref;
    
    /**
     The Dictionary of other objects that this object references and requires to be considered complete.
     This is keyed on the hypervisor object type and contains an NSMutable array of objects.  This should not store
     all references, otherwise when one object is loaded the tree will be very large and end up pulling in
     all objects into the graph.
     */
    NSMutableDictionary *references;
 
    /**
     The identifier for the hypervisor Connection that this object was creted from.
     */
    NSString *connectionID;
}    


  
/**
 Create a new instance of the class with the specified properties dictionary.
 @param vmProperties The dictionary of properties retreived via the XenServer API for the object.
 @returns a newly initialized NSPredicate
 */
- (id) initWithConnection:(XenHypervisorConnection *)xenConnection objectType:(int)hypObjectType Reference:(NSString *)reference Dictionary:(NSDictionary *)hostProperties;

/**
  Store a referenece to another Xen object opaque_ref
  @param opaqueReference The opaque reference for the object being referenced.
  @param objectType The type of hypervisor object the reference is for.
 */
- (void) addReference:(NSString *)opaqueReference forObjectType:(int)objectType;

/**
 Store refereneces to other Xen object opaque_refs
 @param opaqueReferences The array of opaque references for the object being referenced.
 @param objectType The type of hypervisor object the references are for.
 */
- (void) addReferences:(NSArray *)opaqueReferences forObjectType:(int)objectType;

/**
 Retreive a referenec to another Xen object opaque_refs
 @param objectType The type of hypervisor object reference to be retreived.
 */
- (NSArray *) referencesForType:(int)objectType;

/**
 Create a Predicate that defines a specific opaque reference.
 @param reference The opaque reference to predicate on
 @returns the new predeicate
 */
+ (NSPredicate *) XenBaseFor:(NSString *)reference;

/**
 * provides a sort descriptor for each object type
 */
+ (NSSortDescriptor*) defaultSortForhypObjectType:(int)hypObjectType;

/**
 * provides a helper for converting the object type to a string
 * This is primarily for debugging aid and not for display to user.
 */
+ (NSString*) ObjectNameForType:(int)hypObjectType;

/**
 returns the hpervisor connection for the object.
 @returns XenHypervisorConnection
 */
-(XenHypervisorConnection *) hypervisorConnection;

@property (copy) NSString *uuid; 
@property (copy) NSString *opaque_ref; 
@property (retain) NSMutableDictionary *references; 
@property (readonly) int objectType; 
@property (copy) NSString *connectionID;

@end
