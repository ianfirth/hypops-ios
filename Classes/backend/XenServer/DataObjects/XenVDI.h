//
//  XenVDI.h
//  hypOps
//
//  Created by Ian Firth on 23/04/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XenDescriptiveBase.h"

@interface XenVDI : XenDescriptiveBase {
    /**
     The size of the disk (in bytes)
     */
    NSNumber *virtual_size; 

    /**
     Indicates if this disk is readonly
     */
    BOOL read_only; 

    /**
     Indicates the disk locaton
     */
    NSString *location;
}

@property (retain) NSNumber *virtual_size;
@property BOOL read_only;
@property (copy) NSString *location;

@end
