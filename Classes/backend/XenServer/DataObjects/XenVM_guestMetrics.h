//
//  XenVM_guestMetrics.h
//  hypOps
//
//  Created by Ian Firth on 23/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XenBase.h"


@interface XenVM_guestMetrics : XenBase {
    /**
     The PV_drivers version dictionary
     Example is :
     "PV_drivers_version" =     {
        build = 39215;
        major = 5;
        micro = 100;
        minor = 6;
        xennet = "4.0";
        xenvbd = "1.0boot";
     };
     */
    NSDictionary *PV_drivers_version;
    
    /**
     The OS_version dictionary
     Example is :
     "os_version" =     {
          distro = windows;
          major = 6;
          minor = 1;
          name = "Microsoft Windows 7 Ultimate |C:\\Windows|\\Device\\Harddisk0\\Partition2";
          spmajor = 0;
          spminor = 0;
      };
     */
    NSDictionary* OS_version;
    
    /**
     The array of Network Address objects
     */
    NSMutableArray* networkAddress;

}

/**
 * returns the PV drivers version as a string
 * @return NSString of the version
 */
- (NSString *)PVDriversVersionString;

/**
 * returns the Guest OS version as a string
 * @return NSString of the version
 */
- (NSString *)OSVersionName;

@property (retain) NSDictionary *PV_drivers_version;
@property (retain) NSDictionary *OS_version;
@property (retain) NSArray *networkAddress;

@end
