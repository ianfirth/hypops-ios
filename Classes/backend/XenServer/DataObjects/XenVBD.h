//
//  MyClass.h
//  hypOps
//
//  Created by Ian Firth on 23/04/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XenBase.h"

@interface XenVBD : XenBase {
    NSString *type;
    NSNumber *position;
    NSString *device;
}

@property (retain) NSString *type;
@property (retain) NSNumber *position;
@property (copy) NSString *device;

@end
