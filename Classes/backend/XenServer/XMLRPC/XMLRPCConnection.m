// 
// Copyright (c) 2010 Eric Czarny <eczarny@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of  this  software  and  associated documentation files (the "Software"), to
// deal  in  the Software without restriction, including without limitation the
// rights  to  use,  copy,  modify,  merge,  publish,  distribute,  sublicense,
// and/or sell copies  of  the  Software,  and  to  permit  persons to whom the
// Software is furnished to do so, subject to the following conditions:
// 
// The  above  copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE  SOFTWARE  IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED,  INCLUDING  BUT  NOT  LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS  OR  COPYRIGHT  HOLDERS  BE  LIABLE  FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY,  WHETHER  IN  AN  ACTION  OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
// 

#import "XMLRPCConnection.h"
#import "XMLRPCConnectionManager.h"
#import "XMLRPCRequest.h"
#import "XMLRPCResponse.h"
#import "NSStringAdditions.h"

@interface XMLRPCConnection (XMLRPCConnectionPrivate)

- (void)connection: (NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;

- (void)connection: (NSURLConnection *)connection didReceiveData: (NSData *)data;

- (void)connection: (NSURLConnection *)connection didFailWithError: (NSError *)error;

#pragma mark -

- (BOOL)connection: (NSURLConnection *)connection canAuthenticateAgainstProtectionSpace: (NSURLProtectionSpace *)protectionSpace;

- (void)connection: (NSURLConnection *)connection didReceiveAuthenticationChallenge: (NSURLAuthenticationChallenge *)challenge;

- (void)connection: (NSURLConnection *)connection didCancelAuthenticationChallenge: (NSURLAuthenticationChallenge *)challenge;

- (void)connectionDidFinishLoading: (NSURLConnection *)connection;

@end

#pragma mark -

@implementation XMLRPCConnection
- (id)initWithXMLRPCRequest: (XMLRPCRequest *)request delegate: (id<XMLRPCConnectionDelegate>)delegate manager: (XMLRPCConnectionManager *)manager runAsync:(BOOL) runasync{
    self = [super init];
    if (self) {
        myManager = [manager retain];
        myRequest = [request retain];
        myIdentifier = [[NSString stringByGeneratingUUID] retain];
        myData = [[NSMutableData alloc] init];
        
        if (runasync){
            myConnection = [[NSURLConnection alloc] initWithRequest:[request request] delegate: self startImmediately:NO];
            
            myDelegate = [delegate retain];
            
            // start connection in another thread
            // wait for a time to pass or for the connection to be returned.
            // if timeout then call cancel on the connection and terminate the thread
            NSNumber *timeout = [[NSUserDefaults standardUserDefaults] valueForKey:@"connectionTimeout"];
            double timeoutDouble = [timeout doubleValue];
            // if there is no value set for any reason (can occur after update I think
            if (timeoutDouble == 0){
                [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithDouble:15] forKey:@"connectionTimeout"];
                timeoutDouble = 15;
            }
            
            connectionTimer = [NSTimer scheduledTimerWithTimeInterval:timeoutDouble target:self selector:@selector(timeoutOverride:)  userInfo:nil repeats:NO];
            [connectionTimer retain];
            
            [myConnection scheduleInRunLoop:[NSRunLoop mainRunLoop]
                                    forMode:NSDefaultRunLoopMode];
            
            // hopefullt this drops out if fail or if closed
            [myConnection start];
        }
        else{
            myConnection = [[NSURLConnection alloc] initWithRequest:[request request] delegate: self];
            
            myDelegate = [delegate retain];
        }

        if (myConnection) {
            NSLog(@"The connection, %@, has been established!", myIdentifier);
        } else {
            NSLog(@"The connection, %@, could not be established!", myIdentifier);
            
            [self release];
            
            return nil;
        }
    }
    
    return self;
}

//define the targetmethod
-(void) timeoutOverride:(NSTimer *) theTimer {
    if ([theTimer isValid]){
        [self cancel];
         NSLog(@"Connection Timeout");
        [connectionTimer release];
        connectionTimer = nil;
        NSError *error = [NSError errorWithDomain:@"Connection Timeout exceeded.  The timeout can be set in the application settings." code:-100 userInfo:nil];
        [self connection:myConnection didFailWithError: error];
    }
}

#pragma mark -
// without this, the cusom authentication delegates will not be called allowing https to be used with
// self signed certificates
- (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection{
    return NO;
}

+ (XMLRPCResponse *)sendSynchronousXMLRPCRequest: (XMLRPCRequest *)request error: (NSError **)error {
    NSData *data = [[[NSURLConnection sendSynchronousRequest: [request request] returningResponse: nil error: error] retain] autorelease];
    
    if (data) {
        return [[[XMLRPCResponse alloc] initWithData: data] autorelease];
    }
    
    return nil;
}

#pragma mark -

- (NSString *)identifier {
    return [[myIdentifier retain] autorelease];
}

#pragma mark -

- (id<XMLRPCConnectionDelegate>)delegate {
    return myDelegate;
}

#pragma mark -

- (void)cancel {
    [myConnection cancel];
}

#pragma mark -

- (void)dealloc {    
    [myManager release];
    [myRequest release];
    [myIdentifier release];
    [myData release];
    [myConnection release];
    [myDelegate release];
    if (connectionTimer){
        [connectionTimer release];
        connectionTimer = nil;
    }
    [super dealloc];
}

@end

#pragma mark -

@implementation XMLRPCConnection (XMLRPCConnectionPrivate)

- (void)connection: (NSURLConnection *)connection didReceiveResponse: (NSURLResponse *)response {
    if([response respondsToSelector: @selector(statusCode)]) {
        int statusCode = [(NSHTTPURLResponse *)response statusCode];
        
        if(statusCode >= 400) {
            NSError *error = [NSError errorWithDomain: @"HTTP" code: statusCode userInfo: nil];
            
            [myDelegate request: myRequest didFailWithError: error];
        } else if (statusCode == 304) {
            [myManager closeConnectionForIdentifier: myIdentifier];
        }
    }
    
    [myData setLength: 0];
}

- (void)connection: (NSURLConnection *)connection didReceiveData: (NSData *)data {
    [myData appendData: data];
}

- (void)connection: (NSURLConnection *)connection didFailWithError: (NSError *)error {
    XMLRPCRequest *request = [[myRequest retain] autorelease];
    
    NSLog(@"The connection, %@, failed with the following error: %@", myIdentifier, [error localizedDescription]);
    
    [myDelegate request: request didFailWithError: error];
    
    [myManager closeConnectionForIdentifier: myIdentifier];
}

#pragma mark -

- (BOOL)connection: (NSURLConnection *)connection canAuthenticateAgainstProtectionSpace: (NSURLProtectionSpace *)protectionSpace {
    return [myDelegate request: myRequest canAuthenticateAgainstProtectionSpace: protectionSpace];
}

- (void)connection: (NSURLConnection *)connection didReceiveAuthenticationChallenge: (NSURLAuthenticationChallenge *)challenge {
    [myDelegate request: myRequest didReceiveAuthenticationChallenge: challenge];
}

- (void)connection: (NSURLConnection *)connection didCancelAuthenticationChallenge: (NSURLAuthenticationChallenge *)challenge {
    [myDelegate request: myRequest didCancelAuthenticationChallenge: challenge];
}

- (void)connectionDidFinishLoading: (NSURLConnection *)connection {
    [connectionTimer invalidate];
    if (connectionTimer){
       [connectionTimer release];
        connectionTimer = nil;
    }
    if (myData && ([myData length] > 0)) {
        XMLRPCResponse *response = [[XMLRPCResponse alloc] initWithData: myData];
        XMLRPCRequest *request = [myRequest retain];
        
        [myDelegate request: request didReceiveResponse: response];
        [request release];
        [response release];
    }
    
    [myManager closeConnectionForIdentifier: myIdentifier];
}

@end
