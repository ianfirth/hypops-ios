//
//  XenHypervisorConnectionDefinition.m
//  hypOps
//
//  Created by Ian Firth on 28/06/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenHypervisorConnectionDefinition.h"


@implementation XenHypervisorConnectionDefinition

@synthesize address,secure,userName,password,name;

-(id) init{
    self = [super init];
    if (self){
        UITextField *field = [[UITextField alloc] initWithFrame:CGRectMake(0, 10, 200, 30)];
        [self setAddress: field];
        [field release];
        [self address].autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self address].adjustsFontSizeToFitWidth = YES;
        [self address].textColor = [UIColor blackColor];
        [self address].placeholder = @"MyServer.com";
        [self address].keyboardType = UIKeyboardTypeURL;
        [self address].secureTextEntry = NO;
        [self address].backgroundColor = [UIColor whiteColor];
        [self address].autocorrectionType = UITextAutocorrectionTypeNo; // no auto correction support
        [self address].autocapitalizationType = UITextAutocapitalizationTypeNone; // no auto capitalization support
        [self address].textAlignment = UITextAlignmentLeft;
        [self address].tag = 0;
        [self address].clearButtonMode = UITextFieldViewModeWhileEditing; // no clear 'x' button to the right
        [[self address] setEnabled: YES];
        
        UISwitch *uiSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 10, 50, 30)];
        [self setSecure: uiSwitch];
        [uiSwitch release];
        [self secure].autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self secure].tag = 0;
        [[self secure] setOn:YES];
        [[self secure] setEnabled: YES];
        
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 10, 190, 30)];
        [self setUserName: textField];
        [textField release];
        [self userName].autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self userName].adjustsFontSizeToFitWidth = YES;
        [self userName].textColor = [UIColor blackColor];
        [self userName].placeholder = @"my User";
        [self userName].keyboardType = UIKeyboardTypeAlphabet;
        [self userName].secureTextEntry = NO;
        [self userName].backgroundColor = [UIColor whiteColor];
        [self userName].autocorrectionType = UITextAutocorrectionTypeNo; 
        [self userName].autocapitalizationType = UITextAutocapitalizationTypeNone; 
        [self userName].textAlignment = UITextAlignmentLeft;
        [self userName].tag = 0;
        [self userName].clearButtonMode = UITextFieldViewModeWhileEditing; 
        [[self userName] setEnabled: YES];

        textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 10, 190, 30)];
        [self setPassword: textField];
        [textField release];
        [self password].autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self password].adjustsFontSizeToFitWidth = YES;
        [self password].textColor = [UIColor blackColor];
        [self password].placeholder = @"Password";
        [self password].keyboardType = UIKeyboardTypeAlphabet;
        [self password].secureTextEntry = YES;
        [self password].backgroundColor = [UIColor whiteColor];
        [self password].autocorrectionType = UITextAutocorrectionTypeNo; 
        [self password].autocapitalizationType = UITextAutocapitalizationTypeNone; 
        [self password].textAlignment = UITextAlignmentLeft;
        [self password].tag = 0;
        [self password].clearButtonMode = UITextFieldViewModeWhileEditing; 
        [[self password] setEnabled: YES];

        textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 10, 155, 30)];
        [self setName: textField];
        [textField release];
        [self name].autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self name].adjustsFontSizeToFitWidth = YES;
        [self name].textColor = [UIColor blackColor];
        [self name].placeholder = @"My XenServer";
        [self name].keyboardType = UIKeyboardTypeAlphabet;
        [self name].secureTextEntry = NO;
        [self name].backgroundColor = [UIColor whiteColor];
        [self name].autocorrectionType = UITextAutocorrectionTypeYes; 
        [self name].autocapitalizationType = UITextAutocapitalizationTypeSentences;
        [self name].textAlignment = UITextAlignmentLeft;
        [self name].tag = 0;
        [self name].clearButtonMode = UITextFieldViewModeWhileEditing; 
        [[self name] setEnabled: YES];
    }
    return self;
}

- (int)numberOfProperties{
    return 5;
}

-(NSString *) titleForIndex:(int)index{
    switch (index) {
        case 0:
            return @"Address";
            break;
        case 1:
            return @"Secure";
            break;
        case 2:
            return @"Username";
            break;
        case 3:
            return @"Password";
            break;
        case 4:
            return @"Display Name";
            break;
        default:
            return nil;
            break;
    } 
}

-(UIView *) viewForIndex:(int)index{
    switch (index) {
        case 0:
            return address;
            break;
        case 1:
            return secure;
            break;
        case 2:
            return userName;
            break;
        case 3:
            return password;
            break;
        case 4:
            return name;
            break;
        default:
            return nil;
            break;
    }
}

- (void)populateFields:(NSManagedObject *) fieldData{
    
    NSString *addressString = [fieldData valueForKey:@"address"];
    NSURL *raw;
    
    if ([[addressString commonPrefixWithString:@"http://" options:NSCaseInsensitiveSearch] length] == [@"http://" length] || 
        [[addressString commonPrefixWithString:@"https://" options:NSCaseInsensitiveSearch]length] == [@"https://" length] ){
        raw = [NSURL URLWithString:[fieldData valueForKey:@"address"]];
    }
    else
    {
        raw = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@",addressString]];
    }
    
    if ([raw port] != 0){
        address.text = [NSString stringWithFormat:@"%@:%@",[raw host],[raw port]] ;
    }
    else
    {
        address.text = [NSString stringWithFormat:@"%@",[raw host]] ;
    }
    password.text = [fieldData valueForKey:@"password"];
    userName.text = [fieldData valueForKey:@"username"];
    name.text = [fieldData valueForKey:@"name"];
    secure.on = [[raw scheme] isEqualToString:@"https"];

}

-(void) saveDataToManagedObject:(NSManagedObject *) managedObject{
    
    // If appropriate, configure the new managed object.
    if ([name text] == nil || [[name text] isEqualToString: @""]){
        [name setText:[address text]];
    }
    
    // process address here
    // if it does not start with http:// or https:// then look at the 
    // secure box and add http or https as appropriate
    NSString *enteredAddress = [address text];
    // can i make this case insensitve?
    if ([[enteredAddress commonPrefixWithString:@"http://" options:NSCaseInsensitiveSearch] length] == [@"http://" length] || 
        [[enteredAddress commonPrefixWithString:@"https://" options:NSCaseInsensitiveSearch]length] == [@"https://" length] ){
        // address entered begins with http or https
    }
    else
    {
        NSString *prefix = @"https";
        if (! secure.on){
            prefix = @"http";
        }
        enteredAddress = [NSString stringWithFormat:@"%@://%@",prefix,enteredAddress];
    }
    [managedObject setValue:enteredAddress forKey:@"address"];
    [managedObject setValue:[userName text] forKey:@"username"];
    [managedObject setValue:[password text] forKey:@"password"];
    [managedObject setValue:[name text] forKey:@"name"];
    
    // put hypervisor number into the type field now
    [managedObject setValue:[NSString stringWithFormat:@"%i",HYPERVISOR_XEN] forKey:@"type"];
    // can add metadata to these at a later point if required.
    // the datamodel supports metadata for this purpose.
}

-(void) dealloc{
    [self setAddress:nil];
    [self setUserName:nil];
    [self setPassword:nil];
    [self setSecure:nil];
    [self setName:nil];
    [super dealloc];
}

@end
