//
//  XenHypervidoeConnection.m
//  hypOps
//
//  Created by Ian Firth on 20/02/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

// comment this in for more debug level logging.
//#define logCommsData

#import "XenHypervisorConnection.h"
#import "XenPoolViewController.h"
#import "XMLRPCRequest.h"
#import "XMLRPCResponse.h"
#import "HypervisorConnectionFactory.h"
#import "XenVM.h"
#import "XenHost.h"
#import "XenStorage.h"
#import "XenNIC.h"
#import "XenHost_metrics.h"
#import "XenVDI.h"
#import "XenVBD.h"
#import "XenNetwork.h"

// declare the private methods
@interface XenHypervisorConnection ()
- (void)setLastErrorFromXMLRPCResponse: (XMLRPCResponse *)response;
- (void)sendRequestWithMethod:(NSString *)method runAsync:(BOOL)runAsync;
- (void)sendRequestWithMethod:(NSString *)method andParamters:(NSArray *)parameters runAsync:(BOOL)runAsync;
- (void)sendRequestWithMethod:(NSString *)method andParamter:(NSString *)parameter runAsync:(BOOL)runAsync;
- (void)ProcessRequestQueue;
- (void) processLoginResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processEventRegisterAllResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processEventNextResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processEventUnregisterAllResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processVMResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleVMObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processHostResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleHostObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processHostMetricsResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleHostMetricsObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processHostCPUResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleHostCPUObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processNICResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleNICObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processNetworkResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleNetworkObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processStorageResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleStorageObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processPBDResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSinglePBDObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processVMGuestMetricsResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleVMGuestMetricsObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processVBDResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleVBDObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processVDIResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleVDIObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processType:(int)objectType usingResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processObjectRef:(NSString *) requestedOpaqueRef Type:(int)objectType usingResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processEventDataUsingResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (XenBase *) buildObjectType:(int)objectType Reference:reference FromDictionary:objectContent;
- (void) setConnectionAddress:(NSURL*) theConnectionAddress;
- (void) setSessionID:(NSString *)theSessionID;

@end

// store the list of class names that the XenServer API uses to register events for (along with the
// identifier used by this connection class, so that all objects that are stored in the cache are 
// also registered with the eventing mechanism.
static NSDictionary *eventClasses = nil;

@implementation XenHypervisorConnection

@synthesize closing;

- (NSURL*) connectionAddress{
    return connectionAddress;
}

- (NSString*) sessionID{
    return sessionID;
}

- (void) setSessionID:(NSString *)theSessionID{
    sessionID = [theSessionID copy];
}

- (void) setConnectionAddress:(NSURL*) theConnectionAddress{
    connectionAddress  = [theConnectionAddress retain]; 
}

- (void)reloadCoreData;{
    int requestObjects = 0;
    if ([self isUpdateAvailableForType:HYPOBJ_VM]){
        requestObjects = HYPOBJ_VM;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_STORAGE]){
        requestObjects = requestObjects | HYPOBJ_STORAGE;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_HOST]){
        requestObjects = requestObjects | HYPOBJ_HOST;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_NETWORK]){
        requestObjects = requestObjects | HYPOBJ_NETWORK;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_STORAGE]){
        requestObjects = requestObjects | HYPOBJ_STORAGE;
    }
    
    if (requestObjects != 0){
        [self RequestHypObjectsForType:requestObjects];  //VM implies TEMPLATE TOO
    }
}

// constructor for the class
-(id) init{
    // build the event dictionary
    if (!eventClasses){
       eventClasses = [NSDictionary dictionaryWithObjectsAndKeys:
                       [NSNumber numberWithInt:HYPOBJ_VM], @"vm",
                       [NSNumber numberWithInt:HYPOBJ_HOST], @"host",
                       [NSNumber numberWithInt:HYPOBJ_STORAGE], @"sr",
                       [NSNumber numberWithInt:HYPOBJ_NETWORK], @"network",
                       [NSNumber numberWithInt:HYPOBJ_NIC], @"pif",
                       [NSNumber numberWithInt:HYPOBJ_PBD], @"pbd",
                       [NSNumber numberWithInt:HYPOBJ_VM_GUEST_METRICS], @"vm_guest_metrics",
                       [NSNumber numberWithInt:HYPOBJ_HOST_METRICS], @"host_metrics",
                       [NSNumber numberWithInt:HYPOBJ_VBD], @"vbd",
                       [NSNumber numberWithInt:HYPOBJ_VDI], @"vdi",
                       nil];
      [eventClasses retain];
    }

    hypervisorConnectionType = HYPERVISOR_XEN;

    [self setClosing:NO]; 
    [super init];
    return self;
}
// implement any abstract methods that are required

// close the connection
- (void)closeConnection{
    NSNumber *hypervisorId = [[NSNumber alloc] initWithInt:HYPERVISOR_XEN];
    NSMutableDictionary *xenConnections = [[HypervisorConnectionFactory connectionDictionary] objectForKey:hypervisorId];
    if (xenConnections != nil){
        NSString *key = nil;
        for (NSString* connectionKey in [xenConnections allKeys]){
            XenHypervisorConnection *connection = [xenConnections objectForKey:connectionKey];
            if (connection == self){
                key = connectionKey;
            }
        }
        if (key){
            [self setClosing:YES];
            NSLog(@"Closing connection %@", key);
            // send an unregister and logout
            NSArray *x = [[NSArray alloc] init];
            [self sendRequestWithMethod:CMD_EVENT_UNREGISTER andParamters:[NSArray arrayWithObjects:sessionID,x,nil] runAsync:NO];
            [x release];
            [self sendRequestWithMethod:CMD_SESSION_LOGOUT andParamter:sessionID runAsync:NO];
            // if suspending dont remove the connection
            if (!suspending){
               [xenConnections removeObjectForKey:key];
            }
        }
    }
    [hypervisorId release];
    if (suspending){
        suspending = NO;
        suspended = YES;
    }
    else
    {
        connectionUsername = nil;
        connectionPassword = nil;
    }
}


// Connect to the hypervisor using the supplied details
// return Yes if connected or No if failed
- (void)ConnectToAddress:(NSString *)address withUsername:(NSString *)username andPassword:(NSString*)password
{
    NSLog(@"ConnectToAddress %@ username is %@",address,username);
    if ([self GetConnectionState] != CONNECTION_CONNECTED && [self GetConnectionState] != CONNECTION_CONNECTING){
        closing = NO;
        NSString *finalAddress;
        if (![address hasPrefix:@"http://"] && ![address hasPrefix:@"https://"]){
            finalAddress = [@"http://" stringByAppendingString:address];
        }
        else {
            finalAddress = address;
        }
        [self setConnectionAddress: [NSURL URLWithString:finalAddress]];

        // use the async version so that can apply short timeout from settings
        [self sendRequestWithMethod:CMD_SESSION_LOGIN andParamters:[NSArray arrayWithObjects:username,password,nil] runAsync:YES];
        [connectionUsername release];
        connectionUsername = nil;
        connectionUsername = [username copy];
        [connectionPassword release];
        connectionPassword = nil;
        connectionPassword = [password copy];
    }
    else {
        // dont connect again just imply the connect
        NSLog(@"Already connected");  
    }
}

// close the connection mark all objects as outofdate
- (void)suspendConnection{
    if (autoUpdateEnabled){
        suspending = YES;
        [self closeConnection];
        [self flushHypObjectCache];
    }
}

// clear the cache, reopen the connection mark all objects as outofdate
- (void)resumeConnection{
    if (autoUpdateEnabled){
        resuming = YES;
        [self ConnectToAddress:[connectionAddress description] withUsername:connectionUsername andPassword:connectionPassword];
    }
}

// Obtain the root view controller to be used for the Xen connections
// this allows different connection types to have totally different presentations
// and to be able to take advantage of all the implementation details for the specific hypervisor platform.
// in future this might also need to be modified to provide different ui based on iPhone/iPad, or maybe
// there should be a common UI that just works in both TBD.
- (UIViewController *)GetViewController{
    if (!xenPoolViewController){
         xenPoolViewController = [[XenPoolViewController alloc] initWithHypervisorConnection:self];
    }
    return xenPoolViewController;
}

// return the state of the current connection
- (ConnectionState) GetConnectionState{
    //Todo --> implement this properly if have sessionID then its connected I think right now
    // might not be correct after dropped to backgrond for long periods of time.  Might need to
    // be resolved as part of reconnection logic TBD.
    if (suspending){
        return CONNECTION_SUSPENDING;
    }

    if (resuming){
        return CONNECTON_RESUMING;
    }

    if (suspended){
        return CONNECTION_SUSPENDED;
    }

    if ([self sessionID] == nil){
        return CONNECTION_UNCONNECTED;
    }
    else
    {
        return CONNECTION_CONNECTED;
    }
}

// clean up
- (void)dealloc{
    NSLog(@"dealloc XenHypervisor Connection");
    if (outstandingSingleObjectRequestsByType){
        [outstandingSingleObjectRequestsByType release];
        outstandingSingleObjectRequestsByType = nil;
    }
    if (eventClasses){
        [eventClasses release];
        eventClasses = nil;
    }
    
    if (manager){
        [manager closeConnections];
        [manager release];
        manager = nil;
    }
    [self setSessionID:nil];
    
    if (xenPoolViewController){
        [xenPoolViewController release];
        xenPoolViewController = nil;
    }
    
    if (connectionAddress){
        [connectionAddress release];
        connectionAddress = nil;
    }
    [super dealloc];
}

#pragma mark -
#pragma mark XMLRPCConnectionDelegate protocol

/*
 * call back that is called every time the XMLRPCConnection receives a response
 */
- (void)request: (XMLRPCRequest *)request didReceiveResponse: (XMLRPCResponse *)response {
    
    NSLog (@" ------ Received response to request %@ --------",[request method]);

    // dont process any responses if the connection is closing.
    if (closing){
        NSLog(@"Not processing response as connection is closing");
        if ([[request method] isEqualToString:CMD_SESSION_LOGOUT]){
            if (manager){
                [manager closeConnections];
                [manager release];
                manager = nil;
                // clear the cache
                if (cachedHypObjects){
                    [cachedHypObjects removeAllObjects];
                }
            }
        }
        [self setSessionID:nil];
        return;
    }
   
    BOOL sucsess = YES;
    if ([response isFault]) {
        sucsess = NO;
        NSLog(@"Fault code: %@", [response faultCode]);
        NSLog(@"Fault string: %@", [response faultString]);
        [self setLastErrorFromXMLRPCResponse:response];
    } else {
        //NSLog(@"Parsed response: %@", [response object]);
    }
    //NSLog(@"Response body: %@", [response body]);

    NSDictionary *responseData =  [response object];
    if (![response isFault] && responseData != nil){
        NSString *status = [responseData valueForKey:@"Status"];
        sucsess = [status isEqualToString:@"Success"];
    }

    // just fire a delegate saying got response
    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
    for (id theConnectionDelegate in copyOfDelegates){
        if ([theConnectionDelegate respondsToSelector: @selector(hypervisorResponseReceived:requestCmd:error:)]){
            NSError* error = [NSError errorWithDomain:@"blah" code:6 userInfo:nil];
            [theConnectionDelegate hypervisorResponseReceived:self requestCmd:[request method] error:error];
        }
    }

    if (!sucsess){
        NSArray* errorDescription = [responseData valueForKey:@"ErrorDescription"];
        NSString* errorCode = [errorDescription objectAtIndex:0];
        // if a request reports a different address to connect to as the specified server is a slave then
        // resend the request to the new address and ignore the error
        // otherwise pass the error on to be dealt wth.
        // this is only processed if the original request was http, otherwise it will not
        // work as the new address is only an ipAddress which is not enough for the redirect.
        // There is no solution for this in the htts world at present.
        if ([errorCode isEqualToString:ERROR_HOST_IS_SLAVE]){
            NSString *newAddress = [errorDescription objectAtIndex:1];
            NSString *newUrlString = [NSString stringWithFormat:@"%@://%@", [[self connectionAddress] scheme], newAddress];
            [self setConnectionAddress: [NSURL URLWithString:newUrlString]];
            // resend the request to the new address
            [self sendRequestWithMethod:[request method] andParamter:[[request parameters] objectAtIndex:0]runAsync:NO];
            return;
        }
    }
    
    NSString *requestedOpaqueRef = nil;
        
    if ( [request method] == CMD_SESSION_LOGIN){
        [self processLoginResponse:responseData withSucsess:sucsess];
    }
    if ( [request method] == CMD_EVENT_REGISTER){
        [self processEventRegisterAllResponse:responseData withSucsess:sucsess];
    }
    if ( [request method] == CMD_EVENT_UNREGISTER){
        [self processEventUnregisterAllResponse:responseData withSucsess:sucsess];
    }
    if ( [request method] == CMD_EVENT_NEXT){
        [self processEventNextResponse:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_VM_GET_ALL){
        [self processVMResponse:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_VM_GET_ONE){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleVMObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_HOST_GET_ALL){
        [self processHostResponse:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_HOST_GET_ONE){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleHostObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_HOST_CPUS_GET_ALL){
        [self processHostCPUResponse:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_HOST_CPUS_GET_ONE){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleHostCPUObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_STORAGE_GET_ALL){
        [self processStorageResponse:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_STORAGE_GET_ONE){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleStorageObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_NIC_GET_ALL){
        [self processNICResponse:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_NIC_GET_ONE){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleNICObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_NETWORK_GET_ALL){
        [self processNetworkResponse:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_NETWORK_GET_ONE){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleNetworkObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_HOST_METRICS_GET_ALL){
        [self processHostMetricsResponse:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_HOST_METRICS_GET_ONE){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleHostMetricsObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_PBD_GET_ALL){
        [self processPBDResponse:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_PBD_GET_ONE){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSinglePBDObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_VM_GUESTMETRICS_GET_ALL){
        [self processVMGuestMetricsResponse:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_VM_GUESTMETRICS_GET_ONE){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleVMGuestMetricsObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_VBD_GET_ALL){
        [self processVBDResponse:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_VBD_GET_ONE){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleVBDObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_VDI_GET_ALL){
        [self processVDIResponse:responseData withSucsess:sucsess];
    }
    else if ([request method] == CMD_VDI_GET_ONE){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleVDIObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    
    // if this was a response to a single object request then clean up the queue
    if (requestedOpaqueRef){
        NSLog(@"Received data with ref %@",requestedOpaqueRef);
        // find the object in the request list
        NSArray* keys = [[outstandingSingleObjectRequestsByType allKeys] copy];
        for(NSNumber* num in keys){
            NSMutableArray* queuedReferences = [outstandingSingleObjectRequestsByType objectForKey:num];
            if ([queuedReferences count] >0){
                if ([queuedReferences containsObject:requestedOpaqueRef]){
                    [queuedReferences removeObject:requestedOpaqueRef];
                    NSLog(@"Removing ref %@ of type %@",requestedOpaqueRef,num);
                }
            }
            if ([queuedReferences count] ==0) {
                NSLog(@"No more outstanding requests for objects of type %@",num);
                [outstandingSingleObjectRequestsByType removeObjectForKey:num];
            }
        }
        
#if defined (logCommsData)
        NSLog(@"%@", outstandingSingleObjectRequestsByType);
#endif 
        [keys release];
    }
    
    // precess any firther requests in the queue that were waiting for this response
    // TODO if requests fail listen to this in the loading and report the issue otherwise 
    // can be locked forever
    [self ProcessRequestQueue];
}


/*
 * call back that is called every time the XMLRPCConnection receives an error
 */
- (void)request: (XMLRPCRequest *)request didFailWithError: (NSError *)error{
    NSLog(@"didFailWitherror: %@", [error localizedDescription]);
    [self setLastError:error]; 
    
    if ([request method] == CMD_SESSION_LOGIN){
        [self setSessionID:nil];
        NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
        for (id theConnectionDelegate in copyOfDelegates){
            if ([theConnectionDelegate respondsToSelector: @selector(hypervisorConnectedToConnection:withResult:)]){
                [theConnectionDelegate hypervisorConnectedToConnection:self withResult:NO];
            }
        }
        [copyOfDelegates release];
    }
    // todo what about the get one calls. Should these relate to the actual object that failed get since we can tell
    else{
        if (hypervisorConnectionDelegates){
            for (id delegate in hypervisorConnectionDelegates){
                if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withResult:)]){
                    if ([request method] == CMD_VM_GET_ALL){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_VM | HYPOBJ_TEMPLATE withResult:NO];
                    }
                    else if ([request method] == CMD_HOST_GET_ALL){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_HOST withResult:NO];
                    }
                    else if ([request method] == CMD_NIC_GET_ALL){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_NIC withResult:NO];
                    }
                    else if ([request method] == CMD_STORAGE_GET_ALL){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_STORAGE withResult:NO];
                    }
                    else if ([request method] == CMD_HOST_METRICS_GET_ALL){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_HOST_METRICS withResult:NO];
                    }
                    else if ([request method] == CMD_PBD_GET_ALL){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_PBD withResult:NO];
                    }
                    else if ([request method] == CMD_VM_GUESTMETRICS_GET_ALL){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_VM_GUEST_METRICS withResult:NO];
                    }
                    else if ([request method] == CMD_VBD_GET_ALL){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_VBD withResult:NO];
                    }
                    else if ([request method] == CMD_VDI_GET_ALL){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_VDI withResult:NO];
                    }
                }
            }
        }
    }
}

// for some reason and this probably needs some deeper thought the following allows for 
// self signed certificates to work on the simulator
// but not the the iOS device itslef.  These breakpoints are never hit when on the real
// device
- (void)request: (XMLRPCRequest *)request didReceiveAuthenticationChallenge: (NSURLAuthenticationChallenge *)challenge{
    // to make this work like XenServer I should get the fingerpring here and store it if I have not had one before
    // if it is different ask if you want to accept it if not go ahead.  For now leave it like this, although it
    // is not very secure, but then self sighned certs are not anyway :)

    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    }
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (void)request: (XMLRPCRequest *)request didCancelAuthenticationChallenge: (NSURLAuthenticationChallenge *)challenge{
    NSLog(@"didCancelAuthenticationChallenge");
}

- (BOOL)request: (XMLRPCRequest *)request canAuthenticateAgainstProtectionSpace: (NSURLProtectionSpace *)protectionSpace{
    // this will allow self signed certificates accross https connecton
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}


#pragma mark -
#pragma mark XenHypervisorConnection

/*
 * Request a snapshot is taken
 * The VMRef can be of a VM or a snapshot
 * Can return errors -- VM BAD POWER STATE, SR FULL, OPERATION NOT ALLOWED
 * TODO - really should be able to cope with the errors here (i.e. not shutdown etc..)
 */
- (void)RequestNewSnapshot:(NSString*)newName forVMReference:(NSString *)ref{
    //session_id s, VM ref vm, string new_name
    NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref, newName, nil];
    [self sendRequestWithMethod:CMD_VM_SNAPSHOT andParamters:parameters runAsync:NO];
}

/*
 * Request a revert to snapshot
 * can return VM BAD POWER STATE, OPERATION NOT ALLOWED, SR FULL, VM REVERT FAILED
 * TODO - really should be able to cope with the errors here (i.e. not shutdown etc..)
 */
- (void)RequestRevertToSnapshotWithReference:(NSString *)snapshotRef{
    //session_id s, VM ref snapshot
    NSArray *parameters = [NSArray arrayWithObjects:sessionID, snapshotRef, nil];
    [self sendRequestWithMethod:CMD_VM_REVERT andParamters:parameters runAsync:NO];
}

- (void)RequestDestroyObjectType:(int)objectType WithReference:(NSString *)objectRef{
    //session_id s, obejct ref snapshot
    NSArray *parameters = [NSArray arrayWithObjects:sessionID, objectRef, nil];
    NSString* cmd;
    switch (objectType) {
        case HYPOBJ_SNAPSHOT:
            cmd = CMD_VM_DESTROY;
            break;
        default:
            break;
    }
    [self sendRequestWithMethod:cmd andParamters:parameters runAsync:NO];
}

/*
 * Request a power operation for a VM.  No operation will be carried out if
 * the reference is not for a VM (e.g. it is for a template or a snapshot)
 * or the power operation requested is not one of the available ones
 * TODO - really should return some sort of error code in these circumstances
 */
- (void)RequestPowerOperation:(int)operation forVMReference:(NSString *)ref{
    // check that the reference is for a VM
    NSArray *vms = [self hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:ref]]; 
    if (vms != nil && [vms count] > 0){
        // check that the power operation is an available one
        XenVM *vm = [vms objectAtIndex:0];
        NSSet *powerOps = [vm availablePowerOperations];
        for (NSNumber *op in powerOps){
            if ([op intValue] == operation){
                // ok lets request it then
                //NSLog(@"Requesting powerOperation %d", operation);
                switch ([op intValue]) {
                    case XENPOWEROPERATION_START:{
                        NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref, kCFBooleanFalse , kCFBooleanTrue, nil];
                        [self sendRequestWithMethod:CMD_VM_START andParamters:parameters runAsync:NO];
                        break;
                    }
                    case XENPOWEROPERATION_STOP_CLEAN:{
                        NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref, nil];
                        [self sendRequestWithMethod:CMD_VM_STOP_CLEAN andParamters:parameters runAsync:NO];
                        break;
                    }
                    case XENPOWEROPERATION_STOP_FORCE:{
                        NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref, nil];
                        [self sendRequestWithMethod:CMD_VM_STOP_FORCE andParamters:parameters runAsync:NO];
                        break;
                    }
                    case XENPOWEROPERATION_SUSSPEND:{
                        NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref,  nil];
                        [self sendRequestWithMethod:CMD_VM_SUSSPEND andParamters:parameters runAsync:NO];
                        break;
                    }
                    case XENPOWEROPERATION_RESUME:{
                        NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref, kCFBooleanFalse , kCFBooleanTrue, nil];
                        [self sendRequestWithMethod:CMD_VM_RESUME andParamters:parameters runAsync:NO];
                        break;
                    }
                    case XENPOWEROPERATION_RESTART_CLEAN:{
                        NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref, nil];
                        [self sendRequestWithMethod:CMD_VM_RESTART_CLEAN andParamters:parameters runAsync:NO];
                        break;
                    }
                    case XENPOWEROPERATION_RESTART_FORCE:{
                        NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref, nil];
                        [self sendRequestWithMethod:CMD_VM_RESTART_FORCE andParamters:parameters runAsync:NO];
                        break;
                    }
                    default:
                        break;
                }
                break;
            }
        }
    }
}

-(void) LogInformationForType:(int)hypObjectType andReference:(NSString*)reference withMessage:(NSString*) message{
#ifdef logCommsData
    NSString *objectType = [XenBase ObjectNameForType:hypObjectType];

    if (reference){
        NSLog(@"%@ <<type %@ and reference %@>>",message,objectType, reference);
    }
    else{
        NSLog(@"%@ <<type %@>>",message,objectType);
    }
#endif
}

-(void) LogInformationForType:(int)hypObjectType withMessage:(NSString*)message{
    [self LogInformationForType:hypObjectType andReference:nil withMessage:message];
}

/*
 * Request data for the required hypervisor object types from the XenServer
 * Can request multiple types in one go by using the flags as required
 */
- (void)RequestHypObjectsForType:(int)hypObjectType{
    [self LogInformationForType:hypObjectType withMessage:@"Request all"];
    
    if (hypObjectType & HYPOBJ_VM || hypObjectType & HYPOBJ_TEMPLATE || hypObjectType & HYPOBJ_SNAPSHOT){   // for Xen this gets VMs, templates and snapshots
        [self setObjectTypeRequestPending:HYPOBJ_VM];
        [self setObjectTypeRequestPending:HYPOBJ_TEMPLATE];
        [self setObjectTypeRequestPending:HYPOBJ_SNAPSHOT];
        [self sendRequestWithMethod:CMD_VM_GET_ALL andParamter:sessionID runAsync:NO];
    }
    if (hypObjectType & HYPOBJ_HOST){
        [self setObjectTypeRequestPending:HYPOBJ_HOST];
        [self sendRequestWithMethod:CMD_HOST_GET_ALL andParamter:sessionID runAsync:NO];
    }
    if (hypObjectType & HYPOBJ_STORAGE){
        [self setObjectTypeRequestPending:HYPOBJ_STORAGE];
        [self sendRequestWithMethod:CMD_STORAGE_GET_ALL andParamter:sessionID runAsync:NO];
    }
    if (hypObjectType & HYPOBJ_NIC){
        [self setObjectTypeRequestPending:HYPOBJ_NIC];
        [self sendRequestWithMethod:CMD_NIC_GET_ALL andParamter:sessionID runAsync:NO];
    }
    if (hypObjectType & HYPOBJ_NETWORK){
        [self setObjectTypeRequestPending:HYPOBJ_NETWORK];
        [self sendRequestWithMethod:CMD_NETWORK_GET_ALL andParamter:sessionID runAsync:NO];
    }
    if (hypObjectType & HYPOBJ_HOST_METRICS){
        [self setObjectTypeRequestPending:HYPOBJ_HOST_METRICS];
        [self sendRequestWithMethod:CMD_HOST_METRICS_GET_ALL andParamter:sessionID runAsync:NO];
    }
    if (hypObjectType & HYPOBJ_HOST_CPU){
        [self setObjectTypeRequestPending:HYPOBJ_HOST_CPU];
        [self sendRequestWithMethod:CMD_HOST_CPUS_GET_ALL andParamter:sessionID runAsync:NO];
    }
    if (hypObjectType & HYPOBJ_PBD){
        [self setObjectTypeRequestPending:HYPOBJ_PBD];
        [self sendRequestWithMethod:CMD_PBD_GET_ALL andParamter:sessionID runAsync:NO];
    }
    if (hypObjectType & HYPOBJ_VM_GUEST_METRICS){
        [self setObjectTypeRequestPending:HYPOBJ_VM_GUEST_METRICS];
        [self sendRequestWithMethod:CMD_VM_GUESTMETRICS_GET_ALL andParamter:sessionID runAsync:NO];
    }
    if (hypObjectType & HYPOBJ_VBD){
        [self setObjectTypeRequestPending:HYPOBJ_VBD];
        [self sendRequestWithMethod:CMD_VBD_GET_ALL andParamter:sessionID runAsync:NO];
    }
    if (hypObjectType & HYPOBJ_VDI){
        [self setObjectTypeRequestPending:HYPOBJ_VDI];
        [self sendRequestWithMethod:CMD_VDI_GET_ALL andParamter:sessionID  runAsync:NO];
    }
}

-(void) queueRequest:(NSString *)reference ForType:(int)hypObjectType{
    if (!outstandingSingleObjectRequestsByType){
        outstandingSingleObjectRequestsByType = [[NSMutableDictionary alloc] initWithCapacity:5];
    }
    
    NSMutableArray* typeList = [outstandingSingleObjectRequestsByType objectForKey:[NSNumber numberWithInt:hypObjectType]];
    if (!typeList){
        typeList = [[NSMutableArray alloc] init];
        [outstandingSingleObjectRequestsByType setObject:typeList forKey:[NSNumber numberWithInt:hypObjectType]];
    }

    // if the object request is already queued dont add it again.
    if (![typeList containsObject:reference]){
        [typeList addObject:[reference copy]];
    }
}

- (void)RequestHypObject:(NSString *)reference ForType:(int)hypObjectType{
    if (!reference || [reference isEqualToString: @""]){
        // should not really ever get here, but if it does dont process the request
        NSLog(@"Dont queue an object with a opaque reference that is nil or empty");
        return;
    }
    [self queueRequest:reference ForType:hypObjectType];
    [self LogInformationForType:hypObjectType andReference:reference withMessage:@"Queued Request Object"];
    [self ProcessRequestQueue];
}

-(void)ProcessRequestQueueForType:(int)type withMethod:(NSString*)method{

    // if already processing a request do nothing
    if (pendingUpdates != 0){
        NSLog(@"waiting --> Can't processing Request for type %d - request in progress",type);
        return;
    }

    [self setObjectTypeRequestPending:type];
    NSMutableArray* queuedReferences = [outstandingSingleObjectRequestsByType objectForKey:[NSNumber numberWithInt:type]];
    if ([queuedReferences count] >0){
        NSLog(@"processing Request for type %d",type);
        NSString* reference = [queuedReferences objectAtIndex:0];
        NSArray *parameters = [NSArray arrayWithObjects:sessionID, reference, nil];
        [self sendRequestWithMethod:method andParamters:parameters runAsync:NO];
    }
}
/*
 * Request data for the required hypervisor object types from the XenServer
 * Can request single type in one go as the reference can only be for a single type
 */
- (void)ProcessRequestQueue{

    if ([outstandingSingleObjectRequestsByType count] == 0)
    {
        // nothing queued
        return;
    }

    // only process the first queued request.
    // XenServer is not very good at processing requests in parallel
    // the requests time out and get lost
    NSArray* keys = [[outstandingSingleObjectRequestsByType allKeys] copy];
    int hypObjectType = [[keys objectAtIndex:0] intValue];
    switch (hypObjectType) {
        case HYPOBJ_VM:
            [self ProcessRequestQueueForType:HYPOBJ_VM withMethod:CMD_VM_GET_ONE];
            break;
        case HYPOBJ_TEMPLATE:
            [self ProcessRequestQueueForType:HYPOBJ_TEMPLATE withMethod:CMD_VM_GET_ONE];
            break;
        case HYPOBJ_SNAPSHOT:
            [self ProcessRequestQueueForType:HYPOBJ_SNAPSHOT withMethod:CMD_VM_GET_ONE];
            break;
        case HYPOBJ_HOST:
            [self ProcessRequestQueueForType:HYPOBJ_HOST withMethod:CMD_HOST_GET_ONE];
            break;
        case HYPOBJ_HOST_CPU:
            [self ProcessRequestQueueForType:HYPOBJ_HOST_CPU withMethod:CMD_HOST_CPUS_GET_ONE];
            break;
        case HYPOBJ_STORAGE:
            [self ProcessRequestQueueForType:HYPOBJ_STORAGE withMethod:CMD_STORAGE_GET_ONE];
            break;
        case HYPOBJ_NIC:
            [self ProcessRequestQueueForType:HYPOBJ_NIC withMethod:CMD_NIC_GET_ONE];
            break;
        case HYPOBJ_NETWORK:
            [self ProcessRequestQueueForType:HYPOBJ_NETWORK withMethod:CMD_NETWORK_GET_ONE];
            break;
        case HYPOBJ_HOST_METRICS:
            [self ProcessRequestQueueForType:HYPOBJ_HOST_METRICS withMethod:CMD_HOST_METRICS_GET_ONE];
            break;
        case HYPOBJ_PBD:
            [self ProcessRequestQueueForType:HYPOBJ_PBD withMethod:CMD_PBD_GET_ONE];
            break;
        case HYPOBJ_VM_GUEST_METRICS:
            [self ProcessRequestQueueForType:HYPOBJ_VM_GUEST_METRICS withMethod:CMD_VM_GUESTMETRICS_GET_ONE];
            break;
        case HYPOBJ_VBD:
            [self ProcessRequestQueueForType:HYPOBJ_VBD withMethod:CMD_VBD_GET_ONE];
            break;
        case HYPOBJ_VDI:
            [self ProcessRequestQueueForType:HYPOBJ_VDI withMethod:CMD_VDI_GET_ONE];
            break;
        default:
            break;
    }
    [keys release];
}

// all objects in Xen have an opaque_ref property that defines their uniqueness
-(NSString *) uniqueObjectReferencePropertyForHypObjectType:(int)objectType{
    return @"opaque_ref";
}

//
//  Determine if there are updates available for the specified object type.
//  This assumes that in 
//      AutoUpdate mode that once all objects of a specified type have been requested that
//         the automatic update mechanism will keep the list up to date.
//      Manual update mode that the lists are never up to date and can always be retreived
- (BOOL)isUpdateAvailableForType:(int)hypObjectType{
    // if in auto mode it is yes for each type until a response for all objects of that type has been recieved then after that
    // it is no (unless a lost events message is received).
    if ([self isAutoUpdateEnabled]){
        return (availableUpdates & hypObjectType);
    }
    // if in manual mode this is always yes
    else {
        return YES;
    }
}

#pragma mark - 
#pragma mark private methods

/*
 * Send a request to the connected service with the specified parameters
 * Note URL was configured during initialization, when connection was established
 */
- (void)sendRequestWithMethod:(NSString *)method runAsync:(BOOL)runAsync{
    //NSLog(@"Request sent with method: %@", method);
    XMLRPCRequest *request = [[XMLRPCRequest alloc] initWithURL: [self connectionAddress]];
    if (!manager){
        manager = [XMLRPCConnectionManager sharedManager];
        [manager retain];
    }
    [request setMethod: method];
    // NSLog(@"Request body: %@", [request body]);
    [manager spawnConnectionWithXMLRPCRequest:request delegate: self runAsync:runAsync];
    [request release];
}

/*
 * Send a request to the connected service with the specified parameter
 * Note URL was configured during initialization, when connection was established
 */
- (void)sendRequestWithMethod:(NSString *)method andParamter:(NSString *)parameter runAsync:(BOOL)runAsync{
    //NSLog(@"Request sent with method: %@", method);
    XMLRPCRequest *request = [[XMLRPCRequest alloc] initWithURL: [self connectionAddress]];
    if (!manager){
        manager = [XMLRPCConnectionManager sharedManager];
        [manager retain];
    }
    [request setMethod: method withParameter: parameter];
    //NSLog(@"Request body: %@", [request body]);
    [manager spawnConnectionWithXMLRPCRequest:request delegate: self runAsync:runAsync];
    [request release];
}

/*
 * Send a request to the connected service with the specified parameters
 * Note URL was configured during initialization, when connection was established
 */
- (void)sendRequestWithMethod:(NSString *)method andParamters:(NSArray *)parameters runAsync:(BOOL)runAsync{
    //NSLog(@"Request sent with method: %@", method);
    XMLRPCRequest *request = [[XMLRPCRequest alloc] initWithURL: [self connectionAddress]];
    if (!manager){
        manager = [XMLRPCConnectionManager sharedManager];
        [manager retain];
    }
    [request setMethod: method withParameters: parameters];
    // NSLog(@"Request body: %@", [request body]);
    [manager spawnConnectionWithXMLRPCRequest: request delegate: self runAsync:runAsync];
    [request release];
}

/*
 set the last error from the XMLRPCResponse that was received.
 */
- (void)setLastErrorFromXMLRPCResponse: (XMLRPCResponse *)response{
    [self setLastError:[NSError errorWithDomain:[response faultString] code:(NSInteger)[response faultCode] userInfo:nil]];
}    

#pragma mark -
#pragma mark ResponseProcessing
- (void) processVMResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM request");
    if (sucsess){
        NSDictionary *vms = [responseData objectForKey:@"Value"];  // dictionary with key of VM opaqueRef
        NSMutableArray *vmResults = [[NSMutableArray alloc] initWithCapacity:[vms count]];
        NSMutableArray *templateResults = [[NSMutableArray alloc] initWithCapacity:10];
        NSMutableArray *snapshotResults = [[NSMutableArray alloc] initWithCapacity:10];
        NSArray *vmContentKeys = [vms allKeys];
        for(NSString *key in vmContentKeys){
            NSDictionary *vmContent = [vms valueForKey:key];
            XenVM *vm = [[XenVM alloc] initWithConnection:self Reference:key Dictionary: vmContent];
            // dont show the control domains
            if (![vm is_control_domain]){
                if ([vm vmType] == XENVMTYPE_VM){
                    [vmResults addObject:vm];
                }
                if ([vm vmType] == XENVMTYPE_SNAPSHOT){ 
                    [snapshotResults addObject:vm];
                }
                if ([vm vmType] == XENVMTYPE_TEMPLATE){
                    [templateResults addObject:vm];
                }
                if (walkTreeMode){
                    // go though the object graph for this object and check that all required data is loaded
                    [self PopulateHypObject:vm];
                }
                if (hypervisorConnectionDelegates){
                    // work on a copy of the delegates so that if anyone removes themselves whilst being used
                    // the array is not mutated.
                    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
                    for (id delegate in copyOfDelegates){
                        if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withReference:withResult:)]){
                            
                            [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:[vm vmType] withReference:[vm opaque_ref] withResult:sucsess];
                        }
                    }
                    [copyOfDelegates release];
                }
            }
            [vm release];
        }
        [self updateCachedHypObjects:[NSArray arrayWithArray:vmResults] forObjectType:HYPOBJ_VM isCompleteSet:YES];
        [self updateCachedHypObjects:[NSArray arrayWithArray:templateResults] forObjectType:HYPOBJ_TEMPLATE isCompleteSet:YES];
        [self updateCachedHypObjects:[NSArray arrayWithArray:snapshotResults] forObjectType:HYPOBJ_SNAPSHOT isCompleteSet:YES];
        [vmResults release]; 
        [templateResults release]; 
        [snapshotResults release];
    }
    else {
        // assume last known list of VMs is still good
        [self setLastError:[NSError errorWithDomain:@"XenServer VM request failed" code:0 userInfo:nil]];
    }

    if (hypervisorConnectionDelegates){
        // work on a copy of the delegates so that if anyone removes themselves whilst being used
        // the array is not mutated.
        NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
        for (id delegate in copyOfDelegates){
            if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withResult:)]){
                [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_VM| HYPOBJ_TEMPLATE withResult:sucsess];
            }
        }
        [copyOfDelegates release];
    }
    
    // clear the object type from the available updates flag, eventing will keep it upto date
    // after this point.
    availableUpdates = availableUpdates & (0xffffffff ^ HYPOBJ_VM);  // clear the available bit
    availableUpdates = availableUpdates & (0xffffffff ^ HYPOBJ_TEMPLATE);  // clear the available bit
    availableUpdates = availableUpdates & (0xffffffff ^ HYPOBJ_SNAPSHOT);  // clear the available bit
}

// process get single record style requests that have only one object in response,
// these are disctionaries of values where the key is the object type all for one object
// these dont contain the opaque ref so this is passed in from the request
- (void) processObjectRef:(NSString *) requestedOpaqueRef Type:(int)objectType usingResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    
    NSDictionary *objectContent = [responseData objectForKey:@"Value"];     
    XenBase *newObject = [self buildObjectType:objectType Reference:requestedOpaqueRef FromDictionary:objectContent];
    if ([newObject isKindOfClass:[XenVM class]]){
        if ([(XenVM *)newObject vmType] == XENVMTYPE_VM){
            objectType = HYPOBJ_VM;
        }else if ([(XenVM *)newObject vmType] == XENVMTYPE_SNAPSHOT){ 
            objectType = HYPOBJ_SNAPSHOT;
        }else if ([(XenVM *)newObject vmType] == XENVMTYPE_TEMPLATE){
            objectType = HYPOBJ_TEMPLATE;
        }
    }
    [self updateCachedHypObjects:[NSArray arrayWithObjects:newObject,nil] forObjectType:objectType isCompleteSet:NO];
    [self LogInformationForType:objectType andReference:[newObject opaque_ref] withMessage:@"Received Object"];

    if (hypervisorConnectionDelegates){
        // work on a copy of the delegates so that if anyone removes themselves whilst being used
        // the array is not mutated.
        NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
        for (id delegate in copyOfDelegates){
            if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withReference:withResult:)]){
                if (delegate){
                    [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:objectType withReference:[newObject opaque_ref] withResult:sucsess];
                }
            }
            if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withResult:)]){
                if (delegate){
                    [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:objectType withResult:sucsess];
                }
            }
        }
        [copyOfDelegates release];
    }
}

- (XenBase *) buildObjectType:(int)objectType Reference:reference FromDictionary:objectContent{
    XenBase *newObject = nil;
    switch (objectType) {
        case HYPOBJ_HOST_CPU:
            newObject = [[[NSClassFromString(@"XenHostCPU") alloc] initWithConnection:self Reference:reference Dictionary:objectContent] autorelease];
            break;
        case HYPOBJ_HOST:
            newObject = [[[NSClassFromString(@"XenHost") alloc] initWithConnection:self Reference:reference Dictionary:objectContent] autorelease];
            break;
        case HYPOBJ_VM:
            newObject = [[[NSClassFromString(@"XenVM") alloc] initWithConnection:self Reference:reference Dictionary:objectContent] autorelease];
            break;
        case HYPOBJ_TEMPLATE:
            newObject = [[[NSClassFromString(@"XenVM") alloc] initWithConnection:self Reference:reference Dictionary:objectContent] autorelease];
            break;
        case HYPOBJ_SNAPSHOT:
            newObject = [[[NSClassFromString(@"XenVM") alloc] initWithConnection:self Reference:reference Dictionary:objectContent] autorelease];
            break;
        case HYPOBJ_STORAGE:
            newObject = [[[NSClassFromString(@"XenStorage") alloc] initWithConnection:self Reference:reference Dictionary:objectContent]  autorelease];
            break;
        case HYPOBJ_NIC:
            newObject = [[[NSClassFromString(@"XenNIC") alloc] initWithConnection:self Reference:reference Dictionary:objectContent] autorelease];
            break;
        case HYPOBJ_NETWORK:
            newObject = [[[NSClassFromString(@"XenNetwork") alloc] initWithConnection:self Reference:reference Dictionary:objectContent] autorelease];
            break;
        case HYPOBJ_HOST_METRICS:
            newObject = [[[NSClassFromString(@"XenHost_metrics") alloc] initWithConnection:self Reference:reference Dictionary:objectContent] autorelease];
            break;
        case HYPOBJ_PBD:
            newObject = [[[NSClassFromString(@"XenPBD") alloc] initWithConnection:self Reference:reference Dictionary:objectContent] autorelease];
            break;
        case HYPOBJ_VM_GUEST_METRICS:
            newObject = [[[NSClassFromString(@"XenVM_guestMetrics") alloc] initWithConnection:self Reference:reference Dictionary:objectContent] autorelease];
            break;
        case HYPOBJ_VBD:
            newObject = [[[NSClassFromString(@"XenVBD") alloc] initWithConnection:self Reference:reference Dictionary:objectContent] autorelease];
            break;
        case HYPOBJ_VDI:
            newObject = [[[NSClassFromString(@"XenVDI") alloc] initWithConnection:self Reference:reference Dictionary:objectContent] autorelease];
            break;
        default:
            break;
    }
    if (walkTreeMode){
        // go though the object graph for this object and check that all required data is loaded
        [self PopulateHypObject:newObject];
    }
    return newObject;
}

- (int) convertXenClassToObjectType:(NSString *)xenClass{
    NSNumber *resultObj = [eventClasses objectForKey:xenClass];
    if (resultObj){
       return [ resultObj intValue];
    }
    NSLog(@"Failed converting XenClass %@ to object type. XenClass type unrecognized",xenClass);
    return -1;
}

//process event response
- (void) processEventDataUsingResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //inside value if sucsess
    //class = vm;
    //id = 265229;
    //operation = mod;
    //ref = "OpaqueRef:f90a07cb-1a17-3697-4281-67bcca6c4425";
    //snapshot = dictionary as normal object{
    //timestamp = "1301507667.";
    if (sucsess){
        NSArray *objects = [responseData objectForKey:@"Value"];  // array of objects
        for( NSDictionary *objectDetails in objects){
            NSDictionary *objectContent = [objectDetails valueForKey:@"snapshot"];
            int objectType = [self convertXenClassToObjectType:[objectDetails valueForKey:@"class"]];
            NSString *objectRef = [objectDetails valueForKey:@"ref"];
            NSString *operationType = [objectDetails valueForKey:@"operation"];
            if (objectType != -1){
                if ([operationType isEqualToString:@"del"]){
                    // remove object from cache
                    [self removeCachedHypObjectswithReference:objectRef forObjectType:objectType];
                }
                else{
                    XenBase *newObject = [self buildObjectType:objectType Reference:objectRef FromDictionary:objectContent];
                    if (newObject != nil){
                        if ([newObject isKindOfClass:[XenVM class]]){
                            if (![(XenVM *)newObject is_control_domain]){
                                if ([(XenVM *)newObject vmType] == XENVMTYPE_VM){  // ignore shaphosts here
                                    objectType = HYPOBJ_VM;
                                }
                                if ([(XenVM *)newObject vmType] == XENVMTYPE_SNAPSHOT){ 
                                    objectType = HYPOBJ_SNAPSHOT;
                                }
                                if ([(XenVM *)newObject vmType] == XENVMTYPE_TEMPLATE){
                                    objectType = HYPOBJ_TEMPLATE;
                                }
                            }                    
                        }
                    }
                    // because this was not a requested update do not reset the pending state
                    [self updateCachedHypObjects:[NSArray arrayWithObjects:newObject,nil] forObjectType:objectType ResetPendingUpdates:NO  isCompleteSet:NO];
                }
                
                if (hypervisorConnectionDelegates){
                    // work on a copy of the delegates so that if anyone removes themselves whilst being used
                    // the array is not mutated.
                    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
                    for (id delegate in copyOfDelegates){
                        if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withReference:withResult:)]){
                            [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:objectType withReference:objectRef withResult:sucsess];
                        }
                        if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withResult:)]){
                            [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:objectType withResult:sucsess];
                        }
                    }
                    [copyOfDelegates release];
                }
            }
        }
    }
}

// process get all style requests that have muliple objects in response,
// these are disctionaries of objects where the key is the opage ref
- (void) processType:(int)objectType usingResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    if (sucsess){
        [self LogInformationForType:objectType withMessage:@"Received data"];
        
        NSDictionary *objects = [responseData objectForKey:@"Value"];  // dictionary with key of host opaqueRef
        NSMutableArray *objectsResults = [[NSMutableArray alloc] initWithCapacity:[objects count]];
        NSArray *objectsContentKeys = [objects allKeys];
        for(NSString *key in objectsContentKeys){
            NSDictionary *objectContent = [objects valueForKey:key];
            XenBase *newObject = [self buildObjectType:objectType Reference:key FromDictionary:objectContent];
            if (newObject != nil){
                // special case for Network objects
                if ([newObject isKindOfClass:[XenNetwork class]]){
                    XenNetwork *network = (XenNetwork *)newObject;
                    if ([network isGuestInstallerNetwork])
                    {
                        continue;
                    }
                }
                
                [objectsResults addObject:newObject];
                if (hypervisorConnectionDelegates){
                    // work on a copy of the delegates so that if anyone removes themselves whilst being used
                    // the array is not mutated.
                    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
                    for (id delegate in copyOfDelegates){
                        if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withReference:withResult:)]){
                            [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:objectType withReference:[newObject opaque_ref] withResult:sucsess];
                        }
                    }
                    [copyOfDelegates release];
                }
            }
        }
        
        [self updateCachedHypObjects:[NSArray arrayWithArray:objectsResults] forObjectType:objectType isCompleteSet:YES];
        [objectsResults release]; 
    }
    else {
        // assume last known list of Host Metrics is still good
        [self setLastError:[NSError errorWithDomain:@"XenServer request failed" code:0 userInfo:nil]];
    }
    
    if (hypervisorConnectionDelegates){
        // work on a copy of the delegates so that if anyone removes themselves whilst being used
        // the array is not mutated.
        NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
        for (id delegate in copyOfDelegates){
            if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withResult:)]){
                [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:objectType withResult:sucsess];
            }
        }
        [copyOfDelegates release];
    }
    
    // clear the object type from the available updates flag, eventing will keep it upto date
    // after this point.
    availableUpdates = availableUpdates & (0xffffffff ^ objectType);  // clear the available bit
}

// TODO need process single template ans saphot too.
- (void) processSingleVMObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_VM usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processHostMetricsResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to HOST METRICS request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_HOST_METRICS usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleHostMetricsObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to HOST METRICS request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_HOST_METRICS usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processHostCPUResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to HOST CPU request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_HOST_CPU usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleHostCPUObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to HOST CPU request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_HOST_CPU usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processHostResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to HOST request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_HOST usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleHostObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to HOST request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_HOST usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processNICResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to NIC request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_NIC usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleNICObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to NIC request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_NIC usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processNetworkResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to Network request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_NETWORK usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleNetworkObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to Network request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_NETWORK usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processStorageResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to Storage request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_STORAGE usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleStorageObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to Storage request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_STORAGE usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processPBDResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to PBD request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_PBD usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSinglePBDObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to PBD request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_PBD usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processVMGuestMetricsResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM Guest Metrics request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_VM_GUEST_METRICS usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleVMGuestMetricsObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM Guest Metrics request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_VM_GUEST_METRICS usingResponse:responseData withSucsess:sucsess];
    }
}


- (void) processVBDResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VBD request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_VBD usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleVBDObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM Guest Metrics request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_VBD usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processVDIResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VDI request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_VDI usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleVDIObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM Guest Metrics request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_VDI usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processLoginResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Got response to Login request");
    if (sucsess){
        [self setSessionID:[responseData valueForKey:@"Value"]];
        // set up the event registration
        if (autoUpdateEnabled){
            NSArray *registerClasses = [eventClasses allKeys];
            [self sendRequestWithMethod:CMD_EVENT_REGISTER andParamters:[NSArray arrayWithObjects:sessionID,[NSArray arrayWithArray:registerClasses],nil] runAsync:NO];
        }
    }
    else {
        [self setSessionID:nil];
        [self setLastError:[NSError errorWithDomain:@"XenServer connection failed - possibly wrong username or password" code:0 userInfo:nil]];
    }
    
    // work on a copy of the delegates so that if anyone removes themselves whilst being used
    // the array is not mutated.
    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
    for (NSObject<HypervisorConnectionDelegate> *connectionDelegateToCall in copyOfDelegates){
        if ([connectionDelegateToCall respondsToSelector:@selector(hypervisorConnectedToConnection:withResult:)])
        {
            [connectionDelegateToCall hypervisorConnectedToConnection:self withResult:sucsess];
        }
    }
    [copyOfDelegates release];
    
    resuming = NO;
    suspended = NO;
    suspending = NO;
}

- (void) processEventRegisterAllResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    // if sucsess set the process event loop flag and then do get events request
    runEventLoop = sucsess;
    if (sucsess){
        // call the event.next method here
        runEventLoop = YES;
        [self sendRequestWithMethod:CMD_EVENT_NEXT andParamter:sessionID runAsync:NO];
    }
}

- (void) processEventNextResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    if (sucsess){
        [self processEventDataUsingResponse:responseData withSucsess:sucsess];
        if (autoUpdateEnabled && runEventLoop){
            // call the event.next method here
            [self sendRequestWithMethod:CMD_EVENT_NEXT andParamter:sessionID runAsync:NO];
        }
    }
    else
    {
        NSLog(@"Hypervisor event data lost.");
        if (autoUpdateEnabled && runEventLoop){
            // missing events detected? assume that this the only error that can happen here for now
            if (hypervisorConnectionDelegates){

                // work on a copy of the delegates so that if anyone removes themselves whilst being used
                // the array is not mutated.
                NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
               for (id delegate in copyOfDelegates){
                    if ([delegate respondsToSelector:@selector(hypervisorEventDataLost) ]){
                        //[delegate hypervisorEventDataLost];
                    }
                }
                [copyOfDelegates release];
            }
            // carry on by calling the event.next method here, it is up to the app to
            // respond to the event to reset the cache and get back in sync
            // as who knows which objects have changed and what the UI is currently relyinh on being
            // in the cache.
            [self sendRequestWithMethod:CMD_EVENT_NEXT andParamter:sessionID runAsync:NO];
        }
    }
}

- (void) processEventUnregisterAllResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    // clear the event loop flag
    // regardless of the sucsess of the event un register
    runEventLoop = NO;
}

+ (BOOL) isAutoUpdateConfigured{
    NSNumber *eventingOn = [[NSUserDefaults standardUserDefaults] valueForKey:@"XenAutoUpdate"];
    if (eventingOn == nil)
    {
        return YES;
    }
    if ([eventingOn intValue] == 1){
        return YES;
    }
    return NO;
}

- (NSSortDescriptor*) sortForhypObjectType:(int)hypObjectType{
    return [XenBase defaultSortForhypObjectType:hypObjectType];
}

@end
