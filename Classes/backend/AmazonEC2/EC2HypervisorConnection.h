//
//  EC2HypervisorConnection.h
//  hypOps
//
//  Created by Ian Firth on 29/06/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HypervisorConnectionFactory.h"
#import <AWSiOSSDK/EC2/AmazonEC2Client.h>

#define HYPOPJ_EC2INSTANCES 1
#define HYPOPJ_EC2IMAGES_MACHINE 2
#define HYPOPJ_EC2IMAGES_KERNEL 4
#define HYPOPJ_EC2IMAGES_RAMDISK 8
#define HYPOPJ_EC2VOLUMES 16
#define HYPOBJ_EC2SECURITYGROUPS 32
#define HYPOPJ_EC2REGIONS 64
#define HYPOBJ_EC2_LASTOBJECTTYPE 64

@interface EC2HypervisorConnection : HypervisorConnectionFactory {
    AmazonEC2Client *ec2Client;
    /**
     The pool view controller for the root pool display
     */
    UIViewController *ec2ConnectionViewController;
    
    ConnectionState connectionState;
    
    /**
     Stores the userName for the current connection
     */
    NSString *connectionUserName;
}

// for the HypervisorConnectionFactory abstract methods
- (void)ConnectToAddress:(NSString *)address withUsername:(NSString *)username andPassword:(NSString *)password;
- (ConnectionState)GetConnectionState;
- (UIViewController *)GetViewController;
- (void)releaseMemory;
- (void)closeConnection;
- (void)reloadCoreData;
- (void)RequestHypObjectsForType:(int)hypObjectType;
/**
 Request all data objects for a specific type from the Hypervisor connection.  This is an async request.
 @param hypObjectType The type of objects to requrest.
 */
- (void)RequestHypObjectsForType:(int)hypObjectType;

/**
 Request specific data object for a specific type from the Hypervisor connection.  This is an async request.
 @param reference The reference for the object to requrest.
 @param hypObjectType The type of objects to requrest.
 */
- (void)RequestHypObject:(NSString *)reference ForType:(int)hypObjectType;

// methods for just this connection type

/**
 Sets the client region endpoint.  This will essestially resrict the connection
 to data within this specifiec region.  Setting nil will open it up to all regions (I think)
 */
-(void) setEC2ClientEndpoint:(NSString *)endpoint;

@property (readonly) AmazonEC2Client *ec2Client;
@property (readonly) NSString *connectionUserName;

@end

@interface RequestTypes: NSObject{
    EC2HypervisorConnection *hypervisorConnection;
    int objectTypes;
}

-(id) initWithHypervisorConnection:(EC2HypervisorConnection *)connection types:(int)objectTypes;

@property (readonly) EC2HypervisorConnection *hypervisorConnection;
@property (readonly)int objectTypes;

@end

@interface RequestObject: NSObject{
    EC2HypervisorConnection *hypervisorConnection;
    int objectType;
    NSString *reference;
}

-(id) initWithHypervisorConnection:(EC2HypervisorConnection *)connection type:(int)theObjectType andReference:(NSString *)theReference;

@property (readonly) EC2HypervisorConnection *hypervisorConnection;
@property (readonly) int objectType;
@property (readonly) NSString* reference;

@end

@interface ResponseTypes: NSObject{
    EC2HypervisorConnection *hypervisorConnection;
    int objectTypes;
    BOOL sucsess;
    NSArray *objects;
    BOOL isCompleteSet;
}


- (id) initWithHypervisorConnection:(EC2HypervisorConnection *)connection types:(int)types result:(BOOL)result resultObjectsOrNil:(NSArray *)results  isCompleteSet:(BOOL)completeSet;

@property (readonly) EC2HypervisorConnection *hypervisorConnection;
@property (readonly)int objectTypes;
@property (readonly)BOOL sucsess;
@property (readonly)NSArray *objects;
@property (readonly)BOOL isCompleteSet;

@end