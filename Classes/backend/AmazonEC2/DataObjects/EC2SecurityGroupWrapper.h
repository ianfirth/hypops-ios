//
//  EC2SecurityGroupWrapper.h
//  hypOps
//
//  Created by Ian Firth on 24/07/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AWSiOSSDK/EC2/EC2SecurityGroup.h>
#import "HypObjReferencesProtocol.h"

@interface EC2SecurityGroupWrapper : NSObject<HypObjReferencesProtocol>  {
    EC2SecurityGroup *wrappedSecurityGroup; 
}

// default constructor
-(id) initWithEC2SecurityGroup:(EC2SecurityGroup *)securityGroup;

+ (NSPredicate *) groupNameBeginsOrContainsAWordBeginningWith:(NSString *)name;
+ (NSPredicate *) groupWithReference:(NSString *)groupId;

// these are the properties that the wrapped object exposes
@property (readonly) NSString *groupName;
@property (readonly) NSString *groupId;
@property (readonly) NSString *ownerId;
@property (readonly) NSString *descriptionValue;
@property (readonly) NSMutableArray * ipPermissions;
@property (readonly) NSMutableArray * tags;
@property (readonly) int tags_count;

// comparators for seaching
- (NSComparisonResult)compareByName:(EC2SecurityGroupWrapper *)otherObject;


@end
