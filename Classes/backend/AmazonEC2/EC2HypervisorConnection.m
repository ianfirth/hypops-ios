//
//  EC2HypervisorConnection.m
//  hypOps
//
//  Created by Ian Firth on 29/06/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "EC2HypervisorConnection.h"
#import <AWSiOSSDK/EC2/EC2DescribeImagesRequest.h>
#import "EC2RegionsViewController.h"
#import "HypObjReferencesProtocol.h"
#import "EC2ImageWrapper.h"
#import "EC2InstanceWrapper.h"
#import "EC2RegionWrapper.h"
#import "EC2SecurityGroupWrapper.h"

@implementation RequestTypes

@synthesize hypervisorConnection,objectTypes;

-(id) initWithHypervisorConnection:(EC2HypervisorConnection *)connection types:(int)types{
    self = [super init];
    if (self){
    hypervisorConnection = connection;
    [hypervisorConnection retain];
    objectTypes = types;
    }
    return self;
}

-(void)dealloc{
    if (hypervisorConnection){
        [hypervisorConnection release];
        hypervisorConnection = nil;
    }
    [super dealloc];
}
@end

@implementation RequestObject

@synthesize hypervisorConnection,objectType,reference;

-(id) initWithHypervisorConnection:(EC2HypervisorConnection *)connection type:(int)theObjectType andReference:(NSString *)theReference{
    self = [super init];
    if (self){
        reference = [theReference copy];
        hypervisorConnection  = [connection retain];
        objectType = theObjectType;
    }
    return self;
}

-(void)dealloc{
    if (hypervisorConnection){
        [hypervisorConnection release];
        hypervisorConnection = nil;
    }
    [reference release];
    reference = nil;
    [super dealloc];
}

@end

@implementation ResponseTypes

@synthesize hypervisorConnection,objectTypes,sucsess,objects, isCompleteSet;

-(id) initWithHypervisorConnection:(EC2HypervisorConnection *)connection types:(int)types result:(BOOL)result resultObjectsOrNil:(NSArray *)results  isCompleteSet:(BOOL)completeSet{
    self = [super init];
    if (self){
        hypervisorConnection = connection;
        [hypervisorConnection retain];
        objectTypes = types;
        sucsess = result;
        objects = [results retain];
        isCompleteSet = completeSet;
    }
    return self;
}

-(void)dealloc{
    if (hypervisorConnection){
        [hypervisorConnection release];
        hypervisorConnection = nil;
    }
    if (objects){
        [objects release];
        objects = nil;
    }
    [super dealloc];
}
@end

@interface EC2HypervisorConnection ()
    -(void)RequestHypObjects:(RequestTypes *)request;
    -(void)RequestHypSingleObject:(RequestObject *)request;
@end

@implementation EC2HypervisorConnection

@synthesize ec2Client, connectionUserName;


// constructor for the class
-(id) init{
    self = [super init];
    if (self){
        hypervisorConnectionType = HYPERVISOR_EC2;
        connectionState = CONNECTION_UNCONNECTED;
    }
    return self;
}

-(void) setEC2ClientEndpoint:(NSString *)endpoint{
    // this is what needs to be set to control the regions
    // might be better to use NSURL here, but could not work out how to create one without a path
    NSString *finalUrl = [NSString stringWithFormat:@"https://%@",endpoint];
    [ec2Client setEndpoint:finalUrl];              
}

-(void) dealloc{
    [ec2Client release];
    ec2Client = nil;
    [connectionUserName release];
    connectionUserName = nil;
    [super dealloc];    
}

#pragma mark HypervisorConnectionFactory implementation
// EC2 has currently no autoupdate mode.  This could be implemented as a continuous
// background timer if required but no need for now at all.
- (BOOL) isAutoUpdateConfigured{
    return NO;
}

- (void)ConnectToAddress:(NSString *)address withUsername:(NSString *)username andPassword:(NSString *)password{
    NSLog(@"ConnectToAddress %@ ACCESS Key is %@",address,username);
    if ([self GetConnectionState] != CONNECTION_CONNECTED && [self GetConnectionState] != CONNECTION_CONNECTING){

        // clean up any previous client
        if (!ec2Client){
            [ec2Client release];
            ec2Client = nil;
        }
        ec2Client = [[AmazonEC2Client alloc] initWithAccessKey:username withSecretKey:password];
        [ec2Client retain];
        connectionUserName = [username copy];
        connectionState = CONNECTION_CONNECTING;
        [NSThread detachNewThreadSelector:@selector(connectTo:) 
                                 toTarget:self 
                               withObject:self];

    }
    else {
        // dont connect again just imply the connect
        NSLog(@"Already connected");  
    }
}


- (ConnectionState)GetConnectionState{
    return connectionState;
}

- (void)suspendConnection{
   // nothing to do for Amazon EC2.  Each connection can just be left as there
   // is no automatic refresh mode at present.
}

- (UIViewController *)GetViewController{
    if (!ec2ConnectionViewController){
        ec2ConnectionViewController  = [[EC2RegionsViewController alloc] initWithHypervisorConnection:self];
    }
    return ec2ConnectionViewController;
}

- (void)releaseMemory{
    
}

// close the connection
- (void)closeConnection{
    NSNumber *hypervisorId = [[NSNumber alloc] initWithInt:HYPERVISOR_EC2];
    NSMutableDictionary *ec2Connections = [[HypervisorConnectionFactory connectionDictionary] objectForKey:hypervisorId];
    if (ec2Connections != nil){
        NSString *key = nil;
        for (NSString* connectionKey in [ec2Connections allKeys]){
            EC2HypervisorConnection *connection = [ec2Connections objectForKey:connectionKey];
            if (connection == self){
                key = connectionKey;
            }
        }
        if (key){
            NSLog(@"Closing connection %@", key);
            // send an unregister and logout
            [ec2Connections removeObjectForKey:key];
        }
    }
    
    [hypervisorId release];
}


- (void)reloadCoreData;{
    int requestObjects = 0;
    if ([self isUpdateAvailableForType:HYPOPJ_EC2INSTANCES]){
        requestObjects = HYPOPJ_EC2INSTANCES;
    }
    if ([self isUpdateAvailableForType:HYPOPJ_EC2IMAGES_MACHINE]){
        requestObjects = requestObjects | HYPOPJ_EC2IMAGES_MACHINE;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_EC2SECURITYGROUPS]){
        requestObjects = requestObjects | HYPOBJ_EC2SECURITYGROUPS;
    }
    if (requestObjects != 0){
        RequestTypes *request = [[[RequestTypes alloc] initWithHypervisorConnection:self types:requestObjects] autorelease];
        [self RequestHypObjects:request];
    }
}

/**
 Request specific data object for a specific type from the Hypervisor connection.  This is an async request.
 @param reference The reference for the object to requrest.
 @param hypObjectType The type of objects to requrest.
 */
- (void)RequestHypObject:(NSString *)reference ForType:(int)hypObjectType{
   // create a request using a filter for the specific object
    RequestObject *request = [[[RequestObject alloc] initWithHypervisorConnection:self type:hypObjectType andReference:reference] autorelease];
    [self RequestHypSingleObject:request];
}

// provide the appropriate property that defines uniqueness for the specified type
-(NSString *) uniqueObjectReferencePropertyForHypObjectType:(int)objectType{
    switch (objectType){
        case HYPOPJ_EC2IMAGES_MACHINE:
        case HYPOPJ_EC2IMAGES_KERNEL:
        case HYPOPJ_EC2IMAGES_RAMDISK:
           return @"imageId";
        case HYPOPJ_EC2INSTANCES:
            return @"instanceId";
        case HYPOPJ_EC2VOLUMES:
            return @"volumeId";
        case HYPOBJ_EC2SECURITYGROUPS:
            return @"groupId";
        case HYPOPJ_EC2REGIONS:
            return @"regionName";
    }

    NSLog(@"No unique property defined for object Amazon object type");
    return @"";
}

#pragma mark -
#pragma mark helperMethods

/**
 * Method to fire the connection events
 */
- (void)fireConnectedDelegatesWithResult:(NSNumber *)sucsess{
    // work on a copy of the delegates so that if anyone removes themselves whilst being used
    // the array is not mutated.
    BOOL successBool = [sucsess boolValue];
    if (!successBool){
        [self setLastError:[NSError errorWithDomain:@"Amazon EC2 connection failed - possibly wrong username or password" code:0 userInfo:nil]];
        connectionState = CONNECTION_UNCONNECTED;
    }
    else{
        connectionState = CONNECTION_CONNECTED;
    }
    
    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
    for (NSObject<HypervisorConnectionDelegate> *connectionDelegateToCall in copyOfDelegates){
        if ([connectionDelegateToCall respondsToSelector:@selector(hypervisorConnectedToConnection:withResult:)])
        {
           [connectionDelegateToCall hypervisorConnectedToConnection:self withResult:successBool];
        }
    }
    [copyOfDelegates release];
}

/**
* Method to fire the updated events
*/
- (void)fireUpdatedDelegatesWithResult:(ResponseTypes *)response{
    // work on a copy of the delegates so that if anyone removes themselves whilst being used
    // the array is not mutated.    
    if ([response sucsess]){
        [self updateCachedHypObjects:[NSArray arrayWithArray:[response objects]] forObjectType:[response objectTypes] isCompleteSet:[response isCompleteSet]];
    }
    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
    for (NSObject<HypervisorConnectionDelegate> *connectionDelegateToCall in copyOfDelegates){
        if ([connectionDelegateToCall respondsToSelector:@selector(hypervisorObjectsUpdated:updatesObjectsOfType:withResult:)])
        {
            [connectionDelegateToCall hypervisorObjectsUpdated:[response hypervisorConnection] updatesObjectsOfType:[response objectTypes] withResult:[response sucsess]];
        }
    }
    [copyOfDelegates release];
}

- (void)RequestHypObjectsForType:(int)hypObjectType{
    RequestTypes *request = [[[RequestTypes alloc] initWithHypervisorConnection:self types:hypObjectType] autorelease];
    [self RequestHypObjects:request];
}

-(void)RequestHypSingleObject:(RequestObject *)request{
    [NSThread detachNewThreadSelector:@selector(requestDataSingleObject:) 
                             toTarget:self 
                           withObject:request];
}

-(void)RequestHypObjects:(RequestTypes *)request{
    // go though the types and make separate requests here
    int i = 1;
    while (i < (HYPOBJ_EC2_LASTOBJECTTYPE << 1)){
        if ((request.objectTypes & i) > 0){
            RequestTypes *localRequest = [[[RequestTypes alloc] initWithHypervisorConnection:[request hypervisorConnection] types:i] autorelease];
            [NSThread detachNewThreadSelector:@selector(requestData:) 
                                     toTarget:self 
                                   withObject:localRequest];
        }
        i = i << 1;
    }
}

#pragma mark methods that can be called on a background thread to perform syncronous requrests to the cloud
- (void)connectTo:(EC2HypervisorConnection *)connection{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

    @try {
        
        // do something to check that can connect here
        // get regions?
        EC2DescribeRegionsRequest *request = [[EC2DescribeRegionsRequest alloc] init];
        EC2DescribeRegionsResponse *response = [[connection ec2Client] describeRegions:request];

        NSMutableArray *resultArray = [[NSMutableArray alloc] init];
        for (EC2Region *region in [response regions]) {
            [resultArray addObject:[[EC2RegionWrapper alloc] initWithEC2Region:region]];
        }

        [self updateCachedHypObjects:resultArray forObjectType:HYPOPJ_EC2REGIONS isCompleteSet:YES];

        // when get result call the connected delegate on the main thread again
        [self performSelectorOnMainThread:@selector(fireConnectedDelegatesWithResult:) 
                                   withObject:[NSNumber numberWithBool:YES] 
                                waitUntilDone:false];
    }
    @catch (AmazonServiceException *exception) {
        NSLog(@"Exception = %@", exception);
        [self performSelectorOnMainThread:@selector(fireConnectedDelegatesWithResult:) 
                               withObject:[NSNumber numberWithBool:NO] 
                            waitUntilDone:false];

    }
	[pool release];
}

- (void)requestDataSingleObject:(RequestObject *)request{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    //NSLog(@"Reqesting object %@", [request reference]);

    // make sure that there are no outstanding requests for this type already
    while ([self isUpdatePendingForType:[request objectType]]){
        [NSThread sleepForTimeInterval:0.1];
    }
    
    [self setObjectTypeRequestPending:[request objectType]];

    @try {
        NSMutableArray *resultArray = nil;
        switch ([request objectType]) {
            case HYPOPJ_EC2INSTANCES:{
                EC2DescribeInstancesRequest *instancesRequest = [[EC2DescribeInstancesRequest alloc] init];
                EC2Filter *filter = [[EC2Filter alloc] initWithName:@"instance-id" andValues:[NSArray arrayWithObject:[request reference]]];
                [instancesRequest addFilter:filter];
                [filter release];
                EC2DescribeInstancesResponse *instancesResponse = [[[request hypervisorConnection] ec2Client] describeInstances:instancesRequest];
                resultArray = [[NSMutableArray alloc] init];
                for(EC2Reservation *reservation in [instancesResponse reservations]){
                    for (EC2Instance *instance in [reservation instances]){
                        [resultArray addObject:[[EC2InstanceWrapper alloc] initWithEC2Instance:instance]];
                    }
                }
                [instancesRequest release];
                break;
            }
            case HYPOPJ_EC2VOLUMES:{
                EC2DescribeVolumesRequest *volumesRequest = [[EC2DescribeVolumesRequest alloc] init];
                EC2Filter *filter = [[EC2Filter alloc] initWithName:@"volume-id" andValues:[NSArray arrayWithObject:[request reference]]];
                [volumesRequest addFilter:filter];
                [filter release];
                EC2DescribeVolumesResponse *volumesResponse = [[[request hypervisorConnection] ec2Client] describeVolumes:volumesRequest];
                resultArray = [volumesResponse volumes];
                [volumesRequest release];
                break;
            }
            case HYPOPJ_EC2IMAGES_MACHINE:{
                EC2DescribeImagesRequest *imagesRequest = [[EC2DescribeImagesRequest alloc] init];
                EC2Filter *filter = [[EC2Filter alloc] initWithName:@"image-id" andValues:[NSArray arrayWithObject:[request reference]]];
                [imagesRequest addFilter:filter];
                [filter release];
                filter = [[EC2Filter alloc] initWithName:@"image-type" andValues:[NSMutableArray arrayWithObject:@"machine"]];
                [[imagesRequest filters] addObject:filter];
                [filter release];
                EC2DescribeImagesResponse *imageResponse = [[[request hypervisorConnection] ec2Client] describeImages:imagesRequest];
                resultArray = [[NSMutableArray alloc] init];
                for (EC2Image *image in [imageResponse images]) {
                    [resultArray addObject:[[EC2ImageWrapper alloc] initWithEC2Image:image]];
                }
                [imagesRequest release];
                break;
            }
            case HYPOPJ_EC2IMAGES_KERNEL:{
                EC2DescribeImagesRequest *imagesRequest = [[EC2DescribeImagesRequest alloc] init];
                EC2Filter *filter = [[EC2Filter alloc] initWithName:@"image-id" andValues:[NSArray arrayWithObject:[request reference]]];
                [imagesRequest addFilter:filter];
                [filter release];
                filter = [[EC2Filter alloc] initWithName:@"image-type" andValues:[NSMutableArray arrayWithObject:@"kernel"]];
                [[imagesRequest filters] addObject:filter];
                [filter release];
                EC2DescribeImagesResponse *imageResponse = [[[request hypervisorConnection] ec2Client] describeImages:imagesRequest];
                resultArray = [[NSMutableArray alloc] init];
                for (EC2Image *image in [imageResponse images]) {
                    [resultArray addObject:[[EC2ImageWrapper alloc] initWithEC2Image:image]];
                }
                [imagesRequest release];
                break;
            }
            case HYPOPJ_EC2IMAGES_RAMDISK:{
                EC2DescribeImagesRequest *imagesRequest = [[EC2DescribeImagesRequest alloc] init];
                EC2Filter *filter = [[EC2Filter alloc] initWithName:@"image-id" andValues:[NSArray arrayWithObject:[request reference]]];
                [imagesRequest addFilter:filter];
                [filter release];
                filter = [[EC2Filter alloc] initWithName:@"image-type" andValues:[NSMutableArray arrayWithObject:@"ramdisk"]];
                [[imagesRequest filters] addObject:filter];
                [filter release];
                EC2DescribeImagesResponse *imageResponse = [[[request hypervisorConnection] ec2Client] describeImages:imagesRequest];
                resultArray = [[NSMutableArray alloc] init];
                for (EC2Image *image in [imageResponse images]) {
                    [resultArray addObject:[[EC2ImageWrapper alloc] initWithEC2Image:image]];
                }
                [imagesRequest release];
                break;
            }
            case HYPOBJ_EC2SECURITYGROUPS:{
                EC2DescribeSecurityGroupsRequest *securityGroupRequest = [[EC2DescribeSecurityGroupsRequest alloc] init];
                EC2Filter *filter = [[EC2Filter alloc] initWithName:@"group-id" andValues:[NSArray arrayWithObject:[request reference]]];
                [securityGroupRequest addFilter:filter];
                [filter release];
                EC2DescribeSecurityGroupsResponse *securtyGroupResponse = [[[request hypervisorConnection] ec2Client] describeSecurityGroups:securityGroupRequest];

                for (EC2SecurityGroup *group in [securtyGroupResponse securityGroups]) {
                    [resultArray addObject:[[EC2SecurityGroupWrapper alloc] initWithEC2SecurityGroup:group]];
                }
                [securityGroupRequest release];
                break;
            }
            default:
               break ;
        }
        
        if (resultArray){
            ResponseTypes *response = [[ResponseTypes alloc] initWithHypervisorConnection:[request hypervisorConnection] types:[request objectType] result:YES resultObjectsOrNil:resultArray isCompleteSet:NO];
            // when get result call the updated objects delegate on the main thread again
            [self performSelectorOnMainThread:@selector(fireUpdatedDelegatesWithResult:) 
                                   withObject:response 
                                waitUntilDone:false];
        }
    }
    @catch (AmazonServiceException *exception) {
        NSLog(@"Exception = %@", exception);
        // when get result call the updated objects delegate on the main thread again with error
        ResponseTypes *response = [[ResponseTypes alloc] initWithHypervisorConnection:[request hypervisorConnection] types:[request objectType] result:NO resultObjectsOrNil:nil isCompleteSet:NO];
        // when get result call the updated objects delegate on the main thread again
        [self performSelectorOnMainThread:@selector(fireUpdatedDelegatesWithResult:) 
                               withObject:response 
                            waitUntilDone:false];
        
    }

    // clear any pending updates for the type specified
    [self clearObjectTypeRequestPending:[request objectType]];  // clear the update bit 

	[pool release];
}

- (void)requestData:(RequestTypes *)request{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    // make sure that there are no outstanding requests for these types already
    while ([self isUpdatePendingForType:[request objectTypes]]){   
        [NSThread sleepForTimeInterval:0.1];
    }
    
    [self setObjectTypeRequestPending:[request objectTypes]];  // indicate that updates are pending 
    
    @try {
        NSMutableArray *resultArray = nil;
        if (([request objectTypes] & HYPOPJ_EC2INSTANCES) >0){
            EC2DescribeInstancesRequest *instancesRequest = [[EC2DescribeInstancesRequest alloc] init];
            EC2DescribeInstancesResponse *instancesResponse = [[[request hypervisorConnection] ec2Client] describeInstances:instancesRequest];
            resultArray = [[NSMutableArray alloc] init];
            for(EC2Reservation *reservation in [instancesResponse reservations]){
                for (EC2Instance *instance in [reservation instances]){
                    [resultArray addObject:[[EC2InstanceWrapper alloc] initWithEC2Instance:instance]];
                }
            }
        }else if (([request objectTypes] & HYPOPJ_EC2VOLUMES) >0){
            EC2DescribeVolumesRequest *volumesRequest = [[EC2DescribeVolumesRequest alloc] init];
            EC2DescribeVolumesResponse *volumesResponse = [[[request hypervisorConnection] ec2Client] describeVolumes:volumesRequest];
            resultArray = [volumesResponse volumes];
        }else if (([request objectTypes] & HYPOPJ_EC2IMAGES_MACHINE) >0){
            EC2DescribeImagesRequest *imagesRequest = [[EC2DescribeImagesRequest alloc] init];
            EC2Filter *filter = [[EC2Filter alloc] initWithName:@"image-type" andValues:[NSMutableArray arrayWithObject:@"machine"]];
            [[imagesRequest filters] addObject:filter];
            EC2DescribeImagesResponse *imageResponse = [[[request hypervisorConnection] ec2Client] describeImages:imagesRequest];
            resultArray = [[NSMutableArray alloc] init];
            for (EC2Image *image in [imageResponse images]) {
                [resultArray addObject:[[EC2ImageWrapper alloc] initWithEC2Image:image]];
            }
        }else if (([request objectTypes] & HYPOPJ_EC2IMAGES_KERNEL) >0){
            EC2DescribeImagesRequest *imagesRequest = [[EC2DescribeImagesRequest alloc] init];
            EC2Filter *filter = [[EC2Filter alloc] initWithName:@"image-type" andValues:[NSMutableArray arrayWithObject:@"kernel"]];
            [[imagesRequest filters] addObject:filter];
            EC2DescribeImagesResponse *imageResponse = [[[request hypervisorConnection] ec2Client] describeImages:imagesRequest];
            resultArray = [[NSMutableArray alloc] init];
            for (EC2Image *image in [imageResponse images]) {
                [resultArray addObject:[[EC2ImageWrapper alloc] initWithEC2Image:image]];
            }
        }else if (([request objectTypes] & HYPOPJ_EC2IMAGES_RAMDISK) >0){
            EC2DescribeImagesRequest *imagesRequest = [[EC2DescribeImagesRequest alloc] init];
            EC2Filter *filter = [[EC2Filter alloc] initWithName:@"image-type" andValues:[NSMutableArray arrayWithObject:@"ramdisk"]];
            [[imagesRequest filters] addObject:filter];
            EC2DescribeImagesResponse *imageResponse = [[[request hypervisorConnection] ec2Client] describeImages:imagesRequest];
            resultArray = [[NSMutableArray alloc] init];
            for (EC2Image *image in [imageResponse images]) {
                [resultArray addObject:[[EC2ImageWrapper alloc] initWithEC2Image:image]];
            }
        }else if (([request objectTypes] & HYPOBJ_EC2SECURITYGROUPS) >0){
            EC2DescribeSecurityGroupsRequest *securityGroupRequest = [[EC2DescribeSecurityGroupsRequest alloc] init];
            EC2DescribeSecurityGroupsResponse *securtyGroupResponse = [[[request hypervisorConnection] ec2Client] describeSecurityGroups:securityGroupRequest];
            resultArray = [[NSMutableArray alloc] init];
            for (EC2SecurityGroup *group in [securtyGroupResponse securityGroups]) {
                [resultArray addObject:[[EC2SecurityGroupWrapper alloc] initWithEC2SecurityGroup:group]];
            }

        }
       
        ResponseTypes *response = [[ResponseTypes alloc] initWithHypervisorConnection:[request hypervisorConnection] types:[request objectTypes] result:YES resultObjectsOrNil:resultArray  isCompleteSet:YES];
        // when get result call the updated objects delegate on the main thread again
        [self performSelectorOnMainThread:@selector(fireUpdatedDelegatesWithResult:) 
                                   withObject:response 
                                waitUntilDone:false];
    }
    @catch (AmazonServiceException *exception) {
        NSLog(@"Exception = %@", exception);
        // when get result call the updated objects delegate on the main thread again with error
        ResponseTypes *response = [[ResponseTypes alloc] initWithHypervisorConnection:[request hypervisorConnection] types:[request objectTypes] result:NO resultObjectsOrNil:nil isCompleteSet:YES];
        // when get result call the updated objects delegate on the main thread again
        [self performSelectorOnMainThread:@selector(fireUpdatedDelegatesWithResult:) 
                               withObject:response 
                            waitUntilDone:false];
        
    }
    
    // clear any pending updates for the type specified
    [self clearObjectTypeRequestPending:[request objectTypes]];

	[pool release];
}

- (NSSortDescriptor*) sortForhypObjectType:(int)hypObjectType{
    return nil;
}

@end
