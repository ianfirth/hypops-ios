//
//  EC2HypervisorConnectionDefinition.h
//  hypOps
//
//  Created by Ian Firth on 28/06/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HypervisorConnectionFactory.h"

@interface EC2HypervisorConnectionDefinition : NSObject <ConnectionDefintionDelegate> {
    UITextField *address;
    UISwitch *secure;
    UITextField *userName;
    UITextField *password;
    UITextField *name;
}

@property (retain) UITextField *address;
@property (retain) UISwitch *secure;
@property (retain) UITextField *userName;
@property (retain) UITextField *password;
@property (retain) UITextField *name;

@end
