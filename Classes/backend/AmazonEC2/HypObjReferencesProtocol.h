//
//  HypObjReferencesProtocol.h
//  hypOps
//
//  Created by Ian Firth on 13/07/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//
//  This protocol should be applied to all objects that a hyperivsor can load.
//  It is assumed that this is the case for most of the operations that the hypervisorConnections can provide that
//  are produced from the hypervisor connections factory.
#import <Foundation/Foundation.h>


@protocol HypObjReferencesProtocol <NSObject>
/**
 The Dictionary of other objects that this object references and requires to be considered complete.
 This is keyed on the hypervisor object type and contains an NSMutable array of objects.  This should not store
 all references, otherwise when one object is loaded the tree will be very large and end up pulling in
 all objects into the graph.
 */
- (NSMutableDictionary *)references;

/** 
 * The primary unique refernce value for the object
 */
- (NSString *)uniqueReference;

/**
 * the type of the object
 */
- (int) objectType;

@end
