//
//  untitled.m
//  hypOps
//
//  Created by Ian Firth on 20/02/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "HypervisorConnectionFactory.h"
#import "XenHypervisorConnection.h"
#import "XenHypervisorConnectionDefinition.h"
#import "EC2HypervisorConnection.h"
#import "EC2HypervisorConnectionDefinition.h"
#import "HypObjReferencesProtocol.h"

@implementation ConnectionDefinition
@synthesize hypervisorType,connectionString,icon, delegate;

-(id) initWithHypervisorType:(HypervisorType)type connectionName:(NSString *)connectionName icon:(UIImage *)image deleagte: (NSObject<ConnectionDefintionDelegate> *)definitionDelegate{
    self = [super init];
    if (self){
        hypervisorType = type;
        connectionString = [connectionName copy];
        icon = [image retain];
        delegate = [definitionDelegate retain];
    }
    return self;
}

- (void) dealloc{
    [icon release];
    [connectionString release];
    [delegate release];
    [super dealloc];
}

@end

@interface HypervisorConnectionFactory ()
- (void) PopulateHypObject:(id<HypObjReferencesProtocol>)object;
- (BOOL) PopulateHypObject:(id<HypObjReferencesProtocol>)object checkOnly:(BOOL)checkOnly;
@end

@implementation HypervisorConnectionFactory

@synthesize lastError;
@synthesize connectionID;
@synthesize hypervisorConnectionType;
@synthesize walkTreeMode;

static NSArray *connectionDefs;

+(NSMutableDictionary*) connectionDictionary
{
    static NSMutableDictionary* connectionDictionary = nil;
    
    if (connectionDictionary == nil)
    {
        connectionDictionary = [[NSMutableDictionary alloc] initWithCapacity:2 ];
    }
    
    return connectionDictionary;
}

+(NSArray *) connectionDefinitions{

    if (connectionDefs == nil){
        
        NSObject<ConnectionDefintionDelegate> *xenDisplDelegate = [[XenHypervisorConnectionDefinition alloc ] init ]; 

        
        ConnectionDefinition *xenServerConDef = [[ConnectionDefinition alloc] 
                                                initWithHypervisorType:HYPERVISOR_XEN 
                                                connectionName:@"XenServer" 
                                                icon:[UIImage imageNamed:@"XenServer"] 
                                                deleagte:xenDisplDelegate];

        NSObject<ConnectionDefintionDelegate> *amazonEC2DisplDelegate = [[EC2HypervisorConnectionDefinition alloc ] init ]; 

       
        ConnectionDefinition *amazonConDef = [[ConnectionDefinition alloc] 
                                                 initWithHypervisorType:HYPERVISOR_EC2 
                                                 connectionName:@"Amazon EC2" 
                                                 icon:[UIImage imageNamed:@"AmazonSmall"] 
                                                 deleagte:amazonEC2DisplDelegate];
                                                                                                                                                                                                  
                                                                              
      connectionDefs =[[NSArray alloc] initWithObjects:xenServerConDef, amazonConDef,nil];

     [xenDisplDelegate release];
     [amazonEC2DisplDelegate release];
     [xenServerConDef release];
     [amazonConDef release];
    }
   
    return connectionDefs;
}

// Returns nil if type is not supported.
- (id)initWithHypervisorType:(HypervisorType)hypervisorType usingAddress:(NSString *)address withUsername:(NSString *)username andPassword:(NSString *)password delegate:(id<HypervisorConnectionDelegate>)delegate
{
    // Since we’re an abstract object, transparently
    // return one of our concrete subclasses.
    [self release];
    
    // This is where you'd determine which concrete
    // subclass can best handle the desired behavior.
    NSString *connID = [[NSProcessInfo processInfo] globallyUniqueString];
    NSNumber *hypervisorId = [[NSNumber alloc] initWithInt:hypervisorType];
    NSMutableDictionary *connections = [[HypervisorConnectionFactory connectionDictionary] objectForKey:hypervisorId];
    [hypervisorId release];
    //NSLog(@"Looking up all existing connections");
    if (connections == nil){
        NSLog(@"Constructing list of XenServerConnections as none exist to this point");
        connections = [[NSMutableDictionary alloc] initWithCapacity:2];
        [[HypervisorConnectionFactory connectionDictionary] setObject:connections forKey:hypervisorId];
        [connections release];  // dictionary retains it
    }

    switch (hypervisorType) {
        case HYPERVISOR_XEN:{
            NSLog(@"Constructing XenServerConnection");
            XenHypervisorConnection *xenConnection = [[XenHypervisorConnection alloc] init];
            [xenConnection addHypervisorConnectionDelegate:delegate];
            [connections setValue:xenConnection forKey:connID];
            NSLog(@"connecting with specified address, username and password");
            [xenConnection ConnectToAddress:address withUsername:username andPassword:password];
            self = xenConnection;
            autoUpdateEnabled = [XenHypervisorConnection isAutoUpdateConfigured];
            [xenConnection release];
            break;
        }
        case HYPERVISOR_EC2:{
            NSLog(@"Constructing Amazon EC2 Connection");
            EC2HypervisorConnection *ec2Connection = [[EC2HypervisorConnection alloc] init];
            autoUpdateEnabled = [ec2Connection isAutoUpdateConfigured];
            [ec2Connection addHypervisorConnectionDelegate:delegate];
            [connections setValue:ec2Connection forKey:connID];
            NSLog(@"connecting with specified address, username and password");
            [ec2Connection ConnectToAddress:address withUsername:username andPassword:password];
            self = ec2Connection;
            [ec2Connection release];
            break;
        }
        default:{
            self = nil;
            break;
        }
    }

    if (self){
        [self setConnectionID:connID];
        [self setWalkTreeMode:NO];
    
        // no pending updates
        [self clearAllObjectTypeRequestPending];

        // all updates are available
        availableUpdates = 0xffffffff;
    }    
    return self;
}

//Accessor for the hypObjects variable.
//Ensures that the dictionary has the required entries
-(NSMutableDictionary*) cachedHypObjects
{
    @synchronized(self){

    if (cachedHypObjects == nil)
    {
        cachedHypObjects = [[NSMutableDictionary alloc] initWithCapacity:4 ];
    }
    
    return cachedHypObjects;
    }
}

- (void)flushHypObjectCache{
    @synchronized(self){

    NSLog(@"flush cache for Hypervisor connection.");
    
    // release any cached objects here and set types to update available and see if rest of app can cope
    [cachedHypObjects removeAllObjects];
    
    // reset the update information so that cache behaves as expected.
    
    // no pending updates
    [self clearAllObjectTypeRequestPending];
    
    // all updates are available
    availableUpdates = 0xffffffff;
    }
}

- (BOOL) isAutoUpdateEnabled{
    return autoUpdateEnabled;
}

/*
 *Indicates if an update is pending for the specified connection type
 */
- (BOOL)isUpdatePendingForType:(int)hypObjectType{
    return (pendingUpdates & hypObjectType);
}

- (void) clearObjectTypeRequestPending:(int) objectType{
    @synchronized(self){
        pendingUpdates = pendingUpdates & (0xFFFF ^ objectType);  // clear the update bit
    }
}

- (void) clearAllObjectTypeRequestPending{
    @synchronized(self){
        pendingUpdates = 0;  // clear the update bit
    }
}

- (void) setObjectTypeRequestPending:(int) objectType{
    @synchronized(self){
        pendingUpdates = pendingUpdates | objectType;
    }
}

//  Determine if there are updates available for the specified object type.
- (BOOL)isUpdateAvailableForType:(int)hypObjectType{
      return YES;
}


- (void) removeCachedHypObjectswithReference:(NSString *)objectRef forObjectType:(int)objectType{
    @synchronized(self){
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ like [c]'%@'", [self uniqueObjectReferencePropertyForHypObjectType:objectType] ,objectRef]];                               
    NSMutableArray* objects = [[self cachedHypObjects] objectForKey:[NSNumber numberWithInt:objectType]];
    NSArray* matches = [objects filteredArrayUsingPredicate:predicate]; 
    if ([matches count] > 0){
        [objects removeObjectsInArray:matches]; 
    }
    }
}

- (void) updateCachedHypObjects:(NSArray *)objects forObjectType:(int)objectType isCompleteSet:(BOOL)completeSet{
    [self updateCachedHypObjects:objects forObjectType:objectType ResetPendingUpdates:YES isCompleteSet:completeSet];
}

- (void) updateCachedHypObjects:(NSArray *)objects forObjectType:(int)objectType ResetPendingUpdates:(BOOL)resetPending isCompleteSet:(BOOL)completeSet{

    @synchronized(self){
    
    // update not replace
    NSLog(@"Updating object cache with the results"); 
    if (completeSet){
        NSLog(@"Flush cache for type object and add whole set");
        // clear any existing objects
        NSArray *clearArray = [[NSArray alloc] init];
        [[self cachedHypObjects] setObject:clearArray forKey:[NSNumber numberWithInt:objectType]];
        [clearArray release];
    }
    else{
        // assume that since not the complete set that the list will be small enough to display information about
        for (id<HypObjReferencesProtocol>o in objects){
           NSLog(@"adding object to cache %@", [o uniqueReference]);
        }
    }
    NSNumber *key = [NSNumber numberWithInt:objectType];
        NSArray *existingObjects = [[self cachedHypObjects] objectForKey:key];
        NSMutableArray *newObjectSet = [NSMutableArray arrayWithArray:existingObjects];
        // remove duplicates here
        NSString *pkName = [self uniqueObjectReferencePropertyForHypObjectType:objectType];
        
        for (NSObject *object in objects){
            NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ like [c]'%@'",pkName,[object valueForKey:pkName]]];                               
            NSArray* matches = [newObjectSet filteredArrayUsingPredicate:predicate]; 
            if ([matches count] > 0){
                [newObjectSet removeObjectsInArray:matches]; 
            }
        }
        // if there are objects to add then add them.
        if (objects){
           [newObjectSet addObjectsFromArray:objects];
        }
        
        [[self cachedHypObjects] setObject:newObjectSet forKey:[NSNumber numberWithInt:objectType]];
        if (resetPending){
            [self clearObjectTypeRequestPending: objectType];  // clear the update bit
        }
    }
}

/*
 * returns the objects for the specified key.  This should only take one of the
 * flags i.e. VMS or Hosts but not both.
 */
- (NSArray *)hypObjectsForType:(int)hypObjectType{
    NSMutableDictionary *dic = [self cachedHypObjects];
    return [dic objectForKey:[NSNumber numberWithInt:hypObjectType]];
}

/*
 * returns the objects for the specified key with the specified predicate
 * This should only take one of the flags i.e. VMS or Hosts but not both.
 */
- (NSArray *)hypObjectsForType:(int)hypObjectType withCondition:(NSPredicate *)predicate{
    
    NSSortDescriptor *descriptor = [self sortForhypObjectType:hypObjectType];
    if (predicate != nil){
        NSMutableDictionary *dic = [self cachedHypObjects];
        NSArray *objects = nil;
        if (descriptor){
            objects = [[dic objectForKey:[NSNumber numberWithInt:hypObjectType]] sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
        }
        else{
            objects = [dic objectForKey:[NSNumber numberWithInt:hypObjectType]];
        }
        
        NSArray *finalObjects = [objects filteredArrayUsingPredicate:predicate];
        return finalObjects; 
    }else {
        if (descriptor){
            return [[self hypObjectsForType:hypObjectType] sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
        }
        else{
            return [self hypObjectsForType:hypObjectType];
        }
    }
}

/**
 Ensures that a specific hypervisor object has all its required sub elements available.
 */
- (BOOL) PopulateHypObjectreference:(NSString *)reference objectType:(int)hypObjectType checkOnly:(BOOL)checkOnly{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ like [c]'%@'", [self uniqueObjectReferencePropertyForHypObjectType:hypObjectType] ,reference]];                               
    NSArray * objects = [self hypObjectsForType:hypObjectType withCondition:predicate];
    if (!objects || [objects count] == 0){
        if (!checkOnly){
            // only request it if there is not already a request pending for this
            if (![self isUpdatePendingForType:hypObjectType]){
               [self RequestHypObject:reference ForType:hypObjectType];
            }
        }
            
        NSLog(@"Object reference %@ not found", reference );
        return NO;
    }
    else{
        return [self PopulateHypObject:[objects objectAtIndex:0] checkOnly:checkOnly];
    }
}

- (BOOL) CheckPopulatedHypObject:(id<HypObjReferencesProtocol>)object{
    return [self PopulateHypObject:object checkOnly:YES];
}

- (void) PopulateHypObject:(id<HypObjReferencesProtocol>)object{
    NSLog(@"Locating children for %@",object);
    if (![self CheckPopulatedHypObject:object]){
       [self PopulateHypObject:object checkOnly:NO];
    }
}

/**
 Ensures that a specific hypervisor object has all its required sub elements available.
 @param object The hypervisor object to populate
 */
- (BOOL) PopulateHypObject:(id<HypObjReferencesProtocol>)object checkOnly:(BOOL)checkOnly{
    NSMutableDictionary *references = [object references];
    // go though the types and see if the objects exist, request the objects as you go
    for (NSNumber *key in [references keyEnumerator]) {
        NSArray *referencedObjects = [references objectForKey:key];
        if ([referencedObjects count] > 0){
            for (NSString *baseObjRef in referencedObjects){
                NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ like [c]'%@'", [self uniqueObjectReferencePropertyForHypObjectType:[key intValue]] ,baseObjRef]];                               
                NSArray *objects = [self hypObjectsForType:[key intValue] withCondition:pred];
                // if this is nil or empty then need to request it
                // otherwise call populate on it to make sure that the object is complete.
                if ([objects count] == 0){
                    if (checkOnly){
                        NSLog(@"Object reference %@ not found", baseObjRef);
                        return NO;
                    }
                    else{
                        NSLog(@"Requesting Object %@ as child of %@",baseObjRef,object);
                        [self RequestHypObject:baseObjRef ForType:[key intValue]];
                    }
                }
            }
        }
    }
    return YES;
}

- (void) clearHypObjectTreeWithRoot:(id<HypObjReferencesProtocol>)object{
    // get a dictionary of types and opaque refs to get
    // check to see if already stored if they are then don't refetch them (might in future store a out of date flag)
    // so that could fetch if required, but this is a future improvement
    // when all objects are available return.
    // if there is a memory warning then flushing all these object caches might be good idea
    // could do with some indication as to what can be flushed could be just the non main types?
    NSMutableDictionary *references = [object references];
    // go though the types and see if the objects exist, request the objects as you go
    for (NSNumber *key in [references keyEnumerator]) {
        NSArray *referencedObjects = [references objectForKey:key];
        for (NSString *baseObjRef in referencedObjects){
            NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ like [c]'%@'", [self uniqueObjectReferencePropertyForHypObjectType:[key intValue]] ,baseObjRef]];                               
            NSArray *objects = [self hypObjectsForType:[key intValue] withCondition:pred];
            // remove the object from the cache
            if ([objects count] > 0){
                for(id<HypObjReferencesProtocol> object in objects){
                    [self removeCachedHypObjectswithReference:[object uniqueReference] forObjectType:[key intValue]];
                    [self clearHypObjectTreeWithRoot:object];
                }
            }
        }
    }
    [self removeCachedHypObjectswithReference:[object uniqueReference] forObjectType:[object objectType]];
}

- (void)releaseMemory{
    NSLog(@"Memory release requested for connection.");
    //[self flushHypObjectCache];
}

-(void) dealloc{
    if (cachedHypObjects){
        [cachedHypObjects release];
        cachedHypObjects = nil;
    }
    [super dealloc];
}
#pragma mark -
#pragma mark HypervisorConnectionDelegate

// add specified delegate
- (void)addHypervisorConnectionDelegate:(id<HypervisorConnectionDelegate>)delegate{
    if (delegate){
        if (hypervisorConnectionDelegates == nil){
            hypervisorConnectionDelegates = [[NSMutableSet alloc] initWithCapacity:1];
        }
        
        if (![hypervisorConnectionDelegates containsObject:delegate]){
            [hypervisorConnectionDelegates addObject:delegate];
        }
    }
}

// remove specified delegate
- (void)removeHypervisorConnectionDelegate:(id<HypervisorConnectionDelegate>)delegate{
    [hypervisorConnectionDelegates removeObject:delegate];
}

- (void)removeAllHypervisorConnectionDelegate{
    [hypervisorConnectionDelegates removeAllObjects];
}

+ (HypervisorConnectionFactory *) getConnectionWithHypervisorType:(HypervisorType)hypervisorType connectonID:(NSString *)connID{
    HypervisorConnectionFactory *result = nil;
    NSNumber *hypervisorId = [[NSNumber alloc] initWithInt:hypervisorType];
    //NSLog(@"Looking up all existing Connections for type %@",hypervisorId);
    NSMutableDictionary *connections = [[HypervisorConnectionFactory connectionDictionary] objectForKey:hypervisorId];
    [hypervisorId release];
    if (connections != nil){
        //NSLog(@"Looking up connection with specified connID");
        result = [connections valueForKey:connID];
        if (!result){
            NSLog(@"Did not find connection with specified connID");
        }
    }
    return result;
}

+ (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    for ( NSNumber *typedConnectionKey in [[HypervisorConnectionFactory connectionDictionary] keyEnumerator]){
        NSMutableDictionary *typedConnections = [[HypervisorConnectionFactory connectionDictionary] objectForKey:typedConnectionKey];
        for (NSString* connectionAddress in [typedConnections keyEnumerator]){
            HypervisorConnectionFactory *connection = [typedConnections objectForKey:connectionAddress];
            if ([connection respondsToSelector:@selector(releaseMemory)]){
               [connection releaseMemory];
            }
        }
    }
}

@end

