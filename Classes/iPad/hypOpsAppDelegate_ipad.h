//
//  testSplitViewAppDelegate.h
//  testSplitView
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "HypOpsAppDelegate.h"

@class RootViewController_ipad;
@class DetailViewNavigationController_ipad;

@interface hypOpsAppDelegate_ipad : HypOpsAppDelegate {

}

@property (nonatomic) IBOutlet UIWindow *window;
@property (nonatomic) IBOutlet UISplitViewController *splitViewController;
@property (nonatomic) IBOutlet DetailViewNavigationController_ipad *detailViewController;

@property (nonatomic) IBOutlet UINavigationController *navigationController;

@end
