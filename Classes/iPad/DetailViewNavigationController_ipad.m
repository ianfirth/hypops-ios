//
//  DetailViewController.m
//  testSplitView
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "DetailViewNavigationController_ipad.h"

@interface DetailViewNavigationController_ipad ()
@property (nonatomic, strong) UIPopoverController *popoverController;
@end

@implementation DetailViewNavigationController_ipad

@synthesize popoverController=_myPopoverController;

@synthesize rootViewController=_rootViewController;

#pragma mark - Managing the detail item

#pragma mark - Split view support

- (void)splitViewController:(UISplitViewController *)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController: (UIPopoverController *)pc
{
    barButtonItem.title = @"Connection";
    [[[[self navigationBar] items] objectAtIndex:0] setLeftBarButtonItem:barButtonItem];
    self.popoverController = pc;
}

// Called when the view is shown again in the split view, invalidating the button and popover controller.
- (void)splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem;
{
    [[[[self navigationBar] items] objectAtIndex:0] setLeftBarButtonItem:nil];
    self.popoverController = nil;
}

- (void)viewDidUnload
{
	[super viewDidUnload];
	self.popoverController = nil;
}

#pragma mark - Memory management

- (void)didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


@end
