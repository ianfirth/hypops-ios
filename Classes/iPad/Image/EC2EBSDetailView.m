//
//  EC2EBSDetailView.m
//  hypOps
//
//  Created by Ian Firth on 12/07/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "EC2EBSDetailView.h"


@implementation EC2EBSDetailView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

/**
 * linked to the label that displays the web site address
 * see http://aws.amazon.com/trademark-guidelines/ for details
 */
- (IBAction)touchUpInside:(id)sender{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"http://aws.amazon.com/ec2" ];
    [[UIApplication sharedApplication] openURL:url];
    [url release];
}
@end
