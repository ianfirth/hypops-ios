//
//  testSplitViewAppDelegate.m
//  testSplitView
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "HypOpsAppDelegate_ipad.h"

#import "DetailViewNavigationController_ipad.h"

@implementation hypOpsAppDelegate_ipad

@synthesize window=_window;
@synthesize splitViewController=_splitViewController;
@synthesize detailViewController=_detailViewController;
@synthesize navigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    // Add the split view controller's view to the window and display.    
    self.window.rootViewController = self.splitViewController;
//
//
//    UIViewController *vc = [[navigationController viewControllers] objectAtIndex:0];
//    
//    // Add the navigation controller's view to the window and display.
//    UINavigationController* myNavigationController= [[UINavigationController alloc]initWithNavigationBarClass:[TitleIconAndDescriptionNavBar class] toolbarClass:nil];
//    
//    [myNavigationController pushViewController:vc animated:YES];
//
//    [[self splitViewController] setViewControllers:[NSArray arrayWithObject:myNavigationController]];
    
    [self.window makeKeyAndVisible];
    return YES;
}


- (void)awakeFromNib
{
    // Pass the managed object context to the root view controller.
    [[self rootViewController] setManagedObjectContext :self.managedObjectContext];
}

@end
