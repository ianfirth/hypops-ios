//
//  RootViewController.m
//  testSplitView
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "RootViewController_ipad.h"
#import "DetailViewNavigationController_ipad.h"
#import "AlertWithDataStore.h"
#import "XenPoolViewController_ipad.h"
#import "InitialViewController_ipad.h"
#import "ConnectionsViewController_ipad.h"
#import "XenHypervisorConnectionDefinition.h"
#import "EC2HypervisorConnectionDefinition.h"
#import "CloudStack_AWSConnectionDefinition.h"
#import "CloudStack_NativeConnectionDefinition.h"
#import "IOS6UINavigationController.h"

@interface RootViewCOntroller_ipad
-(void) displayConnectionDialog;
-(void) displayConnectionDialogToEditIndex:(NSIndexPath *)indexPath;
@end;

@implementation RootViewController_ipad
		
@synthesize detailViewNavigationController;

@synthesize managedObjectContext;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.clearsSelectionOnViewWillAppear = NO;
    self.preferredContentSize = CGSizeMake(320.0, 600.0);
    // Set up the edit and add buttons.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    UIViewController* controller = [[[self detailViewNavigationController] viewControllers] objectAtIndex:0];
    // during a memory warnign this can be loaded without this method available if the tabViewController is current).
    if ([controller respondsToSelector:@selector(setRootController:)]){
        [controller performSelector:@selector(setRootController:) withObject:self];
    }
}

- (void) insertNewObjectusingDisplayRect:(CGRect) rect{
    insertRect = rect;
    [super insertNewObject];
}

// used pre ios8
-(void) displayHypervisorTypeSelectionActionSheet:(UIActionSheet *) actionSheet{
    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    if ([window.subviews containsObject:self.view]) {
        [actionSheet showFromRect:insertRect inView:[self view] animated:YES];
    } else {
       [actionSheet showFromRect:insertRect inView:window animated:YES];
    }
}

// used in ios8 and later
-(void) displayHypervisorTypeSelectionAlertController:(UIAlertController *)alertController{
    [alertController setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alertController
                                                     popoverPresentationController];

    UIWindow* window = [[UIApplication sharedApplication] keyWindow];
    if ([window.subviews containsObject:self.view]) {
        popPresenter.sourceView = [self view];
        popPresenter.sourceRect = insertRect;

    } else {
        popPresenter.sourceView = window;
        popPresenter.sourceRect = insertRect;
    }

    [self presentViewController:alertController animated:YES completion:nil];
}


// this method replaces the complete set of views in the neviagion pane to be the one provided
+(void) setDetailViewRootController:(UIViewController *)controller onNavigationController:(UINavigationController *)navCon{
    UIBarButtonItem *buttonItem = [[[[navCon navigationBar] items]objectAtIndex:0] leftBarButtonItem];
    if (controller){
        NSArray *controllers = [NSArray arrayWithObject:controller];
        [navCon setViewControllers:controllers animated:YES];
    }
    
    if (buttonItem){
        [[[[navCon navigationBar] items] objectAtIndex:0] setLeftBarButtonItem:buttonItem];         
    }
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // if not already displaying the inital view display it now.
    NSArray *currentControllers =  [[self detailViewNavigationController] viewControllers];
    if (![[currentControllers objectAtIndex:0] isKindOfClass:[InitialViewController_ipad class]]){
        InitialViewController_ipad *initialView = [[InitialViewController_ipad alloc] initWithNibName:@"InitialView_ipad" bundle:[NSBundle mainBundle]];
        [initialView setRootController:self];
     
        [RootViewController_ipad setDetailViewRootController:initialView onNavigationController:[self detailViewNavigationController]];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
//#error think issue is here
    // connect to something that you cannot connect to
    // clear ewrror
    // then select new conneciton and watch it crash and burn.
    // Think I need to clear up stuff created in did appear in the did disapper.
//    [RootViewController_ipad setDetailViewRootController:nil onNavigationController:nil];
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

// this one is for iOS6 (the shouldAutoRotateToInterfaceOrientation is for pre iOS 6 only).
- (BOOL)shouldAutorotate{
    return YES;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
   // close the insert new object popover here if open
    if ([self theActionSheet]){
        [[self theActionSheet] dismissWithClickedButtonIndex:0 animated:YES];
    }
    
}

-(void) displayConnectionDialog:(HypervisorType)type{
    NSArray *conDefs = [HypervisorConnectionFactory connectionDefinitions];
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"hypervisorType == %@",[NSNumber numberWithInt:type]]];
    NSArray *result = [conDefs filteredArrayUsingPredicate:pred];
    
    if ([result count] >0){
        
        NSObject<ConnectionDefintionDelegate> *displayDelegate = nil;
        
        if (type == HYPERVISOR_XEN){
            displayDelegate = [[XenHypervisorConnectionDefinition alloc ] init ];
        }else if (type == HYPERVISOR_EC2){
            displayDelegate = [[EC2HypervisorConnectionDefinition alloc ] init ];            
        }
        else if (type == HYPERVISOR_CLOUDSTACK_AWS){
            displayDelegate = [[CloudStack_AWSConnectionDefinition alloc ] init ];            
        }
        else if (type == HYPERVISOR_CLOUDSTACK_NATIVE){
            displayDelegate = [[CloudStack_NativeConnectionDefinition alloc ] init ];            
        }
        else{
            NSLog(@"Hyperivosr conneciton Type not recognised");
        }
        
        ConnectionsViewController_ipad *c = [[ConnectionsViewController_ipad alloc] initWithNibName:@"Connections" bundle:[NSBundle mainBundle] resultsController:[self fetchedResultsController] connectionDefinition:[result objectAtIndex:0] connectionDelegate:displayDelegate];
        
        // Create the navigation controller and present it modally.
        UINavigationController *navigationController = [[IOS6UINavigationController alloc]
                                                        initWithRootViewController:c];
        
        
        [navigationController setModalPresentationStyle:UIModalPresentationFormSheet];
        [navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        
        [self presentViewController:navigationController animated:YES completion:nil];
    }
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.editing){
        NSArray *conDefs = [HypervisorConnectionFactory connectionDefinitions];
        ConnectionDefinition *conDef;
        NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"hypervisorType == %@",[NSNumber numberWithLong:[[aTableView cellForRowAtIndexPath:indexPath] tag]]]];
        NSArray *result = [conDefs filteredArrayUsingPredicate:pred];
        
        if ([result count] >0){
            conDef = [result objectAtIndex:0];
            
            NSObject<ConnectionDefintionDelegate> *displayDelegate = nil;
            
            if (conDef.hypervisorType == HYPERVISOR_XEN){
                displayDelegate = [[XenHypervisorConnectionDefinition alloc ] init ];
            }
            else if (conDef.hypervisorType == HYPERVISOR_EC2){
                displayDelegate = [[EC2HypervisorConnectionDefinition alloc ] init ];            
            }
            else if (conDef.hypervisorType == HYPERVISOR_CLOUDSTACK_AWS){
                displayDelegate = [[CloudStack_AWSConnectionDefinition alloc ] init ];            
            }
            else if (conDef.hypervisorType == HYPERVISOR_CLOUDSTACK_NATIVE){
                displayDelegate = [[CloudStack_NativeConnectionDefinition alloc ] init ];            
            }
            else {
                NSLog(@"Unrecognised hypervisor connection type");
            }
            
            ConnectionsViewController_ipad *c = [[ConnectionsViewController_ipad alloc] initWithNibName:@"Connections" bundle:[NSBundle mainBundle] resultsController:[self fetchedResultsController] connectionDefinition:conDef
                connectionDelegate:displayDelegate];
            
          
            // Create the navigation controller and present it modally.
            UINavigationController *navigationController = [[IOS6UINavigationController alloc]
                                                            initWithRootViewController:c];

            [c setEditMode:MODE_EDIT];
            [c setObjectToEdit:indexPath];

            // actually need this inside a view with save and cancel controls
            [navigationController setModalPresentationStyle:UIModalPresentationFormSheet];
            [navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
            
            [self presentViewController:navigationController animated:YES completion:nil];
        }

    }else{
        [self makeConnectionWithIndexPath:indexPath];
    }
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

- (void)insertNewObjectForHypervisorType:(HypervisorType)hypType
{
    // get the appropriate connection dialog from the connection factory here
    [self displayConnectionDialog:hypType];
}

@end
