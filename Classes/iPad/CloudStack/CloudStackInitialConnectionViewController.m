//
//  CloudStackInitialConnectionViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackInitialConnectionViewController.h"

@interface CloudStackInitialConnectionViewController ()
-(HypervisorConnectionFactory *) hypervisorConnection;
- (CloudStackZone *) csZone;
@end
@implementation CloudStackInitialConnectionViewController

@synthesize zoneReference;
@synthesize viewTitleLabel;
@synthesize cpuUsage,memoryUsage,storageUsage,storageAllocatedUsage,vnetPublicIPUsage,privateIPUsage,secondaryStorageUsage,vlanUsage,directAttachedPublicIPUsage,localStorageUsage;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil hypConnectionType:(HypervisorType) theHypervisorType andConnectionId:(NSString*)theConnectionId{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        connectionId = theConnectionId;
        hypervisorType = theHypervisorType;
    }
    return self;
}

- (void) setId:(NSString*)newID{
    [self setZoneReference:newID];
    [self viewWillAppear:NO];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark for RefreshablePage protocol
-(void) refreshPage{
#warning should this do anything
}

- (UIImage*) currentTitleImage{
#warning should this have an image
    return nil;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self cpuUsage] setMaximumValue:100];
    [[self cpuUsage] setMinumumValue:0];
    
    [[self memoryUsage] setMaximumValue:100];
    [[self memoryUsage] setMinumumValue:0];
    
    [[self storageUsage] setMaximumValue:100];
    [[self storageUsage] setMinumumValue:0];
    
    [[self storageAllocatedUsage] setMaximumValue:100];
    [[self storageAllocatedUsage] setMinumumValue:0];
    
    [[self vnetPublicIPUsage] setMaximumValue:100];
    [[self vnetPublicIPUsage] setMinumumValue:0];
    
    [[self privateIPUsage] setMaximumValue:100];
    [[self privateIPUsage] setMinumumValue:0];
    
    [[self secondaryStorageUsage] setMaximumValue:100];
    [[self secondaryStorageUsage] setMinumumValue:0];
    
    [[self vlanUsage] setMaximumValue:100];
    [[self vlanUsage] setMinumumValue:0];
    
    [[self directAttachedPublicIPUsage] setMaximumValue:100];
    [[self directAttachedPublicIPUsage] setMinumumValue:0];
    
    [[self localStorageUsage] setMaximumValue:100];
    [[self localStorageUsage] setMinumumValue:0];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewWillAppear:(BOOL)animated{

    NSString* name = @"CloudStack Zone Metrics";
   
    CloudStackZone* zone = [self csZone];
    // need the zone object to fill these in correctly
    
    [[self cpuUsage] setCurrentValue:[zone percentused_cpu]];    
    [[self memoryUsage] setCurrentValue:[zone percentused_memory]];
    [[self storageUsage] setCurrentValue:[zone percentused_storage]];
    [[self storageAllocatedUsage] setCurrentValue:[zone percentused_storageAllocated]];
    [[self vnetPublicIPUsage] setCurrentValue:[zone percentused_vnetPublicIP]];
    [[self privateIPUsage] setCurrentValue:[zone percentused_privateIP]];
    [[self secondaryStorageUsage] setCurrentValue:[zone percentused_storage]];
    [[self vlanUsage] setCurrentValue:[zone percentused_vlan]];
    [[self directAttachedPublicIPUsage] setCurrentValue:[zone percentused_directAttachedPublicIP]];
    [[self localStorageUsage] setCurrentValue:[zone percentused_localStorage]];

    [[self viewTitleLabel] setText:name];
    [super viewWillAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

/**
 * linked to the label that displays the web site address
 * see http://aws.amazon.com/trademark-guidelines/ for details
 */
- (IBAction)touchUpInside:(id)sender{
    NSString* urlString = @"http://cloudstack.org/";
    
    NSURL *url = [ [ NSURL alloc ] initWithString: urlString ];
    [[UIApplication sharedApplication] openURL:url];
}

-(HypervisorConnectionFactory *) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:connectionId];
}

- (CloudStackZone *) csZone{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'", [self zoneReference]]]; 
    NSArray *zones = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_ZONE withCondition:pred];
    if ([zones count] >0){
        return [zones objectAtIndex:0];
    }
    return nil;
}

@end
