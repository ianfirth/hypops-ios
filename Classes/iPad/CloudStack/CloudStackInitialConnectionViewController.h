//
//  CloudStackInitialConnectionViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "HypervisorConnectionFactory.h"
#import "LevelIndicator.h"
#import "CloudStackZone.h"
#import "Identifyable.h"
#import "RefreshablePage.h"

@interface CloudStackInitialConnectionViewController : UIViewController<Identifyable, RefreshablePage>{
    NSString* connectionId;
    HypervisorType hypervisorType;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil hypConnectionType:(HypervisorType) theHypervisorType andConnectionId:(NSString*)theConnectionId;

// this is used in the tab contol to update the page.  In this class it updates the zoneReference.
- (void) setId:(NSString*)newID;

@property NSString* zoneReference;

@property (weak) IBOutlet UILabel* viewTitleLabel;

@property (weak) IBOutlet LevelIndicator* cpuUsage;
@property (weak) IBOutlet LevelIndicator* memoryUsage;
@property (weak) IBOutlet LevelIndicator* storageUsage;
@property (weak) IBOutlet LevelIndicator* storageAllocatedUsage;
@property (weak) IBOutlet LevelIndicator* vnetPublicIPUsage;
@property (weak) IBOutlet LevelIndicator* privateIPUsage;
@property (weak) IBOutlet LevelIndicator* secondaryStorageUsage;
@property (weak) IBOutlet LevelIndicator* vlanUsage;
@property (weak) IBOutlet LevelIndicator* directAttachedPublicIPUsage;
@property (weak) IBOutlet LevelIndicator* localStorageUsage;

@end
