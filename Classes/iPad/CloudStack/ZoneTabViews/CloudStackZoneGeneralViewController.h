//
//  CloudStackZoneGeneralViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ExpandableTableViewController.h"
#import "CloudStackNativeHypervisorConnection.h"
#import "CloudStackZone.h"
#import "Identifyable.h"
#import "RefreshablePage.h"

#define CSZONE_GENERAL_TABLE 0
#define CSZONE_NETWORK_TABLE 1
#define CSZONE_TABLE_COUNT 2

#define CSZONE_GENERAL_TABLE_ALLOCATIONSTATE 0
#define CSZONE_GENERAL_TABLE_DOMAIN 1
#define CSZONE_GENERAL_TABLE_SECURITYGROUPENABLED 2
#define CSZONE_GENERAL_TABLE_COUNT 3

#define CSZONE_NETWORK_TABLE_NETWORKTYPE 0
#define CSZONE_NETWORK_TABLE_DOMAINNAME 1
#define CSZONE_NETWORK_TABLE_DHCPPROVIDER 2
#define CSZONE_NETWORK_TABLE_DNS1 3
#define CSZONE_NETWORK_TABLE_DNS2 4
#define CSZONE_NETWORK_TABLE_VLAN 5
#define CSZONE_NETWORK_TABLE_COUNT 6

@interface CloudStackZoneGeneralViewController : ExpandableTableViewController <HypervisorConnectionDelegate,Identifyable,RefreshablePage>{
    HypervisorType hypervisorType;
    NSString* _zoneId;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withZone:(CloudStackZone *)thisZone;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

// this is used by relection in the tab contol t o update the page.  In this class it updates the zoneID.
- (void) setId:(NSString*)newID;

@property (copy) NSString *hypConnID;
@property (copy) NSString *zoneId;

@end
