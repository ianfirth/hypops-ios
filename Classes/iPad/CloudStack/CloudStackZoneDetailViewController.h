//
//  CloudStackZoneDetailViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CSZoneMemoryRefreshableKeyMetricPanelController.h"
#import "CSZoneMemoryRefreshableKeyMetricPanelController.h"
#import "CSZoneCPURefreshableKeyMetricPanelController.h"
#import "CSZoneLocalStorageRefreshableKeyMetricPanelController.h"
#import "CSZonePrivateIPRefreshableKeyMetricPanelController.h"
#import "CSZoneSecondaryStorageRefreshableKeyMetricPanelController.h"
#import "CSZoneStorageRefreshableKeyMetricPanelController.h"
#import "CSZoneVLANRefreshableKeyMetricPanelController.h"
#import "CSZoneVNETPublicIPRefreshableKeyMetricPanelController.h"

@interface CloudStackZoneDetailViewController : UIViewController<HypervisorConnectionDelegate>{
    NSString* hypConnID;
    CSZoneMemoryRefreshableKeyMetricPanelController* zoneMemoryViewController;
    CSZoneCPURefreshableKeyMetricPanelController* zoneCPUViewController;
    CSZoneLocalStorageRefreshableKeyMetricPanelController* zoneLocalStorageViewController;
    CSZonePrivateIPRefreshableKeyMetricPanelController* zonePrivateIPViewController;
    CSZoneSecondaryStorageRefreshableKeyMetricPanelController* zoneSecondaryStorageViewController;
    CSZoneStorageRefreshableKeyMetricPanelController* zoneStorageViewController;
    CSZoneVLANRefreshableKeyMetricPanelController* zoneVLANViewController;
    CSZoneVNETPublicIPRefreshableKeyMetricPanelController* zoneVNETPublicIPViewController;
    
   IBOutlet __weak UIView* zoneMemoryView;
   IBOutlet __weak UIView* zoneCPUView;
   IBOutlet __weak UIView* zoneLocalStorageView;
   IBOutlet __weak UIView* zonePrivateIPView;
   IBOutlet __weak UIView* zoneSecondaryStorageView;
   IBOutlet __weak UIView* zoneStorageView;
   IBOutlet __weak UIView* zoneVLANView;
   IBOutlet __weak UIView* zoneVNETPublicIPView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andHypervisorConnectionId:(NSString*)connectionID;

@end
