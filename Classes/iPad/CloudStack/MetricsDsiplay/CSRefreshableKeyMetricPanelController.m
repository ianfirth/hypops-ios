//
//  RefreshableKeyMetricPanel.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CSRefreshableKeyMetricPanelController.h"
#import "CloudStackBase.h"

@interface CSRefreshableKeyMetricPanelController (){
    NSString* theConnectionId;
    NSTimer* connectionTimer;
}
- (NSInteger) getInidicatorLevel:(LevelIndicator*)indicator;

@end

@implementation CSRefreshableKeyMetricPanelController

- (id)init
{
    self = [super initWithNibName:@"CSGeneralMetrics" bundle:nil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

-(void) viewDidLoad{
    [super viewDidLoad];
    [levelIndicator1 setHidden:YES];
    [levelIndicator2 setHidden:YES];
    [levelIndicator3 setHidden:YES];
    [textItem1 setHidden:YES];
    [textItem2 setHidden:YES];
    [textItem3 setHidden:YES];
    // put a no data available message here perhaps
}

-(void)setHypervisorConnection:(CloudStackNativeHypervisorConnection*)connection{
    // if the connection is not already set then setup the controller for the
    // connection
    if (!theConnectionId){
        theConnectionId = [connection connectionID];
        NSArray* zones = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_ZONE];
        [self updateDisplayFromDatabase:zones];
//        if (!connectionTimer){
//            // start a timer so this happens every 5 mins
//            connectionTimer = [NSTimer scheduledTimerWithTimeInterval:5 * 60 target:self selector:@selector(update:)  userInfo:nil repeats:YES];
//        }
    }
}

//define the targetmethod
-(void) update:(NSTimer *) theTimer {
    if ([theTimer isValid]){
        connectionTimer = nil;
        [self updateDisplay];
    }
}

-(void) updateDisplay{
    if (theConnectionId){
        NSArray* zones = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_ZONE];
        [self updateDisplayFromDatabase:zones];
    }
}

-(CloudStackNativeHypervisorConnection*)hypervisorConnection{
    NSNumber *hypervisorId = [[NSNumber alloc] initWithInt:HYPERVISOR_CLOUDSTACK_NATIVE];
    NSDictionary* connections = [[CloudStackNativeHypervisorConnection connectionDictionary] objectForKey:hypervisorId];
    return [connections objectForKey:theConnectionId]; 
}


-(void) updateDisplayFromDatabase:(NSArray*)zones{
   // really to be overriden, should be marked abstract really
}

- (NSInteger) getInidicatorLevel:(LevelIndicator*)indicator{
    if ([indicator isHidden]){
        return 0;        
    }

    if ([indicator currentValue] >= [indicator criticalLevel]){
        return 2;        
    }    
    if ([indicator currentValue] >= [indicator warningLevel]){
        return 1;        
    }
    return 0;
}

- (NSArray *)SortedKeysFor:(NSMutableDictionary *)finalResults {
    NSArray* allKeys = [finalResults keysSortedByValueUsingComparator:^(id obj1, id obj2) {
        
        if ([obj1 doubleValue]  > [obj2 doubleValue] ) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        
        if ([obj1 doubleValue]  < [obj2 doubleValue] ) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        return (NSComparisonResult)NSOrderedSame;
	}];
    return allKeys;
}

- (void)DisplayResults:(NSMutableDictionary *)finalResults {
    NSArray *allKeys = [self SortedKeysFor:finalResults];
    
    if ([allKeys count] > 0){
        [levelIndicator1 setHidden:NO];        
        [textItem1 setHidden:NO];
        [textItem1 setText:[allKeys objectAtIndex:0]];
        double value = [[finalResults valueForKey:[allKeys objectAtIndex:0]] doubleValue];
        [levelIndicator1 setCurrentValue:value * 100];
    }
    else{
        [levelIndicator1 setHidden:YES];
        [textItem1 setHidden:YES];
    }
    
    if ([allKeys count] > 1){
        [levelIndicator2 setHidden:NO];        
        [textItem2 setHidden:NO];
        [textItem2 setText:[allKeys objectAtIndex:1]];
        double value = [[finalResults valueForKey:[allKeys objectAtIndex:1]] doubleValue] *100;
        [levelIndicator2 setCurrentValue:value];
    }
    else{
        [levelIndicator2 setHidden:YES];
        [textItem2 setHidden:YES];
    }
    
    if ([allKeys count] > 2){
        [levelIndicator3 setHidden:NO];        
        [textItem3 setHidden:NO];
        [textItem3 setText:[allKeys objectAtIndex:2]];
        double value = [[finalResults valueForKey:[allKeys objectAtIndex:2]] doubleValue] *100;
        [levelIndicator3 setCurrentValue:value];
    }
    else{
        [levelIndicator3 setHidden:YES];
        [textItem3 setHidden:YES];
    }
    
    
    // render the overall Indication based on the indication levels on the individual indicators
    NSUInteger worstLevel = 0; // 0 = normal(green), 1 = warning(orange), 2 = error(red)
    NSUInteger level1 = [self getInidicatorLevel:levelIndicator1];
    if (level1 > worstLevel){
        worstLevel = level1;
    }NSUInteger level2 = [self getInidicatorLevel:levelIndicator2];    
    if (level2 > worstLevel){
        worstLevel = level2;
    }NSUInteger level3 = [self getInidicatorLevel:levelIndicator3];
    if (level3 > worstLevel){
        worstLevel = level3;
    }
    // overallIndication
    switch (worstLevel) {
        case 0:
            [overallIndication setColor:[UIColor greenColor]];
            break;
        case 1:
            [overallIndication setColor:[UIColor yellowColor]];
            break;
        case 2:
            [overallIndication setColor:[UIColor redColor]];
            break;
        default:
            [overallIndication setColor:[UIColor greenColor]];
            break;
    }
}

@end
