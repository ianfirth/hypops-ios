//
//  CSZoneVNETPublicIPRefreshableKeyMetricPanelController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CSZoneVNETPublicIPRefreshableKeyMetricPanelController.h"
#import "DataEntry.h"
#import "CloudStackZone.h"
#import "ImageBuilder.h"
#import "XportDatabase.h"
#import "ValueObject.h"

@implementation CSZoneVNETPublicIPRefreshableKeyMetricPanelController

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}


// when view loads update the compoent display attributes for the view
-(void) viewDidLoad{
    [super viewDidLoad];
    // set the title.
    [descriptionLabel setText:@"Virtual Netowrk Public IP % Capacity"]; 
    [imageView setImage:[ImageBuilder hostImage]];
}


-(void) updateDisplayFromDatabase:(NSArray*)zones{
    
// take the top 3 
//    create an array of value objects name and doubleValue

    NSMutableDictionary* finalResults = [[NSMutableDictionary alloc] init];

    NSMutableArray* results = [[NSMutableArray alloc] initWithCapacity:[zones count]];
    for (CloudStackZone* zone in zones){
        
        ValueObject* v = [[ValueObject alloc] initWithUUID:[zone name] andDoubleValue: ([zone percentused_vnetPublicIP])/100];
        [results addObject:v];
    }
    
    if (results != nil && [results count] >0){
        [results sortUsingDescriptors:
             [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"doubleValue" ascending:NO]]];NSUInteger counter = 0;NSUInteger allowedCounter = 0;
        while (allowedCounter <3 && counter< [results count]){
            NSString* uuid = [[results objectAtIndex:counter] uuid];                
            double value = [[results objectAtIndex:counter] doubleValue];
               
            [finalResults setValue:[NSNumber numberWithDouble:value] forKey:uuid];
            allowedCounter ++;
            counter ++;
        }
    }
    
    [self DisplayResults:finalResults]; 
}

@end

