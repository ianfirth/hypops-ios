//
//  CloudStackZoneDetailViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackZoneDetailViewController.h"
#import "CloudStackNativeHypervisorConnection.h"
#import "CloudStackBase.h"

@interface CloudStackZoneDetailViewController()
-(void) initViewWithContoller:(CSRefreshableKeyMetricPanelController*)controller andView:(UIView*)view;
-(void) setRightBarButtonForNavigationController:(UINavigationController *)navigationController andButton:(UIBarButtonItem *)barButton;
-(void)refreshView;
@end

@implementation CloudStackZoneDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andHypervisorConnectionId:(NSString*)connectionID;
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        hypConnID = connectionID;       
        zoneMemoryViewController = [[CSZoneMemoryRefreshableKeyMetricPanelController alloc] init];
        zoneCPUViewController = [[CSZoneCPURefreshableKeyMetricPanelController alloc] init];
        zoneLocalStorageViewController = [[CSZoneLocalStorageRefreshableKeyMetricPanelController alloc] init];
        zoneStorageViewController = [[CSZoneStorageRefreshableKeyMetricPanelController alloc] init];
        zoneVLANViewController = [[CSZoneVLANRefreshableKeyMetricPanelController alloc] init];
        zonePrivateIPViewController = [[CSZonePrivateIPRefreshableKeyMetricPanelController alloc] init];
        zoneVNETPublicIPViewController = [[CSZoneVNETPublicIPRefreshableKeyMetricPanelController alloc] init];
        zoneSecondaryStorageViewController = [[CSZoneSecondaryStorageRefreshableKeyMetricPanelController alloc] init];
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

-(void) setRightBarButtonForNavigationController:(UINavigationController *)navigationController andButton:(UIBarButtonItem *)barButton{
    // place the toolbar into the navigation bar
    NSArray *viewControllers = [navigationController viewControllers];
    UINavigationItem *navItem = [[viewControllers objectAtIndex:[viewControllers count]-1] navigationItem];
    [navItem setRightBarButtonItem:barButton];
}

-(void)viewDidLoad{
    CloudStackNativeHypervisorConnection* hypervisorConnection = (CloudStackNativeHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_CLOUDSTACK_NATIVE connectonID:hypConnID];
    
    [hypervisorConnection addHypervisorConnectionDelegate: self];
    [super viewDidLoad];

    [self initViewWithContoller:zoneMemoryViewController andView:zoneMemoryView];
    [self initViewWithContoller:zoneCPUViewController andView:zoneCPUView];
    [self initViewWithContoller:zoneLocalStorageViewController andView:zoneLocalStorageView];
    [self initViewWithContoller:zoneStorageViewController andView:zoneStorageView];
    [self initViewWithContoller:zoneVLANViewController andView:zoneVLANView];
    [self initViewWithContoller:zonePrivateIPViewController andView:zonePrivateIPView];
    [self initViewWithContoller:zoneVNETPublicIPViewController andView:zoneVNETPublicIPView];
    [self initViewWithContoller:zoneSecondaryStorageViewController andView:zoneSecondaryStorageView];
    
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                      target:self
                                      action:@selector(refreshView)];
    [refreshButton setStyle:UIBarButtonItemStyleBordered];
    
    [self setRightBarButtonForNavigationController:[self navigationController] andButton:refreshButton];
}

-(void)refreshView{
  // todo reload the zones
    CloudStackNativeHypervisorConnection* hypervisorConnection = (CloudStackNativeHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_CLOUDSTACK_NATIVE connectonID:hypConnID];
    [hypervisorConnection RequestHypObjectsForType:HYPOBJ_CS_ZONE];
}

-(void) initViewWithContoller:(CSRefreshableKeyMetricPanelController*)controller andView:(UIView*)view{
    CloudStackNativeHypervisorConnection* hypervisorConnection = (CloudStackNativeHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_CLOUDSTACK_NATIVE connectonID:hypConnID];

    [view addSubview:[controller view]];
    CGRect newFrame = [view frame];
    newFrame.origin.x=0;
    newFrame.origin.y=0;
    [[controller view] setFrame:newFrame];
    [controller viewDidLoad];
    [controller setHypervisorConnection:hypervisorConnection];    
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [zoneMemoryViewController viewDidAppear:animated];    
    [zoneCPUViewController viewDidAppear:animated];
    [zoneLocalStorageViewController viewDidAppear:animated];
    [zoneStorageViewController viewDidAppear:animated];
    [zoneVLANViewController viewDidAppear:animated];
    [zonePrivateIPViewController viewDidAppear:animated];
    [zoneVNETPublicIPViewController viewDidAppear:animated];
    [zoneSecondaryStorageViewController viewDidAppear:animated];    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    CloudStackNativeHypervisorConnection* hypervisorConnection = (CloudStackNativeHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_CLOUDSTACK_NATIVE connectonID:hypConnID];
    
    [hypervisorConnection addHypervisorConnectionDelegate: self];
    
    [zoneMemoryViewController viewWillAppear:animated];    
    [zoneCPUViewController viewWillAppear:animated];
    [zoneLocalStorageViewController viewWillAppear:animated];
    [zoneStorageViewController viewWillAppear:animated];
    [zoneVLANViewController viewWillAppear:animated];
    [zonePrivateIPViewController viewWillAppear:animated];
    [zoneVNETPublicIPViewController viewWillAppear:animated];
    [zoneSecondaryStorageViewController viewWillAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    CloudStackNativeHypervisorConnection* hypervisorConnection = (CloudStackNativeHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_CLOUDSTACK_NATIVE connectonID:hypConnID];
    
    [hypervisorConnection removeHypervisorConnectionDelegate: self];

    [zoneMemoryViewController viewDidDisappear:animated];    
    [zoneCPUViewController viewDidDisappear:animated];
    [zoneLocalStorageViewController viewDidDisappear:animated];
    [zoneStorageViewController viewDidDisappear:animated];
    [zoneVLANViewController viewDidDisappear:animated];
    [zonePrivateIPViewController viewDidDisappear:animated];
    [zoneVNETPublicIPViewController viewDidDisappear:animated];
    [zoneSecondaryStorageViewController viewDidDisappear:animated];
}

-(void)viewDidUnload{
    [super viewDidUnload];
    [zoneMemoryViewController viewDidUnload];    
    [zoneCPUViewController viewDidUnload];
    [zoneLocalStorageViewController viewDidUnload];
    [zoneStorageViewController viewDidUnload];
    [zoneVLANViewController viewDidUnload];
    [zonePrivateIPViewController viewDidUnload];
    [zoneVNETPublicIPViewController viewDidUnload];
    [zoneSecondaryStorageViewController viewDidUnload];

}

#pragma mark -
#pragma mark HypervisorConnectionDelegate protocol

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    if (objectType && HYPOBJ_CS_ZONE > 0){
        [zoneMemoryViewController updateDisplay];    
        [zoneCPUViewController updateDisplay];
        [zoneLocalStorageViewController updateDisplay];
        [zoneStorageViewController updateDisplay];
        [zoneVLANViewController updateDisplay];
        [zonePrivateIPViewController updateDisplay];
        [zoneVNETPublicIPViewController updateDisplay];
        [zoneSecondaryStorageViewController updateDisplay];
    }
}

@end
