//
//  XenPoolViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenPoolViewController_ipad.h"
#import "XenBase.h"
#import "XenHostMemoryRefreshableKeyMetricPanelController.h"

@implementation XenPoolViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andHypervisorConnectionId:(NSString*)connectionID
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        hypConnID = connectionID;
        hostMetricsMemoryViewController = [[XenHostMemoryRefreshableKeyMetricPanelController alloc] init];
        hostMetricsViewController = [[XenHostRefreshableKeyMetricPanelConntroller alloc] init ];
        vmCPUMetricsViewController = [[XenVMCPURefreshableKeyMetricsPanelController alloc] init ];
        vmMemoryMetricsViewController = [[XenVMMemoryRefreshableKeyMetricsPanelController alloc] init];    
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}


-(void)viewDidLoad{
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:hypConnID];
    
    [xenHypervisorConnection addHypervisorConnectionDelegate: self];

    [super viewDidLoad];
    [hostMetricsMemoryView addSubview:[hostMetricsMemoryViewController view]];
    CGRect newFrame = [hostMetricsMemoryView frame];
    newFrame.origin.x=0;
    newFrame.origin.y=0;
    [[hostMetricsMemoryViewController view] setFrame:newFrame];
    [hostMetricsMemoryViewController viewDidLoad];

    [hostMetricsView addSubview:[hostMetricsViewController view]];
    newFrame = [hostMetricsView frame];
    newFrame.origin.x=0;
    newFrame.origin.y=0;
    [[hostMetricsViewController view] setFrame:newFrame];
    [hostMetricsViewController viewDidLoad];

    [vmCPUMetricsView addSubview:[vmCPUMetricsViewController view]];
    newFrame = [vmCPUMetricsView frame];
    newFrame.origin.x=0;
    newFrame.origin.y=0;
    [[vmCPUMetricsViewController view] setFrame:newFrame];
    [vmCPUMetricsViewController viewDidLoad];

    [vmMemoryMetricsView addSubview:[vmMemoryMetricsViewController view]];
    newFrame = [vmMemoryMetricsView frame];
    newFrame.origin.x=0;
    newFrame.origin.y=0;
    [[vmMemoryMetricsViewController view] setFrame:newFrame];
    [vmMemoryMetricsViewController viewDidLoad];
    [hostMetricsMemoryViewController setHypervisorConnection:xenHypervisorConnection];    
    [hostMetricsViewController setHypervisorConnection:xenHypervisorConnection];    
    [vmCPUMetricsViewController setHypervisorConnection:xenHypervisorConnection];    
    [vmMemoryMetricsViewController setHypervisorConnection:xenHypervisorConnection];    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [hostMetricsMemoryViewController viewDidAppear:animated];    
    [hostMetricsViewController viewDidAppear:animated];    
    [vmCPUMetricsViewController viewDidAppear:animated];    
    [vmMemoryMetricsViewController viewDidAppear:animated];    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:hypConnID];
    
    [xenHypervisorConnection addHypervisorConnectionDelegate: self];

    [hostMetricsMemoryViewController viewWillAppear:animated];    
    [hostMetricsViewController viewWillAppear:animated];    
    [vmCPUMetricsViewController viewWillAppear:animated];    
    [vmMemoryMetricsViewController viewWillAppear:animated];    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:hypConnID];
    
    [xenHypervisorConnection removeHypervisorConnectionDelegate: self];
}

-(void)viewDidUnload{
    [super viewDidUnload];
    [hostMetricsMemoryViewController viewDidUnload];    
    [hostMetricsViewController viewDidUnload];    
    [vmCPUMetricsViewController viewDidUnload];    
    [vmMemoryMetricsViewController viewDidUnload];    
}

#pragma mark -
#pragma mark HypervisorConnectionDelegate protocol

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    if (objectType && HYPOBJ_HOST > 0){
        [hostMetricsMemoryViewController updateDisplay];    
        [hostMetricsViewController updateDisplay];    
        // these need to be here because if the change is from no hosts to hosts, then
        // the metrics will go from no data to data as tehe host property is needed to get the URL
        // to access the metrics data.
        [vmCPUMetricsViewController updateDisplay];    
        [vmMemoryMetricsViewController updateDisplay];    
    }
    if (objectType && HYPOBJ_VM > 0){
        [vmCPUMetricsViewController updateDisplay];    
        [vmMemoryMetricsViewController updateDisplay];    
    }
}
@end
