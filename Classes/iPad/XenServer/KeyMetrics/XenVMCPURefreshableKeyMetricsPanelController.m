//
//  XenVMCPURefreshableKeyMetricsPanelController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVMCPURefreshableKeyMetricsPanelController.h"
#import "XenHypervisorConnection.h"
#import "DataEntry.h"
#import "XenVM.h"
#import "ImageBuilder.h"
#import "XportDatabase.h"
#import "ValueObject.h"
#import "XenBase.h"

@implementation XenVMCPURefreshableKeyMetricsPanelController

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}


// when view loads update the compoent display attributes for the view
-(void) loadView{
    [super loadView];
    // set the title.
    [descriptionLabel setText:@"VM - Highest CPU Load"];
    [imageView setImage:[ImageBuilder vMImage]];
}


-(void) updateDisplayFromDatabase:(NSArray*)databases{
    NSMutableDictionary* results = [[NSMutableDictionary alloc] init];
    
    // go thought the entires and find the ones that are marked vm and
    // start with cpu
    // if there is already an entry for the store a CPU values in a map against the ID add the value and divide by 2 (average aof all processors)
    // if there is no value add the value
    
    for (XportDatabase* database in databases) {
        for (DataEntry* entry in [database dataEntries]) {
            if ([[entry objectType] isEqualToString:@"vm"] && 
                [[entry propertyDescription] hasPrefix:@"cpu"]){
                
                NSNumber *transactionAverage=[[entry values] valueForKeyPath:@"@avg.doubleValue"];
                
                // if there is already an entry add this value to the average one already there
                if ([results valueForKey:[entry objectId]] != nil){
                    NSNumber* existingNum = [results valueForKey:[entry objectId]];
                    transactionAverage = [[NSNumber alloc] initWithDouble:([existingNum doubleValue] + [transactionAverage doubleValue])/2];
                }
                [results setValue:transactionAverage forKey:[entry objectId]];
            }
        }
    }
    
    // build dictionary into array of value objects
    NSMutableArray *allResults = [[NSMutableArray alloc]init];
    for (NSString* uuid in [results allKeys]) {
        double value = [[results valueForKey:uuid] doubleValue];
        ValueObject* o = [[ValueObject alloc] initWithUUID:uuid andDoubleValue:value];
        [allResults addObject:o];

    }
    
    // take the top 3 that resolve to vm (ids)
    // it is possible that some of the entires no longer exist
    // can I sort the results by the values, then go though find the top 3 that resolve
    
    NSMutableDictionary* finalResults = [[NSMutableDictionary alloc] init];
    
    if (allResults != nil && [results count] >0){
        [allResults sortUsingDescriptors:
         [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"doubleValue" ascending:NO]]];NSUInteger counter = 0;NSUInteger allowedCounter = 0;
        while (allowedCounter <3 && counter< [results count]){
            NSString* uuid = [[allResults objectAtIndex:counter] uuid];                
            double value = [[allResults objectAtIndex:counter] doubleValue];
            NSArray *vms = [[self XenHypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseForUUID:uuid]];
            
            if (vms != nil && [vms count] >0){
                XenVM *vm = [vms objectAtIndex:0];
                
                [finalResults setValue:[NSNumber numberWithDouble:value] forKey:[vm name_label]];
                allowedCounter ++;
            }
            counter ++;
        }
    }
    
    [self DisplayResults:finalResults];  
}

@end
