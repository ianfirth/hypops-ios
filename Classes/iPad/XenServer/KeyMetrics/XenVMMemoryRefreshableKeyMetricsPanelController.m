//
//  XenVMMemoryRefreshableKeyMetricsViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVMMemoryRefreshableKeyMetricsPanelController.h"
#import "XenHypervisorConnection.h"
#import "DataEntry.h"
#import "XenVM.h"
#import "ImageBuilder.h"
#import "XportDatabase.h"
#import "ValueObject.h"
#import "XenBase.h"
    
@implementation XenVMMemoryRefreshableKeyMetricsPanelController

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}


// when view loads update the compoent display attributes for the view
-(void) loadView{
    [super loadView];
    // set the title.
    [descriptionLabel setText:@"VM - Highest Memory Load"]; 
    [imageView setImage:[ImageBuilder vMImage]];
}



-(void) updateDisplayFromDatabase:(NSArray*)databases{
    NSMutableDictionary* resultsTotalMemory = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* resultsFreeMemory = [[NSMutableDictionary alloc] init];    
    
    // go though the entires and find the ones that are marked as host and
    // fill up 2 dictionaries, one with total memory and one with free memeory
    
    for (XportDatabase* database in databases) {
        for (DataEntry* entry in [database dataEntries]) {
            if ([[entry objectType] isEqualToString:@"vm"]){
                if ([[entry propertyDescription] isEqualToString:@"memory_internal_free"]){
                    [resultsFreeMemory setValue:[entry values] forKey:[entry objectId]]; 
                }
                if ([[entry propertyDescription] isEqualToString:@"memory"]){
                    [resultsTotalMemory setValue:[entry values] forKey:[entry objectId]];                
                }
            }
        }
    }
    
    // create one new disctionary with the %usage
    NSMutableArray* results = [[NSMutableArray alloc] init];
    NSArray* allkeys = [resultsTotalMemory allKeys];
    for (NSString* key in allkeys) {
        // get all values for both dictionaries
        // build an array of free/total
        // calculate average and store as value
        NSArray* freeArray = [resultsFreeMemory valueForKey:key];
        NSArray* totalArray = [resultsTotalMemory valueForKey:key];
        NSMutableArray* percentArray = [[NSMutableArray alloc] init];
        for (int i=0; i<[freeArray count]; i++) {
            double free = [[freeArray objectAtIndex:i] doubleValue]; 
            double total = [[totalArray objectAtIndex:i] doubleValue];
            double value = 0;
            if (total != 0){
                value = 1 - (free * 1024 / total);   // free is Kb the other is bytes grrr
            }
            [percentArray addObject:[[NSNumber alloc] initWithDouble:value]];
        }
        
        double transactionAverage=[[percentArray valueForKeyPath:@"@avg.doubleValue"] doubleValue]; 
        
        ValueObject* o = [[ValueObject alloc] initWithUUID:key andDoubleValue:transactionAverage];
        [results addObject:o];
    }
    
    // take the top 3 that resolve to vm (ids)
    // it is possible that some of the entires no longer exist
    // can I sort the results by the values, then go though find the top 3 that resolve
    
    NSMutableDictionary* finalResults = [[NSMutableDictionary alloc] init];
    
    if (results != nil && [results count] >0){
        [results sortUsingDescriptors:
         [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"doubleValue" ascending:NO]]];NSUInteger counter = 0;NSUInteger allowedCounter = 0;
        while (allowedCounter <3 && counter< [results count]){
            NSString* uuid = [[results objectAtIndex:counter] uuid];                
            double value = [[results objectAtIndex:counter] doubleValue];
            NSArray *vms = [[self XenHypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseForUUID:uuid]];
                
            if (vms != nil && [vms count] >0){
                XenVM *vm = [vms objectAtIndex:0];
                    
                [finalResults setValue:[NSNumber numberWithDouble:value] forKey:[vm name_label]];
                allowedCounter ++;
            }
            counter ++;
        }
    }
 
    [self DisplayResults:finalResults]; 
}


@end


