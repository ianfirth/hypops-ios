//
//  RefreshableKeyMetricPanel.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenHypervisorConnection.h"
#import "LevelIndicator.h"
#import "IndicatorLight.h"

@interface RefreshableKeyMetricPanelController : UIViewController{
    IBOutlet __weak UILabel* descriptionLabel;
    IBOutlet __weak UIImageView* imageView;
    
    IBOutlet __weak UILabel* textItem1;
    IBOutlet __weak LevelIndicator*  levelIndicator1;
    IBOutlet __weak UILabel*  textItem2;
    IBOutlet __weak LevelIndicator*  levelIndicator2;
    IBOutlet __weak UILabel*  textItem3;
    IBOutlet __weak LevelIndicator*  levelIndicator3;
    
    // lights up with the same color as the worst overall level
    IBOutlet __weak IndicatorLight* overallIndication;
    
    NSString* connectionID;
}

-(void) setHypervisorConnection:(XenHypervisorConnection*)connection;
-(void) updateDisplayFromDatabase:(NSArray*)databases;
-(void) updateDisplay;

@property (readonly, weak) XenHypervisorConnection* XenHypervisorConnection;

- (NSArray*) SortedKeysFor: (NSMutableDictionary*) finalResults ;

- (void) DisplayResults: (NSMutableDictionary*) finalResults ;

@end
