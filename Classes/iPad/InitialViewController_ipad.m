//
//  InitialViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "InitialViewController_ipad.h"

@implementation InitialViewController_ipad

@synthesize rootController, createConnectionButton;

#pragma mark - Managing the detail item


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

#pragma mark - Object insertion

- (IBAction)insertNewObject:(id)sender
{
    // get the absolute location of the button from the origin of the screen
    CGPoint point = [createConnectionButton convertPoint:createConnectionButton.frame.origin fromView:nil];

    CGRect finalFrame;
    
    if (UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation])){
       finalFrame = CGRectMake(abs(point.x), abs(point.y),createConnectionButton.frame.size.width,createConnectionButton.frame.size.height);
    }
    else{
       finalFrame = CGRectMake(abs(point.y), abs(point.x),createConnectionButton.frame.size.width,createConnectionButton.frame.size.height);
    }

    [rootController insertNewObjectusingDisplayRect:finalFrame];
}

#pragma mark -
/**
 * linked to the label that displays the web site address
 */
- (IBAction)touchUpInside:(id)sender{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"http://www.hypops.org.uk" ];
    [[UIApplication sharedApplication] openURL:url];
}


@end
