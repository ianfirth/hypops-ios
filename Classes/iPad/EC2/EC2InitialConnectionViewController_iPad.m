//
//  EC2InitialConnectionViewController_iPad.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2InitialConnectionViewController_iPad.h"


@implementation EC2InitialConnectionViewController_iPad

@synthesize viewTitleLabel, viewTitleImageButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil hypConnectionType:(HypervisorType) hypervisorType
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        theHypervisorType = hypervisorType;
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewWillAppear:(BOOL)animated{
    // USE EITHER AN AMAzon or cloudstack string
    NSString* name = @"Unknown";
    NSString* imageName = nil;
    if (theHypervisorType == HYPERVISOR_EC2){
        name = @"Amazon EC2 Connection";
        imageName = @"AmazonEC2Logo";
    }
    if (theHypervisorType == HYPERVISOR_CLOUDSTACK_AWS){
        name = @"CloudStack Bridge Connection";
        imageName = @"CloudStackLogo";
    }

    [[self viewTitleLabel] setText:name];
    [[self viewTitleImageButton] setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [super viewWillAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

/**
 * linked to the label that displays the web site address
 * see http://aws.amazon.com/trademark-guidelines/ for details
 */
- (IBAction)touchUpInside:(id)sender{
    NSString* urlString = nil;
    if (theHypervisorType == HYPERVISOR_EC2){
        urlString = @"http://aws.amazon.com/ec2";
    }
    if (theHypervisorType == HYPERVISOR_CLOUDSTACK_AWS){
        urlString = @"http://cloudstack.org/";
    }

    NSURL *url = [ [ NSURL alloc ] initWithString: urlString ];
    [[UIApplication sharedApplication] openURL:url];
}

@end
