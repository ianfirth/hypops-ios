//
//  EC2regionViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#define statusURL @"http://status.aws.amazon.com/"

#import "EC2RegionDetailViewController.h"


@implementation EC2RegionDetailViewController
@synthesize webView, staticView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

/**
 * linked to the label that displays the web site address
 * see http://aws.amazon.com/trademark-guidelines/ for details
 */
- (IBAction)touchUpInside:(id)sender{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"http://aws.amazon.com/ec2" ];
    [[UIApplication sharedApplication] openURL:url];
}

#pragma mark - UIWenViewDelegate
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    // if failed hide the WebView and display the alternate page.
    if (progressAlert){
        [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
        progressAlert = nil;
    }
    [[self webView] setHidden:YES];
    [[self staticView] setHidden:NO];
    [self setView:[self staticView]];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView{
    [[self webView] setHidden:NO];
    [[self staticView] setHidden:YES];
    [self setView:[self webView]];
    if (progressAlert){
        [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
        progressAlert = nil;
    }
}

// stop the browser navigating to any other pages.
// only allows the status page to be displayed
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if ([[[request URL] description] isEqualToString:statusURL]){
        return YES;
    }
    else
    {
       // open the URL in an external browser not in the app
       [[UIApplication sharedApplication] openURL:[request URL]];
    }
    return NO;
}

#pragma mark - View lifecycle

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    progressAlert = [[ProgressAlert alloc] initWithTitle:@"Loading" delegate:self];
    [progressAlert show];
    NSURL *url = [NSURL URLWithString:statusURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [[self webView] loadRequest:request];  
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[self webView] stopLoading];
    if (progressAlert){
        [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
        progressAlert = nil;
    }

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

-(void) dealloc{
    [[self webView] stopLoading];
    if (progressAlert){
        progressAlert = nil;
    }
}
@end
