//
//  RRDDisplayButton.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "RRDDisplayButton.h"
#import "ImageBuilder.h"
#import "ProgressAlert.h"
#import "DataSource.h"
#import "RRDPlotViewController.h"
#import "CommonUIStaticHelper.h"

@interface RRDDisplayButton (Private)
    - (void)didPresentAlertView:(UIAlertView *)alertView;
    -(void)graphButtonPressed:(id)sender;
    -(void)resetButtonPressed:(id)sender;
    -(void)displayGraph;
    -(void)reloadGraph;
    -(void) setUpdateDate:(NSDate*)date; 
@end
@implementation RRDDisplayButton

@synthesize delegate;
@synthesize reference;
@synthesize alertView;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

-(void) didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    if (graphPopover){
        [graphPopover dismissPopoverAnimated:NO];
    }
}


-(void) viewDidLoad{
    [super viewDidLoad];
    graphButton = [[UIButton alloc] init];
    [graphButton setImage:[ImageBuilder graphImage] forState:UIControlStateNormal];
    refreshButton = [[UIButton alloc] init];
    [refreshButton setImage:[UIImage imageNamed:@"Refresh"] forState:UIControlStateNormal];
    mainTitleLabel= [[UILabel alloc] init];
    [mainTitleLabel setText:@"View Performance Data"];
    [mainTitleLabel setFont:[UIFont fontWithName:@"Helvetica Bold" size:14.0]];
    
    [mainTitleLabel setTextColor:[UIColor blackColor]];
    updateTimeLabel = [[UILabel alloc] init];
    [updateTimeLabel setText:@""];
    [updateTimeLabel setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
    
    backgroundButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backgroundButton addTarget:self 
             action:@selector(graphButtonPressed:)
            forControlEvents:UIControlEventTouchDown];

    [[self view] addSubview:backgroundButton];
    [backgroundButton addSubview:graphButton];
    [backgroundButton addSubview:mainTitleLabel];
    [backgroundButton addSubview:updateTimeLabel];
    [backgroundButton addSubview:refreshButton];
    [backgroundButton setBackgroundColor:[UIColor clearColor]];
    [graphButton addTarget:self 
                    action:@selector(graphButtonPressed:)
          forControlEvents:UIControlEventTouchDown];

    [refreshButton addTarget:self 
                    action:@selector(refreshButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // set the button locations here  
    CGRect bounds = [[self view] bounds];
    [backgroundButton setFrame:bounds];
    CGRect graphButtonRect = CGRectMake(bounds.origin.x+2,
                                        bounds.origin.y+2, 
                                        40,40);
    
    [graphButton setFrame:graphButtonRect];
    [refreshButton setFrame:CGRectMake(bounds.origin.x + (bounds.size.width)-42,2,40,40)];
    CGRect mainTitleLabelRect = CGRectMake(graphButton.bounds.origin.x + graphButton.bounds.size.width + 5,
                                           bounds.origin.y+2, 
                                           bounds.size.width-refreshButton.bounds.size.width - graphButton.bounds.size.width -10,25);
    CGRect updateTimeLabelRect = CGRectMake(graphButton.bounds.origin.x + graphButton.bounds.size.width + 5,
                                            bounds.origin.y+25, 
                                            bounds.size.width-refreshButton.bounds.size.width - graphButton.bounds.size.width -10,15);

    [mainTitleLabel setFrame:mainTitleLabelRect];
    [updateTimeLabel setFrame:updateTimeLabelRect];
    [backgroundButton setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [backgroundButton setAutoresizesSubviews:YES];

    [refreshButton setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];
    [mainTitleLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin];
    [mainTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [updateTimeLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin];
    [updateTimeLabel setTextAlignment:NSTextAlignmentCenter];
    [self setUpdateDate:[[self delegate] getLastRefreshTime]];
}

-(void) setUpdateDate:(NSDate*)date{
    if (date){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        // toDO improve this so it says today if today, yesterday otherwise date?
        [dateFormatter setDateFormat:@"dd MMM yyyy (HH:mm)"];
        NSString* dateAsString = [dateFormatter stringFromDate:date];
        [updateTimeLabel setText: dateAsString] ;
    }
    else
    {
        [updateTimeLabel setText: @""] ;
    }
}

// method to display the graph.
// Shows a progress loading dialog then
// the event for the loading dialog being complete actually adds the graph to the display.
-(void)graphButtonPressed:(id)sender{
    ProgressAlert *progressAlert = [[ProgressAlert alloc] initWithTitle:@"Loading" delegate:self];
    [self setAlertView:progressAlert];
    [[self alertView] setTag:100];
    [[self alertView] show];
}

-(void)refreshButtonPressed:(id)sender{
    ProgressAlert *progressAlert = [[ProgressAlert alloc] initWithTitle:@"Loading" delegate:self];
    [self setAlertView:progressAlert];
    [[self alertView] setTag:101];
    [[self alertView] show];
}

#pragma mark -
#pragma mark ProgressAlert delegate

- (void)didPresentAlertView:(UIAlertView *)theAlertView{
    // if a loading alert then do this otherwise so nothing
    
    BOOL result = YES;
    switch ([theAlertView tag]) {
        case 100:
            [self performSelector:@selector(displayGraph) withObject:nil afterDelay:0.1];
            break;
        case 101:
            [self performSelector:@selector(reloadGraph) withObject:nil afterDelay:0.1];
            break;
        default:
            break;
    }
}

-(void)displayError{
    UIAlertView *alertViewWarning = [[UIAlertView alloc] initWithTitle:@"Data Load Failed"
                                                               message:@"The Performance data failed to load.  Check that the hyperviosr host is still contactable and try again."
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                     otherButtonTitles:@"OK", nil];
    [alertViewWarning setTag:200];
    [self setAlertView:alertViewWarning];
    [[self alertView] show];
}

-(void)reloadGraph{
    if ([self alertView]){
        [[self alertView] dismissWithClickedButtonIndex:0 animated:YES];
        [self setAlertView: nil];
    }
    BOOL result = [[self delegate] reloadRoundRobinDatabase];
    [self setUpdateDate:[[self delegate] getLastRefreshTime]];
    
    if (!result){
        if ([self alertView]){
            [[self alertView] dismissWithClickedButtonIndex:0 animated:NO];
            [self setAlertView: nil];
        }
        
        // think this needs to be displayed after a delay as otherwise it will not cancel (as the existing alert will close this one
        // display the eror dialog
        [self performSelector:@selector(displayError) withObject:nil afterDelay:0.5];
    }
}

-(void)displayGraph{
    if ([self alertView]){
        [[self alertView] dismissWithClickedButtonIndex:0 animated:YES];
        [self setAlertView: nil];
    }
    RoundRobinDatabase* rrd = [[self delegate] roundRobinDatabase];
    if (rrd){
                        
        RRDPlotViewController* plotView = [[RRDPlotViewController alloc] initWithRRDDsiplayButtonDelegate:[self delegate] withReference:[self reference]];
        
        UIDevice *device = [UIDevice currentDevice];
        if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
            // set a size that fits in the window
            [plotView setPreferredContentSize:CGSizeMake(1000, 400)];
            graphPopover = [[UIPopoverController alloc] initWithContentViewController:plotView];
            [graphPopover setDelegate:self];
            // permitted arrow direction 0 makes the popover appear without any arrows :)
            [graphPopover presentPopoverFromRect:CGRectMake(0,0,10,10) inView:[self view] permittedArrowDirections:0 animated:YES];
        }
        else{
            [CommonUIStaticHelper displayNextView:plotView usingNavigationController:[self navigationController] andRootViewController:[[[self navigationController] viewControllers]objectAtIndex:0]];
        }
    }
    [self setUpdateDate:[[self delegate] getLastRefreshTime]];
    
    bool result = (rrd != nil);
    
    if (!result){
        if ([self alertView]){
            [[self alertView] dismissWithClickedButtonIndex:0 animated:NO];
            [self setAlertView: nil];
        }
        // think this needs to be displayed after a delay as otherwise it will not cancel (as the existing alert will close this one
        // display the eror dialog
        [self performSelector:@selector(displayError) withObject:nil afterDelay:0.5];
    }
}

#pragma mark - 
#pragma mark UIPopoverControllerDelegate
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    if ([self alertView]){
        [[self alertView] dismissWithClickedButtonIndex:0 animated:YES];
        [self setAlertView: nil];
    }
    popoverController = nil;
}
@end
