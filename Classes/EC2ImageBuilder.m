//
//  EC2ImageBuilder.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2ImageBuilder.h"
#import "EC2InstanceWrapper.h"
#import "ImageBuilder.h"

/* This is a collection of UI static methods that provide images for the Amazon EC2 Cloud
 */
@implementation EC2ImageBuilder


// bulild an image that represents the current VM with overlayed power state icon
+ (UIImage* )buildInstanceImageForInstancePowerState:(EC2PowerState)powerState{
    UIImage *vmImage = nil;
    switch (powerState) {
        case EC2POWERSTATE_PENDING:
            vmImage = [ImageBuilder vMImageStarted];
            break;
        case EC2POWERSTATE_RUNNING:
            vmImage = [ImageBuilder vMImageStarted];
            break;
        case EC2POWERSTATE_SHUTTINGDOWN:
            vmImage = [ImageBuilder vMImageStopped];
            break;
        case EC2POWERSTATE_TERMINATED:
            vmImage = [ImageBuilder vMImageTerminated];
            break;
        case EC2POWERSTATE_STOPPING:
            vmImage = [ImageBuilder vMImageStopped];
            break;
        case EC2POWERSTATE_STOPPED:
            vmImage = [ImageBuilder vMImageStopped];
            break;
    }    
    return vmImage;
}

+ (UIImage*) buildInstanceImageForInstance:(EC2InstanceWrapper *)ec2Instance{
    return [EC2ImageBuilder buildInstanceImageForInstancePowerState:[ec2Instance powerState]];
}

@end
