//
//  ConnectionsViewController_ipad.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ConnectionsViewController_ipad.h"

@implementation ConnectionsViewController_ipad

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // setup the save and cancel buttons   
    UIBarButtonItem *cancelButtonBarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(doCancel)];
    UIBarButtonItem *saveButtonBarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveEdit)];
    
    self.navigationItem.leftBarButtonItem = cancelButtonBarItem;
    self.navigationItem.rightBarButtonItem = saveButtonBarItem;
   
}

-(void) close{
    // do nothing here
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction) doSave {
    NSLog(@"save selected");
    [self saveEdit];

}

- (IBAction) doCancel {
    NSLog(@"cancel selected");
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
