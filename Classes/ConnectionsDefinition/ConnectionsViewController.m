//
//  ConnectionsViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ConnectionsViewController.h"
#import "HypervisorConnectionFactory.h"
#import "ServerDetail.h"
#import "ImageBuilder.h"

@interface ConnectionsViewController (Private)
   - (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end;

@implementation ConnectionsViewController

@synthesize fetchedResultsController=fetchedResultsController;
@synthesize objectToEdit;
@synthesize editMode;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resultsController:(NSFetchedResultsController *)resultsController  connectionDefinition:(ConnectionDefinition *)connection
   connectionDelegate:(NSObject<ConnectionDefintionDelegate>*) delegate{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        // Custom initialization.
        fetchedResultsController = resultsController;
        connectionDefinition = connection;
        connectionDefintionDelegate = delegate;
    }
    return self;
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [connectionDefintionDelegate numberOfProperties] +1;  // add in the email cell
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
       if ([[[tableView cellForRowAtIndexPath:indexPath] reuseIdentifier] isEqualToString:@"EmailCell"]){
           NSLog(@" row selected = %ld with section %ld", (long)indexPath.row, (long)indexPath.section);
           MFMailComposeViewController *composer = [[MFMailComposeViewController alloc] init];
           [composer setMailComposeDelegate:self];
           if ([MFMailComposeViewController canSendMail]) {
               [composer setToRecipients:[NSArray arrayWithObjects:@"Me", nil]];
               [composer setSubject:[NSString stringWithFormat:@"hypOps Connection to %@",[connectionDefintionDelegate connectionName]]];

               NSString* appStroeURl = @"http://itunes.apple.com/us/app/hypops/id438105824";
               
               NSString* hypervisorTypeString = nil;
               
               switch ([connectionDefinition hypervisorType]) {
                   case HYPERVISOR_XEN:
                       hypervisorTypeString = @"XenServer";
                       break;
                   case HYPERVISOR_EC2:
                       hypervisorTypeString = @"AmazonEC2";
                       break;
                   case HYPERVISOR_CLOUDSTACK_NATIVE:
                       hypervisorTypeString = @"CloudStack";
                       break;
                   default:
                       hypervisorTypeString = @"unknown";
                       break;
               }

               NSString* encodedName = [[connectionDefintionDelegate connectionName] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
               NSString* encodedAddress = [[connectionDefintionDelegate connectionAddress] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
               NSString* encodedUsername = [[connectionDefintionDelegate connectionUsername]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
               NSString* encodedHypTypeStr = [hypervisorTypeString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
               NSString* encodedPassword = [[connectionDefintionDelegate connectionPassword]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
               
               NSString* params = [NSString stringWithFormat:@"name=%@&address=%@&username=%@&type=%@&password=%@",encodedName, encodedAddress, encodedUsername, encodedHypTypeStr,encodedPassword];

               NSString* newConnectionString = [NSString stringWithFormat:@"hypops:\\\\newConnection?%@",params];
               NSString* launchConnectionString = [NSString stringWithFormat:@"hypops:\\\\launchConnection?%@",params];
               NSString* messageBody = [NSString stringWithFormat:@"You can manage my %@ virtualization environment using hypOps.</br></br> <a href=\"%@\">Click here</a> to <b>add the connection</b> to your hypOps environment or </br> <a href=\"%@\">Click here</a> to <b>manage the virtualization environment</b> without adding it to your connection list. </br></br> If you don't have hypOps yet <a href=\"%@\">click here</a> to go to the app store to install it.",
                                        hypervisorTypeString, newConnectionString,launchConnectionString,appStroeURl];
             
               [composer setMessageBody:messageBody isHTML:YES];

               [self presentViewController:composer animated:YES completion:nil];
           }
           else {
               
               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Can't send your email!" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
               [alert show];
               
           }
           // make self listen to send etc.. and hide the client if finished
           // stick url into subject
      }
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"Cell";
    
    if (indexPath.row == [connectionDefintionDelegate numberOfProperties])
    {
       CellIdentifier = @"EmailCell";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        if  ([CellIdentifier isEqualToString:@"EmailCell"]){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }else{
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
     
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([[cell reuseIdentifier] isEqualToString:@"EmailCell"]){
        // this is the email button
        [[cell imageView] setImage:[ImageBuilder shareImage]];
        [[cell textLabel] setText:@"Send Email for Connection"];
        [cell setUserInteractionEnabled:YES];
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        //[cell setHighlighted:YES];
        
    }else{
        NSString *displayText = [connectionDefintionDelegate titleForIndex:(NSInteger)indexPath.row];
        UIView *requiredView = [connectionDefintionDelegate viewForIndex:(NSInteger)indexPath.row];
        
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:16]};
        CGSize labelSize = [displayText sizeWithAttributes:attributes];
        float x = labelSize.width;
        float y = ([cell frame].size.height - labelSize.height)/2;
        // create the label
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10,y,[cell frame].size.width - (x+10),labelSize.height)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:16];
        label.textColor = [UIColor darkGrayColor];
        [label setText:displayText];
        [[cell contentView] addSubview:label];
        
        // set the value component
        float offset = x + 20;
        [requiredView setFrame:CGRectMake(offset,10,[cell frame].size.width - offset - 5,30)];
        if ([requiredView isKindOfClass:[UITextField class]]){
            UITextField *tf = (UITextField *)requiredView;
            tf.returnKeyType = UIReturnKeyDone;
            [tf setDelegate :self];
        }
        [[cell contentView] addSubview:requiredView];
    }
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    // dismiss the controller regardless of if the mial was sent or not
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    // close the keyboard
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark view actions

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}

/*
 This is called when the view is hidden
 Is is used to clear out the values in the textFields so that when ever it is next used the values are clear
 */
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

/*
 This is called when the view is displayed
 Is is used to initialize the values in the textFields
 */
- (void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    if (connectionDefinition){
        self.title = connectionDefinition.connectionString;
    }

    if (editMode == MODE_EDIT) {
        NSManagedObject *selectedObject = [self.fetchedResultsController objectAtIndexPath:objectToEdit];
        [connectionDefintionDelegate populateFields:selectedObject];
    }
}


// Override to allow orientabtions other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



#pragma mark -
#pragma mark edit actions

/*
 This action is called from the save button bar item
 */
- (void)saveEdit {
    // save and quit the view
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];

    ServerDetail* serverDetail;

    // if in add mode add a new entry otherwsie update the existing one.
    if (self.editMode == MODE_ADD){
        // Create a new instance of the entity managed by the fetched results controller.
        NSEntityDescription *serverDetailEntity = [NSEntityDescription entityForName:@"ServerDetail"
                                                      inManagedObjectContext:context];
        
        serverDetail = [[ServerDetail alloc] initWithEntity:serverDetailEntity
                                           insertIntoManagedObjectContext:context];
        [serverDetail setConnectionCount:0];
        [serverDetail setLastConnectionTime:[NSDate date]];

    } else if (self.editMode == MODE_EDIT){
        serverDetail = [[self fetchedResultsController] objectAtIndexPath:objectToEdit];
    }
    
    // if for any reason the serverDetail is nil then dont try to save it
    if (serverDetail){
       [connectionDefintionDelegate saveDataToManagedObject:serverDetail];
    }
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
         NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    } 

    // close the view
    [self close];
}
@end
