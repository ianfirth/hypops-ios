//
//  ConnectionsViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "GradientView.h"
#import "ShadowView.h"
#import "HypervisorConnectionFactory.h"
#import <MessageUI/MFMailComposeViewController.h>

typedef enum
{
    MODE_ADD,
    MODE_EDIT
} EditMode;

@interface ConnectionsViewController : UITableViewController <UITextFieldDelegate,MFMailComposeViewControllerDelegate> {
    NSFetchedResultsController *fetchedResultsController;
    EditMode editMode;
    NSIndexPath *objectToEdit;
    ConnectionDefinition *connectionDefinition;
    NSObject<ConnectionDefintionDelegate>* connectionDefintionDelegate;
    }

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSIndexPath *objectToEdit;
@property (readwrite) EditMode editMode;

// initializer that allows the FetchedRusultsController to be stated
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil resultsController:(NSFetchedResultsController *)resultsController  connectionDefinition:(ConnectionDefinition *)connection
   connectionDelegate:(NSObject<ConnectionDefintionDelegate>*) delegate;

- (void)saveEdit;

@end

#pragma mark -

@interface ConnectionsViewController (AbstractMethods)
// Define Abstract methods which should be implemented by subclasses:
- (void)close;

@end
