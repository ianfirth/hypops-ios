//
//  EC2HypervisorConnectionDefinition.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2HypervisorConnectionDefinition.h"

@implementation EC2HypervisorConnectionDefinition

@synthesize address,secure,userName,password,name;

-(id) init{
    self = [super init];
    if (self){
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(110, 10, 195, 30)];
        [self setUserName:textField ];
        [self userName].autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self userName].adjustsFontSizeToFitWidth = YES;
        [self userName].textColor = [UIColor blackColor];
        [self userName].placeholder = @"AccessID";
        [self userName].keyboardType = UIKeyboardTypeAlphabet;
        [self userName].secureTextEntry = NO;
        [self userName].backgroundColor = [UIColor whiteColor];
        [self userName].autocorrectionType = UITextAutocorrectionTypeNo; 
        [self userName].autocapitalizationType = UITextAutocapitalizationTypeNone; 
        [self userName].textAlignment = NSTextAlignmentLeft;
        [self userName].tag = 0;
        [self userName].clearButtonMode = UITextFieldViewModeWhileEditing; 
        [[self userName] setEnabled: YES];
        
        textField = [[UITextField alloc] initWithFrame:CGRectMake(110, 10, 195, 30)];
        [self setPassword: textField];
        [self password].autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self password].adjustsFontSizeToFitWidth = YES;
        [self password].textColor = [UIColor blackColor];
        [self password].placeholder = @"SecureKey";
        [self password].keyboardType = UIKeyboardTypeAlphabet;
        [self password].secureTextEntry = YES;
        [self password].backgroundColor = [UIColor whiteColor];
        [self password].autocorrectionType = UITextAutocorrectionTypeNo; 
        [self password].autocapitalizationType = UITextAutocapitalizationTypeNone; 
        [self password].textAlignment = NSTextAlignmentLeft;
        [self password].tag = 0;
        [self password].clearButtonMode = UITextFieldViewModeWhileEditing; 
        [[self password] setEnabled: YES];
        
        textField = [[UITextField alloc] initWithFrame:CGRectMake(140, 10, 165, 30)];
        [self setName: textField ];
        [self name].autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self name].adjustsFontSizeToFitWidth = YES;
        [self name].textColor = [UIColor blackColor];
        [self name].placeholder = @"Example Connection";
        [self name].keyboardType = UIKeyboardTypeAlphabet;
        [self name].secureTextEntry = NO;
        [self name].backgroundColor = [UIColor whiteColor];
        [self name].autocorrectionType = UITextAutocorrectionTypeYes; 
        [self name].autocapitalizationType = UITextAutocapitalizationTypeSentences;
        [self name].textAlignment = NSTextAlignmentLeft;
        [self name].tag = 0;
        [self name].clearButtonMode = UITextFieldViewModeWhileEditing; 
        [[self name] setEnabled: YES];
    }
    return self;
}

- (NSInteger)numberOfProperties{
    return 3;
}

-(NSString*)connectionName{
    return name.text;
}

// amazon never has an address defined.
- (NSString *) connectionAddress{
    return nil;
}
- (NSString *) connectionUsername{
    return userName.text;
}
- (NSString *) connectionPassword{
    return password.text;
}

-(NSString *) titleForIndex:(NSInteger)index{
    switch (index) {
        case 0:
            return @"AccessID";
            break;
        case 1:
            return @"SecureKey";
            break;
        case 2:
            return @"Display Name";
            break;
        default:
            return nil;
            break;
    } 
}

-(UIView *) viewForIndex:(NSInteger)index{
    switch (index) {
        case 0:
            return userName;
            break;
        case 1:
            return password;
            break;
        case 2:
            return name;
            break;
        default:
            return nil;
            break;
    }
}

- (void)populateFields:(NSManagedObject *) fieldData{
    
    password.text = [fieldData valueForKey:@"password"];
    userName.text = [fieldData valueForKey:@"username"];
    name.text = [fieldData valueForKey:@"name"];
}

-(void) saveDataToManagedObject:(NSManagedObject *) managedObject{
    
    // If appropriate, configure the new managed object.
    if ([name text] == nil || [[name text] isEqualToString: @""]){
        [name setText:@"Amazon EC2 Connection"];
    }
    
    [managedObject setValue:[userName text] forKey:@"username"];
    [managedObject setValue:[password text] forKey:@"password"];
    [managedObject setValue:[name text] forKey:@"name"];
    
    // put hypervisor number into the type field now
    [managedObject setValue:[NSString stringWithFormat:@"%i",HYPERVISOR_EC2] forKey:@"type"];
    // can add metadata to these at a later point if required.
    // the datamodel supports metadata for this purpose.
}


@end

