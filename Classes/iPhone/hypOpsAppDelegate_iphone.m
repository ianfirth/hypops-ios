//
//  hypOpsAppDelegate.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "HypOpsAppDelegate_iphone.h"
#import "IOS6UINavigationController.h"

@implementation HypOpsAppDelegate_iphone

@synthesize window;
@synthesize navigationController;

#pragma mark -
#pragma mark Application lifecycle

- (void)awakeFromNib {    
    // this is the iPhone version
    [self setRootViewController: (RootViewController *)[navigationController topViewController]];
    [[self rootViewController] setManagedObjectContext: self.managedObjectContext];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [self.window addSubview:[[self navigationController] view]];
    [self.window makeKeyAndVisible];
    return YES;
}



@end

