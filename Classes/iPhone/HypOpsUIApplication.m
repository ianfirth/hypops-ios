//
//  HypOpsUIApplication.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Note: this file is excluded for ARC because of the requirement to cast id to int* for the
// use of GS Event.

#import "HypOpsUIApplication.h"

@implementation HypOpsUIApplication

/******
 * As far as we can tell the structures look like this
 * details originated from http://stackoverflow.com/questions/1267560/iphone-keyboard-event/8862814#8862814
 *****
typedef struct __GSEvent {
    CFRuntimeBase _base;  // 8 bytes
    GSEventRecord record; // the eventrecord described below
} GSEvent;
 
// ------- GSEvent base offset = 8 so start at = 8 as it is part of that structure
 
 (is address from begining of whole event structure, this one starts at 8 (when part of a GS Event)
typedef struct __GSEvent* GSEventRef;
typedef struct GSEventRecord {
    GSEventType type;          // (0x08) 4 bytes
    GSEventSubType subtype;    // (0x0c) 4 bytes
    CGPoint location;          // (0x10) 8 bytes
    CGPoint windowLocation;    // (0x18) 8 BytesNSUInteger windowContextId;       // (0x20) 4 bytes
    uint64_t timestamp;        // (0x24) 8 bytes
    GSWindowRef window;        // (0x2c) 4 bytes
    GSEventFlags flags;        // (0x30) 4 bytes
    unsigned senderPID;        // (0x34) 4 bytes
    CFIndex infoSize;          // (0x38) 4 bytes
} GSEventRecord;
 
typedef struct GSEventKey {
    GSEvent _super;            // consumes 0x3C bytes
    UniChar keycode,           // (0x3c) 2 bytes
    characterIgnoringModifier, // (0x3e) 2 bytes
    character;                 // (0x40) 2 bytes
    short characterSet;        // (0x42) 2 bytes
    Boolean isKeyRepeating;    // (0x44) ?
} GSEventKey;
*/


// offsets for date in the GSEvent struct
#define GSEVENT_TYPE 0x8
#define GSEVENT_SUBTYPE 0x0c
#define GSEVENT_FLAGS 0x30

// offsets for date in the GSEventKey struct
#define GSEVENTKEY_KEYCODE 0x3C
#define GSEVENTKEY_CHARCODE 0x40

// event types
#define GSEVENT_TYPE_KEYDOWN 0x0a
#define GSEVENT_TYPE_KEYUP 0x0b
#define GSEVENT_TYPE_KEYMODIFIER 0x0c  // modifier key that does anything on its own (i.e. shift)


- (void)sendEvent:(UIEvent *)event
{
    [super sendEvent:event];
    
    // only for ios 5.  GSEvent structure might be different in later versions
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber * versionNumber = [f numberFromString:[[UIDevice currentDevice] systemVersion]];
    
    if (versionNumber && !([versionNumber doubleValue] > 7)) {
       // Key events come in form of UIInternalEvents.
       // They contain a GSEvent object which contains (12 bytes in)
       // a GSEventRecord among other things
       // use this to get round store submission --- NSSelectorFromString
        
        int* pUIInternalEvent = ( void *)event;NSUInteger pGSEvent = pUIInternalEvent[3];
        
        // this uses a private API so Apple will not allow it
        // SEL selector = @selector(_gsEvent);
        // if ([event respondsToSelector:selector]) {
        //char* eventMem = (char*)[event performSelector:selector];
        // instead just look at the event starting 12 bytes in
        
        char* eventMem = (char *)pGSEvent;
        if (eventMem) {
            // So far we got a GSEvent :)
            // construct anNSUIntegerby reading 4 bytes
            NSUInteger eventType =  [self getIntValueForEvent:eventMem atLoaction:GSEVENT_TYPE];
            if (eventType != 0){
            if (eventType == GSEVENT_TYPE_KEYDOWN) {  // check value
                // Now we know it is a GSEventKey from the type so can use the fields speific to this eventType
                NSUInteger keycode =  [self getShortValueForEvent:eventMem atLoaction:GSEVENTKEY_KEYCODE];
                NSUInteger keychar =  [self getShortValueForEvent:eventMem atLoaction:GSEVENTKEY_CHARCODE];
                NSUInteger flags =  [self getIntValueForEvent:eventMem atLoaction:GSEVENT_FLAGS];
                //NSLog(@"KeyDOWN keycode is %i, keychar is %i",keycode,keychar);
                
                // should add event flags I think too
                // Post notification
                NSDictionary *inf;
                inf = [[NSDictionary alloc] initWithObjectsAndKeys:
                       [NSNumber numberWithInteger:keycode],
                       @"keycode",
                       [NSNumber numberWithInteger:keychar],
                       @"keychar",
                       [NSNumber numberWithInteger:flags],
                       @"eventflags",
                       nil];
                
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:GSEventKeyDownNotification
                 object:self
                 userInfo:inf];
                
                [inf release];
                
            }
            if (eventType == GSEVENT_TYPE_KEYUP) {  // check value
                // Now we know it is a GSEventKey from the type so can use the fields speific to this eventType
                NSUInteger keycode =  [self getShortValueForEvent:eventMem atLoaction:GSEVENTKEY_KEYCODE];
                NSUInteger keychar =  [self getShortValueForEvent:eventMem atLoaction:GSEVENTKEY_CHARCODE];
                NSUInteger flags =  [self getIntValueForEvent:eventMem atLoaction:GSEVENT_FLAGS];
                
                // should add event flags I think too
                // Post notification
                NSDictionary *inf;
                inf = [[NSDictionary alloc] initWithObjectsAndKeys:
                       [NSNumber numberWithInteger:keycode],
                       @"keycode",
                       [NSNumber numberWithInteger:keychar],
                       @"keychar",
                       [NSNumber numberWithInteger:flags],
                       @"eventflags",
                       nil];
                
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:GSEventKeyUpNotification
                 object:self
                 userInfo:inf];
                
                [inf release];
            }
            if (eventType == GSEVENT_TYPE_KEYMODIFIER) {  // check value
                // Now we know it is a GSEventKey from the type so can use the fields speific to this eventType
                NSUInteger keycode =  [self getShortValueForEvent:eventMem atLoaction:GSEVENTKEY_KEYCODE];
                NSUInteger keychar =  [self getShortValueForEvent:eventMem atLoaction:GSEVENTKEY_CHARCODE];
                NSUInteger flags =  [self getIntValueForEvent:eventMem atLoaction:GSEVENT_FLAGS];
                
                // should add event flags I think too
                // Post notification
                NSDictionary *inf;
                inf = [[NSDictionary alloc] initWithObjectsAndKeys:
                       [NSNumber numberWithInteger:keycode],
                       @"keycode",
                       [NSNumber numberWithInteger:keychar],
                       @"keychar",
                       [NSNumber numberWithInteger:flags],
                       @"eventflags",
                       nil];
                
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:GSEventKeyModifierNotification
                 object:self
                 userInfo:inf];
                
                [inf release];
            }
        }
        }
    }
    
    [f release];
}

// convert 4 bytes to an int
-(NSInteger) getIntValueForEvent:(char*)event atLoaction:(NSInteger)offset{NSUInteger result = (event[offset]) +
    (event[offset+1] << 8) +
    (event[offset+2] << 16) +
    (event[offset+3] << 24);
    
    return result;
}

// convert 2 bytes to an int
-(NSInteger) getShortValueForEvent:(char*)event atLoaction:(NSInteger)offset{NSUInteger result = (event[offset]) +
    (event[offset+1] << 8);
    
    return result;
}

@end
