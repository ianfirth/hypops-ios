//
//  WelcomeViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "WelcomeViewController.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (id)init
{
    self = [super initWithNibName:@"WelcomeView" bundle:nil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[[self getStartedButton] titleLabel] setNumberOfLines:2];
    [[[self getStartedButton] titleLabel] setLineBreakMode:NSLineBreakByWordWrapping];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 * linked to the label that displays the web site address
 */
- (IBAction)touchUpInside:(id)sender{
    NSURL *url = [ [ NSURL alloc ] initWithString: @"http://www.hypops.org.uk" ];
    [[UIApplication sharedApplication] openURL:url];
}

@end
