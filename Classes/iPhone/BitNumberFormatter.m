//
//  BitNumberFormatter.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "BitNumberFormatter.h"

@implementation BitNumberFormatter

-(NSString*)stringForObjectValue:(id)obj{
    NSNumber* byteNum = (NSNumber*)obj;
    double bitNum = [byteNum doubleValue] *8;NSUInteger  divisor = 1;
    NSString* postFix = @"M";
    divisor = 1024*1024;
    
    if (bitNum >= 1024*1024*1024){
        postFix = @"G";
        divisor = 1024*1024*1024;
    }
    
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    [formatter setRoundingMode:NSNumberFormatterRoundUp];
    //[formatter setNumberStyle:NSNumberFormatterS];
    [formatter setMaximumSignificantDigits:1];
    [formatter setMinimumIntegerDigits:1];
    [formatter setMaximumFractionDigits:1];
    [formatter setMinimumFractionDigits:1];
    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithDouble:bitNum/divisor]];
    return [NSString stringWithFormat:@"%@%@",numberString,postFix];
}
@end
