//
//  MemoryNumberFormatter.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "MemoryNumberFormatter.h"

@implementation MemoryNumberFormatter
// this will affect the divisor (memory is in kb for Host and not for VM
-(id) initWithBasereference:(NSInteger)ref{
    self = [super init];
    if (self){
        baseRef = ref;
    }
    return self;
    
}

// default to kb i.e. a value of 1 is 1k
-(id)init{
    return [self initWithBasereference:1024];
}

-(NSString*)stringForObjectValue:(id)obj{
    NSNumber* kbnum = (NSNumber*)obj;
    // multiply the number by the base power
    double kb =  (baseRef * [kbnum doubleValue])/1024;
    NSString* postFix = @"K";NSUInteger  divisor = 1024;
    
    if (kb >= 1024){
        postFix = @"M";
        divisor = 1024;
    }
    if (kb >= 1024*1024){
        postFix = @"G";
        divisor = 1024*1024;
    }
    
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    [formatter setRoundingMode:NSNumberFormatterRoundUp];
    [formatter setMaximumSignificantDigits:1];
    [formatter setMinimumIntegerDigits:1];
    [formatter setMaximumFractionDigits:1];
    [formatter setMinimumFractionDigits:1];
    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithDouble:kb/divisor]];
    return [NSString stringWithFormat:@"%@%@",numberString,postFix];
}
@end
