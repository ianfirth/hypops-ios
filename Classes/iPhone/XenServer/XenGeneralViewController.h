//
//  XenGeneralViewController.h
//  hypOps
//
//  Created by Ian Firth on 04/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenDescriptiveBase.h"
#import "TitledDetailViewController.h"
#import "RefreshablePage.h"
#import "Identifyable.h"

/**
 Class to represent a base view for XenVm General details. 
 This consists of an image, the name and description for the object.
 Below this area a view is provided for data specific to the object being displayed.
 */
@interface XenGeneralViewController : TitledDetailViewController {
}

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)xenHypConnection
                 xenDescritiveBase:(XenDescriptiveBase *)xenDescriptiveBase 
                       displayMode:(GeneralDisplayMode)theMode
                           tabName:(NSString *)tabName
                   tabBarImageName:(NSString *)imageName 
                             image:(UIImage *)mainImage 
           extentionViewController:(UIViewController<RefreshablePage,Identifyable> *)theExtentionViewController
                titlePressedSelector:(SEL)titleSelected
selectorOnObject:(id) selctorObject;

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)xenHypConnection
                 xenDescritiveBase:(XenDescriptiveBase *)xenDescriptiveBase
                       displayMode:(GeneralDisplayMode)theMode
                           tabName:(NSString *)tabName
                   tabBarImageName:(NSString *)imageName
                             image:(UIImage *)mainImage
           extentionViewController:(UIViewController<RefreshablePage,Identifyable> *)theExtentionViewController;


@end
