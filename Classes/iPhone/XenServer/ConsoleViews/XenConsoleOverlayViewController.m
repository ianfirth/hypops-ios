//
//  XenVMConsoleOverlayViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenConsoleOverlayViewController.h"

@interface XenConsoleOverlayViewController (){
    __weak id<ConsoleOverlayDelegate> delegate; // dont allow this class to stop the delegate being removed.
    BOOL winKeyOn;
}

@end

@implementation XenConsoleOverlayViewController

@synthesize keyboardButton;
@synthesize fullScreenButton;
@synthesize fullScreenModeEnabled;
@synthesize vncKeyDelegate;

- (id)init
{
    self = [super initWithNibName:@"XenConsoleOverlayView" bundle:nil];
    if (self) {
        // Custom initialization
        fullScreenMode = NO;
        winKeyOn = NO;
    }
    return self;
}

-(void) dealloc{
    delegate = nil;
}

-(void) setDelegate:(id<ConsoleOverlayDelegate>)theDeleate{
    delegate = theDeleate;
}

UISlider* volumeViewSlider;

- (void)viewDidLoad {
    [super viewDidLoad];
    [mediaView setShowsVolumeSlider:NO];
    [mediaView setShowsRouteButton:YES];
    [mediaView setRouteButtonImage:[UIImage imageNamed:@"airplay"] forState:UIControlStateNormal];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // can always redock, just cant make full screen again after if using an external monitor.
    if (!fullScreenMode){
       [[self fullScreenButton] setEnabled:[self fullScreenModeEnabled]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSUInteger)supportedInterfaceOrientations{
           return UIInterfaceOrientationMaskAll;
}

-(BOOL)shouldAutorotate{
    return YES;
}

- (void) setFullScreenMode:(BOOL)fullScreen{
    fullScreenMode = fullScreen;
    if (fullScreen){
        [[self fullScreenButton] setTitle:@"Dock Display" forState:UIControlStateNormal];
    }
    else{
        [[self fullScreenButton] setTitle:@"Full Screen" forState:UIControlStateNormal];        
    }
}

-(void) setKeyboardOn:(BOOL)displayed{
    if (displayed){
        [[self keyboardButton] setTitle:@"Hide Keyboard" forState:UIControlStateNormal];
    }
    else{
        [[self keyboardButton] setTitle:@"Show Keyboard" forState:UIControlStateNormal];
    }
}


#pragma mark button press meassaged
/** 
 * call the delegate using the sender tag value so that the reciever can determine what has been requested
 * and carry out the appropriate action
 */
- (IBAction)touchUpInside:(id)sender{
    if (delegate){
        [delegate optionSelected:[sender tag]];
    }
}


- (IBAction)cursorUp:(id)sender{
    // need a delegate to send keypress here
    [vncKeyDelegate cursorUpKeyPressed];
}

- (IBAction)cursorDown:(id)sender{
    [vncKeyDelegate cursorDownKeyPressed];
}

- (IBAction)cursorLeft:(id)sender{
    [vncKeyDelegate cursorLeftKeyPressed];
}

- (IBAction)cursorRight:(id)sender{
    [vncKeyDelegate cursorRightKeyPressed];
}

- (IBAction)toggleWindowsKey:(id)sender{
    winKeyOn = !winKeyOn;
    if (winKeyOn){
        [vncKeyDelegate pressWindowskey];
        [self performSelector:@selector(doHighlight:) withObject:sender afterDelay:0];
    }
    else{
        [vncKeyDelegate releaseWindowskey];
        [windowsKey setHighlighted:NO];
    }
}

-(void)doHighlight:(id)sender{
    [windowsKey setHighlighted:YES];
}

- (IBAction)escapeKey:(id)sender{
    [vncKeyDelegate escKeyPressed];
}

@end
