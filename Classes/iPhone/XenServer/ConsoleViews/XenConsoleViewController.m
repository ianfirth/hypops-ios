//
//  XenVMConsoleViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenConsoleViewController.h"
#import "HypOpsUIApplication.h"
#import "XenConsoleOverlayViewController.h"
#import "RefreshablePage.h"

#define TAG_FULLSCREENVIEW 1000

@interface XenConsoleViewController () <RefreshablePage>{
    
    UIView* embdeddedParentView;
    
    // Stores the default rendeder for the display
    id<VNCRenderer> defaultRenderer;
    
    // flag to indicate if the view should do anything when appear and disappear event are fired
    // this is because the code to force rotation on edit screens causes fake appear and disappear messages
    // as part of the hack to the other screens.
    BOOL takeActionOnAppear;
    
    UIImageView* imageView;
    
    XenConsoleOverlayViewController* overlayController;
    NSUInteger lastKeyFlgs;
    
    BOOL alt;
    BOOL ctrl;
    
    UITextView* hiddenTextView;
    
    // contain the external display UI controls
    UIWindow* externalScreenWindow;
    UIScreen* externalScreen;
    UIView<VNCRenderer>* externalView;
    
    UITapGestureRecognizer *singleFingerTap;
    UILongPressGestureRecognizer *longPressOnScreen;
    UITapGestureRecognizer* multiFingerTap;
    UITapGestureRecognizer *doubleTapGestureRecognizer;
    UIPinchGestureRecognizer *pinchRecognizer;
    UIPanGestureRecognizer *panRegognizer;
 
    // these are used for the touch interactions on the view
    BOOL leftMouseDown;
    BOOL mouseMoving;
    float scale;
    float firstScale;
    float firstX,firstY;
    bool pinching,panning;
    CGAffineTransform baseTransform;
    bool applyBaseTransform;
    
    bool fullScreeenMode;
}

/**
 * translate the location of local mouse to remote mouse location
 */
-(CGPoint)getScaledTouchAtX:(float)x andY:(float)y withinView:(UIView*)theView;

/**
 * convert the data in the event queue to the symcode to send over the VNC conneciton
 */
- (NSInteger) symCodeForEvent:(NSDictionary*)userInfo;

/**
 * make sure that the view is rotated to match the device rotation
 */
- (void)InitializeFullScreenTransformforView:(UIView*)theView;
- (void) InitializeFullScreenTransformforView:(UIView*)theView animated:(bool)animated;

/**
 * send an alt-ctrl-delete message to the server
 */
- (void) sendAltCtrlDelete;

/**
 * configure the view so that the onscreen keyboard behaves as expected
 */
-(void) configureOnScreenKeyboard;

@end

@implementation XenConsoleViewController
@synthesize xenVMRef, hypConnID;

-(id) initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection {
    self = [super init];
    if (self){
        [self setHypConnID: [hypervisorConnection connectionID]];;
        NeedsNewConnection = YES;
        mouseMoving = NO;
        takeActionOnAppear = YES;
        imageView = nil;
        alt = NO;   // is alt pressed
        ctrl = NO;  // is ctrl pressed
        lastKeyFlgs = 0xffff;  // make sure that keys are released first time
        overlayController = [[XenConsoleOverlayViewController alloc] init];
        [overlayController setVncKeyDelegate:self];
        [overlayController setFullScreenModeEnabled:YES];
        [overlayController setDelegate:self];
        hiddenTextView = [[UITextView alloc] init];
        [self setUseLowQuality:NO];
        pinching = NO;
        panning = NO;
        applyBaseTransform = NO;
        scale = 1;
        fullScreeenMode = NO;
        // by default allow presses to control mouse
        [self setUseMousePointer:YES];
    }
    return self;
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [vncController close];
    vncController = nil;
    [theVncConnection closeConnection];
    theVncConnection = nil;
    externalScreenWindow = nil;
    singleFingerTap = nil;
    longPressOnScreen = nil;
    multiFingerTap = nil;
    doubleTapGestureRecognizer = nil;
    pinchRecognizer = nil;
    panRegognizer = nil;
}

-(void) setUpConnection{
    if (NeedsNewConnection){
        NSString* sessionID = [[self hypervisorConnection] sessionID];
        
        XenConsole* console = [self xenConsole];
        if (console){
            location = [console location];
        }
        else{
            location = nil;
        }
        
        if (!location){
            [[self vncView] setVncConnectionState:VNC_CONNECTION_NOCONNECTIONAVAILABLE];
            theVncConnection = nil;
            [[self vncView] setNeedsDisplay];
        }
        else{
            [[self vncView] setVncConnectionState:VNC_CONNECTION_CONNECTING];
            [[self vncView] setNeedsDisplay];
            theVncConnection = [[VNCConnection alloc] initWithConnectionForVMLocation:location andSessionID:sessionID];
            [theVncConnection setDelegate:self];
            
        }
        // reset the flag to indicate that we have a connection
        NeedsNewConnection = NO;
    }
}

// this will allow hide and alt ctrl delete to appear there... could enhance later for function keys etc...
-(void) toggleOnScreenKeyboard{
    if ([hiddenTextView isFirstResponder]){
        [hiddenTextView resignFirstResponder];
        [overlayController setKeyboardOn:NO];
    }
    else{
        [hiddenTextView becomeFirstResponder];
        [overlayController setKeyboardOn:YES];
    }
}

/**
 * Show the VNC view on a second screen in addition to the main display
 * In this mode, the main display can only be embdedded mode.
 */
-(void)displayOnExternalDisplay:(UIScreen*)screenOrNil{

    // remember any the main renderer for the embdedded display if it is not already stored
    if(!defaultRenderer){
        defaultRenderer = [[self vncController] renderer];
    }
    
    // close off any existing external display
    if (externalScreen){
        [self RevertExternalDisplayToMirror];
    }
    if (screenOrNil){
        externalScreen = screenOrNil;
    }
    if (!externalScreen){
        if ([[UIScreen screens] count] > 1){
            externalScreen = [[UIScreen screens] objectAtIndex:1];
        }
    }
    
    if (externalScreen){
        defaultRenderer = [[self vncController] renderer];
        CGRect externalScreenRect = [externalScreen bounds];
        externalView = [[VncView alloc] initWithFrame:externalScreenRect andRemoteScreenSize:[[self vncView] remoteFrameSize]];
        externalScreenWindow = [[UIWindow alloc] initWithFrame:externalScreenRect];
        [externalScreenWindow addSubview:externalView];
        [externalScreenWindow setScreen:externalScreen];
        [externalScreenWindow setHidden: NO];
        [[self vncController] setSecondaryRenderer:externalView];
        [externalView setFrame:externalScreenRect];
        // disable full screen button if there is an external display present
        [overlayController setFullScreenModeEnabled:NO];
    }
    else{
        [self RevertExternalDisplayToMirror];
        // disable full screen button if there is an external display present
        [overlayController setFullScreenModeEnabled:YES];
    }
}

-(void)RevertExternalDisplayToMirror{
    externalScreen = nil;
    externalScreenWindow = nil;
    [vncController setSecondaryRenderer:nil];
    [overlayController setFullScreenModeEnabled:YES];
    externalView = nil;
}

/**
 * Show the VNC view embedded in the tabs like all other screens
 */
-(void)displayEmbeddedView{
    fullScreeenMode = NO;
    [overlayController setFullScreenMode:NO];
    baseTransform = CGAffineTransformMakeRotation(0.0);
    [[self vncView] setTransform:baseTransform];
    [embdeddedParentView addSubview:[self vncView]];
    [self setView:[self vncView]];
    [[self vncView] setFrame:[embdeddedParentView bounds]];
    [[self vncView]  updateScalingFactors];
    // make me the rendeder again
    applyBaseTransform = NO;
}

/**
 * flips the display between fullscreen mode and embedding into the tabview mode,
 */
-(void) toggleFullScreen{
    
//    // if there are externalScreens then do not move from embdedded
//    if (externalScreen){
//        [self displayEmbeddedView];
//    }

    if (fullScreeenMode){
        [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
        [self displayEmbeddedView];
    }
    else{
        fullScreeenMode = YES;
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
        [overlayController setFullScreenMode:YES];
        defaultRenderer = [[self vncController] renderer];
        applyBaseTransform = YES;
        embdeddedParentView = [[self vncView] superview];
        CGRect fullScreen = [[UIScreen mainScreen] bounds];
        [[self vncView] setFrame:fullScreen];
        [[[self view] window] addSubview:[self vncView]];
        [self InitializeFullScreenTransformforView:[self vncView]];
        [[self vncView] updateScalingFactors];
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    }
    
    [self displayShakeMessage];
}

- (void)handleLongPressTap:(UILongPressGestureRecognizer *)recognizer {
    if (![self useMousePointer]){
        return;
    }
    // pass the touches on to other contols
    [recognizer setCancelsTouchesInView:NO];
    // if the mouse has moved since the press then ignore a long press
    //NSLog(@"longTapHandler mouseMoving = %s",mouseMoving ? "true" : "false");
    if (!mouseMoving){
        UIView* theView = [recognizer view];
        CGPoint locationPoint = [recognizer locationOfTouch:0 inView:theView];
        CGPoint scalePoint = [self getScaledTouchAtX:locationPoint.x andY:locationPoint.y withinView:theView];
        
        // do a right click
        [vncController sendMouseInformationwithX:scalePoint.x
                                               Y:scalePoint.y
                               leftButtonPressed:NO
                              rightButtonPressed:YES];
        
        
        [vncController sendMouseInformationwithX:scalePoint.x
                                               Y:scalePoint.y
                               leftButtonPressed:NO
                              rightButtonPressed:NO];
    }
    
}


-(void)handlePinch:(UIPinchGestureRecognizer *)recognizer{

    [recognizer setCancelsTouchesInView:YES];
    if ([recognizer state] == UIGestureRecognizerStateBegan) {
        firstScale = scale;
         pinching = YES;
    }
    
    if ([recognizer state] == UIGestureRecognizerStateEnded) {
        pinching = NO;
    }
    
    scale = firstScale * [recognizer scale];
    if (scale < 1){
        scale = 1;
    }
    
    CGAffineTransform transform = CGAffineTransformMakeScale(scale, scale);
    // make sure that the view is moved so that it does not expose any background
    
    if (applyBaseTransform){
        transform = CGAffineTransformConcat(transform,baseTransform);
    }
    
    [[recognizer view] setTransform:transform];
    
    // make sure when shrinking that it does not exposre background
    CGRect frame = [[recognizer view]  frame];
    
    float width = [[recognizer view ]frame].size.width;
    float height = [[recognizer view]frame].size.height;
    
    float viewPortWidth = width /scale;
    float viewPortHeight = height / scale;
    
    if (frame.origin.x >0){
        frame.origin.x = 0;
    }
    if (frame.origin.y >0){
        frame.origin.y = 0;
    }
    
    if (frame.origin.x + width < viewPortWidth){
        frame.origin.x = viewPortWidth - width;
    }
    if (frame.origin.y + height < viewPortHeight){
        frame.origin.y = viewPortHeight - height;
    }
    
    [[recognizer view ]setFrame:frame];
}

-(void)handlePan:(UIPanGestureRecognizer *)recognizer{
    
    // if frame is not panable (i.e. its at scale 1 then exit
    if (scale == 1){
        return;
    }
    
    CGPoint translatedPoint = [recognizer translationInView:self.view];
    if (applyBaseTransform){
        translatedPoint = CGPointApplyAffineTransform(translatedPoint, baseTransform);
    }

    CGPoint originPoint = [[recognizer view] frame].origin;
    
    if ([recognizer state] == UIGestureRecognizerStateBegan) {
        firstX = originPoint.x / scale;
        firstY = originPoint.y / scale;
        panning = YES;
    }

    if ([recognizer state] == UIGestureRecognizerStateEnded) {
        panning = YES;
    }
    
    // you can implement any int/float value in context of what scale you want to zoom in or out
    float x = translatedPoint.x;
    float y = translatedPoint.y;
    
    float newX = (firstX + x) * scale;
    float newY = (firstY + y) * scale;
    
    float width = [[recognizer view] frame].size.width;
    float height = [[recognizer view] frame].size.height;
    
    float viewPortWidth = width / scale;
    float viewPortHeight = height / scale;
    
    if (-newX + viewPortWidth > width){
        newX = viewPortWidth -width;
    }
    if (newX > 0){
        newX = 0;
    }

    if (-newY + viewPortHeight > height){
        newY = viewPortHeight -height;
    }
    if (newY > 0){
        newY = 0;
    }

    originPoint = CGPointMake(newX, newY);
    
    CGRect frameRect = [[recognizer view] frame];
    // avoid crash due to NaN issues.
    if (frameRect.origin.x == NAN){
        frameRect.origin.x = 0;
    }
    if (frameRect.origin.y == NAN){
        frameRect.origin.y = 0;
    }

    frameRect.origin = originPoint;
    
    
    [[recognizer view] setFrame:frameRect];
}


-(void)handleDoubleTap:(UITapGestureRecognizer *)recognizer {
    if (![self useMousePointer]){
        return;
    }
    // pass the touches on to other contols
    [recognizer setCancelsTouchesInView:NO];
    UIView* theView = [recognizer view];
    CGPoint locationPoint = [recognizer locationOfTouch:0 inView:theView];
    
    CGPoint scalePoint = [self getScaledTouchAtX:locationPoint.x andY:locationPoint.y withinView:theView];

    // do a left click
    [vncController sendMouseInformationwithX:scalePoint.x
                                           Y:scalePoint.y
                           leftButtonPressed:YES
                          rightButtonPressed:NO];
    
    
    [vncController sendMouseInformationwithX:scalePoint.x
                                           Y:scalePoint.y
                           leftButtonPressed:NO
                          rightButtonPressed:NO];
    
    [vncController sendMouseInformationwithX:scalePoint.x
                                           Y:scalePoint.y
                           leftButtonPressed:YES
                          rightButtonPressed:NO];
    
    
    [vncController sendMouseInformationwithX:scalePoint.x
                                           Y:scalePoint.y
                           leftButtonPressed:NO
                          rightButtonPressed:NO];
}


- (void)handleMultiTap:(UITapGestureRecognizer *)recognizer {
    [self displayOptionsOverlay];
}

/**
 * converts the location is a view to a location on the VNC view
 * this is used to make the mouse presses in the right location 
 * in the remote session
 */
-(CGPoint)getScaledTouchAtX:(float)x andY:(float)y withinView:(UIView*)theView{
    float scaleX = x / [theView bounds].size.width;
    scaleX = scaleX * [[vncController renderer] remoteScreenSize].width;
    float scaleY = y / [theView bounds].size.height;
    scaleY = scaleY * [[vncController renderer] remoteScreenSize].height;
    return CGPointMake(scaleX, scaleY);
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    if (![self useMousePointer]){
        return;
    }
    // pass the touches on to other contols
    [recognizer setCancelsTouchesInView:NO];
    UIView* theView = [recognizer view];
    CGPoint locationPoint = [recognizer locationOfTouch:0 inView:theView];

    CGPoint scalePoint = [self getScaledTouchAtX:locationPoint.x andY:locationPoint.y withinView:theView];
    
    // do a left click
    [vncController sendMouseInformationwithX:scalePoint.x
                                           Y:scalePoint.y
                           leftButtonPressed:YES
                          rightButtonPressed:NO];


    [vncController sendMouseInformationwithX:scalePoint.x
                                           Y:scalePoint.y
                           leftButtonPressed:NO
                          rightButtonPressed:NO];

}

#pragma mark touches

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if (![self useMousePointer]){
        return;
    }

    leftMouseDown = YES;
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if (![self useMousePointer]){
        return;
    }

    leftMouseDown = NO;
    mouseMoving = NO;
    pinching = NO;
    panning = NO;
    CGPoint locationPoint = [[touches anyObject] locationInView:[self vncView]];
    CGPoint scalePoint = [self getScaledTouchAtX:locationPoint.x andY:locationPoint.y withinView:[self vncView]];
    
    // do a left click
    [vncController sendMouseInformationwithX:scalePoint.x
                                           Y:scalePoint.y
                           leftButtonPressed:NO
                          rightButtonPressed:NO];
}

-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    if (![self useMousePointer]){
        return;
    }

    // send a mouse move here
    //This gets called for a touch anywhere in the embedded view
    
    if (!pinching && !panning){
    mouseMoving = YES;
    CGPoint locationPoint = [[touches anyObject] locationInView:[self vncView]];
    CGPoint scalePoint = [self getScaledTouchAtX:locationPoint.x andY:locationPoint.y withinView:[self vncView]];

    // do a left click
    [vncController sendMouseInformationwithX:scalePoint.x
                                           Y:scalePoint.y
                           leftButtonPressed:leftMouseDown
                          rightButtonPressed:NO];
    }
}

#pragma mark VNCKeypressDelegate
-(void)releaseWindowskey{
    [vncController sendKeyUpForKey:0xFFEB]; // windows key
}

-(void)pressWindowskey{
    [vncController sendKeyDownForKey:0xFFEB]; // windows key
}

-(void)cursorUpKeyPressed{
    //press
    [vncController sendKeyDownForKey:0xff52];
    // release
    [vncController sendKeyUpForKey:0xff52]; 
}

-(void)cursorDownKeyPressed{
    //press
    [vncController sendKeyDownForKey:0xff54];
    // release
    [vncController sendKeyUpForKey:0xff54]; 
}

-(void)cursorLeftKeyPressed{
    //press
    [vncController sendKeyDownForKey:0xff51]; 
    // release
    [vncController sendKeyUpForKey:0xff51]; 
}
-(void)cursorRightKeyPressed{
    //press
    [vncController sendKeyDownForKey:0xff53];
    // release
    [vncController sendKeyUpForKey:0xff53];
}

-(void)escKeyPressed{
    //press
    [vncController sendKeyDownForKey:0xff1b];
    // release
    [vncController sendKeyUpForKey:0xff1b]; 
}

#pragma mark ConsoleViewOverlayDelegate
/**
 * responds to selected options on the overlay options view
 */
-(void) optionSelected:(NSInteger)theOption{
    switch (theOption) {
        case 100:   // fullscreen or dock screen
            [self toggleFullScreen];  // need to set the overlay mode so it can show the re-embed button
            [[overlayController view] removeFromSuperview];
            [[overlayController view] setHidden:YES];
            break;
        case 200:   // ALT-CTRL-DEL
            [self sendAltCtrlDelete];
            break;
        case 800:
            [self toggleOnScreenKeyboard];
            break;
        case 900:   // Exit
            [[overlayController view] removeFromSuperview];
            [[overlayController view] setHidden:YES];
            break;
        default:
            break;
    }
}

/**
 * send an alt-ctrl-del key sequence to the VNC server
 */
-(void) sendAltCtrlDelete{

    // press
    [vncController sendKeyDownForKey:0xffe9]; // alt
    [vncController sendKeyDownForKey:0xffe3]; // ctrl
    [vncController sendKeyDownForKey:0xffff]; // del
    // release
    [vncController sendKeyUpForKey:0xffe9]; // alt
    [vncController sendKeyUpForKey:0xffe3]; // ctrl
    [vncController sendKeyUpForKey:0xffff]; // del

}

#pragma mark - TextViewDelegate Functions
-(void)textViewDidChange:(UITextView *)textView{
    NSLog(@"new text is '%@'",[textView text]);
    NSLog(@"new range is '%lu'",(unsigned long)[textView selectedRange].location);
    NSString* t = [textView text];
    if ([t length] < 5){
        // can work out if it is backspace or delete by the string remaining :)
        NSLog(@"key pressed is backspace");
            [vncController sendKeyDownForKey:0xff08];
            [vncController sendKeyUpForKey:0xff08];
    }
    else{
        NSString* newChar = [t substringWithRange:NSMakeRange(3, 1) ];
        // if char is uppercase then shift must also be pressed, so if was presse before do nothing
        // otherwise rpessshift.
        // do this by remembering last key if it was upper et....
        NSLog(@"key pressed is '%@'",newChar);
        // if uppercase send a shift character first if shift not already pressed
        // then release shift afterwards
        // convert to lowercase and send this ACSII code (if it is a-z or 0-9)
        NSString* toSend = [newChar lowercaseString];
        if (![toSend isEqualToString:newChar]){
            // send shift down
            NSLog(@"sending shift DOWN");
            [vncController sendKeyDownForKey:0xffe1];
        }

        // separate out the return keu
        if ([toSend characterAtIndex:0] == 0x0d || [toSend characterAtIndex:0] == 0x0a){
            [vncController sendKeyDownForKey:0xff0d];
            [vncController sendKeyUpForKey:0xff0d];
        }else{
            [vncController sendKeyDownForKey:[toSend characterAtIndex:0]];
            [vncController sendKeyUpForKey:[toSend characterAtIndex:0]];
        }
        if (![toSend isEqualToString:newChar]){
            // send shift up
            NSLog(@"sending shift UP");
            [vncController sendKeyUpForKey:0xffe1];
        }
    }
    [self initTextView];
    
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    // when editing finishes make sure that the menu knows that the keyboard is hidden.
    [overlayController setKeyboardOn:NO];
}

/**
 * display the overlay options page that provides things like alt-crtl-del and go full screen
 */
-(void) displayOptionsOverlay{
    // this should stop anything else being pressed until an option is pressed :)
    // put this window on top of everything
    [self InitializeFullScreenTransformforView:[overlayController view]];
    [[[self view] window] addSubview:[overlayController view]];
    [[overlayController view] setHidden:NO];
}

#pragma mark VncConnectionDelegate
-(void)ConnectionEsatblishedWithInputStream:(NSInputStream*)theInputStream andOutputStream:(NSOutputStream*)theOutputStream RFBVersion:(NSString*)rfbVersion{
    // disconnect the VNCConnection listeners from the stream
    // the VNCViewCOntroller will take over now.
    [theInputStream setDelegate:nil];
    [theOutputStream setDelegate:nil];
    [self assignDelegateToStreams:theInputStream and:theOutputStream withRFBVersion:rfbVersion];
}

-(void)ConnectionFailed:(NSString*)theReason{
    [[self vncView] setFailedString:theReason];
    [[self vncView] setVncConnectionState:VNC_CONNECTION_FAILED];
    [[self vncView] setNeedsDisplay];
}

#pragma mark ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    
    
    
    doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]
                                                          initWithTarget:self
                                                          action:@selector(handleDoubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    
    pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    
    panRegognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    
    [panRegognizer setMinimumNumberOfTouches:2];
    [panRegognizer setMinimumNumberOfTouches:2];
    
    multiFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                              action:@selector(handleMultiTap:)];

    [multiFingerTap setNumberOfTouchesRequired:3];
    [multiFingerTap setNumberOfTapsRequired:1];

    longPressOnScreen = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleLongPressTap:)];
    [[overlayController view] setHidden:YES];
}

-(void) displayShakeMessage{
    // don't add another one if there is already one being displayed that has not been removed yet.
    if (!imageView){
    UIImage* message = [UIImage imageNamed:@"shakeForOptionsScreen"];
    imageView = [[UIImageView alloc ]initWithFrame:[[UIScreen mainScreen] bounds]];
    [imageView setContentMode:UIViewContentModeCenter];
    [imageView setImage:message];
    
    [self InitializeFullScreenTransformforView:imageView];
    [[[self view] window] addSubview:imageView];
    [self performSelector:@selector(hideMessage) withObject:nil afterDelay:5.0]; // remove the message after 5 seconds
    }
}

-(void) hideMessage{
    if (imageView){
        [imageView removeFromSuperview];
        imageView = nil;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"XenConsole View will appear");
    if (takeActionOnAppear){
        [self refreshPage];  // cause the connection to get setup
        [super viewWillAppear:animated];
        applyBaseTransform = NO;
        scale = 1;
        pinching = NO;
        panning = NO;
        fullScreeenMode = NO;
        [vncController setRenderer:[self vncView]];
        [[self vncView] updateDisplay];
        [[self vncView] updateScalingFactors];
    }
    NSLog(@"XenConsole View will appear (Complete)");
}

-(void) viewDidAppear:(BOOL)animated{
    NSLog(@"XenConsole View Did appear");
    if (takeActionOnAppear){
        [super viewDidAppear:animated];
        
        // configure the textview as required
        [self configureOnScreenKeyboard];
        
        [self displayShakeMessage];
        [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
        if (theVncConnection){
            [theVncConnection openConnection];
        }
        [self registerForKeyboardNotifications];
        // this will move the display onto the external screen if there is one
        [self displayOnExternalDisplay:nil];
        // this will listen for changes so if one becomes available it will use it.
        [self registerForScreenChangeNotifications];
        
        // listen to rotations so that even the full screen mode will rotate.
        [self registerForRotationNotifications];
        [[self vncView]setTag:TAG_FULLSCREENVIEW];
        [[self vncView] addGestureRecognizer:singleFingerTap];
        [[self vncView] addGestureRecognizer:multiFingerTap];
        [[self vncView] addGestureRecognizer:longPressOnScreen];
        [[self vncView] addGestureRecognizer:doubleTapGestureRecognizer];
        [[self vncView] addGestureRecognizer:pinchRecognizer];
        [[self vncView] addGestureRecognizer:panRegognizer];
    }
    
    NSLog(@"XenConsole View Did appear (Complete)");
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self RevertExternalDisplayToMirror];
    // close the connection
    [theVncConnection closeConnection];
    theVncConnection = nil;
    [vncController close];
    vncController = nil;
    NeedsNewConnection = YES;
    [[overlayController view] setHidden:YES];
    [self hideMessage];  // remove any shake message
    [[self vncView] removeGestureRecognizer:singleFingerTap];
    [[self vncView] removeGestureRecognizer:multiFingerTap];
    [[self vncView] removeGestureRecognizer:longPressOnScreen];
    [[self vncView] removeGestureRecognizer:doubleTapGestureRecognizer];
    [[self vncView] removeGestureRecognizer:pinchRecognizer];
    [[self vncView] removeGestureRecognizer:panRegognizer];
}

-(void) viewDidDisappear:(BOOL)animated{
    // remove listener from the textview
    [hiddenTextView setDelegate:nil];   // UITextViewDelegate
    
    [super viewDidDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    
    [self unregisterForNotifications];
    // close the connection
    [theVncConnection closeConnection];
    theVncConnection = nil;
    [[overlayController view] removeFromSuperview];
    [[overlayController view] setHidden:YES];
    [vncController setRenderer:nil];

    // this is needed so that if you return from a suspended state, and the app was last used in
    // full screen the fullscreen view needs to be removed so that the rest of the app is available.
    if (fullScreeenMode){
        NSArray* allViews = [[[[UIApplication sharedApplication] windows] objectAtIndex:0] subviews];
        for (UIView* view in allViews){
            if ([view tag] == TAG_FULLSCREENVIEW){
               [view removeFromSuperview];
            }
        }
    }
}

-(void) configureOnScreenKeyboard{
    [[self vncView] addSubview:hiddenTextView];
    [hiddenTextView setHidden:YES];
    [hiddenTextView setDelegate:self];   // UITextViewDelegate
    [self initTextView];
}

-(void) initTextView{
    [hiddenTextView setText:@"abcde"];
    [hiddenTextView setSelectedRange:NSMakeRange(3, 0)];
}


- (BOOL)shouldAutorotate{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

//#error when full screen there are no rotation events to here - need to get them so that I can still do this.
//#error need to make the external screen not use the fullFrameScreen object that is too confusing.. make a new external one for this
//#test it on phone, ipad with and without apple tv etc...
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self didRotate:self];
}

-(void) didRotate:(id)sender{

    scale = 1;


    // autorotation will take care of the screen when not in full screen mode
    if (fullScreeenMode){
        // this will resolve it when in full screen mode
       [self InitializeFullScreenTransformforView:[self vncView] animated:YES];
    }
    else{
        [[self vncView] setTransform:CGAffineTransformMakeScale(1, 1)];
        [[self vncView] setFrame:[[[self vncView] superview] frame]];
    }
    
    [[self vncView] updateScalingFactors];
    
    if (![[overlayController view] isHidden]){
        [self displayOptionsOverlay];
    }    
}

/**
 * Set the flag to indicate that the appear operation that
 * are about to happen should not be acted on
 * they come as part of the hack to force screen to be rotated to the
 * desired orientation on edit screens
 */
-(void) setTakeActionOnAppear:(NSNumber*)takeAction{
    takeActionOnAppear =  [takeAction boolValue];
}

#pragma mark General helper routines

/**
 * Register the class for notification from the keyboard capture and resend in the 
 * HypOpsUIApplication class
 */
-(void)registerForKeyboardNotifications {
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self
                           selector:@selector (handle_keyUp:)
                               name:GSEventKeyUpNotification
                             object:[UIApplication sharedApplication]];

    [notificationCenter addObserver:self
                           selector:@selector (handle_keyDown:)
                               name:GSEventKeyDownNotification
                             object:[UIApplication sharedApplication]];

    [notificationCenter addObserver:self
                           selector:@selector (handle_keyModifier:)
                               name:GSEventKeyModifierNotification
                             object:[UIApplication sharedApplication]];

}

-(void) unregisterForNotifications{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:UIScreenDidConnectNotification object:nil];
    [center removeObserver:self name:UIScreenDidDisconnectNotification object:nil];
    [center removeObserver:self name:UIScreenModeDidChangeNotification object:nil];
    [center removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    [center removeObserver:self];
}

-(void) registerForRotationNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
}

-(void) registerForScreenChangeNotifications{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(screenDidConnect:) name:UIScreenDidConnectNotification object:nil];
    [center addObserver:self selector:@selector(screenDidDisconnect:) name:UIScreenDidDisconnectNotification object:nil];
    [center addObserver:self selector:@selector(screenModeDidChange:) name:UIScreenModeDidChangeNotification object:nil];
}

/** transform the display so that it is in the corrent oritnetation and set the size of the
 * screen so that the next frame will be drawn correctly.
 */
- (void) InitializeFullScreenTransformforView:(UIView*)theView{
    [self InitializeFullScreenTransformforView:theView animated:NO];
}

- (void) InitializeFullScreenTransformforView:(UIView*)theView animated:(bool)animated{
    // dont rotate external screen if present
    UIDeviceOrientation orientation;
    
    // choose a sensible default
    if ([[UIDevice currentDevice].model hasPrefix:@"iPhone"]){
        orientation = UIDeviceOrientationPortrait;
    }
    else{
        orientation = UIDeviceOrientationLandscapeLeft;
    }
    
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeLeft){
        orientation = UIDeviceOrientationLandscapeLeft;
    }else if([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeRight){
        orientation = UIDeviceOrientationLandscapeRight;
    }else if([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait){
        orientation = UIDeviceOrientationPortrait;
    }else if([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown){
        orientation = UIDeviceOrientationPortraitUpsideDown;
    }
    
    if (orientation == UIDeviceOrientationLandscapeLeft) {
        baseTransform = CGAffineTransformMakeRotation(M_PI / -2.0);
    } else if (orientation == UIDeviceOrientationLandscapeRight) {
        baseTransform = CGAffineTransformMakeRotation(M_PI / 2.0);
    } else if (orientation == UIDeviceOrientationPortraitUpsideDown) {
        baseTransform = CGAffineTransformMakeRotation(M_PI);
    } else if (orientation == UIDeviceOrientationPortrait) {
        baseTransform = CGAffineTransformMakeRotation(0.0);
    }
    
    float duration = 0;
    if (animated) {
        duration = 0.2;
    }
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{                         
                         [theView setTransform:CGAffineTransformConcat(baseTransform, CGAffineTransformMakeScale(1, 1))];
                         // set the size too.
                         [theView setFrame:[[UIScreen mainScreen] bounds]];
                     }
                     completion:^(BOOL finished){
                     }];
}

#pragma mark used to capture keyboard event
- (void) handle_keyUp:(id)notification {
    NSDictionary* theData = (NSDictionary*)[notification userInfo];
    [vncController sendKeyUpForKey:[self symCodeForEvent:theData]];
}

- (void) handle_keyDown:(id)notification {
    NSDictionary* theData = (NSDictionary*)[notification userInfo];
    [vncController sendKeyDownForKey:[self symCodeForEvent:theData]];
}

- (void) handle_keyModifier:(id)notification {
    // could put something in here to determine from keycode if it is a left or right key,
    // then have 2 sets of this method and lask key press for each side.
    // for now always use left lkey versions
    NSDictionary* theData = (NSDictionary*)[notification userInfo];
    NSNumber* theflags = [theData objectForKey:@"eventflags"];NSUInteger newflags = [theflags intValue];
    
    // alt state has changed
    if ((lastKeyFlgs & KEYPRESSEDFLAG_ALT) != (newflags & KEYPRESSEDFLAG_ALT)){
        if (newflags & KEYPRESSEDFLAG_ALT){
            // press the key (left alt) = 0xffe9
            [vncController sendKeyDownForKey:0xffe9];
            alt = YES;
        }
        else{
            // release the key
            [vncController sendKeyUpForKey:0xffe9];
            alt = NO;
        }
    }

    if ( alt && ctrl){  // alt-ctrl
        [self displayOptionsOverlay];
    }

    // ctrl state has changed
    if ((lastKeyFlgs & KEYPRESSEDFLAG_CTRL) != (newflags & KEYPRESSEDFLAG_CTRL)){
        if (newflags & KEYPRESSEDFLAG_CTRL){
            // press the key (left ctrl) = 0xffe3
            [vncController sendKeyDownForKey:0xffe3];
            ctrl = YES;
        }
        else{
            // release the key
            [vncController sendKeyUpForKey:0xffe3];
            ctrl = NO;
        }
    }

    // shift state has changed
    if ((lastKeyFlgs & KEYPRESSEDFLAG_SHIFT) != (newflags & KEYPRESSEDFLAG_SHIFT)){
        if (newflags & KEYPRESSEDFLAG_SHIFT){
            // press the key (left shift) = 0xffe1
            [vncController sendKeyDownForKey:0xffe1];
        }
        else{
            // release the key
            [vncController sendKeyUpForKey:0xffe1];
        }
    }

    // cmd state has changed
    // this also maps onto the windows key on a bluetooth keyboard.
    if ((lastKeyFlgs & KEYPRESSEDFLAG_CMD) != (newflags & KEYPRESSEDFLAG_CMD)){
        if (newflags & KEYPRESSEDFLAG_CMD){
            // press the windows key
            [vncController sendKeyDownForKey:0xFFEB];
        }
        else{
            // release the key
            [vncController sendKeyUpForKey:0xFFEB];
        }
    }
    
    lastKeyFlgs = newflags;
}


-(NSInteger) symCodeForEvent:(NSDictionary*)userInfo{
//    NSNumber* theChar = [userInfo objectForKey:@"keychar"];
//NSUInteger charVal = [theChar intValue];
    NSNumber* theCode = [userInfo objectForKey:@"keycode"];NSUInteger codeVal = [theCode intValue];
    //NSLog(@"keyCode was 0x%04x and keyChar was 0x%04x", codeVal, charVal);

    if (codeVal < keyCodeMappingSize){
        //    NSLog(@"Using keysym 0x%04x", keyCodeMapping[codeVal]);
        return keyCodeMapping[codeVal];
    }
    
    NSLog(@"No mapping available");
    return 0;
    
}
#pragma mark UIscreen handling
/**
 * This should get called when notification is recieved of a new UIScreen availability
 */
- (void) screenDidConnect:(NSNotification *)aNotification{
    NSLog(@"A new screen got connected: %@", [aNotification object]);
    UIScreen *someScreen = [aNotification object];
    [self displayOnExternalDisplay:someScreen];
}

- (void) screenDidDisconnect:(NSNotification *)aNotification{
    NSLog(@"A screen got disconnected: %@", [aNotification object]);
    [self RevertExternalDisplayToMirror];
}

- (void) screenModeDidChange:(NSNotification *)aNotification{
    UIScreen *someScreen = [aNotification object];
    NSLog(@"The screen mode for a screen did change: %@", [someScreen currentMode]);    
}

#pragma mark hypervisor obejcts
-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

//#error this should be the VNC only one, and not anything else I think otherwise it will not work......
-(XenConsole*) xenConsole{
    NSArray* consolesRefs = [[self xenVM] referencesForType:HYPOBJ_CONSOLES];
    XenConsole* result ;
    if (consolesRefs && [consolesRefs count]>0){
        for (NSString* consoleRef in consolesRefs) {
            NSArray *xenConsoles = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CONSOLES withCondition:[XenBase XenBaseFor:consoleRef]];
            XenConsole* toTest = [xenConsoles objectAtIndex:0];
            if ([[toTest protocol] isEqualToString:@"rfb"]){
                result = [xenConsoles objectAtIndex:0];
            }
        }
    }
    return result;
}

#pragma mark Identifyable
- (void) setId:(NSString *)newID{
    // do nothing here.  This is not needed in this implementation
}

#pragma mark RefreshablePage
-(void) refreshPage{
    if (NeedsNewConnection){
        // close any previous connection here
        [theVncConnection closeConnection];
        // set up the new conneciton
        [self setUpConnection];
    }
}

#pragma mark -
#pragma mark HypervisorCOnnectionDelegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    if ( objectType == HYPOBJ_VM || objectType == HYPOBJ_CONSOLES){
        [self refreshPage];
        if ([[self xenVM] power_state] != XENPOWERSTATE_RUNNING){
            if (![theVncConnection isConnected]){
                [theVncConnection openConnection];
            }
        }
    }
}
@end
