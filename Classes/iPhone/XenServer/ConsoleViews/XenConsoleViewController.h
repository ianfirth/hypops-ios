//
//  XenVMConsoleViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVM.h"
#import "vncViewController.h"
#import "VNCConnection.h"
#import "XenConsoleOverlayViewController.h"
#import "XenConsole.h"
#import "Identifyable.h"
#import "RefreshablePage.h"

// flags indicate which key is pressed here
#define KEYPRESSEDFLAG_CMD     0x010000   // this is also the windows key on a bluetooth keyboard.
#define KEYPRESSEDFLAG_CTRL    0x100000
#define KEYPRESSEDFLAG_ALT     0x080000
#define KEYPRESSEDFLAG_SHIFT   0x020000

/**
 * maps keycodes received from the keyboard to the code that needs to be sent over VNC connection
 * in future there could be multiple ones of these based on different keyboard layouts etc.. defined in the settings for the aoo
 * for now there in one for UK keuboard :)
 */

static const NSUInteger  keyCodeMappingSize = 0x53;
static const NSUInteger  keyCodeMapping[0x53] =
   {0,0,0,0,    // 0-3 unkown
    0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d,0x6e,  // 0x04 ->      = (a - n)
    0x6f,0x70,0x71,0x72,0x73,0x74,0x75,0x76,0x77,0x78,0x79,0x7a,            //      -> 0x1d = (o - z)
    0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x30,                      // 0x1e -> 0x27 = (1 - 0)
    0xff0d,     // 0x28 = return
    0xff1b,     // 0x29 = esc
    0xff08,     // 0x2a = backspace
    0xff09,     // 0x2b = tab
    0x20,       // 0x2c = space
    0x2d,       // 0x2d = -
    0x3d,       // 0x2e = =
    0x5b,       // 0x2f = [
    0x5d,       // 0x30 = ]
    0x5c,       // 0x31 = backslash
    0x00,       // 0x32 = ??????????????????
    0x3b,       // 0x33 = ;
    0x27,       // 0x34 = '
    0x60,       // 0x35 = `
    0x2c,       // 0x36 = ,
    0x2e,       // 0x37 = .
    0x2f,       // 0x38 = /
    0x00,       // 0x39 = ??????????????????
    0xffbe,     // 0x3a = f1
    0xffbf,     // 0x3b = f2
    0xffc0,     // 0x3c = f3
    0xffc1,     // 0x3d = f4
    0xffc2,     // 0x3e = f5
    0xffc3,     // 0x3f = f6
    0xffc4,     // 0x40 = f7
    0xffc5,     // 0x41 = f8
    0xffc6,     // 0x42 = f9
    0xffc7,     // 0x43 = f10
    0xffc8,     // 0x44 = f11
    0xffc9,     // 0x45 = f12
    0x00,       // 0x46 = prt scrn
    0x00,       // 0x47 = ???????????????????
    0x00,       // 0x48 = ???????????????????
    0xff63,     // 0x49 = ins
    0xff50,     // 0x4a = home
    0xff55,     // 0x4b = page up
    0xffff,     // 0x4c = del
    0xff57,     // 0x4d = end
    0xff56,     // 0x4e = page down
    0xff53,     // 0x4f = right arrow
    0xff51,     // 0x50 = left arrow
    0xff54,     // 0x51 = down arrow
    0xff52,     // 0x52 = up arrow
};

// note 0x65 is the menu key on the windows keyboard... not mapped as dont know what to map to and is a little way off the bottom of here


@interface XenConsoleViewController : vncViewController
                        <VncConnectionDelegate,
                        HypervisorConnectionDelegate,
                        UIAccelerometerDelegate,
                        ConsoleOverlayDelegate,
                        UITextViewDelegate,
                        VNCKeyPressDelegate,Identifyable,
                        RefreshablePage> {
    VNCConnection* theVncConnection;
    NSString* lastVMRef;
    bool NeedsNewConnection;
    NSString* location;
}

-(id) initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection;

-(XenHypervisorConnection *) hypervisorConnection;

-(XenConsole*) xenConsole;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenVMRef;

@property BOOL useMousePointer;

@end

@interface XenConsoleViewController (AbstractMethods)
- (XenVM *) xenVM;

// this is used by relection in the tab contol to update the page.  In this class it updates the xenVMRef.
- (void) setId:(NSString*)newID;

@end


