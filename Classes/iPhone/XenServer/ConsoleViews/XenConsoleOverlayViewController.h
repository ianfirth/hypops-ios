//
//  XenConsoleOverlayViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "VNCKeyPressDelegate.h"

@protocol ConsoleOverlayDelegate
    -(void) optionSelected:(NSInteger)theOption;
@end

@interface XenConsoleOverlayViewController : UIViewController{
    IBOutlet MPVolumeView *mediaView;
    IBOutlet UIButton* windowsKey;
    bool fullScreenMode;
}

/**
 * set a method that is used to comunicate that operations have taked place in the overlay view
 */
-(void) setDelegate:(id<ConsoleOverlayDelegate>)theDeleate;

/**
 * used each time a button is pressed
 */
- (IBAction)touchUpInside:(id)sender;

/**
 * set the mode to be fullscreen
 */
- (void) setFullScreenMode:(BOOL)fullScreen;

/**
 * set the mode for keyboard button
 */
-(void) setKeyboardOn:(BOOL)displayed;

@property IBOutlet UIButton* fullScreenButton;
@property IBOutlet UIButton* keyboardButton;
@property BOOL fullScreenModeEnabled;

@property(nonatomic, weak) id<VNCKeyPressDelegate> vncKeyDelegate;

@end
