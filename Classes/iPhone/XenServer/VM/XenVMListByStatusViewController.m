//
//  VMListByStatusViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVMListByStatusViewController.h"
#import "XenVM.h"
#import "XenVMListViewController.h"
#import "XenImageBuilder.h"
#import "ImageBuilder.h"
#import "XenVMListByTagViewController.h"
#import "ConnectionObjectUITableViewCell.h"

@interface XenVMListByStatusViewController ()
- (NSNumber *) xenVMCatagoryAtIndex:(long) index;
-(void) reloadTableData;
@end

@implementation XenVMListByStatusViewController
@synthesize xenVMCatagories, hypConnID;

#pragma mark -
#pragma mark View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil hypervisorConnection:(XenHypervisorConnection *)hypervisorConnection{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        [self setHypConnID:[hypervisorConnection connectionID]];
        // always enough room in portrait so only enable scroll if pull to refresh available
        [[self tableView] setScrollEnabled:![hypervisorConnection isAutoUpdateEnabled]];
    }
    return self;
}

- (void)viewDidLoad {
    [[self tableView] setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh)
             forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;

    [self setXenVMCatagories: [NSDictionary dictionaryWithObjectsAndKeys:
                               [NSNull null], [NSNumber numberWithInt:XEN_VM_ALL],
                               [XenVM VM_Tagged],[NSNumber numberWithInt:XEN_VM_TAGGED],
                               [XenVM VM_PowerStateFor:XENPOWERSTATE_HALTED],[NSNumber numberWithInt:XEN_VM_HALTED],
                               [XenVM VM_PowerStateFor:XENPOWERSTATE_PAUSED],[NSNumber numberWithInt:XEN_VM_PAUSED],
                               [XenVM VM_PowerStateFor:XENPOWERSTATE_RUNNING],[NSNumber numberWithInt:XEN_VM_RUNNING],
                               [XenVM VM_PowerStateFor:XENPOWERSTATE_SUSPENDED],[NSNumber numberWithInt:XEN_VM_SUSPENDED],
                               nil]];
    [super viewDidLoad];
    self.title = @"VMs";
    [[self tableView] registerNib:[UINib nibWithNibName:@"ConnectionObjectUITableViewCell" bundle:nil] forCellReuseIdentifier:@"ConnectionObjectUITableViewCell"];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void) viewDidUnload{
    [self setXenVMCatagories:nil];
    [super viewDidUnload];
}

-(void) reloadTableData{
    [[self tableView] reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [[self tableView] reloadData];
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    [xenHypervisorConnection addHypervisorConnectionDelegate:self];
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    [xenHypervisorConnection removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(ConnectionObjectUITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    NSNumber *xenVMCatagory = [self xenVMCatagoryAtIndex:indexPath.row];
    
    NSString *displayText;
    long objectCount = 0;
    BOOL validCatagory = YES;
    
    switch ([xenVMCatagory intValue]) {
        case XEN_VM_HALTED:
            displayText = @"Stopped";
            [[cell imageView2]setImage:[XenImageBuilder buildVMImageForVMPowerState:XENPOWERSTATE_HALTED]];
            break;
        case XEN_VM_PAUSED:
            displayText = @"Paused";
            [[cell imageView2]setImage:[XenImageBuilder buildVMImageForVMPowerState:XENPOWERSTATE_PAUSED]];
            break;
        case XEN_VM_RUNNING:
            displayText = @"Running";
            [[cell imageView2]setImage:[XenImageBuilder buildVMImageForVMPowerState:XENPOWERSTATE_RUNNING]];
            break;
        case XEN_VM_SUSPENDED:
            displayText = @"Suspended";
            [[cell imageView2]setImage:[XenImageBuilder buildVMImageForVMPowerState:XENPOWERSTATE_SUSPENDED]];
            break;
        case XEN_VM_ALL:
            displayText = @"All VMs";
            [[cell imageView2] setImage:[ImageBuilder vMImage]];
            break;
        case XEN_VM_TAGGED:
            displayText = @"Tagged VMs";
            [[cell imageView2] setImage:[ImageBuilder vMTaggedImage]];
            break;
        default:
            validCatagory = NO;
            break;
    }
    
    if (validCatagory){
        XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

        NSString *detailText;
        NSObject *predicate = [[self xenVMCatagories] objectForKey:xenVMCatagory];
        if ([predicate isKindOfClass:[NSNull class]]){
            objectCount = [[xenHypervisorConnection hypObjectsForType:HYPOBJ_VM] count];
        } else {
            objectCount = [[xenHypervisorConnection hypObjectsForType:HYPOBJ_VM withCondition:(NSPredicate *)predicate] count];
        }
        detailText = [NSString stringWithFormat:@"%lu",objectCount];
        
        // set the selection style and the accessory type
        if (objectCount >0){
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        } else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
      
        // set the label text
        [[cell textLabel2] setText:displayText];
        
        // set the detail text
        if (detailText != nil) {
            [[cell detailTextLabel2] setText:detailText];
        }
    }        
    else {
        NSLog(@"Invalid cell catagory provided %@",xenVMCatagory);
    }
    
    // make every other row use alternate color
    [cell setAlternate:indexPath.row %2];
    [[cell refreshButton] setHidden:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self xenVMCatagories] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"ConnectionObjectUITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ConnectionObjectUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:(ConnectionObjectUITableViewCell*)cell atIndexPath:indexPath];
    
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    NSNumber *xenVMCatagory = [self xenVMCatagoryAtIndex:indexPath.row];
    NSObject *predicate = [[self xenVMCatagories] objectForKey:xenVMCatagory];
    if ([predicate isKindOfClass:[NSNull class]]){
        predicate = nil;
    }
    
    long objectCount = 0;
    if ([predicate isKindOfClass:[NSNull class]]){
        objectCount = [[xenHypervisorConnection hypObjectsForType:HYPOBJ_VM] count];
    } else {
        objectCount = [[xenHypervisorConnection hypObjectsForType:HYPOBJ_VM withCondition:(NSPredicate *)predicate] count];
    }

    if (objectCount > 0){
        
        XenVMListViewController *vMListViewController = [[XenVMListViewController alloc] initWithHypervisorConnection:xenHypervisorConnection withCondition:(NSPredicate *)predicate];
        
        switch ([xenVMCatagory intValue]) {
            case XEN_VM_HALTED:
                [vMListViewController setTitle:@"Stopped VMs"];
                [self.navigationController pushViewController:vMListViewController animated:YES];
                break;
            case XEN_VM_PAUSED:
                [vMListViewController setTitle:@"Paused VMs"];
                [self.navigationController pushViewController:vMListViewController animated:YES];
                break;
            case XEN_VM_RUNNING:
                [vMListViewController setTitle:@"Running VMs"];
                [self.navigationController pushViewController:vMListViewController animated:YES];
                break;
            case XEN_VM_SUSPENDED:
                [vMListViewController setTitle:@"Suspended VMs"];
                [self.navigationController pushViewController:vMListViewController animated:YES];
                break;
            case XEN_VM_ALL:
                [vMListViewController setTitle:@"All VMs"];
                [self.navigationController pushViewController:vMListViewController animated:YES];
                break;
            case XEN_VM_TAGGED:{
                XenVMListByTagViewController *vMListByTagViewController = [[XenVMListByTagViewController alloc] initWithHypervisorConnection:xenHypervisorConnection withPredicate:(NSPredicate *)predicate];
                
                [vMListByTagViewController setTitle:@"VMs by Tag"];
                [self.navigationController pushViewController:vMListByTagViewController animated:YES];
                break;
            }
            default:
                break;
        }
    }
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)dealloc {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    [xenHypervisorConnection removeHypervisorConnectionDelegate:self];
}

#pragma mark -
#pragma mark Private methods
/*
 * return the catagory from the index specified.  This ensures that the list is provided in numerical order
 */
- (NSNumber *) xenVMCatagoryAtIndex:(long) index{
    NSSortDescriptor *numberDescriptor = [[NSSortDescriptor alloc] initWithKey:@"intValue" ascending:YES];
    NSArray *keys = [[[self xenVMCatagories] allKeys] sortedArrayUsingDescriptors:[NSArray arrayWithObject:numberDescriptor]];
    NSNumber *xenVMCatagory = [keys objectAtIndex:index];
    return xenVMCatagory;
}



#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    
    if (![xenHypervisorConnection isUpdatePendingForType:HYPOBJ_VM]){
        [xenHypervisorConnection RequestHypObjectsForType:HYPOBJ_VM];
    }
    // the call to stopLoading is on the data update received.
}

// override, and only add the header if not in auto refresh mode
- (bool) isActive {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    return (![xenHypervisorConnection isAutoUpdateEnabled]);
}

#pragma mark -
#pragma mark XenHypervisorConnection protocol implementation

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | HYPOBJ_VM) >0){
        XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
        
        [self reloadTableData];
        if (![xenHypervisorConnection isAutoUpdateEnabled]){
            [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
        }
    }
}


@end

