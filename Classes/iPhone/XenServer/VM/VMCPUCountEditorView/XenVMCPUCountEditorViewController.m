//
//  XenVMCPUCountEditorViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#define MemoryMultiplier (1024*1024)
#import "XenVMCPUCountEditorViewController.h"
#import "XenVM.h"
#import "HypervisorConnectionFactory.h"
#import "ProgressAlert.h"

@interface XenVMCPUCountEditorViewController (){
    HypervisorType hypervisorType;
    
    NSString* requestString;
    
    NSString* hypConnectionID;
    bool isVisible;
    ProgressAlert* progressAlert;
}

-(void) updateDisplay;
- (bool) canChangeCpuCount;
@end

@implementation XenVMCPUCountEditorViewController

@synthesize xenVMRef = _xenVMRef;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *) [HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:hypConnectionID];
}

- (XenVM *) xenVM{
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVMRef]]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }
    return nil;
}

- (id)initWithHypervisorConnectionID:(NSString*)connectionID andXenVMRef:(NSString*)vmRef
{
    self = [super initWithNibName:@"XenVMCPUCountEditorViewController" bundle:nil];
    if (self) {
        // Custom initialization
        hypConnectionID = connectionID;
        _xenVMRef = vmRef;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // make sure that the view is not displayed behind the navigation bar if there is one
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // Do any additional setup after loading the view from its nib.
    [statusLabel setHidden:YES];
}


- (bool) canChangeCpuCount{
    // if powered off then can always do this
    if ([[self xenVM] power_state] == XENPOWERSTATE_HALTED){
        return YES;
    }
    return NO;
}


-(void) updateDisplay{
    
    bool canChangeCpuCount = [self canChangeCpuCount];
    if (!canChangeCpuCount){
        [allControlsView setHidden:YES];
        [notAvailableWarning setHidden:NO];
    }
    else{
        [notAvailableWarning setHidden:YES];
        [allControlsView setHidden:NO];
        [cpuValue setText:[NSString stringWithFormat:@"%li",(long)[stepperValue value]]];
     }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // enable the screen protection by proximity
    [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
    
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
    [stepperValue setMinimumValue:1];
    [stepperValue setMaximumValue:8];
    [stepperValue setValue:[[self xenVM] VCPUs_at_startup]];
    [stepperValue setStepValue:1];

    [self updateDisplay];
    isVisible = YES;
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    // turn proximity sensor back off again
    [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
    isVisible = NO;
}

-(BOOL)shouldAutorotate{
    return NO;
}

// fired when Max memory stepper value changes
-(IBAction) stepperValueDataChanged:(id) sender{
    [self updateDisplay];
}
    
-(IBAction) PerformDataChange:(id) sender{
    if ([[self xenVM] VCPUs_at_startup] != [stepperValue value]){
        if ([[self xenVM] VCPUs_max] < [stepperValue value]){
            [[self hypervisorConnection] SetVMvMaxCPUCount:(NSInteger)[stepperValue value] forVMRef:[[self xenVM] opaque_ref]];
        }
        progressAlert = [[ProgressAlert alloc] initWithTitle:@"Updating" delegate:self];
        [progressAlert show];
        [[self hypervisorConnection] SetVMvCPUCount:(NSInteger)[stepperValue value] forVMRef:[[self xenVM] opaque_ref]] ;
    }
}

-(void) dealloc{
        [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

-(void) UpdateComplete{
    [statusLabel setHidden:YES];
    [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
    progressAlert = nil;
    if (!isVisible){
        [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    }
}

-(void) UpdateFailed:(NSString*)theIssue{
    [statusLabel setHidden:NO];
    [statusLabel setText:theIssue];
    [statusLabel setTextColor:[UIColor redColor]];
    [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
    progressAlert = nil;
    if (!isVisible){
        [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    }
}


#pragma mark - 
#pragma mark hypervisor Connecton Delegate
-(void) hypervisorResponseReceived:(HypervisorConnectionFactory *)hypervisorConnection requestCmd:(NSString *)cmd error:(NSError *)error{
    if ([cmd isEqualToString:CMD_VM_SET_CPUCOUNT]){
        if (error){
            [self UpdateFailed:[error domain]];
        }
        else{
            [self UpdateComplete];
        }
    }
}
@end
