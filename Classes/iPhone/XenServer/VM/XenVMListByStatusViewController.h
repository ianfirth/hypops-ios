//
//  VMListByStatusViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenHypervisorConnection.h"
#import "PullRefreshTableViewController.h"

#define XEN_VM_ALL 0
#define XEN_VM_TAGGED 1
#define XEN_VM_RUNNING 2
#define XEN_VM_PAUSED 3
#define XEN_VM_HALTED 4
#define XEN_VM_SUSPENDED 5

@interface XenVMListByStatusViewController : UITableViewController <HypervisorConnectionDelegate> {
    NSDictionary *xenVMCatagories;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil hypervisorConnection:(XenHypervisorConnection *)hypervisorConnection;

@property (nonatomic, strong) NSDictionary *xenVMCatagories;
@property (copy) NSString *hypConnID;

@end
