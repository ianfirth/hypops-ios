//
//  VMListViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVMListViewController.h"
#import "XenVmGeneralViewController.h"
#import "XenVMDiskViewController.h"
#import "XenVMPowerViewController.h"
#import "XenVMSnapshotViewController.h"
#import "XenVMConsoleViewController.h"
#import "XenImageBuilder.h"
#import "ImageBuilder.h"
#import "XenVM.h"
#import "IOS6UITabBarController.h"
#import "XenVMNameDescriptionEditorViewController.h"
#import "RootViewController_ipad.h"
#import "MultiLineDetailUITableViewCell.h"
#import "PortraitNavigationController.h"

@interface XenVMListViewController (){
    IOS6UITabBarController *tabBarController;
    UIPopoverController* popover;
}
@end

@implementation XenVMListViewController

#pragma mark -
#pragma mark View lifecycle

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withCondition:(NSPredicate *) condition{
    self = [super initWithHypervisorConnection:hypervisorConnection withCondition:condition forHypObjectType:HYPOBJ_VM  withSearch:YES];
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

// render the cell for the table view
- (void) configureCell:(MultiLineDetailUITableViewCell *)cell forXenObject:(XenBase *)xenObject{
    // set the label text for the cell
    [[cell textLabel2] setText:[(XenDescriptiveBase *)xenObject name_label]];
    // set the detail text for the cell
    [[cell detailTextLabel2] setText:[self descriptionStringForXenObject:xenObject]];
    // set the image for the cell
    [[cell imageView2] setImage:[XenImageBuilder buildVMImageForVM:(XenVM *)xenObject]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
}

-(NSString*) descriptionStringForXenObject:(XenBase *)xenObject{
    return [(XenDescriptiveBase *)xenObject name_description];
}

// called when an object is selected in the list
- (void) didSelectObject:(XenBase *)selectedObject{
    
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    
    // add the general tab
    if (!tabBarController){

        tabBarController = [[IOS6UITabBarController alloc] init];
        [self addChildViewController:tabBarController];  // doing this allows tabbarController to navigationController to be passed down the chain of controllers.

        NSMutableArray *localControllersArray = [[NSMutableArray alloc] initWithCapacity:2];
        
        // add the general tab
        XenVmGeneralViewController *vmGeneral = [[XenVmGeneralViewController alloc] initWithhypervisorConnection: xenHypervisorConnection
                                                                                                              withVM:(XenVM *)selectedObject];
        
        [tabBarController addChildViewController:vmGeneral];
       [localControllersArray addObject:vmGeneral];
        
        
        // add the disks tab
        XenVmDiskViewController *disks = [[XenVmDiskViewController alloc] initWithhypervisorConnection: xenHypervisorConnection
                                                                                                withVM:(XenVM *)selectedObject];
        
        [tabBarController addChildViewController:disks];
        [localControllersArray addObject:disks];
        
        // add the power Control tab
        XenVmPowerViewController *power = [[XenVmPowerViewController alloc] initWithhypervisorConnection: xenHypervisorConnection
                                                                                                  withVM:(XenVM *)selectedObject];
        
        [localControllersArray addObject:power];
        
        // add the snapshot tab
        XenVmSnapshotViewController *snapshots = [[XenVmSnapshotViewController alloc] initWithhypervisorConnection: xenHypervisorConnection
                                                                                                            withVM:(XenVM *)selectedObject];
        
        [tabBarController addChildViewController:snapshots];
        [localControllersArray addObject:snapshots];

        // add the Console tab
        XenVMConsoleViewController *console = [[XenVMConsoleViewController alloc] initWithhypervisorConnection: xenHypervisorConnection
                                                                                                            withVM:(XenVM *)selectedObject];
                
        [tabBarController addChildViewController:console];
        [localControllersArray addObject:console];

        
        tabBarController.viewControllers = localControllersArray;
        
        // the navBar will deal with the title
        [tabBarController setTitle:@""];
    }
    else{
        for (UIViewController<RefreshablePage,Identifyable>* view in [tabBarController viewControllers]){
            // this should be the image general and it should refresh the children ones.
            @try {
                [view setId:[selectedObject opaque_ref]];
                [view refreshPage];
            }
            @catch (NSException *exception) {
                NSLog(@"This should not happen before ship when all controllers updated");
            }
        }
    }

    [self displayNextView:tabBarController];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)titlePressed:(NSString*)theReference{
    XenVMNameDescriptionEditorViewController* c = [[XenVMNameDescriptionEditorViewController alloc]
                                        initWithHypervisorConnectionID: [self hypConnID] andXenVMRef:theReference];
    
    if ([[UIDevice currentDevice].model hasPrefix:@"iPhone"]){
        // appear to navigate into a portrait view by using a new naviagtion controller that only supports Portrait operation
        UIViewController* sec = [[UIViewController alloc] init];
        sec.title = self.title; // to give the correct Back button title
        UINavigationController* nav = [[PortraitNavigationController alloc] initWithRootViewController:sec];
        //SecondViewController* sec2 = [[SecondViewController alloc] init];
        [nav pushViewController:c animated:NO];
        [self presentViewController:nav animated:YES completion:nil];
        nav.delegate = self; // so that we know when the user navigates back
    }
    else{
        popover = [[UIPopoverController alloc] initWithContentViewController:c];
        // permitted arrow direction 0 makes the popover appear without any arrows :)
        CGRect thisFrame = [[UIScreen mainScreen] bounds];
        CGRect pos = CGRectMake(thisFrame.size.width/2 + thisFrame.origin.x, thisFrame.origin.y+40, 10, 10);
        
        UIView* currentView = [tabBarController view];
        
        if ([currentView window] != nil){
          [popover presentPopoverFromRect:pos inView:currentView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
    }
}

// Delgate for retuning from the PortaitNavigation controller
- (void)navigationController:(UINavigationController *)navigationController2
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated {
    if (viewController == [navigationController2.viewControllers objectAtIndex:0])
        [self dismissViewControllerAnimated:YES completion:nil];
}

@end
