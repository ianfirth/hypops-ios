//
//  XenVMSnaphostMainViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVMSnapshotMainViewController.h"
#import "ImageBuilder.h"
#import "HypOpsAppDelegate.h"
#import "AlertWithDataStore.h"
#import "RefreshablePage.h"
#import "XenImageBuilder.h"

@interface tempView:UIView{
    BOOL drawImage;
}

@property BOOL drawImage;

@end

@implementation tempView
@synthesize drawImage;

- (void)drawRect:(CGRect)rect {
    UIImage *snapshotImage2 = [UIImage imageNamed:@"noSnapshots"];
    CGRect rect2 = rect;

    UIDevice *device = [UIDevice currentDevice];
    // ipad portrait
    uint direction = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsPortrait(direction)){
        if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
            // ipad portrait
            rect2.size.width = 400;
            rect2.size.height = 400;
            rect2.origin.x = (rect.size.width - rect2.size.width)/2;
            rect2.origin.y = (rect.size.height - rect2.size.height)/2;
        }
        else{
            // iphone portrait
            rect2.size.width = 200;
            rect2.size.height = 200;            
            rect2.origin.x = (rect.size.width - rect2.size.width)/2;
            rect2.origin.y = (rect.size.height - rect2.size.height)/2;
        }
    }

    if (UIInterfaceOrientationIsLandscape(direction)){
        if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
            // ipad landscape
            rect2.size.width = 400;
            rect2.size.height = 400;
            rect2.origin.x = (rect.size.width - rect2.size.width)/2;
            rect2.origin.y = (rect.size.height - rect2.size.height)/2;
        }
        else{
            // iphone landscape
            rect2.size.width = 120;
            rect2.size.height = 120;            
            rect2.origin.x = (rect.size.width - rect2.size.width)/2;
            rect2.origin.y = 20;
        }
    }

    if (drawImage){
        [snapshotImage2 drawInRect:rect2];
    }
    [super drawRect:rect];
}
@end

@interface XenVMSnapshotMainViewController()<RefreshablePage>{
    BOOL takeActionOnAppear;
}

- (XenHypervisorConnection *) hypervisorConnection;
- (XenVM *) xenVM;
- (XenVM *) snapshotForReference:(NSString*)reference;
- (void)configureCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
-(NSArray*) allXenSnapshots;
-(NSArray*) snapshotsInPathToRoot:(XenVM*)snapshot;
-(void) takeSnapshotwithName:(NSString*)name;
-(void) revertToSnapshot:(NSString*)snapshotRef;
-(void) deleteSnapshot:(NSString*)snapshotRef;
-(void) buildCurrentSnapshotPath;
-(void) reloadData;
-(void) setOverlayPointerXLocation;
@end

@implementation XenVMSnapshotMainViewController

@synthesize xenVMRef, hypConnID;

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM{
    self = [super initWithNibName:@"XenVMSnapshotMainView" bundle:nil];
    if (self){
        [self setXenVMRef: [thisXenVM opaque_ref]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        snapshotSorter = [[NSSortDescriptor alloc] initWithKey:@"snapshot_time" ascending:NO];
        takeActionOnAppear = YES;
    }
    return self;
    
}

-(UIImage*) currentTitleImage {
    return nil;
}

- (void) setId:(NSString*)newID{
    [self setXenVMRef:newID];
    // clear this cache of snapshots so that it is rebuilt correctly.
    snapshotList = nil;
    [self allXenSnapshots];
    [self refreshPage];
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    snapshotList = nil;
    snapshotSorter = nil;
    c = nil;
    
    newSnapshotButton = nil;
    revertToSnapshotButton = nil;
    deleteSnapshotButton = nil;
    currentParentPath = nil;
    selectedXenVMReference = nil;
    overlayView = nil;
    progressAlert = nil;
}

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenVM *) xenVM{
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVMRef]]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }
    return nil;
}

-(NSArray*) allXenSnapshots{
    // this needs to be a cached list, that is cleaned if the object type is updated.
    if (!snapshotList){
         // build the cache
        NSMutableArray* tempSnapshotList = [[NSMutableArray alloc] init];
        NSArray* allSnapshotRefs = [[self xenVM] snapshots];  
         for (NSString* snapshotRef in allSnapshotRefs){
             XenVM *snapshot = [self snapshotForReference:snapshotRef];
             if (snapshot){
                [tempSnapshotList addObject:snapshot];
             }
         }
        
        
         NSMutableArray* tempSortedArray = [[NSMutableArray alloc] initWithArray:[tempSnapshotList sortedArrayUsingDescriptors:[NSArray arrayWithObject:snapshotSorter]]];

        [tempSortedArray insertObject:[self xenVM] atIndex:0];
        snapshotList = tempSortedArray;
                        
    }
    NSLog(@"snapshotlist size %lu",(unsigned long)[snapshotList count]);
    return snapshotList;
}

- (XenVM *) snapshotForReference:(NSString*)reference{
    // try to see if it is a snapshot first
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_SNAPSHOT withCondition:[XenBase XenBaseFor:reference]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }

    // if not a snapshot check to see if it is a VM as the NOW point is a VM
    xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:reference]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }

    return nil;
}

// retrun a list that is in the correct order that lists the snapshots in the path to the root
-(NSArray*) snapshotsInPathToRoot:(XenVM*)snapshot{
    // take the snapshot and get the parent and keep getting parent until there is not one
    NSMutableArray* parentPath = [[NSMutableArray alloc] init];
    NSString* parentRef = [snapshot opaque_ref];
    while (parentRef){
        BOOL located = NO;
        for (XenVM* snapshot in snapshotList){
            if ([[snapshot opaque_ref] isEqualToString:parentRef]){
                located = YES;
            }
        }
        // only add snapshots to the path list if they are in the all snapshots for VM list
        // this stops the VMTemplate that actually was used for the intial vm point being displayed.
        if (located){
           [parentPath addObject:parentRef];
        }
        
        XenVM* parentSnapshot = [self snapshotForReference:parentRef];
        parentRef = [parentSnapshot parent];
        
     }
    return parentPath;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

// this is called by the titledViewController when the manual refresh button
// is pressed and gives the view a chance to redraw properly at the end.
#pragma mark - for RefreshablePage protocol
-(void) refreshPage{
     [self reloadData];
}

#pragma mark - Snapshot operations
-(void) takeSnapshotwithName:(NSString*)name{
    // take snapshot always takes a snapshot from the latest
    [[self hypervisorConnection] RequestNewSnapshot:name forVMReference:xenVMRef];
}

-(void) revertToSnapshot:(NSString*)snapshotRef{
    [[self hypervisorConnection] RequestRevertToSnapshotWithReference:snapshotRef];
}

-(void) deleteSnapshot:(NSString*)snapshotRef{
    [[self hypervisorConnection] RequestDestroyObjectType:HYPOBJ_SNAPSHOT WithReference:snapshotRef];
}

#pragma mark - UIAlertView delegate

- (void)didPresentAlertView:(UIAlertView *)alertView{

    // if it is not a Progress alert then there is no processing to do here.
    if ([alertView isKindOfClass:[ProgressAlert class]]){

        ProgressAlert* alert = (ProgressAlert*)alertView;
    
        NSDictionary* params = [alert parameterMap];
        
        if ([[params objectForKey:@"operation"] isEqualToString:@"create"]){
            [self takeSnapshotwithName:[params objectForKey:@"name"]];
        }
        else if ([[params objectForKey:@"operation"] isEqualToString:@"revert"]){
            [self revertToSnapshot:[params objectForKey:@"reference"]];
        }
        else if ([[params objectForKey:@"operation"] isEqualToString:@"delete"]){
            [self deleteSnapshot:[params objectForKey:@"reference"]];
        }
    }
}
#pragma mark - View lifecycle


- (void)viewDidDisappear:(BOOL)animated{
    // Turn off proximity monitoring so back to normal
    UIDevice *device = [UIDevice currentDevice];
    [device setProximityMonitoringEnabled:NO];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [super viewDidDisappear:animated];
}

-(void)takeSnapshotButton:(id) sender{
    
 
     AlertWithDataStore *myAlertView = [[AlertWithDataStore alloc] initWithTitle:@"Create Snapshot" 
                                                                            message:@"Enter the new snapshot name" 
                                                                           delegate:self 
                                                                  cancelButtonTitle:@"Cancel"
                                                                  otherButtonTitles:@"Create", nil];        
    myAlertView.textField.placeholder = @"snapshot";
    myAlertView.textField.autocorrectionType = UITextAutocorrectionTypeNo;
    myAlertView.textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    myAlertView.textField.secureTextEntry = NO;
    
    [myAlertView setTag:-100];
    [myAlertView show];
}


// process any UIAlert view responses
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    // determine if this is a password box (contains a subView with a tag of -100);
    bool snapshotNameDialog = ([alertView tag] == -100);
    
    if (snapshotNameDialog){
        if (buttonIndex > 0){   // only interested in the create button
            AlertWithDataStore *awds = (AlertWithDataStore*)alertView;
            NSString *snapshotName = [awds textString];
            NSDictionary* params = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    @"create",@"operation",
                                    snapshotName,@"name",nil];
            progressAlert = [[ProgressAlert alloc] initWithTitle:@"Creating" delegate:self parameters:params];
            [progressAlert show];
        }
    }
}

-(void)revertToSnapshotButton:(id) sender{
    NSDictionary* params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            @"revert", @"operation",
                            selectedXenVMReference,@"reference",nil];
    progressAlert = [[ProgressAlert alloc] initWithTitle:@"Reverting" delegate:self parameters:params];
    [progressAlert show];
}

-(void)deleteSnapshotButton:(id) sender{
    NSDictionary* params = [[NSDictionary alloc] initWithObjectsAndKeys:
                            @"delete",@"operation",
                            selectedXenVMReference,@"reference",nil];
    progressAlert = [[ProgressAlert alloc] initWithTitle:@"Deleting" delegate:self parameters:params];
    [progressAlert show];
}

-(void) reloadData{
    // remember the selected one here
//    NSString *selectedRef =  selectedXenVMReference;
    
    selectedXenVMReference = nil;
    currentParentPath = nil;
    snapshotList = nil;
    [[c tableView ] reloadData];
    
//    [overlayView setNeedsDisplay];
//    
//    if (selectedRef){
//        // then if still exists select it and scroll to it
//        for (int i = 0; i< [[self allXenSnapshots] count] ; i++){
//            XenVM* snapshot = [[self allXenSnapshots] objectAtIndex:i];
//            if ([[snapshot opaque_ref] isEqualToString:selectedRef]){
//                // found it
//                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
//                [[c tableView] selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
//                [[[c tableView] delegate] tableView:[c tableView] didSelectRowAtIndexPath:indexPath];
//                [[c tableView] setNeedsDisplay];
//                break;
//            }
//        }
//    }
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {    
    [super viewDidLoad];
    
//    UIImage *snapshotImage = [ImageBuilder snapshotImage];
    [revertToSnapshotButton setEnabled:NO];

//    UIImage *deleteImage = [ImageBuilder deleteImage];
    [deleteSnapshotButton setEnabled:NO];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

/**
 * Set the flag to indicate that the appear operateion that
 * are about to happen should not be acted on
 * they come as part of the hack to force screen to be rotated to the
 * desired orientation on edit screens
 */
-(void) setTakeActionOnAppear:(NSNumber*)takeAction{
    takeActionOnAppear =  [takeAction boolValue];
}

#warning use portrat navigation controller here instead of this bodge
//-(void) viewWillAppear:(BOOL)animated{
//    if (takeActionOnAppear){
//        [super viewWillAppear:animated];
//        if (!UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation])){
//            // ----- force the view to be portrait only
//            //will rotate status bar
//            [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
//            
//            //will re-rotate view according to statusbar
//            UIViewController *temp = [[UIViewController alloc]init];
//            UITabBarController* tabCon = [self tabBarController];
//            for (UIViewController* con in [tabCon viewControllers]){
//                if ([con respondsToSelector:@selector(setTakeActionOnAppear:)]){
//                    [con performSelector:@selector(setTakeActionOnAppear:) withObject:[NSNumber numberWithBool:NO]];
//                }
//            }
//            [self presentViewController:temp animated:NO completion:nil];
//            [self dismissViewControllerAnimated:NO completion:nil];
//            
//            for (UIViewController* con in [tabCon viewControllers]){
//                if ([con respondsToSelector:@selector(setTakeActionOnAppear:)]){
//                    [con performSelector:@selector(setTakeActionOnAppear:) withObject:[NSNumber numberWithBool:YES]];
//                }
//            }
//            
//            // ---- end force view to be portrait only
//        }
//    }
//}

//-(BOOL)shouldAutorotate{
//    return NO;
//}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return NO;
//}

//-(void) setOverlayPointerXLocation{
//    UIDevice *device = [UIDevice currentDevice];
//    // ipad portrait
//    uint direction = [UIApplication sharedApplication].statusBarOrientation;
//    if (UIInterfaceOrientationIsPortrait(direction)){
//        if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
//            xLocation = [[self view] frame].size.width - 60;  //ipad
//        }else{
//            xLocation = [[self view] frame].size.width - 25;  //iPhone
//        }
//    }
//    if (UIInterfaceOrientationIsLandscape(direction)){
//        if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
//            xLocation = [[self view] frame].size.width - 57;  //iPad
//        }
//        else{
//            xLocation = [[self view] frame].size.width - 25;  //iPhone
//        }
//    }
//}

//-(void) viewDidAppear:(BOOL)animated{
//    if (takeActionOnAppear){
//        [super viewDidAppear:animated];
//        
//        [self ensureObjectTreeLoaded];
//        
//        // Turn on proximity monitoring  this will shut off screen when something is close by
//        // stops accedently powering thing on/off when device is in pocket etc..
//        UIDevice *device = [UIDevice currentDevice];
//        [device setProximityMonitoringEnabled:YES];

//        [self setOverlayPointerXLocation];
//        
//NSUInteger  indexArr[] = {0,0};
//        
//        NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:indexArr length:2];
//        UITableViewCell *cell = [[c tableView] cellForRowAtIndexPath:indexPath];
//        //    UITableViewCell *cell = [[c tableView] cellForRowAtIndexPath:[[[c tableView] indexPathsForVisibleRows] objectAtIndex:0]];
//        yStartLocation = [cell frame].origin.y + 25;
//        
//        CGRect f = [[self view] frame];
//        [[c view] setFrame: CGRectMake(0, 50.0, f.size.width, f.size.height -50)];
//        [overlayView setFrame:CGRectMake(0, 50.0, f.size.width, f.size.height -50)];
//        
//        [[c view] setAutoresizingMask: UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
//        [overlayView setAutoresizingMask: UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
//        
//        [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
//        [overlayView setNeedsDisplay];
//    }
//}


#pragma mark -
#pragma mark Table view data source

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    // does not have a title
    return @"";
}

// one row for each snapshot
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // this needs to be all snapshots, including snapshots in the children etc..
    // and includes the extra NOW cell
    if ([[self allXenSnapshots] count] == 1){
        return 0;
    }
    return [[self allXenSnapshots] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // make the top row always say Now, so make this a special cell that can be rendered in its own way
    NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    [self configureCell:cell atIndex:[indexPath row]];
    return cell;
}
        
- (void)configureCell:(UITableViewCell *)cell atIndex:(NSInteger) index {    
    // this bit is the same regardless if it is the 
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];

    // remove cells from subview with tag of -200 here
    [[[cell contentView] viewWithTag:-200] removeFromSuperview];
    
    // use this to put data onto the cell    
    XenVM *snapshot = [[self allXenSnapshots] objectAtIndex:index]; // first cell is now
    if ([snapshot vmType] ==   XENVMTYPE_VM){
        [[cell textLabel] setText:@"Now"];        
        [[cell detailTextLabel] setText:@"Current"];
    }else{
        [[cell textLabel] setText:[snapshot name_label]];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss"];
        NSString* displayDate = [dateFormatter stringFromDate:[snapshot snapshot_time]];
        [[cell detailTextLabel] setText:displayDate];
    }
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    
    yLocation = [cell frame].origin.y;
    
    if (selectedXenVMReference){
        selectedXenVMReference = nil;
    }
    selectedXenVMReference = [[snapshotList objectAtIndex:[indexPath row]] opaque_ref];
    
    XenVM* currentSnapshotSelected = nil;
    currentSnapshotSelected = [snapshotList objectAtIndex:[indexPath row]];
   
    if ([currentSnapshotSelected vmType] ==   XENVMTYPE_VM){
        // this is the NOW
        [deleteSnapshotButton setEnabled:NO];
        [revertToSnapshotButton setEnabled:NO];
    }
    else{
        // I think that these are all always available but need to check that this is the case
        [deleteSnapshotButton setEnabled:YES];
        [revertToSnapshotButton setEnabled:YES];
    }

 
    // build an array of the snaphsots in the linear path back to the root
    [self buildCurrentSnapshotPath];
//    [overlayView setNeedsDisplay];
}

-(void) buildCurrentSnapshotPath{
    // build an array of the snaphsots in the linear path back to the root
    if(currentParentPath){
        currentParentPath = nil;
    }
    
   XenVM*  currentSnapshotSelected = [self snapshotForReference:selectedXenVMReference];
    
   currentParentPath = [self snapshotsInPathToRoot:currentSnapshotSelected];
}

#pragma mark -
#pragma mark TableViewRelationShipDataSource

-(NSInteger) getAllPointCount{
    return [snapshotList count];
}

-(float) getAllPointIntervalForPoint:(NSInteger)pointNum{
    if (pointNum == 0){
        return 44;
    }
    return 45;
}

// return the number of points that there are in the past to draw
-(NSInteger) getPastPointCount{
    return [currentParentPath count];
}

-(CGPoint) getAllPointStartPoint{
    return CGPointMake(xLocation, yStartLocation);
}

// get the point at the required position
- (CGPoint) pointAt:(NSInteger)pointNum{    
    // assume that all cells are the same height for this to work
    // convert a snapshot to a positon in the all snapshot list


    // find the first match this is the start then find out how many further down the list the actual one is
    NSUInteger startIndex = 0;
    NSString *ref= [currentParentPath objectAtIndex:0];
    for (int i=0; i < [snapshotList count] ; i++){ 
        XenVM* snapshot =  [snapshotList objectAtIndex:i];
        if ([[snapshot opaque_ref] isEqualToString: ref]){
            startIndex = i;  
        }
    }    
    
    ref= [currentParentPath objectAtIndex:pointNum];
    
    // find the first match this is the start then find out how many further down the list the actual one is
    for (int i=0; i < [snapshotList count] ; i++){ 
        XenVM* snapshot =  [snapshotList objectAtIndex:i];
        if ([[snapshot opaque_ref] isEqualToString: ref]){
            NSUInteger difference = i - startIndex;
            
            // first table cell is 1 point higher than all the other cells.
            NSUInteger tableCellHeight = 44;
            if (i == 0){
                tableCellHeight = 45;
            }
            float yposition = yLocation + (difference * tableCellHeight) + 25;
            CGPoint point = CGPointMake(xLocation,  yposition);  
            return point;
        }
    }
    return CGPointMake(0,0);  // if get here something has gone wrong
}

#pragma mark -
#pragma mark HypervisorCOnnectionDelegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    if ( objectType == HYPOBJ_VM){
        if ([objectRef isEqualToString:[[self xenVM] opaque_ref]]){
            [self reloadData];
        }
    }
    if ( objectType == HYPOBJ_SNAPSHOT){
        // deal with delete here, I am surprised that the VM is not updated to reflect the change in the
        // snapshots but it does not appear to be.
        [self reloadData];
    }
}

- (void)hypervisorResponseReceived:(HypervisorConnectionFactory *)hypervisorConnection requestCmd:(NSString*)cmd error:(NSError*)error{
    if ([cmd isEqualToString:@"VM.snapshot"] ||
        [cmd isEqualToString:@"VM.revert"] ||
        [cmd isEqualToString:@"VM.destroy"]){
        // make sure that the cache has been updated before processing the screen
        [self performSelector:@selector(responsePostProcess) withObject:nil afterDelay:0.1];
    }
}

-(void) responsePostProcess{
    if (progressAlert){
        // if in manual mode do a refresh of the VM here to make sure that the snapshot list is correct
        if (![[self hypervisorConnection] isAutoUpdateEnabled]){
            [[self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_SNAPSHOT];
            [[self hypervisorConnection] RequestHypObject:[self xenVMRef] ForType:HYPOBJ_VM];
        }
        [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
        progressAlert = nil;
    }
    [self reloadData];
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    CGPoint point = [scrollView contentOffset]; 
//    [overlayView setContentOffset:point animated:false];
//    [overlayView setNeedsDisplay];
}

#pragma mark -
#pragma mark UIWaitForDownload delegate

- (void)dataLoaderFinished:(UIWaitForDataLoad *)dataLoader withResult:(BOOL)sucsess{
    NSLog(@"dataloader reported that it has loaded the required data.");
    
    NSError *err = [dataLoader connectionError];
    
    if (err){
        // if there was an error connecting do not do anyting.
        return;
    }
    
    // also invalidate the extention page so it redraws now all the data is available
    UITabBarController *tabBarController = [self tabBarController];
    // reload all tables in all views in the table view controller.
    if (tabBarController){
        NSArray *allTabbedControllers = [tabBarController viewControllers];
        for (UIViewController *vc in allTabbedControllers){
            if ([vc conformsToProtocol:@protocol(RefreshablePage)]){
                UIViewController<RefreshablePage>  *vcRefresh= (UIViewController<RefreshablePage>  *)vc;
                [vcRefresh refreshPage];
            }
        }
    }
    
    [self refreshPage];
    
    [[self hypervisorConnection] setWalkTreeMode:NO];
}


-(void)ensureObjectTreeLoaded{
    HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    
    // link in the UIDataload to make sure that the object has all its children
    
    NSPredicate *objectPred = [XenBase XenBaseFor:[self xenVMRef]];
    
    NSArray *objects = [hypervisorConnection hypObjectsForType:HYPOBJ_VM withCondition:objectPred];
    if ([objects count] >0){
        [hypervisorConnection setWalkTreeMode:YES];
        UIWaitForDataLoad *dataLoader = [[UIWaitForDataLoad alloc] initWithHypervisorConnection:hypervisorConnection];
        [dataLoader setUIWaitForDataLoadDelegate:self];
        [dataLoader waitForObjectTreeForObjectReference:[self xenVMRef] objectType:HYPOBJ_VM];
    }
}



@end
