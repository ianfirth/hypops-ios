//
//  XenHostNicsViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVmGeneralViewController.h"

#import "XenHypervisorConnection.h"
#import "XenImageBuilder.h"
#import "SectionTitleViewController.h"

@interface XenVmGeneralViewController (){
    SectionTitleViewController* headerViewController;
}

@end

@implementation XenVmGeneralViewController

@synthesize headerView;
@synthesize tableViewController;
@synthesize mainTableView;

@synthesize hypConnID, xenVMRef;

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM{
    self = [super initWithNibName:@"XenVmGeneralViewController" bundle:nil];
    if (self){
        [self setXenVMRef: [thisXenVM opaque_ref]];
        [self setHypConnID: [hypervisorConnection connectionID]];

        XenVMGeneralTableViewController* tvc =[[XenVMGeneralTableViewController alloc] initWithhypervisorConnection:hypervisorConnection withVM:thisXenVM];
        [self addChildViewController:tvc];
        [self setTableViewController:tvc];

        // set the tab bar button details
        self.tabBarItem.title = @"General";
        //Set the image for the tab bar
        self.tabBarItem.image = [UIImage imageNamed:@"tab_General"];
    }
    return self;
}

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenVM *) xenVM{
    NSArray *xenVms = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVMRef]]];
    if ([xenVms count] >0){
        return [xenVms objectAtIndex:0];
    }
    return nil;
}

- (void)viewDidLoad
{    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    headerViewController = [[SectionTitleViewController alloc] initWithNibName:@"SectionTitleViewController" bundle:[NSBundle mainBundle] ];
    
    [[self headerView] addSubview:[headerViewController view]];
    [[self mainTableView] addSubview:[[self tableViewController] tableView]];
    [[tableViewController tableView] setAutoresizingMask: UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    CGRect tableViewArea = [[self mainTableView] frame];
    tableViewArea.origin.x = 0;
    tableViewArea.origin.y =0;
    // same size as parent but seeing as its insize it make x and y =0
    [[headerViewController view] setFrame:[[self headerView] frame]];
    [[[self tableViewController] tableView] setFrame:tableViewArea];
    [self updatePageHeading];
}

-(void) updatePageHeading{
    [headerViewController setTheTitle:[[self xenVM] name_label]];
    [headerViewController setTheDescription:[[self xenVM] name_description]];
    [headerViewController setTheImage:[self currentTitleImage]];
}

- (UIImage*) currentTitleImage{
    XenVM *xenVM = [self xenVM];
    return [XenImageBuilder buildVMImageForVM:xenVM];
}


#pragma mark for Identifyable protocol
-(void) setId:(NSString *)newID{
    [self setXenVMRef:newID];
    [tableViewController setId:newID];
}

#pragma mark for RefreshablePage protocol
-(void) refreshPage{
    [[[self tableViewController] tableView] reloadData];
    [self updatePageHeading];
}

@end
