//
//  XenVmMemoryDelegateImp.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVmMemoryDelegateImpl.h"
#import "DataSource.h"
#import "MemoryNumberFormatter.h"

@implementation XenVmMemoryDelegateImpl

@synthesize hypConnID, xenVmRef;

- (id)initWithhypervisorConnectionID:(NSString *)hypervisorConnectionID withVm:(XenVM *)thisXenVm{
    self = [super init];
    if (self){
        [self setXenVmRef: [thisXenVm opaque_ref]];
        [self setHypConnID: hypervisorConnectionID];
    }
    return self;
}


-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenVM *) xenvm{
    NSArray *xenVms = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVmRef]]];
    if ([xenVms count] >0){
        return [xenVms objectAtIndex:0];
    }
    return nil;
}

#pragma mark -
#pragma mark RRDDisplayButtonDelegate

- (RoundRobinDatabase *)roundRobinDatabase{
    XenVM *xenVm = [self xenvm];
    return [xenVm vmPerformanceData];
}

- (BOOL) reloadRoundRobinDatabase{
    XenVM *xenVm = [self xenvm];
    return [xenVm refreshPerformanceData];
}

-(NSDate*) getLastRefreshTime{
    XenVM *xenVm = [self xenvm];
    return [xenVm lastPreformanceDataUpdateTime];
}

-(NSString*) graphTitleWithReference:(NSString*)sourcereference{
    XenVM *xenVm = [self xenvm];
    return [NSString stringWithFormat:@"Memory usage for %@", [xenVm name_label]];
}

-(NSArray*)dataSourcesToGraphFromRRD:(RoundRobinDatabase*)rrd withReference:(NSString*)sourceReference{
    NSMutableArray* sourceArray = [[NSMutableArray alloc] initWithCapacity:1];
    for (DataSource* dataSource in [rrd dataSources]) {
        if ([[dataSource name] isEqualToString:@"memory"]){
            [dataSource setAlternateName:@"Used memory"];
            [sourceArray addObject:dataSource];
        }
    }
    return sourceArray;
}

-(double)yMinForDataSources:(NSArray*)dataSources withConsolidationStep:(NSInteger)step{
    return 0;
}

-(double) getPlotPointFromDataPoint:(DataPoint*)dataPoint{
    return ([dataPoint average]);  
}

-(NSString*)yTitle{
    return @"";
}


-(double)roundup:(double)inputNumber{
    // round up the the value to the nearest value on a 10's boundary
    NSUInteger resultInt = inputNumber;
    NSUInteger u = 0;
    while (resultInt > 1){
        resultInt /= 10;
        u ++;
    };

    float divisor = pow(10,u-1);
    float resultPower = inputNumber / divisor;
    // round this and multiply by u power 10;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:0];
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    
    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithFloat:resultPower]];
    return [numberString intValue] * divisor;
}

-(double)yMaxForDataSources:(NSArray*)dataSources withConsolidationStep:(NSInteger)step{
    double result = 0;
    for (DataSource* source in dataSources) {
        NSLog(@"DataSourceName = %@",[source name]);
        NSLog(@"Consolidation Step = %ld", (long)step);
        for (DataPoint* point in [source dataPointsForConsolodatedStepCount:step]) {
            double val = [self getPlotPointFromDataPoint:point];
            if (val>result){
                result = val;
            }
        }
    }
    return [self roundup:result];
}


-(NSNumberFormatter*) yFormatter{
    // number formatter in bytes
    NSNumberFormatter* formatter = [[MemoryNumberFormatter alloc] initWithBasereference:1];
    [formatter setLocale:[NSLocale currentLocale]];
    return formatter;
}
@end
