//
//  XenVmCpuDelegateImpl.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVmCpuDelegateImpl.h"
#import "DataSource.h"
#import "MemoryNumberFormatter.h"

@implementation XenVmCpuDelegateImpl

@synthesize hypConnID, xenVmRef;

- (id)initWithhypervisorConnectionID:(NSString *)hypervisorConnectionID withVm:(XenVM *)thisXenVm{
    self = [super init];
    if (self){
        [self setXenVmRef: [thisXenVm opaque_ref]];
        [self setHypConnID: hypervisorConnectionID];
    }
    return self;
}


-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenVM *) xenVm{
    NSArray *xenVms = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVmRef]]];
    if ([xenVms count] >0){
        return [xenVms objectAtIndex:0];
    }
    return nil;
}

#pragma mark -
#pragma mark RRDDisplayButtonDelegate

- (RoundRobinDatabase *)roundRobinDatabase{
    XenVM *xenVm = [self xenVm];
    return [xenVm vmPerformanceData];
}

- (BOOL) reloadRoundRobinDatabase{
    XenVM *xenVm = [self xenVm];
    return [xenVm refreshPerformanceData];
}

-(NSDate*) getLastRefreshTime{
    XenVM *xenVm = [self xenVm];
    return [xenVm lastPreformanceDataUpdateTime];
}

-(NSString*) graphTitleWithReference:(NSString*)sourcereference{
    XenVM *xenVm = [self xenVm];
    return [NSString stringWithFormat:@"CPU usage for %@", [xenVm name_label]];
}


-(NSArray*)dataSourcesToGraphFromRRD:(RoundRobinDatabase*)rrd withReference:(NSString*)sourceReference{
    NSMutableArray* sourceArray = [[NSMutableArray alloc] initWithCapacity:1];
    for (DataSource* dataSource in [rrd dataSources]) {
        if ([[dataSource name] hasPrefix:@"cpu"]){
            [dataSource setAlternateName:[dataSource name]];
            [sourceArray addObject:dataSource];
        }
    }
    return sourceArray;
}

-(double)yMinForDataSources:(NSArray*)dataSources withConsolidationStep:(NSInteger)step{
    return 0;
}

-(double)yMaxForDataSources:(NSArray*)dataSources withConsolidationStep:(NSInteger)step{
    return 1.0;
}

-(NSString*)yTitle{
    return @"";
}

-(double) getPlotPointFromDataPoint:(DataPoint*)dataPoint{
    // beacuse using a minmanl RRD all points are the same
    return [dataPoint average];
//    double displayVal = [dataPoint average];
//    if (displayVal == 0){
//        displayVal = [dataPoint max];
//    }
//    return displayVal;    
}

-(NSNumberFormatter*) yFormatter{
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterPercentStyle];
    // Set to the current locale
    [formatter setLocale:[NSLocale currentLocale]];
    return formatter;
}
@end
