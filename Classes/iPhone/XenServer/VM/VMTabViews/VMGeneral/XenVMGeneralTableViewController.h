//
//  XenVMGeneralViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVM.h"
#import "XenHypervisorConnection.h"
#import "ExpandableTableViewController.h"
#import "RRDDisplayButton.h"
#import "Identifyable.h"
#import "RefreshablePage.h"
#import "UIWaitForDataLoad.h"

#define XENVM_SECTION_GENERAL 0
#define XENVM_SECTION_MEMORY 1
#define XENVM_SECTION_CPU 2
#define XENVM_SECTION_BOOTORDER 3
#define XENVM_SECTION_TAGS 4
#define XENVM_SECTION_COUNT 5

// general table
#define XENVMDETAILSTABLESTARTTIME 0
#define XENVMDETAILSTABLEVIRTUALIZATIONSTATE 1
#define XENVMDETAILSTABLEOS 2
#define XENVMDETAILSTABLEHOST 3
#define XENVMDETAILSTABLEBIOSSTRINGCOPY 4
#define XENVMDETAILSTABLEAUTOBOOT 5
#define XENVMDETAILSTABLEIPADDRESS 6
#define XENVMDETAILSTABLECOUNT 7

// Memory table
#define XENVM_MEMORY_TABLE_AVAILABLE 0   // static MAX
#define XENVM_MEMORY_TABLE_PERFORMANCEDATA 1
#define XENVM_MEMORY_TABLE_COUNT 2

// CPU Table
#define XENVM_CPU_TABLE_CPUCOUNT 0
#define XENVM_CPU_TABLE_PERFORMANCEDATA 1
#define XENVM_CPU_TABLE_COUNT 2


//number of CPUS
// is using intellicache ( are there any intelli cache stats?  if so possbly in the disk section?
//cpu usage performance data
// memory usage performance data

@interface XenVMGeneralTableViewController :ExpandableTableViewController
            <HypervisorConnectionDelegate,
            Identifyable,RefreshablePage,
            UINavigationControllerDelegate,
            UIWaitForDataLoadDelegate>{
    NSString *xenVMRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;
        
    RRDDisplayButton* rrdCPUButton;
    RRDDisplayButton* rrdMemoryButton;

	// for the memory tableViewCell
    UITableViewCell* memoryTableViewCell;
    UILabel* dynMaxMemoryTitleLabel;
    UILabel* dynMaxMemoryValueLabel;
    UILabel* dynMinMemoryTitleLabel;
    UILabel* dynMinMemoryValueLabel;
    UILabel* availableMemoryTitleLabel;
    UILabel* availableMemoryValueLabel;
}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

// this is used by relection in the tab contol to update the page.  In this class it updates the xenVMRef.
- (void) setId:(NSString*)newID;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenVMRef;
@property IBOutlet UITableViewCell* memoryTableViewCell;
@property IBOutlet UILabel* totalMemoryValueLabel;
@property IBOutlet UILabel* dynMaxMemoryTitleLabel;
@property IBOutlet UILabel* dynMinMemoryTitleLabel;
@property IBOutlet UILabel* dynMaxMemoryValueLabel;
@property IBOutlet UILabel* dynMinMemoryValueLabel;
@property IBOutlet UILabel* availableMemoryTitleLabel;
@property IBOutlet UILabel* availableMemoryValueLabel;

@end
