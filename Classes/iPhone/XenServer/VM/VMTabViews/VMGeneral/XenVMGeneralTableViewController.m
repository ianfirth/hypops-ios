//
//  XenVMGeneralViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#import "XenVMGeneralTableViewController.h"
#import "XenImageBuilder.h"
#import "XenVM_guestMetrics.h"
#import "ScrollableDetailTextCell.h"
#import "NetworkAddress.h"
#import "RRDDisplayButton.h"
#import "XenVmCpuDelegateImpl.h"
#import "XenVmMemoryDelegateImpl.h"
#import "XenHost.h"
#import "XenVM_metrics.h"
#import "XenVMMemoryEditiorViewController.h"
#import "XenVMAutoBootEditorViewController.h"
#import "XenVMCPUCountEditorViewController.h"
#import "XenVMNameDescriptionEditorViewController.h"
#import "PortraitNavigationController.h"
#import "XenImageBuilder.h"
#import "UIWaitForDataLoad.h"
#import "RefreshablePage.h"

#define RRDButtonCell @"RRD"
#define GeneralCell @"General"


@interface XenVMGeneralTableViewController (){
    UIPopoverController* popover;
    BOOL takeActionOnAppear;
}
    - (UITableViewCell *) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier;
    - (UITableViewCell *) configureDetailsCellForTableView:(UITableView *)tableView atIndex:(NSInteger) index;
    - (UITableViewCell *) configureBootOrderCellForTableView:(UITableView *)tableView atIndex:(NSInteger) index;
    - (UITableViewCell *) configureCPUCellForTableView:(UITableView *)tableView atIndex:(NSInteger) index;
    - (UITableViewCell *) configureMemoryCellForTableView:(UITableView *)tableView atIndex:(NSInteger) index;
    - (UITableViewCell *) configureTagsCellForTableView:(UITableView*)tableView atIndex:(NSInteger) index;
    - (UITableViewCell *) getMemoryCelltableView:(UITableView *)tableView;
    -(UITableViewCell*) buildMemoryCellForTableView:(UITableView*)tableView;
@end

@implementation XenVMGeneralTableViewController

@synthesize hypConnID, xenVMRef;
@synthesize memoryTableViewCell;
@synthesize dynMaxMemoryTitleLabel;
@synthesize dynMinMemoryTitleLabel;
@synthesize dynMaxMemoryValueLabel;
@synthesize dynMinMemoryValueLabel;
@synthesize availableMemoryValueLabel;
@synthesize availableMemoryTitleLabel;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenVM *) xenVM{
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVMRef]]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }
    return nil;
}

- (XenVM_metrics *) xenVMMetrics{
    NSArray *opRefs = [[self xenVM] referencesForType:HYPOBJ_VM_METRICS];
    if (opRefs == nil || [opRefs count] ==0)
    {
        return nil;
    }
    else
    {
        return [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM_METRICS withCondition:[XenBase XenBaseFor:[opRefs objectAtIndex:0]]] objectAtIndex:0];
    }
}


- (id)initWithhypervisorConnection: (XenHypervisorConnection *)hypervisorConnection
                            withVM: (XenVM *)thisXenVM
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setXenVMRef: [thisXenVM opaque_ref]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        takeActionOnAppear = YES;
    }
    return self;
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

// this is now mandatory in ios7
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (UITableViewCell *) getMemoryCelltableView:(UITableView *)tableView
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"memoryCell"];
    if (cell == nil) {
		
       [[NSBundle mainBundle] loadNibNamed:@"MemoryTableCellView" owner:self options:nil];
        cell = [self memoryTableViewCell];
    }
    return cell;
}
                  
// provide access to the cell definitions
-(UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }

    // general cells can have different components in them remove them if there are
    if ([identifier isEqualToString: GeneralCell]){
        [[[cell contentView] viewWithTag:100] removeFromSuperview];
    }
    return cell;    
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
    
    //this prevent the tabbar to cover the tableview space
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 90)];
    footer.backgroundColor = [UIColor clearColor];
    [self tableView].tableFooterView = footer;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
    // Releases the view if it doesn't have a superview.
    // this page does any memory cleen up possible for host objects
    // this will get called if any of the host tabs is visible
    // clean up any RRD data for any Hosts snce they cannot be in use at present
    // clean up any RRD data for any other vms that are not the one currently in use
    // -- Future enhancement if required --> unless the graph is not in us anyway inwhich case can clean up RRD data for this vm too
    
    NSArray* vms = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM];
    XenVM* currentVm = [self xenVM];
    for (XenVM* vm in vms) {
        if (![[vm opaque_ref] isEqualToString:[currentVm opaque_ref]]){
            [vm clearVmPerformanceData];
        }
    }
    
    NSArray* hosts = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST];
    for (XenHost* host in hosts) {
        [host clearHostPerformanceData];
    }
    
    [super didReceiveMemoryWarning];

}

- (void)viewDidUnload {
    [super viewDidUnload];
}

/**
 * Set the flag to indicate that the appear operateion that
 * are about to happen should not be acted on
 * they come as part of the hack to force screen to be rotated to the
 * desired orientation on edit screens
 */
-(void) setTakeActionOnAppear:(NSNumber*)takeAction{
    takeActionOnAppear =  [takeAction boolValue];
}

-(void) viewDidAppear:(BOOL)animated{

    if (takeActionOnAppear){
        [self ensureObjectTreeLoaded];
        [super viewDidAppear:animated];
    }
}

- (void) viewWillAppear:(BOOL)animated{
    if (takeActionOnAppear){
        [super viewWillAppear:animated];
        [[self tableView] reloadData];
        XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
        [xenHypervisorConnection addHypervisorConnectionDelegate:self];
    }
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to vm objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    // could cut this down to just the relevent ones if required
    if (objectType == HYPOBJ_VM_GUEST_METRICS){
        [[self tableView ]reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return XENVM_SECTION_COUNT;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // first section is always expanded
    return (section != 0);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case XENVM_SECTION_MEMORY:
            switch (indexPath.row){
                case XENVM_MEMORY_TABLE_AVAILABLE:
                    if ([[self xenVM] isMemoryDynamic]){
                        return 132;  // complex dynamic cell
                    }
                    else{
                        return 45;  // simple static cell
                    }
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case XENVM_SECTION_GENERAL:
            return nil; // No heading required for first section
            break;
        case XENVM_SECTION_MEMORY:
            return @"Memory";
            break;
        case XENVM_SECTION_CPU:
            return @"CPU";
            break;
        case XENVM_SECTION_BOOTORDER:
            return @"Boot Order";
            break;
        case XENVM_SECTION_TAGS:
            return @"Tags";
            break;

        default:
            break;
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    switch (section) {
        case XENVM_SECTION_GENERAL:
            return XENVMDETAILSTABLECOUNT;
            break;
        case XENVM_SECTION_MEMORY:
            return XENVM_MEMORY_TABLE_COUNT;
            break;
        case XENVM_SECTION_CPU:
            return XENVM_CPU_TABLE_COUNT;
            break;
        case XENVM_SECTION_BOOTORDER:
            return [[[self xenVM] bootOrder] count];
            break;
        case XENVM_SECTION_TAGS:
            // other section counts
            // Should we always add a spare row on the end to allow a tag to be added to the list?
            // It could have a '+' icon and text that says 'Add a new tag'
            // unless you can add buttons by the headings easily then I will add a plus there
            // this could pop up a list of all tags availalble and allow entry of a new one
            // Note this would be all tags, not just host tags.  Possibly need to keep an array
            // with all tags in aswell as the ones for each VM etc..
            return [[[self xenVM] tags] count];
            break;
        default:
            break;
    }
    return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Configure the cell...
    switch ([indexPath section]) {
        case XENVM_SECTION_GENERAL:
            return [self configureDetailsCellForTableView:tableView atIndex:[indexPath row]];
            break;
        case XENVM_SECTION_MEMORY:
            return[self configureMemoryCellForTableView:tableView atIndex:[indexPath row]];
            break;
        case XENVM_SECTION_CPU:
            return [self configureCPUCellForTableView:tableView atIndex:[indexPath row]];
            break;
        case XENVM_SECTION_BOOTORDER:
            return [self configureBootOrderCellForTableView:tableView atIndex:[indexPath row]];
            break;
        case XENVM_SECTION_TAGS:
            return [self configureTagsCellForTableView:tableView atIndex:[indexPath row]];
            break;

        default:
            break;
    }
    return nil;
}

-(UITableViewCell*) buildMemoryCellForTableView:(UITableView*)tableView{
    
    // show 2 modes
    // if in static mode (Static max, dynamic max and dynamic min all equal)
    //  show total memory... which is static max
    // if in dynamic mode
    // show static Max
    // Dynamic Max
    // Dynamic min
    
    // also show memory in use from the VM Metrics object
    
    // (can indicate if values are to be applied next boot or if currently valid
    // does changing max auto reboot the vm (XenCeter does).
    
    // in edit box show a tick box to set as static or dynamic
    // and then allow edit of static max, dynamic max and dyamix min

    
    XenVM *xenVM = [self xenVM];
    UITableViewCell* cell = [self getMemoryCelltableView:tableView];
    if ([xenVM isMemoryDynamic]){
        [[self dynMaxMemoryValueLabel] setHidden:NO];
        [[self dynMinMemoryValueLabel] setHidden:NO];
        [[self dynMaxMemoryTitleLabel] setHidden:NO];
        [[self dynMinMemoryTitleLabel] setHidden:NO];
        [[self availableMemoryValueLabel] setHidden:NO];
        [[self availableMemoryTitleLabel] setHidden:NO];
        
        // add the actual this is the amount that is not stolen by memory baloon (I Think).
        NSNumber *memActualBytes = [[self xenVMMetrics] memory_actual];
        long long memActual = [memActualBytes longLongValue];NSUInteger finalMemActualMb = ((float)memActual)/1024/1024;
        [[self availableMemoryValueLabel] setText:[NSString stringWithFormat:@"%lu Mb",(unsigned long)finalMemActualMb]];

        // dynamic Max
        NSNumber *memDynamicMaxBytes = [xenVM memory_dynamic_max];
        long long mem = [memDynamicMaxBytes longLongValue];NSUInteger finalmem = ((float)mem)/1024/1024;
        [[self dynMaxMemoryValueLabel] setText:[NSString stringWithFormat:@"%lu Mb",(unsigned long)finalmem]];

        // dynamic Min
        NSNumber *memDynamicMinBytes = [xenVM memory_dynamic_min];
        mem = [memDynamicMinBytes longLongValue];
        finalmem = ((float)mem)/1024/1024;
        [[self dynMinMemoryValueLabel] setText:[NSString stringWithFormat:@"%lu Mb",(unsigned long)finalmem]];

    }else{
        [[self dynMaxMemoryValueLabel] setHidden:YES];
        [[self dynMinMemoryValueLabel] setHidden:YES];
        [[self dynMaxMemoryTitleLabel] setHidden:YES];
        [[self dynMinMemoryTitleLabel] setHidden:YES];
        [[self availableMemoryValueLabel] setHidden:YES];
        [[self availableMemoryTitleLabel] setHidden:YES];
    }
    

    NSNumber *memTotalBytes = [xenVM memory_static_max];
    long long memTotal = [memTotalBytes longLongValue];NSUInteger finalmemTotalMb = ((float)memTotal)/1024/1024;
    [[self totalMemoryValueLabel] setText:[NSString stringWithFormat:@"%lu Mb",(unsigned long)finalmemTotalMb]];

    return cell;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (UITableViewCell *)configureMemoryCellForTableView:(UITableView *)tableView atIndex:(NSInteger) index {
    
    UITableViewCell* cell;
    
    switch (index) {
        case XENVM_MEMORY_TABLE_AVAILABLE:{  
            cell = [self buildMemoryCellForTableView:tableView];
            [cell setAccessoryType:UITableViewCellAccessoryDetailDisclosureButton];
            break;
        }
        case XENVM_MEMORY_TABLE_PERFORMANCEDATA:{
            cell = [self getCellForTableView:tableView andIdentifier:RRDButtonCell];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [[cell detailTextLabel] setText:@""];
            [[cell textLabel] setText:@""];
            if (!rrdMemoryButton){
                rrdMemoryButton = [[RRDDisplayButton alloc] init];
                [self addChildViewController:rrdMemoryButton];
                XenVmMemoryDelegateImpl *delegate = [[XenVmMemoryDelegateImpl alloc] initWithhypervisorConnectionID:[self hypConnID] withVm:[self xenVM]];
                [rrdMemoryButton setDelegate:delegate];
                [[rrdMemoryButton view ]setTag:100];
            }
            [[rrdMemoryButton view ]setFrame:[[cell contentView] bounds]];
            [[cell contentView] setAutoresizesSubviews:YES];
            [[cell contentView] addSubview:[rrdMemoryButton view]];
            [rrdMemoryButton viewWillAppear:YES];
            break;
        }
        default:
            break;
    }    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];

    return cell;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (UITableViewCell *)configureCPUCellForTableView:(UITableView *)tableView atIndex:(NSInteger) index {
    XenVM *xenVM = [self xenVM];
    
    UITableViewCell* cell = nil;
    switch (index) {
        case XENVM_CPU_TABLE_CPUCOUNT:
        {
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [cell setAccessoryType:UITableViewCellAccessoryDetailDisclosureButton];
            [[cell textLabel] setText:@"CPUs"];
            [[cell detailTextLabel] setText: [NSString stringWithFormat:@"%lu", (unsigned long)[xenVM VCPUs_at_startup]]];
            break;
        }
        case XENVM_CPU_TABLE_PERFORMANCEDATA:{
            cell = [self getCellForTableView:tableView andIdentifier:RRDButtonCell];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [[cell detailTextLabel] setText:@""];
            [[cell textLabel] setText:@""];
            if (!rrdCPUButton){
                rrdCPUButton = [[RRDDisplayButton alloc] init];
                [self addChildViewController:rrdCPUButton];
                XenVmCpuDelegateImpl *delegate = [[XenVmCpuDelegateImpl alloc] initWithhypervisorConnectionID:[self hypConnID] withVm:[self xenVM]];
                [rrdCPUButton setDelegate:delegate];
                [[rrdCPUButton view ]setTag:100];
            }
            [[rrdCPUButton view ]setFrame:[[cell contentView] bounds]];
            [[cell contentView] setAutoresizesSubviews:YES];
            [[cell contentView] addSubview:[rrdCPUButton view]];
            [rrdCPUButton viewWillAppear:YES];
            break;
        }
        default:
            break;
    }   
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];

    return cell;
}

- (UITableViewCell *) configureBootOrderCellForTableView:(UITableView *)tableView atIndex:(NSInteger) index{
    UITableViewCell* cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
    [cell setAccessoryType:UITableViewCellAccessoryNone];    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    [[cell textLabel] setText:[[[self xenVM] bootOrder] objectAtIndex:index]];
    [[cell detailTextLabel] setText: @""];
    return cell;
}

- (UITableViewCell*) configureTagsCellForTableView:(UITableView*)tableView atIndex:(NSInteger) index{
    UITableViewCell *cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    
    // remove the button if it is already there
    // because the same cell is used over and over
    [[[cell contentView] viewWithTag:100] removeFromSuperview];
    
    XenVM *xenVM = [self xenVM];
    NSString *tag = [[xenVM tags] objectAtIndex:index];
    [[cell textLabel] setText:tag];
    [[cell detailTextLabel] setText:@""];
    return cell;
}


// render the text in the cell from the address of the XenServer to be connected to.
- (UITableViewCell *)configureDetailsCellForTableView:(UITableView *)tableView atIndex:(NSInteger) index {
   
    UITableViewCell* cell = nil;

    XenVM *xenVM = [self xenVM];
    NSString *dateTime;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy (HH:mm)"];
    
    switch (index) {
        case XENVMDETAILSTABLESTARTTIME:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [[cell textLabel] setText:@"Start Time"];
            XenVM_metrics* vmMetrics = [self xenVMMetrics];
            if (!vmMetrics)
            {
                [[cell detailTextLabel] setText: @"Unknown"];
            }
            else
            {
                dateTime = [dateFormatter stringFromDate:[vmMetrics start_time]];
                if ([[vmMetrics start_time] timeIntervalSince1970] <= 0){
                    [[cell detailTextLabel] setText: @""];                    
                }
                else{
                    [[cell detailTextLabel] setText:dateTime];
                }
            }
            break;
        }
        case XENVMDETAILSTABLEOS:
        {
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [[cell textLabel] setText:@"OS"];
            NSArray *opRefs = [xenVM referencesForType:HYPOBJ_VM_GUEST_METRICS];
            if (opRefs == nil || [opRefs count] ==0)
            {
                [[cell detailTextLabel] setText: @"Unknown"];
            }
            else
            {
                NSString *opRef = [opRefs objectAtIndex:0];
                XenVM_guestMetrics *gm = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM_GUEST_METRICS withCondition:[XenBase XenBaseFor:opRef]] objectAtIndex:0];
                [[cell detailTextLabel] setText: [gm OSVersionName]];
            }
            break;
        }
        case XENVMDETAILSTABLEVIRTUALIZATIONSTATE:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [[cell textLabel] setText:@"Virtulization State"];
            NSArray *opRefs = [xenVM referencesForType:HYPOBJ_VM_GUEST_METRICS];
            if (opRefs == nil || [opRefs count] ==0)
            {
                [[cell detailTextLabel] setText: @"Tools not installed"];
            }
            else
            {
                NSString *opRef = [opRefs objectAtIndex:0];
                XenVM_guestMetrics *gm = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM_GUEST_METRICS withCondition:[XenBase XenBaseFor:opRef]] objectAtIndex:0];
                [[cell detailTextLabel] setText: [NSString stringWithFormat:@"Optimized ( %@ installed )", [gm PVDriversVersionString]]];
            }
            break;
        }
        case XENVMDETAILSTABLEHOST:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [cell setAccessoryType:UITableViewCellAccessoryNone];            
            [[cell textLabel] setText:@"On Host"];
            NSArray* opRefs = [[self xenVM] referencesForType:HYPOBJ_HOST];
            if (opRefs == nil || [opRefs count] ==0)
            {
                [[cell detailTextLabel] setText: @"None"];
            }
            else
            {
                NSString *opRef = [opRefs objectAtIndex:0];
                XenHost *host = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST withCondition:[XenBase XenBaseFor:opRef]] objectAtIndex:0];
                [[cell detailTextLabel] setText: [host name_label]];
            }
            break;
        }
        case XENVMDETAILSTABLEBIOSSTRINGCOPY:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [[cell textLabel] setText:@"Bios Copy"];
            BOOL switchState = [xenVM is_bios_reflected_inVM];
            if (switchState){
                [[cell detailTextLabel] setText: @"On"];
            }
            else{
                [[cell detailTextLabel] setText: @"Off"];                
            }
            break;
        }
        case XENVMDETAILSTABLEAUTOBOOT:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [cell setAccessoryType:UITableViewCellAccessoryDetailDisclosureButton];  // because its editable
            [[cell textLabel] setText:@"Auto Boot"];
            BOOL switchState = [xenVM auto_power_on];
            if (switchState){
                [[cell detailTextLabel] setText: @"On"];
            }
            else{
                [[cell detailTextLabel] setText: @"Off"];
            }
             break;
        }
        case XENVMDETAILSTABLEIPADDRESS:{
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [[cell textLabel] setText:@"IP"];
            // might want to make this multi line at some point
            // but for now just append all strings together
            // would need to increase the Cell hight otherwise
            NSArray *opRefs = [xenVM referencesForType:HYPOBJ_VM_GUEST_METRICS];
            if (opRefs == nil || [opRefs count] ==0)
            {
                [[cell detailTextLabel] setText: @"Tools not installed"];
            } 
            else{
                NSString *opRef = [opRefs objectAtIndex:0];
                XenVM_guestMetrics *gm = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM_GUEST_METRICS withCondition:[XenBase XenBaseFor:opRef]] objectAtIndex:0];
                NSArray *allAddress = [gm networkAddress];
                NSString* finalString = nil;
                for (NetworkAddress* address in allAddress) {
                    if (!finalString){
                        finalString = [address address];
                    }
                    else{
                        finalString = [NSString stringWithFormat:@"%@ , %@",finalString,[address address]];
                    }
                }
                
                [[cell detailTextLabel] setText: finalString];
            }
            break;
        }
        default:
            break;
    }   
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];

    return cell;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    [self tableView:tableView didSelectRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Configure the cell...
    switch ([indexPath section]) {
        case XENVM_SECTION_GENERAL:
            [self processGeneralSectionSelectionForIndex:[indexPath row] atRect:[tableView rectForRowAtIndexPath:indexPath]];
            break;
        case XENVM_SECTION_MEMORY:
            [self processMemorySectionSelectionForIndex:[indexPath row] atRect:[tableView rectForRowAtIndexPath:indexPath]];
            break;
        case XENVM_SECTION_CPU:
            [self processCPUSectionSelectionForIndex:[indexPath row] atRect:[tableView rectForRowAtIndexPath:indexPath]];
            break;
        case XENVM_SECTION_BOOTORDER:
            break;
        case XENVM_SECTION_TAGS:
            break;
            
        default:
            break;
    }

}

-(void) processCPUSectionSelectionForIndex:(NSInteger) row atRect:(CGRect)rect{
    switch (row) {
        case XENVM_CPU_TABLE_CPUCOUNT:{
            XenVMCPUCountEditorViewController* c = [[XenVMCPUCountEditorViewController alloc]
                                                   initWithHypervisorConnectionID: [self hypConnID] andXenVMRef:[self xenVMRef]];
            [self displayEditor:c forRect:rect andPopoverDirection:UIPopoverArrowDirectionRight];
            
            break;
        }
        default:
            break;
    }
}

-(void) processMemorySectionSelectionForIndex:(NSInteger) row atRect:(CGRect)rect{
    if (row == XENVM_MEMORY_TABLE_AVAILABLE){
        XenVMMemoryEditiorViewController* c = [[XenVMMemoryEditiorViewController alloc]
                                               initWithHypervisorConnectionID: [self hypConnID] andXenVMRef:[self xenVMRef]];
            [self displayEditor:c forRect:rect andPopoverDirection:UIPopoverArrowDirectionRight];
    }
}

-(void) processGeneralSectionSelectionForIndex:(NSInteger)row atRect:(CGRect)rect{
    switch (row) {
        case XENVMDETAILSTABLEAUTOBOOT:{
            XenVMAutoBootEditorViewController* c = [[XenVMAutoBootEditorViewController alloc]
                                                    initWithHypervisorConnectionID: [self hypConnID] andXenVMRef:[self xenVMRef]];

            [self displayEditor:c forRect:rect andPopoverDirection:UIPopoverArrowDirectionRight];
            break;
        }
        default:
            break;
    }
}


- (void) displayEditor:(UIViewController*)viewController forRect:(CGRect)rect andPopoverDirection:(UIPopoverArrowDirection)popOverDirection{
    if ([[UIDevice currentDevice].model hasPrefix:@"iPhone"]){
        // appear to navigate into a portrait view by using a new naviagtion controller that only supports Portrait operation
        UIViewController* sec = [[UIViewController alloc] init];
        sec.title = self.title; // to give the correct Back button title
        UINavigationController* nav = [[PortraitNavigationController alloc] initWithRootViewController:sec];
        [nav pushViewController:viewController animated:NO];
        [self presentViewController:nav animated:YES completion:nil];
        nav.delegate = self; // so that we know when the user navigates back
    }
    else{
        popover = [[UIPopoverController alloc] initWithContentViewController:viewController];
        // permitted arrow direction 0 makes the popover appear without any arrows :)
        if (popOverDirection == UIPopoverArrowDirectionRight){
            rect.origin.x = rect.origin.x + (rect.size.width -75);
        }
        if (popOverDirection == UIPopoverArrowDirectionLeft){
            rect.size.width = 0;
        }
        
        [popover presentPopoverFromRect:rect inView:[self view] permittedArrowDirections:popOverDirection animated:YES];
    }
}

// Delgate for retuning from the PortaitNavigation controller
- (void)navigationController:(UINavigationController *)navigationController2
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated {
    if (viewController == [navigationController2.viewControllers objectAtIndex:0])
        [self dismissViewControllerAnimated:YES completion:nil];
}

//-(void) displayEditor:(UIViewController*)viewController forRect:(CGRect)rect andPopoverDirection:(UIPopoverArrowDirection)popOverDirection{
//    if ([[UIDevice currentDevice].model hasPrefix:@"iPhone"]){
//        [[self navigationController] pushViewController:viewController animated:YES];
//    }
//    else{
//        popover = [[UIPopoverController alloc] initWithContentViewController:viewController];
//        // permitted arrow direction 0 makes the popover appear without any arrows :)
//        if (popOverDirection == UIPopoverArrowDirectionRight){
//            rect.origin.x = rect.origin.x + (rect.size.width -75);
//        }
//        if (popOverDirection == UIPopoverArrowDirectionLeft){
//            rect.size.width = 0;
//        }
//
//        [popover presentPopoverFromRect:rect inView:[self view] permittedArrowDirections:popOverDirection animated:YES];
//    }
//}

// use a default popOverDirection
-(void) displayEditor:(UIViewController*)viewController forRect:(CGRect)rect{
    [self displayEditor:viewController forRect:rect andPopoverDirection:UIPopoverArrowDirectionUp];
}

#pragma mark Identifyable protocol
- (void) setId:(NSString*)newID{
    [self setXenVMRef:newID];
}

#pragma mark RefreshablePage Protocol
- (UIImage*) currentTitleImage{
    XenVM *xenVM = [self xenVM];
    return [XenImageBuilder buildVMImageForVM:xenVM];
}

-(void)refreshPage{
    [[self tableView] reloadData];
}


#pragma mark -
#pragma mark UIWaitForDownload delegate

- (void)dataLoaderFinished:(UIWaitForDataLoad *)dataLoader withResult:(BOOL)sucsess{
    NSLog(@"dataloader reported that it has loaded the required data.");
    
    NSError *err = [dataLoader connectionError];
    
    if (err){
        // if there was an error connecting do not do anyting.
        return;
    }
    
    // also invalidate the extention page so it redraws now all the data is available
    UITabBarController *tabBarController = [self tabBarController];
    // reload all tables in all views in the table view controller.
    if (tabBarController){
        NSArray *allTabbedControllers = [tabBarController viewControllers];
        for (UIViewController *vc in allTabbedControllers){
            if ([vc conformsToProtocol:@protocol(RefreshablePage)]){
                UIViewController<RefreshablePage>  *vcRefresh= (UIViewController<RefreshablePage>  *)vc;
                [vcRefresh refreshPage];
            }
        }
    }
                 
    [self refreshPage];
                 
    [[self hypervisorConnection] setWalkTreeMode:NO];
}

-(void)ensureObjectTreeLoaded{
    HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    
    // link in the UIDataload to make sure that the object has all its children
    
    NSPredicate *objectPred = [XenBase XenBaseFor:[self xenVMRef]];
    
    NSArray *objects = [hypervisorConnection hypObjectsForType:HYPOBJ_VM withCondition:objectPred];
    if ([objects count] >0){
        [hypervisorConnection setWalkTreeMode:YES];
        UIWaitForDataLoad *dataLoader = [[UIWaitForDataLoad alloc] initWithHypervisorConnection:hypervisorConnection];
        [dataLoader setUIWaitForDataLoadDelegate:self];
        [dataLoader waitForObjectTreeForObjectReference:[self xenVMRef] objectType:HYPOBJ_VM];
    }
}

@end
