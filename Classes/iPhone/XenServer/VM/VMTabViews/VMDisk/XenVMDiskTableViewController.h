//
//  XenVMDiskViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVM.h"
#import "XenHypervisorConnection.h"
#import "ExpandableTableViewController.h"
#import "RRDDisplayButtonDelegate.h"
#import "Identifyable.h"
#import "RefreshablePage.h"
#import "UIWaitForDataLoad.h"

#define XENVM_DISK_TABLE_NAME 0
#define XENVM_DISK_TABLE_DESCRIPTION 1
#define XENVM_DISK_TABLE_SIZE 2
#define XENVM_DISK_TABLE_SR 3
#define XENVM_DISK_TABLE_READONLY 4
#define XENVM_DISK_TABLE_DEVICE 5
#define XENVM_DISK_TABLE_PERFORMANCEDATA 6
#define XENVM_DISK_TABLE_COUNT 7

#define XENVM_CD_TABLE_LOADEDISO 0
#define XENVM_CD_TABLE_COUNT 1

@interface XenVMDiskTableViewController :ExpandableTableViewController
      <HypervisorConnectionDelegate,
       RRDDisplayButtonDelegate,RefreshablePage,
       Identifyable,
       UIWaitForDataLoadDelegate>{
           
    NSString *xenVMRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;
    // list of references for disks
    NSArray *disks;
    //list of references for CD/DVD drives
    NSArray *CDs;

    // stores a list of rrdButtons for each nic
    // so that only one button is created for each object.
    NSMutableDictionary *rrdDiskButtons;

}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

// this is used by relection in the tab contol t o update the page.  In this class it updates the xenVMRef.
- (void) setId:(NSString*)newID;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenVMRef;
@property (strong) NSArray *disks;
@property (strong) NSArray *CDs;
@end
