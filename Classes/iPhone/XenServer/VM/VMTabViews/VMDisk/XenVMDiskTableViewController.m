//
//  XenVMDiskViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVMDiskTableViewController.h"
#import "XenVBD.h"
#import "XenVDI.h"
#import "XenStorage.h"
#import "ScrollableDetailTextCell.h"
#import "RRDDisplayButton.h"
#import "DataSource.h"
#import "BitNumberFormatter.h"
#import "XenImageBuilder.h"

#define RRDButtonCell @"RRD"
#define GeneralCell @"General"

@interface XenVMDiskTableViewController (){
    BOOL takeActionOnAppear;
}
- (UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier;
- (UITableViewCell*) configureCellForTableView:(UITableView *)tableView forSection:(NSInteger)section atIndex:(NSInteger) index;
-(XenVDI *) vdiForSection:(NSInteger)section;
-(XenVBD *) vbdForSection:(NSInteger)section;
-(XenVDI *) vdiForCDIndex:(NSInteger)index;
-(XenVBD *) vbdForCDIndex:(NSInteger)index;
-(XenStorage *) storageForSection:(NSInteger)section;
-(XenVBD *) vbdForIndex:(NSInteger)index;
- (void) resetTableDataSource;
-(bool) areCDDrivesPresent;
@end

@implementation XenVMDiskTableViewController

@synthesize hypConnID, xenVMRef, CDs, disks;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenVM *) xenVM{
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVMRef]]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }
    return nil;
}


- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        rrdDiskButtons = [[NSMutableDictionary alloc] init];
        [self setXenVMRef:  [thisXenVM opaque_ref]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        takeActionOnAppear = YES;
    }
    return self;
    
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

// this is now mandatory in ios7
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}

/**
 * Set the flag to indicate that the appear operateion that
 * are about to happen should not be acted on
 * they come as part of the hack to force screen to be rotated to the
 * desired orientation on edit screens
 */
-(void) setTakeActionOnAppear:(NSNumber*)takeAction{
    takeActionOnAppear =  [takeAction boolValue];
}

- (void) viewWillAppear:(BOOL)animated{
    if (takeActionOnAppear){
        [super viewWillAppear:animated];
        [[self tableView ]reloadData];
        XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
        [xenHypervisorConnection addHypervisorConnectionDelegate:self];
        [self resetTableDataSource];
        // set the content insets if ios7 or later
        if ([[[UIDevice currentDevice] systemVersion] intValue] >= 7){
            // only applies to iPhone as split views do not have this issue
            if ([[UIDevice currentDevice].model hasPrefix:@"iPhone"]){
                [[self tableView] setContentInset:UIEdgeInsetsMake(60,0,0,0)];
            }
        }
    }
}

-(void) viewDidAppear:(BOOL)animated{
    if (takeActionOnAppear) {
        [[self view] setNeedsDisplay];
        
        [self ensureObjectTreeLoaded];

        [[self tableView ]reloadData];
        [super viewDidAppear:animated];
    }
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

// provide access to the cell definitions
-(UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    
    // general cells can have different components in them remove them if there are
    if ([identifier isEqualToString: GeneralCell]){
        [[[cell contentView] viewWithTag:100] removeFromSuperview];
    }

    return cell;    
}

#pragma mark Identifyable
- (void) setId:(NSString*)newID{
    [self setXenVMRef:newID];
}

#pragma mark RefreshablePage
-(void)refreshPage{
    [self resetTableDataSource];
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to vm disk objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    // could cut this down to just the relevent ones if required
    if (objectType == HYPOBJ_VBD || objectType == HYPOBJ_VDI || objectType == HYPOBJ_STORAGE){
        [[self tableView ]reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source
- (void) resetTableDataSource{
    
    NSMutableArray* tempCDs = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray* tempDisks = [NSMutableArray arrayWithCapacity:1];
    
    NSInteger totalVBDs = [[[self xenVM] referencesForType:HYPOBJ_VBD] count];
    for (int i =0; i < totalVBDs ; i++){
        XenVBD *vbd = [self vbdForIndex:i];
        if ([[vbd type] isEqualToString:@"CD"]){
            [tempCDs addObject:[vbd opaque_ref]];
        } else {
            [tempDisks addObject:[vbd opaque_ref]];
        }
    }
    
    // sort the disks in order
    NSArray *sortedCDs = [tempCDs sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        XenVBD* first = [self vbdForReference:a];
        XenVBD* second = [self vbdForReference:b];
        NSString* firstStr = [first devicePositionName];
        NSString* secondStr = [second devicePositionName];
        if (secondStr){
            return [firstStr compare:secondStr];
        }
        return 0;
    }];

    NSArray *sortedDisks = [tempDisks sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        XenVBD* first = [self vbdForReference:a];
        XenVBD* second = [self vbdForReference:b];
        NSString* firstStr = [first devicePositionName];
        NSString* secondStr = [second devicePositionName];
        if (secondStr){
            return [firstStr compare:secondStr];
        }
        return 0;
    }];

    [self setCDs:sortedCDs];
    [self setDisks:sortedDisks];

}

-(XenVBD*) vbdForReference:reference{
    XenVBD* vbd = nil;
    NSArray *vbds = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VBD withCondition:[XenBase XenBaseFor:reference]];
    if ([vbds count] >0){
        vbd = [vbds objectAtIndex:0];
    }
    return vbd;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of NICs that the host has.
    if (![self CDs] || ![self disks]){
        [self resetTableDataSource];
    }

    // one section of CDS
    if ([self areCDDrivesPresent]){
        return [[self disks] count] + 1;
    }
    else{
        return [[self disks] count];
    }
}

-(bool) areCDDrivesPresent{
    bool areCDs = NO;
    if ([[self CDs] count] > 0){
        areCDs = YES;
    }
    return areCDs;
}

// returns nil if the vdi is not in the cache, this is expected to rectify itself by a subsequent data load
-(XenStorage *) storageForSection:(NSInteger)section{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    XenStorage *storage = nil;
    XenVDI *vdi = [self vdiForSection:section];
    // now go from vdi to the Storage
    NSArray *storageList = [vdi referencesForType:HYPOBJ_STORAGE];
    if ([storageList count] >0){
        NSString *storageRef = [storageList objectAtIndex:0];
        NSArray *vstorageList2 = [xenHypervisorConnection hypObjectsForType:HYPOBJ_STORAGE withCondition:[XenBase XenBaseFor:storageRef]];
        if ([vstorageList2 count] >0){
            storage = [vstorageList2 objectAtIndex:0];
        }
    }
    return storage;
}

// returns nil if the vdi is not in the cache, this is expected to rectify itself by a subsequent data load
-(XenVDI *) vdiForCDIndex:(NSInteger)index{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    XenVDI *vdi = nil;
    XenVBD *vbd = [self vbdForCDIndex:index];
    // now go from vbd to the VDI
    NSArray *vdiList = [vbd referencesForType:HYPOBJ_VDI];
    if ([vdiList count] >0){
        NSString *vdiRef = [vdiList objectAtIndex:0];
        NSArray *vdis = [xenHypervisorConnection hypObjectsForType:HYPOBJ_VDI withCondition:[XenBase XenBaseFor:vdiRef]];
        if ([vdis count] >0){
            vdi = [vdis objectAtIndex:0];
        }
    }
    return vdi;
}


// returns nil if the vdi is not in the cache, this is expected to rectify itself by a subsequent data load
-(XenVDI *) vdiForSection:(NSInteger)section{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    XenVDI *vdi = nil;
    XenVBD *vbd = [self vbdForSection:section];
    // now go from vbd to the VDI
    NSArray *vdiList = [vbd referencesForType:HYPOBJ_VDI];
    if ([vdiList count] >0){
        NSString *vdiRef = [vdiList objectAtIndex:0];
        NSArray *vdis = [xenHypervisorConnection hypObjectsForType:HYPOBJ_VDI withCondition:[XenBase XenBaseFor:vdiRef]];
        if ([vdis count] >0){
            vdi = [vdis objectAtIndex:0];
        }
    }
    return vdi;
}

// returns nil if the vbd is not in the cache, this is expected to rectify itself by a subsequent data load
-(XenVBD *) vbdForIndex:(NSInteger)index{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    XenVBD *vbd = nil;
    NSString *opRef = [[[self xenVM] referencesForType:HYPOBJ_VBD] objectAtIndex:index];
    NSArray *vbds = [xenHypervisorConnection hypObjectsForType:HYPOBJ_VBD withCondition:[XenBase XenBaseFor:opRef]];
    if ([vbds count] >0){
        vbd = [vbds objectAtIndex:0];
    }
    return vbd;
}

// returns nil if the vbd is not in the cache, this is expected to rectify itself by a subsequent data load
-(XenVBD *) vbdForCDIndex:(NSInteger)index{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    XenVBD *vbd = nil;
    NSString *opRef;
    
    opRef = [[self CDs] objectAtIndex:index];
    
    NSArray *vbds = [xenHypervisorConnection hypObjectsForType:HYPOBJ_VBD withCondition:[XenBase XenBaseFor:opRef]];
    if ([vbds count] >0){
        vbd = [vbds objectAtIndex:0];
    }
    return vbd;
}


// returns nil if the vbd is not in the cache, this is expected to rectify itself by a subsequent data load
-(XenVBD *) vbdForSection:(NSInteger)section{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    XenVBD *vbd = nil;
    NSString *opRef;NSUInteger sectionOffset = 0;
    if ([self areCDDrivesPresent]){
        sectionOffset = 1;
    }
    opRef = [[self disks] objectAtIndex:(section - sectionOffset)];
    
    NSArray *vbds = [xenHypervisorConnection hypObjectsForType:HYPOBJ_VBD withCondition:[XenBase XenBaseFor:opRef]];
    if ([vbds count] >0){
        vbd = [vbds objectAtIndex:0];
    }
    return vbd;
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ([self areCDDrivesPresent] && section == 0){
        return [NSString stringWithFormat:@"DVD Drives"];
    }
    else {NSUInteger position = section;
        if ([self areCDDrivesPresent]){
            position --;
        }
        return [NSString stringWithFormat:@"Disk:%lu", (unsigned long)position];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
   // return disk count if a disk and CD count if a CD
    NSUInteger sectionOffset = 0;
    if ([self areCDDrivesPresent]){
        sectionOffset ++;
    }
    
    if (section <  sectionOffset){
        return [[self CDs] count];;
    }
    else{
        return XENVM_DISK_TABLE_COUNT;
    }
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // CD/DVD drives are always expanded
    if ([self areCDDrivesPresent] && section == 0){
        return NO;
    }
    return YES;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self configureCellForTableView:tableView forSection:[indexPath section] atIndex:[indexPath row]];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (UITableViewCell*) configureCellForTableView:(UITableView *)tableView forSection:(NSInteger)section atIndex:(NSInteger) index{
    
    UITableViewCell *cell = nil;
    
    if ([self areCDDrivesPresent] && section == 0){
        cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
        XenVDI *vdi = [self vdiForCDIndex:index];

        NSString* displayText = [NSString stringWithFormat:@"Drive:%li",index+1 ];
        
        [[cell textLabel] setText:displayText];
        NSString *content = [vdi location];
        if (content == nil || [content isEqualToString:@""])
        {
            content = @"<empty>";
        }
        [[cell detailTextLabel] setText:content];
    }
    else{
        switch (index) {
            case XENVM_DISK_TABLE_DESCRIPTION:{
                cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
                XenVDI *vdi = [self vdiForSection:section];
                [[cell textLabel] setText:@"Description"];
                [[cell detailTextLabel] setText:[vdi name_description]];
                break;
            }
            case XENVM_DISK_TABLE_NAME:{
                cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
                XenVDI *vdi = [self vdiForSection:section];
                [[cell textLabel] setText:@"Name"];
                [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%@",[vdi name_label]]];
                break;
            }
            case XENVM_DISK_TABLE_SIZE:{
                cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
                XenVDI *vdi = [self vdiForSection:section];
                [[cell textLabel] setText:@"Size"];
                NSNumber *sizeBytes = [vdi virtual_size];
                float sizeGb = ((float)[sizeBytes longLongValue])/1024/1024/1024;
                if (sizeGb < 0.9){
                    float sizeMb = ((float)[sizeBytes longLongValue])/1024/1024;
                    [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Mb",sizeMb]];
                }
                else{
                    [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Gb",sizeGb]];
                }
                break;
            }
            case XENVM_DISK_TABLE_DEVICE:{
                cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
                XenVBD *vbd = [self vbdForSection:section];
                [[cell textLabel] setText:@"Device"];
                [[cell detailTextLabel] setText: [vbd device]];
                break;
            }
            case XENVM_DISK_TABLE_READONLY:{
                cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
                XenVDI *vdi = [self vdiForSection:section];
                [[cell textLabel] setText:@"Read Only"];
                [[cell detailTextLabel] setText:[vdi read_only]?@"Yes":@"No"];
                break;
            }
            case XENVM_DISK_TABLE_SR:{
                cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
                XenStorage *storage = [self storageForSection:section];
                [[cell textLabel] setText:@"Storage"];
                [[cell detailTextLabel] setText:[storage name_label]];
                break;
            }
            case XENVM_DISK_TABLE_PERFORMANCEDATA:{
                cell = [self getCellForTableView:tableView andIdentifier:RRDButtonCell];
                [[cell detailTextLabel] setText:@""];
                [[cell textLabel] setText:@""];
                XenVBD *vbd = [self vbdForSection:section];
                RRDDisplayButton* rrdButton = [rrdDiskButtons objectForKey:[vbd uuid]];
                if (!rrdButton){
                    rrdButton = [[RRDDisplayButton alloc] init];
                    [self addChildViewController:rrdButton];
                    [rrdDiskButtons setObject:rrdButton forKey:[vbd uuid]];
                    [rrdButton setDelegate:self];
                    [[rrdButton view ]setTag:100];
                    [rrdButton setReference:[[self vbdForSection:section ] device]];
                }
                [[rrdButton view ]setFrame:[[cell contentView] bounds]];
                [[cell contentView] setAutoresizesSubviews:YES];
                [[cell contentView] addSubview:[rrdButton view]];
                [rrdButton viewWillAppear:YES];
                break;
            }
            default:
                break;
        }  
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];

    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


#pragma mark -
#pragma mark RRDDisplayButtonDelegate

- (RoundRobinDatabase *)roundRobinDatabase{
    XenVM *xenVM = [self xenVM];
    return [xenVM vmPerformanceData];
}

- (BOOL) reloadRoundRobinDatabase{
    XenVM *xenVM = [self xenVM];
    return [xenVM refreshPerformanceData];
}

-(NSDate*) getLastRefreshTime{
    XenVM *xenVM = [self xenVM];
    return [xenVM lastPreformanceDataUpdateTime];
}

-(NSString*)  graphTitleWithReference:(NSString*)sourcereference{
    return [NSString stringWithFormat:@"Disk usage for %@", sourcereference];
}


-(NSArray*)dataSourcesToGraphFromRRD:(RoundRobinDatabase*)rrd withReference:(NSString*)sourcereference{
    NSMutableArray* sourceArray = [[NSMutableArray alloc] initWithCapacity:1];
    // need to convert source referece to the actual reference here
    NSString *storageRef = [NSString stringWithFormat:@"vbd_%@_",sourcereference];
    for (DataSource* dataSource in [rrd dataSources]) {
        if ([[dataSource name] hasPrefix:storageRef]){
            if ([[dataSource name] hasSuffix:@"write"]){
                [dataSource setAlternateName:@"Write"];
                [sourceArray addObject:dataSource];
            }
            if ([[dataSource name] hasSuffix:@"read"]){
                [dataSource setAlternateName:@"Read"];
                [sourceArray addObject:dataSource];
            }
        }
    }
    return sourceArray;
}

-(double)yMinForDataSources:(NSArray*)dataSources withConsolidationStep:(NSInteger)step{
    return 0;
}

-(double) getPlotPointFromDataPoint:(DataPoint*)dataPoint{
    // beacuse using a minmanl RRD all points are the same
    return [dataPoint average];
    //    double displayVal = [dataPoint average];
    //    if (displayVal == 0){
    //        displayVal = [dataPoint max];
    //    }
    //    // NSLog(@"%f",displayVal);
    //    return displayVal;    
}

-(double)roundup:(double)inputNumber{
    // round up the the value to the nearest value on a 10's boundary
    NSUInteger resultInt = inputNumber;
    NSUInteger u = 0;
    while (resultInt > 1){
        resultInt /= 10;
        u ++;
    };
    
    float divisor = pow(10,u-1);
    float resultPower = inputNumber / divisor;
    // round this and multiply by u power 10;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:0];
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    
    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithFloat:resultPower]];
    return [numberString intValue] * divisor;
}

-(double)yMaxForDataSources:(NSArray*)dataSources withConsolidationStep:(NSInteger)step{
    double result = 0;
    for (DataSource* source in dataSources) {
        NSLog(@"DataSourceName = %@",[source name]);
        NSLog(@"Consolidation Step = %lu", (long)step);
        for (DataPoint* point in [source dataPointsForConsolodatedStepCount:step]) {
            double val = [self getPlotPointFromDataPoint:point];
            if (val>result){
                result = val;
            }
        }
    }
    return [self roundup:result];
}

-(NSString*)yTitle{
    return @"bps";
}

-(NSNumberFormatter*) yFormatter{
    NSNumberFormatter* formatter = [[BitNumberFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    return formatter;
}



#pragma mark -
#pragma mark UIWaitForDownload delegate

- (void)dataLoaderFinished:(UIWaitForDataLoad *)dataLoader withResult:(BOOL)sucsess{
    NSLog(@"dataloader reported that it has loaded the required data.");
    
    NSError *err = [dataLoader connectionError];
    
    if (err){
        // if there was an error connecting do not do anyting.
        return;
    }
    
    // also invalidate the extention page so it redraws now all the data is available
    UITabBarController *tabBarController = [self tabBarController];
    // reload all tables in all views in the table view controller.
    if (tabBarController){
        NSArray *allTabbedControllers = [tabBarController viewControllers];
        for (UIViewController *vc in allTabbedControllers){
            if ([vc conformsToProtocol:@protocol(RefreshablePage)]){
                UIViewController<RefreshablePage>  *vcRefresh= (UIViewController<RefreshablePage>  *)vc;
                [vcRefresh refreshPage];
            }
        }
    }
    
    [self refreshPage];
    
    [[self hypervisorConnection] setWalkTreeMode:NO];
}


-(void)ensureObjectTreeLoaded{
    HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    
    // link in the UIDataload to make sure that the object has all its children
    
    NSPredicate *objectPred = [XenBase XenBaseFor:[self xenVMRef]];
    
    NSArray *objects = [hypervisorConnection hypObjectsForType:HYPOBJ_VM withCondition:objectPred];
    if ([objects count] >0){
        [hypervisorConnection setWalkTreeMode:YES];
        UIWaitForDataLoad *dataLoader = [[UIWaitForDataLoad alloc] initWithHypervisorConnection:hypervisorConnection];
        [dataLoader setUIWaitForDataLoadDelegate:self];
        [dataLoader waitForObjectTreeForObjectReference:[self xenVMRef] objectType:HYPOBJ_VM];
    }
}
@end
