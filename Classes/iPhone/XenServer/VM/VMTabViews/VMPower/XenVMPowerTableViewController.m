//
//  XenVMPowerViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVMPowerTableViewController.h"
#import "HypOpsAppDelegate.h"
#import "XenImageBuilder.h"

@interface PowerOperationData :NSObject{NSUInteger powerOp;
    NSString *vmRef;
    BOOL powerOpRequested;
}
@property NSUInteger powerOp;
@property (copy) NSString *vmRef;
@property BOOL powerOpRequested;
@end

@implementation PowerOperationData
@synthesize powerOp;
@synthesize vmRef;
@synthesize powerOpRequested;
@end

@interface XenVMPowerTableViewController(){
    BOOL takeActionOnAppear;
}

- (void) configureCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
- (void)timerFireMethod:(NSTimer*)theTimer;

@end

@implementation XenVMPowerTableViewController

@synthesize hypConnID, xenVMRef, powerOpINProgress;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenVM *) xenVM{
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVMRef]]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }
    return nil;
}


- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setXenVMRef: [thisXenVM opaque_ref]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        [self setPowerOpINProgress:-1];
        [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        takeActionOnAppear = YES;

        // set the tab bar button details
        self.tabBarItem.title = @"Control";
        //Set the image for the tab bar
        self.tabBarItem.image = [UIImage imageNamed:@"tab_Power"];
    }
    return self;
    
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

// this is now mandatory in ios7
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];

    // Without this label the heading will show though the disbale tinit for some reason
    UILabel *l = [[UILabel alloc] init];
    [l setText:@""];
    [tableView setTableHeaderView:l];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

/**
 * Set the flag to indicate that the appear operateion that
 * are about to happen should not be acted on
 * they come as part of the hack to force screen to be rotated to the
 * desired orientation on edit screens
 */
-(void) setTakeActionOnAppear:(NSNumber*)takeAction{
    takeActionOnAppear =  [takeAction boolValue];
}

- (void) viewWillAppear:(BOOL)animated{
#warning use Portrait navigation controller here instead of this bodge
    if (takeActionOnAppear){
    if (!UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation])){
        // ----- force the view to be portrait only
        //will rotate status bar
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
        
        //will re-rotate view according to statusbar
        UIViewController *temp = [[UIViewController alloc]init];
        [self presentViewController:temp animated:NO completion:nil];
        [self dismissViewControllerAnimated:NO completion:nil];
        // ---- end force view to be portrait only
    }

    [super viewWillAppear:animated];
    // make sure that the date on the page is upto date
    [[self tableView] reloadData];
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    [xenHypervisorConnection addHypervisorConnectionDelegate:self];
    [xenHypervisorConnection setWalkTreeMode:NO];
    }
    
    // set the content insets if ios7 or later
    if ([[[UIDevice currentDevice] systemVersion] intValue] >= 7){
        // only applies to iPhone as split views do not have this issue
        if ([[UIDevice currentDevice].model hasPrefix:@"iPhone"]){
            [[self tableView] setContentInset:UIEdgeInsetsMake(60,0,0,0)];
        }
    }
}

-(BOOL)shouldAutorotate{
    return NO;
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (takeActionOnAppear){
        // Turn on proximity monitoring  this will shut off screen when something is close by
        // stops accedently powering thing on/off when device is in pocket etc..
        UIDevice *device = [UIDevice currentDevice];
        [device setProximityMonitoringEnabled:YES];
        [self setPowerOpINProgress:-1];  // clear any existing power op in progress setting
        [self ensureObjectTreeLoaded];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    // Turn off proximity monitoring so back to normal   
    UIDevice *device = [UIDevice currentDevice];
    [device setProximityMonitoringEnabled:NO];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark Identifyable
- (void) setId:(NSString*)newID{
    [self setXenVMRef:newID];
}

#pragma mark RefreshabalePage
- (UIImage*) currentTitleImage{
    XenVM *xenVM = [self xenVM];
    return [XenImageBuilder buildVMImageForVM:xenVM];
}

-(void)refreshPage{
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    return NO;
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    XenPowerState state = [[self xenVM] power_state];
    NSString* stateStr = nil;
    switch (state) {
        case XENPOWERSTATE_RUNNING:
            stateStr = @"Running";
            break;
        case XENPOWERSTATE_HALTED:
            stateStr = @"Stopped";
            break;
        case XENPOWERSTATE_PAUSED:
            stateStr = @"Paused";
            break;
        case XENPOWERSTATE_SUSPENDED:
            stateStr = @"Suspended";
            break;
    }
    return [NSString stringWithFormat:@"Power (currently %@)",stateStr];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[self xenVM] availablePowerOperations] count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    [self configureCell:cell atIndex:[indexPath row]];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    XenVM *xenVM = [self xenVM];NSUInteger powerState = [[[[xenVM availablePowerOperations] allObjects] objectAtIndex:index] intValue];

    NSString *buttonText = nil;
    NSString *imageName = nil;
    switch (powerState){
        case XENPOWEROPERATION_START:
            buttonText = @"Start";
            imageName = @"start_vm";
            break;
        case XENPOWEROPERATION_STOP_CLEAN:
            buttonText = @"Shutdown";
            imageName = @"stop_vm";
            break;
        case XENPOWEROPERATION_STOP_FORCE:
            buttonText = @"** Force Shutdown **";
            imageName = @"stop_vm";
            break;
        case XENPOWEROPERATION_RESUME:
            buttonText = @"Resume";
            imageName = @"start_vm";
            break;
        case XENPOWEROPERATION_SUSSPEND:
            buttonText = @"Suspend";
            imageName = @"suspend_vm";
            break;
        case XENPOWEROPERATION_RESTART_CLEAN:
            buttonText = @"Restart";
            imageName = @"start_vm";
            break;
        case XENPOWEROPERATION_RESTART_FORCE:
            buttonText = @"** Force Restart **";
            imageName = @"start_vm";
            break;
    }
    
    [[cell textLabel] setText:buttonText];
    [[cell imageView] setImage:[UIImage imageNamed:imageName]];

    // remove any existing button or spinner from the view
    [[[cell contentView] viewWithTag:100] removeFromSuperview];
    
    /// set the spinner on the cells if the op is in progress
    if ([self powerOpINProgress] == powerState){
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        CGRect rect = CGRectMake([[cell contentView] frame].size.width-30, 10.0, 20.0, 20.0);
        [spinner setFrame:rect];
        [spinner setAutoresizingMask:UIViewAutoresizingFlexibleRightMargin];
        [spinner setTag:100];
        [[cell contentView] addSubview:spinner];
        [spinner startAnimating];
        [cell setBackgroundColor:[UIColor blueColor]];
    }
    else
    {
        [cell setBackgroundColor:[UIColor whiteColor]];
    }

}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {    
    if ([self powerOpINProgress] > -1){
        NSLog(@"PowerOperation in progress cant do another one");
        return;
    }
    NSSet *powerOpSet = [[self xenVM] availablePowerOperations];NSUInteger powerState = [[[powerOpSet allObjects] objectAtIndex:[indexPath row]] intValue];
    // could do this in the first timer option then could display progress in actual cell possibly
    [self setPowerOpINProgress:powerState];
    // get the spinner going
    [[self tableView] reloadData];
    PowerOperationData *pod = [[PowerOperationData alloc] init];
    [pod setPowerOp:powerState];
    [pod setVmRef:[self xenVMRef]];
    [pod setPowerOpRequested:NO];
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFireMethod:) userInfo:pod repeats:YES];
}

- (void)timerFireMethod:(NSTimer*)theTimer{
    PowerOperationData *pod = (PowerOperationData *)[theTimer userInfo];
    // if not requested the op yet do it now
    if (![pod powerOpRequested]){
        [[self hypervisorConnection] RequestPowerOperation:[pod powerOp] forVMReference:[pod vmRef]];
        [pod setPowerOpRequested:YES];
    }
    else{
       NSArray *vms = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[pod vmRef]]]; 
        if ([vms count]>0){
            XenVM *vm = [vms objectAtIndex:0];
            NSSet *availablePowerOps = [vm availablePowerOperations];
            // if there are no power ops available then assume that the current op is still in progress
            if ([availablePowerOps count] >0){
                // if the powerOp requested is in the list still then not finished
                BOOL found = NO;
                for (NSNumber *op in availablePowerOps) {
                    if ([op intValue] == [pod powerOp]){
                        found = YES;
                        break;
                    }
                }
                if (!found){
                    // must have finished here
                    [self setPowerOpINProgress:-1];
                    [theTimer invalidate];
                    [[self tableView] reloadData];
               }
            }
            if (![[self hypervisorConnection] isAutoUpdateEnabled]){
                [[self hypervisorConnection] RequestHypObject:[pod vmRef] ForType:HYPOBJ_VM];
            }
        }
        else  // something went wrong, can no longer find vm in cache, so give up
        {
            [self setPowerOpINProgress:-1];
            [theTimer invalidate];
        }
    }
}

#pragma mark -
#pragma mark UIWaitForDownload delegate

- (void)dataLoaderFinished:(UIWaitForDataLoad *)dataLoader withResult:(BOOL)sucsess{
    NSLog(@"dataloader reported that it has loaded the required data.");
    
    NSError *err = [dataLoader connectionError];
    
    if (err){
        // if there was an error connecting do not do anyting.
        return;
    }
    
    // also invalidate the extention page so it redraws now all the data is available
    UITabBarController *tabBarController = [self tabBarController];
    // reload all tables in all views in the table view controller.
    if (tabBarController){
        NSArray *allTabbedControllers = [tabBarController viewControllers];
        for (UIViewController *vc in allTabbedControllers){
            if ([vc conformsToProtocol:@protocol(RefreshablePage)]){
                UIViewController<RefreshablePage>  *vcRefresh= (UIViewController<RefreshablePage>  *)vc;
                [vcRefresh refreshPage];
            }
        }
    }
    
    [self refreshPage];
    
    [[self hypervisorConnection] setWalkTreeMode:NO];
}

-(void)ensureObjectTreeLoaded{
    HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    
    // link in the UIDataload to make sure that the object has all its children
    
    NSPredicate *objectPred = [XenBase XenBaseFor:[self xenVMRef]];
    
    NSArray *objects = [hypervisorConnection hypObjectsForType:HYPOBJ_VM withCondition:objectPred];
    if ([objects count] >0){
        [hypervisorConnection setWalkTreeMode:YES];
        UIWaitForDataLoad *dataLoader = [[UIWaitForDataLoad alloc] initWithHypervisorConnection:hypervisorConnection];
        [dataLoader setUIWaitForDataLoadDelegate:self];
        [dataLoader waitForObjectTreeForObjectReference:[self xenVMRef] objectType:HYPOBJ_VM];
    }
}

#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)xenHypervisor:(XenHypervisorConnection *)hypervisorConnection updatesObjectsOfType:(NSInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // Nothing required here, everything is on the view root object, this will be handeled
    // by the XenGeneralViewController.  Only need to listed to obejcts that are
    // not the root object in these 'extention' pages.
}
@end
