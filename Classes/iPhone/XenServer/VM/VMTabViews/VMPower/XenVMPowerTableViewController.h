//
//  XenVMPowerViewTableController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenVM.h"
#import "GradientView.h"
#import "Identifyable.h"
#import "RefreshablePage.h"
#import "UIWaitForDataLoad.h"

@interface XenVMPowerTableViewController : UITableViewController<HypervisorConnectionDelegate,Identifyable,RefreshablePage,UIWaitForDataLoadDelegate> {
    NSString *xenVMRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;NSUInteger  powerOpINProgress;
}
- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withVM:(XenVM *)thisXenVM;

// this is used by relection in the tab contol to update the page.  In this class it updates the xenVMRef.
- (void) setId:(NSString*)newID;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenVMRef;
@property NSUInteger  powerOpINProgress;

@end


