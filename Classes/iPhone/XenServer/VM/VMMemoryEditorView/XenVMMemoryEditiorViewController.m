//
//  VMMemoryEditiorViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#define MemoryMultiplier (1024*1024)
#import "XenVMMemoryEditiorViewController.h"
#import "XenVM.h"
#import "HypervisorConnectionFactory.h"
#import "ProgressAlert.h"

@interface XenVMMemoryEditiorViewController (){
    HypervisorType hypervisorType;
    
    NSString* requestString;
    
    NSString* hypConnectionID;
    
    bool localDynamicOverride;NSUInteger staticMax;NSUInteger dynamicMin;NSUInteger staticMin;

    bool isVisible;
    ProgressAlert* progressAlert;
}

-(void) updateDisplay;
- (bool) canChangeStaticRange;
- (bool) canChangeDynamicRange;
@end

@implementation XenVMMemoryEditiorViewController

@synthesize xenVMRef = _xenVMRef;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *) [HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:hypConnectionID];
}

- (XenVM *) xenVM{
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVMRef]]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }
    return nil;
}

- (id)initWithHypervisorConnectionID:(NSString*)connectionID andXenVMRef:(NSString*)vmRef
{
    self = [super initWithNibName:@"XenVMMemoryEditiorViewController" bundle:nil];
    if (self) {
        // Custom initialization
        hypConnectionID = connectionID;
        _xenVMRef = vmRef;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // make sure that the view is not displayed behind the navigation bar if there is one
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // Do any additional setup after loading the view from its nib.
    [statusLabel setHidden:YES];
    localDynamicOverride = [[self xenVM] isMemoryDynamic];
    
    NSNumber *memNum = [[ self xenVM] memory_static_max];
    long long mem = [memNum longLongValue];NSUInteger finalMemMb = (NSInteger)mem/MemoryMultiplier;
    staticMax = finalMemMb;

    memNum = [[ self xenVM] memory_static_min];
    mem = [memNum longLongValue];
    finalMemMb = (NSInteger)mem/MemoryMultiplier;
    staticMin = finalMemMb;

    memNum = [[ self xenVM] memory_dynamic_min];
    mem = [memNum longLongValue];
    finalMemMb = (NSInteger)mem/MemoryMultiplier;
    dynamicMin = finalMemMb;
}


- (bool) canChangeDynamicRange{
    for (NSString *op in [[self xenVM] allowed_operations]) {
        if ([op isEqualToString:@"changing_dynamic_range"]){
            return YES;
        }
    }
    return NO;
}

- (bool) canChangeStaticRange{
    for (NSString *op in [[self xenVM] allowed_operations]) {
        if ([op isEqualToString:@"changing_static_range"]){
            return YES;
        }
    }
    return NO;
}

-(void) updateDisplay{
    
    bool canChangeStatic = [self canChangeStaticRange];
    bool canChangeDynamic = [self canChangeDynamicRange];
    
    [dynamicMemory setEnabled:canChangeDynamic];
    
    if (!canChangeDynamic && !canChangeStatic){
        [allControlsView setHidden:YES];
        [vmRunningWarning setHidden:NO];
    }
    else{
        [totalMemoryRunningWarning setHidden:canChangeStatic];
        [staticMaxStepper setHidden:!canChangeStatic];
        [staticMaxValueLabel setHidden:!canChangeStatic];
        
        [allControlsView setHidden:NO];
        [vmRunningWarning setHidden:YES];
        [dynamicMemory setOn:localDynamicOverride];
        
        [staticMaxStepper setMinimumValue:staticMin];
        [staticMaxStepper setMaximumValue:10000];
        [staticMaxStepper setValue:staticMax];
        [staticMaxStepper setStepValue:128];
        [staticMaxValueLabel setText:[NSString stringWithFormat:@"%lu Mb",(unsigned long)staticMax]];
        
        if (localDynamicOverride){
            [dynamicMinView setHidden:NO];
            if (staticMax < dynamicMin)
            {
                dynamicMin = staticMax;
            }
            [dynamicMinStepper setMinimumValue:staticMin];
            [dynamicMinStepper setMaximumValue:staticMax];
            [dynamicMinStepper setValue:dynamicMin];
            [dynamicMinStepper setStepValue:128];
            [dynamicMinValueLabel setText:[NSString stringWithFormat:@"%lu Mb",(unsigned long)dynamicMin]];
        } else{
            [dynamicMinView setHidden:YES];
            // keep all values the same in non dynamic mode
            dynamicMin = staticMax;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];

    // enable the screen protection by proximity
    [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
    
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
    [self updateDisplay];
    isVisible = YES;
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    // turn proximity sensor back off again
    [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
    isVisible = NO;
}


-(IBAction) dyynamicMemorySwitchValueChanged:(id)sender{
    localDynamicOverride = [dynamicMemory isOn];
    [self updateDisplay];
}

// fired when Max memory stepper value changes
-(IBAction) staticMaxStepperValueDataChanged:(id) sender{
    // if the val
    // need to format this. from bytes :)
    staticMax = [(UIStepper*)sender value];
    [self updateDisplay];
}

 // fired when dynamic Min stepper value changes
-(IBAction) dynamicMinStepperValueDataChanged:(id) sender{
    dynamicMin = [(UIStepper*)sender value] ;
    [self updateDisplay];
}
    
-(IBAction) PerformDataChange:(id) sender{
    NSLog(@"setting memory limits smin=%lu,smax=%lu,dmin=%lu,dmax=%lu",(unsigned long)staticMin,(unsigned long)staticMax,(unsigned long)dynamicMin,(unsigned long)staticMax);
    
    [[self hypervisorConnection] SetVMMemoryLimitsForVMRef:[self xenVMRef]
                                                 staticMin:(long long)staticMin*MemoryMultiplier
                                                 staticMax:(long long)staticMax*MemoryMultiplier
                                                dynamicMin:(long long)dynamicMin*MemoryMultiplier
                                                dynamicMax:(long long)staticMax*MemoryMultiplier];
    
    // display the updating message so that the user knows an update is occuring
    progressAlert = [[ProgressAlert alloc] initWithTitle:@"Updating" delegate:self];
    [progressAlert show];
}

-(void) dealloc{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

-(void) UpdateComplete{
    [statusLabel setHidden:YES];
    [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
    progressAlert = nil;
    if (!isVisible){
        [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    }
}

-(void) UpdateFailed:(NSString*)theIssue{
    [statusLabel setHidden:NO];
    [statusLabel setText:theIssue];
    [statusLabel setTextColor:[UIColor redColor]];
    [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
    progressAlert = nil;
    if (!isVisible){
        [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    }
}


#pragma mark - 
#pragma mark hypervisor Connecton Delegate
-(void) hypervisorResponseReceived:(HypervisorConnectionFactory *)hypervisorConnection requestCmd:(NSString *)cmd error:(NSError *)error{
    if ([cmd isEqualToString:CMD_VM_SET_MEMORYLIMITS]){
        if (error){
            [self UpdateFailed:[error domain]];
        }
        else{
            [self UpdateComplete];
        }
    }
}
@end
