//
//  VMMemoryEditiorViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import <UIKit/UIKit.h>
#import "HypervisorConnectionFactory.h"

@interface XenVMMemoryEditiorViewController : UIViewController<HypervisorConnectionDelegate>{
    IBOutlet UILabel* statusLabel;
    IBOutlet UIView* dynamicMinView;
    IBOutlet UILabel* dynamicMinValueLabel;
    IBOutlet UIStepper* dynamicMinStepper;
    IBOutlet UILabel* staticMaxValueLabel;
    IBOutlet UIStepper* staticMaxStepper;
    IBOutlet UISwitch* dynamicMemory;
    
    IBOutlet UIView* allControlsView;
    
    IBOutlet UILabel* vmRunningWarning;
    
    IBOutlet UILabel* totalMemoryRunningWarning;
    
    NSString* _xenVMRef;
}

// constructor for Int
- (id)initWithHypervisorConnectionID:(NSString*)connectionID andXenVMRef:(NSString*)vmRef;

// call this when appropriate data changes are acknoledged from the hypervisor
-(void) UpdateComplete;
-(void) UpdateFailed:(NSString*)theIssue;

-(IBAction) PerformDataChange:(id) sender;  // fired when the data changes and the backend needs to be updated

-(IBAction) staticMaxStepperValueDataChanged:(id) sender; // fired when Max memory stepper value changes
-(IBAction) dynamicMinStepperValueDataChanged:(id) sender; // fired when dynamic Min stepper value changes

-(IBAction) dyynamicMemorySwitchValueChanged:(id)sender;

@property (readonly) NSString* xenVMRef;
@end
