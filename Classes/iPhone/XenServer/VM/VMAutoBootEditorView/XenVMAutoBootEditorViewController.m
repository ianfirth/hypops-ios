//
//  VMMemoryEditiorViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#define MemoryMultiplier (1024*1024)
#import "XenVMAutoBootEditorViewController.h"
#import "XenVM.h"
#import "HypervisorConnectionFactory.h"
#import "ProgressAlert.h"

@interface XenVMAutoBootEditorViewController (){
    HypervisorType hypervisorType;
    
    NSString* requestString;
    
    NSString* hypConnectionID;
    bool isVisible;
    ProgressAlert* progressAlert;
}

-(void) updateDisplay;

@end

@implementation XenVMAutoBootEditorViewController

@synthesize xenVMRef = _xenVMRef;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *) [HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:hypConnectionID];
}

- (XenVM *) xenVM{
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVMRef]]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }
    return nil;
}

- (id)initWithHypervisorConnectionID:(NSString*)connectionID andXenVMRef:(NSString*)vmRef
{
    self = [super initWithNibName:@"XenVMAutoBootEditorViewController" bundle:nil];
    if (self) {
        // Custom initialization
        hypConnectionID = connectionID;
        _xenVMRef = vmRef;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // make sure that the view is not displayed behind the navigation bar if there is one
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // Do any additional setup after loading the view from its nib.
    [statusLabel setHidden:YES];
}

-(void) updateDisplay{
    [autoBoot setOn:[[self xenVM] auto_power_on]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // enable the screen protection by proximity
    [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
    
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
    [self updateDisplay];
    isVisible = YES;
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    // turn proximity sensor back off again
    [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
    isVisible = NO;
}

-(void) dealloc{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

-(BOOL)shouldAutorotate{
    return NO;
}

-(IBAction) PerformDataChange:(id) sender{    

    NSDictionary* configSettings;
    if ([autoBoot isOn]){
        configSettings = [NSDictionary dictionaryWithObjectsAndKeys:@"true",@"auto_poweron", nil];
    }
    else{
        configSettings = [NSDictionary dictionaryWithObjectsAndKeys:@"false",@"auto_poweron", nil];
    }
   
    progressAlert = [[ProgressAlert alloc] initWithTitle:@"Updating" delegate:self];
    [progressAlert show];

    [[self hypervisorConnection] SetVMOtherConfig:configSettings forVMRef:[[self xenVM] opaque_ref]] ;
}

-(void) UpdateComplete{
    [statusLabel setHidden:YES];
    [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
    progressAlert = nil;
    if (!isVisible){
        [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    }
}

-(void) UpdateFailed:(NSString*)theIssue{
    [statusLabel setHidden:NO];
    [statusLabel setText:theIssue];
    [statusLabel setTextColor:[UIColor redColor]];
    [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
    progressAlert = nil;
    if (!isVisible){
        [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    }
}


#pragma mark - 
#pragma mark hypervisor Connecton Delegate
-(void) hypervisorResponseReceived:(HypervisorConnectionFactory *)hypervisorConnection requestCmd:(NSString *)cmd error:(NSError *)error{
    if ([cmd isEqualToString:CMD_VM_SET_OTHERCONFIG]){
        if (error){
            [self UpdateFailed:[error domain]];
        }
        else{
            [self UpdateComplete];
        }
    }
}
@end
