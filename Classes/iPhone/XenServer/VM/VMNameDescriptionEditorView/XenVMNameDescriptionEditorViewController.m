//
//  XenVMNameDescriptionEditorViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#define MemoryMultiplier (1024*1024)
#import "XenVMNameDescriptionEditorViewController.h"
#import "XenVM.h"
#import "HypervisorConnectionFactory.h"
#import "ProgressAlert.h"

@interface XenVMNameDescriptionEditorViewController (){
    HypervisorType hypervisorType;
    
    NSString* requestString;
    
    NSString* hypConnectionID;
    ProgressAlert* progressAlert;
    bool isVisible;
}
- (void) animateTextView: (UITextView*) textView up: (BOOL) up;
-(void) updateDisplay;
@end

@implementation XenVMNameDescriptionEditorViewController

@synthesize xenVMRef = _xenVMRef;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *) [HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:hypConnectionID];
}

- (XenVM *) xenVM{
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:[self xenVMRef]]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }
    return nil;
}

- (id)initWithHypervisorConnectionID:(NSString*)connectionID andXenVMRef:(NSString*)vmRef
{
    self = [super initWithNibName:@"XenVMNameDescriptionEditorViewController" bundle:nil];
    if (self) {
        // Custom initialization
        hypConnectionID = connectionID;
        _xenVMRef = vmRef;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // make sure that the view is not displayed behind the navigation bar if there is one
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    // Do any additional setup after loading the view from its nib.
    [statusLabel setHidden:YES];
}


-(void) updateDisplay{
    [nameTextField setText:[[self xenVM] name_label]];
    [descriptionTextField setText:[[self xenVM] name_description]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // enable the screen protection by proximity
    [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
    
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
    [nameTextField setDelegate:self];
    [descriptionTextField setDelegate:self];
    [self updateDisplay];
    isVisible = YES;
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    // turn proximity sensor back off again
    [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
    isVisible = NO;
}

-(void) dealloc{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

-(BOOL)shouldAutorotate{
    return NO;
}

-(void) UpdateComplete{
    [statusLabel setHidden:YES];
    [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
    progressAlert = nil;
    if (!isVisible){
         [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    }
}

-(void) UpdateFailed:(NSString*)theIssue{
    [progressAlert dismissWithClickedButtonIndex:0 animated:YES];
    progressAlert = nil;
    [statusLabel setHidden:NO];
    [statusLabel setText:theIssue];
    [statusLabel setTextColor:[UIColor redColor]];
    if (!isVisible){
        [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];  
    }
}

#pragma mark - 
#pragma mark hypervisor Connecton Delegate
-(void) hypervisorResponseReceived:(HypervisorConnectionFactory *)hypervisorConnection requestCmd:(NSString *)cmd error:(NSError *)error{
    if ([cmd isEqualToString:CMD_VM_SET_NAME_DESCRIPTION] ||
        [cmd isEqualToString:CMD_VM_SET_NAME_LABEL]){
        if (error){
            [self UpdateFailed:[error domain]];
        }
        else{
            [self UpdateComplete];
        }
    }
}

#pragma mark -
#pragma mark UITextViewDelegate
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text length] == 0){
        return YES;
    }
    NSString* theLastChar = [text substringFromIndex:[text length] - 1];
    
   if ([theLastChar characterAtIndex:0] == 0xa){
        [textView resignFirstResponder];
    }
    return YES;
}

//-(void)textViewDidChange:(UITextView *)textView{
//   // if the last character is a return remove it and hide the keyboard
//    NSString* theText = [textView text];
//    
//    NSString* theLastChar = [theText substringFromIndex:[theText length] - 1];\
//
//    // this is no good, the last character might be a return, but that is not what is important. What is important is that the last
//    // character entered on the keyboard is a return and it is ignored.
//    if ([theLastChar characterAtIndex:0] == 0xa){
//      [textView resignFirstResponder];        
//    }
//}

-(BOOL)textViewShouldReturn:(UITextView *)textView {
    // close the keyboard
    [textView resignFirstResponder];
    return YES;
}

- (void) animateTextView: (UITextView*) textView up: (BOOL) up
{
    // if device is an iphone otherwise dont do this.
    if ([[UIDevice currentDevice] userInterfaceIdiom ]== UIUserInterfaceIdiomPad){
       return;
    }NSUInteger animatedDistance;NSUInteger moveUpValue = textView.frame.origin.y+ textView.frame.size.height;
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        
        animatedDistance = 216-(460-moveUpValue-5);
    }
    else
    {
        animatedDistance = 162-(320-moveUpValue-5);
    }
    
    if(animatedDistance>0)
    {
        const NSUInteger movementDistance = animatedDistance;
        const float movementDuration = 0.3f;
        NSUInteger movement = (up ? -movementDistance : movementDistance);
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateTextView:textView up:YES];
}

- (void) textViewDidEndEditing:(UITextView *)textView{
    [self animateTextView:textView up:NO];
    if ([textView tag] == 100){
        if (![[nameTextField text] isEqualToString:[[self xenVM] name_label]]){
            NSLog(@"setting the name for the VM to %@", [nameTextField text]);
            progressAlert = [[ProgressAlert alloc] initWithTitle:@"Updating" delegate:self];
            [progressAlert show];
            [[self hypervisorConnection] SetVMNameForVMRef:[self xenVMRef] name:[nameTextField text]];
        }
    }
    if ([textView tag] == 200){
        if (![[descriptionTextField text] isEqualToString:[[self xenVM] name_description]]){
            NSLog(@"setting the description for the VM to %@", [descriptionTextField text]);
            progressAlert = [[ProgressAlert alloc] initWithTitle:@"Updating" delegate:self];
            [progressAlert show];
            [[self hypervisorConnection] SetVMDescriptionForVMRef:[self xenVMRef] name:[descriptionTextField text]];
        }
    }
}

@end
