//
//  XenVMListByTagViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#import "XenVMListByTagViewController.h"
#import "ImageBuilder.h"
#import "XenVMListViewController.h"
#import "EC2ImageWrapper.h"
#import "ConnectionObjectUITableViewCell.h"

@interface XenVMListByTagViewController ()
- (void) reloadTableData;
-(NSPredicate *) getVMPredicateForTag:(NSString *) tag;
@end

@implementation XenVMListByTagViewController
@synthesize tags, hypConnID ,rootPredicate;

- (HypervisorConnectionFactory*) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

#pragma mark -
#pragma mark View lifecycle

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withPredicate:(NSPredicate *)predicate{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        [self setRootPredicate:predicate];
        [self setHypConnID:[hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
        // always enough room in portrait so only enable scroll if pull to refresh available
        [[self tableView] setScrollEnabled:YES];
        NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:5];
        // build each platform
        NSArray *vms = [hypervisorConnection hypObjectsForType:HYPOBJ_VM withCondition:predicate];
        for (XenVM *vm in vms) {
            NSArray *allTags = [vm tags];
            if (allTags){
                for (NSString *tag in allTags){
                   if (![tempArray containsObject:tag]){
                       [tempArray addObject:tag];
                   }
                }
            }
        }
        NSArray *c = [tempArray copy];
        [self setTags:c];
    }
    return self;
}

- (void) dealloc{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self tableView] setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh)
             forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    [[self tableView] registerNib:[UINib nibWithNibName:@"ConnectionObjectUITableViewCell" bundle:nil] forCellReuseIdentifier:@"ConnectionObjectUITableViewCell"];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(ConnectionObjectUITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSString *displayText;
    
    NSString *tag = (NSString *)[[self tags] objectAtIndex:indexPath.row];
    NSPredicate *finalPredicate = [self getVMPredicateForTag:tag];NSUInteger objectCount = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM withCondition:finalPredicate] count];
    
    displayText = tag;
    [[cell imageView2] setImage:[ImageBuilder vMTaggedImage]];
    
    NSString *detailText;
    detailText = [NSString stringWithFormat:@"%lu",(unsigned long)objectCount];
    
    // set the selection style and the accessory type
    if (objectCount >0){
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    } else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    // set the label text
    [[cell textLabel2] setText:displayText];
    
    // set the detail text
    if (detailText != nil) {
        [[cell detailTextLabel2] setText:detailText];
    }
    
    // make every other row use alternate color
    [cell setAlternate:indexPath.row %2];
    
    [[cell refreshButton] setHidden:YES];

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self tags] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"ConnectionObjectUITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ConnectionObjectUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:(ConnectionObjectUITableViewCell*)cell atIndexPath:indexPath];
    
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

// when selecting the object show the actaul images that meet the defined predicates
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *tag = (NSString *)[[self tags] objectAtIndex:indexPath.row];
    NSPredicate *finalPredicate = [self getVMPredicateForTag:tag];
        
    XenVMListViewController *vMListViewController = [[XenVMListViewController alloc] initWithHypervisorConnection:(XenHypervisorConnection*)[self hypervisorConnection] withCondition:finalPredicate];
    
    [vMListViewController setTitle:tag];
    [self.navigationController pushViewController:vMListViewController animated:YES];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}


#pragma mark -
#pragma mark Private methods

-(void) reloadTableData{
    [[self tableView] reloadData];
}

-(NSPredicate *) getVMPredicateForTag:(NSString *) tag{
    NSPredicate *tagPredicate = [XenVM VM_TaggedWith:tag];
    
    NSPredicate *finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:tagPredicate,[self rootPredicate], nil]];
    return finalPredicate;
}


#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {    
    if (![[self hypervisorConnection] isUpdatePendingForType:HYPOBJ_VM]){
        [[self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_VM];
    }
}

#pragma mark -
#pragma mark HypervisorConnection protocol implementation

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | HYPOBJ_VM) >0){
        //[self setXenObjectList:[xenHypervisorConnection hypObjectsForType:hypObjectType withCondition:predicate]];
        [self reloadTableData];
        [self.refreshControl endRefreshing];
    }
}


@end

