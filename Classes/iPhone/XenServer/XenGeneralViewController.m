//
//  XenVMGeneralViewController.m
//  hypOps
//
//  Created by Ian Firth on 04/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenGeneralViewController.h"
#import "XenImageBuilder.h"
#import "XenHypervisorConnection.h"

@implementation XenGeneralViewController

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)xenHypConnection
           xenDescritiveBase:(XenDescriptiveBase *)xenDescriptiveBase
                 displayMode:(GeneralDisplayMode)theMode
           tabName:(NSString *)tabName
   tabBarImageName:(NSString *)imageName 
             image:(UIImage *)mainImage 
extentionViewController:(UIViewController<Identifyable,RefreshablePage> *)theExtentionViewController
              titlePressedSelector:(SEL)titleSelected
selectorOnObject:(id) selctorObject{
    NSPredicate *objectPred = [XenBase XenBaseFor:[xenDescriptiveBase opaque_ref]];
    self = [super initWithHypervisorConnection:xenHypConnection 
                                 rootObjectRef:[xenDescriptiveBase opaque_ref]
                           rootObjectPredicate:objectPred
                                 hypObjectType:[xenDescriptiveBase objectType]
                              namePropertyName:@"name_label"
                       descriptionPropertyName:@"name_description"
                                   displayMode:theMode
                                       tabName:tabName 
                               tabBarImageName:imageName 
                                         image:mainImage 
                    extentionViewController:theExtentionViewController
                   titlePressedSelector:titleSelected
                              selectorOnObject:selctorObject];
    
    [self addChildViewController:theExtentionViewController];
    [theExtentionViewController didMoveToParentViewController:self];
    return self;
}

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)xenHypConnection
                 xenDescritiveBase:(XenDescriptiveBase *)xenDescriptiveBase
                       displayMode:(GeneralDisplayMode)theMode
                           tabName:(NSString *)tabName
                   tabBarImageName:(NSString *)imageName
                             image:(UIImage *)mainImage
           extentionViewController:(UIViewController<Identifyable,RefreshablePage> *)theExtentionViewController{
    
    return [self initWithHypervisorConnection:xenHypConnection
                            xenDescritiveBase:xenDescriptiveBase
                                  displayMode:theMode
                                      tabName:tabName
                              tabBarImageName:imageName
                                        image:mainImage
                      extentionViewController:theExtentionViewController
                         titlePressedSelector:nil
            selectorOnObject:nil];
}
@end
