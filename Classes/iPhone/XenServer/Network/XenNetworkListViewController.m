//
//  XenHostListViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenNetworkListViewController.h"
#import "ImageBuilder.h"
#import "MultiLineDetailUITableViewCell.h"

@implementation XenNetworkListViewController

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withCondition:(NSPredicate *) condition{
    self = [super initWithHypervisorConnection:hypervisorConnection withCondition:condition forHypObjectType:HYPOBJ_NETWORK withSearch:YES];
    self.title = @"Networks";
    return self;
}


// render the cell for the table view
- (void) configureCell:(MultiLineDetailUITableViewCell *)cell forXenObject:(XenBase *)xenObject{
    // set the lable text for the cell
    [[cell textLabel2] setText:[(XenDescriptiveBase *)xenObject name_label]];
    // set the detail text for the cell
    [[cell detailTextLabel2] setText:[self descriptionStringForXenObject:xenObject]];
    // set the image for the cell
    [[cell imageView2] setImage:[ImageBuilder networkImage]];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone]; 
}

-(NSString*) descriptionStringForXenObject:(XenBase *)xenObject{
    return [(XenDescriptiveBase *)xenObject name_description];
}

// called when an object is selected in the list
- (void) didSelectObject:(XenBase *)selectedObject{
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

@end
