//
//  XenPoolViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenPoolViewController.h"
#import "XenVMListByStatusViewController.h"
#import "XenHostListViewController.h"
#import "XenTemplateListViewController.h"
#import "XenStorageListViewController.h"
#import "XenNetworkListViewController.h"
#import "ImageBuilder.h"
#import "UIWaitForDataLoad.h"
#import "XenPoolViewController_ipad.h"
#import "ConnectionObjectUITableViewCell.h"


// define the private methods
@interface XenPoolViewController ()
@end

@implementation XenPoolViewController

@synthesize xenCatagories;

#pragma mark -
#pragma mark View lifecycle

-(void)dealloc{
    NSLog(@"XenPoolViewController being Deallocated");
}

- (void) viewWillAppear:(BOOL)animated{
    // request data for any object sets that are marked as out of date
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    if (xenHypervisorConnection){
       [xenHypervisorConnection addHypervisorConnectionDelegate:self];
    }
    [[self tableView] reloadData];
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated{
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    [xenHypervisorConnection removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

/*
 * When load set the title and the name of the pool
 * This should only be done to make sure that the connection is populated with all
 * the top level objects the first time that the page is displayed after connection
 * that is why this is in the on load, otherwise it would happen every time the 
 * page appears.
 */
- (void)viewDidLoad {
    // request data for any object sets that are marked as out of date
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    [xenHypervisorConnection setWalkTreeMode:NO];
  
    [self setXenCatagories:[NSArray arrayWithObjects:
                            [NSNumber numberWithInt:HYPOBJ_HOST],
                            [NSNumber numberWithInt:HYPOBJ_VM],
                            [NSNumber numberWithInt:HYPOBJ_TEMPLATE],
                            [NSNumber numberWithInt:HYPOBJ_STORAGE],
                            [NSNumber numberWithInt:HYPOBJ_NETWORK],
                            nil]];    
   
    [xenHypervisorConnection reloadCoreData];
    
    self.title = @"Xen Pool";
    [[self tableView] registerNib:[UINib nibWithNibName:@"ConnectionObjectUITableViewCell" bundle:nil] forCellReuseIdentifier:@"ConnectionObjectUITableViewCell"];
    [super viewDidLoad];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(ConnectionObjectUITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    NSNumber *xenCatagory = [[self xenCatagories] objectAtIndex:indexPath.row];
    NSString *displayText;
    UIImage *image = nil;
    BOOL isLoading = NO;
    BOOL isUpdateAvailable = YES;
    BOOL validCatagory = YES;NSUInteger  hypObjType = [xenCatagory intValue];
    long objectCount = 0;
    
    switch (hypObjType) {
        case HYPOBJ_VM:
            displayText = @"VMs";
            image = [ImageBuilder vMImage];
            break;
        case HYPOBJ_HOST:
            displayText = @"Hosts";
            image = [ImageBuilder hostImage];
            break;
        case HYPOBJ_TEMPLATE:
            displayText = @"Templates";
            image = [ImageBuilder vMTemplateImage];
            break;
        case HYPOBJ_STORAGE:
            displayText = @"Storage";
            image = [ImageBuilder storageImage];
            break;
        case HYPOBJ_NETWORK:
            displayText = @"Network";
            image = [ImageBuilder networkImage];
            break;
        default:
            validCatagory = NO;
            break;
    }
    
    if (validCatagory){       
        NSString *detailText;
        isUpdateAvailable = [xenHypervisorConnection isUpdateAvailableForType:hypObjType];
        isLoading =  [xenHypervisorConnection isUpdatePendingForType:hypObjType];

        if (!isLoading){
            objectCount = [[xenHypervisorConnection hypObjectsForType:hypObjType] count];
            detailText = [NSString stringWithFormat:@"%lu",objectCount];
            // add the navigation indicator to the cell
            if (objectCount > 0){
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
            [cell setLoading:NO];
        }
        else {

            // remove the navigation indicator to the cell
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell setLoading:YES];
        }
        
        // set the default selection style
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        
        // put the refresh button in place
        if (!isLoading && ![xenHypervisorConnection isAutoUpdateEnabled] && isUpdateAvailable){
            // construct the refresh button
            [cell setManualRefresh:YES];
            
            [[cell refreshButton] addTarget:self action:@selector(refreshButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            [[cell refreshButton] setTag:hypObjType];
        }
        else{
            [cell setManualRefresh:NO];
        }
        
        // set the image for the cell
        [[cell imageView2] setImage:image];
        [[cell textLabel2] setText:displayText];
        
        // set the detail text
        if (detailText != nil) {
            [[cell detailTextLabel2] setText:detailText];
        }
    }        
    else {
        NSLog(@"Invalid cell catagory provided %@",xenCatagory);
    }
    // make every other row use alternate color
    [cell setAlternate:indexPath.row %2];
}

-(void)refreshButtonClick:(id)sender{
    // get the button tag
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    long hypObjectType = [sender tag];
    [xenHypervisorConnection RequestHypObjectsForType:hypObjectType];
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark HypervisorConnectionDelegate protocol

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    [[self tableView] reloadData];    
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.xenCatagories count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    static NSString *CellIdentifier = @"ConnectionObjectUITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ConnectionObjectUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:(ConnectionObjectUITableViewCell*)cell atIndexPath:indexPath];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    NSNumber *xenCatagory = [self.xenCatagories objectAtIndex:indexPath.row];
    NSUInteger  hypObjType = [xenCatagory intValue];
    if (![xenHypervisorConnection isUpdatePendingForType:hypObjType]){
        // check that the host objects are loaded into cache
        UIWaitForDataLoad *dataLoader = [[UIWaitForDataLoad alloc] initWithHypervisorConnection:xenHypervisorConnection];
        [dataLoader setUIWaitForDataLoadDelegate:self];
        NSLog(@"Created dataloader to wait for data to be loaded for the next page");
        [dataLoader waitForObjectsOfType:hypObjType];
    }
}

#pragma mark -
#pragma mark UIWaitForDownload delegate

- (void)dataLoaderFinished:(UIWaitForDataLoad *)dataLoader withResult:(BOOL)sucsess{
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    NSLog(@"dataloader reported that it has loaded the required data.");
    
    NSError *err = [dataLoader connectionError];NSUInteger  hypObjType = [dataLoader requestType];

    if (err){
        // if there was an error connecting do not do anyting.
        return;
    }

    switch (hypObjType) {
        case HYPOBJ_VM:{
            XenVMListByStatusViewController *vMListByStatusViewController = [[XenVMListByStatusViewController alloc] initWithNibName:@"XenVMListByStatus" bundle:nil hypervisorConnection:xenHypervisorConnection];
            [self.navigationController pushViewController:vMListByStatusViewController animated:YES];
            break;
        }
        case HYPOBJ_HOST:{
            XenHostListViewController *hostListViewController = [[XenHostListViewController alloc] initWithHypervisorConnection:xenHypervisorConnection  withCondition:nil];
            [self.navigationController pushViewController:hostListViewController animated:YES];
            break;
        }
        case HYPOBJ_TEMPLATE:{
            XenTemplateListViewController *templateListViewController = [[XenTemplateListViewController alloc] initWithHypervisorConnection:xenHypervisorConnection  withCondition:nil];
            [self.navigationController pushViewController:templateListViewController animated:YES];
            break;
        }
        case HYPOBJ_STORAGE:{
            XenStorageListViewController *storageListViewController = [[XenStorageListViewController alloc] initWithHypervisorConnection:xenHypervisorConnection  withCondition:nil];
            [self.navigationController pushViewController:storageListViewController animated:YES];
            break;
        }
        case HYPOBJ_NETWORK:{
            XenNetworkListViewController *networkListViewController = [[XenNetworkListViewController alloc] initWithHypervisorConnection:xenHypervisorConnection  withCondition:nil];

            [self.navigationController pushViewController:networkListViewController animated:YES];
            break;
        }
        default:
            break;
    }
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}


- (UIViewController *) detailContentView{
    UIViewController *poolView = [[XenPoolViewController_ipad alloc] initWithNibName:@"XenPool_ipad" bundle:[NSBundle mainBundle] andHypervisorConnectionId:[self hypConnID]];
    return poolView;
}

@end

