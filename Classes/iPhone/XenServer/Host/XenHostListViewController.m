//
//  XenHostListViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenHostListViewController.h"
#import "XenHostGeneralViewController.h"
#import "XenHostNicsViewController.h"
#import "XenHostStorageViewController.h"
#import "XenHostConsoleViewController.h"
#import "XenImageBuilder.h"
#import "ImageBuilder.h"
#import "XenHost.h"
#import "MultiLineDetailUITableViewCell.h"

@interface XenHostListViewController(){
    UITabBarController *tabBarController;    
}

@end

@implementation XenHostListViewController

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withCondition:(NSPredicate *) condition{
    self = [super initWithHypervisorConnection:hypervisorConnection withCondition:condition forHypObjectType:HYPOBJ_HOST  withSearch:YES];
    self.title = @"Hosts";
    return self;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

// render the cell for the table view
- (void) configureCell:(MultiLineDetailUITableViewCell *)cell forXenObject:(XenBase *)xenObject{
    // set the lable text for the cell
    [[cell textLabel2] setText:[(XenDescriptiveBase *)xenObject name_label]];
    // set the detail text for the cell
    [[cell detailTextLabel2] setText:[self descriptionStringForXenObject:xenObject]];
    // set the image for the cell
    [[cell imageView2] setImage:[XenImageBuilder buildVMImageForHost:(XenHost*)xenObject]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
}

- (NSString *)descriptionStringForXenObject:(XenBase *)xenObject{
    return [(XenDescriptiveBase *)xenObject name_description];
}

// called when an object is selected in the list
- (void) didSelectObject:(XenBase *)selectedObject{

    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    XenHost* selectedHost = (XenHost *)selectedObject;

    // add the general tab
    if (!tabBarController){
        
        tabBarController = [[UITabBarController alloc] init];
        [self addChildViewController:tabBarController];  // doing this allows tabbarController to navigationController to be passed down the chain of controllers.

        NSMutableArray *localControllersArray = [[NSMutableArray alloc] initWithCapacity:2];

        // general details page
        XenHostGeneralViewController *generaltvc = [[XenHostGeneralViewController alloc] initWithhypervisorConnection:xenHypervisorConnection
                                                                                                                       withHost:selectedHost];
        [tabBarController addChildViewController:generaltvc];
        [localControllersArray addObject:generaltvc];
        
        // Nics page
        XenHostNicsViewController *nicstvc = [[XenHostNicsViewController alloc] initWithhypervisorConnection: xenHypervisorConnection
                                                                                                              withHost:selectedHost];
        
        [tabBarController addChildViewController:nicstvc];
        [localControllersArray addObject:nicstvc];
        
        // Storage page
        XenHostStorageViewController *storagetvc = [[XenHostStorageViewController alloc] initWithhypervisorConnection: xenHypervisorConnection
                                                                                                                       withHost:selectedHost];
        [tabBarController addChildViewController:storagetvc];
        [localControllersArray addObject:storagetvc];

        // add the Console tab
        XenHostConsoleViewController *console = [[XenHostConsoleViewController alloc] initWithhypervisorConnection: xenHypervisorConnection
                                                                                                        withHost:(XenHost *)selectedObject];
        [tabBarController addChildViewController:console];
        [localControllersArray addObject:console];

        tabBarController.viewControllers = localControllersArray;
        [tabBarController setTitle:@"Host"];
    }
    else{
        for (UIViewController<RefreshablePage,Identifyable>* view in [tabBarController viewControllers]){
            // this should be the image general and it should refresh the children ones.
            [view setId:[selectedObject opaque_ref]];
            [view refreshPage];
        }
    }

    [self displayNextView:tabBarController];
}

@end
