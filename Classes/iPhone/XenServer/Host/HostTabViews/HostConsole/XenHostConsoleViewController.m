//
//  XenVMConsoleViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenHostConsoleViewController.h"
#import "XenVM.h"
#import "XenImageBuilder.h"

@implementation XenHostConsoleViewController

@synthesize hypConnID, xenHostRef;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenHost *) xenHost{
    NSArray *xenHosts = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST withCondition:[XenBase XenBaseFor:[self xenHostRef]]];
    if ([xenHosts count] >0){
        return [xenHosts objectAtIndex:0];
    }
    return nil;
}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withHost:(XenHost *)thisXenHost{
    self = [super initWithhypervisorConnection:hypervisorConnection];
    if (self){
        [self setXenHostRef: [thisXenHost opaque_ref]];
        [self setHypConnID: [hypervisorConnection connectionID]];

        NSArray* controlsVMs = [thisXenHost referencesForType:HYPOBJ_VM_CONTROL_DOMAIN];
        if (controlsVMs && [controlsVMs count] >0){
            [self setXenVMRef:[controlsVMs objectAtIndex:0]];
            [self setUseLowQuality:YES];
        }
        // do not allow mouse use on this as it causes issues.
        [self setUseMousePointer:NO];
        
        // set the tab bar button details
        self.tabBarItem.title = @"Console";
        //Set the image for the tab bar
        self.tabBarItem.image = [UIImage imageNamed:@"tab_Console"];

    }
    return self;
}

-(void) setDomainVMRefForHost:(NSString*)hostRef{
    NSArray* controlsVMs = [[self xenHostForReference:hostRef] referencesForType:HYPOBJ_VM_CONTROL_DOMAIN];
    if (controlsVMs && [controlsVMs count] >0){
        [self setXenVMRef:[controlsVMs objectAtIndex:0]];
    }
}

// this will be a host
- (void) setId:(NSString*)newID{
    [self setDomainVMRefForHost:newID];

    NSString* newLocation;
    XenConsole* console = [self xenConsole];
    if (console){
        newLocation = [console location];
    }
    
    if (lastVMRef != newID){
        NeedsNewConnection = YES;
        lastVMRef = newID;
    }
    else{
        NeedsNewConnection = NO;
    }
    
    // if the VM has not changes then still check to see if the location has
    // if it has a new connection will be needed.
    if (![newLocation isEqualToString:location]){
        NeedsNewConnection = YES;
    }
}

- (XenHost *) xenHostForReference:(NSString*)hostRef{
    NSArray *xenHosts = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST withCondition:[XenBase XenBaseFor:hostRef]];
    if ([xenHosts count] >0){
        return [xenHosts objectAtIndex:0];
    }
    return nil;
}

- (XenVM *) xenVM{
    NSArray *xenVMs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM_CONTROL_DOMAIN withCondition:[XenBase XenBaseFor:[self xenVMRef]]];
    if ([xenVMs count] >0){
        return [xenVMs objectAtIndex:0];
    }
    return nil;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self ensureObjectTreeLoaded];
}

#pragma mark - RefreshablePage protocol

- (void) refreshPage{
    [super refreshPage];
}

#pragma mark -
#pragma mark UIWaitForDownload delegate

- (void)dataLoaderFinished:(UIWaitForDataLoad *)dataLoader withResult:(BOOL)sucsess{
    NSLog(@"dataloader reported that it has loaded the required data.");
    
    NSError *err = [dataLoader connectionError];
    
    if (err){
        // if there was an error connecting do not do anyting.
        return;
    }
    
    // also invalidate the extention page so it redraws now all the data is available
    UITabBarController *tabBarController = [self tabBarController];
    // reload all tables in all views in the table view controller.
    if (tabBarController){
        NSArray *allTabbedControllers = [tabBarController viewControllers];
        for (UIViewController *vc in allTabbedControllers){
            if ([vc conformsToProtocol:@protocol(RefreshablePage)]){
                UIViewController<RefreshablePage>  *vcRefresh= (UIViewController<RefreshablePage>  *)vc;
                [vcRefresh refreshPage];
            }
        }
    }
    
    [self refreshPage];
    
    [[self hypervisorConnection] setWalkTreeMode:NO];
}

-(void)ensureObjectTreeLoaded{
    HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    
    // link in the UIDataload to make sure that the object has all its children
    
    NSPredicate *objectPred = [XenBase XenBaseFor:[self xenHostRef]];
    
    NSArray *objects = [hypervisorConnection hypObjectsForType:HYPOBJ_HOST withCondition:objectPred];
    if ([objects count] >0){
        [hypervisorConnection setWalkTreeMode:YES];
        UIWaitForDataLoad *dataLoader = [[UIWaitForDataLoad alloc] initWithHypervisorConnection:hypervisorConnection];
        [dataLoader setUIWaitForDataLoadDelegate:self];
        [dataLoader waitForObjectTreeForObjectReference:[self xenHostRef] objectType:HYPOBJ_HOST];
    }
}

@end
