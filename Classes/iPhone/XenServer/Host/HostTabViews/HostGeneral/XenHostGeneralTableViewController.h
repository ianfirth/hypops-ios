//
//  XenVMGeneralViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenHost.h"
#import "XenHypervisorConnection.h"
#import "ExpandableTableViewController.h"
#import "RRDDisplayButtonDelegate.h"
#import "RefreshablePage.h"
#import "Identifyable.h"
#import "UIWaitForDataLoad.h"

#define XENHOST_SECTION_GENERAL 0
#define XENHOST_SECTION_MANAGEMENT 1
#define XENHOST_SECTION_LICENSE 2
#define XENHOST_SECTION_MEMORY 3
#define XENHOST_SECTION_CPU 4
#define XENHOST_SECTION_TAGS 5
#define XENHOST_SECTION_COUNT 6

#define XENHOST_DETAILS_TABLE_SOFTWARE_DETAILS 0
#define XENHOST_DETAILS_TABLE_START_TIME 1
#define XENHOST_DETAILS_TABLE_MAINTANANCE_MODE 2
#define XENHOST_DETAILS_TABLE_TOOLS_START_TIME 3
#define XENHOST_DETAILS_TABLE_ISCSI 4
#define XENHOST_DETAILS_TABLE_CACHEENABLED 5
#define XENHOST_DETAILS_COUNT 6

#define XENHOST_MANAGEMENT_TABLE_HOSTNAME 0
#define XENHOST_MANAGEMENT_TABLE_ADDRESS 1 
#define XENHOST_MANAGEMENT_TABLE_XAPIPERFORMANCE 2
#define XENHOST_MANAGEMENT_COUNT 3

#define XENHOST_LICENSE_TABLE_EDITION 0 
#define XENHOST_LICENSE_TABLE_EXPIRY 1 
#define XENHOST_LICENSE_TABLE_ADDRESS 2 
#define XENHOST_LICENSE_TABLE_PORT 3 
#define XENHOST_LICENSE_COUNT 4 

#define XENHOST_CPU_TABLE_COUNT 0 
#define XENHOST_CPU_TABLE_VENDOR 1 
#define XENHOST_CPU_TABLE_MODEL 2 
#define XENHOST_CPU_TABLE_PERFORMANCEDATA 3
#define XENHOST_CPU_COUNT 4

#define XENHOST_MEMORY_TABLE_AVAILABLE 0 
#define XENHOST_MEMORY_TABLE_USED 1 
#define XENHOST_MEMORY_TABLE_PERFORMANCE 2
#define XENHOST_MEMORY_COUNT 3

@interface XenHostGeneralTableViewController :ExpandableTableViewController <HypervisorConnectionDelegate, RRDDisplayButtonDelegate, RefreshablePage, Identifyable,UIWaitForDataLoadDelegate> {
    NSString *xenHostRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;
    
    NSMutableDictionary* rrdXapiButtons;
    NSMutableDictionary* rrdMemoryButtons;
    NSMutableDictionary* rrdCpuButtons;
    
}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withHost:(XenHost *)thisXenHost;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenHostRef;

@end
