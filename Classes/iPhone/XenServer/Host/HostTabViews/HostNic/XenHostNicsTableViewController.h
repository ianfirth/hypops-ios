//
//  XenVMGeneralViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenHost.h"
#import "XenHypervisorConnection.h"
#import "ExpandableTableViewController.h"
#import "RRDDisplayButtonDelegate.h"
#import "RefreshablePage.h"
#import "Identifyable.h"
#import "UIWaitForDataLoad.h"

#define XENHOST_NICS_TABLE_MAC 0
#define XENHOST_NICS_TABLE_IP 1
#define XENHOST_NICS_TABLE_PERFORMANCEDATA 2
#define XENHOST_NICS_TABLE_COUNT 3

@interface XenHostNicsTableViewController :ExpandableTableViewController<HypervisorConnectionDelegate,RRDDisplayButtonDelegate,Identifyable,UIWaitForDataLoadDelegate>{
    NSString *xenHostRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;

    // stores a list of rrdButtons for each nic
    // so that only one button is created for each object.
    NSMutableDictionary *rrdNicButtons;
}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withHost:(XenHost *)thisXenHost;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenHostRef;

@end
