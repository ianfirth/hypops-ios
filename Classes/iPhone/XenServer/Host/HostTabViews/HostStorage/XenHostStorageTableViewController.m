//
//  XenVMGeneralViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenHostStorageTableViewController.h"
#import "XenStorage.h"
#import "XenPBD.h"
#import "ScrollableDetailTextCell.h"
#import "RRDDisplayButton.h"
#import "DataSource.h"
#import "XenImageBuilder.h"

#define RRDButtonCell @"RRD"
#define GeneralCell @"General"

@interface XenHostStorageTableViewController ()
- (UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier;
- (UITableViewCell*) configureCellForTableView:(UITableView *)tableView forStorage:(XenStorage *)storage atIndex:(NSInteger) index;
-(XenStorage *) storageForSection:(NSInteger)section;
- (XenStorage *) xenStorageWithOpaqueRef:(NSString*)opaque_ref;
@end

@implementation XenHostStorageTableViewController

@synthesize hypConnID, xenHostRef;

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
}

- (XenHost *) xenHost{
    NSArray *xenHosts = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST withCondition:[XenBase XenBaseFor:[self xenHostRef]]];
    if ([xenHosts count] >0){
        return [xenHosts objectAtIndex:0];
    }
    return nil;
}

- (XenStorage *) xenStorageWithOpaqueRef:(NSString*)opaque_ref{
    NSArray *xenSRs = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_STORAGE withCondition:[XenBase XenBaseFor:opaque_ref]];
    if ([xenSRs count] >0){
        return [xenSRs objectAtIndex:0];
    }
    return nil;
}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withHost:(XenHost *)thisXenHost{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setXenHostRef:  [thisXenHost opaque_ref]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        rrdStorageButtons = [[NSMutableDictionary alloc ] init];
        // set the tab bar button details
        self.tabBarItem.title = @"Storage";
        //Set the image for the tab bar
        self.tabBarItem.image = [UIImage imageNamed:@"tab_Storage"];
    }
    return self;
    
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
    
    //this prevent the tabbar to cover the tableview space
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 90)];
    footer.backgroundColor = [UIColor clearColor];
    [self tableView].tableFooterView = footer;
}

- (void) viewWillAppear:(BOOL)animated{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    [xenHypervisorConnection addHypervisorConnectionDelegate:self];
    [[self tableView] reloadData];
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

-(void) viewDidDisappear:(BOOL)animated{
    // make sure that buttons are freed
    // this makes sure that the delegates are released
    // this enables this to be dealloc'd when the parent is
    for (RRDDisplayButton* but in [rrdStorageButtons allValues]) {
        [but setDelegate:nil];
    }
    [rrdStorageButtons removeAllObjects];
    [super viewDidDisappear:animated];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void) viewDidAppear:(BOOL)animated{
    // this gets the text in the labels scrolling again when the view appear, if this is not
    // here, then the text will not start scrolling again until selected. or moved off the screen
    // and back again.
    [[self tableView] reloadData];
    [super viewDidAppear:animated];
    [self ensureObjectTreeLoaded];
}

#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{

    // this code should only be checking for changes to storage  objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    if (objectType == HYPOBJ_STORAGE || objectType == HYPOBJ_PBD){
        [[self tableView ]reloadData];
    }
}

// provide access to the cell definitions
-(UITableViewCell*) getCellForTableView:(UITableView*)tableView andIdentifier:(NSString*)identifier{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    
    // general cells can have different components in them remove them if there are
    if ([identifier isEqualToString: GeneralCell]){
        [[[cell contentView] viewWithTag:100] removeFromSuperview];
    }

    return cell;    
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of NICs that the host has.
    return [[[self xenHost] referencesForType:HYPOBJ_PBD] count];
}

// returns nil if the storage is not in the cache, this is expected to rectify itself by a subsequent data load
-(XenStorage *) storageForSection:(NSInteger)section{
    XenHypervisorConnection *xenHypervisorConnection = [self hypervisorConnection];
    XenStorage *storage = nil;
    NSString *opRef = [[[self xenHost] referencesForType:HYPOBJ_PBD] objectAtIndex:section];
    NSArray *pbds = [xenHypervisorConnection hypObjectsForType:HYPOBJ_PBD withCondition:[XenBase XenBaseFor:opRef]];
    if ([pbds count] >0){
        XenPBD *pbd = [pbds objectAtIndex:0];
        // now go from pbd to the SR
        NSArray *storageList = [pbd referencesForType:HYPOBJ_STORAGE];
        if ([storageList count] >0){
            NSString *storageRef = [storageList objectAtIndex:0];
            NSArray *storageList = [xenHypervisorConnection hypObjectsForType:HYPOBJ_STORAGE withCondition:[XenBase XenBaseFor:storageRef]];
            if ([storageList count]>0){
                storage = [[xenHypervisorConnection hypObjectsForType:HYPOBJ_STORAGE withCondition:[XenBase XenBaseFor:storageRef]] objectAtIndex:0];
            }
        }
    }
    return storage;
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    XenStorage *storage = [self storageForSection:section];
    if (storage){
    return [storage name_label];
    }
    return @"No Storage object was available";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    XenStorage *storage = [self storageForSection:section];NSUInteger count = XENHOST_STORAGE_TABLE_COUNT;
    if ([storage local_cache_enabled]){
        count = count +1;
    }
    return count;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // all sections are expandable
    return YES;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        
    XenStorage *storage = [self storageForSection:[indexPath section]];

    return [self configureCellForTableView:tableView forStorage:storage atIndex:[indexPath row]];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (UITableViewCell*) configureCellForTableView:(UITableView *)tableView forStorage:(XenStorage *)storage atIndex:(NSInteger) index{
    
    UITableViewCell* cell = nil;
    
    switch (index) {
        case XENHOST_STORAGE_TABLE_TYPE:
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Type"];
            [[cell detailTextLabel] setText:[storage type]];
            break;
        case XENHOST_STORAGE_TABLE_CONTENTTYPE:
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Content Type"];
            [[cell detailTextLabel] setText:[storage contentType]];
            break;
        case XENHOST_STORAGE_TABLE_SHARED:
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Shared"];
            if ([storage shared]){
                [[cell detailTextLabel] setText:@"Yes"];
            }
            else
            {
                [[cell detailTextLabel] setText:@"No"];
            }
            break;
        case XENHOST_STORAGE_TABLE_USAGE:
        {
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            NSNumber *memTotalBytes = [storage physicalUtilization];
            long long memTotal = [memTotalBytes longLongValue]; 
            float finalmemTotalGb = ((float)memTotal)/1024/1024/1024;
            [[cell textLabel] setText:@"Usage"];
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Gb",finalmemTotalGb]];
            break;
        }
        case XENHOST_STORAGE_TABLE_SIZE:
        {
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            NSNumber *memTotalBytes = [storage physicalSize];
            long long memTotal = [memTotalBytes longLongValue]; 
            float finalmemTotalGb = ((float)memTotal)/1024/1024/1024;
            [[cell textLabel] setText:@"Size"];
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Gb",finalmemTotalGb]];
            break;
        }
        case XENHOST_STORAGE_TABLE_VIRTUALALLOCATION:
        {
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            NSNumber *memTotalBytes = [storage virtualAllocation];
            long long memTotal = [memTotalBytes longLongValue]; 
            float finalmemTotalGb = ((float)memTotal)/1024/1024/1024;
            [[cell textLabel] setText:@"Allocation"];
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%1.1f Gb",finalmemTotalGb]];
            break;
        }
        case XENHOST_STORAGE_TABLE_CACHEDRIVE:
        {
            cell = [self getCellForTableView:tableView andIdentifier:GeneralCell];
            [[cell textLabel] setText:@"Used by intellicache"];
            if ([storage local_cache_enabled]){
                [[cell detailTextLabel] setText:@"Yes"];
            }
            else
            {
                [[cell detailTextLabel] setText:@"No"];
            }
            break;
        }
        case XENHOST_STORAGE_TABLE_PERFORMANCEDATA:{
            cell = [self getCellForTableView:tableView andIdentifier:RRDButtonCell];
            [[cell detailTextLabel] setText:@""];
            [[cell textLabel] setText:@""];
            RRDDisplayButton* rrdButton = [rrdStorageButtons objectForKey:[storage uuid]];
            if (!rrdButton){
                rrdButton = [[RRDDisplayButton alloc] init];
                [self addChildViewController:rrdButton];
                [rrdStorageButtons setObject:rrdButton forKey:[storage uuid]];
                [rrdButton setDelegate:self];
                [[rrdButton view ]setTag:100];
                [rrdButton setReference:[storage opaque_ref]];
            }
            [[rrdButton view ]setFrame:[[cell contentView] bounds]];
            [[cell contentView] setAutoresizesSubviews:YES];
            [[cell contentView] addSubview:[rrdButton view]];
            [rrdButton viewWillAppear:YES];
            break;
        }
        default:
            break;
    }  
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];

    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark -
#pragma mark RRDDisplayButtonDelegate
    
- (RoundRobinDatabase *)roundRobinDatabase{
    XenHost *xenHost = [self xenHost];
    return [xenHost hostPerformanceData];
}

- (BOOL) reloadRoundRobinDatabase{
    XenHost *xenHost = [self xenHost];
    return [xenHost refreshPerformanceData];
}

-(NSDate*) getLastRefreshTime{
    XenHost *xenHost = [self xenHost];
    return [xenHost lastPreformanceDataUpdateTime];
}

-(NSString*)  graphTitleWithReference:(NSString*)sourcereference{
        XenStorage *xenStorage = [self xenStorageWithOpaqueRef:sourcereference];
        return [NSString stringWithFormat:@"Intellicache usage for %@", [xenStorage name_label]];
}

-(NSArray*)dataSourcesToGraphFromRRD:(RoundRobinDatabase*)rrd withReference:(NSString*)sourcereference{
    NSMutableArray* sourceArray = [[NSMutableArray alloc] initWithCapacity:1];
    // need to convert source referece to the actual reference here
    XenStorage *xenStorage = [self xenStorageWithOpaqueRef:sourcereference];
    NSString *storageRef = [NSString stringWithFormat:@"sr_%@_",[xenStorage uuid]];
    for (DataSource* dataSource in [rrd dataSources]) {
        if ([[dataSource name] hasPrefix:storageRef]){
            if ([[dataSource name] hasSuffix:@"_cache_size"]){
                [dataSource setAlternateName:@"Size"];
                [sourceArray addObject:dataSource];
            }
            if ([[dataSource name] hasSuffix:@"_cache_hits"]){
                [dataSource setAlternateName:@"Hits"];
                [sourceArray addObject:dataSource];
            }
            if ([[dataSource name] hasSuffix:@"_cache_misses"]){
                [dataSource setAlternateName:@"Misses"];
                [sourceArray addObject:dataSource];
            }
        }
    }
    return sourceArray;
}

-(double)yMinForDataSources:(NSArray*)dataSources withConsolidationStep:(NSInteger)step{
    return 0;
}
    
-(double) getPlotPointFromDataPoint:(DataPoint*)dataPoint{
    // beacuse using a minmanl RRD all points are the same
    return [dataPoint average];
//        double displayVal = [dataPoint average];
//        if (displayVal == 0){
//            displayVal = [dataPoint max];
//        }
//        // NSLog(@"%f",displayVal);
//        return displayVal;    
}
        
-(double)yMaxForDataSources:(NSArray*)dataSources withConsolidationStep:(NSInteger)step{
    return 1.0;
}

-(NSString*)yTitle{
    return @"";
}

-(NSNumberFormatter*) yFormatter{
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterPercentStyle];
    // Set to the current locale
    [formatter setLocale:[NSLocale currentLocale]];
    return formatter;
}


#pragma mark for Identifyable protocol

-(void) setId:(NSString *)newID{
    // not needed
}

#pragma mark Refreshable protocol

-(void)refreshPage{
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark UIWaitForDownload delegate

- (void)dataLoaderFinished:(UIWaitForDataLoad *)dataLoader withResult:(BOOL)sucsess{
    NSLog(@"dataloader reported that it has loaded the required data.");
    
    NSError *err = [dataLoader connectionError];
    
    if (err){
        // if there was an error connecting do not do anyting.
        return;
    }
    
    // also invalidate the extention page so it redraws now all the data is available
    UITabBarController *tabBarController = [self tabBarController];
    // reload all tables in all views in the table view controller.
    if (tabBarController){
        NSArray *allTabbedControllers = [tabBarController viewControllers];
        for (UIViewController *vc in allTabbedControllers){
            if ([vc conformsToProtocol:@protocol(RefreshablePage)]){
                UIViewController<RefreshablePage>  *vcRefresh= (UIViewController<RefreshablePage>  *)vc;
                [vcRefresh refreshPage];
            }
        }
    }
    
    [self refreshPage];
    
    [[self hypervisorConnection] setWalkTreeMode:NO];
}

-(void)ensureObjectTreeLoaded{
    HypervisorConnectionFactory *hypervisorConnection = [HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    
    // link in the UIDataload to make sure that the object has all its children
    
    NSPredicate *objectPred = [XenBase XenBaseFor:[self xenHostRef]];
    
    NSArray *objects = [hypervisorConnection hypObjectsForType:HYPOBJ_HOST withCondition:objectPred];
    if ([objects count] >0){
        [hypervisorConnection setWalkTreeMode:YES];
        UIWaitForDataLoad *dataLoader = [[UIWaitForDataLoad alloc] initWithHypervisorConnection:hypervisorConnection];
        [dataLoader setUIWaitForDataLoadDelegate:self];
        [dataLoader waitForObjectTreeForObjectReference:[self xenHostRef] objectType:HYPOBJ_HOST];
    }
}

@end
