//
//  XenVMGeneralViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenHost.h"
#import "XenHypervisorConnection.h"
#import "ExpandableTableViewController.h"
#import "RRDDisplayButtonDelegate.h"
#import "RefreshablePage.h"
#import "Identifyable.h"
#import "UIWaitForDataLoad.h"

#define XENHOST_STORAGE_TABLE_TYPE 0
#define XENHOST_STORAGE_TABLE_CONTENTTYPE 1
#define XENHOST_STORAGE_TABLE_SHARED 2
#define XENHOST_STORAGE_TABLE_CACHEDRIVE 3
#define XENHOST_STORAGE_TABLE_USAGE 4
#define XENHOST_STORAGE_TABLE_SIZE 5
#define XENHOST_STORAGE_TABLE_VIRTUALALLOCATION 6
#define XENHOST_STORAGE_TABLE_COUNT 7
// this is not included in the count above as it is optional
// and is only added in if required.
#define XENHOST_STORAGE_TABLE_PERFORMANCEDATA 7

@interface XenHostStorageTableViewController :ExpandableTableViewController<HypervisorConnectionDelegate, RRDDisplayButtonDelegate,RefreshablePage,Identifyable,UIWaitForDataLoadDelegate>{
    NSString *xenHostRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;
    
    NSMutableDictionary* rrdStorageButtons;
}

- (id)initWithhypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withHost:(XenHost *)thisXenHost;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

@property (copy) NSString *hypConnID;
@property (copy) NSString *xenHostRef;

@end
