//
//  XenRefreshableListController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "XenRefreshableListController.h"
#import "XenGeneralViewController.h"
#import "UIWaitForDataLoad.h"
#import "XenDescriptiveBase.h"
#import "RootViewController_ipad.h"
#import "CommonUIStaticHelper.h"
#import "MultiLineDetailUITableViewCell.h"

@interface XenRefreshableListController ()
- (NSArray *)filteredObjectList;
- (void) reloadTableData;
@end

@implementation XenRefreshableListController

@synthesize hypConnID, xenObjectList;

#pragma mark -
#pragma mark View lifecycle

- (id)initWithHypervisorConnection:(XenHypervisorConnection *)hypervisorConnection withCondition:(NSPredicate *) condition forHypObjectType:(NSInteger)hypObjectType withSearch:(BOOL)searchable{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        objectType = hypObjectType;
        [self setHypConnID:[hypervisorConnection connectionID]];
        if (searchable){
            // was 40
            UISearchBar *theSearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0,320,40)];
            theSearchBar.delegate = self;
            [self.view addSubview:theSearchBar];
            [[self tableView] setTableHeaderView:theSearchBar];
        }
       
        predicate = condition;
        [self setXenObjectList:[hypervisorConnection hypObjectsForType:hypObjectType withCondition:predicate]];
        searchPredicate = nil;
        [[self tableView] setDelegate:self];
        [[self tableView] setDataSource:self];
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [[self tableView] setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [[self tableView] registerNib:[UINib nibWithNibName:@"MultiLineDetailUITableViewCell" bundle:nil] forCellReuseIdentifier:@"MultiLineDetailUITableViewCell"];

    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh)
             forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
}

- (void) viewWillAppear:(BOOL)animated{
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    [xenHypervisorConnection addHypervisorConnectionDelegate:self];
    // refresh the data in the lists
    [self setXenObjectList:[xenHypervisorConnection hypObjectsForType:objectType withCondition:predicate]];
    [[self tableView] reloadData];
    [super viewWillAppear:animated];


}

- (void) viewWillDisappear:(BOOL)animated{
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    [xenHypervisorConnection removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}


- (void)configureCell:(MultiLineDetailUITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
   XenBase *xenObject = [[self filteredObjectList] objectAtIndex:indexPath.row];
   // call the abstract method to render the cell
   [self configureCell:cell forXenObject:xenObject];
    // do alternate striping on the rows
    [cell setAlternate:(indexPath.row % 2)];
}

- (NSString*) descriptionStringForCellAtIndexPath:(NSIndexPath *)indexPath{
    XenBase *xenObject = [[self filteredObjectList] objectAtIndex:indexPath.row];
    // call the abstract method to render the cell
    return [self descriptionStringForXenObject:xenObject];
}

#pragma mark -
#pragma mark View Search Bar

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    // only show the status bar’s cancel button while in edit mode
    searchBar.showsCancelButton = YES;
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    // flush the previous search content
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
    
    if([searchText isEqualToString:@""] || searchText == nil){
        [self reloadTableData];
        return;
    }
    
    searchPredicate = [XenDescriptiveBase nameBeginsOrContainsAWordBeginningWith:searchText];
    [self reloadTableData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
    [self reloadTableData];
    
    [searchBar resignFirstResponder];
    searchBar.text = @"";
}

// called when Search (in our case “Done”) button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

-(void) reloadTableData{
    [[self tableView] reloadData];
}

- (void)refresh {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    if (![xenHypervisorConnection isUpdatePendingForType:objectType]){
        [xenHypervisorConnection RequestHypObjectsForType:objectType];
    }
    // the call to stopLoading is on the data update received.
    [self.refreshControl endRefreshing];
}


#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self filteredObjectList] count];
}


- (NSArray *)filteredObjectList{
    // apply the earch predicate if one is set
    if (searchPredicate == nil)
    {
        return [self xenObjectList];
    }
    else
    {
        return [[self xenObjectList] filteredArrayUsingPredicate:searchPredicate];
    }
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"MultiLineDetailUITableViewCell";

    MultiLineDetailUITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MultiLineDetailUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
   [self configureCell:cell atIndexPath:indexPath];
   return cell;
}

// this needs to match 
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 68;
}

#pragma mark -
#pragma mark XenHypervisorConnection protocol implementation

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)hypObjectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if (hypObjectType | objectType){
        XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

        [self setXenObjectList:[xenHypervisorConnection hypObjectsForType:objectType withCondition:predicate]];
        [self reloadTableData];
        if (![xenHypervisorConnection isAutoUpdateEnabled]){
            [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
        }
    }
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    XenBase *xenBase = [[self filteredObjectList] objectAtIndex:indexPath.row];
    
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    // ensure that the host object data is fully populated here
    [xenHypervisorConnection setWalkTreeMode:YES];
    [xenHypervisorConnection PopulateHypObject:xenBase];
    
    UIWaitForDataLoad *dataLoader = [[UIWaitForDataLoad alloc] initWithHypervisorConnection:xenHypervisorConnection];
    [dataLoader setUIWaitForDataLoadDelegate:self];
    NSLog(@"Created dataloader to wait for data to be loaded for the next page");
    [dataLoader waitForObjectTreeForObject:xenBase];
}

#pragma mark -
#pragma mark UIWaitForDownload delegate

- (void)dataLoaderFinished:(UIWaitForDataLoad *)dataLoader withResult:(BOOL)sucsess{
    NSLog(@"dataloader reported that it has loaded the required data.");
    
    NSError *err = [dataLoader connectionError];
    XenBase *selectedObject = [dataLoader requestedObject];
    
    
    if (err){
        // if there was an error connecting do not do anyting.
        return;
    }
    
    [self didSelectObject:selectedObject];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}


- (void)dealloc {
    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];

    [xenHypervisorConnection removeHypervisorConnectionDelegate:self];
    predicate = nil;
}


/**
 * Displays the next view
 * For iPad this is in the detail view area, on the iPhone it is in the rootView list
 * This enables the same view to be used on both devices
 */
-(void) displayNextView:(UIViewController *)controller{
        
    [CommonUIStaticHelper displayNextView:controller usingNavigationController:[self navigationController] andRootViewController:[[[self navigationController]viewControllers]objectAtIndex:0]];
}
@end


