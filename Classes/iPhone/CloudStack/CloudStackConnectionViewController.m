//
//  EC2ConnectionViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackConnectionViewController.h"
#import "ImageBuilder.h"
#import "CommonUIStaticHelper.h"
#import "RootViewController_ipad.h"
#import "CloudStackBase.h"
#import "CloudStackZone.h"
#import "CloudStackInitialConnectionViewController.h"
#import "CloudStackVMListbyCatagory.h"
#import "CloudStackHostListbyCatagory.h"
#import "CloudStackTemplateListbyCatagory.h"
#import "CloudStackSecurityGroupListbyCatagory.h"
#import "CloudStackNetworkListbyCatagory.h"
#import "CloudStackVolumeListbyCatagory.h"
#import "TitledDetailViewController.h"
#import "CloudStackZoneGeneralViewController.h"
#import "ZoneTabBarController.h"

@interface CloudStackConnectionViewController (){
     ZoneTabBarController* tabBarController;
}
@end

@implementation CloudStackConnectionViewController

@synthesize displayCatagories, hypConnID;

- (id) initWithHypervisorConnection:(HypervisorConnectionFactory *) connection{
    self = [super init];
    if (self){
        hypConnID = [connection connectionID];
        hypervisorType = [connection hypervisorConnectionType];
    }
    return self;
}

- (CloudStackNativeHypervisorConnection*) hypervisorConnection{
    return (CloudStackNativeHypervisorConnection*)[HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

#pragma mark -
#pragma mark View lifecycle

- (void) viewWillAppear:(BOOL)animated{
    // request data for any object sets that are marked as out of date
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
    
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    
}

- (void) viewWillDisappear:(BOOL)animated{
    [[self hypervisorConnection]removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

/*
 * When load set the title and the name of the pool
 * This should only be done to make sure that the connection is populated with all
 * the top level objects the first time that the page is displayed after connection
 * that is why this is in the on load, otherwise it would happen every time the 
 * page appears.
 */
- (void)viewDidLoad {
    // request data for any object sets that are marked as out of date
    CloudStackNativeHypervisorConnection* hypervisorConnection = [self hypervisorConnection];
    
    [self setDisplayCatagories:[NSArray arrayWithObjects:
                                [NSNumber numberWithInt:HYPOBJ_CS_VM_CORE],
                                [NSNumber numberWithInt:HYPOBJ_CS_HOST_CORE],
                                [NSNumber numberWithInt:HYPOBJ_CS_TEMPLATE],
                                [NSNumber numberWithInt:HYPOBJ_CS_NETWORK],
                                [NSNumber numberWithInt:HYPOBJ_CS_VOLUME],
                                [NSNumber numberWithInt:HYPOBJ_CS_SECURITYGROUP],nil]];    
    
    [hypervisorConnection setWalkTreeMode:NO];
    [hypervisorConnection reloadCoreData];
    
    [super viewDidLoad];
    if (self.title == nil){
        // make this the zone title here
        CloudStackZone* theZone = (CloudStackZone*)[hypervisorConnection hypObjectsForType:HYPOBJ_CS_ZONE withCondition:[CloudStackZone zoneWithReference:[hypervisorConnection zoneId]]];
        self.title = [theZone name];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    UIDevice *device = [UIDevice currentDevice];
    if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        UINavigationController *navCon = [self navigationController];
        UIViewController *rootController = [[navCon viewControllers] objectAtIndex:0];
        
        // use detail view for ipad
        if ([rootController respondsToSelector:@selector(detailViewNavigationController)]){
            UINavigationController *dvController = [rootController performSelector:@selector(detailViewNavigationController)];
            if (dvController){
//                if (!inRotation){
                    [RootViewController_ipad setDetailViewRootController:[self detailContentView] onNavigationController:dvController];
//                }
            }
        }
    }
    
    [[self tableView] reloadData];
}

// TODO - implement this here and in other pages too.
//  it is not proving to be an issue at present so dont wory too much.
// commented out here as have not had time to write alert dialog
// or test this, or implement in the other pages.
- (void) hypervisorEventDataLost{
    // might want to do something like this to cope with event data being lost.
    // could do something like this on all screens 
    // could flush cache, then just do a rewalk of the tree for the object being displayed
    // and hey presto.
    //    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    //[xenHypervisorConnection flushHypObjectCache];
    //[self reloadDataForConnection:xenHypervisorConnection];
    //[[self tableView] reloadData];
}

// render the text in the cell from the address of the server to be connected to.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    float frameWidth = [[self view] frame].size.width;
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    NSNumber *catagory = [[self displayCatagories] objectAtIndex:indexPath.row];
    NSString *displayText;
    NSString *detailTextHeader; 
    UIImage *image = nil;
    BOOL isLoading = NO;
    BOOL validCatagory = YES;NSUInteger hypObjType = [catagory intValue];NSUInteger objectCount = 0;
    
    switch (hypObjType) {
        case HYPOBJ_CS_VM_CORE:
            displayText = @"VMs";
            image = [ImageBuilder vMImage];
            detailTextHeader = @"Instance Count:%d";
            break;
        case HYPOBJ_CS_HOST_CORE:
            displayText = @"Hosts";
            image = [ImageBuilder hostImage];
            detailTextHeader = @"Instance Count:%d";
            break;
        case HYPOBJ_CS_TEMPLATE:
            displayText = @"Templates";
            image = [ImageBuilder vMTemplateImage];
            detailTextHeader = @"Instance Count:%d";
            break;
        case HYPOBJ_CS_NETWORK:
            displayText = @"Networks";
            image = [ImageBuilder networkImage];
            detailTextHeader = @"Instance Count:%d";
            break;
        case HYPOBJ_CS_SECURITYGROUP:
            displayText = @"Security Groups";
            image = [ImageBuilder securityImage];
            detailTextHeader = @"Instance Count:%d";
            break;
        case HYPOBJ_CS_VOLUME:
            displayText = @"Volumes";
            image = [ImageBuilder storageImage];
            detailTextHeader = @"Instance Count:%d";
            break;
        default:
            validCatagory = NO;
            break;
    }
    
    if (validCatagory){
        // get the content view
        UIView *content = [cell contentView];
        // set the tag to be the type of object being displayed in the cell
        content.tag = hypObjType;
        
        // remove any existing button or spinner from the view
        [[content viewWithTag:100] removeFromSuperview];
        
        NSString *detailText;
        isLoading =  [[self hypervisorConnection] isUpdatePendingForType:hypObjType];

        if (!isLoading){
            objectCount = [[[self hypervisorConnection] hypObjectsForType:hypObjType] count];
            detailText = [NSString stringWithFormat:detailTextHeader,objectCount];
            // add the navigation indicator to the cell
            if (objectCount > 0){
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
            [cell.imageView setAlpha:1.0];
        }
        else {
            UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            spinner.tag = 100;
            CGRect rect = CGRectMake(frameWidth - 30, 10.0, 20.0, 20.0);
            [spinner setFrame:rect];
            [content addSubview:spinner];
            [spinner startAnimating];
            // remove the navigation indicator to the cell
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell.imageView setAlpha:0.5];
            detailText = @"Loading....";
        }
        
        // set the default selection style
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        
        // put the refresh button in place
        if (!isLoading){
            // construct the refresh button
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button addTarget:self action:@selector(refreshButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            CGRect rect = CGRectMake(frameWidth - 70, 5, 40.0, 40.0);
            [button setFrame:rect];
            [button setBackgroundImage:[UIImage imageNamed:@"Refresh"] forState:UIControlStateNormal];
            button.tag = 100;
            [content addSubview:button];
        }
        // set the image for the cell
        [[cell imageView] setImage:image];  
        [[cell textLabel] setText:displayText];
        
        // set the detail text
        if (detailText != nil) {
            [[cell detailTextLabel] setText:detailText];
        }
    }        
    else {
        NSLog(@"Invalid cell catagory provided %@",catagory);
    }
}

-(void)refreshButtonClick:(id)sender{
    // get the button tag
    NSUInteger hypObjectType = [[sender superview] tag];
    [[self hypervisorConnection] RequestHypObjectsForType:hypObjectType];
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark HypervisorConnection protocol implementation
- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.displayCatagories count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // if the cell has no navigation indicator do not allow navigation as the data is still loading.
    if ([[tableView cellForRowAtIndexPath:indexPath] accessoryType] == UITableViewCellAccessoryNone){
        return;
    }
        
    NSNumber *cloudStackCatagory = [self.displayCatagories objectAtIndex:indexPath.row];NSUInteger hypObjType = [cloudStackCatagory intValue];
    switch (hypObjType) {
        case HYPOBJ_CS_VM_CORE:{
            CloudStackVMListbyCatagory *vmListViewController = [[CloudStackVMListbyCatagory alloc] initWithHypervisorConnection:[self hypervisorConnection]];
                [self.navigationController pushViewController:vmListViewController animated:YES];
                break;
            }
        case HYPOBJ_CS_HOST_CORE:{
            CloudStackHostListbyCatagory *hostListViewController = [[CloudStackHostListbyCatagory alloc] initWithHypervisorConnection:[self hypervisorConnection]];
            [self.navigationController pushViewController:hostListViewController animated:YES];
            break;
        }
        case HYPOBJ_CS_TEMPLATE:{
            CloudStackTemplateListbyCatagory *templateListViewController = [[CloudStackTemplateListbyCatagory alloc] initWithHypervisorConnection:[self hypervisorConnection]];
            [self.navigationController pushViewController:templateListViewController animated:YES];
            break;
        }
        case HYPOBJ_CS_SECURITYGROUP:{
            CloudStackSecurityGroupListbyCatagory *securityGroupListViewController = [[CloudStackSecurityGroupListbyCatagory alloc] initWithHypervisorConnection:[self hypervisorConnection]];
            [self.navigationController pushViewController:securityGroupListViewController animated:YES];
            break;
        }
        case HYPOBJ_CS_NETWORK:{
            CloudStackNetworkListbyCatagory *networkListViewController = [[CloudStackNetworkListbyCatagory alloc] initWithHypervisorConnection:[self hypervisorConnection]];
            [self.navigationController pushViewController:networkListViewController animated:YES];
            break;
        }
        case HYPOBJ_CS_VOLUME:{
            CloudStackVolumeListbyCatagory *volumeListViewController = [[CloudStackVolumeListbyCatagory alloc] initWithHypervisorConnection:[self hypervisorConnection]];
            [self.navigationController pushViewController:volumeListViewController animated:YES];
            break;
        }
        default:
            break;
    }
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (UIViewController *) detailContentView{
    NSString* zoneId = [[self hypervisorConnection] zoneId];
    NSPredicate *objectPred = [CloudStackZone  zoneWithReference:zoneId];
    CloudStackZone *zoneForDisplay =  [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_ZONE withCondition:objectPred] objectAtIndex:0]; 
    
    // add the general tab
    if (!tabBarController){
        tabBarController = [[ZoneTabBarController alloc] init];
        NSMutableArray *localControllersArray = [[NSMutableArray alloc] initWithCapacity:2];


        // the metrics view
        CloudStackInitialConnectionViewController* tbc2 = [[CloudStackInitialConnectionViewController alloc] initWithNibName:@"CloudStackInitialConnectionView" bundle:nil hypConnectionType:HYPERVISOR_CLOUDSTACK_NATIVE andConnectionId:[self hypConnID]];
        
        TitledDetailViewController* zoneMericsTitledView = [[TitledDetailViewController alloc] initWithHypervisorConnection:[self hypervisorConnection] 
                                                                                                              rootObjectRef:[zoneForDisplay uniqueReference]
                                                                                                        rootObjectPredicate:objectPred
                                                                                                              hypObjectType:HYPOBJ_CS_ZONE
                                                                                                           namePropertyName:@"name"
                                                                                                    descriptionPropertyName:@"description"
                                                                                                                displayMode:DISPLAYMODE_NAMEDESCRIPTION
                                                                                                                    tabName:@"Metrics" 
                                                                                                            tabBarImageName:@"graph" 
                                                                                                                      image:[ImageBuilder zoneImage]
                                                                                                    extentionViewController:tbc2];    
        [tbc2 setZoneReference:[zoneForDisplay uniqueReference]];
        [localControllersArray addObject:zoneMericsTitledView];

        // the genral view
        CloudStackZoneGeneralViewController* tbc = [[CloudStackZoneGeneralViewController alloc] initWithhypervisorConnection: [self hypervisorConnection] withZone:zoneForDisplay];
        
        TitledDetailViewController* zoneGeneralTitledView = [[TitledDetailViewController alloc] initWithHypervisorConnection:[self hypervisorConnection] 
                                                                                                               rootObjectRef:[zoneForDisplay uniqueReference]
                                                                                                         rootObjectPredicate:objectPred
                                                                                                               hypObjectType:HYPOBJ_CS_ZONE
                                                                                                            namePropertyName:@"name"
                                                                                                     descriptionPropertyName:nil
                                                                                                                 displayMode:DISPLAYMODE_NAME
                                                                                                                     tabName:@"General" 
                                                                                                             tabBarImageName:@"tab_General" 
                                                                                                                       image:[ImageBuilder zoneImage]
                                                                                                     extentionViewController:tbc];    
        [localControllersArray addObject:zoneGeneralTitledView];

        
        

        
        
        tabBarController.viewControllers = localControllersArray;
        
        [tabBarController setTitle:@"Zone"];
        
    }
    else{
        for (TitledDetailViewController* view in [tabBarController viewControllers]){
            // this should be the image general and it should refresh the children ones.
            NSPredicate *objectPred = [CloudStackZone zoneWithReference:[zoneForDisplay uniqueReference]];
            [view setRootObjectPredicate:objectPred andReference:[zoneForDisplay uniqueReference]];    
        }
    }
    
    return tabBarController;   
}

@end

