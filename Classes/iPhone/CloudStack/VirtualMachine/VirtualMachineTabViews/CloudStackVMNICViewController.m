//
//  CloudStackVMNICViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ScrollableDetailTextCell.h"
#import "CloudStackNativeHypervisorConnection.h"
#import "ImageBuilder.h"
#import "CloudStackVMCore.h"
#import "CloudStackVMDetail.h"
#import "CloudStackNIC.h"
#import "CloudStackNetwork.h"
#import "CloudStackVMNICViewController.h"

@interface CloudStackVMNICViewController ()
- (void)configureGeneralDetailsCell:(UITableViewCell *)cell forSection:(NSInteger)section atIndex:(NSInteger) index ;
- (CloudStackVMDetail *) csVm;
-(CloudStackVMDetail*) csVMDetail;
-(CloudStackNIC*) csNicAtIndex:(NSInteger)index;

@end

@implementation CloudStackVMNICViewController

@synthesize hypConnID;

-(HypervisorConnectionFactory *) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (CloudStackVMCore *) csVm{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'", [self vmId]]]; 
    NSArray *images = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_VM_CORE withCondition:pred];
    if ([images count] >0){
        return [images objectAtIndex:0];
    }
    return nil;
}

-(CloudStackVMDetail*) csVMDetail{
    CloudStackVMDetail* detail = nil;
    NSString* detailRef = [[[self csVm] referencesForType:HYPOBJ_CS_VM] objectAtIndex:0];
    NSArray* details = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_VM withCondition:[CloudStackVMDetail vmWithReference:detailRef]];
    if (details != nil && [details count] >0){
       detail = [details objectAtIndex:0];
    }
    return detail;
}

-(CloudStackNIC*) csNicAtIndex:(NSInteger)index{
    CloudStackNIC* theNic = nil;
    CloudStackVMDetail* detail = [self csVMDetail];
    NSArray* nicRefs = [detail referencesForType:HYPOBJ_CS_NIC];
    if (nicRefs && [nicRefs count] >0){
        NSArray* nics = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_NIC withCondition:[CloudStackNIC nicWithReference:[nicRefs objectAtIndex:0]]];     
        if (nics && [nics count]>0){
            theNic = [nics objectAtIndex:0];
        }
    }
    
    return theNic;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withVm:(CloudStackVMCore *)thisVm{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setVmId: [thisVm uniqueReference]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
    }
    return self;
    
}

-(void)setVmId:(NSString *)vmId{
    _vmId = vmId;
    [[self tableView] reloadData];
}

-(NSString *)vmId{
    return _vmId; 
}

- (void) setId:(NSString*)newID{
    [self setVmId:newID];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark for RefreshablePage protocol
-(void) refreshPage{
    [[self tableView] reloadData];
}

- (UIImage*) currentTitleImage{
#warning should this have an image
    return nil;
}

#pragma mark -
#pragma mark HypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to vm objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    // could cut this down to just the relevent ones if required
    if (objectType == HYPOBJ_CS_NIC){
        [[self tableView ]reloadData];
    }
    if (objectType == HYPOBJ_CS_VM){
        [[self tableView ]reloadData];
    }
    if (objectType == HYPOBJ_CS_VM_CORE){
        [[self tableView ]reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    CloudStackVMDetail* detail = [self csVMDetail];
    NSArray* nicRefs = [detail referencesForType:HYPOBJ_CS_NIC];
    if (nicRefs && [nicRefs count]>0){
        return [nicRefs count];
    }

    // this must always be >0 or the controll goes boom (this should never be reached in a real situation so should be ok)
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // all sections are expandable
    return YES;
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    CloudStackNIC* nic = [self csNicAtIndex:section];
    return [self convertToDisplayString:[nic macaddress]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    return CSVMNIC_GENERAL_TABLE_COUNT;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    [self configureGeneralDetailsCell:cell forSection:[indexPath section] atIndex:[indexPath row]];
    return cell;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureGeneralDetailsCell:(UITableViewCell *)cell forSection:(NSInteger)section atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    // this will be the nic number in the lists macaddress?
   CloudStackNIC* nic = [self csNicAtIndex:section];
    
    switch (index) {
        case CSVMNIC_GENERAL_TABLE_IPADDRESS:
        {
           [[cell textLabel] setText:@"IP Address"];
          [[cell detailTextLabel] setText: [self convertToDisplayString:[nic ipaddress]]];
            break;
        }
        case CSVMNIC_GENERAL_TABLE_NETMASK:
        {
            [[cell textLabel] setText:@"Network Mask"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[nic netmask]]];
            break;
        }
        case CSVMNIC_GENERAL_TABLE_NETWORK:
        {
            // get the netowrk here
            NSString* networkName = @"";
            NSArray* netRefs = [nic referencesForType:HYPOBJ_CS_NETWORK];
            if (netRefs && [netRefs count]>0){
               NSArray* networks = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_NETWORK withCondition:[CloudStackNetwork networkWithReference:[netRefs objectAtIndex:0]]];
                if (networks && [networks count]>0){
                    CloudStackNetwork* network = [networks objectAtIndex:0];
                    networkName = [network name];
                }

            }                        
            [[cell textLabel] setText:@"Network"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:networkName]];
            break;
        }
        case CSVMNIC_GENERAL_TABLE_TRAFFICTYPE:
        {
            [[cell textLabel] setText:@"Traffic type"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[nic traffictype]]];
            break;
        }
        case CSVMNIC_GENERAL_TABLE_TYPE:
        {
            [[cell textLabel] setText:@"Type"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[nic type]]];
            break;
        }
        case CSVMNIC_GENERAL_TABLE_GATEWAY:
        {
            [[cell textLabel] setText:@"Gateway"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[nic gateway]]];
            break;
        }
        case CSVMNIC_GENERAL_TABLE_BROADCASTURI:
        {
            [[cell textLabel] setText:@"Broadcast URI"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[nic broadcastuir]]];
            break;
        }
        case CSVMNIC_GENERAL_TABLE_ISOLATIONURI:
        {
            [[cell textLabel] setText:@"Isolation URI"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[nic isolationuri]]];
            break;
        }
        default:
            break;
    }    
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


@end
