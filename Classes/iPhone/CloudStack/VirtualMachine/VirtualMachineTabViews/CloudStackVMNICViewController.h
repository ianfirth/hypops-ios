//
//  CloudStackVMNICViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#import "ExpandableTableViewController.h"
#import "CloudStackNativeHypervisorConnection.h"
#import "CloudStackVMCore.h"
#import "Identifyable.h"
#import "RefreshablePage.h"

#define CSVMNIC_GENERAL_TABLE_IPADDRESS 0
#define CSVMNIC_GENERAL_TABLE_NETMASK 1
#define CSVMNIC_GENERAL_TABLE_NETWORK 2
#define CSVMNIC_GENERAL_TABLE_TRAFFICTYPE 3
#define CSVMNIC_GENERAL_TABLE_TYPE 4
#define CSVMNIC_GENERAL_TABLE_GATEWAY 5
#define CSVMNIC_GENERAL_TABLE_BROADCASTURI 6
#define CSVMNIC_GENERAL_TABLE_ISOLATIONURI 7
#define CSVMNIC_GENERAL_TABLE_COUNT 8

@interface CloudStackVMNICViewController : ExpandableTableViewController
            <HypervisorConnectionDelegate,Identifyable,RefreshablePage>{
    HypervisorType hypervisorType;
    NSString* _vmId;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withVm:(CloudStackVMCore *)thisVm;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

// this is used by relection in the tab contol t o update the page.  In this class it updates the vmID.
- (void) setId:(NSString*)newID;

@property (copy) NSString *hypConnID;
@property (copy) NSString *vmId;

@end
