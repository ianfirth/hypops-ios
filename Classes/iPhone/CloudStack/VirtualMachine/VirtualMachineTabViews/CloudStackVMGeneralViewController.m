//
//  CloudStackVMGeneralViewController.m
//  hypOps

// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackVMGeneralViewController.h"
#import "ScrollableDetailTextCell.h"
#import "CloudStackNativeHypervisorConnection.h"
#import "ImageBuilder.h"
#import "CloudStackVMCore.h"
#import "CloudStackOsType.h"
#import "CloudStackVMDetail.h"


@interface CloudStackVMGeneralViewController ()
- (void) configureGeneralDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
- (void) configureCpuDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
- (void) configureMemoryDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
- (void) configureNetworkDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
- (CloudStackVMCore *) csVm;
-(CloudStackVMDetail*) csVMDetail;

@end

@implementation CloudStackVMGeneralViewController

@synthesize hypConnID;

-(HypervisorConnectionFactory *) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (CloudStackVMCore *) csVm{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'", [self vmId]]]; 
    NSArray *images = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_VM_CORE withCondition:pred];
    if ([images count] >0){
        return [images objectAtIndex:0];
    }
    return nil;
}

-(CloudStackVMDetail*) csVMDetail{
    CloudStackVMDetail* detail = nil;
    NSString* detailRef = [[[self csVm] referencesForType:HYPOBJ_CS_VM] objectAtIndex:0];
    NSArray* details = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_VM withCondition:[CloudStackVMDetail vmWithReference:detailRef]];
    if (details != nil && [details count] >0){
        detail = [details objectAtIndex:0];
    }
    return detail;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withVm:(CloudStackVMCore *)thisVm{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setVmId: [thisVm uniqueReference]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
    }
    return self;
    
}

-(void)setVmId:(NSString *)vmId{
    _vmId = vmId;
    [[self tableView] reloadData];
}

-(NSString *)vmId{
    return _vmId; 
}

- (void) setId:(NSString*)newID{
    [self setVmId:newID];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark for RefreshablePage protocol
-(void) refreshPage{
    [[self tableView] reloadData];
}

- (UIImage*) currentTitleImage{
#warning should this have an image
    return nil;
}

#pragma mark -
#pragma mark HypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to vm objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    // could cut this down to just the relevent ones if required
    if (objectType == HYPOBJ_CS_VM_CORE){
        [[self tableView ]reloadData];
    }
    if (objectType == HYPOBJ_CS_VM){
        [[self tableView ]reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return CSVM_TABLE_COUNT;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // first section is not expandable
    return (section != 0);
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case CSVM_GENERAL_TABLE:
            return nil;
            break;
        case CSVM_CPU_TABLE:
            return @"CPU";
            break;
        case CSVM_MEMORY_TABLE:
            return @"Memory";
            break;
        case CSVM_NETWORK_TABLE:
            return @"Networking";
            break;
        default:
            return nil;
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    // Configure the cell...
    // configuration details
    switch (section) {
        case CSVM_GENERAL_TABLE:
            return CSVM_GENERAL_TABLE_COUNT;
            break;
        case CSVM_CPU_TABLE:
            return CSVM_CPU_TABLE_COUNT;
            break;
        case CSVM_MEMORY_TABLE:
            return CSVM_MEMORY_TABLE_COUNT;
            break;
        case CSVM_NETWORK_TABLE:
            return CSVM_NETWORK_TABLE_COUNT;
            break;
        default:
            break;
    }
    // should never get here
    return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    switch ([indexPath section]) {
        case CSVM_GENERAL_TABLE:
            [self configureGeneralDetailsCell:cell atIndex:[indexPath row]];
            break;
        case CSVM_CPU_TABLE:
            [self configureCpuDetailsCell:cell atIndex:[indexPath row]];
            break;
        case CSVM_MEMORY_TABLE:
            [self configureMemoryDetailsCell:cell atIndex:[indexPath row]];
            break;
        case CSVM_NETWORK_TABLE:
            [self configureNetworkDetailsCell:cell atIndex:[indexPath row]];
            break;
        default:
            break;
    }
    
    return cell;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureGeneralDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackVMCore *vm = [self csVm];    
    
    switch (index) {
        case CSVM_GENERAL_TABLE_DOMAIN:
        {
            [[cell textLabel] setText:@"Domain"];
            NSString *value = [self convertToDisplayString:[vm domain]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSVM_GENERAL_TABLE_HYPERVISOR:
        {
            [[cell textLabel] setText:@"Hypervisor"];
            NSString *value = [self convertToDisplayString:[vm hypervisor]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSVM_GENERAL_TABLE_HOSTNAME:
        {
            [[cell textLabel] setText:@"Host name"];
            NSString *value = [self convertToDisplayString:[vm hostname]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSVM_GENERAL_TABLE_ACCOUNT:
        {
            [[cell textLabel] setText:@"Account"];
            NSString *value = [self convertToDisplayString:[vm account]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        default:
            break;
    }    
}


- (void)configureCpuDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackVMDetail *vm = [self csVMDetail];
    
    switch (index) {
        case CSVM_CPU_TABLE_CPUNUMBER:
        {
            [[cell textLabel] setText:@"CPU Count"];
            NSString *value = [self convertToDisplayString:[vm cpunumber]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSVM_CPU_TABLE_CPUSPEED:
        {
            [[cell textLabel] setText:@"CPU Speed"];
            NSString *value = [self convertToDisplayString:[vm cpuspeed]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSVM_CPU_TABLE_CPUUSED:
        {
            [[cell textLabel] setText:@"CPU Used"];
            NSString *value = [self convertToDisplayString:[vm cpuused]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        default:
            break;
    }    
}

- (void)configureMemoryDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackVMDetail *vm = [self csVMDetail];    
    switch (index) {
        case CSVM_MEMORY_TABLE_MEMORY:
        {
            [[cell textLabel] setText:@"Memory"];
            NSString *value = [self convertToDisplayString:[vm memory]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        default:
            break;
    }    
}

- (void)configureNetworkDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackVMDetail *vm = [self csVMDetail];
    
    switch (index) {
        case CSVM_NETWORK_TABLE_VIRTUALNETWORK:
        {
            [[cell textLabel] setText:@"Virtual Network"];
            NSString *value = [self convertToDisplayString:[vm forvirtualnetwork]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSVM_NETWORK_TABLE_KBSREAD:
        {
            [[cell textLabel] setText:@"Read rate (Kbs)"];
            NSString *value = [self convertToDisplayString:[vm networkkbsread]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSVM_NETWORK_TABLE_KBSWRITE:
        {
            [[cell textLabel] setText:@"Write rate (Kbs)"];
            NSString *value = [self convertToDisplayString:[vm networkkbswrite]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        default:
            break;
    }    
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}



@end
