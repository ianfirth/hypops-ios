//
//  CloudStackVMGeneralViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ExpandableTableViewController.h"
#import "CloudStackNativeHypervisorConnection.h"
#import "CloudStackVMCore.h"
#import "Identifyable.h"
#import "RefreshablePage.h"

#define CSVM_GENERAL_TABLE 0
#define CSVM_CPU_TABLE 1
#define CSVM_MEMORY_TABLE 2
#define CSVM_NETWORK_TABLE 3
#define CSVM_TABLE_COUNT 4

#define CSVM_GENERAL_TABLE_DOMAIN 0
#define CSVM_GENERAL_TABLE_HYPERVISOR 1
#define CSVM_GENERAL_TABLE_HOSTNAME 2
#define CSVM_GENERAL_TABLE_ACCOUNT 3
#define CSVM_GENERAL_TABLE_COUNT 4

#define CSVM_CPU_TABLE_CPUNUMBER 0
#define CSVM_CPU_TABLE_CPUSPEED 1
#define CSVM_CPU_TABLE_CPUUSED 2
#define CSVM_CPU_TABLE_COUNT 3

#define CSVM_MEMORY_TABLE_MEMORY 0
#define CSVM_MEMORY_TABLE_COUNT 1

#define CSVM_NETWORK_TABLE_VIRTUALNETWORK 0
#define CSVM_NETWORK_TABLE_KBSREAD 1
#define CSVM_NETWORK_TABLE_KBSWRITE 2
#define CSVM_NETWORK_TABLE_COUNT 3


@interface CloudStackVMGeneralViewController : ExpandableTableViewController
        <HypervisorConnectionDelegate,Identifyable,RefreshablePage>{
    HypervisorType hypervisorType;
    NSString* _vmId;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withVm:(CloudStackVMCore *)thisVm;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

// this is used by relection in the tab contol t o update the page.  In this class it updates the vmID.
- (void) setId:(NSString*)newID;

@property (copy) NSString *hypConnID;
@property (copy) NSString *vmId;

@end
