//
//  CloudStackVMListbyCatagory.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackNativeHypervisorConnection.h"
#import "PullRefreshTableViewController.h"

#define CS_VM_ALL 0
#define CS_VM_BYSTATE 1
#define CS_VM_BYHYPERVISOR 2
#define CS_VM_BYHOST 3
#define CS_VM_BYDOMAIN 4
#define CS_VM_BYSERVICEOFFERING 5
#define CS_VM_OWNEDBYME 6

@interface CloudStackVMListbyCatagory : PullRefreshTableViewController <HypervisorConnectionDelegate> {
    HypervisorType hypervisorType;
}

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess;

@property (copy) NSString *hypConnID;
@property (nonatomic, strong) NSDictionary *csVmCatagories;

@end
