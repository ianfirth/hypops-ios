//
//  CloudStackTemplateGeneralViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ExpandableTableViewController.h"
#import "CloudStackNativeHypervisorConnection.h"
#import "CloudStackTemplate.h"
#import "Identifyable.h"
#import "RefreshablePage.h"

#define CSVM_GENERAL_TABLE 0
#define CSVM_CPU_TABLE 1
#define CSVM_MEMORY_TABLE 2
#define CSVM_HOST_TABLE 3
#define CSVM_TABLE_COUNT 3

#define CSVM_GENERAL_TABLE_NAME 0
#define CSVM_GENERAL_TABLE_COUNT 1

#define CSVM_CPU_TABLE_CPUCOUNT 0
#define CSVM_CPU_TABLE_COUNT 1

#define CSVM_MEMORY_TABLE_MEMALLOCATED 0
#define CSVM_MEMORY_TABLE_COUNT 1

#define CSVM_HOST_TABLE_HOSTNAME 0
#define CSVM_HOST_TABLE_COUNT 1


@interface CloudStackTemplateGeneralViewController : ExpandableTableViewController <HypervisorConnectionDelegate,Identifyable,RefreshablePage>{
    HypervisorType hypervisorType;
    NSString* _templateId;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withTemplate:(CloudStackTemplate *)thisTemplate;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

// this is used by relection in the tab contol t o update the page.  In this class it updates the templateID.
- (void) setId:(NSString*)newID;

@property (copy) NSString *hypConnID;
@property (copy) NSString *templateId;

@end
