//
//  CloudStackTemplateGeneralViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackTemplateGeneralViewController.h"
#import "ScrollableDetailTextCell.h"
#import "CloudStackNativeHypervisorConnection.h"
#import "ImageBuilder.h"
#import "CloudStackTemplate.h"

@interface CloudStackTemplateGeneralViewController ()
- (void) configureGeneralDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
- (void) configureCpuDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
- (void) configureMemoryDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
- (void) configureHostDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;

@end

@implementation CloudStackTemplateGeneralViewController

@synthesize hypConnID;

-(HypervisorConnectionFactory *) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (CloudStackTemplate *) csTemplate{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'", [self templateId]]]; 
    NSArray *images = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_TEMPLATE withCondition:pred];
    if ([images count] >0){
        return [images objectAtIndex:0];
    }
    return nil;
}


- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withTemplate:(CloudStackTemplate *)thisTemplate{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setTemplateId: [thisTemplate uniqueReference]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
    }
    return self;
    
}

-(void)setTemplateId:(NSString *)templateId{
    _templateId = templateId;
    [[self tableView] reloadData];
}

-(NSString *)templateId{
    return _templateId; 
}

- (void) setId:(NSString*)newID{
    [self setTemplateId:newID];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark for RefreshablePage protocol
-(void) refreshPage{
    [[self tableView] reloadData];
}

- (UIImage*) currentTitleImage{
#warning should this have an image
    return nil;
}

#pragma mark -
#pragma mark HypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to vm objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    // could cut this down to just the relevent ones if required
    if (objectType == HYPOBJ_CS_TEMPLATE){
        [[self tableView ]reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return CSVM_TABLE_COUNT;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // first section is not expandable
    return (section != 0);
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case CSVM_GENERAL_TABLE:
            return nil;
            break;
        case CSVM_CPU_TABLE:
            return @"CPU";
            break;
        case CSVM_MEMORY_TABLE:
            return @"Memory";
            break;
        case CSVM_HOST_TABLE:
            return @"Host";
            break;
        default:
            return nil;
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    // Configure the cell...
    // configuration details
    switch (section) {
        case CSVM_GENERAL_TABLE:
            return CSVM_GENERAL_TABLE_COUNT;
            break;
        case CSVM_CPU_TABLE:
            return CSVM_CPU_TABLE_COUNT;
            break;
        case CSVM_MEMORY_TABLE:
            return CSVM_MEMORY_TABLE_COUNT;
            break;
        case CSVM_HOST_TABLE:{
            return CSVM_HOST_TABLE_COUNT;
            break;
        }
        default:
            break;
    }
    // should never get here
    return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    switch ([indexPath section]) {
        case CSVM_GENERAL_TABLE:
            [self configureGeneralDetailsCell:cell atIndex:[indexPath row]];
            break;
        case CSVM_CPU_TABLE:
            [self configureCpuDetailsCell:cell atIndex:[indexPath row]];
            break;
        case CSVM_MEMORY_TABLE:
            [self configureMemoryDetailsCell:cell atIndex:[indexPath row]];
            break;
        case CSVM_HOST_TABLE:
            [self configureHostDetailsCell:cell atIndex:[indexPath row]];
            break;
        default:
            break;
    }
    
    return cell;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureGeneralDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackTemplate *template = [self csTemplate];    
    
    switch (index) {
        case CSVM_GENERAL_TABLE_NAME:
        {
            [[cell textLabel] setText:@"Name"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[template name]]];
            break;
        }
        default:
            break;
    }    
}

- (void)configureCpuDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackTemplate *template = [self csTemplate]; 
    
    switch (index) {
        case CSVM_CPU_TABLE_CPUCOUNT:
        {
            [[cell textLabel] setText:@"CPU Count"];
            NSString *value = [self convertToDisplayString:[template name]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        default:
            break;
    }    
}

- (void)configureMemoryDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackTemplate *template = [self csTemplate]; 
    
    switch (index) {
        case CSVM_MEMORY_TABLE_MEMALLOCATED:
        {
            [[cell textLabel] setText:@"Memory Allocated"];
            NSString *value = [self convertToDisplayString:[template name]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        default:
            break;
    }    
}

- (void)configureHostDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackTemplate *template = [self csTemplate]; 
    
    switch (index) {
        case CSVM_HOST_TABLE_HOSTNAME:
        {
            [[cell textLabel] setText:@"Host Name"];
            NSString *value = [self convertToDisplayString:[template name]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        default:
            break;
    }    
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}



@end
