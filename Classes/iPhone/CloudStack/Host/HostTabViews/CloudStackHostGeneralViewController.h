//
//  CloudStackHostGeneralViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#import "ExpandableTableViewController.h"
#import "CloudStackNativeHypervisorConnection.h"
#import "CloudStackHostCore.h"
#import "Identifyable.h"
#import "RefreshablePage.h"

#define CSHOST_GENERAL_TABLE 0
#define CSHOST_CAPABILITY_TABLE 1
#define CSHOST_TABLE_COUNT 2

#define CSHOST_GENERAL_TABLE_VERSION 0
#define CSHOST_GENERAL_TABLE_TYPE 1
#define CSHOST_GENERAL_TABLE_CPUNUMBER 2
#define CSHOST_GENERAL_TABLE_CPUSPEED 3
#define CSHOST_GENERAL_TABLE_IPADDRESS 4
#define CSHOST_GENERAL_TABLE_COUNT 5


@interface CloudStackHostGeneralViewController : ExpandableTableViewController <HypervisorConnectionDelegate, Identifyable,RefreshablePage>{
    HypervisorType hypervisorType;
    NSString* _hostId;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withHost:(CloudStackHostCore *)thisHost;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

// this is used by relection in the tab contol t o update the page.  In this class it updates the hostID.
- (void) setId:(NSString*)newID;

@property (copy) NSString *hypConnID;
@property (copy) NSString *hostId;

@end
