//
//  CloudStackHostGeneralViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackHostGeneralViewController.h"
#import "ScrollableDetailTextCell.h"
#import "CloudStackNativeHypervisorConnection.h"
#import "ImageBuilder.h"
#import "CloudStackHostCore.h"

@interface CloudStackHostGeneralViewController ()
- (void) configureGeneralDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
- (void) configureCapabilityDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;

@end

@implementation CloudStackHostGeneralViewController

@synthesize hypConnID;

-(HypervisorConnectionFactory *) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (CloudStackHostCore *) csHost{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'", [self hostId]]]; 
    NSArray *images = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_HOST_CORE withCondition:pred];
    if ([images count] >0){
        return [images objectAtIndex:0];
    }
    return nil;
}


- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withHost:(CloudStackHostCore *)thisHost{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setHostId: [thisHost uniqueReference]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
    }
    return self;
    
}

-(void)setHostId:(NSString *)hostId{
    _hostId = hostId;
    [[self tableView] reloadData];
}

-(NSString *)hostId{
    return _hostId; 
}

- (void) setId:(NSString*)newID{
    [self setHostId:newID];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark -
#pragma mark HypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to vm objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    // could cut this down to just the relevent ones if required
    if (objectType == HYPOBJ_CS_HOST_CORE){
        [[self tableView ]reloadData];
    }
}

#pragma mark for RefreshablePage protocol
-(void) refreshPage{
    [[self tableView] reloadData];
}

- (UIImage*) currentTitleImage{
#warning should this have an image
    return nil;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return CSHOST_TABLE_COUNT;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // first section is not expandable
    return (section != 0);
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case CSHOST_GENERAL_TABLE:
            return nil;
            break;
        case CSHOST_CAPABILITY_TABLE:
            return @"Capabilities";
            break;
        default:
            return nil;
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    // Configure the cell...
    // configuration details
    switch (section) {
        case CSHOST_GENERAL_TABLE:
            return CSHOST_GENERAL_TABLE_COUNT;
            break;
        case CSHOST_CAPABILITY_TABLE:{
            CloudStackHostCore *host = [self csHost];
            return [[host capabilities] count];
            break;
        }
        default:
            break;
    }
    // should never get here
    return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    switch ([indexPath section]) {
        case CSHOST_GENERAL_TABLE:
            [self configureGeneralDetailsCell:cell atIndex:[indexPath row]];
            break;
        case CSHOST_CAPABILITY_TABLE:
            [self configureCapabilityDetailsCell:cell atIndex:[indexPath row]];
            break;
        default:
            break;
    }
    
    return cell;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureGeneralDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackHostCore *host = [self csHost];    
    
    switch (index) {
        case CSHOST_GENERAL_TABLE_CPUNUMBER:
        {
            [[cell textLabel] setText:@"number CPUs"];
            [[cell detailTextLabel] setText:[self convertToDisplayString:[host cpunumber]]];
            break;
        }
        case CSHOST_GENERAL_TABLE_CPUSPEED:
        {
            [[cell textLabel] setText:@"CPU speed"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[host cpuspeed]]];
            break;
        }
        case CSHOST_GENERAL_TABLE_IPADDRESS:
        {
            [[cell textLabel] setText:@"IP Address"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[host ipaddress]]];
            break;
        }
        case CSHOST_GENERAL_TABLE_TYPE:
        {
            [[cell textLabel] setText:@"Type"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[host type]]];
            break;
        }
        case CSHOST_GENERAL_TABLE_VERSION:
        {
            [[cell textLabel] setText:@"Version"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[host version]]];
            break;
        }
        default:
            break;
    }    
}

- (void)configureCapabilityDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackHostCore *host = [self csHost];
    [[cell textLabel] setText: [self convertToDisplayString:[[host capabilities] objectAtIndex:index]]];    
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}



@end
