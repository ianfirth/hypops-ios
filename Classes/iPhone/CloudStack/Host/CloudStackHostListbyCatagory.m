//
//  CloudStackVMListbyCatagory.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackHostListbyCatagory.h"
#import "ImageBuilder.h"
#import "RootViewController_ipad.h"
#import "CloudStackHostCore.h"
#import "CloudStackObjectsListByProperty.h"
#import "CloudStackHostListByState.h"
#import "CloudStackHostListViewController.h"

@interface CloudStackHostListbyCatagory ()
- (NSNumber *) csHostCatagoryAtIndex:(NSInteger) index;
- (void) reloadTableData;
- (CloudStackNativeHypervisorConnection*) hypervisorConnection;
@end

@implementation CloudStackHostListbyCatagory
@synthesize csHostCatagories, hypConnID;

#pragma mark -
#pragma mark View lifecycle

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        [self setHypConnID:[hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
        // always enough room in portrait so only enable scroll if pull to refresh available
        [[self tableView] setScrollEnabled:YES];
    }
    return self;
}

- (CloudStackNativeHypervisorConnection*) hypervisorConnection{
    return (CloudStackNativeHypervisorConnection*)[HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (void)viewDidLoad {
    [self setCsHostCatagories: [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSNull null], [NSNumber numberWithInt:CS_HOST_ALL],
                                  [NSNull null], [NSNumber numberWithInt:CS_HOST_BYTYPE],
                                  [NSNull null], [NSNumber numberWithInt:CS_HOST_BYHYPERVISOR],
                                  [NSNull null], [NSNumber numberWithInt:CS_HOST_BYSTATE],
                                  [NSNull null], [NSNumber numberWithInt:CS_HOST_BYVERSION],
                                  nil]];
    
    [super viewDidLoad];
    self.title = @"Host Catagory";
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void) viewDidUnload{
    [self setCsHostCatagories:nil];
    [super viewDidUnload];
}

-(void) reloadTableData{
    [[self tableView] reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSNumber *csHostCatagory = [self csHostCatagoryAtIndex:indexPath.row];   
    
    NSString *displayText;
    NSString *detailTextHeader;NSUInteger objectCount = 0;
    BOOL validCatagory = YES;
    BOOL useCount = NO;
    
    switch ([csHostCatagory intValue]) {
        case CS_HOST_ALL:
            displayText = @"All Hosts";
            [[cell imageView] setImage:[ImageBuilder hostImage]];
            detailTextHeader = @"Host Count:%d";
            useCount = YES;
            break;
        case CS_HOST_BYTYPE:
            displayText = @"Hosts By Type";
            [[cell imageView] setImage:[ImageBuilder hostImage]];
            detailTextHeader = @"List Hosts for each type";
            break;
        case CS_HOST_BYHYPERVISOR:
            displayText = @"Hosts By Hypervisor";
            [[cell imageView] setImage:[ImageBuilder hostImage]];
            detailTextHeader = @"List Hosts for each Hypervisor";
            break;
        case CS_HOST_BYSTATE:
            displayText = @"VMs By State";
            [[cell imageView] setImage:[ImageBuilder hostImage]];
            detailTextHeader = @"List Hosts for each State";
            break;
        case CS_HOST_BYVERSION:
            displayText = @"Hosts By Version";
            [[cell imageView] setImage:[ImageBuilder hostImage]];
            detailTextHeader = @"List Hosts for each version";
            break;
        default:
            validCatagory = NO;
            break;
    }
    
    if (validCatagory){        
        NSString *detailText;
        NSObject *predicate = [[self csHostCatagories] objectForKey:csHostCatagory];
        if (useCount){
        if ([predicate isKindOfClass:[NSNull class]]){
            objectCount = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_HOST_CORE] count];
        } else {
            objectCount = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_HOST_CORE withCondition:(NSPredicate *)predicate] count];
        }
        detailText = [NSString stringWithFormat:detailTextHeader,objectCount];
        }
        else{
            detailText = detailTextHeader;
        }
        
        // set the selection style and the accessory type
        if (objectCount >0 || !useCount){
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        } else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        
        // set the label text
        [[cell textLabel] setText:displayText];
        
        // set the detail text
        if (detailText != nil) {
            [[cell detailTextLabel] setText:detailText];
        }
    }        
    else {
        NSLog(@"Invalid cell catagory provided %@",csHostCatagory);
    }
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self csHostCatagories] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // if their are no objects for this cell then the disclosure indicator will not be set
    // if this the case then dont allow selection as there is no point.
    if (![[tableView cellForRowAtIndexPath:indexPath] accessoryType] == UITableViewCellAccessoryDisclosureIndicator){
        return;
    }
    
    NSNumber *csHostCatagory = [self csHostCatagoryAtIndex:indexPath.row];  
    NSPredicate *predicate = [[self csHostCatagories] objectForKey:csHostCatagory];
    
    if ([predicate isEqual:[NSNull null]]){
        predicate = nil;
    }
    
    UITableViewController *nextController = nil;
    UIViewController *detailViewForIpad = nil;
    
    // todo make this do something when selected
    UITableViewController *imageListViewController = [[CloudStackHostListViewController alloc] initWithHypervisorConnection:[self hypervisorConnection] predicate:nil];
    
    switch ([csHostCatagory intValue]) {
        case CS_HOST_ALL:
            nextController = imageListViewController;
            [nextController setTitle:@"All Hosts"];
            break;
        case CS_HOST_BYTYPE:
            nextController = [[CloudStackObjectsListByProperty alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate usingProperty:@"type" ofHypObjectType:HYPOBJ_CS_HOST_CORE nextView:imageListViewController ];
            [nextController setTitle:@"Hosts by Type"];
            break;
        case CS_HOST_BYSTATE:
            nextController = [[CloudStackObjectsListByProperty alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate usingProperty:@"state" ofHypObjectType:HYPOBJ_CS_HOST_CORE nextView:imageListViewController ];
            [nextController setTitle:@"Hosts by State"];
            break;
        case CS_HOST_BYHYPERVISOR:
            nextController = [[CloudStackObjectsListByProperty alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate usingProperty:@"hypervisor" ofHypObjectType:HYPOBJ_CS_HOST_CORE nextView:imageListViewController ];
            [nextController setTitle:@"Hosts by Hypervisor"];
            break;
        case CS_HOST_BYVERSION:
            nextController = [[CloudStackObjectsListByProperty alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate usingProperty:@"version" ofHypObjectType:HYPOBJ_CS_HOST_CORE nextView:imageListViewController ];
            [nextController setTitle:@"Hosts by Version"];
            break;
        default:
            break;
    }
    
    if (nextController){
        [self.navigationController pushViewController:nextController animated:YES];
    }
    
    // if its an ipad display details about the catagory selected
    if (detailViewForIpad){
        UINavigationController *navCon = [self navigationController];
        UIViewController *rootController = [[navCon viewControllers] objectAtIndex:0];
        
        // use detail view for ipad
        if ([rootController respondsToSelector:@selector(detailViewNavigationController)]){
            UINavigationController *dvController = [rootController performSelector:@selector(detailViewNavigationController)];
            [RootViewController_ipad setDetailViewRootController:detailViewForIpad onNavigationController:dvController];
        }
    }
    
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark -
#pragma mark Private methods
/*
 * return the catagory from the index specified.  This ensures that the list is provided in numerical order
 */
- (NSNumber *) csHostCatagoryAtIndex:(NSInteger) index{
    NSSortDescriptor *numberDescriptor = [[NSSortDescriptor alloc] initWithKey:@"intValue" ascending:YES];
    NSArray *keys = [[[self csHostCatagories] allKeys] sortedArrayUsingDescriptors:[NSArray arrayWithObject:numberDescriptor]];
    NSNumber *csVmCatagory = [keys objectAtIndex:index];
    return csVmCatagory;
}

#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {    
    if (![[self hypervisorConnection] isUpdatePendingForType:HYPOBJ_CS_HOST_CORE]){
        [[self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_CS_HOST_CORE];
    }
    // the call to stopLoading is on the data update received.
}


#pragma mark -
#pragma mark HypervisorConnection protocol implementation

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | HYPOBJ_CS_HOST_CORE) >0){
        [self reloadTableData];
        [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
    }
}


@end

