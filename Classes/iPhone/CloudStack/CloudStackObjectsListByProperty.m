//
//  EC2ImageListByPlatform.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackObjectsListByProperty.h"
#import "ImageBuilder.h"
#import "CloudStackBase.h"
#import "CloudStackVMCore.h"

#define NILPROPERTYSTRING @"** None **"

@interface CloudStackObjectsListByProperty ()
- (void) reloadTableData;
-(NSPredicate *) getObjectPredicateForProperty:(NSString *) objectValue;
@end

@implementation CloudStackObjectsListByProperty
@synthesize propertyValues, hypConnID ,rootPredicate,property,hypObjectType, nextViewController;

#pragma mark -
#pragma mark View lifecycle

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withPredicate:(NSPredicate *)predicate usingProperty:(NSString*)theProperty ofHypObjectType:(NSInteger)theHypObjectType nextView:(UIViewController*)theNextViewController{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        [self setRootPredicate:predicate];
        [self setHypConnID:[hypervisorConnection connectionID]];
        [self setProperty:theProperty];
        [self setHypObjectType:theHypObjectType];
        [self setNextViewController:theNextViewController];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
        // always enough room in portrait so only enable scroll if pull to refresh available
        [[self tableView] setScrollEnabled:YES];
        NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:5];
        // build each platform
        NSArray *baseObjects = [hypervisorConnection hypObjectsForType:hypObjectType withCondition:predicate]; 
        for (CloudStackBase *baseObject in baseObjects) {
            NSString *propertyValue = [[baseObject properties] valueForKey:property];  
            if (propertyValue == nil || [propertyValue isEqualToString:@""]){
                propertyValue = NILPROPERTYSTRING;
            }
            if (![tempArray containsObject:propertyValue]){
                [tempArray addObject:propertyValue];
            }
        }
        NSArray *c = [tempArray copy];
        [self setPropertyValues:c];
    }
    return self;
}

- (HypervisorConnectionFactory*) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (void) dealloc{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

-(UIImage*) cellImageforPropertyValue:(NSString*)propertyValue{
    // return defaults for the common types
    // can be overriden for different images
    UIImage* theImage = nil;
    switch ([self hypObjectType]) {
        case HYPOBJ_CS_VM_CORE:
        case HYPOBJ_CS_VM:
            theImage = [ImageBuilder vMImage];
            break;
        case HYPOBJ_CS_HOST_CORE:
        case HYPOBJ_CS_HOST:
            theImage = [ImageBuilder hostImage];            
            break;
        case HYPOBJ_CS_TEMPLATE:
            theImage = [ImageBuilder vMTemplateImage];            
            break;
        case HYPOBJ_CS_SECURITYGROUP:
            theImage = [ImageBuilder securityImage];            
            break;
        case HYPOBJ_CS_ZONE:
            theImage = [ImageBuilder zoneImage];            
            break;
        case HYPOBJ_CS_NETWORK:
            theImage = [ImageBuilder networkImage];            
            break;
        default:
            break;
    }
    
    return theImage;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSString *displayText;
    NSString *detailTextHeader; 

    NSString *objectValue = (NSString *)[[self propertyValues] objectAtIndex:indexPath.row];   
    NSPredicate *finalPredicate = [self getObjectPredicateForProperty:objectValue];NSUInteger objectCount = [[[self hypervisorConnection] hypObjectsForType:[self hypObjectType] withCondition:finalPredicate] count];
    
    displayText = objectValue;
    
    [[cell imageView] setImage:[self cellImageforPropertyValue:objectValue]];
    detailTextHeader = @"Count:%d";
    
    NSString *detailText;
    detailText = [NSString stringWithFormat:detailTextHeader,objectCount];
        
    // set the selection style and the accessory type
    if (objectCount >0){
      [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
      [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    } else {
      [cell setAccessoryType:UITableViewCellAccessoryNone];
      [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
        
    // set the label text
    [[cell textLabel] setText:displayText];
        
    // set the detail text
    if (detailText != nil) {
       [[cell detailTextLabel] setText:detailText];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self propertyValues] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

// when selecting the object show the actaul images that meet the defined predicates
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *objectValue = (NSString *)[[self propertyValues] objectAtIndex:indexPath.row];   
    NSPredicate *finalPredicate = [self getObjectPredicateForProperty:objectValue];
   
    // set the predicate for the view if possible
    UIViewController* theNextView = [self nextViewController];
    if ([theNextView respondsToSelector:@selector(setRootPredicate:)]){
        [theNextView performSelector:@selector(setRootPredicate:) withObject:finalPredicate];
    }
    
    //[imageListViewController setTitle:ec2ImagePlatform];
    [self.navigationController pushViewController:theNextView animated:YES];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}


#pragma mark -
#pragma mark Private methods

-(void) reloadTableData{
    [[self tableView] reloadData];
}

-(NSPredicate *) getObjectPredicateForProperty:(NSString *) objectValue{
    NSPredicate *predicate = nil;
    
    if (![objectValue isEqualToString: NILPROPERTYSTRING]){
        // need to work this out
        predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ like '%@'",[self property], objectValue]];
    }
    else
    {
        predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ == nil",[self property]]];
    }
    
    NSPredicate *finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate,[self rootPredicate], nil]];
    return finalPredicate;
}


#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {
    if (![[self hypervisorConnection] isUpdatePendingForType:[self hypObjectType]]){
        [[self hypervisorConnection] RequestHypObjectsForType:[self hypObjectType]];
    }
    // the call to stopLoading is on the data update received.
}

-(bool) isActive{
    return ![[self hypervisorConnection] isAutoUpdateEnabled];
}

#pragma mark -
#pragma mark HypervisorConnection protocol implementation

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | [self hypObjectType]) >0){
        //[self setXenObjectList:[xenHypervisorConnection hypObjectsForType:hypObjectType withCondition:predicate]];
        [self reloadTableData];
        [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
    }
}


@end

