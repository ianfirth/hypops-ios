//
//  CloudStackSecurityGroupListbyCatagory.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackSecurityGroupListbyCatagory.h"
#import "ImageBuilder.h"
#import "RootViewController_ipad.h"
#import "CloudStackSecurityGroup.h"
#import "CloudStackAccount.h"
#import "CloudStackObjectsListByProperty.h"
#import "CloudStackSecurityGroupListViewController.h"

@interface CloudStackSecurityGroupListbyCatagory ()
- (NSNumber *) csSecurityGroupCatagoryAtIndex:(NSInteger) index;
- (void) reloadTableData;
- (CloudStackNativeHypervisorConnection*) hypervisorConnection;
@end

@implementation CloudStackSecurityGroupListbyCatagory
@synthesize csSecurityGroupCatagories, hypConnID;

#pragma mark -
#pragma mark View lifecycle

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        [self setHypConnID:[hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
        // always enough room in portrait so only enable scroll if pull to refresh available
        [[self tableView] setScrollEnabled:YES];
    }
    return self;
}

- (CloudStackNativeHypervisorConnection*) hypervisorConnection{
    return (CloudStackNativeHypervisorConnection*)[HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (void)viewDidLoad {
    NSString* ref = [[[self hypervisorConnection] cloudStackAccount] name];
    
    [self setCsSecurityGroupCatagories:[NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSNull null], [NSNumber numberWithInt:CS_SECURITYGROUP_ALL],
                                  [NSNull null], [NSNumber numberWithInt:CS_SECURITYGROUP_BYDOMAIN],
                                  [CloudStackSecurityGroup securityGroups_OwnedByMe:ref],[NSNumber numberWithInt:CS_SECURITYGROUP_OWNEDBYME],
                                  nil]];
    
    [super viewDidLoad];
    self.title = @"SecurityGroup Catagory";
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void) viewDidUnload{
    [self setCsSecurityGroupCatagories:nil];
    [super viewDidUnload];
}

-(void) reloadTableData{
    [[self tableView] reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSNumber *csSecurityGroupCatagory = [self csSecurityGroupCatagoryAtIndex:indexPath.row];   
    
    NSString *displayText;
    NSString *detailTextHeader;NSUInteger objectCount = 0;
    BOOL validCatagory = YES;
    BOOL useCount = NO;
    
    switch ([csSecurityGroupCatagory intValue]) {
        case CS_SECURITYGROUP_ALL:
            displayText = @"All SecurityGroups";
            [[cell imageView] setImage:[ImageBuilder securityImage]];
            detailTextHeader = @"SecurityGroup Count:%d";
            useCount = YES;
            break;
        case CS_SECURITYGROUP_BYDOMAIN:
            displayText = @"SecurityGroups By Domain";
            [[cell imageView] setImage:[ImageBuilder securityImage]];
            detailTextHeader = @"List SecurityGroups for each Domain";
            break;
        case CS_SECURITYGROUP_OWNEDBYME:
            displayText = @"SecurityGroups I Own";
            [[cell imageView] setImage:[ImageBuilder securityImage]];
            detailTextHeader = @"SecurityGroup Count:%d";
            useCount = YES;
            break;
        default:
            validCatagory = NO;
            break;
    }
    
    if (validCatagory){        
        NSString *detailText;
        NSObject *predicate = [[self csSecurityGroupCatagories] objectForKey:csSecurityGroupCatagory];
        if (useCount){
        if ([predicate isKindOfClass:[NSNull class]]){
            objectCount = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_SECURITYGROUP] count];
        } else {
            objectCount = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_SECURITYGROUP withCondition:(NSPredicate *)predicate] count];
        }
        detailText = [NSString stringWithFormat:detailTextHeader,objectCount];
        }
        else{
            detailText = detailTextHeader;
        }
        
        // set the selection style and the accessory type
        if (objectCount >0 || !useCount){
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        } else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        
        // set the label text
        [[cell textLabel] setText:displayText];
        
        // set the detail text
        if (detailText != nil) {
            [[cell detailTextLabel] setText:detailText];
        }
    }        
    else {
        NSLog(@"Invalid cell catagory provided %@",csSecurityGroupCatagory);
    }
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self csSecurityGroupCatagories] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // if their are no objects for this cell then the disclosure indicator will not be set
    // if this the case then dont allow selection as there is no point.
    if (![[tableView cellForRowAtIndexPath:indexPath] accessoryType] == UITableViewCellAccessoryDisclosureIndicator){
        return;
    }
    
    NSNumber *csSecurityGroupCatagory = [self csSecurityGroupCatagoryAtIndex:indexPath.row];  
    NSPredicate *predicate = [[self csSecurityGroupCatagories] objectForKey:csSecurityGroupCatagory];
    
    if ([predicate isEqual:[NSNull null]]){
        predicate = nil;
    }
    
    UITableViewController *nextController = nil;
    UIViewController *detailViewForIpad = nil;
    
    // todo make this do something when selected
    UITableViewController *securityGroupListViewController = [[CloudStackSecurityGroupListViewController alloc] initWithHypervisorConnection:[self hypervisorConnection] predicate:nil];
    
    switch ([csSecurityGroupCatagory intValue]) {
        case CS_SECURITYGROUP_ALL:
            nextController = securityGroupListViewController;
            [nextController setTitle:@"All SecurityGroups"];
            break;
        case CS_SECURITYGROUP_BYDOMAIN:
            nextController = [[CloudStackObjectsListByProperty alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate usingProperty:@"domain" ofHypObjectType:HYPOBJ_CS_SECURITYGROUP nextView:securityGroupListViewController ];
            [nextController setTitle:@"SecurityGroups by Domain"];
            break;
        case CS_SECURITYGROUP_OWNEDBYME:
            nextController = [[CloudStackObjectsListByProperty alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate usingProperty:@"account" ofHypObjectType:HYPOBJ_CS_SECURITYGROUP nextView:securityGroupListViewController ];
            [nextController setTitle:@"SecurityGroups I Own"];
            break;
        default:
            break;
    }
    
    if (nextController){
        [self.navigationController pushViewController:nextController animated:YES];
    }
    
    // if its an ipad display details about the catagory selected
    if (detailViewForIpad){
        UINavigationController *navCon = [self navigationController];
        UIViewController *rootController = [[navCon viewControllers] objectAtIndex:0];
        
        // use detail view for ipad
        if ([rootController respondsToSelector:@selector(detailViewNavigationController)]){
            UINavigationController *dvController = [rootController performSelector:@selector(detailViewNavigationController)];
            [RootViewController_ipad setDetailViewRootController:detailViewForIpad onNavigationController:dvController];
        }
    }
    
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark -
#pragma mark Private methods
/*
 * return the catagory from the index specified.  This ensures that the list is provided in numerical order
 */
- (NSNumber *) csSecurityGroupCatagoryAtIndex:(NSInteger) index{
    NSSortDescriptor *numberDescriptor = [[NSSortDescriptor alloc] initWithKey:@"intValue" ascending:YES];
    NSArray *keys = [[[self csSecurityGroupCatagories] allKeys] sortedArrayUsingDescriptors:[NSArray arrayWithObject:numberDescriptor]];
    NSNumber *csVmCatagory = [keys objectAtIndex:index];
    return csVmCatagory;
}

#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {    
    if (![[self hypervisorConnection] isUpdatePendingForType:HYPOBJ_CS_SECURITYGROUP]){
        [[self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_CS_SECURITYGROUP];
    }
    // the call to stopLoading is on the data update received.
}

-(bool) isActive{
    return ![[self hypervisorConnection] isAutoUpdateEnabled];
}

#pragma mark -
#pragma mark HypervisorConnection protocol implementation

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | HYPOBJ_CS_SECURITYGROUP) >0){
        [self reloadTableData];
        [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
    }
}


@end

