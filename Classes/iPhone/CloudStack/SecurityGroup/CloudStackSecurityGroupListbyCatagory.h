//
//  CloudStackSecurityGroupListbyCatagory.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackNativeHypervisorConnection.h"
#import "PullRefreshTableViewController.h"

#define CS_SECURITYGROUP_ALL 0
#define CS_SECURITYGROUP_BYDOMAIN 1
#define CS_SECURITYGROUP_OWNEDBYME 2

@interface CloudStackSecurityGroupListbyCatagory : PullRefreshTableViewController <HypervisorConnectionDelegate> {
    HypervisorType hypervisorType; 
}

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess;

@property (copy) NSString *hypConnID;
@property (nonatomic, strong) NSDictionary *csSecurityGroupCatagories;

@end
