//
//  CloudStackNetworkListViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackNetworkListViewController.h"
#import "ImageBuilder.h"
#import "CloudStackNetwork.h"
#import "TitledDetailViewController.h"
#import "RootViewController_ipad.h"
#import "CommonUIStaticHelper.h"
#import "CloudStackImageBuilder.h"
#import "CloudStackNetworkGeneralViewController.h"

@interface CloudStackNetworkListViewController (){
    UITabBarController *tabBarController;
}

-(NSPredicate *) getFinalPredicateForSection:(NSString *) sectionKey;
   - (void) configureCell:(UITableViewCell *)cell forObject:(CloudStackNetwork*)networkObject;
   - (void) reloadTableData;
   - (void) buildSectionInformation;
   - (NSArray *) sortedImagesForSection:(NSInteger) section; 
@end

@implementation CloudStackNetworkListViewController
@synthesize hypConnID;

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection predicate:(NSPredicate *) predicate{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        [self setHypConnID:[hypervisorConnection connectionID]];
        [self setRootPredicate:predicate];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
    }

    return self;
}

- (HypervisorConnectionFactory*) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (void)dealloc
{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    sectionKeys = nil;
}

- (void) buildSectionInformation{
  // run predicaed for each letter of aplphabet and if returns a result then add predicate and letter
  // to dictionary
  // the number of entiries in the dictionary is the number of sections, and the predicate will
  // provide the content.
    if (sectionKeys != nil){
        sectionKeys = nil;
    }
    sectionKeys = [[NSMutableArray alloc] initWithCapacity:10];

    char letter = 'A';
    while (letter <= 'Z'){
        NSString *letterStr = [NSString stringWithFormat:@"%c",letter];
        NSPredicate *pred = [self getFinalPredicateForSection:letterStr];
        NSArray *matches = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_NETWORK withCondition:pred]; 
        if ([matches count] >0){
            // add to dictionary
            [sectionKeys addObject:letterStr];
        }        
        letter ++;
    }

    letter = '0';
    while (letter <= '9'){
        NSString *letterStr = [NSString stringWithFormat:@"%c",letter];
        NSPredicate *pred = [self getFinalPredicateForSection:letterStr];
        NSArray *matches = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_NETWORK withCondition:pred]; 
        if ([matches count] >0){
            // add to dictionary
            [sectionKeys addObject:letterStr];
        }        
        letter ++;
    }

    // add a section for all the rest
    NSString *letterStr = @"";
    NSPredicate *pred = [self getFinalPredicateForSection:letterStr];
    NSArray *matches = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_NETWORK withCondition:pred]; 
    if ([matches count] >0){
        // add to dictionary
        [sectionKeys addObject:letterStr];
    }            

}

-(void) setRootPredicate:(NSPredicate *)rootPredicate{
    _rootPredicate = rootPredicate;
    [self buildSectionInformation];
    [self reloadTableData];
}

-(NSPredicate *)rootPredicate{
    return _rootPredicate;
}

-(void) reloadTableData{
    // actually needs to break the things up into alpahbetical sections counts etc.
    // the same as when the thing is constucted.  Perhaps use a common method for this
    [self buildSectionInformation];
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark View Search Bar

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    // only show the status bar’s cancel button while in edit mode
    searchBar.showsCancelButton = YES;
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    // flush the previous search content
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
    
    if([searchText isEqualToString:@""] || searchText == nil){
    [self reloadTableData];
        return;
    }
    
    searchPredicate = [CloudStackVMCore nameBeginsOrContainsAWordBeginningWith:searchText];
    [self reloadTableData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
    [self reloadTableData];
    
    [searchBar resignFirstResponder];
    searchBar.text = @"";
}

// called when Search (in our case “Done”) button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self buildSectionInformation];
    UISearchBar *theSearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0,320,40)];
    theSearchBar.delegate = self;
    [self.view addSubview:theSearchBar];
    [[self tableView] setTableHeaderView:theSearchBar];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self buildSectionInformation];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

#pragma mark - Table view data source

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return sectionKeys;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [sectionKeys count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString* sectionStr = [sectionKeys objectAtIndex:section];
    if ([sectionStr isEqualToString: @""]){
        return @"** No Name **";
    }
    return [sectionKeys objectAtIndex:section];
}

- (void) configureCell:(UITableViewCell *)cell forObject:(CloudStackNetwork *)networkObject{
    // set the lable text for the cell
    [[cell textLabel] setText:[networkObject name]];
    // set the detail text for the cell
    [[cell detailTextLabel] setText:[networkObject displaytext]];
    // set the image for the cell

    [[cell imageView] setImage:[ImageBuilder networkImage]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionKey = [sectionKeys objectAtIndex:section];
    
    NSArray *networks = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_NETWORK withCondition:[self getFinalPredicateForSection:sectionKey]];
                               
    return [networks count];
}

/** 
 * returns the list of images for the view in the correctly sorted order
 */
- (NSArray *) sortedImagesForSection:(NSInteger) section {
    NSString *sectionKey = [sectionKeys objectAtIndex:section];
  
    NSArray *unsortedObjects = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_NETWORK withCondition:[self getFinalPredicateForSection:sectionKey]];
    NSArray *sortedObjects = [unsortedObjects sortedArrayUsingSelector:@selector(compareByName:)];
  return sortedObjects;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    
    [self configureCell:cell forObject:[[self sortedImagesForSection:[indexPath section]] objectAtIndex:[indexPath row]]];
    return cell;
}

#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {    
    if (![[self hypervisorConnection] isUpdatePendingForType:HYPOBJ_CS_NETWORK]){
        [[self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_CS_NETWORK];
    }
    // the call to stopLoading is on the data update received.
}

-(bool) isActive{
    return ![[self hypervisorConnection] isAutoUpdateEnabled];
}

#pragma mark -
#pragma mark HypervisorConnection protocol implementation
- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | HYPOBJ_CS_NETWORK) >0){
        //[self setXenObjectList:[xenHypervisorConnection hypObjectsForType:hypObjectType withCondition:predicate]];
        [self reloadTableData];
        [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
    }
}

#pragma mark - private methods
// combines the search predicate and the root predicate
-(NSPredicate *) getFinalPredicateForSection:(NSString *) sectionKey{
    
    NSPredicate *finalPredicate = [self rootPredicate];
    NSPredicate *sectionPredicate = nil;
    
    if (sectionKey){
        if ([sectionKey isEqualToString: @""]){
            sectionPredicate = [CloudStackNetwork nameNullOrEmpty];
        }else{
            sectionPredicate = [CloudStackNetwork nameBeginWith:sectionKey];
        }
    }    

    if (sectionPredicate != nil){
        finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:sectionPredicate, finalPredicate, nil]];
    }
    
    if (searchPredicate != nil){
        finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:searchPredicate, finalPredicate, nil]];
    }
    return finalPredicate;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CloudStackNetwork *networkForDisplay = [[self sortedImagesForSection:[indexPath section]] objectAtIndex:[indexPath row]];

    // add the general tab
    if (!tabBarController){
        tabBarController = [[UITabBarController alloc] init];
        NSMutableArray *localControllersArray = [[NSMutableArray alloc] initWithCapacity:2];

        CloudStackNetworkGeneralViewController* tbc = [[CloudStackNetworkGeneralViewController alloc] initWithhypervisorConnection: [self hypervisorConnection] withNetwork:networkForDisplay];
        
        NSPredicate *objectPred = [CloudStackNetwork networkWithReference:[networkForDisplay uniqueReference]];

        TitledDetailViewController* networkGeneralTitledView = [[TitledDetailViewController alloc] initWithHypervisorConnection:[self hypervisorConnection] 
                                                                                                      rootObjectRef:[networkForDisplay uniqueReference]
                                                                                                rootObjectPredicate:objectPred
                                                                                                      hypObjectType:HYPOBJ_CS_NETWORK
                                                                                                   namePropertyName:@"name"
                                                                                            descriptionPropertyName:@"displaytext"
                                                                                                        displayMode:DISPLAYMODE_NAMEDESCRIPTION
                                                                                                            tabName:@"General" 
                                                                                                    tabBarImageName:@"tab_General" 
                                                                                                              image:[ImageBuilder networkImage]
                                                                                            extentionViewController:tbc];    
        [localControllersArray addObject:networkGeneralTitledView];
        
        tabBarController.viewControllers = localControllersArray;
        
        [tabBarController setTitle:@"VM"];

    }
    else{
        for (TitledDetailViewController* view in [tabBarController viewControllers]){
            // this should be the image general and it should refresh the children ones.
            [view setImage:[ImageBuilder networkImage]];
            NSPredicate *objectPred = [CloudStackVMCore vmWithReference:[networkForDisplay uniqueReference]];
            [view setRootObjectPredicate:objectPred andReference:[networkForDisplay uniqueReference]];    
        }
    }
    
    // this will only push the view back on the stack if it is not already the top level view.
    // otherwise it will do nothing and the view will update in place.
    [CommonUIStaticHelper displayNextView:tabBarController usingNavigationController:[self navigationController] andRootViewController:[[[self navigationController] viewControllers]objectAtIndex:0]];         
}

@end
