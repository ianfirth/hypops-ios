//
//  CloudStackVMGeneralViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackNetworkGeneralViewController.h"
#import "ScrollableDetailTextCell.h"
#import "CloudStackNativeHypervisorConnection.h"
#import "ImageBuilder.h"
#import "CloudStackVMCore.h"

@interface CloudStackNetworkGeneralViewController ()
- (void) configureGeneralDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
- (void) configureAddressingDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
- (void) configureStatusDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;

@end

@implementation CloudStackNetworkGeneralViewController

@synthesize hypConnID;

-(HypervisorConnectionFactory *) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (CloudStackNetwork *) csNetwork{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'", [self networkId]]]; 
    NSArray *images = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_NETWORK withCondition:pred];
    if ([images count] >0){
        return [images objectAtIndex:0];
    }
    return nil;
}


- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withNetwork:(CloudStackNetwork *)thisNetwork{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setNetworkId: [thisNetwork uniqueReference]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
    }
    return self;
    
}

-(void)setNetworkId:(NSString *)networkId{
    _networkId = networkId;
    [[self tableView] reloadData];
}

-(NSString *)networkId{
    return _networkId; 
}

- (void) setId:(NSString*)newID{
    [self setNetworkId:newID];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark for RefreshablePage protocol
-(void) refreshPage{
    [[self tableView] reloadData];
}

- (UIImage*) currentTitleImage{
#warning should this have an image
    return nil;
}

#pragma mark -
#pragma mark HypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to vm objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    // could cut this down to just the relevent ones if required
    if (objectType == HYPOBJ_CS_NETWORK){
        [[self tableView ]reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return CSNETWORK_TABLE_COUNT;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // first section is not expandable
    return (section != 0);
}


// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case CSNETWORK_GENERAL_TABLE:
            return nil;
            break;
        case CSNETWORK_ADDRESSING_TABLE:
            return @"Addressing";
            break;
        case CSNETWORK_STATUS_TABLE:
            return @"Status";
            break;
        default:
            return nil;
            break;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    // Configure the cell...
    // configuration details
    switch (section) {
        case CSNETWORK_GENERAL_TABLE:
            return CSNETWORK_GENERAL_TABLE_COUNT;
            break;
        case CSNETWORK_ADDRESSING_TABLE:
            return CSNETWORK_ADDRESSING_TABLE_COUNT;
            break;
        case CSNETWORK_STATUS_TABLE:
            return CSNETWORK_STATUS_TABLE_COUNT;
            break;
        default:
            break;
    }
    // should never get here
    return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    switch ([indexPath section]) {
        case CSNETWORK_GENERAL_TABLE:
            [self configureGeneralDetailsCell:cell atIndex:[indexPath row]];
            break;
        case CSNETWORK_ADDRESSING_TABLE:
            [self configureAddressingDetailsCell:cell atIndex:[indexPath row]];
            break;
        case CSNETWORK_STATUS_TABLE:
            [self configureStatusDetailsCell:cell atIndex:[indexPath row]];
            break;
        default:
            break;
    }
    
    return cell;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureGeneralDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackNetwork *network = [self csNetwork];    
    
    switch (index) {
        case CSNETWORK_GENERAL_TABLE_ACCOUNT:
        {
            [[cell textLabel] setText:@"Account"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[network account]]];
            break;
        }
        case CSNETWORK_GENERAL_TABLE_DOMAIN:
        {
            [[cell textLabel] setText:@"Domain"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[network domain]]];
            break;
        }
        case CSNETWORK_GENERAL_TABLE_PROJECT:
        {
            [[cell textLabel] setText:@"Project"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[network project]]];
            break;
        }
        case CSNETWORK_GENERAL_TABLE_OFFERIGNNAME:
        {
            [[cell textLabel] setText:@"Network offering"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[network networkofferingname]]];
            break;
        }
        default:
            break;
    }    
}


- (void)configureAddressingDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackNetwork *network = [self csNetwork];    

    switch (index) {
        case CSNETWORK_ADDRESSING_TABLE_NETWORKDOMAIN:
        {
            [[cell textLabel] setText:@"Network domain"];
            NSString *value = [self convertToDisplayString:[network networkdomain]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSNETWORK_ADDRESSING_TABLE_PHYSICALNETWORKID:
        {
            [[cell textLabel] setText:@"Physical Net ID"];
            NSString *value = [self convertToDisplayString:[network physicalnetworkid]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSNETWORK_ADDRESSING_TABLE_DNS1:
        {
            [[cell textLabel] setText:@"Primary DNS"];
            NSString *value = [self convertToDisplayString:[network dns1]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSNETWORK_ADDRESSING_TABLE_DNS2:
        {
            [[cell textLabel] setText:@"Secondary DNS"];
            NSString *value = [self convertToDisplayString:[network dns2]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSNETWORK_ADDRESSING_TABLE_NETMASK:
        {
            [[cell textLabel] setText:@"Net Mask"];
            NSString *value = [self convertToDisplayString:[network netmask]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSNETWORK_ADDRESSING_TABLE_VLAN:
        {
            [[cell textLabel] setText:@"VLAN"];
            NSString *value = [self convertToDisplayString:[network vlan]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSNETWORK_ADDRESSING_TABLE_GATEWAY:
        {
            [[cell textLabel] setText:@"Gateway"];
            NSString *value = [self convertToDisplayString:[network gateway]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSNETWORK_ADDRESSING_TABLE_BROADCASTDOMAINTYPE:
        {
            [[cell textLabel] setText:@"Broadcast Domain type"];
            NSString *value = [self convertToDisplayString:[network broadcasedomaintype]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSNETWORK_ADDRESSING_TABLE_ACLTYPE:
        {
            [[cell textLabel] setText:@"ACL type"];
            NSString *value = [self convertToDisplayString:[network acltype]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSNETWORK_ADDRESSING_TABLE_SUBDOMAINACCESS:
        {
            [[cell textLabel] setText:@"Subdomain Access"];
            NSString *value = [self convertStringToBool:[network subdomainaccess]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        default:
            break;
    }    
}

- (void)configureStatusDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackNetwork *network = [self csNetwork];    
    switch (index) {
        case CSNETWORK_STATUS_TABLE_STATE:
        {
            [[cell textLabel] setText:@"State"];
            NSString *value = [self convertToDisplayString:[network state]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSNETWORK_STATUS_TABLE_TRAFFICTYPE:
        {
            [[cell textLabel] setText:@"Traffic type"];
            NSString *value = [self convertToDisplayString:[network traffictype]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSNETWORK_STATUS_TABLE_TYPE:
        {
            [[cell textLabel] setText:@"Type"];
            NSString *value = [self convertToDisplayString:[network type]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSNETWORK_STATUS_TABLE_RESTARTREQIRED:
        {
            [[cell textLabel] setText:@"Requires restart"];
            NSString *value = [self convertStringToBool:[network restartrequired]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        default:
            break;
    }    
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

@end
