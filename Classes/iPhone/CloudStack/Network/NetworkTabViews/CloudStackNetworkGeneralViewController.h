//
//  CloudStackVMGeneralViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ExpandableTableViewController.h"
#import "CloudStackNativeHypervisorConnection.h"
#import "CloudStackNetwork.h"
#import "Identifyable.h"
#import "RefreshablePage.h"

#define CSNETWORK_GENERAL_TABLE 0
#define CSNETWORK_ADDRESSING_TABLE 1
#define CSNETWORK_STATUS_TABLE 2
#define CSNETWORK_TABLE_COUNT 3

#define CSNETWORK_GENERAL_TABLE_ACCOUNT 0
#define CSNETWORK_GENERAL_TABLE_DOMAIN 1
#define CSNETWORK_GENERAL_TABLE_PROJECT 2
#define CSNETWORK_GENERAL_TABLE_OFFERIGNNAME 3
#define CSNETWORK_GENERAL_TABLE_COUNT 4

#define CSNETWORK_ADDRESSING_TABLE_NETWORKDOMAIN 0
#define CSNETWORK_ADDRESSING_TABLE_PHYSICALNETWORKID 1
#define CSNETWORK_ADDRESSING_TABLE_DNS1 2
#define CSNETWORK_ADDRESSING_TABLE_DNS2 3
#define CSNETWORK_ADDRESSING_TABLE_NETMASK 4
#define CSNETWORK_ADDRESSING_TABLE_VLAN 5
#define CSNETWORK_ADDRESSING_TABLE_GATEWAY 6
#define CSNETWORK_ADDRESSING_TABLE_BROADCASTDOMAINTYPE 7
#define CSNETWORK_ADDRESSING_TABLE_BROADCASETURI 8
#define CSNETWORK_ADDRESSING_TABLE_ACLTYPE 9
#define CSNETWORK_ADDRESSING_TABLE_SUBDOMAINACCESS 10
#define CSNETWORK_ADDRESSING_TABLE_COUNT 11

#define CSNETWORK_STATUS_TABLE_STATE 0
#define CSNETWORK_STATUS_TABLE_TRAFFICTYPE 1
#define CSNETWORK_STATUS_TABLE_TYPE 2
#define CSNETWORK_STATUS_TABLE_RESTARTREQIRED 3
#define CSNETWORK_STATUS_TABLE_COUNT 4


@interface CloudStackNetworkGeneralViewController : ExpandableTableViewController <HypervisorConnectionDelegate, Identifyable,RefreshablePage>{
    HypervisorType hypervisorType;
    NSString* _networkId;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withNetwork:(CloudStackNetwork *)thisNetwork;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

// this is used by relection in the tab contol t o update the page.  In this class it updates the networkID.
- (void) setId:(NSString*)newID;

@property (copy) NSString *hypConnID;
@property (copy) NSString *networkId;

@end
