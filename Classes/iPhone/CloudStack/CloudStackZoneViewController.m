//
//  CloudStackZoneViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackZoneViewController.h"
#import "CloudStackZone.h"
#import "ImageBuilder.h"
#import "CloudStackConnectionViewController.h"
#import "CommonUIStaticHelper.h"
#import "CloudStackZoneDetailViewController.h"

@interface CloudStackZoneViewController ()
-(CloudStackZone *) zoneForRow:(NSInteger)row;
- (CloudStackNativeHypervisorConnection *)hypervisorConnection;
@end

@implementation CloudStackZoneViewController


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [[self tableView] reloadData];
    [super viewWillAppear:animated];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    CloudStackZone *zone = [self zoneForRow:[indexPath row]];
    
    [[cell imageView] setImage:[ImageBuilder zoneImage]];
    
    NSString *detailText = nil;
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    // set the label text
    [[cell textLabel] setText:[zone name]];
    
    // set the detail text
    if (detailText != nil) {
        [[cell detailTextLabel] setText:detailText];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

-(void)viewDidLoad{
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];

    [super viewDidLoad];
    self.title = @"Zones";

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_ZONE withCondition:nil] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

- (CloudStackNativeHypervisorConnection *)hypervisorConnection {
    CloudStackNativeHypervisorConnection *hypervisorConnection = (CloudStackNativeHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_CLOUDSTACK_NATIVE connectonID:[self hypConnID]];
    return hypervisorConnection;
}

// when selecting the object show the actaul images that meet the defined predicates
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CloudStackNativeHypervisorConnection *hypervisorConnection;
    hypervisorConnection = [self hypervisorConnection];
    
    CloudStackZone *zone = [self zoneForRow:[indexPath row]];
    [hypervisorConnection setZoneId:[zone uniqueReference]];

    UIViewController<DetailViewProtocol> *cloudStackConnectionViewController = [[CloudStackConnectionViewController alloc] initWithHypervisorConnection:hypervisorConnection];
    [cloudStackConnectionViewController setTitle:[zone name]];
    
    [CommonUIStaticHelper displayMasterViewAndDetail:cloudStackConnectionViewController usingNavigationController:[self navigationController]];
    
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

#pragma mark -
#pragma mark private helper methods
-(CloudStackZone *) zoneForRow:(NSInteger)row{
   
    CloudStackZone *zone = (CloudStackZone *)[[[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_ZONE withCondition:nil] objectAtIndex:row];
    return zone;
}

- (UIViewController *) detailContentView{

    UIViewController *zoneView = [[CloudStackZoneDetailViewController alloc] initWithNibName:@"CloudStackZoneDetailView" bundle:[NSBundle mainBundle] andHypervisorConnectionId:hypConnID];
    return zoneView;
}

@end
