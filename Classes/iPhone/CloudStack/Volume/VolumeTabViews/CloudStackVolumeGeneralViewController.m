//
//  CloudStackVolumeGeneralViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackVolumeGeneralViewController.h"
#import "ScrollableDetailTextCell.h"
#import "CloudStackNativeHypervisorConnection.h"
#import "ImageBuilder.h"
#import "CloudStackVolume.h"

@interface CloudStackVolumeGeneralViewController ()
- (void) configureGeneralDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
- (void) configureOfferingDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
- (void) configureStorageDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;

@end

@implementation CloudStackVolumeGeneralViewController

@synthesize hypConnID;

-(HypervisorConnectionFactory *) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (CloudStackVolume *) csVolume{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'", [self volumeId]]]; 
    NSArray *images = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_CS_VOLUME withCondition:pred];
    if ([images count] >0){
        return [images objectAtIndex:0];
    }
    return nil;
}


- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withVolume:(CloudStackVolume *)thisVolume{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setVolumeId: [thisVolume uniqueReference]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
    }
    return self;
    
}

-(void)setVolumeId:(NSString *)vmId{
    _volumeId = vmId;
    [[self tableView] reloadData];
}

-(NSString *)volumeId{
    return _volumeId; 
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark Identifyable
- (void) setId:(NSString*)newID{
    [self setVolumeId:newID];
}

#pragma mark RefreshablePage
-(void) refreshPage{
    [[self tableView] reloadData];
}

-(UIImage*) currentTitleImage{
#warning this should probably not be nil.
    return nil;
}

#pragma mark -
#pragma mark HypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to vm objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    // could cut this down to just the relevent ones if required
    if (objectType == HYPOBJ_CS_VOLUME){
        [[self tableView ]reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return CSVOLUME_TABLE_COUNT;
}


- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // first section is not expandable
    return (section != 0);
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case CSVOLUME_GENERAL_TABLE:
            return nil;
            break;
        case CSVOLUME_OFFERING_TABLE:
            return @"Offerings";
            break;
        case CSVOLUME_STORGE_TABLE:
            return @"Storage Details";
            break;
        default:
            return nil;
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    // Configure the cell...
    // configuration details
    switch (section) {
        case CSVOLUME_GENERAL_TABLE:
            return CSVOLUME_GENERAL_TABLE_COUNT;
            break;
        case CSVOLUME_OFFERING_TABLE:
            return CSVOLUME_OFFERING_TABLE_COUNT;
            break;
        case CSVOLUME_STORGE_TABLE:
            return CSVOLUME_STORAGE_TABLE_COUNT;
            break;
        default:
            break;
    }
    // should never get here
    return 0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    switch ([indexPath section]) {
        case CSVOLUME_GENERAL_TABLE:
            [self configureGeneralDetailsCell:cell atIndex:[indexPath row]];
            break;
        case CSVOLUME_OFFERING_TABLE:
            [self configureOfferingDetailsCell:cell atIndex:[indexPath row]];
            break;
        case CSVOLUME_STORGE_TABLE:
            [self configureStorageDetailsCell:cell atIndex:[indexPath row]];
            break;
        default:
            break;
    }
    
    return cell;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureGeneralDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackVolume *volume = [self csVolume];    
    
    switch (index) {
        case CSVOLUME_GENERAL_TABLE_CREATEDON:
        {
            [[cell textLabel] setText:@"Created On"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[volume created]]];
            break;
        }
        case CSVOLUME_GENERAL_TABLE_ACCOUNT:
        {
            [[cell textLabel] setText:@"Account"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[volume account]]];
            break;
        }
        case CSVOLUME_GENERAL_TABLE_ISDESTROYED:
        {
            [[cell textLabel] setText:@"Is destroyed"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[volume destroyed]]];
            break;
        }
        case CSVOLUME_GENERAL_TABLE_DOMAIN:
        {
            [[cell textLabel] setText:@"Domain"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[volume domain]]];
            break;
        }
        case CSVOLUME_GENERAL_TABLE_PROJECT:
        {
            [[cell textLabel] setText:@"Project"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[volume project]]];
            break;
        }
        case CSVOLUME_GENERAL_TABLE_HYPERVISOR:
        {
            [[cell textLabel] setText:@"Hypervisor"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[volume hypervisor]]];
            break;
        }
        case CSVOLUME_GENERAL_TABLE_VMATTACHED:
        {
            [[cell textLabel] setText:@"Attached to"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[volume vmdisplayname]]];
            break;
        }
        default:
            break;
    }    
}

- (void)configureOfferingDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackVolume *volume = [self csVolume];    
    
    switch (index) {
        case CSVOLUME_OFFERING_TABLE_SERVICEOFFERING:
        {
            [[cell textLabel] setText:@"Service Offering"];
            NSString *value = [self convertToDisplayString:[volume serviceofferingname]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSVOLUME_OFFERING_TABLE_DISKOFFERING:
        {
            [[cell textLabel] setText:@"Disk Offering"];
            NSString *value = [self convertToDisplayString:[volume diskofferingname]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        default:
            break;
    }    
}

- (void)configureStorageDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    CloudStackVolume *volume = [self csVolume];    
    
    switch (index) {
        case CSVOLUME_STORAGE_TABLE_SIZE:
        {
            [[cell textLabel] setText:@"Size"];
            NSString *value = [self convertToDisplayString:[volume size]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSVOLUME_STORAGE_TABLE_STATE:
        {
            [[cell textLabel] setText:@"State"];
            NSString *value = [self convertToDisplayString:[volume state]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSVOLUME_STORAGE_TABLE_STORAGENAME:
        {
            [[cell textLabel] setText:@"Storage Name"];
            NSString *value = [self convertToDisplayString:[volume storage]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        case CSVOLUME_STORAGE_TABLE_STORAGETYPE:
        {
            [[cell textLabel] setText:@"Storage Type"];
            NSString *value = [self convertToDisplayString:[volume storagetype]];
            [[cell detailTextLabel] setText: value];
            break;
        }
        default:
            break;
    }    
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}



@end
