//
//  CloudStackVolumeGeneralViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ExpandableTableViewController.h"
#import "CloudStackNativeHypervisorConnection.h"
#import "CloudStackVolume.h"
#import "Identifyable.h"
#import "RefreshablePage.h"

#define CSVOLUME_GENERAL_TABLE 0
#define CSVOLUME_OFFERING_TABLE 1
#define CSVOLUME_STORGE_TABLE 2
#define CSVOLUME_TABLE_COUNT 3

#define CSVOLUME_GENERAL_TABLE_CREATEDON 0
#define CSVOLUME_GENERAL_TABLE_ACCOUNT 1
#define CSVOLUME_GENERAL_TABLE_DOMAIN 2
#define CSVOLUME_GENERAL_TABLE_PROJECT 3
#define CSVOLUME_GENERAL_TABLE_HYPERVISOR 4
#define CSVOLUME_GENERAL_TABLE_VMATTACHED 5
#define CSVOLUME_GENERAL_TABLE_ISDESTROYED 6
#define CSVOLUME_GENERAL_TABLE_COUNT 7

#define CSVOLUME_OFFERING_TABLE_SERVICEOFFERING 0
#define CSVOLUME_OFFERING_TABLE_DISKOFFERING 1
#define CSVOLUME_OFFERING_TABLE_COUNT 2

#define CSVOLUME_STORAGE_TABLE_SIZE 0
#define CSVOLUME_STORAGE_TABLE_STATE 1
#define CSVOLUME_STORAGE_TABLE_STORAGENAME 2
#define CSVOLUME_STORAGE_TABLE_STORAGETYPE 3
#define CSVOLUME_STORAGE_TABLE_COUNT 4

@interface CloudStackVolumeGeneralViewController : ExpandableTableViewController <HypervisorConnectionDelegate,Identifyable,RefreshablePage>{
    HypervisorType hypervisorType;
    NSString* _volumeId;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withVolume:(CloudStackVolume *)thisVolume;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

// this is used by relection in the tab contol to update the page.  In this class it updates the volumeID.
- (void) setId:(NSString*)newID;

@property (copy) NSString *hypConnID;
@property (copy) NSString *volumeId;

@end
