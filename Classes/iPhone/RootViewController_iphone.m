//
//  RootViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "RootViewController_iphone.h"
#import "ImageBuilder.h"
#import "XenHypervisorConnectionDefinition.h"
#import "EC2HypervisorConnectionDefinition.h"
#import "CloudStack_AWSConnectionDefinition.h"
#import "CloudStack_NativeConnectionDefinition.h"
#import "IOS6UINavigationController.h"
#import "WelcomeViewController.h"

@implementation RootViewController_iphone

#pragma mark -
#pragma mark View lifecycle

WelcomeViewController* welcomController;

-(BOOL) isWelcomeRequired{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:0];
    NSInteger resultCount = [sectionInfo numberOfObjects];
    
    if (resultCount == 0){
        return YES;
    }
    return NO;
}

- (void)displayWelcomeIfRequired {
    // if there are no connections show a welcome screen instead.
    if ([self isWelcomeRequired]){
        if (!welcomController){
            welcomController = [[WelcomeViewController alloc] init];
        }

        [[self view] addSubview:[welcomController view]];
        [[[self navigationController] navigationBar] setHidden:YES];
        
        [[welcomController getStartedButton] addTarget:self action:@selector(insertNewObject) forControlEvents:UIControlEventTouchUpInside];
        CGRect rect = [[self view] frame];
        rect.origin.y = 0;
        [[self view] setFrame:rect];
    }
    else{
        [[welcomController view] removeFromSuperview];
        welcomController = nil;
        
        // this is really not the right fix, but for some reason when creating the first connection the list goes up behind the navigation bar
        // when launched with an inital connection it is fine.
        [[[self navigationController] navigationBar] setHidden:NO];
        CGRect rect = [[self view] frame];
        rect.origin.y = 60;
        [[self view] setFrame:rect];
    }
}

-(void) refreshData{
    [self displayWelcomeIfRequired];
    [super refreshData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // make sure that the view is not displayed behind the navigation bar if there is one
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeAll;
    }

    // Set up the edit and add buttons.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    self.title = @"Connections";
}

#pragma mark -
#pragma mark Add a new object

-(void) displayHypervisorTypeSelectionActionSheet:(UIActionSheet *) actionSheet{
    // note that the iphone will need a cancel button and the ipad will not
    NSInteger position = [actionSheet addButtonWithTitle:@"Cancel"];
    [actionSheet setCancelButtonIndex:position];
    [actionSheet showInView:[self view]];
}

// used in ios8 and later
-(void) displayHypervisorTypeSelectionAlertController:(UIAlertController *)alertController{
    [alertController setModalPresentationStyle:UIModalPresentationPopover];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

// add a new object into the list.
// this pops up the editing window for the entry
- (void)insertNewObjectForHypervisorType:(HypervisorType) type {
    
    NSArray *conDefs = [HypervisorConnectionFactory connectionDefinitions];
    ConnectionDefinition *conDef;
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"hypervisorType == %@",[NSNumber numberWithInt:type]]];
    NSArray *result = [conDefs filteredArrayUsingPredicate:pred];

    if ([result count] >0){
        conDef = [result objectAtIndex:0];
        NSObject<ConnectionDefintionDelegate> *displayDelegate = nil;
        
        if (type == HYPERVISOR_XEN){
            displayDelegate = [[XenHypervisorConnectionDefinition alloc ] init ];
        }
        if (type == HYPERVISOR_EC2){
            displayDelegate = [[EC2HypervisorConnectionDefinition alloc ] init ];            
        }
        if (type == HYPERVISOR_CLOUDSTACK_AWS){
            displayDelegate = [[CloudStack_AWSConnectionDefinition alloc ] init ];            
        }
        if (conDef.hypervisorType == HYPERVISOR_CLOUDSTACK_NATIVE){
            displayDelegate = [[CloudStack_NativeConnectionDefinition alloc ] init ];            
        }

        ConnectionsViewController_iPhone *connectionsView = [[ConnectionsViewController_iPhone alloc] initWithNibName:@"Connections" bundle:[NSBundle mainBundle]   resultsController:self.fetchedResultsController connectionDefinition:conDef
                connectionDelegate:displayDelegate];
        
        UINavigationController *cntrol = [[IOS6UINavigationController alloc] initWithRootViewController:connectionsView];
        connectionsView.editMode = MODE_ADD;
        [self presentViewController:cntrol animated:YES completion:nil];
    }
}


- (void)setEditing:(BOOL)editing animated:(BOOL)animated {

    // Prevent new objects being added when in editing mode.
    [super setEditing:(BOOL)editing animated:(BOOL)animated];
    self.navigationItem.rightBarButtonItem.enabled = !editing;
}


-(void) connectionDeleted{
    [super connectionDeleted];
    [self displayWelcomeIfRequired];
}

#pragma mark -
#pragma mark Table view delegate

/*
 Selecting a row will either 
    --> edit the connection if in edit mode, or
    --> attempt to open the connection and navigate to it
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.editing){
        NSArray *conDefs = [HypervisorConnectionFactory connectionDefinitions];
        ConnectionDefinition *conDef;
        NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"hypervisorType == %@",[NSNumber numberWithLong:[[tableView cellForRowAtIndexPath:indexPath] tag]]]];
        NSArray *result = [conDefs filteredArrayUsingPredicate:pred];
        
        if ([result count] >0){
            conDef = [result objectAtIndex:0];
            
            NSObject<ConnectionDefintionDelegate>* displayDelegate;
            if (conDef.hypervisorType == HYPERVISOR_XEN){
                displayDelegate = [[XenHypervisorConnectionDefinition alloc ] init ];
            }
            if (conDef.hypervisorType == HYPERVISOR_EC2){
                displayDelegate = [[EC2HypervisorConnectionDefinition alloc ] init ];            
            }
            if (conDef.hypervisorType == HYPERVISOR_CLOUDSTACK_AWS){
                displayDelegate = [[CloudStack_AWSConnectionDefinition alloc ] init ];            
            }
            if (conDef.hypervisorType == HYPERVISOR_CLOUDSTACK_NATIVE){
                displayDelegate = [[CloudStack_NativeConnectionDefinition alloc ] init ];            
            }

            ConnectionsViewController_iPhone *connectionsView = [[ConnectionsViewController_iPhone alloc] initWithNibName:@"Connections" bundle:[NSBundle mainBundle] resultsController:self.fetchedResultsController connectionDefinition:conDef connectionDelegate:displayDelegate ];
            
            UINavigationController *cntrol = [[IOS6UINavigationController alloc] initWithRootViewController:connectionsView];
            connectionsView.editMode = MODE_EDIT;
            connectionsView.objectToEdit = indexPath;
            
            [self presentViewController:cntrol animated:YES completion:nil];
        }
    }
    else {
        [self makeConnectionWithIndexPath:indexPath];
    }
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

@end

