//
//  EC2InstanceByTagKeyViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2HypervisorConnection.h"
#import "PullRefreshTableViewController.h"

@interface EC2InstanceByTagKeyViewController : PullRefreshTableViewController <HypervisorConnectionDelegate> {
    HypervisorType hypervisorType;
}

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withPredicate:(NSPredicate *)predicate;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess;

@property (copy) NSString *hypConnID;
@property (nonatomic, strong) NSArray *ec2Tags;
@property (nonatomic, strong) NSPredicate *rootPredicate;
@end
