//
//  EC2InstanceListViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2HypervisorConnection.h"
#import "EC2InstanceWrapper.h"
#import "PullRefreshTableViewController.h"
#import "PowerControlViewController.h"

@interface EC2InstanceListViewController : UIViewController
                 <UISearchBarDelegate,
                  HypervisorConnectionDelegate,
                  PullToRefreshDelegate,
                  UITableViewDataSource,
                  UITableViewDelegate,
                  UIAlertViewDelegate>{
    
    // the search predicate used to further filter the objects
    NSPredicate *searchPredicate;
    
    HypervisorType hypervisorType;
}

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection predicate:(NSPredicate *) predicate;
- (UITabBarController *) instanceViewControllerForHypervisorConnection:(HypervisorConnectionFactory *)ec2HypervisorConnection EC2Instance:(EC2InstanceWrapper *)instance;

@property (copy) NSString *hypConnID;
@property (strong) NSPredicate *rootPredicate;

@property IBOutlet UIView* powerContollerView;
@property IBOutlet UIView* instancesView;

@end
