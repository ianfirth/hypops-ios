//
//  EC2InstanceListByStatusViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2InstanceListByStatusViewController.h"
#import "EC2InstanceWrapper.h"
#import "EC2InstanceListViewController.h"
#import "EC2ImageBuilder.h"  
#import "ImageBuilder.h"
#import "EC2InstanceByTagKeyViewController.h"
#import "ConnectionObjectUITableViewCell.h"

@interface EC2InstanceListByStatusViewController ()
    - (NSNumber *) ec2InstanceCatagoryAtIndex:(NSUInteger) index;
    -(void) reloadTableData;
@end

@implementation EC2InstanceListByStatusViewController
@synthesize ec2InstanceCatagories, hypConnID;

- (HypervisorConnectionFactory*) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

#pragma mark -
#pragma mark View lifecycle

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        [self setHypConnID:[hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
        // always enough room in portrait so only enable scroll if pull to refresh available
        [[self tableView] setScrollEnabled:![hypervisorConnection isAutoUpdateEnabled]];
    }
    return self;
}

- (void)viewDidLoad {
    [self setEc2InstanceCatagories: [NSDictionary dictionaryWithObjectsAndKeys:
                               [NSNull null], [NSNumber numberWithInt:EC2_INSTANCE_ALL],
                               [EC2InstanceWrapper instance_Tagged],[NSNumber numberWithInt:EC2_INSTANCE_TAGGED],
                               [EC2InstanceWrapper instance_PowerStateFor:EC2POWERSTATE_PENDING],[NSNumber numberWithInt:EC2_INSTANCE_PENDING],
                               [EC2InstanceWrapper instance_PowerStateFor:EC2POWERSTATE_RUNNING],[NSNumber numberWithInt:EC2_INSTANCE_RUNNING],
                               [EC2InstanceWrapper instance_PowerStateFor:EC2POWERSTATE_SHUTTINGDOWN],[NSNumber numberWithInt:EC2_INSTANCE_SHUTTINGDOWN],
                               [EC2InstanceWrapper instance_PowerStateFor:EC2POWERSTATE_TERMINATED],[NSNumber numberWithInt:EC2_INSTANCE_TERMINATED],
                               [EC2InstanceWrapper instance_PowerStateFor:EC2POWERSTATE_STOPPING],[NSNumber numberWithInt:EC2_INSTANCE_STOPPING],
                               [EC2InstanceWrapper instance_PowerStateFor:EC2POWERSTATE_STOPPED],[NSNumber numberWithInt:EC2_INSTANCE_STOPPED],
                               nil]];
    [super viewDidLoad];
    self.title = @"Instances";
    [[self tableView] registerNib:[UINib nibWithNibName:@"ConnectionObjectUITableViewCell" bundle:nil] forCellReuseIdentifier:@"ConnectionObjectUITableViewCell"];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void) viewDidUnload{
    [self setEc2InstanceCatagories:nil];
    [super viewDidUnload];
}

-(void) reloadTableData{
    [[self tableView] reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(ConnectionObjectUITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSNumber *ec2InstanceCatagory = [self ec2InstanceCatagoryAtIndex:indexPath.row];   
    
    NSString *displayText;
    NSUInteger  objectCount = 0;
    BOOL validCatagory = YES;
    
    switch ([ec2InstanceCatagory intValue]) {
        case EC2_INSTANCE_PENDING:
            displayText = @"Pending";
            [[cell imageView2] setImage:[EC2ImageBuilder buildInstanceImageForInstancePowerState:EC2POWERSTATE_PENDING]];
            break;
        case EC2_INSTANCE_RUNNING:
            displayText = @"Running";
            [[cell imageView2] setImage:[EC2ImageBuilder buildInstanceImageForInstancePowerState:EC2POWERSTATE_RUNNING]];
            break;
        case EC2_INSTANCE_SHUTTINGDOWN:
            displayText = @"Shutting down";
            [[cell imageView2] setImage:[EC2ImageBuilder buildInstanceImageForInstancePowerState:EC2POWERSTATE_SHUTTINGDOWN]];
            break;
        case EC2_INSTANCE_TERMINATED:
            displayText = @"Terminated";
            [[cell imageView2] setImage:[EC2ImageBuilder buildInstanceImageForInstancePowerState:EC2POWERSTATE_TERMINATED]];
            break;
        case EC2_INSTANCE_STOPPING:
            displayText = @"Stopping";
            [[cell imageView2] setImage:[EC2ImageBuilder buildInstanceImageForInstancePowerState:EC2POWERSTATE_STOPPING]];
            break;
        case EC2_INSTANCE_STOPPED:
            displayText = @"Stopped";
            [[cell imageView2] setImage:[EC2ImageBuilder buildInstanceImageForInstancePowerState:EC2POWERSTATE_STOPPED]];
            break;
        case EC2_INSTANCE_ALL:
            displayText = @"All Instances";
            [[cell imageView2] setImage:[ImageBuilder vMImage]];
            break;
        case EC2_INSTANCE_TAGGED:
            displayText = @"Tagged Instances";
            [[cell imageView2] setImage:[ImageBuilder vMTaggedImage]];
            break;
        default:
            validCatagory = NO;
            break;
    }
    
    if (validCatagory){
        
        NSString *detailText;
        NSObject *predicate = [[self ec2InstanceCatagories] objectForKey:ec2InstanceCatagory];
        if ([predicate isKindOfClass:[NSNull class]]){
            objectCount = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2INSTANCES] count];
        } else {
            objectCount = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2INSTANCES withCondition:(NSPredicate *)predicate] count];
        }
        detailText = [NSString stringWithFormat:@"%lu",(unsigned long)objectCount];
        
        // set the selection style and the accessory type
        if (objectCount >0){
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        } else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        
        // set the label text
        [[cell textLabel2] setText:displayText];
        
        // set the detail text
        if (detailText != nil) {
            [[cell detailTextLabel2] setText:detailText];
        }
    }        
    else {
        NSLog(@"Invalid cell catagory provided %@",ec2InstanceCatagory);
    }

    // the refresh is done by pull to refresh so dont need this one.
    [[cell refreshButton] setHidden:YES];
    [[cell refreshButton] setHidden:YES];
    
    // make every other row use alternate color
    [cell setAlternate:indexPath.row %2];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self ec2InstanceCatagories] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"ConnectionObjectUITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ConnectionObjectUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:(ConnectionObjectUITableViewCell*)cell atIndexPath:indexPath];
    
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // if no objects then do nothing
    if ([[tableView cellForRowAtIndexPath:indexPath] accessoryType ] == UITableViewCellAccessoryNone){
        return;
    }
    
    NSNumber *ec2InstanceCatagory = [self ec2InstanceCatagoryAtIndex:indexPath.row]; 
    NSObject *predicate = [[self ec2InstanceCatagories] objectForKey:ec2InstanceCatagory];
    if ([predicate isKindOfClass:[NSNull class]]){
        predicate = nil;
    }
    
    if ([ec2InstanceCatagory intValue] == EC2_INSTANCE_TAGGED){

        EC2InstanceByTagKeyViewController *instanceByTagKey = [[EC2InstanceByTagKeyViewController alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:(NSPredicate *)predicate];
        
        [instanceByTagKey setTitle:@"Instances by Tag"];
        [self.navigationController pushViewController:instanceByTagKey animated:YES];
    }
    else
    {
        EC2InstanceListViewController *instanceListViewController = [[EC2InstanceListViewController alloc] initWithHypervisorConnection:[self hypervisorConnection] predicate:(NSPredicate *)predicate];
        
        switch ([ec2InstanceCatagory intValue]) {
            case EC2_INSTANCE_PENDING:
                [instanceListViewController setTitle:@"Pending Instances"];
                break;
            case EC2_INSTANCE_RUNNING:
                [instanceListViewController setTitle:@"Running Instances"];
                break;
            case EC2_INSTANCE_SHUTTINGDOWN:
                [instanceListViewController setTitle:@"ShuttingDown Instances"];
                break;
            case EC2_INSTANCE_TERMINATED:
                [instanceListViewController setTitle:@"Terminated Instances"];
                break;
            case EC2_INSTANCE_STOPPING:
                [instanceListViewController setTitle:@"Stopping Instances"];
                break;
            case EC2_INSTANCE_STOPPED:
                [instanceListViewController setTitle:@"Stopped Instances"];
                break;
            case EC2_INSTANCE_ALL:
                [instanceListViewController setTitle:@"All Instances"];
                break;
            default:
                break;
        }
        
        [self.navigationController pushViewController:instanceListViewController animated:YES];
    }
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark -
#pragma mark Private methods
/*
 * return the catagory from the index specified.  This ensures that the list is provided in numerical order
 */
- (NSNumber *) ec2InstanceCatagoryAtIndex:(NSUInteger) index{
    NSSortDescriptor *numberDescriptor = [[NSSortDescriptor alloc] initWithKey:@"intValue" ascending:YES];
    NSArray *keys = [[[self ec2InstanceCatagories] allKeys] sortedArrayUsingDescriptors:[NSArray arrayWithObject:numberDescriptor]];
    NSNumber *ec2InstanceCatagory = [keys objectAtIndex:index];
    return ec2InstanceCatagory;
}



#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {    
    if (![[self hypervisorConnection] isUpdatePendingForType:HYPOBJ_EC2INSTANCES]){
        [[self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_EC2INSTANCES];
    }
    // the call to stopLoading is on the data update received.
}


// override, and only add the header if not in auto refresh mode
- (void)addPullToRefreshHeader {
    if (![[self hypervisorConnection] isAutoUpdateEnabled]){
        [super addPullToRefreshHeader];
    }
}

// override, and only add the header if not in auto refresh mode
- (void)startLoading {
    if (![[self hypervisorConnection] isAutoUpdateEnabled]){
        [super startLoading];
    }
}

#pragma mark -
#pragma mark HypervisorConnection protocol implementation
- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | HYPOBJ_EC2INSTANCES) >0){
        [self reloadTableData];
        if (![[self hypervisorConnection] isAutoUpdateEnabled]){
            [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
        }
    }
}


@end

