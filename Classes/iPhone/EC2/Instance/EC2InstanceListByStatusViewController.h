//
//  EC2InstanceListByStatusViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "PullRefreshTableViewController.h"
#import "EC2HypervisorConnection.h"

#define EC2_INSTANCE_ALL 0
#define EC2_INSTANCE_TAGGED 1
#define EC2_INSTANCE_PENDING 2
#define EC2_INSTANCE_RUNNING 3
#define EC2_INSTANCE_SHUTTINGDOWN 4
#define EC2_INSTANCE_STOPPING 5
#define EC2_INSTANCE_STOPPED 6
#define EC2_INSTANCE_TERMINATED 7

@interface EC2InstanceListByStatusViewController : PullRefreshTableViewController <HypervisorConnectionDelegate> {
    NSDictionary *xenVMCatagories;

    HypervisorType hypervisorType;
    
}

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection;

@property (nonatomic, strong) NSDictionary *ec2InstanceCatagories;
    // the connection ID for the current hypervisor connection
@property (copy) NSString *hypConnID;

@end
