//
//  XenVMPowerViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2InstancePowerTableViewController.h"
#import "HypOpsAppDelegate.h"
#import "EC2ImageBuilder.h"

@interface EC2InstancePowerTableViewController(){
    BOOL takeActionOnAppear;
    NSTimer* refreshTimer;
}

- (void) configureCell:(UITableViewCell *)cell atIndex:(NSInteger) index;

@end

@implementation EC2InstancePowerTableViewController

@synthesize hypConnID, instanceId;

-(EC2HypervisorConnection *) hypervisorConnection{
    return (EC2HypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_EC2 connectonID:[self hypConnID]];
}

- (EC2InstanceWrapper *) ec2Instance{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"instanceId like [c]'%@'", [self instanceId]]];
    NSArray *images = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2INSTANCES withCondition:pred];
    if ([images count] >0){
        return [images objectAtIndex:0];
    }
    return nil;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withInstance:(EC2InstanceWrapper *)thisInstance{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setInstanceId: [thisInstance instanceId]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        takeActionOnAppear = YES;
    }
    return self;
    
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];

    // Without this label the heading will show though the disbale tinit for some reason
    UILabel *l = [[UILabel alloc] init];
    [l setText:@""];
    [tableView setTableHeaderView:l];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

/**
 * Set the flag to indicate that the appear operateion that
 * are about to happen should not be acted on
 * they come as part of the hack to force screen to be rotated to the
 * desired orientation on edit screens
 */
-(void) setTakeActionOnAppear:(NSNumber*)takeAction{
    takeActionOnAppear =  [takeAction boolValue];
}

- (void) viewWillAppear:(BOOL)animated{
    if (takeActionOnAppear){
    if (!UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation])){
        // ----- force the view to be portrait only
        //will rotate status bar
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
        
        //will re-rotate view according to statusbar
        UIViewController *temp = [[UIViewController alloc]init];
        [self presentViewController:temp animated:NO completion:nil];
        [self dismissViewControllerAnimated:NO completion:nil];
        // ---- end force view to be portrait only
    }

    [super viewWillAppear:animated];
    // make sure that the date on the page is upto date
    [[self tableView] reloadData];
    EC2HypervisorConnection *ec2HypervisorConnection = (EC2HypervisorConnection*)[self hypervisorConnection];
    [ec2HypervisorConnection addHypervisorConnectionDelegate:self];
    [ec2HypervisorConnection setWalkTreeMode:NO];
    }
}

-(BOOL)shouldAutorotate{
    return NO;
}

-(void) viewDidAppear:(BOOL)animated{
    if (takeActionOnAppear){
        [self startAutoUpdate];
        // Turn on proximity monitoring  this will shut off screen when something is close by
        // stops accedently powering thing on/off when device is in pocket etc..
        UIDevice *device = [UIDevice currentDevice];
        [device setProximityMonitoringEnabled:YES];
        [super viewDidAppear:animated];
        
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self stopAutoUpdate];
    // Turn off proximity monitoring so back to normal
    UIDevice *device = [UIDevice currentDevice];
    [device setProximityMonitoringEnabled:NO];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [self stopAutoUpdate];
}

#pragma mark for RefreshablePage protocol
-(void) refreshPage{
    [[self tableView] reloadData];
}

- (UIImage*) currentTitleImage{
    EC2InstanceWrapper* instance = [self ec2Instance];
    return [EC2ImageBuilder buildInstanceImageForInstance:instance];
}

#pragma mark Identifyable
- (void) setId:(NSString*)newID{
    [self setInstanceId:newID];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    return NO;
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    EC2PowerState state = [[self ec2Instance] powerState];
    NSString* stateStr = nil;
    switch (state) {
        case EC2POWERSTATE_PENDING:
            stateStr = @"Start Pending";
            break;
        case EC2POWERSTATE_RUNNING:
            stateStr = @"Running";
            break;
        case EC2POWERSTATE_SHUTTINGDOWN:
            stateStr = @"Shutting Down";
            break;
        case EC2POWERSTATE_STOPPED:
            stateStr = @"Stopped";
            break;
        case EC2POWERSTATE_STOPPING:
            stateStr = @"Stopping";
            break;
        case EC2POWERSTATE_TERMINATED:
            stateStr = @"Terminated";
    }
    return [NSString stringWithFormat:@"Power (currently %@)",stateStr];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[self ec2Instance] availablePowerOperations] count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    [self configureCell:cell atIndex:[indexPath row]];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    EC2InstanceWrapper *ec2Instance = [self ec2Instance];NSUInteger powerState = [[[[ec2Instance availablePowerOperations] allObjects] objectAtIndex:index] intValue];

    NSString *buttonText = nil;
    NSString *imageName = nil;
    switch (powerState){
        case EC2POWEROPERATION_START:
            buttonText = @"Start Instance";
            imageName = @"start_vm";
            break;
        case EC2POWEROPERATION_STOP:
            buttonText = @"Stop Instance";
            imageName = @"stop_vm";
            break;
        case EC2POWEROPERATION_TERMINATE:
            buttonText = @"Terminate Instance";
            imageName = @"terminate_vm";
            break;
    }
    
    [[cell textLabel] setText:buttonText];
    [[cell imageView] setImage:[UIImage imageNamed:imageName]];

    // remove any existing button or spinner from the view
    [[[cell contentView] viewWithTag:100] removeFromSuperview];
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {    
    NSSet *powerOpSet = [[self ec2Instance] availablePowerOperations];
    EC2PowerOperation powerOp = [[[powerOpSet allObjects] objectAtIndex:[indexPath row]] intValue];
    NSError* error;
    if (powerOp == EC2POWEROPERATION_START){
        error =[[self hypervisorConnection] startInstances:[NSMutableArray arrayWithObject:[self ec2Instance]] ignoreTerminated:NO];
    }
    else if (powerOp == EC2POWEROPERATION_TERMINATE){
        error = [[self hypervisorConnection] terminateInstances:[NSMutableArray arrayWithObject:[self ec2Instance]]];
    }
    else if (powerOp == EC2POWEROPERATION_STOP){
        error = [[self hypervisorConnection] stopInstances:[NSMutableArray arrayWithObject:[self ec2Instance]] ignoreTerminated:NO];
    }
    
    if (error){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[error domain] message:[error localizedDescription]
                                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert setTag:0];
        [alert show];
    }
}

-(void) stopAutoUpdate{
    if (refreshTimer){
        [refreshTimer invalidate];
        refreshTimer = nil;
    }
}

-(void) startAutoUpdate{
    [self stopAutoUpdate];
    refreshTimer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(autoUpdate:)  userInfo:nil repeats:YES];
}

-(void) autoUpdate:(NSTimer *) theTimer {
    [(EC2HypervisorConnection*) [self hypervisorConnection] RequestHypObject:[[self ec2Instance] instanceId] ForType:HYPOBJ_EC2INSTANCES];
}

#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(EC2HypervisorConnection *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    [[self tableView] reloadData];
    [self startAutoUpdate]; // restart the timer
}
@end
