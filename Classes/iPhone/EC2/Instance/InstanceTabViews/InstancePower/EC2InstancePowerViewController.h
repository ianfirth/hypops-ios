//
//  EC2InstancePowerViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import <UIKit/UIKit.h>
#import "SectionTitleViewController.h"
#import "EC2InstancePowerTableViewController.h"
#import "EC2InstanceWrapper.h"

@interface EC2InstancePowerViewController : UIViewController<RefreshablePage,Identifyable>{
    
    IBOutlet UIView* headerView;
    IBOutlet UIView* mainTableView;
    EC2InstancePowerTableViewController* tableViewController;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withInstance:(EC2InstanceWrapper *)thisInstance;

@property UIView* headerView;
@property UIView* mainTableView;
@property EC2InstancePowerTableViewController* tableViewController;

@property (copy) NSString *hypConnID;
@property (copy) NSString *ec2InstanceId;

@end
