//
//  EC2InstancePowerViewController
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "GradientView.h"
#import "EC2HypervisorConnection.h"
#import "EC2InstanceWrapper.h"
#import "Identifyable.h"
#import "RefreshablePage.h"

@interface EC2InstancePowerTableViewController : UITableViewController
            <HypervisorConnectionDelegate, Identifyable,RefreshablePage> {
    NSString *xenVMRef;
    // the connection ID for the current hypervisor connection
    NSString *hypConnID;NSUInteger  powerOpINProgress;
}
- (id)initWithhypervisorConnection:(EC2HypervisorConnection *)hypervisorConnection withInstance:(EC2InstanceWrapper *)thisInstance;

// this is used in the tab contol to update the page.  In this class it updates the xenVMRef.
- (void) setId:(NSString*)newID;

@property (copy) NSString *hypConnID;
@property (copy) NSString *instanceId;

@end


