//
//  EC2ImageGeneralViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2InstanceGeneralViewController.h"

#import "XenHypervisorConnection.h"
#import "XenImageBuilder.h"
#import "SectionTitleViewController.h"
#import "EC2ImageBuilder.h"

@interface EC2InstanceGeneralViewController (){
    SectionTitleViewController* headerViewController;
}

@end

@implementation EC2InstanceGeneralViewController

@synthesize headerView;
@synthesize tableViewController;
@synthesize mainTableView;

@synthesize hypConnID, ec2InstanceId;

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withInstance:(EC2InstanceWrapper *)thisInstance{
    
    self = [super initWithNibName:@"EC2InstanceGeneralViewController" bundle:nil];
    if (self){
        [self setEc2InstanceId:[thisInstance instanceId]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        [self setTableViewController:[[EC2InstanceGeneralTableViewController alloc] initWithhypervisorConnection:hypervisorConnection withInstance:[self ec2Instance]]];
        // set the tab bar button details
        self.tabBarItem.title = @"General";
        //Set the image for the tab bar
        self.tabBarItem.image = [UIImage imageNamed:@"tab_General"];
    }
    return self;
}

-(EC2HypervisorConnection *) hypervisorConnection{
    return (EC2HypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_EC2 connectonID:[self hypConnID]];
}

- (EC2InstanceWrapper *) ec2Instance{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"instanceId like [c]'%@'", [self ec2InstanceId]]];
    NSArray *images = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2INSTANCES withCondition:pred];
    if ([images count] >0){
        return [images objectAtIndex:0];
    }
    return nil;
}


- (void)viewDidLoad
{    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    headerViewController = [[SectionTitleViewController alloc] initWithNibName:@"SectionTitleViewController" bundle:[NSBundle mainBundle] ];
    
    [[self headerView] addSubview:[headerViewController view]];
    [[self mainTableView] addSubview:[[self tableViewController] tableView]];
    [[tableViewController tableView] setAutoresizingMask: UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    CGRect tableViewArea = [[self mainTableView] frame];
    tableViewArea.origin.x = 0;
    tableViewArea.origin.y =0;
    // same size as parent but seeing as its insize it make x and y =0
    [[headerViewController view] setFrame:[[self headerView] frame]];
    [[[self tableViewController] tableView] setFrame:tableViewArea];
    [self updatePageHeading];
}

-(void) updatePageHeading{
    [headerViewController setTheTitle:[[self ec2Instance] instanceId]];
#warning what is the right thing to show for an image description?
    [headerViewController setTheDescription:@""];
    [headerViewController setTheImage:[self currentTitleImage]];
}

- (UIImage*) currentTitleImage{
    return[EC2ImageBuilder buildInstanceImageForInstance:[self ec2Instance]];
}


#pragma mark for Identifyable protocol
-(void) setId:(NSString *)newID{
    [self setEc2InstanceId:newID];
    [tableViewController setId:newID];
    [tableViewController refreshPage];
}

#pragma mark for RefreshablePage protocol
-(void) refreshPage{
    [[[self tableViewController] tableView] reloadData];
    [self updatePageHeading];
}

@end
