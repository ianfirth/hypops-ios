//
//  EC2InstanceGeneralViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2InstanceGeneralTableViewController.h"
#import "ScrollableDetailTextCell.h"
#import "EC2HypervisorConnection.h"
#import "EC2ImageBuilder.h"
#import "EC2ImageWrapper.h"

@interface EC2InstanceGeneralTableViewController ()
- (void) configureGeneralDetailsCell:(UITableViewCell *)cell atIndex:(long) index;
- (void) configureAddressingDetailsCell:(UITableViewCell *)cell atIndex:(long) index;
- (void) configureVPCDetailsCell:(UITableViewCell *)cell atIndex:(long) index;
- (void) configureTagsDetailsCell:(UITableViewCell *)cell atIndex:(long) index;
@end

@implementation EC2InstanceGeneralTableViewController

@synthesize hypConnID, instanceId;

-(HypervisorConnectionFactory *) hypervisorConnection{
    return (EC2HypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (EC2InstanceWrapper *) ec2Instance{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"instanceId like [c]'%@'", [self instanceId]]]; 
    NSArray *images = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2INSTANCES withCondition:pred];
    if ([images count] >0){
        return [images objectAtIndex:0];
    }
    return nil;
}


- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withInstance:(EC2InstanceWrapper *)thisInstance{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setInstanceId: [thisInstance instanceId]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
    }
    return self;
    
}

- (void) setId:(NSString*)newID{
    [self setInstanceId:newID];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to vm objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    // could cut this down to just the relevent ones if required
    if (objectType == HYPOBJ_EC2INSTANCES){
        [[self tableView ]reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return EC2INSTANCE_TABLE_COUNT;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // first section is not expandable
    return (section != 0);
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case EC2INSTANCE_GENERAL_TABLE:
            return nil;
            break;
        case EC2INSTANCE_ADDRESSING_TABLE:
            return @"Addressing";
            break;
        case EC2INSTANCE_VPC_TABLE:
            return @"Virtual Private Cloud";
            break;
        case EC2INSTANCE_TAGS_TABLE:
            return @"Tags";
            break;
        default:
            return nil;
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    // Configure the cell...
    // configuration details
    switch (section) {
        case EC2INSTANCE_GENERAL_TABLE:
            return EC2INSTANCE_GENERAL_TABLE_COUNT;
            break;
        case EC2INSTANCE_ADDRESSING_TABLE:
            return EC2INSTANCE_ADDRESSING_TABLE_COUNT;
            break;
        case EC2INSTANCE_VPC_TABLE:
            return EC2INSTANCE_VPC_TABLE_COUNT;
            break;
        case EC2INSTANCE_TAGS_TABLE:{
            EC2InstanceWrapper *instance = [self ec2Instance];
            return [instance tags_count];
            break;
        }
        default:
            break;
    }
    // should never get here
    return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    switch ([indexPath section]) {
        case EC2INSTANCE_GENERAL_TABLE:
            [self configureGeneralDetailsCell:cell atIndex:[indexPath row]];
            break;
        case EC2INSTANCE_ADDRESSING_TABLE:
            [self configureAddressingDetailsCell:cell atIndex:[indexPath row]];
            break;
        case EC2INSTANCE_VPC_TABLE:
            [self configureVPCDetailsCell:cell atIndex:[indexPath row]];
            break;
        case EC2INSTANCE_TAGS_TABLE:
            [self configureTagsDetailsCell:cell atIndex:[indexPath row]];
            break;
        default:
            break;
    }

    return cell;
}

//render the tags details
- (void)configureTagsDetailsCell:(UITableViewCell *)cell atIndex:(long) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    EC2InstanceWrapper *instance = [self ec2Instance];
    EC2Tag *tag = [[instance tags] objectAtIndex:index];
    [[cell textLabel] setText:[tag key]];
    [[cell detailTextLabel] setText: [tag value]];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureGeneralDetailsCell:(UITableViewCell *)cell atIndex:(long) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    EC2InstanceWrapper *instance = [self ec2Instance];
    
    switch (index) {
        case EC2INSTANCE_GENERAL_TABLE_INSTANCETYPE:
        {
            [[cell textLabel] setText:@"type"];
            [[cell detailTextLabel] setText:  [self convertToDisplayString:[instance instanceType]]];
            break;
        }
        case EC2INSTANCE_GENERAL_TABLE_STATE:
        {
            [[cell textLabel] setText:@"State"];
            [[cell detailTextLabel] setText:  [self convertToDisplayString:[[instance state] name]]];
            break;
        }
        case EC2INSTANCE_GENERAL_TABLE_STATEREASON:
        {
            [[cell textLabel] setText:@"Reason"];
            [[cell detailTextLabel] setText:  [self convertToDisplayString:[instance stateTransitionReason]]];
            break;
        }
        case EC2INSTANCE_GENERAL_TABLE_LAUNCHTIME:
        {
            [[cell textLabel] setText:@"Created"];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd MMM yyyy (HH:mm)"];
            [[cell detailTextLabel] setText:[dateFormatter stringFromDate:[instance launchTime]]];
            break;
        }
        case EC2INSTANCE_GENERAL_TABLE_PLATFORM:
        {
            [[cell textLabel] setText:@"Platform"];
            // if no platform specified assume Linux as the API does not return anything better
            if ([instance platform]){
                [[cell detailTextLabel] setText: [NSString stringWithFormat:@"%@ (%@)",  [self convertToDisplayString:[instance platform]],  [self convertToDisplayString:[instance architecture]]]];
            }
            else
            {
                [[cell detailTextLabel] setText: [NSString stringWithFormat:@"Linux (%@)",  [self convertToDisplayString:[instance architecture]]]];
            }
            break;
        }
        case EC2INSTANCE_GENERAL_TABLE_VIRTUALIZATIONTYPE:
        {
            [[cell textLabel] setText:@"virtualization Type"];
            [[cell detailTextLabel] setText: [instance virtualizationType]];
            break;
        }
        case EC2INSTANCE_GENERAL_TABLE_IMAGEID:
        {
            [[cell textLabel] setText:@"image ID"];
            // convert the image ID to an image name
            NSString *displayValue = [instance imageId];
            NSArray *images = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2IMAGES_MACHINE withCondition:[EC2ImageWrapper imageWithReference:displayValue]];

            if ([images count] >0){
                [[cell textLabel] setText:@"image"];
                EC2ImageWrapper *image = [images objectAtIndex:0];
                displayValue = [image name];
            }
            [[cell detailTextLabel] setText: displayValue];
            break;
        }
        case EC2INSTANCE_GENERAL_TABLE_LAUNCHINDEX:
        {
            [[cell textLabel] setText:@"Launch Index"];
            [[cell detailTextLabel] setText: [[instance amiLaunchIndex] stringValue]];
            break;
        }
        case EC2INSTANCE_GENERAL_TABLE_ROOTDEVICE:
        {
            [[cell textLabel] setText:@"Root Device"];
            [[cell detailTextLabel] setText: [NSString stringWithFormat:@"%@ (%@)",  [self convertToDisplayString:[instance rootDeviceName]],  [self convertToDisplayString:[instance rootDeviceType]]]];
            break;
        }

        default:
            break;
    }    
}

- (void)configureAddressingDetailsCell:(UITableViewCell *)cell atIndex:(long) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    EC2InstanceWrapper *instance = [self ec2Instance];
    
    switch (index) {
        case EC2INSTANCE_ADDRESSING_TABLE_PUBLICDNS:
        {
            [[cell textLabel] setText:@"Public DNS"];
            NSString *value =  [self convertToDisplayString:[instance publicDnsName]];
            if (value == nil  || [value isEqualToString: @""]){
                value = @"** No Value **";
            }
            [[cell detailTextLabel] setText: value];
            break;
        }
        case EC2INSTANCE_ADDRESSING_TABLE_PUBLICIP:
        {
            [[cell textLabel] setText:@"Public IP"];
            NSString *value =  [self convertToDisplayString:[instance publicIpAddress]];
            if (value == nil  || [value isEqualToString: @""]){
                value = @"** No Value **";
            }
            [[cell detailTextLabel] setText: value];
            break;
        }
        case EC2INSTANCE_ADDRESSING_TABLE_PRIVATEDNS:
        {
            [[cell textLabel] setText:@"Private DNS"];
            NSString *value =  [self convertToDisplayString:[instance privateDnsName]];
            if (value == nil  || [value isEqualToString: @""]){
                value = @"** No Value **";
            }
            [[cell detailTextLabel] setText: value];
            break;
        }
        case EC2INSTANCE_ADDRESSING_TABLE_PRIVATEIP:
        {
            [[cell textLabel] setText:@"Public IP"];
            NSString *value =  [self convertToDisplayString:[instance privateIpAddress]];
            if (value == nil  || [value isEqualToString: @""]){
                value = @"** No Value **";
            }
            [[cell detailTextLabel] setText: value];
            break;
        }
        default:
            break;
    }    
}

- (void)configureVPCDetailsCell:(UITableViewCell *)cell atIndex:(long) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    EC2InstanceWrapper *instance = [self ec2Instance];
    
    switch (index) {
        case EC2INSTANCE_VPC_TABLE_ID:
        {
            [[cell textLabel] setText:@"VPC ID"];
            NSString *value =  [self convertToDisplayString:[instance vpcId]];
            if (value == nil  || [value isEqualToString: @""]){
                value = @"** No Value **";
            }
            [[cell detailTextLabel] setText: value];
            break;
        }
        case EC2INSTANCE_VPC_TABLE_SUBNET:
        {
            [[cell textLabel] setText:@"Subnet ID"];
            NSString *value =  [self convertToDisplayString:[instance subnetId]];
            if (value == nil  || [value isEqualToString: @""]){
                value = @"** No Value **";
            }
            [[cell detailTextLabel] setText: value];
            break;
        }
        default:
            break;
    }    
}

#pragma mark for RefreshablePage protocol
-(void) refreshPage{
    [[self tableView] reloadData];
}

- (UIImage*) currentTitleImage{
    EC2InstanceWrapper* instance = [self ec2Instance];
    return [EC2ImageBuilder buildInstanceImageForInstance:instance];
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}



@end
