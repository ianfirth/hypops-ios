//
//  EC2InstanceGeneralViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ExpandableTableViewController.h"
#import "EC2HypervisorConnection.h"
#import "EC2InstanceWrapper.h"
#import "Identifyable.h"
#import "RefreshablePage.h"

#define EC2INSTANCE_GENERAL_TABLE 0
#define EC2INSTANCE_ADDRESSING_TABLE 1
#define EC2INSTANCE_VPC_TABLE 2
#define EC2INSTANCE_TAGS_TABLE 3
#define EC2INSTANCE_TABLE_COUNT 4

#define EC2INSTANCE_GENERAL_TABLE_INSTANCETYPE 0
#define EC2INSTANCE_GENERAL_TABLE_STATE 1
#define EC2INSTANCE_GENERAL_TABLE_STATEREASON 2
#define EC2INSTANCE_GENERAL_TABLE_LAUNCHTIME 3
#define EC2INSTANCE_GENERAL_TABLE_PLATFORM 4
#define EC2INSTANCE_GENERAL_TABLE_VIRTUALIZATIONTYPE 5
#define EC2INSTANCE_GENERAL_TABLE_IMAGEID 6
#define EC2INSTANCE_GENERAL_TABLE_LAUNCHINDEX 7
#define EC2INSTANCE_GENERAL_TABLE_ROOTDEVICE 8
#define EC2INSTANCE_GENERAL_TABLE_COUNT 9

#define EC2INSTANCE_ADDRESSING_TABLE_PUBLICDNS 0
#define EC2INSTANCE_ADDRESSING_TABLE_PUBLICIP 1
#define EC2INSTANCE_ADDRESSING_TABLE_PRIVATEDNS 2
#define EC2INSTANCE_ADDRESSING_TABLE_PRIVATEIP 3
#define EC2INSTANCE_ADDRESSING_TABLE_COUNT 4

#define EC2INSTANCE_VPC_TABLE_SUBNET 0
#define EC2INSTANCE_VPC_TABLE_ID 1
#define EC2INSTANCE_VPC_TABLE_COUNT 2


@interface EC2InstanceGeneralTableViewController : ExpandableTableViewController
                <HypervisorConnectionDelegate,Identifyable, RefreshablePage>{
    HypervisorType hypervisorType;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withInstance:(EC2InstanceWrapper *)thisInstance;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

// this is used by relection in the tab contol t o update the page.  In this class it updates the instanceId.
- (void) setId:(NSString*)newID;

@property (copy) NSString *hypConnID;
@property (copy) NSString *instanceId;

@end
