//
//  PowerControlViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "PowerControlViewController.h"

@interface PowerControlViewController ()

@end

@implementation PowerControlViewController

- (id)init
{
    self = [super initWithNibName:@"PowerControlView" bundle:nil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self backgroundView] setStartColor: [UIColor colorWithRed:0 green:0 blue:0 alpha:0]];
    [[self backgroundView] setEndColor: [UIColor colorWithRed:0 green:0 blue:0 alpha:1]];
    
    [[self backgroundView] setGradientViewFillMode:GradientViewFillModeFull];
    [[self backgroundView] setUseRadial:NO];
    [[self backgroundView] setNeedsDisplay];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
