//
//  EC2InstanceByTagKeyValueViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2InstanceByTagKeyValueViewController.h"
#import "EC2ImageBuilder.h"
#import "ImageBuilder.h"
#import "EC2InstanceListViewController.h"
#import "EC2InstanceWrapper.h"
#import "CommonUIStaticHelper.h"
#import "EC2InstanceGeneralTableViewController.h"
#import "TitledDetailViewController.h"
#import "EC2ImageWrapper.h"
#import "EC2ImageGeneralTableViewController.h"
#import "EC2InstancePowerTableViewController.h"
#import "ConnectionObjectUITableViewCell.h"

@interface EC2InstanceByTagKeyValueViewController (){
        UITabBarController *tabBarController;
}
   - (void) reloadTableData;
   -(NSPredicate *) getInstancePredicateForTagKeyValue:(NSString *) ec2ImagePlatform;
@end

@implementation EC2InstanceByTagKeyValueViewController
@synthesize ec2TagValues, tagKey, hypConnID ,rootPredicate;

#pragma mark -
#pragma mark View lifecycle

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withKey:(NSString *)key andPredicate:(NSPredicate *)predicate{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        [self setTagKey:key];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
        [self setRootPredicate:predicate];
        [self setHypConnID:[hypervisorConnection connectionID]];
        // always enough room in portrait so only enable scroll if pull to refresh available
        [[self tableView] setScrollEnabled:YES];
        NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:5];
        // build each platform
        NSArray *instances = [hypervisorConnection hypObjectsForType:HYPOBJ_EC2INSTANCES withCondition:predicate]; 
        for (EC2InstanceWrapper *instance in instances) {
            NSArray *tags = [instance tags];  
            if (tags){
                for (EC2Tag *tag in tags){
                    if ([[self tagKey] isEqualToString:[tag key]]){
                        if ([tag value] == nil){
                            if (![tempArray containsObject:[NSNull null]]){
                                [tempArray addObject:[NSNull null]];
                            }
                        }
                        else{
                            if (![tempArray containsObject:[tag value]]){
                                [tempArray addObject:[tag value]];
                            }
                        }
                    }
                }
            }
        }
        NSArray *c = [tempArray copy];
        [self setEc2TagValues:c];
    }
    return self;
}

- (HypervisorConnectionFactory*) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (void) dealloc{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self tableView] registerNib:[UINib nibWithNibName:@"ConnectionObjectUITableViewCell" bundle:nil] forCellReuseIdentifier:@"ConnectionObjectUITableViewCell"];
}

// render the text in the cell
- (void)configureCell:(ConnectionObjectUITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSString *displayText;
    NSString *ec2InstanceTagValue;
    NSString* detailText;
    
    id object = [[self ec2TagValues] objectAtIndex:indexPath.row];
    if ([object isKindOfClass:[NSNull class]]){
        ec2InstanceTagValue = nil;
        displayText = @"** No Value **";
    }
    else{
        ec2InstanceTagValue = (NSString *)[[self ec2TagValues] objectAtIndex:indexPath.row];   
        displayText = ec2InstanceTagValue;
    } 

    NSPredicate *finalPredicate = [self getInstancePredicateForTagKeyValue:ec2InstanceTagValue];NSUInteger  objectCount = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2INSTANCES withCondition:finalPredicate] count];
        
    [[cell imageView2] setImage:[ImageBuilder vMTaggedImage]];

    if (objectCount != 1)
    {
        detailText = [NSString stringWithFormat:@"%lu",(unsigned long)objectCount];
    }
    else if (objectCount == 1){
        NSArray *instances = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2INSTANCES withCondition:finalPredicate];
        EC2InstanceWrapper *instance = [instances objectAtIndex:0];
        [[cell imageView2] setImage:[EC2ImageBuilder buildInstanceImageForInstance:instance]];
    }
        
    // set the selection style and the accessory type
    if (objectCount >0){
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    } else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    // set the label text
    [[cell textLabel2] setText:displayText];
    
    // set the detail text
    if (detailText != nil) {
        [[cell detailTextLabel2] setText:detailText];
    }
    
    // set the number of objects into the tag
    // this is used to work out if should display the instance directly ot a list of them
    [cell setTag:objectCount];
    
    // make every other row use alternate color
    [cell setAlternate:indexPath.row %2];
    [[cell refreshButton] setHidden:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self ec2TagValues] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"ConnectionObjectUITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ConnectionObjectUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:(ConnectionObjectUITableViewCell*)cell atIndexPath:indexPath];
    
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

// when selecting the object show the actaul images that meet the defined predicates
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *ec2InstanceTagValue;
    id object = [[self ec2TagValues] objectAtIndex:indexPath.row];
    if ([object isKindOfClass:[NSNull class]]){
        ec2InstanceTagValue = nil;
    }
    else{
        ec2InstanceTagValue = (NSString *)[[self ec2TagValues] objectAtIndex:indexPath.row];   
    } 
    NSPredicate *finalPredicate = [self getInstancePredicateForTagKeyValue:ec2InstanceTagValue];
    
    NSArray *instances = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2INSTANCES withCondition:finalPredicate];

   if ([instances count] > 1){
        // display the list seeing as there are more than one
        EC2InstanceListViewController *imageListViewController = [[EC2InstanceListViewController alloc] initWithHypervisorConnection:[self hypervisorConnection] predicate:finalPredicate];
    
        [imageListViewController setTitle:ec2InstanceTagValue];
        [self.navigationController pushViewController:imageListViewController animated:YES];
    }
    else{
        // display the instance since there is only one
        EC2InstanceWrapper *instance = [instances objectAtIndex:0];
        // actually construct a new one here

        UIViewController *c = [self instanceViewControllerForHypervisorConnection:[self hypervisorConnection] EC2Instance:instance];
 
        [CommonUIStaticHelper displayNextView:c usingNavigationController:[self navigationController] andRootViewController:[[[self navigationController] viewControllers]objectAtIndex:0]];
    }
}


-(UITabBarController *) instanceViewControllerForHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection EC2Instance:(EC2InstanceWrapper *)instance{
    EC2InstanceWrapper *instanceForDisplay = instance;
    NSPredicate *instancPred = [EC2InstanceWrapper instanceWithReference:[instanceForDisplay instanceId]];
    UIImage* instanceImage = [EC2ImageBuilder buildInstanceImageForInstance:instanceForDisplay];
    
    if (!tabBarController){
        tabBarController = [[UITabBarController alloc] init];
        NSMutableArray *localControllersArray = [[NSMutableArray alloc] initWithCapacity:2];
        
        
        
        
        // add the general tab
        EC2InstanceGeneralTableViewController *tbc = [[EC2InstanceGeneralTableViewController alloc] initWithhypervisorConnection: hypervisorConnection
                                                                                                          withInstance:instanceForDisplay];
        
        
        
        
        TitledDetailViewController *instanceGeneral = [[TitledDetailViewController alloc] initWithHypervisorConnection:hypervisorConnection 
                                                                                                         rootObjectRef:[instanceForDisplay instanceId]
                                                                                                   rootObjectPredicate:instancPred
                                                                                                         hypObjectType:HYPOBJ_EC2INSTANCES
                                                                                                      namePropertyName:@"instanceId"
                                                                                               descriptionPropertyName:nil
                                                                                                           displayMode:DISPLAYMODE_NAME
                                                                                                               tabName:@"General" 
                                                                                                       tabBarImageName:@"tab_General" 
                                                                                                                 image:instanceImage
                                                                                               extentionViewController:tbc];    
        
        [localControllersArray addObject:instanceGeneral];
        
        
        // add the ramdisk tab
        if ([instanceForDisplay ramDiskId]){
            NSPredicate *ramdiskPred = [EC2ImageWrapper imageWithReference:[instanceForDisplay ramDiskId]];
            EC2ImageGeneralTableViewController *tbcRamdisk = [[EC2ImageGeneralTableViewController alloc] initWithhypervisorConnection: hypervisorConnection
                                                                                                                   withImageReference:[instanceForDisplay ramDiskId]
                                                                                                                            imageType:EC2_IMAGETYPE_RAMDISK];
            
            TitledDetailViewController *instanceRamdisk = [[TitledDetailViewController alloc] initWithHypervisorConnection:hypervisorConnection 
                                                                                                             rootObjectRef:[instanceForDisplay ramDiskId]
                                                                                                       rootObjectPredicate:ramdiskPred
                                                                                                             hypObjectType:HYPOBJ_EC2IMAGES_RAMDISK
                                                                                                          namePropertyName:@"name"
                                                                                                   descriptionPropertyName:nil
                                                                                                               displayMode:DISPLAYMODE_NAME
                                                                                                                   tabName:@"Ram Disk" 
                                                                                                           tabBarImageName:@"tab_Storage" 
                                                                                                                     image:instanceImage
                                                                                                   extentionViewController:tbcRamdisk];    
            
            
            [localControllersArray addObject:instanceRamdisk];
        }
        
        // add the kernel tab
        NSPredicate *kernelPred = [EC2ImageWrapper imageWithReference:[instanceForDisplay kernelId]];
        EC2ImageGeneralTableViewController *tbckernel = [[EC2ImageGeneralTableViewController alloc] initWithhypervisorConnection: hypervisorConnection
                                                                                                              withImageReference:[instanceForDisplay kernelId]
                                                                                                                       imageType:EC2_IMAGETYPE_KERNEL];
        
        
        TitledDetailViewController *instanceKernel = [[TitledDetailViewController alloc] initWithHypervisorConnection:hypervisorConnection 
                                                                                                        rootObjectRef:[instanceForDisplay kernelId]
                                                                                                  rootObjectPredicate:kernelPred
                                                                                                        hypObjectType:HYPOBJ_EC2IMAGES_KERNEL
                                                                                                     namePropertyName:@"name"
                                                                                              descriptionPropertyName:nil
                                                                                                          displayMode:DISPLAYMODE_NAME
                                                                                                              tabName:@"Kernel" 
                                                                                                      tabBarImageName:@"tab_Storage" 
                                                                                                                image:instanceImage
                                                                                              extentionViewController:tbckernel];    
        
        
        [localControllersArray addObject:instanceKernel];
        
        // add the power tab
        EC2InstancePowerTableViewController *powerControl = [[EC2InstancePowerTableViewController alloc] initWithhypervisorConnection: (EC2HypervisorConnection*)hypervisorConnection
                                                                                                               withInstance:instanceForDisplay];
        
        
        
        
        TitledDetailViewController *instancePower = [[TitledDetailViewController alloc] initWithHypervisorConnection:hypervisorConnection
                                                                                                       rootObjectRef:[instanceForDisplay instanceId]
                                                                                                 rootObjectPredicate:instancPred
                                                                                                       hypObjectType:HYPOBJ_EC2INSTANCES
                                                                                                    namePropertyName:@"instanceId"
                                                                                             descriptionPropertyName:nil
                                                                                                         displayMode:DISPLAYMODE_NAME
                                                                                                             tabName:@"Control"
                                                                                                     tabBarImageName:@"tab_Power"
                                                                                                               image:instanceImage
                                                                                             extentionViewController:powerControl];
        
        [localControllersArray addObject:instancePower];
        
        tabBarController.viewControllers = localControllersArray;
        
        [tabBarController setTitle:@"Instance"];
    }
    else {
        for (TitledDetailViewController* view in [tabBarController viewControllers]){
            // this should be the image general and it should refresh the children ones.
            [view setImage:instanceImage];
            [view setRootObjectPredicate:instancPred andReference:[instanceForDisplay instanceId]];    
        }
    }
    return tabBarController;
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}


#pragma mark -
#pragma mark Private methods

-(void) reloadTableData{
    [[self tableView] reloadData];
}

-(NSPredicate *) getInstancePredicateForTagKeyValue:(NSString *) ec2InstanceTagValue{
    NSPredicate *tagPredicate = [EC2InstanceWrapper Instances_WithTagKey:[self tagKey] andValue:ec2InstanceTagValue];
    
    NSPredicate *finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:tagPredicate,[self rootPredicate], nil]];
    return finalPredicate;
}


#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {
    if (![[self hypervisorConnection] isUpdatePendingForType:HYPOBJ_EC2INSTANCES]){
        [[self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_EC2INSTANCES];
    }
    // the call to stopLoading is on the data update received.
}


#pragma mark -
#pragma mark HypervisorConnection protocol implementation

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | HYPOBJ_EC2IMAGES_MACHINE) >0){
        [self reloadTableData];
        [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
    }
}


@end

