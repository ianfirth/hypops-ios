//
//  EC2InstanceListViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2InstanceListViewController.h"
#import "EC2ImageBuilder.h"
#import "TitledDetailViewController.h"
#import "RootViewController_ipad.h"
#import "EC2InstanceWrapper.h"
#import "EC2ImageWrapper.h"
#import "EC2HypervisorConnection.h"
#import "EC2InstanceGeneralViewController.h"
#import "EC2ImageGeneralViewController.h"
#import "EC2InstancePowerViewController.h"
#import "CommonUIStaticHelper.h"
#import "MultiLineDetailUITableViewCell.h"

#define ALERT_START 100
#define ALERT_STOP 200
#define ALERT_TERMINATE 300

@interface EC2InstanceListViewController (){
    NSArray* sortedObjects;
    UITabBarController *tabBarController;
    PowerControlViewController* powerViewController;
    PullRefreshTableViewController *tableViewController;
    NSTimer* refreshTimer;
}

-(NSPredicate *) getFinalPredicate;
- (void) configureCell:(MultiLineDetailUITableViewCell *)cell forObject:(EC2InstanceWrapper *)instanceObject atIndexPath:(NSIndexPath*)indexPath;
-(void) reloadTableData;
@end

@implementation EC2InstanceListViewController
@synthesize hypConnID,rootPredicate;

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection predicate:(NSPredicate *) predicate{
    self = [super initWithNibName:@"EC2InstanceListView" bundle:nil ];
    if (self){
        tableViewController = [[PullRefreshTableViewController alloc] initWithStyle:UITableViewStylePlain];
        powerViewController = [[PowerControlViewController alloc] init];
        [tableViewController setPullControllerDelegate:self];
        [self setHypConnID:[hypervisorConnection connectionID]];
        [self setRootPredicate:predicate];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
        powerViewController = [[PowerControlViewController alloc] init];
    }
    
    return self;
}

-(void) dealloc{
    [self startAutoUpdate];
    powerViewController = nil;
    tableViewController = nil;
}

- (HypervisorConnectionFactory*) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

#pragma mark -
#pragma mark View Search Bar

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    // only show the status bar’s cancel button while in edit mode
    searchBar.showsCancelButton = YES;
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    // flush the previous search content
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
}

-(void) reloadTableData{
    [[tableViewController tableView] reloadData];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
    
    if([searchText isEqualToString:@""] || searchText == nil){
        [[tableViewController tableView] reloadData];
        return;
    }
    
    searchPredicate = [EC2InstanceWrapper instanceIDBeginsOrContainsAWordBeginningWith:searchText];
    [[tableViewController tableView] reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
    [[tableViewController tableView] reloadData];
    
    [searchBar resignFirstResponder];
    searchBar.text = @"";
}

// called when Search (in our case “Done”) button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void) autoUpdate:(NSTimer *) theTimer {
   [(EC2HypervisorConnection*) [self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_EC2INSTANCES withEC2Objects: [self sortedInstances]];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
   
    [super viewDidLoad];
    
    [[tableViewController tableView] registerNib:[UINib nibWithNibName:@"MultiLineDetailUITableViewCell" bundle:nil] forCellReuseIdentifier:@"MultiLineDetailUITableViewCell"];
    
    UISearchBar *theSearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0,320,40)];
    theSearchBar.delegate = self;
    [self.view addSubview:theSearchBar];
    [[tableViewController tableView] setTableHeaderView:theSearchBar];
  
    [[tableViewController tableView] setDataSource:self];
    [[self instancesView] addSubview:[tableViewController tableView]];
    
    [[self powerContollerView] addSubview:[powerViewController view]];

    [[powerViewController startButton] addTarget:self
                                          action:@selector(startInstances:)
                   forControlEvents:UIControlEventTouchUpInside];

    [[powerViewController stopButton] addTarget:self
                                         action:@selector(stopInstances:)
                                forControlEvents:UIControlEventTouchUpInside];
   
    [[powerViewController terminateButton] addTarget:self
                                         action:@selector(terminateInstances:)
                               forControlEvents:UIControlEventTouchUpInside];
}

/** 
 * Send the start instances requrest with all the instances in the current list
 *  Some of these might fail depending on instance type
 */
-(void)startInstances:(id) sender{
    NSLog(@"start instances");
    NSArray* instances = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2INSTANCES withCondition:[self getFinalPredicate]];
    NSString* message = [NSString stringWithFormat:@"This will start %lu instances.\nDo you want to continue?", (unsigned long)[instances count] ];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Start Instances" message:message
												   delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert setTag:ALERT_START];
    [alert show];
}

// send the stop instances requrest with all the instances in the current list
-(void)stopInstances:(id) sender{
    NSLog(@"stop instances");
    NSArray* instances = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2INSTANCES withCondition:[self getFinalPredicate]];
    NSString* message = [NSString stringWithFormat:@"This will stop %lu instances.\nInstances that cannot be stopped will remain in their current state. These will need to be terminated separately if required.\nDo you want to continue?", (unsigned long)[instances count] ];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Stop Instances" message:message
												   delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert setTag:ALERT_STOP];
    [alert show];
}

// send the stop instances requrest with all the instances in the current list
-(void)terminateInstances:(id) sender{
    NSLog(@"terminate instances");
    NSArray* instances = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2INSTANCES withCondition:[self getFinalPredicate]];
    NSString* message = [NSString stringWithFormat:@"This will terminate %lu instances.\nDo you want to continue?", (unsigned long)[instances count] ];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Terminate Instances" message:message
												   delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert setTag:ALERT_TERMINATE];
    [alert show];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    sortedObjects = nil;
    [super viewWillAppear:animated];
    // enable the screen protection by proximity
    [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
    [[tableViewController tableView] setFrame:[[self instancesView] bounds]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
    [self startAutoUpdate];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    // enable the screen protection by proximity
    [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
    [self stopAutoUpdate];
    sortedObjects = nil;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

#pragma mark -
#pragma mark UIAlertViewDelegate 
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSArray* instances = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2INSTANCES withCondition:[self getFinalPredicate]];
    NSError* error;
    if (buttonIndex == 1){
        switch ([alertView tag]) {
            case ALERT_START:
                error = [(EC2HypervisorConnection*)[self hypervisorConnection] startInstances:[NSMutableArray arrayWithArray:instances] ignoreTerminated:YES];
                break;
            case ALERT_STOP:
                error = [(EC2HypervisorConnection*)[self hypervisorConnection] stopInstances:[NSMutableArray arrayWithArray:instances] ignoreTerminated:YES];
                break;
            case ALERT_TERMINATE:
                error = [(EC2HypervisorConnection*)[self hypervisorConnection] terminateInstances:[NSMutableArray arrayWithArray:instances]];
                break;
            default:
                break;
        }
    }
    if (error){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[error domain] message:[error localizedDescription]
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert setTag:0];
        [alert show];
    }
}

#pragma mark - 
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void) configureCell:(MultiLineDetailUITableViewCell *)cell forObject:(EC2InstanceWrapper *)instanceObject atIndexPath:(NSIndexPath*)indexPath{
    // set the lable text for the cell
    [[cell textLabel2] setText:[instanceObject instanceId]];
    // set the image for the cell
    [[cell imageView2] setImage:[EC2ImageBuilder buildInstanceImageForInstance:instanceObject]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    [cell setAlternate:(indexPath.row % 2)];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
    return [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2INSTANCES withCondition:[self getFinalPredicate]] count];
}

/**
 * returns the list of images for the view in the correctly sorted order
 */
- (NSArray *) sortedInstances {
    if (!sortedObjects){
        NSArray *unsortedObjects = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2INSTANCES withCondition:[self getFinalPredicate]];
        
        SEL compareByInstanceIdSelector = sel_registerName("compareByInstanceId:");
        sortedObjects = [unsortedObjects sortedArrayUsingSelector:compareByInstanceIdSelector];
    }
    return sortedObjects;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MultiLineDetailUITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MultiLineDetailUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    [self configureCell:(MultiLineDetailUITableViewCell*)cell forObject:[[self sortedInstances] objectAtIndex:[indexPath row]] atIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 68;
}

#pragma mark - public helper methods

-(UITabBarController *) instanceViewControllerForHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection EC2Instance:(EC2InstanceWrapper *)instance{
    EC2InstanceWrapper *instanceForDisplay = instance;
    
    //    if (!tabBarController){
    tabBarController = [[UITabBarController alloc] init];
    NSMutableArray *localControllersArray = [[NSMutableArray alloc] initWithCapacity:2];
    
    
    
    
    // add the general tab
    EC2InstanceGeneralViewController *instanceGeneral = [[EC2InstanceGeneralViewController alloc] initWithhypervisorConnection: hypervisorConnection
                                                                                                                  withInstance:instanceForDisplay];
    
    
    [localControllersArray addObject:instanceGeneral];
    
    
    // add the ramdisk tab
    if ([instanceForDisplay ramDiskId]){
        NSPredicate *ramDiskPred = [EC2ImageWrapper imageWithReference:[instanceForDisplay ramDiskId]];
        
        NSArray* ramdisks = [hypervisorConnection hypObjectsForType:EC2_IMAGETYPE_RAMDISK withCondition:ramDiskPred];
        
        if ([ramdisks count] > 0)
        {
            EC2ImageGeneralViewController *instanceRamdisk = [[EC2ImageGeneralViewController alloc] initWithhypervisorConnection: hypervisorConnection withImage:[ramdisks objectAtIndex:0] tabName:@"Ram Disk" andTabImageName:@"tab_Storage"];
            
            [localControllersArray addObject:instanceRamdisk];
            
        }
        
    }
    
    // add the kernel tab
    if ([instanceForDisplay kernelId]){
        NSPredicate *kernelDiskPred = [EC2ImageWrapper imageWithReference:[instanceForDisplay kernelId]];
        
        NSArray* kernelDisks = [hypervisorConnection hypObjectsForType:EC2_IMAGETYPE_KERNEL withCondition:kernelDiskPred];
        
        if ([kernelDisks count] > 0){
            EC2ImageGeneralViewController *instanceKernel = [[EC2ImageGeneralViewController alloc] initWithhypervisorConnection: hypervisorConnection withImage:[kernelDisks objectAtIndex:0]];
            
            
            [localControllersArray addObject:instanceKernel];
        }
    }
    // add the power tab
    EC2InstancePowerViewController *powerControl = [[EC2InstancePowerViewController alloc] initWithhypervisorConnection: (EC2HypervisorConnection*)hypervisorConnection
                                                                                                           withInstance:instanceForDisplay];
    
    [localControllersArray addObject:powerControl];
    
    tabBarController.viewControllers = localControllersArray;
    
    [tabBarController setTitle:@"Instance"];
    
    return tabBarController;
}

#pragma mark - private methods
// combines the search predicate and the root predicate
-(NSPredicate *) getFinalPredicate{
    NSPredicate *finalPredicate = [self rootPredicate];
    
    if (searchPredicate != nil){
        finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:searchPredicate, [self rootPredicate], nil]];
        
    }
    return finalPredicate;
}

#pragma mark -
#pragma mark PullRefreshTableViewController delegate

- (void)refreshData {
    // restart the autoUpdate timer.
    [self startAutoUpdate];
    sortedObjects = nil;
    if (![[self hypervisorConnection] isUpdatePendingForType:HYPOBJ_EC2INSTANCES]){
        [[self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_EC2INSTANCES];
    }
    // the call to stopLoading is on the data update received.
}

// override, and only add the header if not in auto refresh mode
- (bool) isActive {
    return (![[self hypervisorConnection] isAutoUpdateEnabled]);
}

#pragma mark - PullRefreshTableViewControllerDelegate (extends the tableViewDelegate)

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITabBarController *ilvc = [self instanceViewControllerForHypervisorConnection:[self hypervisorConnection] EC2Instance:[[self sortedInstances] objectAtIndex:[indexPath row]]];
    
    [CommonUIStaticHelper displayNextView:ilvc usingNavigationController:[self navigationController] andRootViewController:[[[self navigationController] viewControllers]objectAtIndex:0]];
}

-(void) stopAutoUpdate{
    if (refreshTimer){
        [refreshTimer invalidate];
        refreshTimer = nil;
    }
}

-(void) startAutoUpdate{
    [self stopAutoUpdate];
    refreshTimer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(autoUpdate:)  userInfo:nil repeats:YES];
}


#pragma mark -
#pragma mark HypervisorConnection protocol implementation
- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | HYPOBJ_EC2INSTANCES) >0){
        sortedObjects = nil;
        [self reloadTableData];
        if (![[self hypervisorConnection] isAutoUpdateEnabled]){
            [tableViewController performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
        }
    }
}

@end
