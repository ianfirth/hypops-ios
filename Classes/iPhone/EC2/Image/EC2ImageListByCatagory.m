//
//  EC2ImageListByCatagory.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ImageBuilder.h"
#import "EC2ImageListByCatagory.h"
#import "EC2ImageListByPlatform.h"
#import "EC2ImageListByTagKeyViewController.h"
#import "RootViewController_ipad.h"
#import "EC2EBSDetailView.h"
#import "EC2ImageWrapper.h"
#import "ConnectionObjectUITableViewCell.h"

@interface EC2ImageListByCatagory ()
- (NSNumber *) ec2ImageCatagoryAtIndex:(long) index;
- (void) reloadTableData;
@end
 
@implementation EC2ImageListByCatagory
@synthesize ec2ImageCatagories, hypConnID;

#pragma mark -
#pragma mark View lifecycle

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        [self setHypConnID:[hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
        // always enough room in portrait so only enable scroll if pull to refresh available
        [[self tableView] setScrollEnabled:YES];
    }
    return self;
}

- (HypervisorConnectionFactory*) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (void)viewDidLoad {
    [self setEc2ImageCatagories: [NSDictionary dictionaryWithObjectsAndKeys:
                               [NSNull null], [NSNumber numberWithInt:EC2_IMAGE_ALL],
                               [EC2ImageWrapper Images_EBS],[NSNumber numberWithInt:EC2_IMAGE_EBS],
                               [EC2ImageWrapper Images_Tagged],[NSNumber numberWithInt:EC2_IMAGE_TAGGED],
                               [EC2ImageWrapper Images_Amazon],[NSNumber numberWithInt:EC2_IMAGE_AMAZON],
                               [EC2ImageWrapper Images_Public],[NSNumber numberWithInt:EC2_IMAGE_PUBLIC],
                               [EC2ImageWrapper Images_Private],[NSNumber numberWithInt:EC2_IMAGE_PRIVATE],
                               [EC2ImageWrapper Images_32Bit],[NSNumber numberWithInt:EC2_IMAGE_32BIT],
                               [EC2ImageWrapper Images_64Bit],[NSNumber numberWithInt:EC2_IMAGE_64BIT],
                               [EC2ImageWrapper Images_OwnedByMe:[[self hypervisorConnection] connectionUserName]],[NSNumber numberWithInt:EC2_IMAGE_OWNEDBYME],
                               nil]];
    
    [super viewDidLoad];
    [[self tableView] registerNib:[UINib nibWithNibName:@"ConnectionObjectUITableViewCell" bundle:nil] forCellReuseIdentifier:@"ConnectionObjectUITableViewCell"];
    self.title = @"AMI Catagory";
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void) viewDidUnload{
    [self setEc2ImageCatagories:nil];
    [super viewDidUnload];
}

-(void) reloadTableData{
    [[self tableView] reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(ConnectionObjectUITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSNumber *ec2ImageCatagory = [self ec2ImageCatagoryAtIndex:indexPath.row];   
    
    NSString *displayText;
    long objectCount = 0;
    BOOL validCatagory = YES;
 
  
    switch ([ec2ImageCatagory intValue]) {
        case EC2_IMAGE_ALL:
            displayText = @"All Images";
            [[cell imageView2] setImage:[ImageBuilder vMImage]];
            break;
        case EC2_IMAGE_TAGGED:
            displayText = @"Tagged Images";
            [[cell imageView2] setImage:[ImageBuilder vMTaggedImage]];
            break;
        case EC2_IMAGE_AMAZON:
            displayText = @"Amazon Images";
            [[cell imageView2] setImage:[ImageBuilder vMImage]];
            break;
        case EC2_IMAGE_PUBLIC:
            displayText = @"Public Images";
            [[cell imageView2] setImage:[ImageBuilder vMImage]];
            break;
        case EC2_IMAGE_PRIVATE:
            displayText = @"Private Images";
            [[cell imageView2] setImage:[ImageBuilder vMImage]];
            break;
        case EC2_IMAGE_32BIT:
            displayText = @"32 bit Images";
            [[cell imageView2] setImage:[ImageBuilder vMImage]];
            break;
        case EC2_IMAGE_64BIT:
            displayText = @"64 bit Images";
            [[cell imageView2] setImage:[ImageBuilder vMImage]];
            break;
        case EC2_IMAGE_EBS:
            displayText = @"EBS Images";
            [[cell imageView2] setImage:[ImageBuilder vMImage]];
            break;
        case EC2_IMAGE_OWNEDBYME:
            displayText = @"Images I own";
            [[cell imageView2] setImage:[ImageBuilder vMImage]];
            break;
        default:
            validCatagory = NO;
            break;
    }
    
    if (validCatagory){        
        NSString *detailText;
        NSObject *predicate = [[self ec2ImageCatagories] objectForKey:ec2ImageCatagory];
        if ([predicate isKindOfClass:[NSNull class]]){
            objectCount = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2IMAGES_MACHINE] count];
        } else {
            objectCount = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2IMAGES_MACHINE withCondition:(NSPredicate *)predicate] count];
        }
        detailText = [NSString stringWithFormat:@"%lu",objectCount];
        
        // set the selection style and the accessory type
        if (objectCount >0){
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        } else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        
        // set the label text
        [[cell textLabel2] setText:displayText];
        
        // set the detail text
        if (detailText != nil) {
            [[cell detailTextLabel2] setText:detailText];
        }
    }        
    else {
        NSLog(@"Invalid cell catagory provided %@",ec2ImageCatagory);
    }
    
    // make every other row use alternate color
    [cell setAlternate:indexPath.row %2];
    [[cell refreshButton] setHidden:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}



#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self ec2ImageCatagories] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"ConnectionObjectUITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ConnectionObjectUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:(ConnectionObjectUITableViewCell*)cell atIndexPath:indexPath];
    
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    // if their are no objects for this cell then the disclosure indicator will not be set
    // if this the case then dont allow selection as there is no point.
    if (![[tableView cellForRowAtIndexPath:indexPath] accessoryType] == UITableViewCellAccessoryDisclosureIndicator){
        return;
    }
        
    NSNumber *ec2ImageCatagory = [self ec2ImageCatagoryAtIndex:indexPath.row];  
    NSPredicate *predicate = [[self ec2ImageCatagories] objectForKey:ec2ImageCatagory];
    
    if ([predicate isEqual:[NSNull null]]){
        predicate = nil;
    }

    UITableViewController *nextController = nil;
    UIViewController *detailViewForIpad = nil;
    
    switch ([ec2ImageCatagory intValue]) {
        case EC2_IMAGE_ALL:
            nextController = [[EC2ImageListByPlatform alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate];
            [nextController setTitle:@"All Images"];
            break;
        case EC2_IMAGE_TAGGED:
            nextController = [[EC2ImageListByTagKeyViewController alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate];
            [nextController setTitle:@"Tagged Images"];
            break;
        case EC2_IMAGE_AMAZON:
            nextController = [[EC2ImageListByPlatform alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate];
            [nextController setTitle:@"Amazon Images"];
            break;
        case EC2_IMAGE_PUBLIC:
            nextController = [[EC2ImageListByPlatform alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate];
            [nextController setTitle:@"Public Images"];
            break;
        case EC2_IMAGE_PRIVATE:
            nextController = [[EC2ImageListByPlatform alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate];
            [nextController setTitle:@"Private Images"];
            break;
        case EC2_IMAGE_32BIT:
            nextController = [[EC2ImageListByPlatform alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate];
            [nextController setTitle:@"32 Bit Images"];
            break;
        case EC2_IMAGE_64BIT:
            nextController = [[EC2ImageListByPlatform alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate];
            [nextController setTitle:@"64 Bit Images"];
            break;
        case EC2_IMAGE_EBS:
            nextController = [[EC2ImageListByPlatform alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate];
            [nextController setTitle:@"EBS Images"];
            detailViewForIpad = [[EC2EBSDetailView alloc] initWithNibName:@"Ec2EBSDetailView" bundle:nil];
            break;
        case EC2_IMAGE_OWNEDBYME:
            nextController = [[EC2ImageListByPlatform alloc] initWithHypervisorConnection:[self hypervisorConnection] withPredicate:predicate];
            [nextController setTitle:@"My Images"];
            break;
        default:
            break;
    }

    if (nextController){
        [self.navigationController pushViewController:nextController animated:YES];
    }
    
    // if its an ipad display details about the catagory selected
    if (detailViewForIpad){
        UINavigationController *navCon = [self navigationController];
        UIViewController *rootController = [[navCon viewControllers] objectAtIndex:0];
        
        // use detail view for ipad
        if ([rootController respondsToSelector:@selector(detailViewNavigationController)]){
            UINavigationController *dvController = [rootController performSelector:@selector(detailViewNavigationController)];
            [RootViewController_ipad setDetailViewRootController:detailViewForIpad onNavigationController:dvController];
        }
    }
    
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark -
#pragma mark Private methods
/*
 * return the catagory from the index specified.  This ensures that the list is provided in numerical order
 */
- (NSNumber *) ec2ImageCatagoryAtIndex:(long) index{
    NSSortDescriptor *numberDescriptor = [[NSSortDescriptor alloc] initWithKey:@"intValue" ascending:YES];
    NSArray *keys = [[[self ec2ImageCatagories] allKeys] sortedArrayUsingDescriptors:[NSArray arrayWithObject:numberDescriptor]];
    NSNumber *ec2ImageCatagory = [keys objectAtIndex:index];
    return ec2ImageCatagory;
}

#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {    
    if (![[self hypervisorConnection] isUpdatePendingForType:HYPOBJ_EC2IMAGES_MACHINE]){
        [[self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_EC2IMAGES_MACHINE];
    }
    // the call to stopLoading is on the data update received.
}


#pragma mark -
#pragma mark HypervisorConnection protocol implementation

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | HYPOBJ_EC2IMAGES_MACHINE) >0){
        [self reloadTableData];
        [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
    }
}


@end

