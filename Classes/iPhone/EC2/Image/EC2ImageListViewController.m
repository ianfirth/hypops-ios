//
//  EC2ImageListViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2ImageListViewController.h"
#import "ImageBuilder.h"
#import "TitledDetailViewController.h"
#import "RootViewController_ipad.h"
#import "EC2ImageWrapper.h"
#import "EC2ImageGeneralViewController.h"
#import "CommonUIStaticHelper.h"
#import "MultiLineDetailUITableViewCell.h"
#import "ProgressAlert.h"

@interface EC2ImageListViewController (){
    UITabBarController *tabBarController;
    NSMutableDictionary *sortedObjectsDictionary;
}
-(NSPredicate *) getFinalPredicateForSection:(NSString *) sectionKey;
- (void) configureCell:(MultiLineDetailUITableViewCell *)cell forObject:(EC2ImageWrapper *)imageObject atIndexPath:(NSIndexPath*)indexPath;
   - (void) reloadTableData;
   - (void) buildSectionInformation;
   - (NSArray *) sortedImagesForSection:(long) section;
@end

@implementation EC2ImageListViewController
@synthesize hypConnID,rootPredicate;

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection predicate:(NSPredicate *) predicate{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        [self setHypConnID:[hypervisorConnection connectionID]];
        [self setRootPredicate:predicate];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
    }

    return self;
}

- (HypervisorConnectionFactory*) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (void)dealloc
{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];

    sectionKeys = nil;
}

- (void) buildSectionInformation{
  // run predicaed for each letter of aplphabet and if returns a result then add predicate and letter
  // to dictionary
  // the number of entiries in the dictionary is the number of sections, and the predicate will
  // provide the content.
    if (sectionKeys != nil){
        sectionKeys = nil;
    }
    sectionKeys = [[NSMutableArray alloc] initWithCapacity:10];

    char letter = 'A';
    while (letter <= 'Z'){
        NSString *letterStr = [NSString stringWithFormat:@"%c",letter];
        NSPredicate *pred = [self getFinalPredicateForSection:letterStr];
        NSArray *matches = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2IMAGES_MACHINE withCondition:pred]; 
        if ([matches count] >0){
            // add to dictionary
            [sectionKeys addObject:letterStr];
        }        
        letter ++;
    }

    letter = '0';
    while (letter <= '9'){
        NSString *letterStr = [NSString stringWithFormat:@"%c",letter];
        NSPredicate *pred = [self getFinalPredicateForSection:letterStr];
        NSArray *matches = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2IMAGES_MACHINE withCondition:pred]; 
        if ([matches count] >0){
            // add to dictionary
            [sectionKeys addObject:letterStr];
        }        
        letter ++;
    }

    // add a section for all the rest
    NSString *letterStr = @"";
    NSPredicate *pred = [self getFinalPredicateForSection:letterStr];
    NSArray *matches = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2IMAGES_MACHINE withCondition:pred]; 
    if ([matches count] >0){
        // add to dictionary
        [sectionKeys addObject:letterStr];
    }            

}


-(void) reloadTableData{
    // actually needs to break the things up into alpahbetical sections counts etc.
    // the same as when the thing is constucted.  Perhaps use a common method for this
    [self buildSectionInformation];
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark View Search Bar

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    // only show the status bar’s cancel button while in edit mode
    searchBar.showsCancelButton = YES;
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    // flush the previous search content
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
    
    if([searchText isEqualToString:@""] || searchText == nil){
    [self reloadTableData];
        return;
    }
    
    searchPredicate = [EC2ImageWrapper nameBeginsOrContainsAWordBeginningWith:searchText];
    [self reloadTableData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
    [self reloadTableData];
    
    [searchBar resignFirstResponder];
    searchBar.text = @"";
}

// called when Search (in our case “Done”) button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self tableView] registerNib:[UINib nibWithNibName:@"MultiLineDetailUITableViewCell" bundle:nil] forCellReuseIdentifier:@"MultiLineDetailUITableViewCell"];
    [self buildSectionInformation];
    UISearchBar *theSearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0,320,40)];
    theSearchBar.delegate = self;
    [self.view addSubview:theSearchBar];
    [[self tableView] setTableHeaderView:theSearchBar];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    sortedObjectsDictionary = nil;
    [self buildSectionInformation];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

#pragma mark - Table view data source

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return sectionKeys;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [sectionKeys count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString* sectionStr = [sectionKeys objectAtIndex:section];
    if ([sectionStr isEqualToString: @""]){
        return @"** No Name **";
    }
    return [sectionKeys objectAtIndex:section];
}

- (void) configureCell:(MultiLineDetailUITableViewCell *)cell forObject:(EC2ImageWrapper *)imageObject atIndexPath:(NSIndexPath*)indexPath{
    // set the lable text for the cell
    [[cell textLabel2] setText:[imageObject name]];
    // set the detail text for the cell
    [[cell detailTextLabel2] setText:[imageObject imageLocation]];
    // set the image for the cell
    [[cell imageView2] setImage:[ImageBuilder vMTemplateImage]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    [cell setAlternate:(indexPath.row % 2)];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionKey = [sectionKeys objectAtIndex:section];
    
    NSArray *machineImages = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2IMAGES_MACHINE withCondition:[self getFinalPredicateForSection:sectionKey]];
                               
    return [machineImages count];
}

/**
 * returns the list of images for the view in the correctly sorted order
 */
- (NSArray *) sortedImagesForSection:(long) section {
    // only sort once :)
    if (!sortedObjectsDictionary){
        sortedObjectsDictionary = [NSMutableDictionary dictionaryWithCapacity:1];
    }
    NSString *sectionKey = [sectionKeys objectAtIndex:section];
    
    if (![sortedObjectsDictionary valueForKey:sectionKey]){
        NSString *sectionKey = [sectionKeys objectAtIndex:section];
        
        NSArray *unsortedObjects = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2IMAGES_MACHINE withCondition:[self getFinalPredicateForSection:sectionKey]];
        NSArray* theSortedObjects = [unsortedObjects sortedArrayUsingSelector:@selector(compareByName:)];
        [sortedObjectsDictionary setValue:theSortedObjects forKey:sectionKey];
    }

  return [sortedObjectsDictionary valueForKey:sectionKey];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MultiLineDetailUITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MultiLineDetailUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    [self configureCell:(MultiLineDetailUITableViewCell*)cell forObject:[[self sortedImagesForSection:[indexPath section]] objectAtIndex:[indexPath row]] atIndexPath:indexPath];
    return cell;
}

// this needs to match
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 68;
}

#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {
    sortedObjectsDictionary = nil;
    if (![[self hypervisorConnection] isUpdatePendingForType:HYPOBJ_EC2IMAGES_MACHINE]){
        [[self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_EC2IMAGES_MACHINE];
    }
    // the call to stopLoading is on the data update received.
}

// override, and only add the header if not in auto refresh mode
- (bool) isActive {
    return (![[self hypervisorConnection] isAutoUpdateEnabled]);
}

#pragma mark -
#pragma mark HypervisorConnection protocol implementation
- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | HYPOBJ_EC2IMAGES_MACHINE) >0){
        //[self setXenObjectList:[xenHypervisorConnection hypObjectsForType:hypObjectType withCondition:predicate]];
        [self reloadTableData];
        [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
    }
}

#pragma mark - private methods
// combines the search predicate and the root predicate
-(NSPredicate *) getFinalPredicateForSection:(NSString *) sectionKey{
    
    NSPredicate *finalPredicate = [self rootPredicate];
    NSPredicate *sectionPredicate = nil;
    
    if (sectionKey){
        if ([sectionKey isEqualToString: @""]){
            sectionPredicate = [EC2ImageWrapper nameNullOrEmpty];
        }else{
            sectionPredicate = [EC2ImageWrapper nameBeginWith:sectionKey];
        }
    }    

    if (sectionPredicate != nil){
        finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:sectionPredicate, finalPredicate, nil]];
    }
    
    if (searchPredicate != nil){
        finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:searchPredicate, finalPredicate, nil]];
    }
    return finalPredicate;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EC2ImageWrapper *imageForDisplay = [[self sortedImagesForSection:[indexPath section]] objectAtIndex:[indexPath row]];
    NSPredicate *objectPred = [EC2ImageWrapper imageWithReference:[imageForDisplay imageId]];
    
    if (!tabBarController){
        tabBarController = [[UITabBarController alloc] init];
        NSMutableArray *localControllersArray = [[NSMutableArray alloc] initWithCapacity:2];
        

        
        
        // add the general tab
        EC2ImageGeneralViewController *imageGeneralViewController = [[EC2ImageGeneralViewController alloc] initWithhypervisorConnection: [self hypervisorConnection]
                                                                                                                 withImage:imageForDisplay];
        
        [localControllersArray addObject:imageGeneralViewController];
        
        
        tabBarController.viewControllers = localControllersArray;
        
        [tabBarController setTitle:@"Image"];
    }
    else {
        for (TitledDetailViewController* view in [tabBarController viewControllers]){
            // this should be the image general and it should refresh the children ones.
            [view setImage:[ImageBuilder vMTemplateImage]];
            [view setRootObjectPredicate:objectPred andReference:[imageForDisplay imageId]];    
        }
    }
    
    [CommonUIStaticHelper displayNextView:tabBarController usingNavigationController:[self navigationController] andRootViewController:[[[self navigationController] viewControllers]objectAtIndex:0]];
}

@end
