//
//  EC2ImageListByPlatform.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2ImageListByPlatform.h"
#import "ImageBuilder.h"
#import "EC2ImageListViewController.h"
#import "EC2ImageWrapper.h"
#import "ProgressAlert.h"
#import "ConnectionObjectUITableViewCell.h"

#define NILPLTFORMSTRING @"Linux"

@interface EC2ImageListByPlatform ()
- (void) reloadTableData;
-(NSPredicate *) getPlatformPredicateForPlatform:(NSString *) ec2ImagePlatform;
@end

@implementation EC2ImageListByPlatform
@synthesize ec2Platforms, hypConnID ,rootPredicate;

#pragma mark -
#pragma mark View lifecycle

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withPredicate:(NSPredicate *)predicate{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        [self setRootPredicate:predicate];
        [self setHypConnID:[hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
        // always enough room in portrait so only enable scroll if pull to refresh available
        [[self tableView] setScrollEnabled:YES];
        NSMutableArray *tempArray = [NSMutableArray arrayWithCapacity:5];
        // build each platform
        NSArray *images = [hypervisorConnection hypObjectsForType:HYPOBJ_EC2IMAGES_MACHINE withCondition:predicate]; 
        for (EC2ImageWrapper *image in images) {
            NSString *plaform = [image platform];  
            if (plaform == nil){
                plaform = NILPLTFORMSTRING;
            }
            if (![tempArray containsObject:plaform]){
                [tempArray addObject:plaform];
            }
        }
        NSArray *c = [tempArray copy];
        [self setEc2Platforms:c];
    }
    return self;
}

- (HypervisorConnectionFactory*) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (void) dealloc{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self tableView] registerNib:[UINib nibWithNibName:@"ConnectionObjectUITableViewCell" bundle:nil] forCellReuseIdentifier:@"ConnectionObjectUITableViewCell"];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(ConnectionObjectUITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    NSString *displayText;

    NSString *ec2ImagePlatform = (NSString *)[[self ec2Platforms] objectAtIndex:indexPath.row];   
    NSPredicate *finalPredicate = [self getPlatformPredicateForPlatform:ec2ImagePlatform];
    
    long objectCount = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2IMAGES_MACHINE withCondition:finalPredicate] count];
    
    
    displayText = ec2ImagePlatform;
    [[cell imageView2] setImage:[ImageBuilder vMImage]];
    
    NSString *detailText;
    detailText = [NSString stringWithFormat:@"%lu",objectCount];
        
    // set the selection style and the accessory type
    if (objectCount >0){
      [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
      [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    } else {
      [cell setAccessoryType:UITableViewCellAccessoryNone];
      [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
        
    // set the label text
    [[cell textLabel2] setText:displayText];
        
    // set the detail text
    if (detailText != nil) {
       [[cell detailTextLabel2] setText:detailText];
    }
    // make every other row use alternate color
    [cell setAlternate:indexPath.row %2];
    [[cell refreshButton] setHidden:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self ec2Platforms] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"ConnectionObjectUITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ConnectionObjectUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:(ConnectionObjectUITableViewCell*)cell atIndexPath:indexPath];
    
    return cell;

}


#pragma mark -
#pragma mark Table view delegate

ProgressAlert*  progressAlert;

// when selecting the object show the actaul images that meet the defined predicates
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    progressAlert = [[ProgressAlert alloc] initWithTitle:@"Preparing" delegate:self];
    [progressAlert setTag:indexPath.row];
    [progressAlert show];
}

- (void)didPresentAlertView:(UIAlertView *)alertView{
    NSString *ec2ImagePlatform = (NSString *)[[self ec2Platforms] objectAtIndex:[alertView tag]];
    NSPredicate *finalPredicate = [self getPlatformPredicateForPlatform:ec2ImagePlatform];
    
    EC2ImageListViewController *imageListViewController = [[EC2ImageListViewController alloc] initWithHypervisorConnection:[self hypervisorConnection] predicate:finalPredicate];
    
    [imageListViewController setTitle:ec2ImagePlatform];
    [self.navigationController pushViewController:imageListViewController animated:YES];
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    progressAlert = nil;
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}


#pragma mark -
#pragma mark Private methods

-(void) reloadTableData{
    [[self tableView] reloadData];
}

-(NSPredicate *) getPlatformPredicateForPlatform:(NSString *) ec2ImagePlatform{
    NSPredicate *platformPredicate = nil;
    
    if (![ec2ImagePlatform isEqualToString: NILPLTFORMSTRING]){
        platformPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"platform like '%@'",ec2ImagePlatform]];
    }
    else
    {
        platformPredicate = [NSPredicate predicateWithFormat:@"platform == nil"];
    }
    
    NSPredicate *finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:platformPredicate,[self rootPredicate], nil]];
    return finalPredicate;
}


#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {
    if (![[self hypervisorConnection] isUpdatePendingForType:HYPOBJ_EC2IMAGES_MACHINE]){
        [[self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_EC2IMAGES_MACHINE];
    }
    // the call to stopLoading is on the data update received.
}


#pragma mark -
#pragma mark HypervisorConnection protocol implementation

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | HYPOBJ_EC2IMAGES_MACHINE) >0){
        //[self setXenObjectList:[xenHypervisorConnection hypObjectsForType:hypObjectType withCondition:predicate]];
        [self reloadTableData];
        [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
    }
}


@end

