//
//  XenVmDiskViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import <UIKit/UIKit.h>
#import "SectionTitleViewController.h"
#import "EC2ImageGeneralTableViewController.h"
#import "EC2ImageWrapper.h"

@interface EC2ImageGeneralViewController : UIViewController<RefreshablePage,Identifyable>{

    IBOutlet UIView* headerView;
    IBOutlet UIView* mainTableView;
    EC2ImageGeneralTableViewController* tableViewController;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withImage:(EC2ImageWrapper *)thisImage tabName:(NSString*)tabName andTabImageName:(NSString*)tabImageName;

// this constructor defaults to the standard tabname and tabImageName
- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withImage:(EC2ImageWrapper *)thisImage;

@property UIView* headerView;
@property UIView* mainTableView;
@property EC2ImageGeneralTableViewController* tableViewController;

@property (copy) NSString *hypConnID;
@property (copy) NSString *ec2ImageId;
@property EC2_ImageType ec2ImageType;

@end
