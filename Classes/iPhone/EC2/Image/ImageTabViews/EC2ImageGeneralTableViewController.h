//
//  ImageGeneralTableViewController.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "ExpandableTableViewController.h"
#import "EC2HypervisorConnection.h"
#import "EC2ImageWrapper.h"
#import "Identifyable.h"
#import "RefreshablePage.h"

#define EC2IMAGEDETAILSTABLEIMAGEID 0
#define EC2IMAGEDETAILSTABLEIMAGELOCATION 1
#define EC2IMAGEDETAILSTABLEIMAGEOWNERALIAS 2
#define EC2IMAGEDETAILSTABLEPLATFORM 3
#define EC2IMAGEDETAILSTABLEPUBLIC 4
#define EC2IMAGEDETAILSTABLECOUNT 5

@interface EC2ImageGeneralTableViewController : ExpandableTableViewController
            <HypervisorConnectionDelegate,Identifyable,RefreshablePage>{
    HypervisorType hypervisorType;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withImageReference:(NSString *)thisImageReference imageType:(EC2_ImageType)thisImageType;
- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withImage:(EC2ImageWrapper *)thisImage;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

// this is used by relection in the tab contol t o update the page.  In this class it updates the imageId.
- (void) setId:(NSString*)newID;

@property (copy) NSString *hypConnID;
@property (copy) NSString *imageId;
@property EC2_ImageType imageType;
@end
