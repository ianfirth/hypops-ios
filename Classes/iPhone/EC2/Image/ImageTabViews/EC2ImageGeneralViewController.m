//
//  EC2ImageGeneralViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2ImageGeneralViewController.h"

#import "XenHypervisorConnection.h"
#import "XenImageBuilder.h"
#import "SectionTitleViewController.h"
#import "ImageBuilder.h"

@interface EC2ImageGeneralViewController (){
    SectionTitleViewController* headerViewController;
}

@end

@implementation EC2ImageGeneralViewController

@synthesize headerView;
@synthesize tableViewController;
@synthesize mainTableView;

@synthesize hypConnID, ec2ImageId;

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withImage:(EC2ImageWrapper *)thisImage tabName:(NSString*)tabName andTabImageName:(NSString*)tabImageName {
    self = [super initWithNibName:@"EC2ImageGeneralViewController" bundle:nil];
    if (self){
        [self setEc2ImageId: [thisImage imageId]];
        [self setEc2ImageType: [thisImage imageType]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        [self setTableViewController:[[EC2ImageGeneralTableViewController alloc] initWithhypervisorConnection:hypervisorConnection withImage:[self ec2Image]]];
        // set the tab bar button details
        self.tabBarItem.title = tabName;
        //Set the image for the tab bar
        self.tabBarItem.image = [UIImage imageNamed:tabImageName];
    }
    return self;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withImage:(EC2ImageWrapper *)thisImage{
    
    return [self initWithhypervisorConnection:hypervisorConnection withImage:thisImage tabName:@"General" andTabImageName:@"tab_General"];
}

-(EC2HypervisorConnection *) hypervisorConnection{
    return (EC2HypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_EC2 connectonID:[self hypConnID]];
}

- (EC2ImageWrapper *) ec2Image{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"imageId like [c]'%@'", [self ec2ImageId]]];
    
    NSArray *images = [[self hypervisorConnection] hypObjectsForType:[self hypObjectTypeFromImageType:[self ec2ImageType]] withCondition:pred];
    if ([images count] >0){
        return [images objectAtIndex:0];
    }
    return nil;
}

-(NSInteger) hypObjectTypeFromImageType:(EC2_ImageType)this_imageType{
    switch (this_imageType) {
        case EC2_IMAGETYPE_KERNEL:
            return HYPOBJ_EC2IMAGES_KERNEL;
            break;
        case EC2_IMAGETYPE_RAMDISK:
            return HYPOBJ_EC2IMAGES_RAMDISK;
            break;
        case EC2_IMAGETYPE_MACHINE:
            return HYPOBJ_EC2IMAGES_MACHINE;
            break;
        default:
            return -1;
            break;
    }
}

- (void)viewDidLoad
{    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    headerViewController = [[SectionTitleViewController alloc] initWithNibName:@"SectionTitleViewController" bundle:[NSBundle mainBundle] ];
    
    [[self headerView] addSubview:[headerViewController view]];
    [[self mainTableView] addSubview:[[self tableViewController] tableView]];
    [[tableViewController tableView] setAutoresizingMask: UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    CGRect tableViewArea = [[self mainTableView] frame];
    tableViewArea.origin.x = 0;
    tableViewArea.origin.y =0;
    // same size as parent but seeing as its insize it make x and y =0
    [[headerViewController view] setFrame:[[self headerView] frame]];
    [[[self tableViewController] tableView] setFrame:tableViewArea];
    [self updatePageHeading];
}

-(void) updatePageHeading{
    [headerViewController setTheTitle:[[self ec2Image] name]];
#warning what is the right thing to show for an image description?
    [headerViewController setTheDescription:@""];
    [headerViewController setTheImage:[self currentTitleImage]];
}

- (UIImage*) currentTitleImage{
    return [ImageBuilder vMTemplateImage];
}


#pragma mark for Identifyable protocol
-(void) setId:(NSString *)newID{
    [self setEc2ImageId:newID];
    [tableViewController setId:newID];
    [tableViewController refreshPage];
}

#pragma mark for RefreshablePage protocol
-(void) refreshPage{
    [[[self tableViewController] tableView] reloadData];
    [self updatePageHeading];
}

@end
