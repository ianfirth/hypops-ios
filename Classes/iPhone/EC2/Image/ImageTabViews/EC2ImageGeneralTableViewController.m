//
//  ImageGeneralTableViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2ImageGeneralTableViewController.h"
#import "ScrollableDetailTextCell.h"
#import "EC2HypervisorConnection.h"
#import "ImageBuilder.h"

@interface EC2ImageGeneralTableViewController ()
- (void) configureDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index;
@end

@implementation EC2ImageGeneralTableViewController

@synthesize hypConnID, imageId, imageType;

-(HypervisorConnectionFactory *) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

-(NSInteger) hypObjectTypeFromImageType:(EC2_ImageType)this_imageType{
    switch (this_imageType) {
        case EC2_IMAGETYPE_KERNEL:
            return HYPOBJ_EC2IMAGES_KERNEL;
            break;
        case EC2_IMAGETYPE_RAMDISK:
            return HYPOBJ_EC2IMAGES_RAMDISK;
            break;
        case EC2_IMAGETYPE_MACHINE:
            return HYPOBJ_EC2IMAGES_MACHINE;
            break;
        default:
            return -1;
            break;
    }    
}

- (EC2ImageWrapper *) ec2Image{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"imageId like [c]'%@'", [self imageId]]]; 
    NSArray *images = [[self hypervisorConnection] hypObjectsForType:[self hypObjectTypeFromImageType:[self imageType]] withCondition:pred];
    if ([images count] >0){
        return [images objectAtIndex:0];
    }
    return nil;
}


- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withImage:(EC2ImageWrapper *)thisImage{
    return [self initWithhypervisorConnection:hypervisorConnection withImageReference:[thisImage imageId] imageType:[thisImage imageType]];
}

- (void) setId:(NSString*)newID{
    [self setImageId:newID];
}

// ==========================
// = Designated initializer =
// ==========================
- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withImageReference:(NSString *)thisImageReference imageType:(EC2_ImageType)thisImageType{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setImageId: thisImageReference];
        [self setHypConnID: [hypervisorConnection connectionID]];
        [self setImageType: thisImageType];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to vm objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    // could cut this down to just the relevent ones if required
    if (objectType == HYPOBJ_EC2IMAGES_MACHINE){
        [[self tableView ]reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // first section is not expandable
    return (section != 0);
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    // Configure the cell...
    // configuration details
   return EC2IMAGEDETAILSTABLECOUNT;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureDetailsCell:cell atIndex:[indexPath row]];
    return cell;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureDetailsCell:(UITableViewCell *)cell atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    EC2ImageWrapper *image = [self ec2Image];
    
    switch (index) {
        case EC2IMAGEDETAILSTABLEIMAGEID:
        {
            [[cell textLabel] setText:@"ID"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[image imageId]]];
            break;
        }
        case EC2IMAGEDETAILSTABLEIMAGELOCATION:{
            [[cell textLabel] setText:@"Location"];
            [[cell detailTextLabel] setText:  [self convertToDisplayString:[image imageLocation]]];
            break;
        }
        case EC2IMAGEDETAILSTABLEIMAGEOWNERALIAS:{
            [[cell textLabel] setText:@"Owner Alias"];
            [[cell detailTextLabel] setText:  [self convertToDisplayString:[image imageOwnerAlias]]];
            break;
        }
        case EC2IMAGEDETAILSTABLEPLATFORM:{
            [[cell textLabel] setText:@"Platform"];
            [[cell detailTextLabel] setText: [self convertToDisplayString:[image platform]]];
            break;
        }
        case EC2IMAGEDETAILSTABLEPUBLIC:{
            [[cell textLabel] setText:@"Public"];
            BOOL switchState = [image publicValueIsSet] && [image publicValue];
            if (switchState){
                [[cell detailTextLabel] setText: @"On"];
            }
            else{
                [[cell detailTextLabel] setText: @"Off"];
            }
            break;
        }
        default:
            break;
    }    
}

#pragma mark for RefreshablePage protocol
-(void) refreshPage{
    [[self tableView] reloadData];
}

- (UIImage*) currentTitleImage{
    return [ImageBuilder vMTemplateImage];
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}



@end
