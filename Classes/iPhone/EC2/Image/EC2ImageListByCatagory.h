//
//  EC2ImageListByCatagory.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2HypervisorConnection.h"
#import "PullRefreshTableViewController.h"

#define EC2_IMAGE_ALL 0
#define EC2_IMAGE_TAGGED 1
#define EC2_IMAGE_AMAZON 2
#define EC2_IMAGE_PUBLIC 3
#define EC2_IMAGE_PRIVATE 4
#define EC2_IMAGE_32BIT 5
#define EC2_IMAGE_64BIT 6
#define EC2_IMAGE_EBS 7
#define EC2_IMAGE_OWNEDBYME 8

@interface EC2ImageListByCatagory : PullRefreshTableViewController <HypervisorConnectionDelegate> {
    HypervisorType hypervisorType; // allows for the controller to be used by EC2 or CloudStackAWS
}

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection;

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess;

@property (copy) NSString *hypConnID;
@property (nonatomic, strong) NSDictionary *ec2ImageCatagories;

@end
