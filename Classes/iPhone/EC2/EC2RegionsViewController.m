//
//  EC2RegionsViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2RegionsViewController.h"
#import "EC2regionDetailViewController.h"
#import "EC2RegionWrapper.h"
#import "ImageBuilder.h"
#import "EC2ConnectionViewController.h"
#import "CommonUIStaticHelper.h"
#import "EC2RegionUITableViewCell.h"

@interface EC2RegionsViewController (){
    NSMutableDictionary* instanceCounts;
    NSThread* backgroundThead;
}

-(EC2RegionWrapper *) regionForRow:(NSInteger)row;
@end

@implementation EC2RegionsViewController


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [[self tableView] reloadData];
    [super viewWillAppear:animated];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(EC2RegionUITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    EC2RegionWrapper *region = [self regionForRow:[indexPath row]];
    
    [cell setRegion:region];
    InstanceCounter* count = (InstanceCounter*)[instanceCounts valueForKey:[region regionName]];
 
    if (count){
        if ([count totalInstanceCount] >= 0){
            [cell setInstanceCount:[count totalInstanceCount] andRunning:[count runningInstanceCount]];
        }
        else{
            [cell setInstanceCountsUnknown];
        }
    }
    else{
        [cell setInstanceCountsLoading];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    // do alternate striping on the rows
    [cell setAlternate:(indexPath.row % 2)];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    EC2HypervisorConnection *ec2HypervisorConnection = (EC2HypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_EC2 connectonID:[self hypConnID]];
    // Return the number of rows in the section.
    return [[ec2HypervisorConnection hypObjectsForType:HYPOBJ_EC2REGIONS withCondition:nil] count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"EC2RegionUITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[EC2RegionUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:(EC2RegionUITableViewCell*)cell atIndexPath:indexPath];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

-(void) viewDidLoad{
    [super viewDidLoad];
    [[self tableView] registerNib:[UINib nibWithNibName:@"EC2RegionUITableViewCell" bundle:nil] forCellReuseIdentifier:@"EC2RegionUITableViewCell"];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // start a background thread here loading the number of instances for each reagion
    // this will need to be canceled the second the user actually selects a region
    instanceCounts = [[NSMutableDictionary alloc] initWithCapacity:10];
    [[self tableView] reloadData];
    // store the region counts into an array, then the screen can just be updated as appropriate.
    // this actually needs to be a new thead so I can cacel it etc..
    backgroundThead = [[NSThread alloc] initWithTarget:self selector:@selector(loadInstanceCounts) object:nil];
    [backgroundThead start];
//    [self performSelectorInBackground:@selector(loadInstanceCounts) withObject:nil];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (backgroundThead){
        [backgroundThead cancel];
        backgroundThead = nil;
    }
}

-(void) dealloc{
    if (backgroundThead){
        [backgroundThead cancel];
        backgroundThead = nil;
    }
}

/**
 * This is reun in the background to load the current instance counts.
 */
-(void) loadInstanceCounts{
    EC2HypervisorConnection *ec2HypervisorConnection = (EC2HypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_EC2 connectonID:[self hypConnID]];
    
    for (int i = 0 ; i < [[self tableView] numberOfRowsInSection:0] ; i ++){
        if (![[NSThread currentThread] isCancelled]){
            EC2RegionWrapper *region = [self regionForRow:i];
            [ec2HypervisorConnection setEC2ClientEndpoint:[region endpoint]];
            InstanceCounter* instanceCount = [ec2HypervisorConnection numberOfInstances];
            [instanceCounts setValue:instanceCount forKey:[region regionName]];
            // do an update on the table here.
            [[self tableView] reloadData];
        }
    }
}

#pragma mark -
#pragma mark Table view delegate

// when selecting the object show the actaul images that meet the defined predicates
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (backgroundThead){
        [backgroundThead cancel];
        backgroundThead = nil;
    }

    EC2HypervisorConnection *ec2HypervisorConnection = (EC2HypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_EC2 connectonID:[self hypConnID]];
    
    EC2RegionWrapper *region = [self regionForRow:[indexPath row]];

    [ec2HypervisorConnection setEC2ClientEndpoint:[region endpoint]];

    UIViewController<DetailViewProtocol> *ec2ConnectionViewController = [[EC2ConnectionViewController alloc] initWithHypervisorConnection:ec2HypervisorConnection];
    [ec2ConnectionViewController setTitle:[region regionName]];

    [CommonUIStaticHelper displayMasterViewAndDetail:ec2ConnectionViewController usingNavigationController:[self navigationController]];
     
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

#pragma mark -
#pragma mark private helper methods
-(EC2RegionWrapper *) regionForRow:(NSInteger)row{
    EC2HypervisorConnection *ec2HypervisorConnection = (EC2HypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_EC2 connectonID:[self hypConnID]];
    
    EC2RegionWrapper *region = (EC2RegionWrapper *)[[ec2HypervisorConnection hypObjectsForType:HYPOBJ_EC2REGIONS withCondition:nil] objectAtIndex:row];
    return region;
}

- (UIViewController *) detailContentView{
    UIViewController *regionsView = [[EC2RegionDetailViewController alloc] initWithNibName:@"EC2RegionDetailView" bundle:[NSBundle mainBundle]];
    return regionsView;
}

@end
