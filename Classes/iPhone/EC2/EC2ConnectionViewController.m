//
//  EC2ConnectionViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2ConnectionViewController.h"
#import "ImageBuilder.h"
#import "EC2InitialConnectionViewController_iPad.h"
#import "EC2RegionDetailViewController.h"
#import "EC2ImageListByCatagory.h"
#import "EC2InstanceListByStatusViewController.h"
#import "EC2SecurityGroupListViewController.h"
#import "CommonUIStaticHelper.h"
#import "RootViewController_ipad.h"
#import "ConnectionObjectUITableViewCell.h"

@implementation EC2ConnectionViewController

@synthesize ec2Catagories, hypConnID;

- (id) initWithHypervisorConnection:(HypervisorConnectionFactory *) connection{
    self = [super init];
    if (self){
        hypConnID = [connection connectionID];
        hypervisorType = [connection hypervisorConnectionType];
    }
    return self;
}

- (HypervisorConnectionFactory*) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

#pragma mark -
#pragma mark View lifecycle

- (void) viewWillAppear:(BOOL)animated{
    // request data for any object sets that are marked as out of date
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
    
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    
}

- (void) viewWillDisappear:(BOOL)animated{
    [[self hypervisorConnection]removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

/*
 * When load set the title and the name of the pool
 * This should only be done to make sure that the connection is populated with all
 * the top level objects the first time that the page is displayed after connection
 * that is why this is in the on load, otherwise it would happen every time the 
 * page appears.
 */
- (void)viewDidLoad {
    // request data for any object sets that are marked as out of date
    
    [self setEc2Catagories:[NSArray arrayWithObjects:
                            [NSNumber numberWithInt:HYPOBJ_EC2INSTANCES],
                            [NSNumber numberWithInt:HYPOBJ_EC2IMAGES_MACHINE],
                            [NSNumber numberWithInt:HYPOBJ_EC2SECURITYGROUPS],
                            nil]];    
    
    [[self hypervisorConnection] reloadCoreData];
    
    [super viewDidLoad];
    if (self.title == nil){
        self.title = @"EC2 Connection";
    }
    [[self tableView] registerNib:[UINib nibWithNibName:@"ConnectionObjectUITableViewCell" bundle:nil] forCellReuseIdentifier:@"ConnectionObjectUITableViewCell"];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    UIDevice *device = [UIDevice currentDevice];
    if([device userInterfaceIdiom] == UIUserInterfaceIdiomPad){
        UINavigationController *navCon = [self navigationController];
        UIViewController *rootController = [[navCon viewControllers] objectAtIndex:0];
        
        // use detail view for ipad
        if ([rootController respondsToSelector:@selector(detailViewNavigationController)]){
            UINavigationController *dvController = [rootController performSelector:@selector(detailViewNavigationController)];
            if (dvController){
//                if (!inRotation){
                    [RootViewController_ipad setDetailViewRootController:[self detailContentView] onNavigationController:dvController];
//                }
            }
        }
    }
    
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    
}

// TODO - implement this here and in other pages too.
//  it is not proving to be an issue at present so dont wory too much.
// commented out here as have not had time to write alert dialog
// or test this, or implement in the other pages.
- (void) hypervisorEventDataLost{
    // might want to do something like this to cope with event data being lost.
    // could do something like this on all screens 
    // could flush cache, then just do a rewalk of the tree for the object being displayed
    // and hey presto.
    //    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
    //[xenHypervisorConnection flushHypObjectCache];
    //[self reloadDataForConnection:xenHypervisorConnection];
    //[[self tableView] reloadData];
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(ConnectionObjectUITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    NSNumber *ec2Catagory = [[self ec2Catagories] objectAtIndex:indexPath.row];
    NSString *displayText;

    UIImage *image = nil;
    BOOL isLoading = NO;
    BOOL validCatagory = YES;
    long hypObjType = [ec2Catagory longValue];
    long objectCount = 0;
    
    switch (hypObjType) {
        case HYPOBJ_EC2INSTANCES:
            displayText = @"Instances";
            image = [ImageBuilder vMImage];
            break;
        case HYPOBJ_EC2IMAGES_MACHINE:
            displayText = @"Images";
            image = [ImageBuilder vMTemplateImage];
            break;
        case HYPOBJ_EC2SECURITYGROUPS:
            displayText = @"Security Groups";
            image = [ImageBuilder securityImage];
            break;
        default:
            validCatagory = NO;
            break;
    }
    
    if (validCatagory){
        
        NSString *detailText;
        isLoading =  [[self hypervisorConnection] isUpdatePendingForType:hypObjType];

        if (!isLoading){
            objectCount = [[[self hypervisorConnection] hypObjectsForType:hypObjType] count];
            detailText = [NSString stringWithFormat:@"%lu",objectCount];
            // add the navigation indicator to the cell
            if (objectCount > 0){
                [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
            [cell setLoading:NO];
        }
        else {
            // remove the navigation indicator to the cell
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell setLoading:YES];
        }
        
        // set the default selection style
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        
        // put the refresh button in place
        if (!isLoading){
            // construct the refresh button
            [cell setManualRefresh:YES];
            [[cell refreshButton] setTag:hypObjType];
            [[cell refreshButton] addTarget:self action:@selector(refreshButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        }
        else{
            // don't show the refresh button whilst a load is still occuring.
            [cell setManualRefresh:NO];
        }

        // set the image for the cell
        [[cell imageView2] setImage:image];
        [[cell textLabel2] setText:displayText];
        
        // set the detail text
        if (detailText != nil) {
            [[cell detailTextLabel2] setText:detailText];
        }
    }        
    else {
        NSLog(@"Invalid cell catagory provided %@",ec2Catagory);
    }
    [cell setAlternate:indexPath.row %2];
}

-(void)refreshButtonClick:(id)sender{
    // get the button tag
    long hypObjectType = [sender tag];
    [[self hypervisorConnection] RequestHypObjectsForType:hypObjectType];
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark HypervisorConnection protocol implementation
- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.ec2Catagories count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"ConnectionObjectUITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ConnectionObjectUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:(ConnectionObjectUITableViewCell*)cell atIndexPath:indexPath];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // if the cell has no navigation indicator do not allow navigation as the data is still loading.
    if ([[tableView cellForRowAtIndexPath:indexPath] accessoryType] == UITableViewCellAccessoryNone){
        return;
    }
    
    NSNumber *ec2Catagory = [self.ec2Catagories objectAtIndex:indexPath.row];NSUInteger  hypObjType = [ec2Catagory intValue];
    switch (hypObjType) {
        case HYPOBJ_EC2IMAGES_MACHINE:{
            EC2ImageListByCatagory *imageListViewController = [[EC2ImageListByCatagory alloc] initWithHypervisorConnection:[self hypervisorConnection]];
               [self.navigationController pushViewController:imageListViewController animated:YES];
                break;
            }
        case HYPOBJ_EC2INSTANCES:{
            EC2InstanceListByStatusViewController *instanceListByStatusViewController = [[EC2InstanceListByStatusViewController alloc] initWithHypervisorConnection:[self hypervisorConnection]];
            [self.navigationController pushViewController:instanceListByStatusViewController animated:YES];
            break;
        }
        case HYPOBJ_EC2SECURITYGROUPS:{
            EC2SecurityGroupListViewController *securityGroupListViewController = [[EC2SecurityGroupListViewController alloc] initWithHypervisorConnection:[self hypervisorConnection] predicate:nil];
            [self.navigationController pushViewController:securityGroupListViewController animated:YES];
            break;
        }
        default:
            break;
    }
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    //    XenHypervisorConnection *xenHypervisorConnection = (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self hypConnID]];
//    // Releases the view if it doesn't have a superview.
//    [super didReceiveMemoryWarning];
//    // Relinquish ownership any cached data, images, etc. that aren't in use.
//    // kill the connection to (i.e. disconnect)
//    [xenHypervisorConnection closeConnection];
//    
//    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (UIViewController *) detailContentView{
    UIViewController *poolView = [[EC2InitialConnectionViewController_iPad alloc] initWithNibName:@"EC2InitialConnectionView" bundle:[NSBundle mainBundle] hypConnectionType:hypervisorType];
    return poolView;
}

@end

