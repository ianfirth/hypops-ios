//
//  EC2SecurityGroupIPPermissionsTableViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2SecurityGroupIPPermissionsTableViewController.h"
#import "EC2SecurityGroupWrapper.h"
#import "EC2HypervisorConnection.h"
#import "ScrollableDetailTextCell.h"
//#import <AWSiOSSDK/EC2/EC2IpPermission.h>

@interface EC2SecurityGroupIPPermissionsTableViewController ()
- (void)configureCell:(UITableViewCell *)cell forIPPermission:(EC2IpPermission *)ipPermission atIndex:(NSInteger) index ;
- (HypervisorConnectionFactory *) hypervisorConnection;
- (EC2SecurityGroupWrapper *) ec2SecurityGroup;
@end

@implementation EC2SecurityGroupIPPermissionsTableViewController

@synthesize hypConnID, groupId;

-(HypervisorConnectionFactory *) hypervisorConnection{
    return (EC2HypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (EC2SecurityGroupWrapper *) ec2SecurityGroup{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"groupId like [c]'%@'", [self groupId]]]; 
    NSArray *groups = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2SECURITYGROUPS withCondition:pred];
    if ([groups count] >0){
        return [groups objectAtIndex:0];
    }
    return nil;
}

- (id)initWithhypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection withSecurityGroup:(EC2SecurityGroupWrapper *)thisSecurityGroup{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setGroupId: [thisSecurityGroup groupId]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
    }
    return self;
}

-(void)setId:(NSString *)newID{
    [self setGroupId:newID];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}

- (void) viewWillAppear:(BOOL)animated{
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
    [[self hypervisorConnection] setWalkTreeMode:NO];
    [[self tableView] reloadData];
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated{
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
    [super viewWillDisappear:animated];
}

-(void) viewDidAppear:(BOOL)animated{
    // this gets the text in the labels scrolling again when the view appear, if this is not
    // here, then the text will not start scrolling again until selected. or moved off the screen
    // and back again.
    [[self tableView] reloadData];
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark for RefreshablePage protocol
-(void) refreshPage{
#warning should this do anything
}

- (UIImage*) currentTitleImage{
#warning should this have an image
    return nil;
}

#pragma mark -
#pragma mark HypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to nic objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    if (objectType == HYPOBJ_EC2SECURITYGROUPS){
        [[self tableView ]reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of NICs that the host has.
    return [[[self ec2SecurityGroup] ipPermissions] count];
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {

    NSArray *ipPermissions = [[self ec2SecurityGroup] ipPermissions];
                               
    if ([ipPermissions count] == 0){
        return nil;
    }

    // not sure if this is the best thing for the title of the permission at the moment
    EC2IpPermission *ipPermission = [ipPermissions objectAtIndex:section];
    return [ipPermission ipProtocol];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    return EC2SECURITYGROUPT_IPPERMISSIONS_TABLE_COUNT;
}

- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // all sections are expandable
    return YES;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier =  @"Common";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    

    NSArray *ipPermissions = [[self ec2SecurityGroup] ipPermissions];

    // not sure if this is the best thing for the title of the permission at the moment
    EC2IpPermission *ipPermission = [ipPermissions objectAtIndex:indexPath.section];

    [self configureCell:cell forIPPermission:ipPermission atIndex:[indexPath row]];
    return cell;
}

// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureCell:(UITableViewCell *)cell forIPPermission:(EC2IpPermission *)ipPermission atIndex:(NSInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    
    switch (index) {
        case EC2SECURITYGROUPT_IPPERMISSIONS_TABLE_PORTSTART:
            [[cell textLabel] setText:@"from Port"];
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%@", [ipPermission fromPort]]];
            break;
        case EC2SECURITYGROUPT_IPPERMISSIONS_TABLE_PORTEND:
            [[cell textLabel] setText:@"to Port"];
            [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%@", [ipPermission toPort]]];
            break;
        default:
            break;
    }    
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}



@end
