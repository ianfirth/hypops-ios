//
//  EC2SecurityGroupGeneralViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2SecurityGroupGeneralViewController.h"
#import "ScrollableDetailTextCell.h"
#import "EC2HypervisorConnection.h"
#import "ImageBuilder.h"
#import "EC2SecurityGroupWrapper.h"

@interface EC2SecurityGroupGeneralViewController ()
- (void) configureGeneralDetailsCell:(UITableViewCell *)cell atIndex:(NSUInteger) index;
- (void) configureTagsDetailsCell:(UITableViewCell *)cell atIndex:(NSUInteger) index;
@end

@implementation EC2SecurityGroupGeneralViewController

@synthesize hypConnID, groupId;

-(HypervisorConnectionFactory *) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}

- (EC2SecurityGroupWrapper *) ec2SecurityGroup{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"groupId like [c]'%@'", [self groupId]]]; 
    NSArray *groups = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2SECURITYGROUPS withCondition:pred];
    if ([groups count] >0){
        return [groups objectAtIndex:0];
    }
    return nil;
}


- (id)initWithhypervisorConnection:(EC2HypervisorConnection *)hypervisorConnection 
                 withSecurityGroup:(EC2SecurityGroupWrapper *)thisSecurityGroup{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self){
        [self setGroupId: [thisSecurityGroup groupId]];
        [self setHypConnID: [hypervisorConnection connectionID]];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
    }
    return self;
    
}

-(void)setId:(NSString *)newID{
    [self setGroupId:newID];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[self view] setAutoresizesSubviews:YES];
    [[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    UITableView *tableView  = [[UITableView alloc] initWithFrame:[[self view] frame] style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [self setTableView:tableView];
    [tableView setDelegate:self];
    
    // without this the ipad version will always have a gray background
    [[self tableView] setBackgroundView:nil];
    [[self tableView] setBackgroundView:[[UIView alloc] init]];
    [[self tableView] setBackgroundColor:UIColor.whiteColor];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self tableView] reloadData];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (void)dealloc {
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

#pragma mark -
#pragma mark XenHypervisorConnection delegate

- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess{
    // this code should only be checking for changes to vm objects releated to this page
    // all other changes will be handled by the general view controller
    // these are the object types relevent for this page.
    // could cut this down to just the relevent ones if required
    if (objectType == HYPOBJ_EC2SECURITYGROUPS){
        [[self tableView ]reloadData];
    }
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return EC2SECURITYGROUP_TABLE_COUNT;
}



- (BOOL)tableView:(UITableView *)tableView isExpandableSection:(NSInteger)section{
    // first section is not expandable
    return (section != 0);
}

// Provide a title for the section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case EC2SECURITYGROUP_GENERAL_TABLE:
            return nil;
            break;
        case EC2SECURITYGROUP_TAGS_TABLE :
            return @"Tags";
            break;
        default:
            return nil;
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInExpandableSection:(NSInteger)section {
    // Configure the cell...
    // configuration details
    switch (section) {
        case EC2SECURITYGROUP_GENERAL_TABLE:
            return EC2SECURITYGROUP_GENERAL_TABLE_COUNT;
            break;
        case EC2SECURITYGROUP_TAGS_TABLE:{
            EC2SecurityGroupWrapper *group = [self ec2SecurityGroup];
            return [group tags_count];
            break;
        }
        default:
            break;
    }
    // should never get here
    return 0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ScrollableDetailTextCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    switch ([indexPath section]) {
        case EC2SECURITYGROUP_GENERAL_TABLE:
            [self configureGeneralDetailsCell:cell atIndex:[indexPath row]];
            break;
        case EC2SECURITYGROUP_TAGS_TABLE:
            [self configureTagsDetailsCell:cell atIndex:[indexPath row]];
            break;
        default:
            break;
    }

    return cell;
}

//render the tags details
- (void)configureTagsDetailsCell:(UITableViewCell *)cell atIndex:(NSUInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    EC2SecurityGroupWrapper *group = [self ec2SecurityGroup];
    EC2Tag *tag = [[group tags] objectAtIndex:index];
    [[cell textLabel] setText:[tag key]];
    [[cell detailTextLabel] setText: [tag value]];
}


// render the text in the cell from the address of the XenServer to be connected to.
- (void)configureGeneralDetailsCell:(UITableViewCell *)cell atIndex:(NSUInteger) index {
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor whiteColor]];
    EC2SecurityGroupWrapper *group = [self ec2SecurityGroup];
    
    switch (index) {
        case EC2SECURITYGROUP_GENERAL_TABLE_GROUPNAME:
        {
            [[cell textLabel] setText:@"Name"];
            [[cell detailTextLabel] setText: [group groupName]];
            break;
        }
        case EC2SECURITYGROUP_GENERAL_TABLE_GROUPID:
        {
            [[cell textLabel] setText:@"Id"];
            [[cell detailTextLabel] setText: [group groupId]];
            break;
        }
        case EC2SECURITYGROUP_GENERAL_TABLE_OWNERID:
        {
            [[cell textLabel] setText:@"Owner"];
            [[cell detailTextLabel] setText: [group ownerId]];
            break;
        }
        default:
            break;
    }    
}

#pragma mark for RefreshablePage protocol
-(void) refreshPage{
    [[self tableView] reloadData];
}

- (UIImage*) currentTitleImage{
    return [ImageBuilder securityImage];
}



#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}



@end
