//
//  EC2SecurityGroupListViewController.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2SecurityGroupListViewController.h"
#import "EC2SecurityGroupGeneralViewController.h"
#import "EC2SecurityGroupIPPermissionsTableViewController.h"
#import "ImageBuilder.h"
#import "TitledDetailViewController.h"
#import "RootViewController_ipad.h"
#import "EC2SecurityGroupWrapper.h"
#import "EC2HypervisorConnection.h"
#import "CommonUIStaticHelper.h"
#import "MultiLineDetailUITableViewCell.h"

@interface EC2SecurityGroupListViewController (){
    UITabBarController *tabBarController;
}
-(NSPredicate *) getFinalPredicate;
- (void) configureCell:(MultiLineDetailUITableViewCell *)cell forObject:(EC2SecurityGroupWrapper *)securityGroupObject atIndexPath:(NSIndexPath*)indexPath;
- (void) reloadTableData;
@end

@implementation EC2SecurityGroupListViewController
@synthesize hypConnID,rootPredicate;

- (id)initWithHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection predicate:(NSPredicate *) predicate{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self){
        [self setHypConnID:[hypervisorConnection connectionID]];
        [self setRootPredicate:predicate];
        hypervisorType = [hypervisorConnection hypervisorConnectionType];
        
        UISearchBar *theSearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0,320,40)];
        theSearchBar.delegate = self;
        [self.view addSubview:theSearchBar];
        [[self tableView] setTableHeaderView:theSearchBar];
    }
    
    return self;
}

- (HypervisorConnectionFactory*) hypervisorConnection{
    return [HypervisorConnectionFactory getConnectionWithHypervisorType:hypervisorType connectonID:[self hypConnID]];
}


-(void) reloadTableData{
    [[self tableView] reloadData];
}

#pragma mark -
#pragma mark View Search Bar

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    // only show the status bar’s cancel button while in edit mode
    searchBar.showsCancelButton = YES;
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    // flush the previous search content
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
    
    if([searchText isEqualToString:@""] || searchText == nil){
        [[self tableView] reloadData];
        return;
    }
    
    searchPredicate = [EC2SecurityGroupWrapper groupNameBeginsOrContainsAWordBeginningWith:searchText];
    [[self tableView] reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    if (searchPredicate != nil){
        searchPredicate = nil;
    }
    [[self tableView] reloadData];
    
    [searchBar resignFirstResponder];
    searchBar.text = @"";
}

// called when Search (in our case “Done”) button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self tableView] registerNib:[UINib nibWithNibName:@"MultiLineDetailUITableViewCell" bundle:nil] forCellReuseIdentifier:@"MultiLineDetailUITableViewCell"];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    sortedObjects = nil;
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[self hypervisorConnection] addHypervisorConnectionDelegate:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[self hypervisorConnection] removeHypervisorConnectionDelegate:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void) configureCell:(MultiLineDetailUITableViewCell *)cell forObject:(EC2SecurityGroupWrapper *)securityGroupObject atIndexPath:(NSIndexPath*)indexPath{
    // set the lable text for the cell
    [[cell textLabel2] setText:[securityGroupObject groupName]];
    // set the detail text for the cell
    [[cell detailTextLabel2] setText:[securityGroupObject groupId]];
    // set the image for the cell
    [[cell imageView2] setImage:[ImageBuilder securityImage]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    [cell setAlternate:(indexPath.row % 2)];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
    return [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2SECURITYGROUPS withCondition:[self getFinalPredicate]] count];
}

NSArray *sortedObjects;

/**
 * returns the list of images for the view in the correctly sorted order
 */
- (NSArray *) sortedSecurityGroups {    
    if (!sortedObjects){
        NSArray *unsortedObjects = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_EC2SECURITYGROUPS withCondition:[self getFinalPredicate]];
        sortedObjects = [unsortedObjects sortedArrayUsingSelector:@selector(compareByName:)];
    }
    return sortedObjects;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MultiLineDetailUITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MultiLineDetailUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    [self configureCell:(MultiLineDetailUITableViewCell*)cell forObject:[[self sortedSecurityGroups] objectAtIndex:[indexPath row]] atIndexPath:indexPath];
    return cell;
}

#pragma mark - public helper methods

-(UITabBarController *) securityGroupViewControllerForHypervisorConnection:(HypervisorConnectionFactory *)hypervisorConnection 
                                                          EC2SecurityGroup:(EC2SecurityGroupWrapper *)securityGroup{
    
    return tabBarController;
}

#pragma mark - private methods
// combines the search predicate and the root predicate
-(NSPredicate *) getFinalPredicate{
    NSPredicate *finalPredicate = [self rootPredicate];
    
    if (searchPredicate != nil){
        finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:searchPredicate, [self rootPredicate], nil]];
        
    }
    return finalPredicate;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 68;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    EC2SecurityGroupWrapper *securityGroupForDisplay;
    
    NSArray* sgs = [self sortedSecurityGroups];
    if (sgs != nil && [sgs count] >0){
        securityGroupForDisplay = [sgs objectAtIndex:[indexPath row]];
    }
        
    if (!securityGroupForDisplay){
        return;
    }
    
    NSPredicate *groupPred = [EC2SecurityGroupWrapper groupWithReference:[securityGroupForDisplay groupId]];
    UIImage* securityImage = [ImageBuilder securityImage];
    
    if (!tabBarController){
        tabBarController = [[UITabBarController alloc] init];
        NSMutableArray *localControllersArray = [[NSMutableArray alloc] initWithCapacity:2];
        
        
        
        
        // add the general tab
        EC2SecurityGroupGeneralViewController *tbc = [[EC2SecurityGroupGeneralViewController alloc] initWithhypervisorConnection: [self hypervisorConnection]
                                                                                                               withSecurityGroup:securityGroupForDisplay];
        
        
        
        
        TitledDetailViewController *securityGroupGeneral = [[TitledDetailViewController alloc] initWithHypervisorConnection:[self hypervisorConnection] 
                                                                                                              rootObjectRef:[securityGroupForDisplay groupId]
                                                                                                        rootObjectPredicate:groupPred
                                                                                                              hypObjectType:HYPOBJ_EC2SECURITYGROUPS
                                                                                                           namePropertyName:@"groupId"
                                                                                                    descriptionPropertyName:@"descriptionValue"
                                                                                                                displayMode:DISPLAYMODE_NAMEDESCRIPTION
                                                                                                                    tabName:@"General" 
                                                                                                            tabBarImageName:@"tab_General" 
                                                                                                                      image:securityImage
                                                                                                    extentionViewController:tbc];    
        
        [localControllersArray addObject:securityGroupGeneral];
        
        // add the IPPermissions tab
        EC2SecurityGroupIPPermissionsTableViewController *tbcIp = [[EC2SecurityGroupIPPermissionsTableViewController alloc] initWithhypervisorConnection: [self hypervisorConnection]
                                                                                                                                       withSecurityGroup:securityGroupForDisplay];
        
        TitledDetailViewController *securityGroupIPPermissions = [[TitledDetailViewController alloc] initWithHypervisorConnection:[self hypervisorConnection] 
                                                                                                                    rootObjectRef:[securityGroupForDisplay groupId]
                                                                                                              rootObjectPredicate:groupPred
                                                                                                                    hypObjectType:HYPOBJ_EC2SECURITYGROUPS
                                                                                                                 namePropertyName:@"groupId"
                                                                                                          descriptionPropertyName:@"descriptionValue"
                                                                                                                      displayMode:DISPLAYMODE_NAME
                                                                                                                          tabName:@"IP Permissions" 
                                                                                                                  tabBarImageName:@"tab_NIC" 
                                                                                                                            image:securityImage
                                                                                                          extentionViewController:tbcIp];    
        
        [localControllersArray addObject:securityGroupIPPermissions];
        
        
        tabBarController.viewControllers = localControllersArray;
        
        [tabBarController setTitle:@"Security Group"];
    }
    else {
        for (TitledDetailViewController* view in [tabBarController viewControllers]){
            // this should be the image general and it should refresh the children ones.
            [view setImage:securityImage];
            [view setRootObjectPredicate:groupPred andReference:[securityGroupForDisplay groupId]];    
        }
    }
    
    [CommonUIStaticHelper displayNextView:tabBarController usingNavigationController:[self navigationController] andRootViewController:[[[self navigationController] viewControllers] objectAtIndex:0]];
}

#pragma mark -
#pragma mark PullRefreshTableViewController

- (void)refresh {
    sortedObjects = nil;
    if (![[self hypervisorConnection] isUpdatePendingForType:HYPOBJ_EC2SECURITYGROUPS]){
        [[self hypervisorConnection] RequestHypObjectsForType:HYPOBJ_EC2SECURITYGROUPS];
    }
    // the call to stopLoading is on the data update received.
}

// override, and only add the header if not in auto refresh mode
- (bool) isActive {
    return (![[self hypervisorConnection] isAutoUpdateEnabled]);
}

#pragma mark -
#pragma mark HypervisorConnection protocol implementation
- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(NSUInteger)objectType withResult:(BOOL)sucsess{
    // update the list of VMs when this message is received
    if ((objectType | HYPOBJ_EC2SECURITYGROUPS) >0){
        [self reloadTableData];
        if (![hypervisorConnection isAutoUpdateEnabled]){
            [self performSelector:@selector(stopLoading) withObject:nil afterDelay:0.5];
        }
    }
}

@end
