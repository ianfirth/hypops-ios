//
//  RRDDisplayButtonDelegate.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "RoundRobinDatabase.h"
#import "DataPoint.h"

@class RRDDisplayButton;

@protocol RRDDisplayButtonDelegate <NSObject>

@required
/**
 
 */
- (RoundRobinDatabase *)roundRobinDatabase;

@required
- (BOOL) reloadRoundRobinDatabase;

-(NSDate*) getLastRefreshTime;

@required
-(NSString*) graphTitleWithReference:(NSString*)sourcereference;;

@required
-(NSArray*)dataSourcesToGraphFromRRD:(RoundRobinDatabase*)rrd withReference:(NSString*)sourcereference;

//-(double)yMinForDataSources:(NSArray*)dataSources;
//-(double)yMaxForDataSources:(NSArray*)dataSources;
-(double)yMaxForDataSources:(NSArray*)dataSources withConsolidationStep:(NSInteger)step;
-(double)yMinForDataSources:(NSArray*)dataSources withConsolidationStep:(NSInteger)step;
-(double) getPlotPointFromDataPoint:(DataPoint*)dataPoint;

-(NSString*)yTitle;
-(NSNumberFormatter*) yFormatter;
@end
