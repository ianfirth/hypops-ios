//
//  ImageBuilder.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#import "XenImageBuilder.h"
#import "ImageBuilder.h"

/* This is a collection of UI static methods that provide images for the Xen
 * Hypervisor
 */
@implementation XenImageBuilder

// bulild an image that represents the current VM with overlayed power state icon
+ (UIImage* )buildVMImageForVMPowerState:(XenPowerState)powerState{
    UIImage *vmImage = nil;
    switch (powerState) {
        case XENPOWERSTATE_HALTED:
            vmImage = [ImageBuilder vMImageStopped];
            break;
        case XENPOWERSTATE_PAUSED:
            vmImage = [ImageBuilder vMImagePaused];
            break;
        case XENPOWERSTATE_RUNNING:
            vmImage = [ImageBuilder vMImageStarted];
            break;
        case XENPOWERSTATE_SUSPENDED:
            vmImage = [ImageBuilder vMImageSuspended];
            break;
    }
    
    return vmImage;
}


+ (UIImage*) buildVMImageForHost:(XenHost *)xenHost{
    UIImage *vmImage = nil;
    if ([xenHost isInMaintananceMode]){
        vmImage = [ImageBuilder hostImageAlert];
    }
    else{
       vmImage = [ImageBuilder hostImageUp];
   }
   return vmImage;
}

+ (UIImage*) buildVMImageForVM:(XenVM *)xenVM{
    return [XenImageBuilder buildVMImageForVMPowerState:[xenVM power_state]];
}
@end
