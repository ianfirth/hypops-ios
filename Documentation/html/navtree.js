var NAVTREE =
[
  [ "HypOps", "index.html", [
    [ "Class List", "annotated.html", [
      [ "AlertWithDataStore", "interface_alert_with_data_store.html", null ],
      [ "AutoScrollLabel", "interface_auto_scroll_label.html", null ],
      [ "ConnectionsViewController", "interface_connections_view_controller.html", null ],
      [ "ExpandableTableViewController", "interface_expandable_table_view_controller.html", null ],
      [ "<HypervisorConnectionDelegate>", "protocol_hypervisor_connection_delegate-p.html", null ],
      [ "HypervisorConnectionFactory", "interface_hypervisor_connection_factory.html", null ],
      [ "HypOpsAppDelegate", "interface_hyp_ops_app_delegate.html", null ],
      [ "ImageBuilder", "interface_image_builder.html", null ],
      [ "NSData", "class_n_s_data.html", null ],
      [ "NSString", "class_n_s_string.html", null ],
      [ "PowerOperationData", "interface_power_operation_data.html", null ],
      [ "PullRefreshTableViewController", "interface_pull_refresh_table_view_controller.html", null ],
      [ "RootViewController", "interface_root_view_controller.html", null ],
      [ "ScrollableDetailTextCell", "interface_scrollable_detail_text_cell.html", null ],
      [ "<SlideToCancelDelegate>", "protocol_slide_to_cancel_delegate-p.html", null ],
      [ "SlideToCancelViewController", "interface_slide_to_cancel_view_controller.html", null ],
      [ "XenBase", "interface_xen_base.html", null ],
      [ "XenDescriptiveBase", "interface_xen_descriptive_base.html", null ],
      [ "XenGeneralViewController", "interface_xen_general_view_controller.html", null ],
      [ "XenHost", "interface_xen_host.html", null ],
      [ "XenHost_metrics", "interface_xen_host__metrics.html", null ],
      [ "XenHostCPU", "interface_xen_host_c_p_u.html", null ],
      [ "XenHostGeneralTableViewController", "interface_xen_host_general_table_view_controller.html", null ],
      [ "XenHostListViewController", "interface_xen_host_list_view_controller.html", null ],
      [ "XenHostNicsTableViewController", "interface_xen_host_nics_table_view_controller.html", null ],
      [ "XenHostStorageTableViewController", "interface_xen_host_storage_table_view_controller.html", null ],
      [ "XenHypervisorConnection", "interface_xen_hypervisor_connection.html", null ],
      [ "<XenHypervisorConnectionDelegate>", "protocol_xen_hypervisor_connection_delegate-p.html", null ],
      [ "XenHypervisorDelegate", "class_xen_hypervisor_delegate.html", null ],
      [ "XenImageBuilder", "interface_xen_image_builder.html", null ],
      [ "XenNetwork", "interface_xen_network.html", null ],
      [ "XenNetworkListViewController", "interface_xen_network_list_view_controller.html", null ],
      [ "XenNIC", "interface_xen_n_i_c.html", null ],
      [ "XenPBD", "interface_xen_p_b_d.html", null ],
      [ "XenPoolViewController", "interface_xen_pool_view_controller.html", null ],
      [ "XenRefreshableListController", "interface_xen_refreshable_list_controller.html", null ],
      [ "XenStorage", "interface_xen_storage.html", null ],
      [ "XenStorageListViewController", "interface_xen_storage_list_view_controller.html", null ],
      [ "XenTemplateListViewController", "interface_xen_template_list_view_controller.html", null ],
      [ "XenUIWaitForDataLoad", "interface_xen_u_i_wait_for_data_load.html", null ],
      [ "<XenUIWaitForDataLoadDelegate>", "protocol_xen_u_i_wait_for_data_load_delegate-p.html", null ],
      [ "XenVBD", "interface_xen_v_b_d.html", null ],
      [ "XenVDI", "interface_xen_v_d_i.html", null ],
      [ "XenVM", "interface_xen_v_m.html", null ],
      [ "XenVM_guestMetrics", "interface_xen_v_m__guest_metrics.html", null ],
      [ "XenVMDiskViewController", "interface_xen_v_m_disk_view_controller.html", null ],
      [ "XenVMGeneralTableViewController", "interface_xen_v_m_general_table_view_controller.html", null ],
      [ "XenVMListByStatusViewController", "interface_xen_v_m_list_by_status_view_controller.html", null ],
      [ "XenVMListViewController", "interface_xen_v_m_list_view_controller.html", null ],
      [ "XenVMPowerViewController", "interface_xen_v_m_power_view_controller.html", null ],
      [ "XenVMViewController", "interface_xen_v_m_view_controller.html", null ],
      [ "XMLRPCConnection", "interface_x_m_l_r_p_c_connection.html", null ],
      [ "<XMLRPCConnectionDelegate>", "protocol_x_m_l_r_p_c_connection_delegate-p.html", null ],
      [ "XMLRPCConnectionManager", "interface_x_m_l_r_p_c_connection_manager.html", null ],
      [ "XMLRPCEncoder", "interface_x_m_l_r_p_c_encoder.html", null ],
      [ "XMLRPCEventBasedParser", "interface_x_m_l_r_p_c_event_based_parser.html", null ],
      [ "XMLRPCEventBasedParserDelegate", "interface_x_m_l_r_p_c_event_based_parser_delegate.html", null ],
      [ "XMLRPCRequest", "interface_x_m_l_r_p_c_request.html", null ],
      [ "XMLRPCResponse", "interface_x_m_l_r_p_c_response.html", null ]
    ] ],
    [ "Class Index", "classes.html", null ],
    [ "Class Hierarchy", "hierarchy.html", [
      [ "AlertWithDataStore", "interface_alert_with_data_store.html", null ],
      [ "AutoScrollLabel", "interface_auto_scroll_label.html", null ],
      [ "ConnectionsViewController", "interface_connections_view_controller.html", null ],
      [ "ExpandableTableViewController", "interface_expandable_table_view_controller.html", [
        [ "XenHostGeneralTableViewController", "interface_xen_host_general_table_view_controller.html", null ],
        [ "XenHostNicsTableViewController", "interface_xen_host_nics_table_view_controller.html", null ],
        [ "XenHostStorageTableViewController", "interface_xen_host_storage_table_view_controller.html", null ],
        [ "XenVMDiskViewController", "interface_xen_v_m_disk_view_controller.html", null ],
        [ "XenVMGeneralTableViewController", "interface_xen_v_m_general_table_view_controller.html", null ]
      ] ],
      [ "<HypervisorConnectionDelegate>", "protocol_hypervisor_connection_delegate-p.html", [
        [ "RootViewController", "interface_root_view_controller.html", null ]
      ] ],
      [ "HypervisorConnectionFactory", "interface_hypervisor_connection_factory.html", [
        [ "XenHypervisorConnection", "interface_xen_hypervisor_connection.html", null ]
      ] ],
      [ "HypOpsAppDelegate", "interface_hyp_ops_app_delegate.html", null ],
      [ "ImageBuilder", "interface_image_builder.html", null ],
      [ "NSData", "class_n_s_data.html", null ],
      [ "NSString", "class_n_s_string.html", null ],
      [ "PowerOperationData", "interface_power_operation_data.html", null ],
      [ "PullRefreshTableViewController", "interface_pull_refresh_table_view_controller.html", [
        [ "XenRefreshableListController", "interface_xen_refreshable_list_controller.html", [
          [ "XenHostListViewController", "interface_xen_host_list_view_controller.html", null ],
          [ "XenNetworkListViewController", "interface_xen_network_list_view_controller.html", null ],
          [ "XenStorageListViewController", "interface_xen_storage_list_view_controller.html", null ],
          [ "XenTemplateListViewController", "interface_xen_template_list_view_controller.html", null ],
          [ "XenVMListViewController", "interface_xen_v_m_list_view_controller.html", null ]
        ] ],
        [ "XenVMListByStatusViewController", "interface_xen_v_m_list_by_status_view_controller.html", null ]
      ] ],
      [ "ScrollableDetailTextCell", "interface_scrollable_detail_text_cell.html", null ],
      [ "<SlideToCancelDelegate>", "protocol_slide_to_cancel_delegate-p.html", [
        [ "XenVMPowerViewController", "interface_xen_v_m_power_view_controller.html", null ]
      ] ],
      [ "SlideToCancelViewController", "interface_slide_to_cancel_view_controller.html", null ],
      [ "XenBase", "interface_xen_base.html", [
        [ "XenDescriptiveBase", "interface_xen_descriptive_base.html", [
          [ "XenHost", "interface_xen_host.html", null ],
          [ "XenNetwork", "interface_xen_network.html", null ],
          [ "XenStorage", "interface_xen_storage.html", null ],
          [ "XenVDI", "interface_xen_v_d_i.html", null ],
          [ "XenVM", "interface_xen_v_m.html", null ]
        ] ],
        [ "XenHost_metrics", "interface_xen_host__metrics.html", null ],
        [ "XenHostCPU", "interface_xen_host_c_p_u.html", null ],
        [ "XenNIC", "interface_xen_n_i_c.html", null ],
        [ "XenPBD", "interface_xen_p_b_d.html", null ],
        [ "XenVBD", "interface_xen_v_b_d.html", null ],
        [ "XenVM_guestMetrics", "interface_xen_v_m__guest_metrics.html", null ]
      ] ],
      [ "<XenHypervisorConnectionDelegate>", "protocol_xen_hypervisor_connection_delegate-p.html", [
        [ "XenGeneralViewController", "interface_xen_general_view_controller.html", null ],
        [ "XenHostGeneralTableViewController", "interface_xen_host_general_table_view_controller.html", null ],
        [ "XenHostNicsTableViewController", "interface_xen_host_nics_table_view_controller.html", null ],
        [ "XenHostStorageTableViewController", "interface_xen_host_storage_table_view_controller.html", null ],
        [ "XenPoolViewController", "interface_xen_pool_view_controller.html", null ],
        [ "XenRefreshableListController", "interface_xen_refreshable_list_controller.html", null ],
        [ "XenUIWaitForDataLoad", "interface_xen_u_i_wait_for_data_load.html", null ],
        [ "XenVMDiskViewController", "interface_xen_v_m_disk_view_controller.html", null ],
        [ "XenVMGeneralTableViewController", "interface_xen_v_m_general_table_view_controller.html", null ],
        [ "XenVMListByStatusViewController", "interface_xen_v_m_list_by_status_view_controller.html", null ],
        [ "XenVMPowerViewController", "interface_xen_v_m_power_view_controller.html", null ]
      ] ],
      [ "XenHypervisorDelegate", "class_xen_hypervisor_delegate.html", null ],
      [ "XenImageBuilder", "interface_xen_image_builder.html", null ],
      [ "<XenUIWaitForDataLoadDelegate>", "protocol_xen_u_i_wait_for_data_load_delegate-p.html", [
        [ "XenGeneralViewController", "interface_xen_general_view_controller.html", null ],
        [ "XenPoolViewController", "interface_xen_pool_view_controller.html", null ],
        [ "XenRefreshableListController", "interface_xen_refreshable_list_controller.html", null ]
      ] ],
      [ "XenVMViewController", "interface_xen_v_m_view_controller.html", null ],
      [ "XMLRPCConnection", "interface_x_m_l_r_p_c_connection.html", null ],
      [ "<XMLRPCConnectionDelegate>", "protocol_x_m_l_r_p_c_connection_delegate-p.html", [
        [ "XenHypervisorConnection", "interface_xen_hypervisor_connection.html", null ]
      ] ],
      [ "XMLRPCConnectionManager", "interface_x_m_l_r_p_c_connection_manager.html", null ],
      [ "XMLRPCEncoder", "interface_x_m_l_r_p_c_encoder.html", null ],
      [ "XMLRPCEventBasedParser", "interface_x_m_l_r_p_c_event_based_parser.html", null ],
      [ "XMLRPCEventBasedParserDelegate", "interface_x_m_l_r_p_c_event_based_parser_delegate.html", null ],
      [ "XMLRPCRequest", "interface_x_m_l_r_p_c_request.html", null ],
      [ "XMLRPCResponse", "interface_x_m_l_r_p_c_response.html", null ]
    ] ],
    [ "Class Members", "functions.html", null ],
    [ "File List", "files.html", [
      [ "/Users/ianfirth/Documents/Xcode/hypOps/ConnectionsViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/ConnectionsViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/main.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/AlertWithDataStore.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/AlertWithDataStore.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/HypervisorConnectionDelegate.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/HypOpsAppDelegate.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/hypOpsAppDelegate.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/ImageBuilder.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/ImageBuilder.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/RootViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/RootViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/XenVMViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/XenVMViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/HypervisorConnectionDelegate.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/HypervisorConnectionFactory.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/HypervisorConnectionFactory.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XenHypervisorConnection.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XenHypervisorConnection.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenBase.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenBase.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenDescriptiveBase.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenDescriptiveBase.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenHost.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenHost.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenHost_metrics.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenHost_metrics.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenHostCPU.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenHostCPU.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenNetwork.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenNetwork.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenNIC.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenNIC.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenPBD.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenPBD.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenStorage.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenStorage.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenVBD.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenVBD.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenVDI.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenVDI.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenVM.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenVM.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenVM_guestMetrics.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/DataObjects/XenVM_guestMetrics.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/ExpandableTableViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/ExpandableTableViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/XenGeneralViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/XenGeneralViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/XenImageBuilder.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/XenImageBuilder.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/XenPoolViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/XenPoolViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/XenRefreshableListController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/XenRefreshableListController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/XenUIWaitForDataLoad.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/XenUIWaitForDataLoad.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/Host/XenHostListViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/Host/XenHostListViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/Host/HostTabViews/XenHostGeneralTableViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/Host/HostTabViews/XenHostGeneralTableViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/Host/HostTabViews/XenHostNicsTableViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/Host/HostTabViews/XenHostNicsTableViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/Host/HostTabViews/XenHostStorageTableViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/Host/HostTabViews/XenHostStorageTableViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/Network/XenNetworkListViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/Network/XenNetworkListViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/Storage/XenStorageListViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/Storage/XenStorageListViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/Template/XenTemplateListViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/Template/XenTemplateListViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/VM/XenVMListByStatusViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/VM/XenVMListByStatusViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/VM/XenVMListViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/VM/XenVMListViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/VM/VMTabViews/XenVMDiskViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/VM/VMTabViews/XenVMDiskViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/VM/VMTabViews/XenVMGeneralTableViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/VM/VMTabViews/XenVMGeneralTableViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/VM/VMTabViews/XenVMPowerViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/UI/VM/VMTabViews/XenVMPowerViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/NSDataAdditions.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/NSDataAdditions.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/NSStringAdditions.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/NSStringAdditions.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPC.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCConnection.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCConnection.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCConnectionDelegate.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCConnectionManager.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCConnectionManager.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCEncoder.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCEncoder.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCEventBasedParser.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCEventBasedParser.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCEventBasedParserDelegate.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCEventBasedParserDelegate.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCRequest.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCRequest.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCResponse.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC/XMLRPCResponse.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/NSDataAdditions.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/NSDataAdditions.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/NSStringAdditions.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/NSStringAdditions.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPC.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCConnection.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCConnection.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCConnectionDelegate.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCConnectionManager.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCConnectionManager.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCEncoder.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCEncoder.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCEventBasedParser.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCEventBasedParser.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCEventBasedParserDelegate.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCEventBasedParserDelegate.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCRequest.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCRequest.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCResponse.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/backend/XenServer/XMLRPC_previousver(now 2.2.1)/XMLRPCResponse.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/Common/AutoScrollLabel.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/Common/AutoScrollLabel.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/Common/PullRefreshTableViewController copy.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/Common/PullRefreshTableViewController copy.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/Common/PullRefreshTableViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/Common/PullRefreshTableViewController.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/Common/ScrollableDetailTextCell.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/Common/ScrollableDetailTextCell.m", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/Common/SlideToCancelViewController.h", null, null ],
      [ "/Users/ianfirth/Documents/Xcode/hypOps/Classes/Common/SlideToCancelViewController.m", null, null ]
    ] ]
  ] ]
];

function createIndent(o,domNode,node,level)
{
  if (node.parentNode && node.parentNode.parentNode)
  {
    createIndent(o,domNode,node.parentNode,level+1);
  }
  var imgNode = document.createElement("img");
  if (level==0 && node.childrenData)
  {
    node.plus_img = imgNode;
    node.expandToggle = document.createElement("a");
    node.expandToggle.href = "javascript:void(0)";
    node.expandToggle.onclick = function() 
    {
      if (node.expanded) 
      {
        $(node.getChildrenUL()).slideUp("fast");
        if (node.isLast)
        {
          node.plus_img.src = node.relpath+"ftv2plastnode.png";
        }
        else
        {
          node.plus_img.src = node.relpath+"ftv2pnode.png";
        }
        node.expanded = false;
      } 
      else 
      {
        expandNode(o, node, false);
      }
    }
    node.expandToggle.appendChild(imgNode);
    domNode.appendChild(node.expandToggle);
  }
  else
  {
    domNode.appendChild(imgNode);
  }
  if (level==0)
  {
    if (node.isLast)
    {
      if (node.childrenData)
      {
        imgNode.src = node.relpath+"ftv2plastnode.png";
      }
      else
      {
        imgNode.src = node.relpath+"ftv2lastnode.png";
        domNode.appendChild(imgNode);
      }
    }
    else
    {
      if (node.childrenData)
      {
        imgNode.src = node.relpath+"ftv2pnode.png";
      }
      else
      {
        imgNode.src = node.relpath+"ftv2node.png";
        domNode.appendChild(imgNode);
      }
    }
  }
  else
  {
    if (node.isLast)
    {
      imgNode.src = node.relpath+"ftv2blank.png";
    }
    else
    {
      imgNode.src = node.relpath+"ftv2vertline.png";
    }
  }
  imgNode.border = "0";
}

function newNode(o, po, text, link, childrenData, lastNode)
{
  var node = new Object();
  node.children = Array();
  node.childrenData = childrenData;
  node.depth = po.depth + 1;
  node.relpath = po.relpath;
  node.isLast = lastNode;

  node.li = document.createElement("li");
  po.getChildrenUL().appendChild(node.li);
  node.parentNode = po;

  node.itemDiv = document.createElement("div");
  node.itemDiv.className = "item";

  node.labelSpan = document.createElement("span");
  node.labelSpan.className = "label";

  createIndent(o,node.itemDiv,node,0);
  node.itemDiv.appendChild(node.labelSpan);
  node.li.appendChild(node.itemDiv);

  var a = document.createElement("a");
  node.labelSpan.appendChild(a);
  node.label = document.createTextNode(text);
  a.appendChild(node.label);
  if (link) 
  {
    a.href = node.relpath+link;
  } 
  else 
  {
    if (childrenData != null) 
    {
      a.className = "nolink";
      a.href = "javascript:void(0)";
      a.onclick = node.expandToggle.onclick;
      node.expanded = false;
    }
  }

  node.childrenUL = null;
  node.getChildrenUL = function() 
  {
    if (!node.childrenUL) 
    {
      node.childrenUL = document.createElement("ul");
      node.childrenUL.className = "children_ul";
      node.childrenUL.style.display = "none";
      node.li.appendChild(node.childrenUL);
    }
    return node.childrenUL;
  };

  return node;
}

function showRoot()
{
  var headerHeight = $("#top").height();
  var footerHeight = $("#nav-path").height();
  var windowHeight = $(window).height() - headerHeight - footerHeight;
  navtree.scrollTo('#selected',0,{offset:-windowHeight/2});
}

function expandNode(o, node, imm)
{
  if (node.childrenData && !node.expanded) 
  {
    if (!node.childrenVisited) 
    {
      getNode(o, node);
    }
    if (imm)
    {
      $(node.getChildrenUL()).show();
    } 
    else 
    {
      $(node.getChildrenUL()).slideDown("fast",showRoot);
    }
    if (node.isLast)
    {
      node.plus_img.src = node.relpath+"ftv2mlastnode.png";
    }
    else
    {
      node.plus_img.src = node.relpath+"ftv2mnode.png";
    }
    node.expanded = true;
  }
}

function getNode(o, po)
{
  po.childrenVisited = true;
  var l = po.childrenData.length-1;
  for (var i in po.childrenData) 
  {
    var nodeData = po.childrenData[i];
    po.children[i] = newNode(o, po, nodeData[0], nodeData[1], nodeData[2],
        i==l);
  }
}

function findNavTreePage(url, data)
{
  var nodes = data;
  var result = null;
  for (var i in nodes) 
  {
    var d = nodes[i];
    if (d[1] == url) 
    {
      return new Array(i);
    }
    else if (d[2] != null) // array of children
    {
      result = findNavTreePage(url, d[2]);
      if (result != null) 
      {
        return (new Array(i).concat(result));
      }
    }
  }
  return null;
}

function initNavTree(toroot,relpath)
{
  var o = new Object();
  o.toroot = toroot;
  o.node = new Object();
  o.node.li = document.getElementById("nav-tree-contents");
  o.node.childrenData = NAVTREE;
  o.node.children = new Array();
  o.node.childrenUL = document.createElement("ul");
  o.node.getChildrenUL = function() { return o.node.childrenUL; };
  o.node.li.appendChild(o.node.childrenUL);
  o.node.depth = 0;
  o.node.relpath = relpath;

  getNode(o, o.node);

  o.breadcrumbs = findNavTreePage(toroot, NAVTREE);
  if (o.breadcrumbs == null)
  {
    o.breadcrumbs = findNavTreePage("index.html",NAVTREE);
  }
  if (o.breadcrumbs != null && o.breadcrumbs.length>0)
  {
    var p = o.node;
    for (var i in o.breadcrumbs) 
    {
      var j = o.breadcrumbs[i];
      p = p.children[j];
      expandNode(o,p,true);
    }
    p.itemDiv.className = p.itemDiv.className + " selected";
    p.itemDiv.id = "selected";
    $(window).load(showRoot);
  }
}

